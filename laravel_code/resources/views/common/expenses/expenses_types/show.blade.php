@extends($main_layout.'.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">
            جميع أنواع المصاريف
        </div>
        <div class="panel-body">
            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>النوع</td>
                    <td>هل يحمل علي المنتج ؟</td>
                    <td>نسبه التحميل علي المنتج</td>
                    <td>مشاهدة المصاريف</td>
                    <td>تعديل</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>النوع</td>
                    <td>هل يحمل علي المنتج ؟</td>
                    <td>نسبه التحميل علي المنتج</td>
                    <td>مشاهدة المصاريف</td>
                    <td>تعديل</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_expenses_types as $key => $expenses_type): ?>
                <tr id="row<?= $expenses_type->expense_type_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$expenses_type->expense_type_name}}</td>
                    <td>{{($expenses_type->afford_on_product==1)?"Afford":"Not Afford"}}</td>
                    <td>{{$expenses_type->afford_percentage}}</td>
                    <td>
                        <a href="{{url("/admin/expenses/show_all_expenses/$expenses_type->expense_type_id/0/0/0")}}">
                            مشاهدة المصاريف
                        </a>
                    </td>

                    <td>
                        <a href="<?= url("admin/expenses/save_expenses_type/$expenses_type->expense_type_id") ?>">
                            <span class="label label-info"> تعديل <i class="fa fa-edit"></i></span>
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>


@endsection
