@extends($main_layout.'.main_layout')

@section('subview')

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <style>
                    hr{
                        width: 100%;
                        height:1px;
                    }
                </style>
                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }

                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>


                <?php

                $header_text="إضافة نوع مصروف جديد";
                $expense_type_id="";
                if ($expenses_type_data!="") {
                    $header_text="تعديل نوع المصروف  ".$expenses_type_data->expense_type_name;
                    $expense_type_id=$expenses_type_data->expense_type_id;
                }

                ?>



                <div class="panel panel-info">
                    <div class="panel-heading"><h2><?=$header_text?></h2></div>
                    <div class="panel-body">
                        <div class="row">
                            <form id="save_form" action="<?=url("admin/expenses/save_expenses_type/$expense_type_id")?>" method="POST" enctype="multipart/form-data">

                                <?php
                                    $normal_tags=array("expense_type_name","afford_percentage");

                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $expenses_type_data,
                                        "yes",
                                        "required"
                                    );


                                    $attrs[0]["expense_type_name"]="الاسم";
                                    $attrs[0]["afford_percentage"]="نسبه التحمل علي المنتجات";

                                    $attrs[3]["afford_percentage"]="number";


                                    echo
                                    generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                    );
                                ?>

                                <hr>

                                <?php
                                    echo
                                    generate_select_tags(
                                        $field_name="afford_on_product",
                                        $label_name="يحمل علي المنتج ؟",
                                        $text=["Yes","No"],
                                        $values=[1,0],
                                        $selected_value="",
                                        $class="form-control",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = $expenses_type_data
                                    );
                                ?>

                                <hr>



                                {{csrf_field()}}
                                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
                            </form>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>

@endsection
