@extends($main_layout.'.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            العمليات
        </div>
        <div class="panel-body" style="text-align: center">
            <a class="btn btn-primary" href="{{url("/common/expenses/save_expenses")}}">اضف جديد</a>


        </div>
    </div>

    <input type="hidden" id="group_by_id" value="{{$group_by}}">

    <div class="panel panel-info">
        <div class="panel-heading">
            بحث في كل المصاريف
        </div>
        <div class="panel-body">

            <form action="{{url("/common/expenses/show_all_expenses")}}" method="get">

                <div class="col-md-12">
                    <div class="col-md-3">

                        <div class="form-group">
                            @include("common.category.accounts_tree.tree_modal")

                            <label for="" style="visibility: hidden;">اظهر الشجرة</label>
                            
                            <button
                                    type="button"
                                    class="btn btn-primary show_tree_modal center-block"
                                    data-select_what="category"
                                    data-select_type="single"
                                    data-div_to_show_selection=".move_to_new_category_div"
                                    data-input_field_name="expenses_type_id"
                            >
                                اظهر الشجرة لتختار التصنيف
                            </button>


                            <div class="move_to_new_category_div" style="text-align: center;">
                                <?php if(isset($expense_cat_data)): ?>

                                    <label class="label label-primary selected_individual_item" data-item_id="5">
                                        {{$expense_cat_data->cat_name}}
                                        <a href="#" class="remove_selected_individual_item">×</a>
                                        <input type="hidden" name="expenses_type_id" value="{{$expense_cat_data->cat_id}}">
                                    </label>

                                <?php endif; ?>
                            </div>

                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">من</label>
                            <input type="date" id="filter_date_from" name="expenses_date_from" value="{{$expenses_date_from}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">الي</label>
                            <input type="date" id="filter_date_to" name="expenses_date_to" value="{{$expenses_date_to}}" class="form-control">

                        </div>
                    </div>

                    <div class="col-md-3">
                        <?php
                            echo
                            generate_select_tags(
                                $field_name="group_by",
                                $label_name="الفلترة علي اساس",
                                $text=["لا اساس","الفرع","نوع المصروف","الفرع مع نوع المصروف"],
                                $values=["","user","expenses_type_id","user_and_expenses_type_id"],
                                $selected_value=[$group_by],
                                $class="form-control",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = "",
                                $grid=""
                            );

                        ?>

                    </div>



                </div>

                {{csrf_field()}}

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button class="btn btn-info">نتائج البحث</button>
                    </div>
                </div>

            </form>






        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            كل المصاريف

        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <?php if(empty($group_by)): ?>

                <?php
                    $expenses_accept_amount_sum=0;
                    $expenses_refused_amount_sum=0;
                ?>
                @include("common.expenses.expenses.show_table")
            <?php elseif($group_by=="user"): ?>
                <?php
                    $all_expenses_groups=collect($all_expenses_rows)->groupBy("user_id")->all();
                ?>

                <div class="panel-group" id="accordion">
                    <?php foreach($all_expenses_groups as $key=>$all_expenses_rows): ?>

                        <?php
                            if(count($all_expenses_rows)==0){
                                continue;
                            }
                        ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                                <h4 class="panel-title" >
                                    <a>{{$all_expenses_rows[0]->branch_full_name}} مصروفات</a>
                                </h4>
                            </div>
                            <div id="collapse{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php
                                        $expenses_accept_amount_sum=0;
                                        $expenses_refused_amount_sum=0;
                                    ?>
                                    @include("common.expenses.expenses.show_table")
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>

                </div>

            <?php elseif($group_by=="expenses_type_id"): ?>
                <?php
                    $all_expenses_groups=collect($all_expenses_rows)->groupBy("cat_id")->all();
                ?>

                <div class="panel-group" id="accordion">
                    <?php foreach($all_expenses_groups as $key=>$all_expenses_rows): ?>

                        <?php
                            if(count($all_expenses_rows)==0){
                                continue;
                            }
                        ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                                <h4 class="panel-title" >
                                    <a>  مصروفات -{{$all_expenses_rows[0]->cat_name}}</a>
                                </h4>
                            </div>
                            <div id="collapse{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php
                                        $expenses_accept_amount_sum=0;
                                        $expenses_refused_amount_sum=0;
                                    ?>
                                    @include("common.expenses.expenses.show_table")
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>

                </div>

            <?php elseif($group_by=="user_and_expenses_type_id"): ?>
                <?php
                    $all_expenses_users=collect($all_expenses_rows)->groupBy("user_id")->all();
                ?>

                <div class="panel-group" id="accordion">
                    <?php foreach($all_expenses_users as $key=>$user_expenses): ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                                <h4 class="panel-title" >
                                    <a>  مصروفات -{{$user_expenses[0]->branch_full_name}}</a>
                                </h4>
                            </div>
                            <div id="collapse{{$key}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php
                                        $user_expenses=$user_expenses->groupBy("cat_id")->all();
                                    ?>

                                    <?php foreach($user_expenses as $child_key=>$all_expenses_rows): ?>

                                        <?php
                                            if(count($all_expenses_rows)==0){
                                                continue;
                                            }

                                            $expenses_accept_amount_sum=0;
                                            $expenses_refused_amount_sum=0;
                                        ?>

                                        <div class="panel panel-info">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#collapse{{$key}}" href="#inner_collapse{{$child_key}}" style="cursor:pointer;">
                                                {{$all_expenses_rows[0]->cat_name}}
                                            </div>
                                            <div id="inner_collapse{{$child_key}}" class="panel-collapse collapse">
                                                <div class="panel-body" style="overflow-x: scroll;">
                                                    @include("common.expenses.expenses.show_table")
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach;?>

                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>

                </div>


            <?php endif; ?>






        </div>
    </div>




@endsection
