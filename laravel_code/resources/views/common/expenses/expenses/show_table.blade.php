<table id="cat_table_1" class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%">
    <thead>
    <tr>
        <td>#</td>
        <td>مصروف الفرع</td>
        <td>مدخل المصروف</td>
        <td>نوع المصروف</td>
        <td>اسم المصروف</td>
        <td>الوصف</td>
        <td>المبلغ</td>
        <td>التاريخ</td>

        <?php if(check_permission($user_permissions,$page_permission,"change_expense_status",$current_user)): ?>
            <td>حالة المصروف</td>
        <?php endif; ?>

        <?php if(false && check_permission($user_permissions,$page_permission,"edit_action",$current_user)): ?>
            <td>تعديل</td>
        <?php endif; ?>

        <?php if(check_permission($user_permissions,$page_permission,"delete_action",$current_user)): ?>
            <td>مسح</td>
        <?php endif; ?>

    </tr>
    </thead>



    <tbody>
    <?php foreach ($all_expenses_rows as $key => $expenses_row): ?>
    <?php
    if($expenses_row->expense_status=="0"){
        $expenses_refused_amount_sum=$expenses_refused_amount_sum+$expenses_row->expense_amount;

    }
    else if($expenses_row->expense_status=="1"){

        $expenses_accept_amount_sum=$expenses_accept_amount_sum+$expenses_row->expense_amount;
    }
    ?>

    <tr id="row<?= $expenses_row->expense_id ?>">
        <td><?=$key+1?></td>
        <td>{{$expenses_row->branch_full_name}}</td>
        <td>{{$expenses_row->expense_maker_name}}</td>
        <td>{{$expenses_row->cat_name}}</td>
        <td>{{$expenses_row->expense_name}}</td>
        <td><p>{{$expenses_row->expense_desc}}</p></td>
        <td>{{$expenses_row->expense_amount}}</td>
        <td>{{dump_date($expenses_row->expense_date)}}</td>

        <?php if(check_permission($user_permissions,$page_permission,"change_expense_status",$current_user)): ?>
            <td>
                <?php if($expenses_row->expense_status): ?>
                <label class="label label-info">تمت الموافقة عليه</label>
                <?php else: ?>
                    <?php
                        echo generate_multi_accepters(
                            $accepturl=url("/common/expenses/change_expense_status"),
                            $item_obj=$expenses_row,
                            $item_primary_col="expense_id",
                            $accept_or_refuse_col="expense_status",
                            $model="",
                            $accepters_data=["1"=>"وافق"]
                        );
                    ?>
                <?php endif; ?>
            </td>
        <?php endif; ?>

        <?php if(false && check_permission($user_permissions,$page_permission,"edit_action",$current_user)): ?>
            <td>
                <a href="<?= url("common/expenses/save_expenses/$expenses_row->expense_id") ?>">
                    <span class="label label-info"> تعديل<i class="fa fa-edit"></i></span>
                </a>
            </td>
        <?php endif; ?>

        <?php if(check_permission($user_permissions,$page_permission,"delete_action",$current_user)): ?>
            <td>
                <?php if($expenses_row->expense_status): ?>
                    <span class="label label-warning">غير مسموح بعد الموفقه</span>
                <?php else: ?>
                    <a href='#' class="general_remove_item" data-tablename="App\models\expenses\expenses_m" data-deleteurl="<?= url("/common/expenses/remove_expenses") ?>" data-itemid="<?= $expenses_row->expense_id ?>">
                        <span class="label label-danger"> مسح <i class="fa fa-remove"></i></span>
                    </a>
                <?php endif; ?>
            </td>
        <?php endif; ?>

    </tr>
    <?php endforeach ?>
    </tbody>


    <tfoot>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="success">
            المبالغ الموافق عليها {{$expenses_accept_amount_sum}}
            <br>
            المبالغ الغير موافق عليها {{$expenses_refused_amount_sum}}
        </td>
        <td></td>

        <?php if(check_permission($user_permissions,$page_permission,"change_expense_status",$current_user)): ?>
        <td></td>
        <?php endif; ?>

        <?php if(false && check_permission($user_permissions,$page_permission,"edit_action",$current_user)): ?>
        <td></td>
        <?php endif; ?>

        <?php if(check_permission($user_permissions,$page_permission,"delete_action",$current_user)): ?>
        <td></td>
        <?php endif; ?>

    </tr>
    </tfoot>

</table>
