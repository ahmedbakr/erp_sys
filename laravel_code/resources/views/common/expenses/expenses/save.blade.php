@extends($main_layout.'.main_layout')

@section('subview')

    <div class="">

        <style>
            hr{
                width: 100%;
                height:1px;
            }
        </style>
        <?php

        if (count($errors->all()) > 0)
        {
            $dump = "<div class='alert alert-danger'>";
            foreach ($errors->all() as $key => $error)
            {
                $dump .= $error." <br>";
            }
            $dump .= "</div>";

            echo $dump;
        }

        if (isset($success)&&!empty($success)) {
            echo $success;
        }
        ?>


        <?php

            $header_text="إضافة مصروف جديد";
            $expense_id="";
            if ($expenses_data!="") {
                $header_text="تعديل المصروف  ".$expenses_data->expense_name;
                $expense_id=$expenses_data->expense_id;
            }

        ?>



        <div class="panel panel-info">
            <div class="panel-heading"><?=$header_text?></div>
            <div class="panel-body">
                <form id="save_form" action="<?=url("common/expenses/save_expenses/$expense_id")?>" method="POST" enctype="multipart/form-data">

                        @include("common.category.accounts_tree.tree_modal")

                        <div class="form-group">



                            <?php if(is_object($expenses_data)): ?>
                                <div class="alert alert-primary">
                                    لقد اخترت
                                    {{$expenses_data->cat_name}}
                                </div>

                            <?php else: ?>
                                <div class="alert alert-info" style="text-align: center;">
                                    ملحوظة : لا يمكنك ان تتمم عملية الحفظ الا اذا اخترت تصنيف
                                </div>
                            <?php endif; ?>



                            <button
                                    type="button"
                                    class="btn btn-primary show_tree_modal center-block"
                                    data-select_what="category"
                                    data-select_type="single"
                                    data-div_to_show_selection=".move_to_new_category_div"
                                    data-input_field_name="expense_type_id"
                            >
                                اظهر الشجرة لتختار التصنيف
                            </button>


                            <div class="move_to_new_category_div" style="text-align: center;">

                            </div>

                        </div>


                        <hr>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <?php
//                                    echo
//                                    generate_select_tags(
//                                        $field_name="user_id",
//                                        $label_name="المصروف خاص ب:",
//                                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
//                                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
//                                        $selected_value="",
//                                        $class="form-control",
//                                        $multiple="",
//                                        $required="",
//                                        $disabled = "",
//                                        $data = $expenses_data
//                                    );
                                    echo generate_depended_selects(
                                        $field_name_1="user_id",
                                        $field_label_1="اختار الفرع",
                                        $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                                        $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                                        $field_selected_value_1="",
                                        $field_required_1="",
                                        $field_class_1="form-control",
                                        $field_name_2="expenses_maker",
                                        $field_label_2="اختار المستخدم",
                                        $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                                        $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                                        $field_selected_value_2="",
                                        $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                                        $field_required_2="",
                                        $field_class_2="form-control",
                                        $add_blank_options=true,
                                        $obj_data=""
                                    );
                                ?>
                            </div>
                        </div>


                        <hr>

                        <?php
                        $normal_tags=array("expense_name","expense_amount","expense_date","expense_desc");

                        $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $expenses_data,
                            "yes",
                            "required"
                        );


                        $attrs[0]["expense_name"]="اسم المصروف";
                        $attrs[0]["expense_desc"]="الوصف";
                        $attrs[0]["expense_amount"]="المبلغ";
                        $attrs[0]["expense_date"]="التاريخ";

                        $attrs[3]["expense_desc"]="textarea";
                        $attrs[3]["expense_amount"]="number";
                        $attrs[3]["expense_date"]="date";


                        $attrs[6]["expense_name"]="4";
                        $attrs[6]["expense_desc"]="12";
                        $attrs[6]["expense_amount"]="4";
                        $attrs[6]["expense_date"]="4";



                        echo
                        generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5]),
                            reformate_arr_without_keys($attrs[6])
                        );
                        ?>

                        <hr>

                        <?php
                            echo
                            generate_slider_imgs_tags(
                                $slider_photos=(isset($expenses_data->slider_items)&&isset_and_array($expenses_data->slider_items))?$expenses_data->slider_items:"",
                                $field_name="slider_file",
                                $field_label="الملفات",
                                $field_id="slider_file_id",
                                $accept="*",
                                $need_alt_title="no",
                                $additional_inputs_arr=array(),
                                $show_as_link=true,
                                $add_item_label="إضافة ملحق جديد"
                            );
                        ?>



                        <hr>



                        {{csrf_field()}}
                        <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
                    </form>
            </div>
        </div>


    </div>

@endsection
