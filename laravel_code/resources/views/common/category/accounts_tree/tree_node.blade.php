<?php
    if (isset($data_index_prefix)){
        $categories_str=$data_index_prefix."categories";
        $categories=$$categories_str;

        $category_items_str=$data_index_prefix."category_items";
        $category_items_str=$$category_items_str;
    }

?>




<?php foreach($categories as $key=>$cat): ?>

    <div class="accordion load_cat_childs" data-catname="{{$cat->cat_name}}" data-catid="{{$cat->cat_id}}" data-loadchilds="false" data-cattype="{{$cat->cat_type}}">
        <div class="accordion-group">
            <div class="accordion-heading accordion-toggle account_category" data-toggle="collapse" href=".cat_{{$cat->cat_id}}" data-catid="{{$cat->cat_id}}" data-catname="<?=$cat->{"cat_name".$en}?>" >
                <a>
                    <?=$cat->{"cat_name".$en}?>
                </a>

                <a class="dropdown-toggle category_links" data-toggle="dropdown" href="#" style="font-style: italic">
                    ...
                </a>

                <ul class="dropdown-menu category_links_dropdown">

                    <li>
                        <a href="{{url("/common/category/save/$cat->cat_type/$cat->parent_id/$cat->cat_id")}}">تعديل بيانات التصنيف</a>
                    </li>
                    <li class="divider"></li>

                    <li>
                        <a href="{{url("/common/category/save/$cat->cat_type/$cat->cat_id")}}">اضف تصنيف جديد</a>
                    </li>
                    <li class="divider"></li>

                    <li>
                        <a href="{{url("/common/category/move_category_to_another_parent/$cat->cat_type/$cat->cat_id")}}">نقل التصنيف</a>
                    </li>
                    <li class="divider"></li>

                    <?php if(in_array($current_user->user_type,["admin","factory_admin"])&&$selected_cat_type=="product"): ?>
                    <li>
                        <a href="{{url("/admin/product/save_products_on_stock/$cat->cat_id")}}">اضف جديد ({{$selected_cat_type_text[$selected_cat_type]}})</a>
                    </li>
                    <li class="divider"></li>
                    <?php endif; ?>



                    <li>
                        <a href="#" class="general_remove_item"
                           data-deleteurl="{{url("/common/category/remove_category")}}"
                           data-itemid="{{$cat->cat_id}}" data-parentdiv=".load_cat_childs">
                            مسح التصنيف
                        </a>
                    </li>


                    <li class="select_li" style="display: none;" data-item_id="{{$cat->cat_id}}" data-item_name="<?=$cat->{"cat_name".$en}?>">
                        <a href="#">اختر</a>
                    </li>
                </ul>
            </div>

            <div class="accordion-body collapse cat_{{$cat->cat_id}}">
                <div class="accordion-inner">

                    {{-- check if there is items or not  --}}
                    <?php if(isset($category_items[$cat->cat_id])): ?>
                        <div class="row" style="text-align: center;">
                            <div class="form-group col-md-12">
                                <label for="">ابحث بالاسم او بالباركود</label>
                                <input type="text" class="form-control search_for_item">
                                <input type="checkbox" class="search_by_name">:بالاسم
                                <input type="checkbox" class="search_by_barcode">:الباركود
                            </div>
                        </div>

                        <ul class="nav nav-list items_list">
                            <?php foreach($category_items[$cat->cat_id] as $key=>$item): ?>
                                <li class="item_itself" data-itemit="{{$item->pro_id}}" data-itembarcode="{{$item->pro_barcode}}" data-itemname="<?=$item->{"pro_name".$en}?>">
                                    <a>
                                        <i class="icon-chevron-right"></i> <?=$item->{"pro_name".$en}?>
                                    </a>

                                        <a class="dropdown-toggle item_links" data-toggle="dropdown" href="#" style="font-style: italic" aria-expanded="false">...</a>
                                        <ul class="dropdown-menu item_links_dropdown">
                                            <?php if(in_array($current_user->user_type,["admin","factory_admin"])): ?>

                                            <li>
                                                <a href="{{url("/admin/product/save_products_on_stock/$cat->cat_id/$item->pro_id")}}">تعديل بيانات منتج</a>
                                            </li>


                                            <li class="divider"></li>
                                            <li>
                                                <a href="#" class="general_remove_item"
                                                   data-deleteurl="{{url("/admin/product/remove_product_from_stock")}}"
                                                   data-itemid="{{$item->pro_id}}" data-parentdiv=".item_itself">
                                                    مسح منتج
                                                </a>
                                            </li>

                                            <?php endif; ?>



                                            <li class="select_li" style="display: none;" data-item_id="{{$item->pro_id}}" data-item_name="<?=$item->{"pro_name".$en}?>">
                                                <a href="#">اختر</a>
                                            </li>
                                        </ul>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif; ?>

                    <!--load here other childs-->

                </div>
            </div>
        </div>
    </div>

<?php endforeach;?>
