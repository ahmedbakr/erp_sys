



<div id="tree_modal" class="modal fade {{(isset($data_index_prefix)?$data_index_prefix:"")}}tree_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> شجرة ({{$selected_cat_type_text[$selected_cat_type]}})</h4>
            </div>
            <div class="modal-body">


                <div class="menu">
                    <div class="row" style="text-align: center;">
                        <div class="form-group col-md-8 col-md-offset-2">
                            <label for="">ابحث عن تصنيف</label>
                            <input type="text" class="form-control search_for_category">
                        </div>
                    </div>

                    @include("common.category.accounts_tree.tree_node")
                </div>

                <hr>

                <div class="selected_items_div center-block" style="text-align: center;">
                    <p>
                        لقد اخترت
                    </p>
                    <br>

                    <div class="selected_items"></div>

                    <br>


                    <button type="button" class="btn btn-info approve_selected_items_btn">
                        تم الاختيار
                    </button>
                </div>



            </div>
        </div>

    </div>
</div>