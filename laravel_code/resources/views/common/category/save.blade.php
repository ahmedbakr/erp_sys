@extends($main_layout.'.main_layout')

@section('subview')


    <?php
        $header_text="اضف تصنيف جديد "."($selected_cat_type_text[$selected_cat_type])";
        $cat_id="";
        if (is_object($cat_obj)){
            $header_text="تعديل تصنيف";
            $cat_id=$cat_obj->cat_id;
        }

        if (count($errors->all()) > 0)
        {
            $dump = "<div class='alert alert-danger'>".implode(" <br> ",$errors->all())."</div>";
            echo $dump;
        }

        if (isset($success)&&!empty($success)) {
            echo $success;
        }
    ?>


        <div class="panel panel-info">
            <div class="panel-heading">
                {{$header_text}}
            </div>
            <div class="panel-body save_category">

                <form action="<?=url("common/category/save/$selected_cat_type/$parent_id/$cat_id")?>" method="POST" enctype="multipart/form-data">


                    <input type="hidden" name="parent_id" value="{{$parent_id}}">
                    <?php
                    $normal_tags=array(
                        'cat_code', 'cat_name', 'cat_name_en',
                        'cat_path', 'cat_path_en'
                    );

                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $cat_obj,
                        "yes",
                        ""
                    );


                    $attrs[0]["cat_code"]="كود التصنيف";
                    $attrs[0]["cat_name"]="اسم التصنيف";
                    $attrs[0]["cat_name_en"]="اسم التصنيف انجليزي";
                    $attrs[0]["cat_path"]="مسار التصنيف";
                    $attrs[0]["cat_path_en"]="مسار التصنيف انجليزي";

                    $attrs[2]["cat_name"]="required";
                    $attrs[2]["cat_name_en"]="required";
                    $attrs[2]["cat_path"]="required";
                    $attrs[2]["cat_path_en"]="required";

                    $attrs[6]["cat_code"]="12";
                    $attrs[6]["cat_name"]="6";
                    $attrs[6]["cat_name_en"]="6";
                    $attrs[6]["cat_path"]="6";
                    $attrs[6]["cat_path_en"]="6";


                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>


                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
                </form>





            </div>
        </div>
@endsection