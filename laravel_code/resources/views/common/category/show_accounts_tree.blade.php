@extends($main_layout.'.main_layout')

@section('subview')



    <?php if(check_permission($user_permissions,"product/products_on_stock","show_action",$current_user)&&$selected_cat_type=="product"): ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            المنتجات
        </div>
        <div class="panel-body" style="overflow-y: scroll;">
            <a class="btn btn-info" href="{{url('/admin/product/products_on_stock')}}">المنتجات (في الفرع الرئيسي)</a>
            <a class="btn btn-info" href="{{url('/admin/product/products_on_branches_stock')}}">المنتجات (في بقية الافرع)</a>
        </div>
    </div>

    <?php endif; ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            ({{$selected_cat_type_text[$selected_cat_type]}})
        </div>
        <div class="panel-body" style="overflow-y: scroll;">
            {!! $tree_html !!}
        </div>
    </div>




@endsection