@extends('front.main_layout')

@section('subview')

    <div class="top-header">
        <div class="container">
            <div class="row">

                <div class="col-md-10 top_nav_dates">
                    <div class="date">
                        هجري :
                        {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                    </div>
                    <div class="date">
                        ميلادي :
                        {{$current_date}}
                    </div>
                    <div class="date direction_rtl">
                        الوقت :
                        <span class="show_time"></span>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="lang">
                        <a href="#" class="btn">English</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo">
                        <h1>{{$company->company_name}}</h1>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="box-login">
                        <form  method="POST" action="{{url('/admin_panel')}}">
                            <div class="col-md-5">
                                <label for="login_email">البريد الالكتروني او اسم المستخدم</label>
                                <input type="text" id="login_email" name="email" placeholder="البريد الالكتروني او اسم المستخدم" />
                            </div>
                            <div class="col-md-5">
                                <label for="login_password">كلمة السر</label>
                                <input type="password" id="login_password" name="password" placeholder="كلمة السر" />
                            </div>
                            <div class="col-md-2">
                                {{csrf_field()}}
                                <input type="submit" class="btn btn-primary" value="تسجيل الدخول">
                            </div>
                            <div class="col-md-12">
                                {!! \Session::get("msg") !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="container">
        <div class="row">

            <div class="col-md-6" style="margin-top: 50px;">

                <div class="col-md-12 padd-left padd-right direction_rtl">
                    <div class="send-mail">
                        <h3 class="heading_panel">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <input type="text" id="receiver_email" class="form-control receiver_email" placeholder="ايميل المستلم">
                                </div>
                                <div class="col-md-3 send_email_label">
                                    <label for="receiver_email">أرسل إلي </label>
                                </div>
                            </div>
                        </h3>
                        <textarea rows="5" class="form-control email_body" placeholder="نص الرسالة"></textarea>
                        <div class="row">
                            <div class="col-md-7 col-md-offset-1 show_custom_email_msg"></div>
                            <div class="col-md-3">
                                <button class="btn btn-info send_custom_email" > إرسال </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 padd-left padd-right direction_rtl">
                    <div class="send-mail">
                        <h3 class="heading_panel">إشعارات عامة</h3>
                        <p>على السادة مديري المعارض الدخول على شاشة العروض ومعرفة عرض اليوم </p>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="forgood">
                    <img src="{{get_image_or_default("$company->path")}}" />
                    <p>
                        {{$company->company_short_desc}}
                    </p>
                </div>
            </div>


        </div>
    </div>

@endsection
