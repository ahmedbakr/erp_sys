@extends('front.main_layout')

@section('subview')

    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <div class="lang">
                        <a href="{{url('/')}}" class="btn logout_link"><i class="fa fa-lock"></i> خروج  </a>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="lang">
                        <a href="#" class="btn">English</a>
                    </div>
                </div>

                <div class="col-md-10 top_nav_dates">
                    <div class="date">
                        هجري :
                        {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                    </div>
                    <div class="date">
                        ميلادي :
                        {{$current_date}}
                    </div>
                    <div class="date direction_rtl">
                        الوقت :
                        <span class="show_time"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6 header_logo">
                    <div class="logo">
                        <img src="{{get_image_or_default('public_html/img/seoera.png')}}" style="width: 9%" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-logout color_white">
                        <h3>معرض الضباب</h3>
                        <p>
                            اخر دخول :-
                            {{date("Y-m-d H:i:s")}}
                            <a href="#">الخصائص</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header float_right">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav float_right">
                    <li><a href="#">تقرير المبيعات اليومية</a></li>
                    <li><a href="#">شاشة طلب أجازة</a></li>
                    <li><a href="#">تحويلات مخازن</a></li>
                    <li><a href="#">إدخال بضاعه</a></li>
                    <li><a href="#">أمر توريد</a></li>
                    <li><a href="#">فتح الدرج</a></li>
                    <li><a href="#">مبيعات الكاشيير</a></li>
                    <li class="active"><a href="#">الرئيسية</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <div class="container">

        <h1 class="text_align_center">الاشعارات والتنبيهات</h1>

        <div class="row">
            <div class="col-md-12">
                <table id="cat_table" class="table table-striped table-bordered direction_rtl" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>القسم</td>
                        <td>الاشعار</td>
                        <td>التاريخ</td>
                        <td>مسح</td>
                    </tr>
                    </thead>

                    <tbody>
                    <tr id="row1">
                        <td>1</td>
                        <td>
                            <div class="jstree_demo_div">
                                <ul>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاصناف والمخازن
                                        <ul>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>أمر توريد</li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        </td>
                        <td>تم انشاء امر توريد رقم 123</td>
                        <td>
                            {{date("Y-m-d H:i:s")}}
                        </td>

                        <td><a href='#'><span class="label label-danger">مسح <i class="fa fa-remove"></i></span></a></td>
                    </tr>

                    <tr id="row1">
                        <td>1</td>
                        <td>
                            <div class="jstree_demo_div">
                                <ul>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تحويلات مخازن
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td>تم إنشاء تحويل مخازن برقم 123 لمعرض الشقا</td>
                        <td>
                            {{date("Y-m-d H:i:s")}}
                        </td>

                        <td><a href='#'><span class="label label-danger">مسح <i class="fa fa-remove"></i></span></a></td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>


@endsection
