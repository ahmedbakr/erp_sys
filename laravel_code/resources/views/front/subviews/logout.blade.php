@extends('front.main_layout')

@section('subview')

    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 top_nav_dates">
                    <div class="date">
                        هجري :
                        {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                    </div>
                    <div class="date">
                        ميلادي :
                        {{$current_date}}
                    </div>
                    <div class="date direction_rtl">
                        الوقت :
                        <span class="show_time"></span>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="lang">
                        <a href="#" class="btn">English</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="logo">
                        <img src="{{get_image_or_default('public_html/img/seoera.png')}}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-logout">
                        <h1>{{$company->company_name}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="container">
        <div class="row">

            <div class="col-md-8">

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right">
                        <h3 class="heading_panel">شكرا</h3>
                        <p class="paragraph_content">
                            تم تسجيل الخروج بنجاح
                            <br>
                            لقد خرجت من النظام شكرا لاستخدامك Point Of Sale
                            <br>
                            <br>
                            دخول جديد
                            <br>
                            <a href="{{url("/")}}">انقر هنا لتسجيل الدخول مرة اخري</a>
                        </p>
                    </div>
                </div>

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail">
                        <p class="paragraph_content">
                            {!! $company->after_logout_text_1 !!}
                        </p>
                    </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right">
                        <h3 class="heading_panel">معلومات</h3>
                        <p class="paragraph_content">
                            {!! $company->after_logout_text_2 !!}
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <footer class="logout_footer">
        <a href="http://www.perfumesgate.com/" style="float: left;" target="_blank">http://www.perfumesgate.com/</a>
    </footer>

@endsection
