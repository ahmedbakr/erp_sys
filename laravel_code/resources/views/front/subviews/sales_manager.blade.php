@extends('front.main_layout')

@section('subview')

    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <div class="lang">
                        <a href="{{url('/')}}" class="btn logout_link"><i class="fa fa-lock"></i> خروج  </a>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="lang">
                        <a href="#" class="btn">English</a>
                    </div>
                </div>

                <div class="col-md-10 top_nav_dates">
                    <div class="date">
                        هجري :
                        {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                    </div>
                    <div class="date">
                        ميلادي :
                        {{$current_date}}
                    </div>
                    <div class="date direction_rtl">
                        الوقت :
                        <span class="show_time"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6 header_logo">
                    <div class="logo">
                        <img src="{{get_image_or_default('public_html/img/seoera.png')}}" style="width: 9%" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-logout color_white">
                        <h3>معرض الضباب</h3>
                        <p>
                            اخر دخول :-
                            {{date("Y-m-d H:i:s")}}
                            <a href="#">الخصائص</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header float_right">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav float_right">
                    <li><a href="#">تقرير المبيعات اليومية</a></li>
                    <li><a href="#">شاشة طلب أجازة</a></li>
                    <li><a href="#">تحويلات مخازن</a></li>
                    <li><a href="#">إدخال بضاعه</a></li>
                    <li><a href="#">أمر توريد</a></li>
                    <li><a href="#">فتح الدرج</a></li>
                    <li><a href="#">مبيعات الكاشيير</a></li>
                    <li class="active"><a href="#">الرئيسية</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right direction_rtl">
                        <h3 class="heading_panel">شاشات المستخدم</h3>
                        <div class="jstree_demo_div">
                            <ul>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاصناف والمخازن
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>أمر توريد</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>إدخال بضاعه</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تحويلات مخازن</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>العملاء والمبيعات
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>مبيعات الكاشيير</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>فاتورة مبيعات</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تقارير المبيعات
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقرير المبيعات اليومية</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تقارير مالية
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقرير النقدية بالدرج</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تقارير الأصناف والمخازن
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقرير حركة الاصناف</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقرير اوامر التوريد</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقرير التصنيع</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right">
                        <h3 class="heading_panel">الاشعارات والتنبيهات</h3>
                        <p class="paragraph_content">على السادة مديري المعارض الدخول على شاشة العروض ومعرفة عرض اليوم
                        <br>
                        <br>
                            الرجاء مراجعه الايميلات
                        </p>
                    </div>
                </div>

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right">
                        <h3 class="heading_panel">المبيعات اليومية للمعارض</h3>

                        <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                            <tbody>
                                <tr>
                                    <td>نقدي : </td>
                                    <td>8000 ريال</td>
                                </tr>
                                <tr>
                                    <td>شيك : </td>
                                    <td>8000 ريال</td>
                                </tr>
                                <tr>
                                    <td>أجل : </td>
                                    <td>4000 ريال</td>
                                </tr>
                                <tr>
                                    <td>الإجمالي : </td>
                                    <td>20000 ريال</td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right">
                        <h3 class="heading_panel">التارجت الشهري للمعرض</h3>

                        <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                            <tbody>
                            <tr>
                                <td>المطلوب تحقيقة : </td>
                                <td>50000 ريال</td>
                            </tr>
                            <tr>
                                <td>المحقق : </td>
                                <td>10000 ريال</td>
                            </tr>
                            <tr>
                                <td>الباقي : </td>
                                <td>40000 ريال</td>
                            </tr>
                            <tr>
                                <td>باقي </td>
                                <td>يوم علي انتهاء الشهر</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
