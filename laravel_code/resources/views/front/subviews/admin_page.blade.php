@extends('front.main_layout')

@section('subview')

    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <div class="lang">
                        <a href="{{url('/')}}" class="btn logout_link"><i class="fa fa-lock"></i> خروج  </a>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="lang">
                        <a href="#" class="btn">English</a>
                    </div>
                </div>

                <div class="col-md-10 top_nav_dates">
                    <div class="date">
                        هجري :
                        {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                    </div>
                    <div class="date">
                        ميلادي :
                        {{$current_date}}
                    </div>
                    <div class="date direction_rtl">
                        الوقت :
                        <span class="show_time"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6 header_logo">
                    <div class="logo">
                        <img src="{{get_image_or_default('public_html/img/seoera.png')}}" style="width: 9%" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-logout color_white">
                        <h3>معرض الضباب</h3>
                        <p>
                            اخر دخول :-
                            {{date("Y-m-d H:i:s")}}
                            <a href="#">الخصائص</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header float_right">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav float_right">
                    <li><a href="#">تقرير المبيعات اليومية</a></li>
                    <li><a href="#">شاشة طلب أجازة</a></li>
                    <li><a href="#">تحويلات مخازن</a></li>
                    <li><a href="#">إدخال بضاعه</a></li>
                    <li><a href="#">أمر توريد</a></li>
                    <li><a href="#">فتح الدرج</a></li>
                    <li><a href="#">مبيعات الكاشيير</a></li>
                    <li class="active"><a href="#">الرئيسية</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right direction_rtl">
                        <h3 class="heading_panel">أرصدة الحسابات</h3>
                        <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                            <tbody>
                            <tr>
                                <td>الرصيد الحالي للخزينة : </td>
                                <td>50000 ريال</td>
                            </tr>
                            <tr>
                                <td>الرصيد الحالي للبنك الاهلي : </td>
                                <td>10000 ريال</td>
                            </tr>
                            <tr>
                                <td>الرصيد الحالي لبنك الرياض : </td>
                                <td>40000 ريال</td>
                            </tr>
                            <tr>
                                <td>رصيد شبكات تحت التحصيل : </td>
                                <td>40000 ريال</td>
                            </tr>
                            <tr>
                                <td>الرصيد الحالي لصناديق المعارض : </td>
                                <td>40000 ريال</td>
                            </tr>
                            <tr>
                                <td>إجمالي رصيد العملاء الاجلين : </td>
                                <td>40000 ريال</td>
                            </tr>
                            <tr>
                                <td>إجمالي رصيد الموردين : </td>
                                <td>40000 ريال</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail text_align_right direction_rtl">
                        <h3 class="heading_panel">الموظفين</h3>
                        <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                            <tbody>
                                <tr>
                                    <td>عدد طلبات الاجازة : </td>
                                    <td>200 اجازة</td>
                                </tr>
                                <tr>
                                    <td>عدد الموظفين الغائبين : </td>
                                    <td>300 موظف</td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">

                <div class="col-md-12 padd-left padd-right">
                    <div class="send-mail direction_rtl">
                        <h3 class="heading_panel">الصفحة الرئيسية</h3>
                        <div class="jstree_demo_div">
                            <ul>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاعدادات العامة
                                    <ul>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>بيانات الشركة</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>الاعدادات العامة</li>
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>صلاحيات المستخدمين</li>
                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الأصناف والمخازن
                                    <ul>
                                        <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>أكواد أساسية
                                            <ul>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>الأكواد</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>بيانات المخزن</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>مستندات الاضافة والصرف</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>مجموعات الاصناف</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>المصروفات المباشرة المصروفات الغير مباشرة</li>
                                            </ul>
                                        </li>
                                        <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>عمليات المخازن
                                            <ul>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>اذن اضافة</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>اذن صرف</li>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تحويلات المخازن</li>
                                            </ul>
                                        </li>
                                        <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تصنيع المنتجات
                                            <ul>
                                                <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>امر تصنيع</li>
                                            </ul>
                                        </li>
                                        <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>التقارير
                                            <ul>
                                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تقارير الاًصناف
                                                    <ul>
                                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقارير حركة الاصناف</li>
                                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقارير تفاصيل الاصناف</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>العملاء و المبيعات</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الموردين والمشتريات</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>شئون الموظفين</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاصول الثابته</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الحركة المالية</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الشيكات</li>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الحسابات العامة</li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>


        </div>
    </div>


@endsection
