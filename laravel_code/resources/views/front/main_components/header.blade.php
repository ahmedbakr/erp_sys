<!DOCTYPE HTML>
<html>
<head>
    <title>{{$meta_title}}</title>
    <meta name="description" content="<?php echo $meta_desc ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public_html/admin')}}/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/login.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/general.css" rel='stylesheet' type='text/css'/>

    <!-- font-awesome CSS -->
    <link href="{{url('/public_html/admin')}}/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <!-- lined-icons -->
    <link rel="stylesheet" href="{{url('/public_html/admin')}}/css/icon-font.min.css" type='text/css'/>
    <!-- //lined-icons -->

    <!--datatable css-->
    <link href="<?= url('public_html/admin/js/datatables/css/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= url('public_html/admin/js/datatables/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet" type="text/css">


    <!----webfonts--->
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <!---//webfonts--->


    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/public_html/admin')}}/js/bootstrap.min.js"></script>


    <!--datatable js-->
    <script src="<?= url('public_html/admin/js/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= url('public_html/admin/js/datatables/js/dataTables.bootstrap.min.js') ?>"></script>

    <!--istree -->
    <link href="<?= url('public_html/admin/tree/themes/default/style.min.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= url('public_html/admin/tree/js/jstree.js') ?>"></script>

    <script src="<?= url('public_html/jscode/admin/config.js') ?>"></script>
    <script src="<?= url('public_html/jscode/homepage.js') ?>"></script>

</head>

<body>


    <!-- hidden csrf -->

    <input type="hidden" class="csrf_input_class" value="{{csrf_token()}}">
    <!-- /hidden csrf -->
    <!-- hidden base url -->
    <input type="hidden" class="url_class" value="<?= url("/") ?>">
    <!-- /hidden base url -->