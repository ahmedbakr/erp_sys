@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة كوبون جديد";
    $coupon_id="";

    if ($coupon_data!="") {
        $header_text="تعديل كوبون ".$coupon_data->coupon_code;

        $coupon_id=$coupon_data->coupon_id;
    }

    //dump($pro_data);
    ?>

    <!--new_editor-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
    <!--END new_editor-->

    <div class="panel panel-primary">
        <div class="panel-heading">أضف كوبونات من ملف اكسيل</div>
        <div class="panel-body">
            <div class="row">
                <form id="save_form" action="<?=url("/admin/coupons/import_coupons")?>" method="POST" enctype="multipart/form-data">

                    <div class="alert alert-info" style="font-weight: bold;">
                        <h3 style="text-align: center;">برجاء الإنتباه قبل تحميل ملف الاكسيل برجاء اتباع ما يلي :-</h3>
                        <ul>
                            <li> برجاء تحميل هذا الملف وملئ الصفوف قبل الرفع <a href="{{url('uploads/excel_templates/coupons_template.xlsx')}}" target="_blank">التحميل من هنا</a></li>
                            <li>يجيب ان يكون إمتداد الملف واحد من هؤلاء ["xls",".XLS",".xlsx",".XLSX.")</li>
                            <li>يجب ترك اول صف كما هو دون تغيير او مسح او تعديل اي عمود</li>
                            <li>اذا تم كتابه كوبون موجود مسبقا سيتم تجاهله وعدم إدخاله</li>
                            <li>إذا تم كتاب اكتر من صف داخل ملف الاكسيل بنفس الكود للكوبون سيتم إدخال كوبون واحد فقط وهو الاول </li>
                            <li>اذا كانت قيمة الكوبون نسبه برجاء وضع هذة العلامه % في خانه is rate </li>
                            <li>coupon end date: <br>(5/24/2017)</li>
                            <li>يجب ادخال بالاسفل عدد مرات الاستخدام لكل كوبون داخل الملف ويجب الا تقل عن 1</li>
                        </ul>

                    </div>


                    <div class="col-md-4">
                        <?php
                            echo generate_select_tags(
                                $field_name="cat_id",
                                $label_name="اختار التصنيف",
                                $text=$all_coupon_cats->pluck("cat_name")->all(),
                                $values=$all_coupon_cats->pluck("cat_id")->all(),
                                $selected_value=[""],
                                $class="form-control select_2_class",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = "",
                                $grid="12",
                                $show_label_as_option=false
                            );
                        ?>
                    </div>

                    <div class="col-md-4">
                        <label for="">
                            <b>عدد مرات الإستخدام لكل كوبون</b>
                        </label>
                        <input type="number" min="1" class="form-control" name="coupon_used_times" value="1">
                    </div>

                    <div class="col-md-4">
                        <?php
                            echo generate_img_tags_for_form(
                                $filed_name="excel_file",
                                $filed_label="excel_file",
                                $required_field="",
                                $checkbox_field_name="pro_excel_checkbox",
                                $need_alt_title="no",
                                $required_alt_title="",
                                $old_path_value="",
                                $old_title_value="",
                                $old_alt_value="",
                                $recomended_size="",
                                $disalbed="",
                                $displayed_img_width="100",
                                $display_label="رفع ملف اكسيل للكوبونات",
                                $img_obj = ""
                            );
                        ?>
                    </div>



                    {{csrf_field()}}

                    <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-2 btn btn-primary btn-lg" style="margin-top: 5px;">

                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/coupons/save_coupon/$coupon_id")?>" method="POST" enctype="multipart/form-data">


                    <?php
                    echo generate_select_tags(
                        $field_name="cat_id",
                        $label_name="اختار التصنيف",
                        $text=$all_coupon_cats->pluck("cat_name")->all(),
                        $values=$all_coupon_cats->pluck("cat_id")->all(),
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $coupon_data,
                        $grid="12",
                        $show_label_as_option=false
                    );
                    ?>

                    <?php

                    echo
                    generate_select_tags(
                            $field_name="is_rate",
                            $label_name="نوع الكوبون",
                            $text=array("نسبة","مبلغ"),
                            $values=[1,0],
                            $selected_value=array(""),
                            $class="form-control",
                            $multiple="",
                            $required = "",
                            $disabled = "",
                            $data = $coupon_data
                    );
                    ?>
                    <hr>

                    <?php

                    $normal_tags=array('coupon_code','coupon_used_times','coupon_end_date','coupon_value');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $coupon_data,
                            "yes",
                            $required="required"
                    );


                    $attrs[0]["coupon_code"]="كود الكوبون *";
                    $attrs[0]["coupon_used_times"]="عدد مرات استخدام الكوبون *";
                    $attrs[0]["coupon_end_date"]="تاريخ انتهاء الكوبون";
                    $attrs[0]["coupon_value"]="قيمة الكوبون";


                    $attrs[3]["coupon_code"]="text";
                    $attrs[3]["coupon_end_date"]="date";
                    $attrs[3]["coupon_value"]="number";
                    $attrs[3]["coupon_used_times"]="number";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">


                </form>
            </div>
        </div>
    </div>


@endsection



