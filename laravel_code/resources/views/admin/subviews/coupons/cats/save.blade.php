@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة تصنيف جديد";
    $cat_id="";

    if ($cat_data!="") {
        $header_text="تعديل تصنيف ".$cat_data->cat_code;

        $cat_id=$cat_data->cat_id;
    }

    //dump($pro_data);
    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/coupons/cats/save_cat/$cat_id")?>" method="POST" enctype="multipart/form-data">

                    <?php

                    $normal_tags=array('cat_name');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $cat_data,
                            "yes",
                            $required="required"
                    );


                    $attrs[0]["cat_name"]="اسم التصنيف *";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">


                </form>
            </div>
        </div>
    </div>


@endsection



