@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            تصنيف الكوبونات
        </div>
        <div class="panel-body">

            <?php if(check_permission($user_permissions,"admin/coupons","add_action",$current_user)): ?>
            <div class="col-md-4 col-md-offset-4">
                <a href="{{url("/admin/coupons/cats/save_cat")}}" class="btn btn-primary">إضافة تصنيف جديد</a>
            </div>
            <?php endif; ?>

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم التصنيف</td>
                    <td>عدد الكوبونات الكلي</td>
                    <td>عدد الكوبونات المستعملة</td>
                    <td>لينك الكوبونات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم التصنيف</td>
                    <td>عدد الكوبونات الكلي</td>
                    <td>عدد الكوبونات المستعملة</td>
                    <td>لينك الكوبونات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody id="sortable">
                <?php foreach ($all_coupon_cats as $key => $cat): ?>

                <tr id="row<?= $cat->cat_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$cat->cat_name}}</td>
                    <td>{{$cat->total_cat_coupons}}</td>
                    <td>{{$cat->total_used_cat_coupons}}</td>
                    <td>
                        <a class="btn btn-info" href="{{url("/admin/coupons?cat_id=$cat->cat_id")}}">link</a>
                    </td>


                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","edit_action",$current_user)): ?>
                            <a href="<?= url("admin/coupons/cats/save_cat/$cat->cat_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("/admin/coupons/remove_coupons_cat") ?>" data-tablename="App\models\coupons_m"  data-itemid="<?= $cat->cat_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>
            </table>

        </div>
    </div>




@endsection
