@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="{{url("/admin/coupons")}}" method="get">
                <div class="col-md-6">
                    <?php
                        echo generate_select_tags(
                                $field_name="cat_id",
                                $label_name="اختار التصنيف",
                                $text=$all_coupon_cats->pluck("cat_name")->all(),
                                $values=$all_coupon_cats->pluck("cat_id")->all(),
                                $selected_value=[$selected_cat_id],
                                $class="form-control select_2_class",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = "",
                                $grid="12",
                                $show_label_as_option=false
                        );
                    ?>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">اسم الكوبون</label>
                    <input type="text" name="coupon_text" class="form-control" value="{{$selected_coupon_text}}">
                </div>
                <button class="btn btn-primary" type="submit">ابحث</button>
            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            الكوبونات
        </div>
        <div class="panel-body">

            <?php if(check_permission($user_permissions,"admin/coupons","add_action",$current_user)): ?>
            <div class="col-md-4 col-md-offset-4">
                <a href="{{url("/admin/coupons/save_coupon")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الكود</td>
                    <td>تاريخ الانتهاء</td>
                    <td>نوع الكوبون</td>
                    <td>قيمة الكوبون</td>
                    <td>عدد المرات المتاحة للإستخدام</td>
                    <td>عدد المرات المستخدمة</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>الكود</td>
                    <td>تاريخ الانتهاء</td>
                    <td>نوع الكوبون</td>
                    <td>قيمة الكوبون</td>
                    <td>عدد المرات المتاحة للإستخدام</td>
                    <td>عدد المرات المستخدمة</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody id="sortable">
                <?php foreach ($all_coupons as $key => $coupon): ?>

                <tr id="row<?= $coupon->coupon_id ?>" data-itemid="<?= $coupon->coupon_id ?>" data-tablename="App\models\coupons_m">
                    <td><?=$key+1?></td>
                    <td>{{$coupon->coupon_code}}</td>
                    <td>{{$coupon->coupon_end_date}}</td>

                    <td>
                        <?php
                        echo generate_multi_accepters(
                                $accepturl="",
                                $item_obj=$coupon,
                                $item_primary_col="coupon_id",
                                $accept_or_refuse_col="is_rate",
                                $model='App\models\coupons_m',
                                $accepters_data=["0"=>"مبلغ","1"=>"نسبة"]
                        );
                        ?>
                    </td>
                    <td>{{$coupon->coupon_value}}</td>
                    <td>{{$coupon->coupon_used_times}}</td>
                    <td>{{$coupon->coupon_current_used}}</td>


                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","edit_action",$current_user)): ?>
                            <a href="<?= url("admin/coupons/save_coupon/$coupon->coupon_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","delete_action",$current_user)): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("/admin/coupons/remove_coupon") ?>" data-tablename="App\models\coupons_m"  data-itemid="<?= $coupon->coupon_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <div class="col-md-12" style="text-align: center;">
                {{$all_coupons->links()}}
            </div>



        </div>
    </div>




@endsection
