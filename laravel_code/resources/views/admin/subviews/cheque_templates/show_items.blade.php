@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
            جميع العناصر الخاصة بالقالب
            {{$cheque_data->cheque_template_name}}
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم العنصر</td>
            <td>إخفاء العنصر</td>
        </tr>
        </thead>


        <tbody>
        <?php foreach ($cheque_items as $key => $item): ?>
        <tr id="row{{$item->item_id}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-default">
                    <?= $item->item_name ?>
                </span>
            </td>
            <td>
                <?php
                echo
                generate_multi_accepters(
                        $accepturl=url( "/admin/cheques/change_template_item_status"),
                        $item_obj=$item,
                        $item_primary_col="item_id",
                        $accept_or_refuse_col="item_hide",
                        $model='App\models\cheques\cheque_template_items_m',
                        $accepters_data=["0"=>"إظهار","1"=>"إخفاء"]
                );
                ?>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(check_permission($user_permissions,"admin/cheque_templates","save_items_position",$current_user)): ?>
    <div class="col-md-4 col-md-offset-4">
        <a class="btn btn-primary btn-block" href="{{url('/admin/cheques/preview_template_items/'.$cheque_data->cheque_template_id)}}"> تعديل شكل قالب الشيك </a>
    </div>
    <?php endif; ?>
    <br><br>

@endsection