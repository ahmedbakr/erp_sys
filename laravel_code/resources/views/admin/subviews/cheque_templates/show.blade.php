@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
            جميع القوالب الخاصة بالشيكات
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم الفرع</td>
            <td>اسم القالب</td>
            <td>عرض القالب</td>
            <td>طول القالب</td>
            <td>تاريخ الانشاء</td>
            <td>قالب الاساسي</td>
            <td>عناصر القالب</td>
            <td>تعديل القالب</td>
            <td>مسح القالب</td>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($all_cheques as $key => $cheque): ?>
        <tr id="row{{$cheque->cheque_template_id}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-info">
                    <?= $cheque->full_name ?>
                </span>
            </td>
            <td>
                <span class="label label-default">
                    <?= $cheque->cheque_template_name ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $cheque->cheque_width ?>
                </span>
            </td>
            <td>
                <span class="label label-warning">
                    <?= $cheque->cheque_height ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $cheque->cheque_date ?>
                </span>
                <span class="label label-success">
                    <?= \Carbon\Carbon::createFromTimestamp(strtotime($cheque->cheque_date))->diffForHumans() ?>
                </span>
            </td>
            <td>
                <?php
                echo
                generate_multi_accepters(
                        $accepturl=url( "/admin/cheques/change_template_default"),
                        $item_obj=$cheque,
                        $item_primary_col="cheque_template_id",
                        $accept_or_refuse_col="set_default",
                        $model='App\models\cheques\cheque_templates_m',
                        $accepters_data=["0"=>"لا","1"=>"نعم"]
                );
                ?>
            </td>
            <td>
                <a class="btn btn-primary btn-block" href="{{url('/admin/cheques/show_cheque_items/'.$cheque->cheque_template_id)}}"> مشاهدة </a>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/cheque_templates","edit_action",$current_user)): ?>
                <a class="btn btn-primary btn-block" href="{{url('/admin/cheques/save_template/'.$cheque->cheque_template_id)}}"> تعديل </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/cheque_templates","delete_action",$current_user)): ?>
                <a href='#' class="general_remove_item" data-tablename="App\models\cheques\cheque_templates_m" data-deleteurl="<?= url("/admin/cheques/remove_template") ?>" data-itemid="<?= $cheque->cheque_template_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(check_permission($user_permissions,"admin/cheque_templates","add_action",$current_user)): ?>
    <div class="col-md-3 col-md-offset-2">
        <a class="btn btn-primary btn-block" href="{{url('/admin/cheques/save_template')}}"> إضافة قالب جديد </a>
    </div>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/print_cheques","show_action",$current_user)): ?>
    <div class="col-md-3 col-md-offset-2">
        <a class="btn btn-primary btn-block" href="{{url('/prepare_print_cheques')}}"> طباعة شيك جديد </a>
    </div>
    <?php endif; ?>


@endsection