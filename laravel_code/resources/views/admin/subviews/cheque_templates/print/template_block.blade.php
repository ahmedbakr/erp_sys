<style>
    .ui-resizable-helper { border: 2px dotted #00F; }
</style>

<div class="cheque-box cheque-box-container">

    <img src="{{url('public_html/admin/images/cheque.PNG')}}" class="cheque_template_img"
         style="
                 width: {{convert_from_cm_to_px($template_data->cheque_width)}}px;
                 height: {{convert_from_cm_to_px($template_data->cheque_height)}}px;
         "
    >

    <?php if($template_items["cheque_date"][0]->item_hide==0): ?>
        <div class="cheque_item"
             style = "
                     position: relative;
                     width: {{$template_items["cheque_date"][0]->item_width}};
                     height: {{$template_items["cheque_date"][0]->item_height}};
                     left:{{$template_items["cheque_date"][0]->item_position_left}};
                     top:{{$template_items["cheque_date"][0]->item_position_top}};
                     "
             data-item_id="{{$template_items["cheque_date"][0]->item_id}}">
            <b>{{$template_items["cheque_date"][0]->item_name}}</b><br>
        </div>
    <?php endif; ?>

    <?php if($template_items["receiver_name"][0]->item_hide==0): ?>
        <div class="cheque_item"
             style = "
                     position: relative;
                     width: {{$template_items["receiver_name"][0]->item_width}};
                     height: {{$template_items["receiver_name"][0]->item_height}};
                     left:{{$template_items["receiver_name"][0]->item_position_left}};
                     top:{{$template_items["receiver_name"][0]->item_position_top}};
                     "
             data-item_id="{{$template_items["receiver_name"][0]->item_id}}">
            <b>{{$template_items["receiver_name"][0]->item_name}}</b><br>
        </div>
    <?php endif; ?>


    <?php if($template_items["money_in_numbers"][0]->item_hide==0): ?>
        <div class="cheque_item"
             style = "
                     position: relative;
                     width: {{$template_items["money_in_numbers"][0]->item_width}};
                     height: {{$template_items["money_in_numbers"][0]->item_height}};
                     left:{{$template_items["money_in_numbers"][0]->item_position_left}};
                     top:{{$template_items["money_in_numbers"][0]->item_position_top}};
                     "
             data-item_id="{{$template_items["money_in_numbers"][0]->item_id}}">
            <b>{{$template_items["money_in_numbers"][0]->item_name}}</b><br>
        </div>
    <?php endif; ?>


    <?php if($template_items["money_in_text"][0]->item_hide==0): ?>
        <div class="cheque_item"
             style = "
                     position: relative;
                     width: {{$template_items["money_in_text"][0]->item_width}};
                     height: {{$template_items["money_in_text"][0]->item_height}};
                     left:{{$template_items["money_in_text"][0]->item_position_left}};
                     top:{{$template_items["money_in_text"][0]->item_position_top}};
                     "
             data-item_id="{{$template_items["money_in_text"][0]->item_id}}">
            <b>{{$template_items["money_in_text"][0]->item_name}}</b><br>
        </div>
    <?php endif; ?>


</div>