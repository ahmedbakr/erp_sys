@extends('admin.main_layout')

@section('subview')

    <link rel="stylesheet" href="{{url('public_html/admin/cheques/style.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="{{url('public_html/jscode/admin/cheque.js')}}"></script>


    <style>
        .cheque-box{
            width: {{convert_from_cm_to_px($template_data->cheque_width)}}px;
            height: {{convert_from_cm_to_px($template_data->cheque_height)}}px;
        }
    </style>


    <div class="panel panel-info">
        <div class="panel-heading">
          بيانات الشيك
        </div>
        <div class="panel-body">

            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>

            <?php if(is_array($template_items) && count($template_items)): ?>

                <div class="col-md-12">

                    <?php if(isset($template_items["cheque_date"]) &&
                            isset($template_items["cheque_date"][0]) &&
                            $template_items["cheque_date"][0]->item_hide == 0
                    ): ?>

                        <div class="col-md-6 form-group">
                            <label for="">تاريخ الشيك</label>
                            <input type="date" class="form-control" id="cheque_date" value="{{$cheque_date}}">
                        </div>
                    <?php endif; ?>

                    <?php if(isset($template_items["receiver_name"]) &&
                            isset($template_items["receiver_name"][0]) &&
                            $template_items["receiver_name"][0]->item_hide == 0
                    ): ?>
                        <div class="col-md-6 form-group">
                            <label for="">اسم المستفيد</label>
                            <input type="text" class="form-control" id="receiver_name" value="{{$receiver_name}}">
                        </div>
                    <?php endif; ?>

                    <?php if(isset($template_items["money_in_numbers"]) &&
                            isset($template_items["money_in_numbers"][0]) &&
                            $template_items["money_in_numbers"][0]->item_hide == 0
                    ): ?>
                        <div class="col-md-6 form-group">
                            <label for="">المبلغ بالارقام</label>
                            <input type="number" min="0" step="0.1" class="form-control" id="money_in_numbers" value="{{$money_in_numbers}}">
                        </div>
                    <?php endif; ?>

                    <?php if(isset($template_items["money_in_text"]) &&
                            isset($template_items["money_in_text"][0]) &&
                            $template_items["money_in_text"][0]->item_hide == 0
                    ): ?>
                        <div class="col-md-6 form-group">
                            <label for="">المبلغ بالاحرف</label>
                            <input type="text" class="form-control" id="money_in_text" value="{{$money_in_text}}">
                        </div>
                    <?php endif; ?>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button class="btn btn-info preview_cheque_before_print">عرض</button>
                    </div>
                </div>

            <?php endif; ?>

        </div>
    </div>


        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        قالب
                        {{$template_data->cheque_template_name}}
                    </h3>
                </div>
            </div>

            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }


            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <div class="panel panel-info">
                <div class="panel-heading" role="button" data-toggle="collapse"
                     href="#template_body" aria-expanded="true" aria-controls="template_body">شكل قالب الشيك</div>
                <div class="panel-body">
                    <div class="row custom_template_html">
                        @include('admin.subviews.cheque_templates.print.template_block')
                    </div>
                </div>

                <?php if(1!=1): ?>
                    <button type="button" class="col-md-3 col-md-offset-4 btn btn-primary btn-lg save_cheque_template_html"
                            data-cheque_template_id = "{{$template_data->cheque_template_id}}">حفظ</button>
                    <div class="show_cheque_template_errors"></div>
                <?php endif; ?>

            </div>


        </div>


@endsection