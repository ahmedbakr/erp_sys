@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading">
            الاشعارات
        </div>
        <div class="panel-body">
            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الاشعار</td>
                    <td>التاريخ</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($all_notifications as $key => $not): ?>
                <tr id="row<?= $not->not_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $not->not_title ?></td>
                    <td><?= $not->created_at ?></td>

                    <td><a href='#' class="general_remove_item" data-tablename="App\models\notification_m" data-deleteurl="<?= url("/general_remove_item") ?>" data-itemid="<?= $not->not_id ?>"><span class="label label-danger">مسح <i class="fa fa-remove"></i></span></a></td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <div style="text-align: center;">
                {{$notifications_pagination->links()}}
            </div>

        </div>
    </div>


@endsection
