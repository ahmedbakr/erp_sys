@extends('admin.main_layout')

@section('subview')


    <?php

    $header_text = "<i class='fa fa-plus'></i> إضافة ماده خام جديدة";
    $mat_id = "";
    if (is_object($material_data))
    {
        $mat_id = $material_data->mat_id;
        $header_text = "<i class='fa fa-edit'></i> تعديل الماده الخام رقم.$mat_id";
    }

    ?>


    <div class="container">

        <div class="col-md-8 col-md-offset-2">

            <form id="save_form" action="<?=url("/admin/materials/save/$mat_id")?>" method="POST" enctype="multipart/form-data">
                <h1>{!! $header_text !!}</h1>
                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }

                $normal_tags=array("mat_name","mat_type");
                $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $material_data,
                        "yes",
                        $required = "required"
                );

                $attrs[0]["mat_name"]="اسم الماده الخام";
                $attrs[0]["mat_type"]="الوحدة (مثال كيلو او لتر ..)";

                $attrs[3]["sup_mat_name"]="text";
                $attrs[3]["sup_mat_type"]="text";

                echo
                generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5])
                );

                ?>

                {{csrf_field()}}
                <input id="submit" type="submit" value="حفظ" class="col-md-4 btn btn-primary btn-lg">
                <a href="{{url('/admin/materials/save')}}" class="col-md-4 col-md-offset-2 btn btn-primary btn-lg"><i class="fa fa-plus"></i> أضف جديد</a>
            </form>

        </div>

    </div>


@endsection