@extends('admin.main_layout')

@section('subview')


    <div class="container">

        <div class="col-md-12">

            <?php if(is_array($materials) && count($materials)): ?>

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>اسم الماده الخام</td>
                        <td>الوحدة للماده الخام</td>
                        <td>التاريخ</td>
                        <?php if(check_permission($user_permissions,"factory/materials","edit_action")): ?>
                            <td>تعديل</td>
                        <?php endif; ?>

                    </tr>
                </thead>

                <tbody>

                    <?php foreach($materials as $key => $material): ?>

                        <tr>
                            <td>{{$key+1}}</td>
                            <td>
                                <span class="label label-info">
                                    {{$material->mat_name}}
                                </span>
                            </td>
                            <td>
                                <span class="label label-success">
                                    {{$material->mat_type}}
                                </span>
                            </td>
                            <td>
                                <span class="label label-default">
                                    {{$material->created_at}}
                                </span>
                                <br>
                                <span class="label label-success">
                                    {{Carbon\Carbon::createFromTimestamp(strtotime($material->created_at))->diffForHumans()}}
                                </span>
                            </td>

                            <?php if(check_permission($user_permissions,"factory/materials","edit_action")): ?>
                                <td>
                                    <a class="btn btn-primary btn-block" href="{{url('/admin/materials/save/'.$material->mat_id)}}"> تعديل </a>
                                </td>
                            <?php endif; ?>

                        </tr>

                    <?php endforeach; ?>

                </tbody>

            </table>

            <?php endif; ?>

            <?php if(check_permission($user_permissions,"factory/materials","add_action")): ?>
                <div class="col-md-3 col-md-offset-3">
                    <a class="btn btn-primary btn-block" href="{{url('/admin/materials/save')}}"> إضافة ماده خام جديدة </a>
                </div>
            <?php endif; ?>

        </div>

    </div>


@endsection