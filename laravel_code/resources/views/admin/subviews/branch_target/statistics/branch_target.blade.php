@extends('admin.main_layout')

@section("subview")

    <style>
        .progress-bar-success{
            max-width: 100%;
        }
    </style>


    <div class="">

        <div class="panel panel-primary">
            <div class="panel-heading">
                البحث المتقدم
            </div>
            <div class="panel-body">
                <form action="{{url("/admin/branch_target/branch_target/get_branch_target")}}" method="get">
                    <div class="col-md-12">

                        <div class="col-md-3">
                            <?php
                            echo generate_select_tags(
                                $field_name="branch_id",
                                $label_name="اختار الفرع",
                                $text=array_merge(["الكل"],convert_inside_obj_to_arr($all_branches,"full_name")),
                                $values=array_merge([""],convert_inside_obj_to_arr($all_branches,"user_id")),
                                $selected_value=[$selected_branch_id],
                                $class="form-control",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = ""
                            );
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?php
                                echo generate_select_tags(
                                    $field_name="target_type",
                                    $label_name="علي اساس اليوم او الشهر او السنة",
                                    $text=["ايام شهر","شهر","سنة"],
                                    $values=["day","month","year"],
                                    $selected_value=[$target_type],
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = ""
                                );
                            ?>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="">الشهر</label>
                            <input type="number" name="month" class="form-control target_month" value="{{$target_month}}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="">السنة</label>
                            <input type="number" name="year" class="form-control target_year" value="{{$target_year}}">
                        </div>

                        <div class="col-md-3 form-group">
                            <label for="">من يوم</label>
                            <input type="number" name="from_day" class="form-control " value="{{isset($all_post_data->from_day)?$all_post_data->from_day:""}}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="">الي يوم</label>
                            <input type="number" name="to_day" class="form-control " value="{{isset($all_post_data->to_day)?$all_post_data->to_day:""}}">
                        </div>

                    </div>
                    <div class="col-md-12" style="text-align: center;">
                        <button type="submit" class="btn btn-primary filter_branch_target">ابحث</button>
                    </div>
                </form>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">

                تارجت الفرع

            </div>
            <div class="panel-body">

                <?php if($selected_branch_id==""): ?>
                    <?php
                        $all_branches_grouped_by_id=collect($all_branches)->groupBy("user_id")->all();
                    ?>


                    <?php if($target_type==="year"): ?>
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            كل سنين الافرع
                        </div>
                        <div class="panel-body">

                            <?php if(isset_and_array($all_bills)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                <thead>

                                <tr>
                                    <td>الفرع</td>
                                    <td>السنة</td>
                                    <td>قيمة التارجت</td>
                                    <td>المحقق</td>
                                    <td>نسبة تحقيق التارجت</td>
                                    <?php if(false): ?>
                                    <td>نسبة العجز</td>
                                    <?php endif; ?>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $branch_target_all_branches=$branch_target_all_years;
                                ?>
                                <?php foreach($all_bills as $branch_id=>$branch_orders): ?>

                                    <?php
                                        if(!isset($all_branches_grouped_by_id[$branch_id])||!isset($branch_target_all_branches[$branch_id])){
                                            continue;
                                        }

                                        $branch_name=$all_branches_grouped_by_id[$branch_id][0]->full_name;
                                        $branch_orders=collect($branch_orders)->groupBy("created_year")->all();

                                        $branch_target_all_years=$branch_target_all_branches[$branch_id];
                                        $branch_target_all_years=collect($branch_target_all_years)->groupBy("year")->all();
                                    ?>

                                    <?php foreach($branch_orders as $year_key=>$year_orders): ?>

                                        <?php

                                            if(!empty($target_year)&&$target_year!=$year_key){
                                                continue;
                                            }

                                            $target_val=0;

                                            if(isset($branch_target_all_years[$year_key])){
                                                $target_val=$branch_target_all_years[$year_key]->all();
                                                $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                                $target_val=array_sum($target_val);
                                            }

                                            if(count($year_orders->all())==0){
                                                continue;
                                            }


                                            $client_bill_total_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_amount");
                                            $client_bill_total_amount=array_sum($client_bill_total_amount);

                                            $client_bill_total_return_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_return_amount");
                                            $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                            $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_cash");
                                            $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                            $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_atm");
                                            $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);



                                            $progress_val=0;
                                            if($target_val>0){
                                                $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                $progress_val=round($progress_val,2);
                                            }
                                            else{
                                                $progress_val="100";
                                            }
                                        ?>

                                        <tr>
                                            <td>
                                                <label class="label label-info">{{$branch_name}}</label>
                                            </td>

                                            <td>
                                                <label class="label label-info">{{$year_key}}</label>
                                            </td>

                                            <td>
                                                <label class="label label-info">{{$target_val}}</label>
                                            </td>
                                            <td>

                                                {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                            </td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                        {{round($progress_val,2)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php if(false): ?>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                        {{round((100-$progress_val),2)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach;?>

                                <?php endforeach;?>
                                </tbody>

                            </table>

                            <?php else: ?>
                            <div class="alert alert-warning">
                                ليس لديك اي فواتير في الفرع
                            </div>
                            <?php endif; ?>


                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if($target_type==="month"): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                كل اشهر الافرع
                            </div>
                            <div class="panel-body">

                                <?php if(isset_and_array($all_bills)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>

                                    <tr>
                                        <td>الفرع</td>
                                        <td>الشهر</td>
                                        <td>قيمة التارجت</td>
                                        <td>المحقق</td>
                                        <td>نسبة تحقيق التارجت</td>
                                        <?php if(false): ?>
                                        <td>نسبة العجز</td>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $branch_target_all_branches=$branch_target_all_years;
                                        ?>
                                        <?php foreach($all_bills as $branch_id=>$branch_orders): ?>

                                            <?php
                                                if(!isset($all_branches_grouped_by_id[$branch_id])||!isset($branch_target_all_branches[$branch_id])){
                                                    continue;
                                                }

                                                $branch_name=$all_branches_grouped_by_id[$branch_id][0]->full_name;
                                                $branch_orders=collect($branch_orders)->groupBy("created_year")->all();

                                                $branch_target_all_years=$branch_target_all_branches[$branch_id];
                                                $branch_target_all_years=collect($branch_target_all_years)->groupBy("year")->all();
                                            ?>

                                            <?php foreach($branch_orders as $year_key=>$year_orders): ?>
                                                <?php
                                                    if($target_year!=$year_key){
                                                        continue;
                                                    }

                                                    $month_bills=collect($year_orders)->groupBy("created_month")->all();
                                                    $branch_target_this_year=collect($branch_target_all_years[$year_key])->groupBy("month")->all();

                                                ?>

                                                <?php for($i=1;$i<=12;$i++): ?>

                                                    <?php
                                                        if(!empty($target_month)&&$target_month!=$i){
                                                            continue;
                                                        }

                                                        $month_key=$i;

                                                        $month_orders=[];
                                                        if(isset($month_bills[$i])){
                                                            $month_orders=$month_bills[$i]->all();
                                                        }


                                                        $target_val=0;

                                                        if(isset($branch_target_this_year[$month_key])){
                                                            $target_val=$branch_target_this_year[$month_key]->all();
                                                            $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                                            $target_val=array_sum($target_val);
                                                        }


                                                        $client_bill_total_amount=0;
                                                        $client_bill_total_return_amount=0;
                                                        $client_total_paid_amount_in_cash=0;
                                                        $client_total_paid_amount_in_atm=0;

                                                        if(count($month_orders)){
                                                            $client_bill_total_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_amount");
                                                            $client_bill_total_amount=array_sum($client_bill_total_amount);

                                                            $client_bill_total_return_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_return_amount");
                                                            $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                                            $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_cash");
                                                            $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                                            $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_atm");
                                                            $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                                        }

                                                        $progress_val=0;
                                                        if($target_val>0){
                                                            $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                            $progress_val=round($progress_val,2);

                                                        }
                                                        else{
                                                            $progress_val="0";
                                                        }
                                                    ?>

                                                    <tr>
                                                        <td>{{$branch_name}}</td>
                                                        <td>
                                                            <label class="label label-info">{{$month_key}}-{{$year_key}}</label>
                                                        </td>
                                                        <td>
                                                            <label class="label label-info">{{$target_val}}</label>
                                                        </td>
                                                        <td>
                                                            {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                            {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                            {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                            {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                            <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                                        </td>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                                    {{$progress_val}}%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <?php if(false): ?>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                                    {{(100-$progress_val)}}%
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <?php endif; ?>

                                                    </tr>


                                                <?php endfor; ?>

                                            <?php endforeach;?>

                                        <?php endforeach;?>
                                    </tbody>

                                </table>

                                <?php else: ?>
                                <div class="alert alert-warning">
                                    ليس لديك اي فواتير في الفرع
                                </div>
                                <?php endif; ?>


                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if($target_type==="day"): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                كل ايام الافرع
                            </div>
                            <div class="panel-body">

                                <?php if(isset_and_array($all_bills)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>

                                    <tr>
                                        <td>الفرع</td>
                                        <td>اليوم</td>
                                        <td>قيمة التارجت</td>
                                        <td>المحقق</td>
                                        <td>نسبة تحقيق التارجت</td>
                                        <?php if(false): ?>
                                        <td>نسبة العجز</td>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $branch_target_all_branches=$branch_target_all_years;
                                        ?>
                                        <?php foreach($all_bills as $branch_id=>$branch_orders): ?>

                                            <?php
                                                if(!isset($all_branches_grouped_by_id[$branch_id])||!isset($branch_target_all_branches[$branch_id])){
                                                    continue;
                                                }

                                                $branch_name=$all_branches_grouped_by_id[$branch_id][0]->full_name;
                                                $branch_orders=collect($branch_orders)->groupBy("created_year")->all();

                                                $branch_target_all_years=$branch_target_all_branches[$branch_id];
                                                $branch_target_all_years=collect($branch_target_all_years)->groupBy("year")->all();
                                            ?>

                                            <?php foreach($branch_orders as $year_key=>$year_orders): ?>
                                                <?php
                                                    if($target_year!=$year_key){
                                                        continue;
                                                    }

                                                    $month_bills=collect($year_orders)->groupBy("created_month")->all();
                                                    $branch_target_this_year=collect($branch_target_all_years[$year_key])->groupBy("month")->all();

                                                ?>

                                                <?php for($i=1;$i<=12;$i++): ?>

                                                    <?php
                                                        if(!empty($target_month)&&$target_month!=$i){
                                                            continue;
                                                        }

                                                        $month_key=$i;

                                                        $month_orders=[];
                                                        if(isset($month_bills[$i])){
                                                            $month_orders=$month_bills[$i]->all();
                                                        }


                                                        $target_val=0;

                                                        if(isset($branch_target_this_year[$month_key])){
                                                            $target_val=$branch_target_this_year[$month_key]->all();
                                                            $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                                            $target_val=array_sum($target_val);
                                                        }

                                                        $branch_target_this_month=$target_val;

                                                        $month_days_number=cal_days_in_month(CAL_GREGORIAN, $month_key, $year_key);
                                                    ?>


                                                    <?php for($i=1;$i<=$month_days_number;$i++): ?>

                                                        <?php
                                                            $day_key=$i;

                                                            if(
                                                                isset($all_post_data->from_day)&&!empty($all_post_data->from_day)&&
                                                                isset($all_post_data->to_day)&&!empty($all_post_data->to_day)&&
                                                                (!($day_key>=$all_post_data->from_day&&$day_key<=$all_post_data->to_day))
                                                            ){
                                                                continue;
                                                            }

                                                            $date_from=\Carbon\Carbon::create($target_year,$target_month,$day_key,5);
                                                            $date_to=\Carbon\Carbon::create($target_year,$target_month,$day_key+1,4,59);
                                                            $date_from=$date_from->toDateTimeString();
                                                            $date_to=$date_to->toDateTimeString();



                                                            $day_orders = collect($month_orders)->flatten()->filter(function ($bill, $key)use($date_from,$date_to) {
                                                                if($bill->client_bill_date>=$date_from&&$bill->client_bill_date<=$date_to){
                                                                    return $bill;
                                                                }
                                                            });


                                                            $target_val=($branch_target_this_month)/$month_days_number;
                                                            $target_val=round($target_val,2);

                                                            $client_bill_total_amount=0;
                                                            $client_bill_total_return_amount=0;
                                                            $client_total_paid_amount_in_cash=0;
                                                            $client_total_paid_amount_in_atm=0;

                                                            if(count($day_orders)){
                                                                $client_bill_total_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_amount");
                                                                $client_bill_total_amount=array_sum($client_bill_total_amount);

                                                                $client_bill_total_return_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_return_amount");
                                                                $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                                                $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_cash");
                                                                $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                                                $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_atm");
                                                                $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                                            }

                                                            $progress_val=0;
                                                            if($target_val>0){
                                                                $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                                $progress_val=round($progress_val,2);
                                                            }
                                                            else{
                                                                $progress_val="0";
                                                            }
                                                        ?>

                                                        <tr>
                                                            <td>{{$branch_name}}</td>
                                                            <td>
                                                                <label class="label label-info">{{$day_key}}-{{$target_month}}-{{$target_year}}</label>
                                                            </td>
                                                            <td>
                                                                <label class="label label-info">{{$target_val}}</label>
                                                            </td>
                                                            <td>
                                                                {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                                {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                                {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                                {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                                <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                                            </td>
                                                            <td>
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                                        {{$progress_val}}%
                                                                    </div>
                                                                </div>
                                                            </td>

                                                            <?php if(false): ?>
                                                            <td>
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                                        {{(100-$progress_val)}}%
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <?php endif; ?>


                                                        </tr>


                                                    <?php endfor; ?>


                                                <?php endfor; ?>

                                            <?php endforeach;?>

                                        <?php endforeach;?>
                                    </tbody>

                                </table>

                                <?php else: ?>
                                <div class="alert alert-warning">
                                    ليس لديك اي فواتير في الفرع
                                </div>
                                <?php endif; ?>


                            </div>
                        </div>
                    <?php endif; ?>



                    <?php else: ?>

                    <?php if($target_type=="year"): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                كل السنين
                            </div>
                            <div class="panel-body">
                                <?php if(isset_and_array($all_bills)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>

                                    <tr>
                                        <td>السنة</td>
                                        <td>قيمة التارجت</td>
                                        <td>المحقق</td>
                                        <td>نسبة تحقيق التارجت</td>
                                        <?php if(false): ?>
                                        <td>نسبة العجز</td>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php foreach($all_bills as $year_key=>$year_orders): ?>
                                        <?php

                                            $target_val=0;

                                            if(isset($branch_target_all_years[$year_key])){
                                                $target_val=$branch_target_all_years[$year_key]->all();
                                                $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                                $target_val=array_sum($target_val);
                                            }

                                            if(count($year_orders->all())==0){
                                                continue;
                                            }


                                            $client_bill_total_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_amount");
                                            $client_bill_total_amount=array_sum($client_bill_total_amount);

                                            $client_bill_total_return_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_return_amount");
                                            $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                            $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_cash");
                                            $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                            $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_atm");
                                            $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);



                                            $progress_val=0;
                                            if($target_val>0){
                                                $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                $progress_val=round($progress_val,2);
                                            }
                                            else{
                                                $progress_val="100";
                                            }
                                        ?>

                                        <tr>
                                            <td>
                                                <label class="label label-info">{{$year_key}}</label>
                                            </td>
                                            <td>
                                                <label class="label label-info">{{$target_val}}</label>
                                            </td>
                                            <td>
                                                {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                            </td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                        {{round($progress_val,2)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php if(false): ?>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                        {{round((100-$progress_val),2)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php endif; ?>

                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>

                                </table>

                                <?php else: ?>
                                    <div class="alert alert-warning">
                                        ليس لديك اي فواتير في الفرع
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php elseif($target_type=="month"): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{$target_year}}
                            </div>
                            <div class="panel-body">

                                <?php if(isset_and_array($all_bills)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>

                                    <tr>
                                        <td>#</td>
                                        <td>الشهر</td>
                                        <td>قيمة التارجت</td>
                                        <td>المحقق</td>
                                        <td>نسبة تحقيق التارجت</td>
                                        <?php if(false): ?>
                                        <td>نسبة العجز</td>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php for($i=1;$i<=12;$i++): ?>

                                        <?php
                                            $month_key=$i;

                                            $month_orders=[];
                                            if(isset($all_bills[$i])){
                                                $month_orders=$all_bills[$i]->all();
                                            }


                                            $target_val=0;

                                            if(isset($branch_target_this_year[$month_key])){
                                                $target_val=$branch_target_this_year[$month_key]->all();
                                                $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                                $target_val=array_sum($target_val);
                                            }


                                            $client_bill_total_amount=0;
                                            $client_bill_total_return_amount=0;
                                            $client_total_paid_amount_in_cash=0;
                                            $client_total_paid_amount_in_atm=0;

                                            if(count($month_orders)){
                                                $client_bill_total_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_amount");
                                                $client_bill_total_amount=array_sum($client_bill_total_amount);

                                                $client_bill_total_return_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_return_amount");
                                                $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                                $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_cash");
                                                $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                                $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_atm");
                                                $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                            }

                                            $progress_val=0;
                                            if($target_val>0){
                                                $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                $progress_val=round($progress_val,2);

                                            }
                                            else{
                                                $progress_val="0";
                                            }
                                        ?>

                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>
                                                <label class="label label-info">{{$month_key}}-{{$target_year}}</label>
                                            </td>
                                            <td>
                                                <label class="label label-info">{{$target_val}}</label>
                                            </td>
                                            <td>
                                                {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                            </td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                        {{$progress_val}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php if(false): ?>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                        {{(100-$progress_val)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php endif; ?>

                                        </tr>


                                    <?php endfor; ?>
                                    </tbody>

                                </table>



                                <?php else: ?>
                                <div class="alert alert-warning">
                                    ليس لديك اي فواتير في الفرع
                                </div>


                                <?php endif; ?>

                            </div>
                        </div>
                    <?php elseif($target_type=="day"): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{$target_year}} - {{$target_month}}
                            </div>
                            <div class="panel-body">

                                <?php if(isset_and_array($all_bills)): ?>
                                <?php



                                ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>

                                    <tr>
                                        <td>#</td>
                                        <td>اليوم</td>
                                        <td>قيمة التارجت</td>
                                        <td>المحقق</td>
                                        <td>نسبة تحقيق التارجت</td>
                                        <?php if(false): ?>
                                        <td>نسبة العجز</td>
                                        <?php endif; ?>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php for($i=1;$i<=$month_days_number;$i++): ?>

                                        <?php
                                            $day_key=$i;

                                            if(
                                                isset($all_post_data->from_day)&&!empty($all_post_data->from_day)&&
                                                isset($all_post_data->to_day)&&!empty($all_post_data->to_day)&&
                                                (!($day_key>=$all_post_data->from_day&&$day_key<=$all_post_data->to_day))
                                            ){
                                                continue;
                                            }

                                            $date_from=\Carbon\Carbon::create($target_year,$target_month,$day_key,5);
                                            $date_to=\Carbon\Carbon::create($target_year,$target_month,$day_key+1,4,59);
                                            $date_from=$date_from->toDateTimeString();
                                            $date_to=$date_to->toDateTimeString();



                                            $day_orders = collect($all_bills)->flatten()->filter(function ($bill, $key)use($date_from,$date_to) {
                                                if($bill->client_bill_date>=$date_from&&$bill->client_bill_date<=$date_to){
                                                    return $bill;
                                                }
                                            });


    //                                        $day_orders=[];
    //                                        if(isset($all_bills[$i])){
    //                                            $day_orders=$all_bills[$i]->all();
    //                                        }


                                            $target_val=($branch_target_this_month->target_amount)/$month_days_number;
                                            $target_val=round($target_val,2);

                                            $client_bill_total_amount=0;
                                            $client_bill_total_return_amount=0;
                                            $client_total_paid_amount_in_cash=0;
                                            $client_total_paid_amount_in_atm=0;

                                            if(count($day_orders)){
                                                $client_bill_total_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_amount");
                                                $client_bill_total_amount=array_sum($client_bill_total_amount);

                                                $client_bill_total_return_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_return_amount");
                                                $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                                $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_cash");
                                                $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                                $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_atm");
                                                $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                            }

                                            $progress_val=0;
                                            if($target_val>0){
                                                $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                                $progress_val=round($progress_val,2);
                                            }
                                            else{
                                                $progress_val="0";
                                            }
                                        ?>

                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>
                                                <label class="label label-info">{{$day_key}}-{{$target_month}}-{{$target_year}}</label>
                                            </td>
                                            <td>
                                                <label class="label label-info">{{$target_val}}</label>
                                            </td>
                                            <td>
                                                {{--<label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>--}}
                                                {{--<label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>--}}
                                                {{--<label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>--}}
                                                {{--<label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>--}}

                                                <label class="label label-primary"> {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                            </td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                        {{$progress_val}}%
                                                    </div>
                                                </div>
                                            </td>

                                            <?php if(false): ?>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                        {{(100-$progress_val)}}%
                                                    </div>
                                                </div>
                                            </td>
                                            <?php endif; ?>


                                        </tr>


                                    <?php endfor; ?>

                                    </tbody>

                                </table>



                                <?php else: ?>
                                <div class="alert alert-warning">
                                    ليس لديك اي فواتير في الفرع
                                </div>


                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>






            </div>
        </div>




    </div>


@endsection