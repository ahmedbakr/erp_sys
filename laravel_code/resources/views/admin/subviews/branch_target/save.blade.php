@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة تارجت جديد";
    $branch_target_id="";
    $selected_year="";
    $selected_month="";

    if ($branch_target_data!="") {
        $header_text="تعديل تارجت للفرع ".$branch_target_data->full_name;

        $branch_target_id=$branch_target_data->branch_target_id;
        $selected_year=$branch_target_data->year;
        $selected_month=$branch_target_data->month;
    }

    //dump($pro_data);
    ?>

    <!--new_editor-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
    <!--END new_editor-->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">

            <?php if(is_array($all_branches) && count($all_branches)): ?>


                <form id="save_form" action="<?=url("admin/branch_target/save_target/$branch_target_id")?>" method="POST" enctype="multipart/form-data">


                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <?php
                            echo
                            generate_select_tags(
                                $field_name="branch_id",
                                $label_name="إختار الفرع",
                                $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                                $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                $selected_value="",
                                $class="form-control select_2_class",
                                $multiple="",
                                $required = "",
                                $disabled = "",
                                $data = $branch_target_data
                            );

                            ?>
                        </div>
                    </div>

                    <?php


                    echo generate_select_years(
                            $already_selected_value = $selected_year ,
                            $earliest_year = "2000" ,
                            $class = "form-control select_2_class",
                            $name = "year",
                            $label = "إختار السنة"
                        );

                    echo generate_select_month_ar(
                            $field_name = "month",
                            $label = "إختار الشهر",
                            $selected_value = $selected_month,
                            $class = "form-control select_2_class",
                            $required="required");

                    $normal_tags=array('target_amount');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $branch_target_data,
                            "yes",
                            $required="required"
                    );


                    $attrs[0]["target_amount"]="المبلغ *";


                    $attrs[3]["target_amount"]="number";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

                </form>


            <?php else: ?>
            {{get_warning(url("admin/users/save"),"لا توجد فروع أضف فرع جديد من هنا")}}

            <?php endif; ?>


        </div>
    </div>


@endsection



