@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            التارجت لكل فرع
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>الشهر</td>
                    <td>السنه</td>
                    <td>المبلغ</td>
                    <?php if(check_permission($user_permissions,"admin/branch_target","show_branch_target",$current_user)): ?>
                    <td>متابعه التارجت</td>
                    <?php endif; ?>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>الشهر</td>
                    <td>السنه</td>
                    <td>المبلغ</td>
                    <?php if(check_permission($user_permissions,"admin/branch_target","show_branch_target",$current_user)): ?>
                    <td>متابعه التارجت</td>
                    <?php endif; ?>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($branch_target as $key => $branch): ?>

                <tr id="row<?= $branch->branch_target_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$branch->full_name}}</td>
                    <td>{{$branch->month}}</td>
                    <td>{{$branch->year}}</td>
                    <td>{{$branch->target_amount}}</td>

                    <?php if(check_permission($user_permissions,"admin/branch_target","show_branch_target",$current_user)): ?>
                    <td><a href="{{url("admin/branch_target/branch_target/get_branch_target?branch_id=$branch->branch_id&target_type=year&month=0&year=0")}}">اظهر تارجت الفرع</a></td>
                    <?php endif; ?>

                    <td>
                        <?php if(check_permission($user_permissions,"admin/branch_target","edit_action",$current_user)): ?>
                        <a href="<?= url("admin/branch_target/save_target/$branch->branch_target_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/branch_target","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/branch_target/remove_branch_target") ?>" data-tablename="App\models\branch_target_m"  data-itemid="<?= $branch->branch_target_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/branch_target","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/branch_target/save_target")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
