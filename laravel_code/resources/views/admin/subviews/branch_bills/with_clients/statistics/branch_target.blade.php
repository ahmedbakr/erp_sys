@extends('admin.main_layout')

@section("subview")


    <div class="">

        <div class="panel panel-primary">
            <div class="panel-heading">
                البحث المتقدم
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php
                            echo generate_select_tags(
                                $field_name="target_type",
                                $label_name="علي اساس اليوم او الشهر او السنة",
                                $text=["ايام شهر","شهر","سنة"],
                                $values=["day","month","year"],
                                $selected_value=[$target_type],
                                $class="form-control",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = ""
                            );
                        ?>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="">الشهر</label>
                        <input type="number" class="form-control target_month" value="{{$target_month}}">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="">السنة</label>
                        <input type="number" class="form-control target_year" value="{{$target_year}}">
                    </div>
                </div>
                <div class="col-md-12" style="text-align: center;">
                    <button class="btn btn-primary filter_branch_target">ابحث</button>
                </div>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">

                تارجت الفرع

            </div>
            <div class="panel-body">

                <?php if($target_type=="year"): ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            كل السنين
                        </div>
                        <div class="panel-body">
                            <?php if(isset_and_array($all_bills)): ?>

                            <table class="table table-striped table-bordered table_without_paging">
                                <thead>

                                <tr>
                                    <td>السنة</td>
                                    <td>قيمة التارجت</td>
                                    <td>قيم الفواتير</td>
                                    <td>نسبة تحقيق التارجت</td>
                                    <td>نسبة العجز</td>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach($all_bills as $year_key=>$year_orders): ?>
                                    <?php

                                        $target_val=0;

                                        if(isset($branch_target_all_years[$year_key])){
                                            $target_val=$branch_target_all_years[$year_key]->all();
                                            $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                            $target_val=array_sum($target_val);
                                        }

                                        if(count($year_orders->all())==0){
                                            continue;
                                        }


                                        $client_bill_total_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_amount");
                                        $client_bill_total_amount=array_sum($client_bill_total_amount);

                                        $client_bill_total_return_amount=convert_inside_obj_to_arr($year_orders->all(),"client_bill_total_return_amount");
                                        $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                        $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_cash");
                                        $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                        $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($year_orders->all(),"client_total_paid_amount_in_atm");
                                        $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);



                                        $progress_val=0;
                                        if($target_val>0){
                                            $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                        }
                                        else{
                                            $progress_val="100";
                                        }
                                    ?>

                                    <tr>
                                        <td>
                                            <label class="label label-info">{{$year_key}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$target_val}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>
                                            <label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>
                                            <label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>
                                            <label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>

                                            <label class="label label-primary">الاجمالي {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                    {{$progress_val}}%
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                    {{(100-$progress_val)}}%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>

                            </table>

                            <?php else: ?>
                                <div class="alert alert-warning">
                                    ليس لديك اي فواتير في الفرع
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>

                <?php elseif($target_type=="month"): ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$target_year}}
                        </div>
                        <div class="panel-body">

                            <?php if(isset_and_array($all_bills)): ?>

                            <table class="table table-striped table-bordered table_without_paging">
                                <thead>

                                <tr>
                                    <td>الشهر</td>
                                    <td>قيمة التارجت</td>
                                    <td>قيم الفواتير</td>
                                    <td>نسبة تحقيق التارجت</td>
                                    <td>نسبة العجز</td>
                                </tr>
                                </thead>

                                <tbody>
                                <?php for($i=1;$i<=12;$i++): ?>

                                    <?php
                                        $month_key=$i;

                                        $month_orders=[];
                                        if(isset($all_bills[$i])){
                                            $month_orders=$all_bills[$i]->all();
                                        }


                                        $target_val=0;

                                        if(isset($branch_target_this_year[$month_key])){
                                            $target_val=$branch_target_this_year[$month_key]->all();
                                            $target_val=convert_inside_obj_to_arr($target_val,"target_amount");
                                            $target_val=array_sum($target_val);
                                        }


                                        $client_bill_total_amount=0;
                                        $client_bill_total_return_amount=0;
                                        $client_total_paid_amount_in_cash=0;
                                        $client_total_paid_amount_in_atm=0;

                                        if(count($month_orders)){
                                            $client_bill_total_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_amount");
                                            $client_bill_total_amount=array_sum($client_bill_total_amount);

                                            $client_bill_total_return_amount=convert_inside_obj_to_arr($month_orders,"client_bill_total_return_amount");
                                            $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                            $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_cash");
                                            $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                            $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($month_orders,"client_total_paid_amount_in_atm");
                                            $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                        }

                                        $progress_val=0;
                                        if($target_val>0){
                                            $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                        }
                                        else{
                                            $progress_val="0";
                                        }
                                    ?>

                                    <tr>
                                        <td>
                                            <label class="label label-info">{{$month_key}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$target_val}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>
                                            <label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>
                                            <label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>
                                            <label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>

                                            <label class="label label-primary">الاجمالي {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                    {{$progress_val}}%
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                    {{(100-$progress_val)}}%
                                                </div>
                                            </div>
                                        </td>


                                    </tr>


                                <?php endfor; ?>
                                </tbody>

                            </table>



                            <?php else: ?>
                            <div class="alert alert-warning">
                                ليس لديك اي فواتير في الفرع
                            </div>


                            <?php endif; ?>

                        </div>
                    </div>

                <?php elseif($target_type=="day"): ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$target_year}} - {{$target_month}}
                        </div>
                        <div class="panel-body">

                            <?php if(isset_and_array($all_bills)): ?>

                            <table class="table table-striped table-bordered table_without_paging">
                                <thead>

                                <tr>
                                    <td>اليوم</td>
                                    <td>قيمة التارجت</td>
                                    <td>قيم الفواتير</td>
                                    <td>نسبة تحقيق التارجت</td>
                                    <td>نسبة العجز</td>
                                </tr>
                                </thead>

                                <tbody>
                                <?php for($i=1;$i<=$month_days_number;$i++): ?>

                                    <?php
                                        $day_key=$i;

                                        $day_orders=[];
                                        if(isset($all_bills[$i])){
                                            $day_orders=$all_bills[$i]->all();
                                        }


                                        $target_val=($branch_target_this_month->target_amount)/$month_days_number;
                                        $target_val=round($target_val,2);

                                        $client_bill_total_amount=0;
                                        $client_bill_total_return_amount=0;
                                        $client_total_paid_amount_in_cash=0;
                                        $client_total_paid_amount_in_atm=0;

                                        if(count($day_orders)){
                                            $client_bill_total_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_amount");
                                            $client_bill_total_amount=array_sum($client_bill_total_amount);

                                            $client_bill_total_return_amount=convert_inside_obj_to_arr($day_orders,"client_bill_total_return_amount");
                                            $client_bill_total_return_amount=array_sum($client_bill_total_return_amount);

                                            $client_total_paid_amount_in_cash=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_cash");
                                            $client_total_paid_amount_in_cash=array_sum($client_total_paid_amount_in_cash);

                                            $client_total_paid_amount_in_atm=convert_inside_obj_to_arr($day_orders,"client_total_paid_amount_in_atm");
                                            $client_total_paid_amount_in_atm=array_sum($client_total_paid_amount_in_atm);
                                        }

                                        $progress_val=0;
                                        if($target_val>0){
                                            $progress_val=(($client_bill_total_amount-$client_bill_total_return_amount)/$target_val)*100;
                                            $progress_val=round($progress_val,2);
                                        }
                                        else{
                                            $progress_val="0";
                                        }
                                    ?>

                                    <tr>
                                        <td>
                                            <label class="label label-info">{{$day_key}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$target_val}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info"> قيمة الفواتير {{$client_bill_total_amount}}</label>
                                            <label class="label label-info"> الكاش المستلم{{$client_total_paid_amount_in_cash}}</label>
                                            <label class="label label-info"> الشبكة المستلمة {{$client_total_paid_amount_in_atm}}</label>
                                            <label class="label label-danger"> مرجعات {{$client_bill_total_return_amount}}</label>

                                            <label class="label label-primary">الاجمالي {{$client_bill_total_amount-$client_bill_total_return_amount}}</label>
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{$progress_val}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$progress_val}}%">
                                                    {{$progress_val}}%
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="{{(100-$progress_val)}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{(100-$progress_val)}}%">
                                                    {{(100-$progress_val)}}%
                                                </div>
                                            </div>
                                        </td>


                                    </tr>


                                <?php endfor; ?>
                                </tbody>

                            </table>



                            <?php else: ?>
                            <div class="alert alert-warning">
                                ليس لديك اي فواتير في الفرع
                            </div>


                            <?php endif; ?>

                        </div>
                    </div>



                <?php endif; ?>







            </div>
        </div>




    </div>


@endsection