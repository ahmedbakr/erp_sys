@extends('admin.main_layout')

@section("subview")

    <div class="">


        <div class="panel panel-success">
            <div class="panel-heading">إحصائيات فواتير العملاء لليوم الحالي</div>
            <div class="panel-body">

                <form action="{{url('admin/client_bills/show_client_bills_statistics')}}" method="GET">

                    <?php if(false): ?>
                        <div class="col-md-4 col-md-offset-4">

                            <?php
                            echo
                            generate_select_tags(
                                $field_name="branch_id",
                                $label_name="إختار الفرع",
                                $text=array_merge(["الكل",convert_inside_obj_to_arr($all_branches,"full_name")]),
                                $values=array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id")),
                                $selected_value=array((isset($_GET['branch_id'])?$_GET['branch_id']:0)),
                                $class="form-control select_2_class",
                                $multiple="",
                                $required = "",
                                $disabled = "",
                                $data = ""
                            );
                            ?>

                        </div>
                        <button type="submit" class="btn btn-primary">عرض</button>

                    <?php endif; ?>


                </form>

                <div class="alert alert-info">
                    {{date("j-m, g:i a",strtotime($start_day_date))}}
                    <br>
                    {{date("j-m, g:i a",strtotime($end_day_date))}}
                </div>

                <div class="col_3">
                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: green"></i>
                            <div class="stats">
                                <h5>{{$this_day_buy_amount}}</h5>
                                <div class="grow">
                                    <p>إجمالي المستلم</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red"></i>
                            <div class="stats">
                                <h5>{{$this_day_return_amount}}</h5>
                                <div class="grow">
                                    <p>إجمالي المرتجع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">إحصائيات فواتير العملاء لليوم السابق</div>
            <div class="panel-body">

                <div class="alert alert-info">
                    {{date("j-m, g:i a",strtotime($start_before_date))}}
                    <br>
                    {{date("j-m, g:i a",strtotime($end_before_date))}}
                </div>

                <div class="col_3">
                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: green"></i>
                            <div class="stats">
                                <h5>{{$before_day_return_amount}}</h5>
                                <div class="grow">
                                    <p>إجمالي المستلم</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red"></i>
                            <div class="stats">
                                <h5>{{$before_day_return_amount}}</h5>
                                <div class="grow">
                                    <p>إجمالي المرتجع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">جميع إحصائيات فواتير العملاء لهذا الشهر</div>
            <div class="panel-body">

                <div class="col_3">
                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: green"></i>
                            <div class="stats">
                                <h5>{{$buy_amount_this_month}}</h5>
                                <div class="grow">
                                    <p>إجمالي المستلم</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red"></i>
                            <div class="stats">
                                <h5>{{$return_amount_this_month}}</h5>
                                <div class="grow">
                                    <p>إجمالي المرتجع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">جميع إحصائيات فواتير العملاء لهذا العام</div>
            <div class="panel-body">

                <div class="col_3">
                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: green"></i>
                            <div class="stats">
                                <h5>{{$buy_amount_this_year}}</h5>
                                <div class="grow">
                                    <p>إجمالي المستلم</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 widget widget1">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red"></i>
                            <div class="stats">
                                <h5>{{$return_amount_this_year}}</h5>
                                <div class="grow">
                                    <p>إجمالي المرتجع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>


@endsection