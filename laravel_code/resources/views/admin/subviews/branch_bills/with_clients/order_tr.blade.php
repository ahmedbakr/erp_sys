<?php if(isset($pro_obj)): ?>

<tr data-proid="{{$pro_obj->pro_id}}">
    <td>
        <input type="hidden" name="pro_id[]" value="{{$pro_obj->pro_id}}">
        <input type="text" disabled  value="{{$pro_obj->pro_name}}" class="form-control">
    </td>
    <td style="text-align: center;">

        <label class="label label-success"  style="display: none;">اقصي كمية مسموح بها {{$pro_obj->b_p_quantity}}</label>
        <input type="number" name="order_quantity[]" min="1" max="{{$pro_obj->b_p_quantity}}" value="1" class="form-control check_quantity" style="width: 100px;">


        <button class="btn btn-warning btn_decrease_order" style="display: none;"><i class="fa fa-minus-circle"></i></button>
        <button class="btn btn-success btn_increase_order" style="display: none;"><i class="fa fa-plus-circle"></i></button>

    </td>
    <td>
        <input type="number" name="pro_sell_price[]" disabled data-current_pro_price="{{$pro_obj->current_pro_price}}" value="{{$pro_obj->current_pro_price}}" class="form-control pro_sell_price_class">
    </td>
    <td>
        <input type="number" disabled  value="0" class="form-control total_tr_price">
    </td>

    <td>
        <label class="label label-default enter_package_label">لا</label>
    </td>

    <td>
        <button class="btn btn-danger remove_tr_order">مسح</button>
    </td>
</tr>
<?php endif; ?>
