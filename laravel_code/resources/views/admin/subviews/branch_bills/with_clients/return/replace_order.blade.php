@extends('admin.main_layout')

@section("subview")


    <div class="panel panel-info">
        <div class="panel-heading">
            تبديل منتج
        </div>
        <div class="panel-body">

            <div class="alert alert-info">
                <p>المنتج المراد تبديله من التصنيف {{$cat_obj->cat_name}}</p>
            </div>

            <form action="{{url("/admin/client_bills/replace_bill_order")}}" method="post">

                @include("common.category.accounts_tree.tree_modal")
                <button
                        type="button"
                        class="btn btn-primary show_tree_modal center-block"
                        data-select_what="item"
                        data-select_type="single"
                        data-div_to_show_selection=".select_pro"
                        data-input_field_name="replace_with_pro_id"
                >
                    اظهر الشجرة لتختار المنتج
                </button>

                <div class="select_pro"></div>


                <input type="hidden" name="order_id" value="{{$order_id}}">

                {{csrf_field()}}
                <input id="submit" type="submit" value="تبديل" class="col-md-2 col-md-offset-10 btn btn-primary btn-lg" >
            </form>


        </div>
    </div>


@endsection