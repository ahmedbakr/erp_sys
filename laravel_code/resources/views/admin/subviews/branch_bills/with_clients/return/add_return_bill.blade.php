@extends('admin.main_layout')

@section("subview")

    <div class="return_client_bill_form">

        <input type="hidden" class="bill_id_value" value="{{$bill_id}}">
        <input type="hidden" class="return_bill_allow_remain_money" value="{{(int)check_permission($user_permissions,"branch/client_orders","return_bill_allow_remain_money",$current_user)}}">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        إضافة فاتورة مرتجع علي البيع
                    </h3>
                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading">أدخل رقم الفاتورة بس اضغظ علي زر الادخال</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="input-group">
                                <input type="text" placeholder="أدخل رقم الفاتورة بس اضغظ علي زر الادخال" value="{{$bill_id}}" name="bill_number" class="form-control type_bill_number">
                                <span class="input-group-btn">
                                        <button class="btn btn-primary get_bill_orders_submit" type="button">احصل علي الفاتورة</button>
                                    </span>
                            </div><!-- /input-group -->

                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-info bill_short_info hide_row">
                <div class="panel-heading">تاريخ الفاتورة</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-info content_body" style="text-align: center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/client_bills/add_return_bill/$bill_id")?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>



                <div class="bill_returns_containers hide_row">

                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">العناصر التي تم ارجاعها</div>
                        <div class="panel-body collapse_body collapse in">

                        </div>
                    </div>

                </div>

                <br>
                <div class="bill_orders_containers hide_row">

                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">العناصر التي تم بيعها</div>
                        <div class="panel-body collapse_body collapse in">

                        </div>
                    </div>

                    {{csrf_field()}}
                    <div class="form_submit_btns">
                        <input id="submit" type="submit" value="حفظ" class="col-md-2 col-md-offset-10 btn btn-primary btn-lg add_return_bill_submit_btn" >
                    </div>

                </div>


            </form>



        </div>

    </div>


@endsection


