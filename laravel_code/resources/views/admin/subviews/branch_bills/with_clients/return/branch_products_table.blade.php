<?php

    $return_orders = collect($return_orders);
    $return_orders = $return_orders->groupBy("pro_id");


?>

<table class="table table-striped">

    <th>#</th>
    <th>اسم المنتج</th>
    <th>سعر شراء العميل للمنتج</th>
    <th>الكميلة المطلوبه للمنتج</th>
    <th>الكمية المتاحه لعمل مرتجع</th>
    <th>الكمية التي تريد ارجاعها</th>
    <th>المال المستحق للعميل</th>

    <?php foreach($orders as $key => $order): ?>

    <?php
        $order_quantity=$order->order_quantity;
        $return_quantity=isset($return_orders[$order->pro_id])?array_sum(convert_inside_obj_to_arr($return_orders[$order->pro_id],"order_quantity")):0;
        $max_return_order=$order_quantity-$return_quantity;
    ?>

    <tr>
        <td>{{$key+1}}</td>
        <td>
            <span class="label label-default"><?= $order->pro_name ?></span>
            <br>
            <a href="{{url("/admin/client_bills/replace_bill_order?order_id=$order->order_id")}}">تبديل المنتج</a>

        </td>
        <td>
            <span class="label label-primary product_price"><?= $order->pro_sell_price ?></span>
        </td>
        <td>
            <span class="label label-info"><?= $order->order_quantity ?></span>
        </td>
        <td>
            <label class="label label-warning">{{$max_return_order}}</label>
        </td>
        <td>
            <input type='number' min='0' max='{{$max_return_order}}' value='0' class='form-control returned_quantity' name='returned_quantity[]' >

            <input type='hidden' value='{{$order->order_id}}' class='form-control' name='order_id[]' >
        </td>
        <td class="row_returned_money"></td>
    </tr>

    <?php endforeach; ?>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="total_returned_money">
            الاجمالي
        </td>

    </tr>


</table>

<div class="col-md-12">

    <div class="form-group col-md-4">
        <label for="">اجمالي المال المتسحق للعميل</label>
        <input type="number" readonly name="return_money_to_client" value="0" class="form-control return_money_to_client">
    </div>
    <div class="form-group col-md-4">
        <label for="">الدفع بالكاش</label>
        <input type="number" name="return_cash_money_to_client" value="0" class="form-control return_cash_money_to_client">
    </div>

    <?php if(check_permission($user_permissions,"branch/client_orders","return_bill_allow_use_atm",$current_user)): ?>
    <div class="form-group col-md-4">
        <label for="">الدفع بالشبكة</label>
        <input type="number" name="return_atm_money_to_client" value="0" class="form-control return_atm_money_to_client">
    </div>
    <?php endif; ?>


    <div class="col-md-12">
        <div class="return_money_to_client_class"></div>
    </div>



</div>


