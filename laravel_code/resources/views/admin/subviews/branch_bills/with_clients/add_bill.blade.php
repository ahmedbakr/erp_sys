@extends('admin.main_layout')

@section("subview")

    <style>
        .add_client_bill_form input[type='number']{
            font-weight: bolder;
        }

        .enter_package_label{
            padding: 0;
            background: none;
            color: #000;
            white-space: initial;
            width: 85px;
            word-break: break-word;
            display: inline-block;
        }
    </style>


    <div class="panel panel-primary">
        <div class="panel-heading">
            فاتورة بيع جديدة
        </div>
        <div class="panel-body">


            <div class="add_client_bill_form">

                <div class="">

                    <form id="save_form" action="<?=url("/admin/client_bills/add_bill?branch_id=$branch_id&branch_admin_id=$branch_admin_id")?>" method="POST" enctype="multipart/form-data">



                        <input type="hidden" id="branch_id_id" name="branch_id" value="{{$branch_id}}">
                        <input type="hidden" name="branch_admin_id" value="{{$branch_admin_id}}">

                        <?php

                        if (count($errors->all()) > 0)
                        {
                            $dump = "<div class='alert alert-danger'>";
                            foreach ($errors->all() as $key => $error)
                            {
                                $dump .= $error." <br>";
                            }
                            $dump .= "</div>";

                            echo $dump;
                        }


                        if (isset($success)&&!empty($success)) {
                            echo $success;
                        }
                        ?>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-warning" >
                                    <div class="panel-heading" data-toggle="collapse" href=".shortcuts_div" style="cursor:pointer;">الاختصارات</div>
                                    <div class="panel-body shortcuts_div collapse">
                                        <p>الاختصارات هتبقي شغالة فقط علي الصف الملون في جدول المنتجات المختارة</p>
                                        <p>alt,+ تزود الكمية</p>
                                        <p>alt,- تنقص الكمية</p>
                                        <p>alt,backspace تمسح</p>
                                        <p>alt,(اي رقم) تكتب</p>
                                        <p>alt,ctrl للتنقل بين الصفوف في جدول المنتجات المختارة</p>
                                        <p>alt,enter تحسب الفاتورة</p>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading" data-toggle="collapse" href=".client_data" style="cursor:pointer;">بيانات الفاتورة والعميل</div>
                                    <div class="panel-body client_data collapse in">
                                        <div class="col-md-12" style="text-align: center;    margin-bottom: 10px;">
                                            <a href="{{url("/admin/users/save/customer")}}" class="btn btn-primary" target="_blank">
                                                اضف عميل جديد
                                            </a>
                                        </div>

                                        <hr style="margin-top: 5px;">

                                        <div class="col-md-6">
                                            <label for="">تاريخ الفاتورة*</label>
                                            <input type="date" required name="client_bill_date" value="{{date("m/j/Y g:i a")}}" class="form-control date_time_input">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">رقم تليفون العميل*</label>
                                            <input type="text" required name="client_tel_number" class="form-control client_tel_number">

                                            <?php if(false): ?>
                                            <select name="client_tel_number" class="form-control client_tel_number">
                                                <option value="0">0</option>
                                            </select>
                                            <?php endif; ?>


                                        </div>

                                        <?php if(false): ?>
                                        <div class="col-md-6">
                                            <label for="">اسم العميل</label>
                                            <input type="text" name="client_name" class="form-control client_name">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">البريد الالكتروني للعميل</label>
                                            <input type="text" name="client_email" class="form-control client_email">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">عنوان العميل</label>
                                            <input type="text" name="client_address" class="form-control client_address">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">الفئة العمرية للعميل</label>
                                            <select name="age_range" class="form-control age_range">
                                                <option value="" disabled selected>اختر الفئة العمرية</option>
                                                <option value="15-25">15-25</option>
                                                <option value="26-40">26-40</option>
                                                <option value="41-60">41-60</option>
                                                <option value="العمر>60">العمر>60</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">نوع العميل</label>
                                            <select name="user_gender" class="form-control user_gender" >
                                                <option value="" disabled selected>اختر نوع العميل</option>
                                                <option value="male">ذكر</option>
                                                <option value="female">انثي</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">من طرف عميل (رقم تليفون العميل الاصلي)</label>
                                            <input type="text" name="original_client_tel_number" class="form-control original_client_tel_number">
                                        </div>
                                        <?php endif; ?>


                                        <div class="col-md-12">
                                            <button class="btn btn-primary add_new_client_data" type="button" style="margin: 5px;margin-right: 0px;">تحقق من بيانات العميل</button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading" data-toggle="collapse" href=".show_client_data" style="cursor:pointer;">
                                        نتائج البحث عن العميل
                                    </div>
                                    <div class="panel-body show_client_data collapse in">
                                        <div class="col-md-12">
                                            <div class="client_msgs"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-9">
                                <div class="panel panel-primary">
                                    <div class="panel-heading" data-toggle="collapse" href=".choose_product" style="cursor:pointer;">
                                        اضف منتج عن طريق الاختيار
                                    </div>
                                    <div class="panel-body choose_product collapse">

                                        <div class="col-md-9 load_cats_div">
                                            <div class="row">
                                                <button class="btn btn-primary show_parent_0_cats"> اظهر التصنيفات الرئيسية</button>
                                            </div>


                                            {!! $cats_view !!}
                                        </div>

                                        <div class="col-md-3" style="border-right: 1px solid #CCC;margin-bottom: 10px;">
                                            <div class="form-group">
                                                <label for="">ابحث بواسطة الباركود او الاسم</label>
                                                <input type="text"  class="form-control search_for_product_by_name_or_barcode">
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="col-md-12 load_products_div">

                                            {!! $products_view !!}
                                        </div>

                                        <div class="col-md-12 search_res">

                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        اضف منتج جديد
                                    </div>
                                    <div class="panel-body">

                                        <input type="hidden" name="selected_price_id" class="selected_price_id">

                                        <div class="form-group">
                                            <label for="">باركود المنتج</label>
                                            <input type="text" disabled="disabled" class="form-control search_for_product_barcode">
                                        </div>
                                        <div class="form-group">
                                            <div class="barcode_msgs"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        {{--products & packages--}}
                        <div class="row">
                            <div class="bill_orders_containers col-md-12">

                                <div class="col-md-7">

                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            المنتجات المختارة
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-bordered table-striped orders_table">
                                                <tr>
                                                    <th>اسم المنتج</th>
                                                    <th>الكمية</th>
                                                    <th>سعر الوحدة</th>
                                                    <th>السعر الكلي</th>
                                                    <th>دخل عرض؟</th>
                                                    <th>مسح</th>
                                                </tr>
                                            </table>

                                            <div class="col-md-12" style="text-align: center;">
                                                <button class="btn btn-primary calc_total_bill" type="button">احسب مجموع الفاتورة</button>
                                            </div>
                                            <div class="col-md-12" style="text-align: center;">
                                                <div class="alert alert-success">
                                                    <h1>مجموع الفاتورة</h1>
                                                    <h1 class="client_bill_total_amount_class_label"></h1>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            كوبون
                                        </div>
                                        <div class="panel-body">
                                            <div class="alert alert-info">
                                                <p>
                                                    بعد ان تدخل كل الكوبونات التي تريد ان تدخلها
                                                    يجب ان تضغط زر تحقق من الكوبونات لكي تعرف قيمة الخصم التي سوف تخصمها الكوبونات
                                                    ان لم تضغط هذا الزر سوف تكون كأنك لم تدخل الكوبونات من الاساس
                                                    .بعد الضغط علي زر تحقق من الكوبونات لا يمكنك ان تعدل البيانات الموضوعة بداخل الجدول
                                                </p>
                                            </div>

                                            <table class="table table-striped table-bordered coupons_table">
                                                <thead>
                                                <tr>
                                                    <th>كود الكوبون</th>
                                                    <th>نوع الكوبون</th>
                                                    <th>قيمة الكوبون</th>
                                                    <th>مسح كوبون</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr class="coupon_item">
                                                    <td>
                                                        <input type="text" name="coupon_text[]" class="form-control coupon_text">
                                                    </td>
                                                    <td>
                                                        <input type="text" disabled class="form-control coupon_type_label">
                                                        <input type="hidden" class="coupon_type">
                                                    </td>
                                                    <td>
                                                        <input type="number" disabled class="form-control coupon_value">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger remove_coupon_row">مسح كوبون</button>
                                                    </td>
                                                </tr>
                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="4">
                                                        <div class="coupon_msgs"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-info add_coupon_row">اضف كوبون اخر</button>
                                                    </td>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-primary check_coupons">تحقق من الكوبونات</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><span class="label label-info">اجمالي الخصم</span></td>
                                                    <td colspan="2"><span class="total_coupon_discount">0</span></td>
                                                </tr>


                                                </tfoot>




                                            </table>



                                        </div>
                                    </div>
                                </div>


                                {{--this is an important code please do not remove even it is displayed none--}}
                                <div class="panel panel-info" style="display: none;">
                                    <div class="panel-heading">
                                        العروض
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <th>اسم العرض</th>
                                                <th>العدد الادني لدخول العرض</th>
                                                <th>نسبة الخصم علي منتجات العرض</th>
                                                <th>العدد الادني لتستلم هدية العرض</th>
                                                <th>هدية العرض</th>
                                            </tr>

                                            <?php foreach($all_branch_packages_data as $key=>$pac): ?>
                                                <?php
                                                    if(!isset($pac->proids)){
                                                        continue;
                                                    }
                                                    $pac_pro_ids=$pac->proids;
                                                ?>
                                                <tr>
                                                    <td>

                                                        <input type="hidden" class="package_data"
                                                               data-packagename="{{$pac->package_name}}"
                                                               data-packagetype="{{(int)$pac->package_type}}"
                                                               data-packageid="{{$pac->package_id}}"
                                                               data-min_products_to_get_offer="{{$pac->min_products_to_get_offer}}"
                                                               data-min_products_to_get_gift="{{$pac->min_products_to_get_gift}}"
                                                               data-proids="{{json_encode($pac_pro_ids)}}"
                                                               data-discount_percentage="{{$pac->discount_percentage_for_product}}"
                                                               data-gift_id="{{$pac->product_gift_id}}"
                                                               data-gift_name="{{$pac->gift_name}}"
                                                        >
                                                        {{$pac->package_name}}
                                                    </td>
                                                    <td>{{$pac->min_products_to_get_offer}}</td>
                                                    <td>{{$pac->discount_percentage_for_product}}%</td>
                                                    <td>{{$pac->min_products_to_get_gift}}</td>
                                                    <td>{{$pac->gift_name}}</td>
                                                </tr>
                                            <?php endforeach;?>

                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        دفع الفاتورة
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-md-12" style="display: none;">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">مقدار ما دفعه العميل</label>
                                                    <input type="number" required step=".01" value="0" class="form-control total_client_paid_just_for_calc">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">المتبقي للعميل</label>
                                                    <input type="number" required step=".01" value="0" class="form-control total_remain_just_for_calc" style="color:red !important;">
                                                </div>
                                            </div>

                                        </div>

                                        <hr>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="">إجمالي الفاتورة + 5% ضريبه القيمة المضافة</label>
                                                <input type="number" required name="client_bill_total_amount" readonly="readonly" class="form-control client_bill_total_amount_class" style="font-size: 24px;font-weight: bolder;">
                                            </div>

                                            <input type="hidden" name="cash_money" value="0" class="cash_money">
                                            <input type="hidden" name="atm_money" value="0" class="atm_money">

                                            <?php foreach($all_payment_methods as $key=>$method): ?>
                                            <div class="col-md-4">
                                                <label for="">{{$method->payment_method_name}}</label>
                                                <input type="number" step=".01" name="payment_method_{{$method->payment_method_id}}" value="0" data-is_payment_method_atm="{{(int)$method->is_payment_method_atm}}" class="form-control payment_method_money">
                                            </div>
                                            <?php endforeach;?>


                                            <div class="col-md-12">
                                                <div class="money_msgs"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" id="allow_cashier_atm" value="{{(int)check_permission($user_permissions,"admin/client_orders","allow_cashier_atm",$current_user)}}">
                        <input type="hidden" id="allow_cashier_to_remain_money" value="{{(int)check_permission($user_permissions,"admin/client_orders","allow_cashier_to_remain_money",$current_user)}}">

                        <input type="hidden" class="total_bill_tax_value" name="total_bill_tax_value" value="">

                        {{csrf_field()}}

                        <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg add_new_bill_btn">
                    </form>

                </div>

            </div>

        </div>
    </div>

@endsection


