@extends('admin.main_layout')

@section("subview")

    <div class="">

        <div class="col-md-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    اضف فاتورة جديدة
                </div>
                <div class="panel-body" style="text-align: center;">

                    <form action="{{url("/admin/client_bills/add_bill")}}" method="get">

                    <?php

                        echo generate_depended_selects(
                            $field_name_1="branch_id",
                            $field_label_1="اختار الفرع",
                            $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                            $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                            $field_selected_value_1="",
                            $field_required_1="required",
                            $field_class_1="form-control",
                            $field_name_2="branch_admin_id",
                            $field_label_2="اختار المستخدم",
                            $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                            $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                            $field_selected_value_2="",
                            $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                            $field_required_2="required",
                            $field_class_2="form-control",
                            $add_blank_options=false,
                            $obj_data=""
                        );
                    ?>
                        <button class="btn btn-primary" type="submit">ادخل فاتورة بالفرع والمستخدم المختارين</button>
                    </form>


                </div>
            </div>


            <div class="panel panel-primary">
                <div class="panel-heading">
                    بحث في كل الفواتير
                </div>
                <div class="panel-body">

                    <form action="{{url("/admin/client_bills/show_all")}}">

                        <div class="col-md-12">

                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <?php
    //                                    echo
    //                                    generate_select_tags(
    //                                        $field_name="branch_id",
    //                                        $label_name="اختار الفرع",
    //                                        $text=array_merge(["All"],convert_inside_obj_to_arr($all_branches,"full_name")),
    //                                        $values=array_merge(["0"],convert_inside_obj_to_arr($all_branches,"user_id")),
    //                                        $selected_value="",
    //                                        $class="form-control select_2_class",
    //                                        $multiple="",
    //                                        $required="",
    //                                        $disabled = "",
    //                                        $data = $request_data
    //                                    );

                                        echo generate_depended_selects(
                                            $field_name_1="select_branch_id",
                                            $field_label_1="اختار الفرع",
                                            $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                                            $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                                            $field_selected_value_1="",
                                            $field_required_1="",
                                            $field_class_1="form-control",
                                            $field_name_2="select_branch_admin_id",
                                            $field_label_2="اختار المستخدم",
                                            $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                                            $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                                            $field_selected_value_2="",
                                            $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                                            $field_required_2="",
                                            $field_class_2="form-control",
                                            $add_blank_options=true,
                                            $obj_data=$request_data
                                        );


                                    ?>
                                </div>

                                <div class="col-md-6">

                                    <?php

                                        $customers_arr=array_map(
                                            function($a, $b) { return $a . ' - ' . $b; },
                                            convert_inside_obj_to_arr($all_customers,"full_name"),
                                            convert_inside_obj_to_arr($all_customers,"user_tel")
                                        );

                                        echo
                                        generate_select_tags(
                                            $field_name="customer_id",
                                            $label_name="اختار العميل",
                                            $text=array_merge(["All"],$customers_arr),
                                            $values=array_merge(["0"],convert_inside_obj_to_arr($all_customers,"user_id")),
                                            $selected_value="",
                                            $class="form-control select_2_class",
                                            $multiple="",
                                            $required="",
                                            $disabled = "",
                                            $data = $request_data
                                        );
                                    ?>

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">من</label>
                                        <input type="date" name="from_date" id="filter_date_from" value="{{date('Y-m-d',strtotime($bill_date_from))}}" class="form-control date_input">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">الي</label>
                                        <input type="date" name="to_date" id="filter_date_to" value="{{date('Y-m-d',strtotime("+1 day",strtotime($bill_date_to)))}}" class="form-control date_input">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">عدد الفواتير المعروضة</label>
                                        <input type="number" name="bill_pagination_number"  value="{{$bill_pagination_number}}" class="form-control">
                                    </div>
                                </div>

                            </div>


                        </div>

                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                                <button class="btn btn-info">نتائج البحث</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


            <div class="panel panel-primary">
                <div class="panel-heading">
                    بحث برقم الفاتورة
                </div>
                <div class="panel-body">

                    <form action="{{url("/admin/client_bills/show_all")}}">

                        <div class="col-md-12">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">إكتب رقم الفاتورة</label>
                                    <input type="number" name="bill_id" value="{{(isset($_GET['bill_id']))?$_GET['bill_id']:""}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-2" style="text-align: center;margin-top: 25px;">
                                <button class="btn btn-info">نتائج البحث</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    فواتير العملاء
                </div>

                <div class="panel-body" style="overflow-x: scroll;">

                    <div class="col-md-12" style="text-align: center;">
                        {{$bill_objs_pagination->links()}}
                    </div>

                    <table class="table table-striped table-bordered table_without_paging_1" cellspacing="0" width="100%">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>اسم الفرع</th>
                                <th>اسم الكاشير(المستخدم)</th>
                                <th>رقم الفاتورة</th>
                                <th>اسم العميل</th>
                                <th>تاريخ الفاتورة</th>
                                <th>تكلفة الفاتورة</th>
                                <th>المدفوع من الفاتورة</th>
                                <th>المتبقي من الفاتورة</th>
                                <th>قيمة مرتجع الفاتورة</th>
                                <th>طلبات الفاتورة</th>
                                <th>ارجع طلبات من الفاتورة</th>
                            </tr>
                            </thead>


                            <tbody>
                                <?php
                                    $all_bills_total_amount=0;
                                    $all_bills_client_paid=0;
                                    $all_bills_remain=0;
                                    $all_bills_return=0;
                                ?>
                                <?php foreach($all_bills as $key=>$bill): ?>
                                    <?php
                                        $all_bills_total_amount=$all_bills_total_amount+$bill->client_bill_total_amount;
                                        $all_bills_client_paid=$all_bills_client_paid+$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm;
                                        $all_bills_remain=$all_bills_remain+$bill->client_bill_total_remain_amount;
                                        $all_bills_return=$all_bills_return+$bill->client_bill_total_return_amount;
                                    ?>

                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            <label class="label label-info">{{$bill->branch_full_name}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$bill->cashier_full_name}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">#{{$bill->client_bill_id}}</label>
                                        </td>
                                        <td>
                                            <a href="{{url("admin/clients/profile/$bill->customer_id")}}">
                                                <label class="label label-default">{{$bill->client_full_name}}</label>
                                            </a>

                                            <label class="label label-info">{{$bill->client_user_tel}}</label>
                                        </td>


                                        <td>
                                            <label class="label label-info">{{date("Y-m-d",strtotime($bill->client_bill_date))}}</label>
                                            <label class="label label-default">{{get_hegri_date(strtotime($bill->client_bill_date))}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$bill->client_bill_total_amount}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-info">{{$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-warning">{{$bill->client_bill_total_remain_amount}}</label>
                                        </td>
                                        <td>
                                            <label class="label label-danger">{{$bill->client_bill_total_return_amount}}</label>
                                        </td>
                                        <td>
                                            <a href="{{url("/admin/client_bills/show_bill_orders/$bill->client_bill_id")}}">
                                                <label class="label label-primary">طلبات الفاتورة</label>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{url("/admin/client_bills/add_return_bill/$bill->client_bill_id")}}">
                                                <label class="label label-warning">ارجع طلبات من الفاتورة</label>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>

                                <tr>
                                    <td>الاجمالي</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <label class="label label-info">{{$all_bills_total_amount}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-info">{{$all_bills_client_paid}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-warning">{{$all_bills_remain}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-danger">{{$all_bills_return}}</label>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                        </table>

                    <div class="col-md-12"  style="text-align: center;">
                        {{$bill_objs_pagination->links()}}
                    </div>
                </div>
            </div>


        </div>

    </div>

@endsection


