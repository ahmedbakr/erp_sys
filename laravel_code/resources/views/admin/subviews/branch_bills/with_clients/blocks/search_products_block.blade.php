<div class="col-md-12" style="margin-top: 10px;">

<?php if(count($products)): ?>

    <div class="row loaded_products" style="margin-top: 10px;">
        <div class="form-group">
            <label for="">البحث السريع(بواسطة الاسم او الباركود)</label>
            <input type="text" class="form-control fast_search">
        </div>

        <?php foreach($products as $key=>$product): ?>
        <div class="select_product" data-pro_name="{{$product->pro_name}}" data-pro_barcode="{{$product->pro_barcode}}" data-pro_barcode="{{$product->pro_barcode}}" style="cursor: pointer;float: right;padding: 5px;margin: 5px;border: 1px solid #CCC;background: #f1f1f1;text-align: center;">
            <div class="col-md-12" style="display: none;border: 1px solid #CCC">
                <img src="{{url("/".$product->pro_img_path)}}" style="width: 100%;height: 200px;" alt="">
            </div>
            <div class="" style="background: #f1f1f1;text-align: center; ">
                <h5>{{$product->pro_name}}</h5>
            </div>
        </div>
        <?php endforeach;?>
    </div>

<?php else: ?>

    <div class="alert alert-warning">
        <p>لا يوجد منتجات بهذا الاسم او الباركود</p>
    </div>


<?php endif; ?>


</div>
