<div class="panel panel-info">
    <div class="panel-heading">{{$header}}</div>
    <div class="panel-body">
        <table id="cat_table" class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>#</td>
                <td>باركود المنتج</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($barcode_arr as $key => $barcode): ?>
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$barcode}}</td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>