<form id="save_form" action="<?=url("/admin/branch_bills/approve_import_products_first_time_to_stock")?>" method="POST" enctype="multipart/form-data">

    <table id="cat_table" class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم المنتج</td>
            <td>باركود المنتج</td>
            <td>الكمية</td>
            <td>حد الطلب</td>
            <td>مسح</td>
        </tr>
        </thead>
        <tbody>
        <?php $counter = 1; ?>
        <?php foreach($input_arr as $key => $value): ?>
            <tr id="row_{{$counter}}" class="table_item">
                <td>
                    {{$counter}}
                    <input type="hidden" name="pro_id[]" value="{{$value['pro_id']}}">
                    <input type="hidden" name="branch_id[]" value="{{$value['branch_id']}}">
                </td>
                <td><input type="text" readonly name="pro_barcode[]" value="{{$value['pro_name']}}"></td>
                <td><input type="text" readonly name="pro_barcode[]" value="{{$value['pro_barcode']}}"></td>
                <td>
                    <input type="number" min="0" name="quantity[]" class="quantity_val" value="{{$value['b_p_quantity']}}">
                </td>
                <td>
                    <input type="number" min="0" name="limit[]" class="limit_val" value="{{$value['b_p_quantity_limit']}}">
                </td>
                <td>
                    <button class="btn btn-danger delete_table_row" data-row_id="row_{{$counter}}"> مسح <i class="fa fa-times"></i> </button>
                </td>
            </tr>
            <?php $counter++; ?>
        <?php endforeach; ?>
        </tbody>
    </table>

    {{csrf_field()}}
    <button type="submit" class="btn btn-primary col-md-4 col-md-offset-4 btn-lg">حفظ</button>
</form>