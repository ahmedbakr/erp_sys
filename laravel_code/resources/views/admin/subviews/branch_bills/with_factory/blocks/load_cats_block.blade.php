<div class="row loaded_cats" data-parent_id="{{$loaded_cat_id}}" style="margin-top: 10px;">

    <?php if($loaded_cat_id>0): ?>
        <button class="btn btn-primary show_prev_cats" data-load_cat_id="{{$loaded_cat_id}}">السابق</button>
    <?php endif; ?>


    <?php if(count($all_parent_cats)): ?>


        <?php foreach($all_parent_cats as $key=>$cat): ?>
        <button type="button" class="btn btn-info select_cat" style="cursor: pointer" data-cat_id="{{$cat->cat_id}}">
            {{$cat->cat_name}}
        </button>
        <?php endforeach;?>

    <?php else: ?>

        <div class="alert alert-warning">
            <p>لا يوجد تصنيفات</p>
        </div>

    <?php endif; ?>

</div>

