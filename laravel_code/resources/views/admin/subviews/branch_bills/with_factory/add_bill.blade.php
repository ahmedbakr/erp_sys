@extends('admin.main_layout')

@section("subview")
    <script src="{{url("/public_html/jscode/admin/branch_bills.js")}}"></script>


    <?php

        $header = " إضافة فاتورة طلبات منتجات للفرع";
        $branch_bill_id = "";
        $selected_branch = "";
        $bill_date = "";

        if (!empty($bill_data) && is_object($bill_data))
        {
            $header = " تعديل فاتورة طلبات منتجات للفرع";
            $branch_bill_id = "/".$bill_data->branch_bill_id;
            $selected_branch = $bill_data->branch_id;
            $bill_date = $bill_data->branch_bill_date;
        }

    ?>

    <div class="add_branch_bill_form add_branch_bill_another_class">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {{$header}}
                    </h3>
                </div>
            </div>

            <?php if(count($all_products)): ?>

                <input type="hidden" class="products_quantities_in_factory" value="{{json_encode(array_combine(convert_inside_obj_to_arr($all_products,"pro_id"),convert_inside_obj_to_arr($all_products,"pro_quantity")))}}">


                <form id="save_form" action="<?=url("/admin/branch_bills/add_bill".$branch_bill_id)?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">تفاصيل الفاتورة</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <?php
                                        echo
                                        generate_select_tags(
                                            $field_name="branch_id",
                                            $label_name="إختار الفرع",
                                            $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                                            $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                            $selected_value=["$selected_branch"],
                                            $class="form-control select_2_class",
                                            $multiple="",
                                            $required = "",
                                            $disabled = "",
                                            $data = ""
                                        );
                                        ?>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="">تاريخ الفاتورة</label>
                                        <input type="date" required name="bill_date" value="{{(!empty($bill_date))?$bill_date:date('Y-m-d')}}" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <?php if(false): ?>
                        <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"> اضافة المنتجات</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label for="">اختار المنتج</label>
                                        <select class="form-control branch_get_products">
                                            <option value="0">0</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">الكمية</label>
                                        <input type="number" class="add_quantity form-control" value="" >
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-info add_bill_order">اضف</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary pro_advanced_search">
                            <div class="panel-heading" data-toggle="collapse" href=".choose_product" style="cursor:pointer;">
                                اضف منتج عن طريق الاختيار
                            </div>
                            <div class="panel-body choose_product collapse">

                                <div class="col-md-9 load_cats_div">
                                    <div class="row">
                                        <button class="btn btn-primary show_parent_0_cats"> اظهر التصنيفات الرئيسية</button>
                                    </div>


                                    {!! $cats_view !!}
                                </div>

                                <div class="col-md-3" style="border-right: 1px solid #CCC;margin-bottom: 10px;">
                                    <div class="form-group">
                                        <label for="">ابحث بواسطة الباركود او الاسم</label>
                                        <input type="text"  class="form-control search_for_product_by_name_or_barcode">
                                    </div>
                                </div>

                                <hr>

                                <div class="col-md-12 load_products_div">

                                    {!! $products_view !!}
                                </div>

                                <div class="col-md-12 search_res">

                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">المنتجات</div>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped orders_table">
                                    <tr>
                                        <th>المنتج</th>
                                        <th>الكمية</th>
                                        <th>حذف</th>
                                    </tr>
                                    <?php if(isset_and_array($bill_orders)): ?>
                                        <?php
                                            $all_products_collection=collect($all_products)->groupBy("pro_id");
                                        ?>
                                        <?php foreach($bill_orders as $key=>$order): ?>

                                        <tr>
                                            <td>
                                                <?php
                                                    $pro_name="";
                                                    if(isset($all_products_collection[$order->pro_id])){
                                                        $pro_name=$all_products_collection[$order->pro_id]->all()[0]->pro_name;
                                                    }
                                                ?>
                                                <input type="hidden" name="pro_id[]" value="{{$order->pro_id}}">
                                                {{$pro_name}}
                                            </td>
                                            <td>
                                                <input type="number" name="b_o_quantity[]" class="form-control check_quantity" value="{{$order->b_o_quantity}}">
                                            </td>
                                            <td>
                                                <button type='button' class='btn btn-danger remove_parent_tr'>مسح</button>
                                            </td>
                                        </tr>

                                        <?php endforeach;?>
                                    <?php endif; ?>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>



                <?php if(false): ?>
                <div class="bill_orders_containers">

                <?php if(is_array($bill_orders) && count($bill_orders)): ?>

                    <?php foreach($bill_orders as $key => $bill_order): ?>
                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">
                            المنتج
                        </div>
                        <div class="panel-body collapse_body collapse">

                            <?php

                            echo generate_select_tags(
                                $field_name="pro_id[]",
                                $label_name="اختار المنتج",
                                $text=convert_inside_obj_to_arr_and_concat($all_products,"pro_name","pro_barcode"),
                                $values=convert_inside_obj_to_arr($all_products,"pro_id"),
                                $selected_value=[$bill_order->pro_id],
                                $class="form-control select_product select_2_class",
                                $multiple="",$required="",$disabled = "",$data = "",$grid="6"
                            );

                            $normal_tags=array("b_o_quantity[]");
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $lang_data="",
                                "yes",
                                $required = "required"
                            );

                            $attrs[0]["b_o_quantity[]"]="الكمية";
                            $attrs[2]["b_o_quantity[]"]="";
                            $attrs[3]["b_o_quantity[]"]="number";
                            $attrs[5]["b_o_quantity[]"].=" check_quantity";


                            $attrs[4]["b_o_quantity[]"] = $bill_order->b_o_quantity;

                            $attrs[6]["b_o_quantity[]"]="6";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>


                            <button class="btn btn-danger delete_order_item">مسح</button>

                        </div>

                    </div>
                    <?php endforeach; ?>

                <?php endif; ?>


                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">
                            المنتج
                        </div>
                        <div class="panel-body collapse_body collapse">

                            <div class="col-md-6" style="margin-top: 30px;">
                                <select name="pro_id[]" class="form-control branch_get_products">
                                    <option value="0">0</option>
                                </select>
                            </div>

                            <?php

//                            echo generate_select_tags(
//                                $field_name="pro_id[]",
//                                $label_name="اختار المنتج",
//                                $text=convert_inside_obj_to_arr_and_concat($all_products,"pro_name","pro_barcode"),
//                                $values=convert_inside_obj_to_arr($all_products,"pro_id"),
//                                $selected_value=array(),
//                                $class="form-control select_product select_2_class",
//                                $multiple="",$required="",$disabled = "",$data = "",$grid="6"
//                            );

                            $normal_tags=array("b_o_quantity[]");
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $lang_data="",
                                "yes",
                                $required = "required"
                            );

                            $attrs[0]["b_o_quantity[]"]="الكمية";
                            $attrs[2]["b_o_quantity[]"]="";
                            $attrs[3]["b_o_quantity[]"]="number";
                            $attrs[5]["b_o_quantity[]"].=" check_quantity";


                            $attrs[6]["b_o_quantity[]"]="6";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>


                            <button class="btn btn-danger delete_order_item">مسح</button>

                        </div>

                    </div>
                </div>
                <a href="#" class="col-md-3 col-md-offset-2 btn btn-info clone_order_item"><i class="fa fa-plus"></i> New Item</a>
                <?php endif; ?>


                {{csrf_field()}}
                <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>

            <?php else: ?>
                {!! get_warning($url = "#" , $text = "لا توجد منتجات حاليه فالمصنع") !!}

            <?php endif; ?>
        </div>

    </div>


@endsection


