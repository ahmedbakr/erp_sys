@extends('admin.main_layout')

@section("subview")

    <div class="">

        <div class="col-md-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    طلبات الفاتورة
                </div>

                <div class="panel-body">
                    <table class="table table-hover table_without_paging">

                        <thead>
                        <tr>
                            <th>#</th>
                            <th>صورة المنتج</th>
                            <th>اسم المنتج</th>
                            <th>الباركود</th>
                            <th>الكمية</th>
                            <th>النوع (شراء - مرتجع)</th>
                            <th>التاريخ</th>
                        </tr>
                        </thead>


                        <tbody>

                        <?php if(isset_and_array($all_bill_orders)): ?>
                            <?php foreach($all_bill_orders as $key=>$order): ?>
                                <tr class="{{($order->b_o_return)?"info":""}}">
                                    <th>{{$key+1}}</th>
                                    <th><img src="{{get_image_or_default($order->pro_img_path)}}" width="100" alt=""></th>
                                    <th>{{$order->pro_name}}</th>
                                    <th>{{$order->pro_barcode}}</th>
                                    <th>{{$order->b_o_quantity}}</th>
                                    <th>{{($order->b_o_return)?"مرتجع":"شراء"}}</th>
                                    <th>{{$order->b_o_date}}</th>
                                </tr>
                            <?php endforeach;?>
                        <?php else: ?>
                            <tr>
                                <th colspan="9">الصراحه مش عارف بس الفاتورة ملهاش طلبات ^_^</th>
                            </tr>

                        <?php endif; ?>

                        </tbody>



                    </table>
                </div>
            </div>



        </div>

    </div>

@endsection


