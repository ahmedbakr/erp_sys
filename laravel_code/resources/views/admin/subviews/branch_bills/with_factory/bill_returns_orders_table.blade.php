
<?php
    $orders = collect($orders);
    $orders = $orders->groupBy("pro_id");
    $counter = 0;

?>

<table class="table table-striped">

    <th>#</th>
    <th>اسم المنتج</th>
    <th>كمية المنتج</th>
    <th>التاريخ</th>

    <?php foreach($orders as $key => $order): ?>
        <tr class="info">
            <td>{{$counter+1}}</td>
            <td>
                <span class="label label-default"><?=$order[0]->pro_name ?></span>
            </td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach($order as $key2 => $value): ?>

            <tr>
                <td></td>
                <td></td>
                <td>
                    <span class="label label-info"><?= $value->b_o_quantity ?></span>
                </td>

                <td>
                    <span class="label label-info">{{$value->b_o_date}}</span>
                    <span class="label label-success">
                        {{\Carbon\Carbon::createFromTimestamp(strtotime($value->b_o_date))->diffForHumans()}}
                    </span>
                </td>
            </tr>

        <?php endforeach; ?>

        <?php $counter++; ?>

    <?php endforeach; ?>

</table>