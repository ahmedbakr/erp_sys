<?php

    $return_orders = collect($return_orders);
    $return_orders = $return_orders->groupBy("pro_id");


?>

<table class="table table-striped">

    <th>#</th>
    <th>اسم المنتج</th>
    <th>الكميلة المطلوبه للمنتج</th>
    <th>الكمية المتاحه لعمل مرتجع</th>
    <th>كمية المنتج في المخزن</th>
    <th>الكمية التي تريد ارجاعها</th>

    <?php foreach($orders as $key => $order): ?>

    <?php
        $available_quantity_on_stock=$branch_product[$order->pro_id][0]->b_p_quantity;
        $order_quantity=$order->b_o_quantity;
        $return_quantity=isset($return_orders[$order->pro_id])?array_sum(convert_inside_obj_to_arr($return_orders[$order->pro_id],"b_o_quantity")):0;

        $max_return_order=0;

        if($available_quantity_on_stock>=$order_quantity){
            $max_return_order=$order_quantity-$return_quantity;
        }
        else{
            if($order_quantity>=$return_quantity){
                $max_return_order=$order_quantity-$return_quantity;
                if($max_return_order>$available_quantity_on_stock){
                    $max_return_order=$available_quantity_on_stock;
                }
            }
            else{
                $max_return_order=0;
            }
        }


    ?>

    <tr>
        <td>{{$key+1}}</td>
        <td>
            <span class="label label-default">
                <?= $order->pro_name ?>
            </span>
        </td>
        <td>
            <span class="label label-info"><?= $order->b_o_quantity ?></span>
        </td>
        <td>
            {{$max_return_order}}
        </td>
        <td>
            <span class="label label-info"><?= $branch_product[$order->pro_id][0]->b_p_quantity ?></span>
        </td>
        <td>
            <input type='number' min='0' max='{{$max_return_order}}' value='0' class='form-control' name='returned_quantity[]' >

            <input type='hidden' value='{{$order->b_o_id}}' class='form-control' name='order_id[]' >
        </td>
    </tr>

    <?php endforeach; ?>

</table>