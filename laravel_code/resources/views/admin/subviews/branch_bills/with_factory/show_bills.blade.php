@extends('admin.main_layout')

@section("subview")

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url('admin/branch_bills/show_all')}}" method="GET">

                <div class="col-md-4 col-md-offset-4">

                    <?php
                        echo
                        generate_select_tags(
                            $field_name="branch_id",
                            $label_name="إختار الفرع",
                            $text=array_merge(["الكل"],convert_inside_obj_to_arr($all_branches,"full_name")),
                            $values=array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id")),
                            $selected_value=(isset($_GET['branch_id'])?[$_GET['branch_id']]:[""]),
                            $class="form-control select_2_class",
                            $multiple="",
                            $required = "",
                            $disabled = "",
                            $data = ""
                        );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" name="start_date" value="{{(isset($_GET["start_date"])?$_GET["start_date"]:"")}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" name="end_date" value="{{(isset($_GET["end_date"])?$_GET["end_date"]:"")}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="">

        <div class="col-md-12">

            <div class="panel panel-info">
                    <div class="panel-heading">
                       الفواتير
                    </div>

                    <div class="panel-body">
                        <table class="table table-hover">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>تاريخ الفاتورة</th>
                                <?php if(check_permission($user_permissions,"branch_orders","show_bill_orders",$current_user)): ?>
                                    <th>طلبات الفاتورة</th>
                                <?php endif; ?>
                                <th>حاله الفاتورة الحالية</th>
                                <?php if(check_permission($user_permissions,"branch_orders","change_bill_status",$current_user)): ?>
                                    <th>تعديل حاله الفاتورة</th>
                                <?php endif; ?>

                            </tr>
                            </thead>


                            <tbody>
                            <?php foreach($all_bills as $key=>$bill): ?>
                            <tr>
                                <th>{{$key+1}}</th>
                                <th>{{$bill->branch_bill_date}}</th>
                                <?php if(check_permission($user_permissions,"branch_orders","show_bill_orders",$current_user)): ?>
                                    <th><a href="{{url("/admin/branch_bills/show_bill_orders/".$bill->branch_bill_id)}}">عرض التفاصيل</a></th>
                                <?php endif; ?>
                                <th>{{$bill->current_status}}</th>
                                <?php if(check_permission($user_permissions,"branch_orders","change_bill_status",$current_user)): ?>
                                <th>
                                    <?php
                                    if(in_array($bill->status_id,[4,5,6])){
                                        echo "لا يمكن تغيير حاله الفاتورة اذا تم الاستلام او الغيت او رفضت";
                                    }
                                    else{
                                        echo
                                        generate_multi_accepters(
                                            $accepturl=url("/admin/branch_bills/change_bill_status"),
                                            $item_obj=$bill,
                                            $item_primary_col="branch_bill_id",
                                            $accept_or_refuse_col="status_id",
                                            $model='App\models\bills\branch_bills_m',
                                            $accepters_data=array_combine(convert_inside_obj_to_arr($all_status,"status_id"),convert_inside_obj_to_arr($all_status,"status_label"))
                                        );
                                    }

                                    ?>
                                </th>
                                <?php endif; ?>
                            </tr>
                            <?php endforeach;?>
                            </tbody>


                            <thead>
                            <tr>
                                <th>#</th>
                                <th>تاريخ الفاتورة</th>
                                <th>طلبات الفاتورة</th>
                                <th>حاله الفاتورة الحالية</th>
                                <th>تعديل حاله الفاتورة</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>

            <?php if(check_permission($user_permissions,"branch_orders","add_action",$current_user)): ?>
                <div class="panel panel-primary">
                        <div class="panel-heading">
                            إضافة فاتورة طلبات منتجات للفرع
                        </div>

                        <div class="panel-body">
                            <a href="{{url("/admin/branch_bills/add_bill")}}">     إضافة فاتورة طلبات منتجات للفرع</a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


    </div>

@endsection


