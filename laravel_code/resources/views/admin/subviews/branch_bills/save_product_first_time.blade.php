@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <?php

            $header_text="تسجيل بضاعه أول مدة يدويا";

            ?>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>تسجيل بضاعه أول مدة من ملف اكسيل</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("/admin/branch_bills/import_products_first_time_to_stock")?>" method="POST" enctype="multipart/form-data">

                            <div class="alert alert-info" style="font-weight: bold;">
                                <h3 style="text-align: center;">برجاء الإنتباه قبل تحميل ملف الاكسيل برجاء اتباع ما يلي :-</h3>
                                <ul>
                                    <li> برجاء تحميل هذا الملف وملئ الصفوف قبل الرفع <a href="{{url('uploads/excel_templates/products_first_time_template.xlsx')}}" target="_blank">التحميل من هنا</a></li>
                                    <li>يجيب ان يكون إمتداد الملف واحد من هؤلاء ["xls",".XLS",".xlsx",".XLSX.")</li>
                                    <li>يجب ترك اول صف كما هو دون تغيير او مسح او تعديل اي عمود</li>
                                    <li>برجاء كتابه باركود المنتج كما مسجل في بيانات المنتجات من قبل والا سيعتبر منتج غير موجود</li>
                                    <li>يجب عدم إدخال المنتج مرتين وتكرارة او عدم دخول منتج سبق ادخاله من قبل</li>
                                    <li>بعد الحفظ سيتم عرض بيانات المنتجات الخطأ والمكررة والمدخلة مسبقا وعرض المنتجات الصالحه للإدخال ويمكنك التعديل علي الكميات وحد الطلب او مسح اي منتج</li>
                                </ul>

                            </div>

                            <div class="col-md-6">
                                <?php
                                echo generate_select_tags(
                                    $field_name="branch_id",
                                    $label_name="إختار الفرع *",
                                    $text=array_merge(convert_inside_obj_to_arr($all_branches,"full_name")),
                                    $values=array_merge(convert_inside_obj_to_arr($all_branches,"user_id")),
                                    $selected_value=[""],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>


                            <div class="col-md-6">
                                <?php

                                echo generate_img_tags_for_form(
                                    $filed_name="excel_file",
                                    $filed_label="excel_file",
                                    $required_field="",
                                    $checkbox_field_name="pro_excel_checkbox",
                                    $need_alt_title="no",
                                    $required_alt_title="",
                                    $old_path_value="",
                                    $old_title_value="",
                                    $old_alt_value="",
                                    $recomended_size="",
                                    $disalbed="",
                                    $displayed_img_width="100",
                                    $display_label="رفع ملف اكسيل للمنتجات",
                                    $img_obj = ""
                                );

                                ?>
                            </div>

                            {{csrf_field()}}

                            <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg" style="margin-top: 5px;">

                        </form>
                    </div>
                </div>
            </div>

            <?php if( isset($input_arr) &&( count($input_arr) || count($inserted_before_pros_barcode_arr) ||
                count($not_found_pros_barcode_arr) || count($duplicate_pros_barcode_arr))): ?>

            <div class="panel panel-success">
                <div class="panel-heading"><h2>مراجعه قبل الحفظ</h2></div>
                <div class="panel-body">
                    <div class="row">

                        <div class="alert alert-info" style="text-align: center;">
                            <b>إجمالي المنتجات الصحيحة :</b> <span class="label label-success">{{count($input_arr)}}</span> <br>
                            <b>إجمالي المنتجات المدخلة مسبقا :</b> <span class="label label-success">{{count($inserted_before_pros_barcode_arr)}}</span> <br>
                            <b>إجمالي المنتجات الغير موجودة لخطأ الباركود :</b> <span class="label label-success">{{count($not_found_pros_barcode_arr)}}</span> <br>
                            <b>إجمالي المنتجات المكررة في ملف الاكسيل :</b> <span class="label label-success">{{count($duplicate_pros_barcode_arr)}}</span> <br>
                        </div>

                        <?php if(count($inserted_before_pros_barcode_arr)): ?>
                            <div class="col-md-6">
                                <?php
                                    $header = "تفاصيل المنتجات المدخله مسبقا";
                                    $barcode_arr = $inserted_before_pros_barcode_arr;
                                ?>
                                @include('admin.subviews.branch_bills.blocks.show_barcode_pros_table')
                            </div>
                        <?php endif; ?>


                        <?php if(count($not_found_pros_barcode_arr)): ?>
                            <div class="col-md-6">
                                <?php
                                    $header = "تفاصيل المنتجات الغير موجودة لخطأ الباركود";
                                    $barcode_arr = $not_found_pros_barcode_arr;
                                ?>
                                @include('admin.subviews.branch_bills.blocks.show_barcode_pros_table')
                            </div>
                        <?php endif; ?>

                        <?php if(count($duplicate_pros_barcode_arr)): ?>
                            <div class="col-md-6">
                                <?php
                                    $header = "تفاصيل المنتجات المكررة في ملف الاكسيل";
                                    $barcode_arr = $duplicate_pros_barcode_arr;
                                ?>
                                @include('admin.subviews.branch_bills.blocks.show_barcode_pros_table')
                            </div>
                        <?php endif; ?>

                        <?php if(count($input_arr)): ?>
                            <div class="col-md-12">
                                <input type="hidden" class="origin_imported_arr" value="{{htmlentities(json_encode($input_arr),ENT_QUOTES,'UTF-8')}}">
                                @include('admin.subviews.branch_bills.blocks.show_barcode_pros_table_with_edit')
                            </div>
                        <?php endif; ?>


                    </div>
                </div>
            </div>


            <?php else: ?>

            <div class="panel panel-info">
                <div class="panel-heading"><h2><?=$header_text?></h2></div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/branch_bills/save_product_first_time")?>" method="POST" enctype="multipart/form-data">

                            <div class="col-md-6">
                                <?php
                                echo generate_select_tags(
                                    $field_name="branch_id",
                                    $label_name="إختار الفرع *",
                                    $text=array_merge(["إختار"],convert_inside_obj_to_arr($all_branches,"full_name")),
                                    $values=array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id")),
                                    $selected_value=[""],
                                    $class="form-control select_2_class select_branch_to_get_products",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>

                            <div class="col-md-6">
                                <?php
                                echo generate_select_tags(
                                    $field_name="pro_id",
                                    $label_name="إختار المنتج *",
                                    $text=[],
                                    $values=[],
                                    $selected_value=[""],
                                    $class="form-control select_2_class show_products_list",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "disabled",
                                    $data = ""
                                );
                                ?>
                            </div>


                            <?php

                            $normal_tags=array("b_p_quantity","b_p_quantity_limit","b_p_date");

                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $user_data="",
                                "yes",
                                $required ="required"
                            );

                            $attrs[0]["b_p_quantity"]="الكمية *";
                            $attrs[0]["b_p_quantity_limit"]="حد الطلب *";
                            $attrs[0]["b_p_date"]="التاريخ *";


                            $attrs[3]["b_p_quantity"]="number";
                            $attrs[3]["b_p_quantity_limit"]="number";
                            $attrs[3]["b_p_date"]="date_time";

                            $attrs[4]["b_p_date"]=date("Y-m-d H:i:s");

                            $attrs[6]["b_p_quantity"]="4";
                            $attrs[6]["b_p_quantity_limit"]="4";
                            $attrs[6]["b_p_date"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>

                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                    </div>
                </div>

            </div>

            <?php endif; ?>

        </div>
    </div>

@endsection
