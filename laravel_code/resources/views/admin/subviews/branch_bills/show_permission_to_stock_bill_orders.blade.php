@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading">
            منتجات الأذن
        </div>
        <div class="panel-body">
            <table id="cat_table_1" class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم المنتج</td>
                    <td>الكمية</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم المنتج</td>
                    <td>الكمية</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($orders as $key => $order): ?>

                <tr id="row<?= $order->permission_bill_order_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$order->pro_name}}</td>
                    <td>{{$order->order_pro_quantity}}</td>

                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"add_permission_to_stock","add_action",$current_user)): ?>
                <div class="col-md-3 col-md-offset-4">
                    <a href="{{url("/admin/branch_bills/add_permission_to_stock/add")}}" class="btn btn-primary">إضافة إذن إضافة</a>
                </div>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"get_permission_from_stock","add_action",$current_user)): ?>
                <div class="col-md-3">
                    <a href="{{url("/admin/branch_bills/add_permission_to_stock/get")}}" class="btn btn-primary">إضافة إذن صرف</a>
                </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
