@extends('admin.main_layout')

@section("subview")

    <script src="{{url("/public_html/jscode/admin/branch_bills.js")}}"></script>

    <?php

        if ($permission_type== "add")
        {
            $header = " إذن إضافة";
        }
        else if ($permission_type== "get")
        {
            $header = " إذن صرف";
        }

    ?>

    <div class="add_branch_bill_form add_permission_to_stock_parent_div">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {{$header}}
                    </h3>
                </div>
            </div>

            <?php if(count($all_products)): ?>

                <input type="hidden" class="products_quantities_in_factory" value="{{json_encode(array_combine(convert_inside_obj_to_arr($all_products,"pro_id"),convert_inside_obj_to_arr($all_products,"pro_quantity")))}}">


                <form id="save_form" action="<?=url("/admin/branch_bills/add_permission_to_stock/$permission_type")?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">بيانات الإذن</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <?php

                                        $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                                        $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                                        if ($current_user->user_type == "admin")
                                        {
                                            $branches_text = array_merge(["الفرع الرئيسي"],convert_inside_obj_to_arr($all_branches,"full_name"));
                                            $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
                                        }

                                        echo
                                        generate_select_tags(
                                            $field_name="branch_id",
                                            $label_name="إختار الفرع",
                                            $text=$branches_text,
                                            $values=$branches_values,
                                            $selected_value=[""],
                                            $class="form-control select_2_class",
                                            $multiple="",
                                            $required = "",
                                            $disabled = "",
                                            $data = ""
                                        );
                                        ?>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="">تاريخ الإذن</label>
                                        <input type="date" required name="permission_date" value="{{(!empty($bill_date))?$bill_date:date('Y-m-d')}}" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary pro_advanced_search">
                            <div class="panel-heading" data-toggle="collapse" href=".choose_product" style="cursor:pointer;">
                                اضف منتج عن طريق الاختيار
                            </div>
                            <div class="panel-body choose_product collapse">

                                <div class="col-md-9 load_cats_div">
                                    <div class="row">
                                        <button class="btn btn-primary show_parent_0_cats"> اظهر التصنيفات الرئيسية</button>
                                    </div>


                                    {!! $cats_view !!}
                                </div>

                                <div class="col-md-3" style="border-right: 1px solid #CCC;margin-bottom: 10px;">
                                    <div class="form-group">
                                        <label for="">ابحث بواسطة الباركود او الاسم</label>
                                        <input type="text"  class="form-control search_for_product_by_name_or_barcode">
                                    </div>
                                </div>

                                <hr>

                                <div class="col-md-12 load_products_div">

                                    {!! $products_view !!}
                                </div>

                                <div class="col-md-12 search_res">

                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">المنتجات</div>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped orders_table">
                                    <tr>
                                        <th>المنتج</th>
                                        <th>الكمية</th>
                                        <th>حذف</th>
                                    </tr>
                                </table>

                                <div class="all_pros_count"></div>
                            </div>
                        </div>
                    </div>

                </div>


                {{csrf_field()}}
                <?php if(false): ?>
                <a href="#" class="col-md-3 col-md-offset-2 btn btn-info clone_order_item"><i class="fa fa-plus"></i> New Item</a>
                <?php endif; ?>

                <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>

            <?php else: ?>
                {!! get_warning($url = "#" , $text = "لا توجد منتجات حاليه فالمصنع") !!}

            <?php endif; ?>
        </div>

    </div>


@endsection


