@extends('admin.main_layout')

@section('subview')

    <?php if(is_array($all_branches) && count($all_branches)): ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url("admin/branch_bills/show_permission_to_stock_bills/$permission_type")}}" method="GET">

                <div class="col-md-4">

                    <?php

                    $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                    $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $branches_text = array_merge(["الفرع الرئيسي"],convert_inside_obj_to_arr($all_branches,"full_name"));
                        $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=(isset($_GET['branch_id']))?[$_GET['branch_id']]:[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-4 form-group">
                    <label for="">من التاريخ</label>
                    <input type="date" class="form-control" name="from_date" value="{{(isset($_GET['from_date']))?$_GET['from_date']:""}}">
                </div>

                <div class="col-md-4 form-group">
                    <label for="">الي التاريخ</label>
                    <input type="date" class="form-control" name="to_date" value="{{(isset($_GET['to_date']))?$_GET['to_date']:""}}">
                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info ">ابحث</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <?php endif; ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات أذون
        <?php if($permission_type == "add"): ?>
        الإضافة
            <?php else: ?>
            الصرف
        <?php endif; ?>
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اسم المستخدم</td>
                    <td>التاريخ</td>
                    <td>مشاهدة المنتجات</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اسم المستخدم</td>
                    <td>التاريخ</td>
                    <td>مشاهدة المنتجات</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($bills as $key => $bill): ?>

                <tr id="row<?= $bill->permission_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        <?php if($bill->branch_id > 0): ?>
                        {{$bill->branch_full_name}}
                            <?php else: ?>
                            الفرع الرئيسي
                        <?php endif; ?>
                    </td>
                    <td>{{$bill->user_full_name}}</td>
                    <td>{{$bill->permission_date}}</td>

                    <td>
                        <a href="{{url("admin/branch_bills/show_permission_to_stock_bill_orders/$permission_type/$bill->permission_id")}}">مشاهدة</a>
                    </td>

                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"add_permission_to_stock","add_action",$current_user) && $permission_type == "add"): ?>
                <div class="col-md-3 col-md-offset-4">
                    <a href="{{url("/admin/branch_bills/add_permission_to_stock/add")}}" class="btn btn-primary">إضافة إذن إضافة بضاعة</a>
                </div>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"get_permission_from_stock","add_action",$current_user) && $permission_type == "get"): ?>
                <div class="col-md-3">
                    <a href="{{url("/admin/branch_bills/add_permission_to_stock/get")}}" class="btn btn-primary">إضافة إذن صرف بضاعة</a>
                </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
