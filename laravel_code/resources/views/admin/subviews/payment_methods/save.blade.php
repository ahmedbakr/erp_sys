@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <?php

            $header_text="تسجيل طريقة جديدة";

            $id = "";

            if ($payment != "")
            {
                $header_text="تعديل ".$payment->{'payment_method_name'.$en};
                $id = $payment->payment_method_id;
            }

            ?>



            <div class="panel panel-info">
                <div class="panel-heading"><h2><?=$header_text?></h2></div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/payment_methods/save/$id")?>" method="POST" enctype="multipart/form-data">

                            <?php

                            $normal_tags=array(
                                "payment_method_name","payment_method_name_en" , "atm_ratio"
                            );

                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $user_data=$payment,
                                "yes",
                                $required = "required"
                            );

                            $attrs[0]["payment_method_name"]="الاسم بالعربي *";
                            $attrs[0]["payment_method_name_en"]="الاسم بالإنجليزي *";
                            $attrs[0]["atm_ratio"]="نسبة الخصم من البنك في حالة طريقة السداد شبكة";


                            $attrs[3]["payment_method_name"]="text";
                            $attrs[3]["payment_method_name_en"]="text";
                            $attrs[3]["atm_ratio"]="number";

                            $attrs[6]["payment_method_name"]="6";
                            $attrs[6]["payment_method_name_en"]="6";
                            $attrs[6]["atm_ratio"]="6";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>

                            <?php
                            echo
                            generate_select_tags(
                                $field_name="is_payment_method_atm",
                                $label_name="إختار النوع *",
                                $text=["كاش","شبكة"],
                                $values=[0,1],
                                $selected_value="",
                                $class="form-control",
                                $multiple="",
                                $required = "",
                                $disabled = "",
                                $data = $payment,
                                $grid="6"
                            );

                            ?>


                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection
