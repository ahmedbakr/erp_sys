@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
أنواع السداد
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالإنجليزي</td>
                    <td>النوع</td>
                    <?php if(check_permission($user_permissions,"factory/payment_methods","add_action",$current_user)): ?>
                    <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالإنجليزي</td>
                    <td>النوع</td>
                    <?php if(check_permission($user_permissions,"factory/payment_methods","add_action",$current_user)): ?>
                    <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_payments as $key => $payment): ?>
                <tr id="row<?= $payment->payment_method_id ?>">
                    <td><?=$key+1?></td>
                    <td><?=$payment->payment_method_name ?></td>
                    <td><?=$payment->payment_method_name_en ?></td>
                    <td>
                        <?php if($payment->is_payment_method_atm == 0): ?>
                        كاش
                            <?php else: ?>
                            شبكة
                        <?php endif; ?>
                    </td>
                    <?php if(check_permission($user_permissions,"factory/payment_methods","add_action",$current_user)): ?>
                    <td>
                        <span class="label label-info">
                            <a href="{{url('admin/payment_methods/save/'.$payment->payment_method_id)}}">تعديل</a>
                        </span>
                    </td>
                    <?php endif; ?>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"factory/payment_methods","add_action",$current_user)): ?>
            <div class="row">
                <a href="{{url('admin/payment_methods/save')}}" class="btn btn-info col-md-3 col-md-offset-4">تسجيل طريقة جديدة</a>
            </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
