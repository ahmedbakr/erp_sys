@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">الفتلرة</div>
        <div class="panel-body">
            <form action="" method="get">
                <div class="form-group">
                    <label for="">رقم العميل</label>
                    <input type="text" class="form-control" name="user_tel" value="{{isset($all_post_data->user_tel)?$all_post_data->user_tel:""}}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">ابحث</button>
                </div>
            </form>
        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            مبيعات العملاء
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <td>#</td>
                    <th>الفرع</th>
                    <th>العميل</th>
                    <th>رقم الفاتورة</th>
                    <th>المنتج</th>
                    <th>سعر المنتج عند البيع</th>
                    <th>الكمية</th>
                    <th>شراء او مرتجع</th>
                    <th>الاجمالي</th>
                </tr>
                </thead>



                <tbody>
                <?php foreach ($all_client_bills_orders as $key => $order): ?>
                    <?php
                        if(!isset($all_client_bills[$order->client_bill_id]))continue;
                    ?>
                    <tr>
                        <td></td>
                        <td>{{$all_client_bills[$order->client_bill_id][0]->branch_full_name}}</td>
                        <td>
                            {{$all_client_bills[$order->client_bill_id][0]->client_full_name}}
                            <br>
                            {{$all_client_bills[$order->client_bill_id][0]->client_user_tel}}
                        </td>
                        <td>
                            {{$order->client_bill_id}}
                        </td>
                        <td>
                            {{$order->pro_name}}
                            <br>
                            {{$order->pro_barcode}}
                        </td>
                        <td>
                            {{$order->pro_sell_price}}
                        </td>
                        <td>
                            {{$order->order_quantity}}
                        </td>
                        <th>{{($order->order_return=="1"?"مرتجع":"شراء")}}</th>
                        <td>
                            {{$order->pro_sell_price*$order->order_quantity}}
                        </td>
                    </tr>
                <?php endforeach ?>


                </tbody>

            </table>


        </div>
    </div>


@endsection

