@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <div class="col-md-12">

                    <?php

                    $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                    $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $branches_text = array_merge(["كل الافرع"],convert_inside_obj_to_arr($all_branches,"full_name"));
                        $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="from_branch",
                        $label_name="من الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "6"
                    );

                    echo
                    generate_select_tags(
                        $field_name="to_branch",
                        $label_name="الي الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "6"
                    );

                    ?>



                </div>

                <div class="col-md-12">

                    <div class="form-group col-md-4">
                        <label for="">من</label>
                        <input type="date" name="start_date" class="form-control" value="{{(isset($all_post_data->start_date))?$all_post_data->start_date:""}}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">الي</label>
                        <input type="date" name="end_date" class="form-control" value="{{(isset($all_post_data->end_date))?$all_post_data->end_date:""}}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">باركود|اسم المنتج</label>
                        <input type="text" name="pro_search" class="form-control" value="{{(isset($all_post_data->pro_search))?$all_post_data->pro_search:""}}">
                    </div>

                </div>


                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">تحويلات الفروع</div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>من</td>
                        <td>الي</td>
                        <td>اسم وباركود المنتج</td>
                        <td>الكمية</td>
                        <td>التاريخ</td>
                    </tr>
                </thead>

                <?php
                    $branch_transfer=collect($branch_transfer)->groupBy("b_t_id")->all();
                ?>

                <tbody>
                    <?php foreach($branch_transfer_products as $key=>$pro): ?>
                        <?php
                            if(!isset($branch_transfer[$pro->b_t_id])){
                                continue;
                            }

                            $from_branch_id=$branch_transfer[$pro->b_t_id][0]->from_branch;
                            $to_branch_id=$branch_transfer[$pro->b_t_id][0]->to_branch;
                        ?>
                        <tr>
                            <td>#</td>
                            <td>{{$all_branches_grouped_by_id[$from_branch_id][0]->full_name}}</td>
                            <td>{{$all_branches_grouped_by_id[$to_branch_id][0]->full_name}}</td>
                            <td>
                                <label class="label label-default">{{$pro->pro_name}}</label>
                                <label class="label label-default">{{$pro->pro_barcode}}</label>
                            </td>
                            <td>{{$pro->transfer_pro_quantity}}</td>
                            <td>{{$branch_transfer[$pro->b_t_id][0]->created_at}}</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>


@endsection