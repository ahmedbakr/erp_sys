@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">
            الفلترة
        </div>
        <div class="panel-body">
            <form action="{{url("/admin/reports/best_sell_bills_report")}}" method="get">


                <?php

                $normal_tags=[
                    "start_date","end_date"
                ];


                $attrs = generate_default_array_inputs_html(
                    $normal_tags,
                    $post_data="",
                    "yes",
                    "",
                    "4"
                );

                $attrs[0]["start_date"]="من ";
                $attrs[0]["end_date"]="إلي ";


                $attrs[3]["start_date"]="date";
                $attrs[3]["end_date"]="date";

                $attrs[6]["start_date"]="6";
                $attrs[6]["end_date"]="6";

                $attrs[4]["start_date"]=(isset($_GET['start_date']) && !empty($_GET['start_date']))?$_GET['start_date']:date('Y-m-d',time());
                $attrs[4]["end_date"]=(isset($_GET['end_date']) && !empty($_GET['end_date']))?$_GET['end_date']:date('Y-m-d',time());

                echo
                generate_inputs_html(
                    reformate_arr_without_keys($attrs[0]),
                    reformate_arr_without_keys($attrs[1]),
                    reformate_arr_without_keys($attrs[2]),
                    reformate_arr_without_keys($attrs[3]),
                    reformate_arr_without_keys($attrs[4]),
                    reformate_arr_without_keys($attrs[5]),
                    reformate_arr_without_keys($attrs[6])
                );
                ?>

                <button class="btn btn-primary" style="margin-top: 27px;">ابحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            النتائج
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <?php
            $all_branches=collect($all_branches)->groupBy("user_id")->all();
            ?>


            <table class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <?php for($ind = 1 ;$ind<=24; $ind ++): ?>
                        <td>
                            {{$ind}}
                            {{($ind < 12)? 'ص' : 'م'}}
                        </td>
                    <?php endfor; ?>
                    <td>الاجمالي</td>
                </tr>
                </thead>


                <tbody>

                <?php
                    $total_per_hour = [];
                    $total = 0;
                ?>
                <?php foreach($all_bills as $branch_name => $bills): ?>
                <tr>

                    <td></td>
                    <td>{{$branch_name}}</td>
                    <?php
                        $temp_sum = 0;
                    ?>
                    <?php for($ind = 1 ;$ind<=24; $ind ++): ?>
                    <td>
                        <?php
                            if(isset($bills[$ind]))
                            {
                                echo $bills[$ind]['total_bills_amount'];
                                $temp_sum += $bills[$ind]['total_bills_amount'];

                                if(isset($total_per_hour[$ind]))
                                {
                                    $total_per_hour[$ind] += $bills[$ind]['total_bills_amount'];
                                }
                                else{
                                    $total_per_hour[$ind] = $bills[$ind]['total_bills_amount'];
                                }

                            }
                            else{
                                echo 0;
                            }
                        ?>
                    </td>
                    <?php endfor; ?>

                    <td>{{$temp_sum}}</td>
                    <?php $total += $temp_sum; ?>
                </tr>
                <?php endforeach;?>

                <tr>

                    <td></td>
                    <td>إجمالي الفروع</td>
                    <?php for($ind = 1 ;$ind<=24; $ind ++): ?>
                    <td>
                        <?php
                            if(isset($total_per_hour[$ind]))
                            {
                                echo $total_per_hour[$ind];
                            }
                            else{
                                echo 0;
                            }
                        ?>
                    </td>
                    <?php endfor; ?>
                    <td>{{$total}}</td>
                </tr>


                </tbody>

            </table>

        </div>
    </div>


@endsection