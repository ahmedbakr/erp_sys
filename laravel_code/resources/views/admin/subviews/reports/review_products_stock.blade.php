@extends('admin.main_layout')

@section('subview')

    @include("common.category.accounts_tree.tree_modal")

    <style>
        .general_self_edit{
            display: block;
        }
        .general_self_edit:hover{
            border: 2px dashed #0000cc;
        }
    </style>


    <div class="panel panel-info">
        <div class="panel-heading">العمليات</div>
        <div class="panel-body">
            <form action="{{url("/admin/reports/review_products_stock")}}" method="get">

                <?php


                if($current_user->user_type == "admin")
                {
                    $text_arr = ["الفرع الرئيسي","كل المعارض"];
                    $values_arr = [-1,0];
                }
                else{
                    $text_arr = ["المعرض الخاص بي"];
                    $values_arr = [0];
                }

                $branches_text = array_merge($text_arr,convert_inside_obj_to_arr($all_branches,"full_name"));
                $branches_values = array_merge($values_arr,convert_inside_obj_to_arr($all_branches,"user_id"));

                echo generate_select_tags(
                    $field_name="branch_id",
                    $label_name="اختار الفرع",
                    $text=$branches_text,
                    $values=$branches_values,
                    $selected_value=[$branch_id],
                    $class="form-group select_2_class",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="6"
                );

                echo generate_select_tags(
                    $field_name="price_id",
                    $label_name="اختار الأسعار",
                    $text=convert_inside_obj_to_arr($prices_list,"price_title"),
                    $values=convert_inside_obj_to_arr($prices_list,"price_id"),
                    $selected_value=$price_ids,
                    $class="form-group select_2_class",
                    $multiple="multiple",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="6"
                );

                ?>

                <div class="col-md-4" style="margin-top: 20px;">
                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="single"
                            data-div_to_show_selection=".select_cats"
                            data-input_field_name="cat_ids"
                    >
                        اظهر الشجرة لتختار التصنيفات
                    </button>

                    <div class="select_cats" style="text-align: center;">

                    </div>
                </div>

                <div class="col-md-2" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-primary">ابحث</button>
                </div>

            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">{{$meta_title}}</div>
        <div class="panel-body" style="overflow-x: scroll;">

            <?php if(false && count($all_products)): ?>
            <div class="row">
                <?php

                $get_values = "branch_id=$branch_id";

                ?>
                <a href="{{url("admin/reports/export_products_on_branches_stock?$get_values")}}" class="btn btn-info">إستيراد إكسيل</a>
            </div>
            <?php endif; ?>

            <?php if(false): ?>
                <div class="row toggle_columns" style="margin: 0 50px 50px 0;">
                    إخفاء أو إظهار عمود:
                    <?php foreach($table_headers as $key => $header): ?>
                    <a class="toggle-vis" style="cursor: pointer;text-decoration: none;" data-column="{{$key}}">
                        <span class="label label-default">{{$header}}</span>
                    </a>
                    <?php if($key < (count($table_headers))-1): ?>
                    -
                    <?php endif; ?>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <?php $td_count = count($table_headers); ?>
                <tr>
                    <?php foreach($table_headers as $key => $header): ?>
                        <td>{{$header}}</td>
                    <?php endforeach; ?>
                </tr>
                </thead>


                <tbody>
                <?php if(count($all_products)): ?>

                <?php $total = []; ?>
                <?php foreach ($all_products as $key => $product): ?>
                    <tr>
                        <?php foreach($table_headers as $key2 => $header): ?>
                        <td>{{$product[$key2]}}</td>
                        <?php
                            if($key2 > 4)
                            {
                                if(isset($total[$key2]))
                                {
                                    $total[$key2] = $total[$key2]+$product[$key2];
                                }
                                else{
                                    $total[$key2] = $product[$key2];
                                }
                            }
                        ?>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?php foreach($total as $key => $value): ?>
                        <td>
                            <span class="label label-info">{{$value}}</span>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endif; ?>

                </tbody>

            </table>

        </div>
    </div>


@endsection