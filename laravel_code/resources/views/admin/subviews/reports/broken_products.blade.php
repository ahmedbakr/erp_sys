@extends('admin.main_layout')

@section('subview')

    @include("common.category.accounts_tree.tree_modal")

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="{{url('admin/reports/broken_products')}}" method="get">

                <div class="row">

                <?php

                $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                if ($current_user->user_type == "admin")
                {
                    $branches_text = array_merge(["الفرع الرئيسي","كل الفروع"],convert_inside_obj_to_arr($all_branches,"full_name"));
                    $branches_values = array_merge([-1,0],convert_inside_obj_to_arr($all_branches,"user_id"));
                }

                echo
                generate_select_tags(
                    $field_name="branch_id",
                    $label_name="إختار الفرع",
                    $text=$branches_text,
                    $values=$branches_values,
                    $selected_value=(isset($_GET['branch_id']))?[$_GET['branch_id']]:[""],
                    $class="form-control select_2_class",
                    $multiple="",
                    $required = "",
                    $disabled = "",
                    $data = "",
                    "4"
                );


                echo generate_select_tags(
                    $field_name="price_id",
                    $label_name="اختار السعر لتحديد خسائر المنتجات",
                    $text=convert_inside_obj_to_arr($prices_list,"price_title"),
                    $values=convert_inside_obj_to_arr($prices_list,"price_id"),
                    $selected_value=$price_ids,
                    $class="form-control select_2_class",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="4"
                );

                ?>

                <div class="form-group col-md-4"></div>

                <div class="form-group col-md-4">
                    <label for="">من</label>
                    <input type="date" name="from_date" class="form-control" value="{{(isset($_GET['from_date']))?$_GET['from_date']:""}}">
                </div>

                <div class="form-group col-md-4">
                    <label for="">الي</label>
                    <input type="date" name="to_date" class="form-control" value="{{(isset($_GET['to_date']))?$_GET['to_date']:""}}">
                </div>

                <div class="form-group col-md-4" style="margin-top: 20px;">
                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="single"
                            data-div_to_show_selection=".select_cats"
                            data-input_field_name="cat_ids"
                    >
                        اظهر الشجرة لتختار التصنيفات
                    </button>

                    <div class="select_cats" style="text-align: center;">

                    </div>
                </div>

                </div>
                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">المنتجات الهالكه</div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <?php $td_count = count($table_headers); ?>
                <tr>
                    <?php foreach($table_headers as $key => $header): ?>
                    <td>{{$header}}</td>
                    <?php endforeach; ?>
                </tr>
                </thead>


                <tbody>
                <?php if(count($all_products)): ?>

                <?php
                $total_quantity = 0;
                $total_cost = 0;
                ?>
                <?php foreach ($all_products as $key => $product): ?>
                <tr>
                    <?php foreach($table_headers as $key2 => $header): ?>
                    <td>{{$product[$key2]}}</td>
                    <?php
                    if($key2 == 4)
                    {
                        $total_quantity = $total_quantity + $product[$key2];
                    }
                    else if($key2 == 6)
                    {
                        $total_cost = $total_cost + $product[$key2];
                    }
                    ?>
                    <?php endforeach; ?>

                </tr>
                <?php endforeach ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><span class="label label-info">{{$total_quantity}}</span></td>
                    <td></td>
                    <?php if($td_count == 7): ?>
                    <td><span class="label label-info">{{$total_cost}}</span></td>
                    <?php endif; ?>
                </tr>
                <?php endif; ?>

                </tbody>

            </table>

        </div>
    </div>



@endsection