@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <?php
                $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                if ($current_user->user_type == "admin")
                {
                    $branches_text = array_merge(["كل الافرع"],convert_inside_obj_to_arr($all_branches,"full_name"));
                    $branches_values = array_merge([""],convert_inside_obj_to_arr($all_branches,"user_id"));
                }

                echo
                generate_select_tags(
                    $field_name="branch_id",
                    $label_name="إختار الفرع",
                    $text=$branches_text,
                    $values=$branches_values,
                    $selected_value=[""],
                    $class="form-control select_2_class",
                    $multiple="",
                    $required = "",
                    $disabled = "",
                    $data = $all_post_data,
                    "4"
                );

                echo
                generate_select_tags(
                    $field_name="age_range",
                    $label_name="الفئة العمرية",
                    $text=["الكل","15-25","26-40","41-60","العمر>60"],
                    $values=[0,"15-25","26-40","41-60","العمر>60"],
                    $selected_value="",
                    $class="form-control",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = $all_post_data,
                    $grid="4",
                    true
                );


                echo
                generate_select_tags(
                    $field_name="user_gender",
                    $label_name="نوع العميل",
                    $text=["الكل","ذكر","انثي"],
                    $values=[0,"male","female"],
                    $selected_value="",
                    $class="form-control",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = $all_post_data,
                    $grid="4",
                    true
                );

                ?>

                <button type="submit" class="btn btn-primary">ابحث</button>

            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            العملاء
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <th>#</th>
                    <th>الفرع</th>
                    <th>عدد الزيارات</th>
                    <th>كود العميل</th>
                    <th>اسم العميل</th>
                    <th>ايميل العميل</th>
                    <th>تليفون العميل</th>
                    <th>موبايل العميل</th>
                    <th>فاكس العميل</th>
                    <th>تاريخ بدء التعامل العميل</th>
                    <th>عنوان العميل</th>
                    <th>سعر العميل</th>
                    <th>الفئة العمرية للعميل</th>
                    <th>نوع العميل</th>
                    <th>عدد العملاء الحاليين من طرفه</th>
                    <th>مقدار ما دفعه</th>
                    <th>مقدار ما استلمه</th>
                    <th>المتبقي له</th>
                    <th>المتبقي عليه</th>
                    <th>عدد العملاء من طرفة</th>
                    <th>تاريخ تسجيل العميل</th>
                    <th>تاريخ آخر فاتورة</th>
                </tr>
                </thead>



                <tbody>
                <?php foreach ($all_clients as $key => $user): ?>
                <tr id="row<?= $user->user_id ?>">

                    <td><?=$key+1?></td>
                    <td>
                        <?php
                            if(isset($all_branches_grouped_by_id[$user->related_id])){
                                echo $all_branches_grouped_by_id[$user->related_id][0]->full_name;
                            }
                        ?>
                    </td>
                    <td>
                        {{(isset($get_client_bills[$user->user_id]))?(count($get_client_bills[$user->user_id]->all())):0}}
                    </td>
                    <td><?=$user->user_id ?></td>
                    <td>
                        <?=$user->full_name ?>
                        <br>
                        <?=$user->full_name_en ?>
                    </td>
                    <td><?=$user->email ?></td>
                    <td><?=$user->user_tel ?></td>
                    <td><?=$user->user_mobile ?></td>
                    <td><?=$user->user_fax ?></td>
                    <td><?=$user->start_join_date ?></td>
                    <td><?=$user->user_address ?></td>
                    <td><?=$user->client_price_title ?></td>
                    <td><?=$user->age_range ?></td>
                    <td><?= ($user->user_gender == "male")?"ذكر":"أنثي" ?></td>
                    <td>
                        {{$user->user_as_sponsor_count}}
                    </td>

                    <td>
                        {{$user->total_payed_cash + $user->total_payed_atm}}
                    </td>

                    <td>
                        {{$user->total_received_cash + $user->total_received_atm}}
                    </td>

                    <td>{{$user->total_remain_for_him}}</td>
                    <td>{{$user->total_remain_for_branch}}</td>

                    <td>
                        {{$user->user_as_sponsor_count}}
                    </td>

                    <td>{{$user->created_at}}</td>
                    <td>{{$user->last_bill_date}}</td>


                </tr>
                <?php endforeach ?>


                </tbody>

            </table>


        </div>
    </div>


@endsection
