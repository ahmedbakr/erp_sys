@extends('admin.main_layout')

@section('subview')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{url('public_html/jscode/admin/reports.js')}}"></script>



    <div class="panel panel-info">
        <div class="panel-heading">
            إجمالي المبيعات حسب تاريخ الجلسه
        </div>
        <div class="panel-body">

            <?php $url = url('admin/reports/sell_bills_session'); ?>
            @include('admin.subviews.reports.blocks.session_filteration')



            <?php if(count($branch_details)): ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    بيانات عن الفرع
                </div>
                <div class="panel-body">

                    <table class="table table-striped"  style="direction: rtl;">
                        <tr>
                            <td>اسم الفرع</td>
                            <td>
                                <label class="label label-default">{{$branch_details["branch_name"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>الرصيد الحالي للمبيعات</td>
                            <td>
                                <label class="label label-default">{{$branch_details["branch_balance"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>رصيد العهد للفرع</td>
                            <td>
                                <label class="label label-default">{{$branch_details["branch_duty_balance"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>عدد الكاشيير</td>
                            <td>
                                <label class="label label-info">{{$branch_details["number_of_cashiers"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>عدد العملاء</td>
                            <td>
                                <label class="label label-info">{{$branch_details["number_of_clients"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>عدد نقاط البيع</td>
                            <td>
                                <label class="label label-primary">{{$branch_details["number_of_point_of_sale"]}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>محصلة أرصدة نقاط البيع</td>
                            <td>
                                <label class="label label-primary">{{$branch_details["total_balance_of_point_of_sale"]}}</label>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <?php endif; ?>

            <?php if(count($points_of_sale)): ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    نقاط البيع
                </div>
                <div class="panel-body">

                    <table class="table table-striped"  style="direction: rtl;">
                        <?php foreach($points_of_sale as $key => $point_of_sale): ?>
                        <tr>
                            <td> <label class="label label-default">{{$point_of_sale->point_of_sale_desc}}</label></td>
                            <td>
                                الرصيد الحالي :
                                <label class="label label-info">{{$point_of_sale->point_of_sale_balance}}</label>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            </div>
            <?php endif; ?>

            <div class="panel panel-success">
                <div class="panel-heading">
                    النتائج
                </div>
                <div class="panel-body">

                    <div class="row">
                        <?php if($results["number_of_bills"] > 0): ?>

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4><b>إجمالي عدد الفواتير</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 50px;">{{$results["number_of_bills"]}}</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4> <b>عدد الفواتير التي تم مراجعتها</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 49px;">{{$results["number_of_reviewed_bills"]}}</span>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4> <b>عدد الفواتير التي لم تراجع</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 50px;">{{$results["number_of_waiting_review_bills"]}}</span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12" style="overflow-x: hidden;overflow-y: hidden;">
                            <input type="hidden" class="sell_bills" value="{{json_encode($results)}}">
                            <div id="sell_bills" style="width: 900px; height: 500px;"></div>
                        </div>

                        <?php else: ?>
                            <?php if(count($_GET)): ?>
                                <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>


                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    بيانات الفواتير
                </div>
                <div class="panel-body">

                    <div class="row">
                        <?php if(count($group_bills)): ?>

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php $counter = 0; ?>
                        <?php foreach($group_bills as $group_key => $bills): ?>
                            <?php
                                $bills = $bills->all();
                                $total_costs = array_sum(convert_inside_obj_to_arr($bills,"client_bill_total_amount"));
                                $total_paid_costs_cash = array_sum(convert_inside_obj_to_arr($bills,"client_total_paid_amount_in_cash"));
                                $total_paid_costs_atm = array_sum(convert_inside_obj_to_arr($bills,"client_total_paid_amount_in_atm"));
                                $total_remain_costs = array_sum(convert_inside_obj_to_arr($bills,"client_bill_total_remain_amount"));
                                $total_return_costs = array_sum(convert_inside_obj_to_arr($bills,"client_bill_total_return_amount"));
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$counter}}" aria-expanded="true" aria-controls="collapse{{$counter}}">
                                            {{$group_key}}
                                            <span style="float: left">
                                                <b>إجمالي التكلفة : <span class="label label-info">{{$total_costs}}</span></b> ||
                                                <b>إجمالي المدفوع : <span class="label label-success">{{$total_paid_costs_cash+$total_paid_costs_atm}}</span></b> ||
                                                <b>إجمالي المتبقي : <span class="label label-warning">{{$total_remain_costs}}</span></b> ||
                                                <b>إجمالي المرتجع : <span class="label label-danger">{{$total_return_costs}}</span></b>
                                            </span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$counter}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">

                                        <div class="col-md-12" style="overflow-x: scroll;">

                                            @include('admin.subviews.reports.blocks.show_bills')

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <?php $counter++; ?>
                        <?php endforeach; ?>
                        </div>


                        <?php else: ?>
                            <?php if(count($_GET)): ?>
                                <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>


                </div>
            </div>


        </div>
    </div>


@endsection
