@extends('admin.main_layout')

@section('subview')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{url('public_html/jscode/admin/reports.js')}}"></script>



    <div class="panel panel-info">
        <div class="panel-heading">
            إجمالي المبيعات حسب المنتج
        </div>
        <div class="panel-body">

            <?php $url = url('admin/reports/sell_bills_depend_on_product'); ?>
            @include('admin.subviews.reports.blocks.date_with_products_filteration')

                <?php if(count($pro_details)): ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        تفاصيل المنتج
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped"  style="direction: rtl;">
                            <tr>
                                <td>اسم المنتج</td>
                                <td>
                                    <label class="label label-default">{{$pro_details["pro_name"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>باركود المنتج</td>
                                <td>
                                    <label class="label label-default">{{$pro_details["pro_barcode"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>تاريخ صلاحية المنتج</td>
                                <td>
                                    <label class="label label-default">{{$pro_details["pro_expire_date"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>كمية المنتج في المخزن</td>
                                <td>
                                    <label class="label label-info">{{$pro_details["total_pro_quantity_on_stock"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>حد الطلب للمنتج</td>
                                <td>
                                    <label class="label label-info">{{$pro_details["pro_limit"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>كمية المنتج المباعه</td>
                                <td>
                                    <label class="label label-primary">{{$pro_details["total_sell_pro_quantity"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>كمية المنتج المرتجعه</td>
                                <td>
                                    <label class="label label-primary">{{$pro_details["total_return_pro_quantity"]}}</label>
                                </td>
                            </tr>
                            <tr>
                                <td>كمية المنتج المباعه كهدية فالعروض</td>
                                <td>
                                    <label class="label label-danger">{{$pro_details["total_pro_sell_as_gift_quantity"]}}</label>
                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
                <?php endif; ?>


                <div class="panel panel-success">
                <div class="panel-heading">
                  إحصائيات
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12" style="overflow-x: hidden;overflow-y: hidden;">
                            <?php if(count($product_statistics)): ?>

                            <input type="hidden" class="products_chart"
                                   value="{{json_encode($product_statistics)}}">
                            <div id="products_chart" style="width: 700px; height: 500px;"></div>

                            <?php endif; ?>

                            <?php if(!count($product_statistics) && count($_GET)): ?>
                            <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                            <?php endif; ?>

                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>


@endsection
