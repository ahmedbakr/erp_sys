@extends('admin.main_layout')

@section('subview')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{url('public_html/jscode/admin/reports.js')}}"></script>



    <div class="panel panel-info">
        <div class="panel-heading">
            إجمالي المبيعات حسب جنس العميل
        </div>
        <div class="panel-body">

            <?php $url = url('admin/reports/sell_bills_depend_on_user_gender'); ?>
            @include('admin.subviews.reports.blocks.date_with_user_gender_filteration')

            <div class="panel panel-success">
                <div class="panel-heading">
                    النتائج
                </div>
                <div class="panel-body">

                    <div class="row">
                        <?php if($results["number_of_bills"] > 0): ?>

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4><b>إجمالي عدد الفواتير</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 50px;">{{$results["number_of_bills"]}}</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4> <b>عدد الفواتير التي تم مراجعتها</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 30px;">{{$results["number_of_reviewed_bills"]}}</span>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="alert alert-info" style="text-align: center;height: 100px;">
                                    <h4> <b>عدد الفواتير التي لم تراجع</b></h4>
                                    <span class="label label-success" style="display: block;margin-top: 50px;">{{$results["number_of_waiting_review_bills"]}}</span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12" style="overflow-x: hidden;overflow-y: hidden;">
                            <input type="hidden" class="sell_bills" value="{{json_encode($results)}}">
                            <div id="sell_bills" style="width: 900px; height: 500px;"></div>
                        </div>

                        <?php else: ?>
                        <?php if(count($_GET)): ?>
                        <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                        <?php endif; ?>

                        <?php endif; ?>
                    </div>


                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    تقرير المنتجات لأكتر 10
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12" style="overflow-x: hidden;overflow-y: hidden;">
                            <?php if(count($sell_products)): ?>

                            <input type="hidden" class="sell_products_chart"
                                   value="{{json_encode($sell_products)}}">
                            <div id="sell_products_chart" style="width: 450px; height: 500px;float: left;"></div>

                            <?php endif; ?>

                            <?php if(count($return_products)): ?>

                            <input type="hidden" class="return_products_chart"
                                   value="{{json_encode($return_products)}}">
                            <div id="return_products_chart" style="width: 450px; height: 500px;"></div>

                            <?php endif; ?>

                            <?php if(!count($sell_products) && !count($return_products) && count($_GET)): ?>
                            <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                            <?php endif; ?>

                        </div>
                    </div>


                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    أحدث 20 فاتورة
                </div>
                <div class="panel-body">

                    <div class="row">
                        <?php if(count($bills)): ?>

                        <div class="col-md-12" style="overflow-x: scroll;">

                            @include('admin.subviews.reports.blocks.show_bills')

                        </div>

                        <?php else: ?>
                        <?php if(count($_GET)): ?>
                        <div class="alert alert-danger">لا توجد نتائج في هذة الفترة برجاء تغيير البحث</div>
                        <?php endif; ?>
                        <?php endif; ?>
                    </div>


                </div>
            </div>


        </div>
    </div>


@endsection
