@extends($main_layout.'.main_layout')

@section('subview')

    <input type="hidden" id="group_by_id" value="{{$group_by}}">

    <div class="panel panel-info">
        <div class="panel-heading">
            بحث في كل المصاريف
        </div>
        <div class="panel-body">

            <form action="{{url("/admin/reports/expenses_report")}}" method="GET">

                <div class="col-md-12">
                    <?php

                    echo generate_depended_selects(
                        $field_name_1="user_id",
                        $field_label_1="اختار الفرع",
                        $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $field_selected_value_1=(isset($_GET['user_id'])?$_GET['user_id']:""),
                        $field_required_1="",
                        $field_class_1="form-control",
                        $field_name_2="expenses_maker",
                        $field_label_2="اختار المستخدم",
                        $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                        $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                        $field_selected_value_2=(isset($_GET['expenses_maker'])?$_GET['expenses_maker']:""),
                        $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                        $field_required_2="",
                        $field_class_2="form-control",
                        $add_blank_options=true,
                        $obj_data=""
                    );

                    ?>

                </div>

                <div class="col-md-12">
                    <div class="col-md-3">

                        <div class="form-group">
                            @include("common.category.accounts_tree.tree_modal")

                            <label for="" style="visibility: hidden;">اظهر الشجرة</label>

                            <button
                                    type="button"
                                    class="btn btn-primary show_tree_modal center-block"
                                    data-select_what="category"
                                    data-select_type="single"
                                    data-div_to_show_selection=".move_to_new_category_div"
                                    data-input_field_name="expenses_type_id"
                            >
                                اظهر الشجرة لتختار التصنيف
                            </button>


                            <div class="move_to_new_category_div" style="text-align: center;">
                                <?php if(isset($expense_cat_data)): ?>

                                <label class="label label-primary selected_individual_item" data-item_id="5">
                                    {{$expense_cat_data->cat_name}}
                                    <a href="#" class="remove_selected_individual_item">×</a>
                                    <input type="hidden" name="expenses_type_id" value="{{$expense_cat_data->cat_id}}">
                                </label>

                                <?php endif; ?>
                            </div>

                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">من</label>
                            <input type="date" id="filter_date_from" name="expenses_date_from" value="{{$expenses_date_from}}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">الي</label>
                            <input type="date" id="filter_date_to" name="expenses_date_to" value="{{$expenses_date_to}}" class="form-control">

                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button class="btn btn-info">نتائج البحث</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
           {{$meta_title}}
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <?php if(empty($group_by)): ?>

            <?php
            $expenses_accept_amount_sum=0;
            $expenses_refused_amount_sum=0;
            ?>
            @include("admin.subviews.reports.blocks.expenses_table")
            <?php elseif($group_by=="user"): ?>
            <?php
            $all_expenses_groups=collect($all_expenses_rows)->groupBy("user_id")->all();
            ?>

            <div class="panel-group" id="accordion">
                <?php foreach($all_expenses_groups as $key=>$all_expenses_rows): ?>

                <?php
                if(count($all_expenses_rows)==0){
                    continue;
                }
                ?>

                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                        <h4 class="panel-title" >
                            <a>{{$all_expenses_rows[0]->branch_full_name}} مصروفات</a>
                        </h4>
                    </div>
                    <div id="collapse{{$key}}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php
                            $expenses_accept_amount_sum=0;
                            $expenses_refused_amount_sum=0;
                            ?>
                            @include("admin.subviews.reports.blocks.expenses_table")
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>

            <?php endif; ?>

        </div>
    </div>

@endsection
