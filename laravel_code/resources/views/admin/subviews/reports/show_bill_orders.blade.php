@extends('admin.main_layout')

@section("subview")

    <div class="">

        <div class="col-md-12">


            <div class="panel panel-info">
                <div class="panel-heading">
                    طباعة الفاتورة
                </div>
                <div class="panel-body">

                    <div class="col-md-12" style="text-align: center;">

                        <a href="{{url("/print_client_bill?bill_id=$bill_data->client_bill_id")}}" class="btn btn-primary">طباعة الفاتورة</a>
                        <a href="{{url("/print_client_bill?bill_id=$bill_data->client_bill_id&bill_type=1")}}" class="btn btn-primary">طباعة مرجعات الفاتورة</a>
                        <a href="{{url("prepare_print_cheques?money_in_numbers=$bill_data->client_total_paid_amount_in_atm")}}" class="btn btn-primary">طباعة الشيك</a>
                    </div>

                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    تفاصيل الفاتورة
                </div>
                <div class="panel-body">

                    <table class="table table-striped"  style="direction: rtl;">
                        <tr>
                            <td>اسم الكاشير</td>
                            <td>
                                <label class="label label-default">{{$bill_data->cashier_full_name}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>اسم العميل</td>
                            <td>
                                <label class="label label-default">{{$bill_data->client_full_name}}</label>
                                <label class="label label-info">{{$bill_data->client_user_tel}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>تكلفة الفاتورة</td>
                            <td>
                                <label class="label label-primary">{{$bill_data->client_bill_total_amount}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>تكلفة مرجعات الفاتورة</td>
                            <td>
                                <label class="label label-danger">{{$bill_data->client_bill_total_return_amount}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار ما دفع العميل بواسطة الكاش</td>
                            <td>
                                <label class="label label-info">{{$bill_data->client_total_paid_amount_in_cash}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار ما دفع العميل بواسطة الشبكة</td>
                            <td>
                                <label class="label label-info">{{$bill_data->client_total_paid_amount_in_atm}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار المتبقي علي العميل</td>
                            <td>
                                <label class="label label-warning">{{$bill_data->client_bill_total_remain_amount}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار ما اخذه البنك من تحويلة الشبكة</td>
                            <td>
                                <label class="label label-danger">{{$bill_data->bank_atm_ratio_value}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>تاريخ الفاتورة</td>
                            <td>
                                <label class="label label-default">{{$bill_data->client_bill_date}}</label>
                            </td>
                        </tr>
                        <?php if(!empty($bill_data->bill_coupon)): ?>

                        <tr>
                            <td>الكوبون المستعمل علي الفاتورة</td>
                            <td>
                                <label class="label label-default">{{$bill_data->bill_coupon}}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار الخصم بسبب الكوبون</td>
                            <td>
                                <label class="label label-danger">{{$bill_data->bill_coupon_amount}}</label>
                            </td>
                        </tr>
                        <?php endif; ?>

                    </table>



                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading">
                    طلبات الفاتورة
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-bordered ">

                        <thead>
                        <tr>
                            <th>#</th>
                            <th>صورة المنتج</th>
                            <th>اسم المنتج</th>
                            <th>الباركود</th>
                            <th>الكمية</th>
                            <th>سعر الاصلي للوحده</th>
                            <th>سعر البيع للوحده</th>
                            <th>الاجمالي</th>
                            <th>النوع (بيع - مرتجع)</th>
                            <th>هل المنتج هدية</th>
                            <th>التاريخ</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $all_bill_cost=0;
                        ?>
                        <?php if(isset_and_array($all_bill_orders)): ?>
                        <?php foreach($all_bill_orders as $key=>$order): ?>
                        <?php
                        $all_bill_cost=$all_bill_cost+((($order->order_return)?-1:1)*$order->order_quantity*$order->pro_sell_price);
                        ?>
                        <tr class="{{($order->order_return)?"warning":""}}">
                            <td>{{$key+1}}</td>
                            <td><img src="{{get_image_or_default($order->pro_img_path)}}" width="100" alt=""></td>
                            <td>{{$order->pro_name}}</td>
                            <td>{{$order->pro_barcode}}</td>
                            <td><label class="label label-default">{{$order->order_quantity}}</label></td>
                            <td><label class="label label-info">{{$order->pro_original_price}}</label></td>
                            <td><label class="label label-primary">{{$order->pro_sell_price}}</label></td>
                            <td><label class="label label-info">{{(($order->order_return)?-1:1)*   $order->order_quantity*$order->pro_sell_price}}</label></td>
                            <td>{{($order->order_return)?"مرتجع":"بيع"}}</td>
                            <td>
                                <?php if($order->is_gift): ?>
                                <label class="label label-danger">هدية</label>
                                <?php endif; ?>
                            </td>
                            <td>
                                <label class="label label-default">{{date("Y-m-d",strtotime($order->order_created_at))}}</label>
                                <label class="label label-default">{{date("g:i a",strtotime($order->order_created_at))}}</label>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif; ?>

                        </tbody>


                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><label class="label label-info">{{$all_bill_cost}}</label></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>




                    </table>
                </div>
            </div>



        </div>

    </div>

@endsection


