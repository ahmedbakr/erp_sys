@extends('admin.main_layout')

@section("subview")

    <div class="">

        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    بحث بفترة محددة
                </div>
                <div class="panel-body">

                    <form action="{{url("/admin/reports/review_expenses_report")}}">

                        <div class="col-md-12">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">من</label>
                                    <input type="date" name="expenses_date_from" value="{{(isset($_GET['expenses_date_from'])?date("Y-m-d",strtotime($_GET['expenses_date_from'])):"")}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">الي</label>
                                    <input type="date" name="expenses_date_to" value="{{(isset($_GET['expenses_date_to'])?date("Y-m-d",strtotime($_GET['expenses_date_to'])):"")}}" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3" style="text-align: center;margin-top: 25px;">
                                <button class="btn btn-info">نتائج البحث</button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    {{$meta_title}}
                </div>

                <div class="panel-body" style="overflow-x: scroll;">

                    <table class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">

                        <thead>
                        <tr>
                            <td>#</td>
                            <td>الفرع</td>
                            <td>مجموع المصاريف</td>
                            <td>مجموع العهد المستلمة</td>
                            <td>العهد المستلمة - المصاريف</td>
                            <td>المتبقي الحالي من العهد</td>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                            $total_expenses = 0;
                            $total_received_duty = 0;
                            $total_supposed_duty = 0;
                            $total_current_duty = 0;
                        ?>
                        <?php foreach ($all_expenses_rows as $key => $expense): ?>
                        <tr>
                            <td></td>
                            <td><?=$expense['branch_name'] ?></td>
                            <td>
                                <?php $total_expenses = $total_expenses + $expense['total_expenses']; ?>
                                <?=$expense['total_expenses'] ?>
                            </td>
                            <td>
                                <?php $total_received_duty = $total_received_duty + $expense['total_received_duty']; ?>
                                <?=$expense['total_received_duty'] ?>
                            </td>
                            <td>
                                <?php $total_supposed_duty = $total_supposed_duty + ($expense['total_received_duty'] - $expense['total_expenses']); ?>
                                <?=($expense['total_received_duty'] - $expense['total_expenses']) ?>
                            </td>
                            <td>
                                <?php $total_current_duty = $total_current_duty + $expense['total_current_duty']; ?>
                                <?=$expense['total_current_duty'] ?>
                            </td>

                        </tr>
                        <?php endforeach ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><span class="label label-default">{{$total_expenses}}</span></td>
                            <td><span class="label label-default">{{$total_received_duty}}</span></td>
                            <td><span class="label label-default">{{$total_supposed_duty}}</span></td>
                            <td><span class="label label-default">{{$total_current_duty}}</span></td>
                        </tr>
                    </table>

                </div>
            </div>


        </div>

    </div>

@endsection


