@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">الفتلرة</div>
        <div class="panel-body">
            <form action="" method="get">
                <div class="form-group">
                    <label for="">باركود المنتج</label>
                    <input type="text" class="form-control" name="pro_barcode" value="{{isset($all_post_data->pro_barcode)?$all_post_data->pro_barcode:""}}">
                </div>
                <?php

                $normal_tags=[
                    "start_date","end_date"
                ];


                $attrs = generate_default_array_inputs_html(
                    $normal_tags,
                    $post_data="",
                    "yes",
                    "",
                    "4"
                );

                $attrs[0]["start_date"]="من ";
                $attrs[0]["end_date"]="إلي ";


                $attrs[3]["start_date"]="date";
                $attrs[3]["end_date"]="date";

                $attrs[6]["start_date"]="6";
                $attrs[6]["end_date"]="6";

                $attrs[4]["start_date"]=(isset($_GET['start_date']) && !empty($_GET['start_date']))?$_GET['start_date']:date('Y-m-d',time());
                $attrs[4]["end_date"]=(isset($_GET['end_date']) && !empty($_GET['end_date']))?$_GET['end_date']:date('Y-m-d',time());

                echo
                generate_inputs_html(
                    reformate_arr_without_keys($attrs[0]),
                    reformate_arr_without_keys($attrs[1]),
                    reformate_arr_without_keys($attrs[2]),
                    reformate_arr_without_keys($attrs[3]),
                    reformate_arr_without_keys($attrs[4]),
                    reformate_arr_without_keys($attrs[5]),
                    reformate_arr_without_keys($attrs[6])
                );
                ?>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">ابحث</button>
                </div>
            </form>
        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            مبيعات المنتج
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <td>#</td>
                    <th>الفرع</th>
                    <th>اسم الكاشيير</th>
                    <th>العميل</th>
                    <th>المنتج</th>
                    <th>الكمية</th>
                    <th>شراء او مرتجع</th>
                    <th>الفاتورة</th>
                </tr>
                </thead>

                <?php
                    $total_quantities = 0;
                    $total_bills = 0;
                ?>

                <tbody>
                <?php foreach ($all_client_bills_orders as $key => $order): ?>
                    <?php
                        if(!isset($all_client_bills[$order->client_bill_id]))continue;

                        $total_quantities += $order->product_sum;
                        $total_bills += $all_client_bills[$order->client_bill_id][0]->client_bill_total_amount;
                    ?>
                    <tr>
                        <td></td>
                        <td>{{$all_client_bills[$order->client_bill_id][0]->branch_full_name}}</td>
                        <td>
                            {{$all_client_bills[$order->client_bill_id][0]->cashier_full_name}}
                        </td>
                        <td>
                            {{$all_client_bills[$order->client_bill_id][0]->client_full_name}}
                            <br>
                            {{$all_client_bills[$order->client_bill_id][0]->client_user_tel}}
                        </td>
                        <td>
                            {{$order->pro_name}}
                            <br>
                            {{$order->pro_barcode}}
                        </td>
                        <td>
                            {{$order->product_sum}}
                        </td>
                        <th>{{($order->order_return=="1"?"مرتجع":"شراء")}}</th>
                        <td>
                            {{$all_client_bills[$order->client_bill_id][0]->client_bill_total_amount}}
                        </td>
                    </tr>
                <?php endforeach ?>

                    <tr style="font-size: 20px;color: blue;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>{{$total_quantities}}</b></td>
                        <td></td>
                        <td><b>{{$total_bills}}</b></td>
                    </tr>

                </tbody>

            </table>


        </div>
    </div>


@endsection

