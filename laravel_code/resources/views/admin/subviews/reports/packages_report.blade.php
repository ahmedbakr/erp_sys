@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <div class="col-md-12">

                    <?php

                    $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                    $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $branches_text = array_merge(["كل الافرع"],convert_inside_obj_to_arr($all_branches,"full_name"));
                        $branches_values = array_merge([""],convert_inside_obj_to_arr($all_branches,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "4"
                    );


                    echo generate_select_tags(
                        $field_name="price_id",
                        $label_name="اختار السعر لتحديد الارباح",
                        $text=convert_inside_obj_to_arr($prices_list,"price_title"),
                        $values=convert_inside_obj_to_arr($prices_list,"price_id"),
                        $selected_value=(isset($_GET['price_id']))?[$_GET['price_id']]:[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = "",
                        $grid="4"
                    );

                    ?>


                    <div class="form-group col-md-4">
                        <label for="">من</label>
                        <input type="date" name="start_date" class="form-control" value="{{(isset($all_post_data->start_date))?$all_post_data->start_date:""}}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">الي</label>
                        <input type="date" name="end_date" class="form-control" value="{{(isset($all_post_data->end_date))?$all_post_data->end_date:""}}">
                    </div>
                </div>


                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">العروض</div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">


                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اسم العرض</td>
                    <td>عدد مرات استعمال العرض</td>
                    <td>عدد الفواتير</td>
                    <td>إجمالي المبيعات</td>
                    <?php if(isset($_GET['price_id'])): ?>
                    <td>الربح</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tbody>
                <?php
                $total_packages_used = 0;
                $bills_count = 0;
                $total_bills_after_discount = 0;
                $total_gain = 0;
                ?>
                <?php foreach($output_data as $key => $value): ?>
                <?php
                $total_packages_used += $value['total_packages_used'];
                $bills_count += $value['bills_count'];
                $total_bills_after_discount += $value['total_bills_after_discount'];
                $total_gain += $value['total_gain'];
                ?>
                <tr>
                    <td></td>
                    <td>{{$value['branch_name']}}</td>
                    <td>{{$value['cat_name']}}</td>
                    <td>{{$value['total_packages_used']}}</td>
                    <td>{{$value['bills_count']}}</td>
                    <td>{{$value['total_bills_after_discount']}}</td>
                    <?php if(isset($_GET['price_id'])): ?>
                    <td>{{$value['total_gain']}}</td>
                    <?php endif; ?>

                </tr>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{$total_packages_used}}</td>
                    <td>{{$bills_count}}</td>
                    <td>{{$total_bills_after_discount}}</td>
                    <?php if(isset($_GET['price_id'])): ?>
                    <td>{{$total_gain}}</td>
                    <?php endif; ?>
                </tr>
                </tbody>

            </table>
        </div>
    </div>


@endsection