@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <div class="col-md-12">

                    <?php

                    $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                    $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $branches_text = array_merge(["كل الافرع"],convert_inside_obj_to_arr($all_branches,"full_name"));
                        $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "4"
                    );


                    echo
                    generate_select_tags(
                        $field_name="bill_status_id",
                        $label_name="حالة الفاتوة",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($all_status,"status_name")),
                        $values=array_merge([""],convert_inside_obj_to_arr($all_status,"status_id")),
                        $selected_value=[""],
                        $class="form-control",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "4"
                    );
                    ?>

                    <div class="form-group col-md-4">
                        <label for="">من</label>
                        <input type="date" name="bill_start_date" class="form-control" value="{{(isset($all_post_data->bill_start_date))?$all_post_data->bill_start_date:""}}">
                    </div>

                </div>

                <div class="col-md-12">

                    <div class="form-group col-md-4">
                        <label for="">الي</label>
                        <input type="date" name="bill_end_date" class="form-control" value="{{(isset($all_post_data->bill_end_date))?$all_post_data->bill_end_date:""}}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">باركود|اسم المنتج</label>
                        <input type="text" name="pro_search" class="form-control" value="{{(isset($all_post_data->pro_search))?$all_post_data->pro_search:""}}">
                    </div>

                </div>


                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">طلبات الفروع</div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>الفرع</td>
                        <td>رقم الفاتورة</td>
                        <td>اسم وباركود المنتج</td>
                        <td>العدد المطلوب</td>
                        <td>تاريخ الطلب</td>
                        <td>حالة الطلب</td>
                        <td>الطلب مرتجع؟</td>
                        <td>تم الموافقه علي المرتجع؟</td>
                    </tr>
                </thead>


                <tbody>
                    <?php foreach($branch_orders as $key=>$order): ?>

                    <tr>
                        <td>#</td>
                        <td>{{$order->full_name}}</td>
                        <td>{{$order->branch_bill_id}}</td>
                        <td>
                            <label class="label label-default">{{$order->pro_name}}</label>
                            <label class="label label-default">{{$order->pro_barcode}}</label>
                        </td>
                        <td>{{$order->b_o_quantity}}</td>
                        <td>{{$order->b_o_date}}</td>
                        <td>{{isset($all_status_grouped_by_status_id[$order->status_id])?$all_status_grouped_by_status_id[$order->status_id][0]->status_name:""}}</td>
                        <td>{{$order->b_o_return=="0"?"لا":"نعم"}}</td>
                        <td>{{$order->factory_accept_return=="0"?"لا":"نعم"}}</td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>


@endsection