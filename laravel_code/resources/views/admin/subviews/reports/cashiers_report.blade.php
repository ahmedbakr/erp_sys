@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            المستخدمين
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الفرع</th>
                    <th>كود المستخدم</th>
                    <th>اسم المستخدم</th>
                    <th>ايميل المستخدم</th>
                    <th>تليفون المستخدم</th>
                    <th>موبايل المستخدم</th>
                    <th>فاكس المستخدم</th>
                    <th>عنوان المستخدم</th>
                    <th>تاريخ بدء العمل</th>
                    <th>رقم الهوية الوطنية او الإقامة</th>
                    <th>الجنسية</th>
                    <th>تاريخ إنتهاء الهوية او الإقامة</th>
                    <th>المهنه في الإقامة</th>
                    <th>المهنه في الشركة</th>

                </tr>
                </thead>

                <tbody>
                <?php foreach ($all_cashiers as $key => $user): ?>
                <tr id="row<?= $user->user_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        {{$user->related_branch_name}}
                        <br>
                        {{$user->related_branch_name_en}}
                    </td>
                    <td>{{$user->user_id}}</td>

                    <td>
                        <?=$user->full_name ?>
                        <br>
                        <?=$user->full_name_en ?>
                    </td>
                    <td><?=$user->email ?></td>

                    <td><?=$user->user_tel ?></td>
                    <td><?=$user->user_mobile ?></td>
                    <td><?=$user->user_fax ?></td>
                    <td><?=$user->user_address ?></td>
                    <td><?=$user->start_join_date ?></td>
                    <td><?=$user->identification_number ?></td>
                    <td><?=$user->nationality ?></td>
                    <td><?=$user->identification_expire_date ?></td>
                    <td><?=$user->user_job_on_passport ?></td>
                    <td><?=$user->user_job_on_company ?></td>

                </tr>
                <?php endforeach ?>
                </tbody>


            </table>

        </div>
    </div>

@endsection
