@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <div class="col-md-12">

                    <?php

                    $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                    $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $branches_text = array_merge(["كل الافرع","الفرع الرئيسي"],convert_inside_obj_to_arr($all_branches,"full_name"));
                        $branches_values = array_merge(["","0"],convert_inside_obj_to_arr($all_branches,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "4"
                    );


                    echo
                    generate_select_tags(
                        $field_name="permission_type",
                        $label_name="نوع الان",
                        $text=["الكل","اذن اضافة","اذن صرف"],
                        $values=["","add","get"],
                        $selected_value=[""],
                        $class="form-control",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = $all_post_data,
                        "4"
                    );
                    ?>

                    <div class="form-group col-md-4">
                        <label for="">من</label>
                        <input type="date" name="bill_start_date" class="form-control" value="{{(isset($all_post_data->bill_start_date))?$all_post_data->bill_start_date:""}}">
                    </div>

                </div>

                <div class="col-md-12">

                    <div class="form-group col-md-4">
                        <label for="">الي</label>
                        <input type="date" name="bill_end_date" class="form-control" value="{{(isset($all_post_data->bill_end_date))?$all_post_data->bill_end_date:""}}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">باركود|اسم المنتج</label>
                        <input type="text" name="pro_search" class="form-control" value="{{(isset($all_post_data->pro_search))?$all_post_data->pro_search:""}}">
                    </div>

                </div>


                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">اذون الفروع</div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>الفرع</td>
                        <td>رقم الاذن</td>
                        <td>اسم وباركود المنتج</td>
                        <td>العدد المطلوب</td>
                        <td>تاريخ الان</td>
                        <td>نوع الاذن</td>
                    </tr>
                </thead>


                <?php
                    $all_branch_bills=collect($all_branch_bills)->groupBy("permission_id")->all();
                ?>
                <tbody>
                    <?php foreach($permission_bill_orders as $key=>$order): ?>
                    <?php
                        if(!isset($all_branch_bills[$order->permission_id])){
                            continue;
                        }
                        $bill_obj=$all_branch_bills[$order->permission_id][0];
                    ?>
                    <tr>
                        <td>#</td>
                        <td>{{($bill_obj->branch_id=="0")?"الفرع الرئيسي":$bill_obj->branch_full_name}}</td>
                        <td>{{$order->permission_id}}</td>
                        <td>
                            <label class="label label-default">{{$order->pro_name}}</label>
                            <label class="label label-default">{{$order->pro_barcode}}</label>
                        </td>
                        <td>{{$order->order_pro_quantity}}</td>
                        <td>{{$bill_obj->permission_date}}</td>
                        <td>{{($bill_obj->permission_type=="add")?"اذن اضافة":"اذن صرف"}}</td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>


@endsection