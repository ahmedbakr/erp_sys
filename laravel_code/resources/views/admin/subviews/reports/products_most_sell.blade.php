@extends('admin.main_layout')

@section('subview')

    @include("common.category.accounts_tree.tree_modal")

    <script>
        $(function () {

            $('#datetimepicker_start_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('#datetimepicker_end_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });

        });
    </script>


    <div class="panel panel-info">
        <div class="panel-heading">العمليات</div>
        <div class="panel-body">
            <form action="{{url("/admin/reports/products_most_sell")}}" method="get">

                <?php

                    $branches_text = array_merge(["الكل"],convert_inside_obj_to_arr($all_branches,"full_name"));
                    $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));

                    echo generate_select_tags(
                        $field_name="branch_id",
                        $label_name="اختار الفرع",
                        $text=$branches_text,
                        $values=$branches_values,
                        $selected_value=[$branch_id],
                        $class="form-group select_2_class",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = "",
                        $grid="6"
                    );

                    echo generate_select_tags(
                        $field_name="price_id",
                        $label_name="اختار السعر لتحديد صافي الربح",
                        $text=array_merge(["السعر الذي بيع به بدون خصومات"],convert_inside_obj_to_arr($prices_list,"price_title")),
                        $values=array_merge([-1],convert_inside_obj_to_arr($prices_list,"price_id")),
                        $selected_value=(isset($_GET['price_id'])?[$_GET['price_id']]:[""]),
                        $class="form-group select_2_class",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = "",
                        $grid="6"
                    );

                ?>


                <div class="col-md-3">
                    <label for="">تاريخ من :</label>
                    <input type="text" id="datetimepicker_start_date" name="from_date" class="form-control"
                           value="{{($from_date == "")?"":$from_date}}">
                </div>

                <div class="col-md-3">
                    <label for="">تاريخ إلي :</label>
                    <input type="text" id="datetimepicker_end_date" name="to_date" class="form-control"
                           value="{{($to_date == "")?"":$to_date}}">
                </div>


                <div class="col-md-3" style="margin-top: 20px;">
                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="single"
                            data-div_to_show_selection=".select_cats"
                            data-input_field_name="cat_ids"
                    >
                        اظهر الشجرة لتختار التصنيفات
                    </button>

                    <div class="select_cats" style="text-align: center;">

                    </div>
                </div>

                <div class="col-md-2" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-primary">ابحث</button>
                </div>

            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">{{$meta_title}}</div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td></td>
                    <td>اسم المنتج</td>
                    <td>باركود المنتج</td>
                    <td>كمية الشراء</td>
                    <td>مجموع الشراء</td>
                    <td>كمية المرتجع</td>
                    <td>مجموع المرتجع</td>
                    <td>صافي الكمية</td>
                    <td>صافي المبيعات</td>
                    <td>صافي الربح</td>
                </tr>
                </thead>


                <tbody>
                <?php
                    $total_sell_quantity = 0;
                    $total_sell_amount = 0;
                    $total_return_quantity = 0;
                    $total_return_amount = 0;
                    $total_all_quantity = 0;
                    $total_all_amount = 0;
                    $total_gain = 0;
                ?>
                <?php foreach ($all_products as $key => $product): ?>
                <tr>
                    <td></td>
                    <td>
                        <span class="label label-info"><?= $product["pro_name"] ?></span>
                    </td>
                    <td>
                        <span class="label label-default"><?= $product["pro_barcode"] ?></span>
                    </td>
                    <td>
                        <span class="label label-info">
                            <?= $product["sell_quantity"] ?>
                            <?php $total_sell_quantity += $product["sell_quantity"]; ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            <?= $product["sell_amount"] ?>
                            <?php $total_sell_amount += $product["sell_amount"]; ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-warning">
                            <?= $product["return_quantity"] ?>
                            <?php $total_return_quantity += $product["return_quantity"]; ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-warning">
                            <?= $product["return_amount"] ?>
                            <?php $total_return_amount += $product["return_amount"]; ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            <?= ($product["sell_quantity"]-$product["return_quantity"]) ?>
                            <?php $total_all_quantity += ($product["sell_quantity"]-$product["return_quantity"]); ?>
                        </span>
                    </td>
                    
                    <td>
                        <span class="label label-success">
                            <?= ($product["sell_amount"]-$product["return_amount"]) ?>
                            <?php $total_all_amount += ($product["sell_amount"]-$product["return_amount"]); ?>
                        </span>
                    </td>

                    <td>
                        <span class="label label-success">
                            <?= ($product["sell_amount"]-$product["return_amount"])-($product["sell_gain"]-$product["return_gain"]); ?>
                            <?php $total_gain += ($product["sell_gain"]-$product["return_gain"]); ?>
                        </span>
                    </td>

                </tr>
                <?php endforeach ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><span class="label label-default">{{$total_sell_quantity}}</span></td>
                    <td><span class="label label-default">{{$total_sell_amount}}</span></td>
                    <td><span class="label label-default">{{$total_return_quantity}}</span></td>
                    <td><span class="label label-default">{{$total_return_amount}}</span></td>
                    <td><span class="label label-default">{{$total_all_quantity}}</span></td>
                    <td><span class="label label-default">{{$total_all_amount}}</span></td>
                    <td><span class="label label-default">{{$total_all_amount-$total_gain}}</span></td>
                </tr>
                </tbody>

            </table>

        </div>
    </div>


@endsection