<script>
    $(function () {

        $('#datetimepicker_start_date').datetimepicker({
            format: 'YYYY-MM-DD LT'
        });

        $('#datetimepicker_end_date').datetimepicker({
            format: 'YYYY-MM-DD LT'
        });

    });
</script>

<div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" href="#results_filteration" style="cursor: pointer;"  aria-controls="collapseExample">
   <i class="fa fa-filter"></i>     فلتر النتائج
    </div>
    <div class="panel-body collapse" id="results_filteration">
        <div class="row">

            <form action="{{$url}}">

                <div class="col-md-4">
                    <label for="">تاريخ من :</label>
                    <input type="text" id="datetimepicker_start_date" name="from_date" class="form-control"
                           value="{{($from_date == "")?"":$from_date}}">
                </div>

                <div class="col-md-4">
                    <label for="">تاريخ إلي :</label>
                    <input type="text" id="datetimepicker_end_date" name="to_date" class="form-control"
                           value="{{($to_date == "")?"":$to_date}}">
                </div>

                <?php if(count($customers)): ?>
                    <div class="col-md-4">
                        <?php
                        echo generate_select_tags(
                            $field_name="cust_id",
                            $label_name="إختار العميل",
                            $text=array_merge(["الكل"],convert_inside_obj_to_arr($customers,"full_name")),
                            $values=array_merge([0],convert_inside_obj_to_arr($customers,"user_id")),
                            $selected_value=(isset($_GET['cust_id']) && $_GET['cust_id'] > 0)?[$_GET['cust_id']]:[""],
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                <?php endif; ?>

                <?php if(count($branches)): ?>

                <div class="col-md-4">
                    <?php
                    echo generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($branches,"full_name")),
                        $values=array_merge([0],convert_inside_obj_to_arr($branches,"user_id")),
                        $selected_value=(isset($_GET['branch_id']) && $_GET['branch_id'] > 0)?[$_GET['branch_id']]:[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required="required",
                        $disabled = "",
                        $data = ""
                    );
                    ?>
                </div>
                <?php endif; ?>


                <?php if(count($cashiers)): ?>

                <div class="col-md-4">
                    <?php
                    echo generate_select_tags(
                        $field_name="cashier_id",
                        $label_name="إختار المستخدم (الكاشيير)",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($cashiers,"full_name")),
                        $values=array_merge([0],convert_inside_obj_to_arr($cashiers,"user_id")),
                        $selected_value=(isset($_GET['cashier_id']) && $_GET['cashier_id'] > 0)?[$_GET['cashier_id']]:[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required="required",
                        $disabled = "",
                        $data = ""
                    );
                    ?>
                </div>
                <?php endif; ?>

                <?php
                echo
                generate_select_tags(
                    $field_name="age_range",
                    $label_name="الفئة العمرية",
                    $text=["الكل","15-25","26-40","41-60","العمر>60"],
                    $values=["","15-25","26-40","41-60","العمر>60"],
                    $selected_value=(isset($_GET['age_range']))?[$_GET['age_range']]:[""],
                    $class="form-control",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="4"
                );

                echo
                generate_select_tags(
                    $field_name="user_gender",
                    $label_name="نوع العميل",
                    $text=["الكل","ذكر","انثي"],
                    $values=["","male","female"],
                    $selected_value=(isset($_GET['user_gender']))?[$_GET['user_gender']]:[""],
                    $class="form-control",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="4"
                );

                ?>


                <div class="col-md-2" style="margin-top: 22px;">
                    <input type="submit" class="btn btn-success" value="عرض النتائج">
                </div>

            </form>

        </div>
    </div>
</div>
