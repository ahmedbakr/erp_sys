<div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" href="#results_filteration" style="cursor: pointer;"  aria-controls="collapseExample">
        <i class="fa fa-filter"></i>     فلتر النتائج
    </div>
    <div class="panel-body collapse" id="results_filteration">
        <div class="row">

            <form action="{{$url}}">

                <div class="col-md-3">
                    <label for="">تاريخ من :</label>
                    <input type="date" name="from_date" class="form-control"
                           value="{{($from_date == "")?date("Y-m-d"):$from_date}}">
                </div>

                <div class="col-md-3">
                    <label for="">تاريخ إلي :</label>
                    <input type="date" name="to_date" class="form-control"
                           value="{{($to_date == "")?date("Y-m-d"):$to_date}}">
                </div>

                <?php
                echo
                    generate_select_tags(
                        $field_name="age_range",
                        $label_name="الفئة العمرية *",
                        $text=["15-25","26-40","41-60","العمر>60"],
                        $values=["15-25","26-40","41-60","العمر>60"],
                        $selected_value=["$age_range"],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = "",
                        $grid="3"
                    );
                ?>

                <div class="col-md-4" style="margin-top: 22px;">
                    <input type="submit" class="btn btn-success" value="عرض النتائج">
                </div>

            </form>

        </div>
    </div>
</div>
