<div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" href="#results_filteration" style="cursor: pointer;"  aria-controls="collapseExample">
        <i class="fa fa-filter"></i>     فلتر النتائج
    </div>
    <div class="panel-body collapse" id="results_filteration">
        <div class="row">

            <form action="{{$url}}">

                <div class="col-md-4">
                    <label for="">تاريخ من :</label>
                    <input type="text" name="from_date" id="fromdate_session_datetimepicker" class="form-control"
                           value="{{(isset($_GET['from_date']) && $_GET['from_date'] != "")?$_GET['from_date']:""}}">
                </div>

                <div class="col-md-4">
                    <label for="">تاريخ إلي :</label>
                    <input type="text" name="to_date" id="todate_session_datetimepicker" class="form-control"
                           value="{{(isset($_GET['to_date']) && $_GET['to_date'] != "")?$_GET['to_date']:""}}">
                </div>

                <?php if(count($branches)): ?>

                <div class="col-md-4">
                    <?php
                    echo generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($branches,"full_name")),
                        $values=array_merge([0],convert_inside_obj_to_arr($branches,"user_id")),
                        $selected_value=(isset($_GET['branch_id']) && $_GET['branch_id'] > 0)?[$_GET['branch_id']]:[""],
                        $class="form-control",
                        $multiple="",
                        $required="required",
                        $disabled = "",
                        $data = ""
                    );
                    ?>
                </div>
                <?php endif; ?>


                <?php if(count($cashiers)): ?>

                <div class="col-md-4">
                    <?php
                    echo generate_select_tags(
                        $field_name="cashier_id",
                        $label_name="إختار المستخدم (الكاشيير)",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($cashiers,"full_name")),
                        $values=array_merge([0],convert_inside_obj_to_arr($cashiers,"user_id")),
                        $selected_value=(isset($_GET['cashier_id']) && $_GET['cashier_id'] > 0)?[$_GET['cashier_id']]:[""],
                        $class="form-control",
                        $multiple="",
                        $required="required",
                        $disabled = "",
                        $data = ""
                    );
                    ?>
                </div>
                <?php endif; ?>


                <div class="col-md-2" style="margin-top: 22px;">
                    <input type="submit" class="btn btn-success" value="عرض النتائج">
                </div>

            </form>

        </div>
    </div>
</div>
<script>
    $(function () {
        $('#fromdate_session_datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD LT'
        });
        $('#todate_session_datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD LT'
        });
    });
</script>