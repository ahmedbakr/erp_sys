<div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" href="#results_filteration" style="cursor: pointer;"  aria-controls="collapseExample">
        <i class="fa fa-filter"></i>     فلتر النتائج
    </div>
    <div class="panel-body collapse" id="results_filteration">
        <div class="row">

            <form action="{{$url}}">

                <div class="col-md-3">
                    <label for="">تاريخ من :</label>
                    <input type="date" name="from_date" class="form-control"
                           value="{{($from_date == "")?date("Y-m-d"):$from_date}}">
                </div>

                <div class="col-md-3">
                    <label for="">تاريخ إلي :</label>
                    <input type="date" name="to_date" class="form-control"
                           value="{{($to_date == "")?date("Y-m-d"):$to_date}}">
                </div>

                <div class="col-md-3">
                    <?php
                    echo generate_select_tags(
                        $field_name="pro_id",
                        $label_name="إختار المنتج *",
                        $text=convert_inside_obj_to_arr($products,"pro_name"),
                        $values=convert_inside_obj_to_arr($products,"pro_id"),
                        $selected_value=["$pro_id"],
                        $class="form-control",
                        $multiple="",
                        $required="required",
                        $disabled = "",
                        $data = ""
                    );
                    ?>
                </div>

                <div class="col-md-4" style="margin-top: 22px;">
                    <input type="submit" class="btn btn-success" value="عرض النتائج">
                </div>

            </form>

        </div>
    </div>
</div>
