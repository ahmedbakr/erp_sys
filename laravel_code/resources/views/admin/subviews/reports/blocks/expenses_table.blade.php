<table class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
    <thead>
    <tr>
        <td>#</td>
        <td>الفرع</td>
        <td>مدخل المصروف</td>
        <td>نوع المصروف</td>
        <td>اسم المصروف</td>
        <td>الوصف</td>
        <td>المبلغ</td>
        <td>التاريخ</td>
        <td>حالة المصروف</td>

    </tr>
    </thead>

    <tbody>
    <?php foreach ($all_expenses_rows as $key => $expenses_row): ?>
    <?php
    if($expenses_row->expense_status=="0"){
        $expenses_refused_amount_sum=$expenses_refused_amount_sum+$expenses_row->expense_amount;

    }
    else if($expenses_row->expense_status=="1"){

        $expenses_accept_amount_sum=$expenses_accept_amount_sum+$expenses_row->expense_amount;
    }
    ?>

    <tr id="row<?= $expenses_row->expense_id ?>">
        <td></td>
        <td>{{$expenses_row->branch_full_name}}</td>
        <td>{{$expenses_row->expense_maker_name}}</td>
        <td>{{$expenses_row->cat_name}}</td>
        <td>{{$expenses_row->expense_name}}</td>
        <td><p>{{$expenses_row->expense_desc}}</p></td>
        <td>{{$expenses_row->expense_amount}}</td>
        <td>{{dump_date($expenses_row->expense_date)}}</td>

        <td>
            <?php if($expenses_row->expense_status): ?>
            <label class="label label-info">تمت الموافقة عليه</label>
            <?php else: ?>
            <label class="label label-warning">لم يتم الموافقة</label>
            <?php endif; ?>
        </td>

    </tr>
    <?php endforeach ?>
    </tbody>

    <tfoot>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="success">
            المبالغ الموافق عليها {{$expenses_accept_amount_sum}}
            <br>
            المبالغ الغير موافق عليها {{$expenses_refused_amount_sum}}
        </td>
        <td></td>
        <td></td>

    </tr>
    </tfoot>

</table>
