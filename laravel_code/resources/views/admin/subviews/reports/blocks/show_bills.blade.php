<table class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%">

    <thead>
    <tr>
        <th>#</th>
        <th>اسم الكاشير</th>
        <th>رقم الفاتورة</th>
        <th>اسم العميل</th>
        <th>تاريخ الفاتورة</th>
        <th>تكلفة الفاتورة</th>
        <th>المدفوع من الفاتورة</th>
        <th>المتبقي من الفاتورة</th>
        <th>قيمة مرتجع الفاتورة</th>
        <th>تفاصيل الفاتورة</th>
    </tr>
    </thead>

    <tbody>
    <?php
    $all_bills_total_amount=0;
    $all_bills_client_paid=0;
    $all_bills_remain=0;
    $all_bills_return=0;
    ?>
    <?php foreach($bills as $key=>$bill): ?>
    <?php
    $all_bills_total_amount=$all_bills_total_amount+$bill->client_bill_total_amount;
    $all_bills_client_paid=$all_bills_client_paid+$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm;
    $all_bills_remain=$all_bills_remain+$bill->client_bill_total_remain_amount;
    $all_bills_return=$all_bills_return+$bill->client_bill_total_return_amount;
    ?>

    <tr>
        <td>{{$key+1}}</td>
        <td>
            <label class="label label-info">{{$bill->cashier_full_name}}</label>
        </td>
        <td>
            <label class="label label-info">#{{$bill->client_bill_id}}</label>
        </td>
        <td>
            <label class="label label-default" style="display: block;">{{$bill->client_full_name}}</label>
            <label class="label label-info">{{$bill->client_user_tel}}</label>
        </td>


        <td>
            <label class="label label-info" style="display: block;">{{date("Y-m-d",strtotime($bill->client_bill_date))}}</label>
            <label class="label label-default">{{get_hegri_date(strtotime($bill->client_bill_date))}}</label>
        </td>
        <td>
            <label class="label label-info">{{$bill->client_bill_total_amount}}</label>
        </td>
        <td>
            <label class="label label-info">{{$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm}}</label>
        </td>
        <td>
            <label class="label label-warning">{{$bill->client_bill_total_remain_amount}}</label>
        </td>
        <td>
            <label class="label label-danger">{{$bill->client_bill_total_return_amount}}</label>
        </td>
        <td>
            <a href="{{url("/admin/reports/show_bill_orders/$bill->client_bill_id")}}">
                <span class="label label-primary">مشاهدة</span>
            </a>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>


    <tfoot>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            <label class="label label-info">{{$all_bills_total_amount}}</label>
        </td>
        <td>
            <label class="label label-info">{{$all_bills_client_paid}}</label>
        </td>
        <td>
            <label class="label label-warning">{{$all_bills_remain}}</label>
        </td>
        <td>
            <label class="label label-danger">{{$all_bills_return}}</label>
        </td>
        <td></td>
    </tr>
    </tfoot>

</table>