@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">
            الفلترة
        </div>
        <div class="panel-body">
            <form action="{{url("/admin/reports/total_cashiers_clients_bills")}}" method="get">

                <?php

                    echo generate_depended_selects(
                        $field_name_1="branch_id",
                        $field_label_1="اختار الفرع",
                        $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $field_selected_value_1=$selected_branch_id,
                        $field_required_1="required",
                        $field_class_1="form-control",
                        $field_name_2="branch_admin_id",
                        $field_label_2="اختار المستخدم",
                        $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                        $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                        $field_selected_value_2=$selected_branch_admin_id,
                        $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                        $field_required_2="required",
                        $field_class_2="form-control",
                        $add_blank_options=false,
                        $obj_data=""
                    );

                ?>

                <?php

                    $normal_tags=[
                        "date_from","date_to"
                    ];


                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        "",
                        "4"
                    );

                    $attrs[0]["date_from"]="من";
                    $attrs[0]["date_to"]="الي";


                    $attrs[3]["date_from"]="date";
                    $attrs[3]["date_to"]="date";

                    if(empty($attrs[4]["date_from"])){
                        $attrs[4]["date_from"]=date('Y-m-d',time());
                    }

                    if(empty($attrs[4]["date_to"])){
                        $attrs[4]["date_to"]=date('Y-m-d',strtotime("+1 day",time()));
                    }

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                ?>

                <button class="btn btn-primary" style="margin-top: 25px;">ابحث</button>

            </form>
        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            النتائج
        </div>
        <div class="panel-body">

            <?php
                $all_branches=collect($all_branches)->groupBy("user_id")->all();
            ?>


            <table class="table table-striped table-bordered table_with_paging_with_auto_inc_report"
                style="font-weight: bold;">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>اسم الفرع</td>
                        <td>عدد الفواتير</td>
                        <td>الكاش</td>
                        <td>الشبكة</td>
                        <td>المرتجعات</td>
                        <td>الخصومات</td>
                        <td>العروض</td>
                        <td>الاجمالي قبل القيمة المضافة</td>
                        <td>إجمالي القيمة المضافة</td>
                        <td>الاجمالي</td>
                    </tr>
                </thead>


                <tbody>

                    <?php
                        $total_count_cash=0;
                        $total_count_atm=0;
                        $total_count_return=0;
                        $total_count_coupon_amount=0;
                        $total_count_package_amount=0;
                        $iteration=1;
                        $total_tax = 0;

                        $bills_count = 0;

                    ?>
                    <?php foreach($bills_data as $key=>$bill): ?>
                        <?php
                            $total_commission_val=0;

                            // return values
                            $total_count_cash=$total_count_cash+$bill->client_total_paid_amount_in_cash-($bill->client_bill_return_cash_amount);
                            $total_count_atm=$total_count_atm+($bill->client_total_paid_amount_in_atm-$bill->client_bill_return_atm_amount);
                            $total_count_return=$total_count_return+$bill->client_bill_total_return_amount;
                            $total_count_coupon_amount=$total_count_coupon_amount+$bill->bill_coupon_amount;
                            $total_count_package_amount=$total_count_package_amount+$bill->bill_packages_amount;

                            $total_tax += ($bill->total_client_bill_tax_value-$bill->total_client_bill_tax_return_value);

                            $bills_count += $bill->bills_count;

                        ?>

                        <tr>
                            <td>{{$iteration++}}</td>
                             <td>
                                 <?php if(isset($all_branches[$bill->branch_id])): ?>
                                     {{$all_branches[$bill->branch_id][0]->full_name}}
                                 <?php endif; ?>
                             </td>
                             <td>{{$bill->bills_count}}</td>
                             <td>{{$bill->client_total_paid_amount_in_cash-($bill->client_bill_return_cash_amount)}}</td>
                             <td>{{$bill->client_total_paid_amount_in_atm-$bill->client_bill_return_atm_amount}}</td>
                             <td>{{$bill->client_bill_total_return_amount}}</td>
                             <td>{{$bill->bill_coupon_amount}}</td>
                             <td>{{$bill->bill_packages_amount}}</td>
                             <td>
                                 {{
                                     ($bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm)
                                     - ($bill->client_bill_total_return_amount)
                                     - ($bill->total_client_bill_tax_value-$bill->total_client_bill_tax_return_value)

                                }}
                             </td>
                            <td>{{($bill->total_client_bill_tax_value-$bill->total_client_bill_tax_return_value)}}</td>
                            <td>
                                {{
                                    ($bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm)
                                    - ($bill->client_bill_total_return_amount)
                               }}
                            </td>
                         </tr>
                    <?php endforeach;?>

                    <tr style="font-size: 20px;color: blue;">
                        <td>{{$iteration++}}</td>
                        <td>اجمالي الفروع</td>
                        <td>{{$bills_count}}</td>
                        <td>{{$total_count_cash}}</td>
                        <td>{{$total_count_atm}}</td>
                        <td>{{$total_count_return}}</td>
                        <td>{{$total_count_coupon_amount}}</td>
                        <td>{{$total_count_package_amount}}</td>
                        <td>{{($total_count_cash+$total_count_atm)-($total_tax)}}</td>
                        <td>{{$total_tax}}</td>
                        <td>{{($total_count_cash+$total_count_atm)}}</td>
                    </tr>


                    <tr>
                        <td>{{$iteration++}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h1>{{$company->company_name}}</h1></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                </tbody>

            </table>

        </div>
    </div>


@endsection