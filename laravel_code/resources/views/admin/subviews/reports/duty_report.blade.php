@extends('admin.main_layout')

@section("subview")

    <div class="">

        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    بحث في كل العهد
                </div>
                <div class="panel-body">

                    <form action="{{url("/admin/reports/duty_report")}}">

                        <div class="col-md-12">

                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <?php
                                    echo generate_depended_selects(
                                        $field_name_1="select_branch_id",
                                        $field_label_1="اختار الفرع",
                                        $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                                        $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                                        $field_selected_value_1=(isset($_GET['select_branch_id'])?$_GET['select_branch_id']:""),
                                        $field_required_1="",
                                        $field_class_1="form-control",
                                        $field_name_2="select_branch_admin_id",
                                        $field_label_2="اختار المستخدم",
                                        $field_text_2=convert_inside_obj_to_arr($all_branch_admins,"full_name"),
                                        $field_values_2=convert_inside_obj_to_arr($all_branch_admins,"user_id"),
                                        $field_selected_value_2=(isset($_GET['select_branch_admin_id'])?$_GET['select_branch_admin_id']:""),
                                        $field_2_depend_values=convert_inside_obj_to_arr($all_branch_admins,"related_id"),
                                        $field_required_2="",
                                        $field_class_2="form-control",
                                        $add_blank_options=true,
                                        $obj_data=""
                                    );


                                    ?>
                                </div>

                                <div class="col-md-6"></div>

                            </div>

                            <div class="col-md-12">

                                <div class="col-md-3">
                                    <?php
                                    echo generate_select_tags(
                                        $field_name="duty_type",
                                        $label_name="اختار نوع العهد",
                                        $text=["الكل","المستلمة","المرسلة"],
                                        $values=[-1,1,0],
                                        $selected_value=(isset($_GET['duty_type'])?[$_GET['duty_type']]:[""]),
                                        $class="form-control",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = "",
                                        $grid="12"
                                    );
                                    ?>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">من</label>
                                        <input type="date" name="from_date" id="filter_date_from" value="{{(isset($_GET['from_date'])?date("Y-m-d",strtotime($_GET['from_date'])):"")}}" class="form-control date_input">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">الي</label>
                                        <input type="date" name="to_date" id="filter_date_to" value="{{(isset($_GET['to_date'])?date("Y-m-d",strtotime($_GET['to_date'])):"")}}" class="form-control date_input">
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                                <button class="btn btn-info">نتائج البحث</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    بيانات العهد
                </div>

                <div class="panel-body" style="overflow-x: scroll;">

                    <table class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">

                        <thead>
                        <tr>
                            <td>#</td>
                            <td>من</td>
                            <td>إلي</td>
                            <td>التاريخ</td>
                            <td>المبلغ</td>
                            <td>التفقيط</td>
                            <td>بيان</td>
                            <td>الحالة</td>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $total_money = 0; ?>
                        <?php foreach ($documents as $key => $document): ?>
                        <tr>
                            <td></td>
                            <td><?=$document->{'from_user_full_name'.$en} ?></td>
                            <td><?=$document->{'to_user_full_name'.$en} ?></td>
                            <td><?=$document->duty_document_date ?></td>
                            <td>
                                <?php $total_money = $total_money + $document->duty_document_money; ?>
                                <?=$document->duty_document_money ?>
                            </td>
                            <td><?=$document->duty_document_money_text ?></td>
                            <td><?=$document->duty_document_desc ?></td>
                            <td>

                                <?php if($document->from_user_id == $current_user->user_id): ?>

                                <?php if($document->duty_document_received == 1): ?>
                                <span class="label label-info">مستلمة</span>
                                <?php else: ?>
                                <span class="label label-info">غير مستلمة</span>
                                <?php endif; ?>

                                <?php else: ?>

                                <?php if($document->duty_document_received == 1): ?>
                                    <span class="label label-info">تم الإستلام</span>
                                    <?php else: ?>
                                        <span class="label label-warning">لم يتم الإستلام</span>
                                <?php endif; ?>
                                <?php endif; ?>

                            </td>

                        </tr>
                        <?php endforeach ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><span class="label label-default">{{$total_money}}</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>


        </div>

    </div>

@endsection


