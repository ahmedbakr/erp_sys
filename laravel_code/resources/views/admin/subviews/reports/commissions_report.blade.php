@extends('admin.main_layout')

@section('subview')

    <?php if(is_array($all_users) && count($all_users)): ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url("admin/reports/commissions_report")}}" method="GET">

                <div class="col-md-4 col-md-offset-4">

                    <?php

                    $text_arr = convert_inside_obj_to_arr($all_users,"full_name");
                    $values_arr = convert_inside_obj_to_arr($all_users,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $text_arr = array_merge(["الكل"],convert_inside_obj_to_arr($all_users,"full_name"));
                        $values_arr = array_merge([0],convert_inside_obj_to_arr($all_users,"user_id"));
                    }

                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$text_arr,
                        $values=$values_arr,
                        $selected_value=array($selected_user),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" name="from_date" id="from_date_id" value="{{$start_date}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" name="to_date" id="to_date_id" value="{{$end_date}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <?php endif; ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            عمولات الفروع
        </div>
        <div class="panel-body">

            <table class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اليوم</td>
                    <td>مبيعات الفرع(مجموع المبيعات/عدد المبيعات)</td>
                    <td>عمولة الفرع</td>
                    <td>عمولة الادارة</td>
                    <td>العمولة الكلية</td>
                    <td>حالة العمولة</td>
                </tr>
                </thead>

                <tbody>
                <?php
                    $total_bills = 0;
                    $total_branch = 0;
                    $total_manager = 0;
                    $total_commission = 0;
                ?>
                <?php foreach ($all_commissions as $key => $commission): ?>

                <tr>
                    <td></td>
                    <td>
                        {{$commission->full_name}}
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$commission->day_date}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-default">
                            <?php $total_bills += $commission->total_day_bills_money; ?>
                            {{$commission->total_day_bills_money}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            <?php $total_branch += $commission->branch_commission_value; ?>
                            {{$commission->branch_commission_value}}
                        </span>
                    </td>

                    <td>
                        <span class="label label-success">
                            <?php $total_manager += $commission->factory_commission_value; ?>
                            {{$commission->factory_commission_value}}
                        </span>
                    </td>

                    <td>
                        <span class="label label-success">
                            <?php $total_commission += $commission->total_commission_value; ?>
                            {{$commission->total_commission_value}}
                        </span>
                    </td>

                    <td>

                        <?php if($commission->is_received == 0): ?>
                            <span class="label label-warning">مستلمه</span>
                        <?php else: ?>
                        <span class="label label-success">مستلمه</span>
                        <?php endif; ?>

                    </td>
                </tr>

                <?php endforeach ?>
                <td></td>
                <td></td>
                <td></td>
                <td><span class="label label-default">{{$total_bills}}</span></td>
                <td><span class="label label-default">{{$total_branch}}</span></td>
                <td><span class="label label-default">{{$total_manager}}</span></td>
                <td><span class="label label-default">{{$total_commission}}</span></td>
                <td></td>

                </tbody>

            </table>

        </div>
    </div>




@endsection
