@extends('admin.main_layout')

@section('subview')

    @include("common.category.accounts_tree.tree_modal")

    <style>
        .general_self_edit{
            display: block;
        }
        .general_self_edit:hover{
            border: 2px dashed #0000cc;
        }
    </style>


    <div class="panel panel-info">
        <div class="panel-heading">العمليات</div>
        <div class="panel-body">
            <form action="{{url("/admin/reports/products_on_factory_stock")}}" method="get">


                <div class="col-md-4" style="margin-top: 20px;">
                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="single"
                            data-div_to_show_selection=".select_cats"
                            data-input_field_name="cat_ids"
                    >
                        اظهر الشجرة لتختار التصنيفات
                    </button>

                    <div class="select_cats" style="text-align: center;">

                    </div>
                </div>

                <div class="col-md-2" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-primary">ابحث</button>
                </div>

            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">{{$meta_title}}</div>
        <div class="panel-body" style="overflow-x: scroll;">


            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td></td>
                    <td>اسم المنتج</td>
                    <td>كمية المنتج</td>
                    <td>حد الطلب</td>
                    <td>وحدة القياس</td>
                    <td>الباركود</td>
                </tr>
                </thead>


                <tbody>
                <?php $total_quantity = 0; ?>
                <?php foreach ($all_products as $key => $product): ?>
                <tr id="row<?= $product->pro_id ?>">
                    <td></td>
                    <td>
                        <span class="label label-info"><?= $product->pro_name ?></span>
                    </td>
                    <td>
                        <span class="label label-default">
                            <?= $product->pro_quantity ?>
                            <?php $total_quantity += $product->pro_quantity; ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-danger">
                            <?= $product->pro_limit ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            <?= $product->pro_unit_name ?>
                        </span>
                    </td>
                    <td>
                        <?php if(!empty($product->pro_barcode)): ?>
                        <span class="label label-default">
                        <?= $product->pro_barcode ?>
                    </span>
                        <?php endif; ?>
                    </td>

                </tr>
                <?php endforeach ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td>{{$total_quantity}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>

            </table>

        </div>
    </div>


@endsection