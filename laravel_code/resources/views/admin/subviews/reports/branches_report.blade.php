@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            الفروع
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging_with_auto_inc" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>كود الفرع</th>
                    <th>اسم الفرع</th>
                    <th>ايميل الفرع</th>
                    <th>تليفون الفرع</th>
                    <th>موبايل الفرع</th>
                    <th>فاكس الفرع</th>
                    <th>عنوان الفرع</th>
                    <th>رقم السجل التجاري</th>
                    <th>تاريخ إنتهاء السجل التجاري</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($all_branches as $key => $user): ?>
                <tr id="row<?= $user->user_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$user->user_id}}</td>

                    <td>
                        <?=$user->full_name ?>
                        <br>
                        <?=$user->full_name_en ?>
                    </td>
                    <td><?=$user->email ?></td>

                    <td><?=$user->user_tel ?></td>
                    <td><?=$user->user_mobile ?></td>
                    <td><?=$user->user_fax ?></td>
                    <td><?=$user->user_address ?></td>
                    <td><?=$user->commercial_record_number ?></td>
                    <td><?=$user->commercial_record_expire_date ?></td>

                </tr>
                <?php endforeach ?>
                </tbody>


            </table>


        </div>
    </div>

@endsection
