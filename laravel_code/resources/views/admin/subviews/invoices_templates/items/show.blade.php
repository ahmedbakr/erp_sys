@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
            جميع العناصر الخاصة بالقالب
            {{$invoice_data->template_name}}
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم العنصر</td>
            <td>نوع العنصر</td>
            <td>إخفاء العنصر</td>
            <td>تعديل</td>
            {{--<td>مسح</td>--}}
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($invoice_items as $key => $item): ?>
        <tr id="row{{$item->item_id}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-default">
                    <?= $item->item_name ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $item->item_type ?>
                </span>
            </td>
            <td>
                <?php
                echo
                generate_multi_accepters(
                        $accepturl=url( "/admin/invoices_templates/change_template_item_status"),
                        $item_obj=$item,
                        $item_primary_col="item_id",
                        $accept_or_refuse_col="item_hide",
                        $model='App\models\invoices\invoices_template_content_m',
                        $accepters_data=["0"=>"إظهار","1"=>"إخفاء"]
                );
                ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/invoice_template_items","edit_action",$current_user)): ?>
                <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/save_template_item/'.$item->inv_temp_id.'/'.$item->item_id)}}"> تعديل </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <?php if(1!=1): ?>

            <td>
                <?php if(check_permission($user_permissions,"admin/invoice_template_items","delete_action",$current_user)): ?>
                <a href='#' class="general_remove_item" data-tablename="App\models\invoices\invoices_template_content_m" data-deleteurl="<?= url("/admin/invoices_templates/remove_template_item") ?>" data-itemid="<?= $item->item_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <?php endif; ?>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(1!=1&&check_permission($user_permissions,"admin/invoice_template_items","add_action",$current_user)): ?>
    <div class="col-md-3 col-md-offset-2">
        <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/save_template_item/'.$invoice_data->inv_temp_id)}}"> إضافة عنصر جديد </a>
    </div>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/invoice_template_items","save_items_position",$current_user)): ?>
    <div class="col-md-3 col-md-offset-2">
        <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/preview_template_items/'.$invoice_data->inv_temp_id)}}"> تعديل شكل قالب الفاتورة </a>
    </div>
    <?php endif; ?>
    <br><br>

@endsection