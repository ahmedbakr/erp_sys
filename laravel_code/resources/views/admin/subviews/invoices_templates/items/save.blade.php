@extends('admin.main_layout')

@section('subview')

    <!--new_editor-->
    <script src="{{url("/public_html/ckeditor/ckeditor.js")}}" type="text/javascript"></script>
    <script src="{{url("/public_html/ckeditor/adapters/jquery.js")}}" type="text/javascript"></script>

    <?php

    $header_txt = '<i class="fa fa-plus"></i> إضافة عنصر جديد للقالب رقم '.$inv_temp_id;
    $item_id = "";
    $temp_img_path = "";

    if (is_object($item_obj))
    {
        $header_txt = '<i class="fa fa-edit"></i> تعديل عنصر للقالب '.$item_obj->item_name;
        $item_id = $item_obj->item_id;

        if (!empty($item_obj->temp_img_path))
        {
            $temp_img_path = url($item_obj->temp_img_path);
        }

    }

    ?>

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {!! $header_txt !!}
                    </h3>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/invoices_templates/save_template_item/".$inv_temp_id."/".$item_id)?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading"> بيانات العنصر</div>
                    <div class="panel-body">
                        <div class="row">
                            <?php

                            if ($item_obj->item_type == "image")
                            {
                                echo generate_img_tags_for_form(
                                        $filed_name="item_img_file",
                                        $filed_label="item_img_file",
                                        $required_field="",
                                        $checkbox_field_name="item_img_checkbox",
                                        $need_alt_title="no",
                                        $required_alt_title="",
                                        $old_path_value=$temp_img_path,
                                        $old_title_value="",
                                        $old_alt_value="",
                                        $recomended_size="",
                                        $disalbed="disabled",
                                        $displayed_img_width="100",
                                        $display_label="صورة العنصر اذا كان نوع العنصر صورة",
                                        $img_obj = ""
                                );
                            }
                            elseif($item_obj->item_type == "static")
                            {


                                $normal_tags=array("item_label");
                                $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data=$item_obj,
                                        "yes",
                                        $required = ""
                                );

                                $attrs[0]["item_label"]="الوصف";


                                $attrs[2]["item_label"]="required";

                                $attrs[3]["item_label"]="textarea";

                                $attrs[5]["item_label"] .= " ckeditor";

                                echo
                                generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                );

                            }


                            ?>

                        </div>

                    </div>

                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">
                    <?php if(1!=1): ?>
                    <a href="{{url("/admin/invoices_templates/save_template_item/".$inv_temp_id)}}" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">إضافة جديد</a>
                    <?php endif; ?>
                </div>

            </form>


        </div>

    </div>


@endsection