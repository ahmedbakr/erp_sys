<style>
    .ui-resizable-helper { border: 2px dotted #00F; }
</style>

<div class="invoice-box invoice-box-container" style="direction: {{$template_data->table_direction}}">
    <table cellpadding="0" cellspacing="0">
        <?php if($template_items["logo"][0]->item_hide==0): ?>

            <div class="logo_invoice_item logo_item" data-item_id="{{$template_items["logo"][0]->item_id}}"
                 style = "
                         position: absolute;
                         left:{{get_px_base_on_direction($template_items["logo"][0]->item_position_left,$template_data->table_direction)}};
                         top:{{get_px_base_on_direction($template_items["logo"][0]->item_position_top,$template_data->table_direction)}};
                         ">
                <img class="logo_resize_item" src="{{get_image_or_default($template_items["logo"][0]->temp_img_path)}}"
                     style = "
                             position: relative;
                             width: {{$template_items["logo"][0]->item_width}};
                             height: {{$template_items["logo"][0]->item_height}};
                             ">
            </div>

        <?php endif; ?>

    </table>

    <?php if($template_items["branch_name"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["branch_name"][0]->item_width}};
                 height: {{$template_items["branch_name"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["branch_name"][0]->item_id}}">
        <b style="font-weight: bold;">{{$template_items["branch_name"][0]->item_label}}</b>: المروج<br>
    </div>
    <?php endif; ?>
    <?php if($template_items["invoice_number"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["invoice_number"][0]->item_width}};
                 height: {{$template_items["invoice_number"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["invoice_number"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["invoice_number"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["invoice_number"][0]->item_id}}">
        <b>{{$template_items["invoice_number"][0]->item_label}}</b>: 123<br>
    </div>
    <?php endif; ?>
    <?php if($template_items["cashier_name"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["cashier_name"][0]->item_width}};
                 height: {{$template_items["cashier_name"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["cashier_name"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["cashier_name"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["cashier_name"][0]->item_id}}">
        <b>{{$template_items["cashier_name"][0]->item_label}}</b>: Seoera<br>
    </div>

    <?php endif; ?>
    <?php if($template_items["customer_tel"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["customer_tel"][0]->item_width}};
                 height: {{$template_items["customer_tel"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["customer_tel"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["customer_tel"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["customer_tel"][0]->item_id}}">
        <b>{{$template_items["customer_tel"][0]->item_label}}</b>: 0123456789<br>
    </div>
    <?php endif; ?>
    <?php if($template_items["created"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["created"][0]->item_width}};
                 height: {{$template_items["created"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["created"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["created"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["created"][0]->item_id}}">
        <b>{{$template_items["created"][0]->item_label}}</b>: 2017-02-27 at 10:09 AM <br>
    </div>
    <?php endif; ?>


    <div class="invoice_item format_table"
         style="
                 position: relative;
                 left:{{get_px_base_on_direction($template_data->table_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_data->table_position_top,$template_data->table_direction)}};
                 width: {{$template_data->table_width}};height: {{$template_data->table_height}};
                 " >

        <table cellpadding="0" cellspacing="0">

            <tr class="heading">
                <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_name"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_desc"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_quantity"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_price"][0]->item_label}}
                    </td>
                <?php endif; ?>
            </tr>


            <?php if($template_items["items_pre_total"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                        <td>
                            <b>{{$template_items["items_pre_total"][0]->item_label}}</b>: 300
                        </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

            <?php if($template_items["items_coupon"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["items_coupon"][0]->item_label}}</b>: 123456
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>


            <?php if($template_items["items_discount"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["items_discount"][0]->item_label}}</b>: 50
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>


            <?php if($template_items["items_post_total"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["items_post_total"][0]->item_label}}</b>: 250
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

            <?php if($template_items["total_cash_paid"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["total_cash_paid"][0]->item_label}}</b>: 50
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

            <?php if($template_items["total_atm_paid"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["total_atm_paid"][0]->item_label}}</b>: 150
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

            <?php if($template_items["total_paid"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["total_paid"][0]->item_label}}</b>: 200
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

            <?php if($template_items["total_remain"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{{$template_items["total_remain"][0]->item_label}}</b>: 50
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endif; ?>

        </table>

    </div>

    <?php if($template_items["invoice_note"][0]->item_hide==0): ?>
        <div class="invoice_conditions invoice_item"
             style = "
                     position: relative;
                     width: {{$template_items["invoice_note"][0]->item_width}};
                     height: {{$template_items["invoice_note"][0]->item_height}};
                     left:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_left,$template_data->table_direction)}};
                     top:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_top,$template_data->table_direction)}};
                     "
             data-item_id="{{$template_items["invoice_note"][0]->item_id}}">
            <hr>
            {!! $template_items["invoice_note"][0]->item_label !!}
        </div>
    <?php endif; ?>


</div>