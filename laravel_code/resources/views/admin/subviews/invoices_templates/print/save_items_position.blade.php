@extends('admin.main_layout')

@section('subview')

    <link rel="stylesheet" href="{{url('public_html/admin/invoices/template1/css/style.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="{{url('public_html/jscode/admin/templates.js')}}"></script>


    <style>
        .invoice-box{
            width: {{convert_from_cm_to_px($template_data->template_width)}}px;
            height: {{convert_from_cm_to_px($template_data->template_height)}}px;
        }
    </style>

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        قالب
                        {{$template_data->template_name}}
                    </h3>
                </div>
            </div>

            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }


            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <div class="panel panel-info">
                <div class="panel-heading" role="button" data-toggle="collapse"
                     href="#template_body" aria-expanded="true" aria-controls="template_body">شكل قالب الفاتورة</div>
                <div class="panel-body">
                    <div class="row custom_template_html">
                        @include('admin.subviews.invoices_templates.print.template_block')
                    </div>
                </div>
                <button type="button" class="col-md-3 col-md-offset-4 btn btn-primary btn-lg save_template_html" data-inv_temp_id = "{{$template_data->inv_temp_id}}">حفظ</button>
                <div class="show_template_errors"></div>
            </div>



        </div>

    </div>


@endsection