@extends('admin.main_layout')

@section('subview')

    <link rel="stylesheet" href="{{url('public_html/admin/invoices/template1/css/style.css')}}">
    <?php if(false): ?>
    <!-- for resizable and dragable -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="{{url('public_html/jscode/admin/templates.js')}}"></script>
    <?php endif; ?>


    <style>
        .invoice-box{
            width: {{convert_from_cm_to_px($template_data->template_width)}}px;
            height: {{convert_from_cm_to_px($template_data->template_height)}}px;
        }
    </style>


        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center;height: 100px;">
                    <div class="col-md-3">
                        <button class="btn btn-success btn-lg fire_print_event" style="float: right;height: 100px;margin-top: -16px;width: 63%;">طباعة</button>
                    </div>
                    <div class="col-md-4">
                        <h3>
                            قالب
                            {{$template_data->template_name}}
                        </h3>
                    </div>
                </div>
            </div>


            <?php if(is_array($_GET) && count($_GET)): ?>
                <div class="panel panel-info">
                    <div class="panel-heading" role="button" data-toggle="collapse"
                         href="#template_body" aria-expanded="true" aria-controls="template_body"> شكل قالب الفاتورة
                        <button class="btn btn-info fire_print_event">طباعة</button>
                    </div>
                    <div class="panel-body">
                        <div class="row custom_template_html">
                            @include('admin.subviews.invoices_templates.print_today_bills.template_block')
                        </div>
                    </div>
                </div>
            <?php endif; ?>



        </div>


@endsection