<style>
    .ui-resizable-helper { border: 2px dotted #00F; }
</style>


<?php
    $total_orders_money = 0;
?>

<div class="invoice-box invoice-box-container resize_invoice_box" style="direction: {{$template_data->table_direction}};height: auto;">
    <table cellpadding="0" cellspacing="0">
        <?php if($template_items["logo"][0]->item_hide==0): ?>

            <div class="logo_invoice_item logo_item" data-item_id="{{$template_items["logo"][0]->item_id}}"
                 style = "
                         position: absolute;
                         left:{{get_px_base_on_direction($template_items["logo"][0]->item_position_left,$template_data->table_direction)}};
                         top:{{get_px_base_on_direction($template_items["logo"][0]->item_position_top,$template_data->table_direction)}};
                         ">
                <img class="logo_resize_item" src="{{get_image_or_default($template_items["logo"][0]->temp_img_path)}}"
                     style = "
                             position: relative;
                             width: {{$template_items["logo"][0]->item_width}};
                             height: {{$template_items["logo"][0]->item_height}};
                             ">
            </div>

        <?php endif; ?>

    </table>

    <?php if($template_items["branch_name"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["branch_name"][0]->item_width}};
                 height: {{$template_items["branch_name"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["branch_name"][0]->item_id}}">
        <b style="font-weight: bold;font-size: 24px;">{{$branch_name}}</b><br>
    </div>
    <?php endif; ?>

    <?php if($template_items["created"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["created"][0]->item_width}};
                 height: {{$template_items["created"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["created"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["created"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["created"][0]->item_id}}">
        <b>{{$template_items["created"][0]->item_label}}</b>: {{date("Y-m-d H:i:s")}} <br>
    </div>
    <?php endif; ?>

    <?php if($template_items["created"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["created"][0]->item_width}};
                 height: {{$template_items["created"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["created"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["created"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["created"][0]->item_id}}">
        <b>{{$template_items["cashier_name"][0]->item_label}}</b>: {{$current_user->full_name}} <br>
    </div>
    <?php endif; ?>


    <div class="invoice_item format_table"
         style="
                 position: relative;
                 left:{{get_px_base_on_direction($template_data->table_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_data->table_position_top,$template_data->table_direction)}};
                 width: {{$template_data->table_width}};
                 {{--height: {{$template_data->table_height}};--}}
                 " >

        <table cellpadding="0" cellspacing="0">

            <tr class="heading">
                <td>
                    الي{{$end_day_date}}
                </td>
                <td>
                    من {{$start_day_date}}
                </td>
            </tr>

            <tr>
                <td>{{isset($_GET['today_cash'])?$_GET['today_cash']:0}} ريال </td>
                <td>نقدي </td>
            </tr>
            <tr>
                <td>{{isset($_GET['today_atm'])?$_GET['today_atm']:0}} ريال </td>
                <td>شبكة </td>
            </tr>
            <tr>
                <td>{{isset($_GET['today_remain'])?$_GET['today_remain']:0}} ريال </td>
                <td>أجل </td>
            </tr>
            <tr>
                <td>{{isset($_GET['today_return'])?$_GET['today_return']:0}} ريال </td>
                <td>مرتجع </td>
            </tr>
            <tr>
                <td>{{isset($_GET['today_total_before_tax'])?$_GET['today_total_before_tax']:0}} ريال </td>
                <td>الإجمالي قبل القيمة المضافة </td>
            </tr>
            <tr>
                <td>{{isset($_GET['total_tax'])?$_GET['total_tax']:0}} ريال </td>
                <td>إجمالي القيمة المضافة </td>
            </tr>
            <tr>
                <td>{{isset($_GET['today_total'])?$_GET['today_total']:0}} ريال </td>
                <td>الإجمالي </td>
            </tr>
        </table>
        <?php if($template_items["invoice_note"][0]->item_hide==0): ?>
        <div class="invoice_conditions invoice_item"
             data-item_id="{{$template_items["invoice_note"][0]->item_id}}">
            <hr>
            {!! $template_items["invoice_note"][0]->item_label !!}
        </div>
        <?php endif; ?>
    </div>

    <?php if(false && $template_items["invoice_note"][0]->item_hide==0): ?>
        <div class="invoice_conditions invoice_item"
             style = "
                     position: relative;
                     width: {{$template_items["invoice_note"][0]->item_width}};
                     height: {{$template_items["invoice_note"][0]->item_height}};
                     margin-top: 20px;
                     left:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_left,$template_data->table_direction)}};
                     top:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_top,$template_data->table_direction)}};
                     "
             data-item_id="{{$template_items["invoice_note"][0]->item_id}}">
            <hr>
            {!! $template_items["invoice_note"][0]->item_label !!}
        </div>
    <?php endif; ?>


</div>