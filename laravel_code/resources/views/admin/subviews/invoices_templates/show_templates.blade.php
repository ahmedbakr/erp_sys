@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
            جميع القوالب الخاصة بالفواتير
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <td>#</td>
                <td>اسم الفرع</td>
                <td>اسم القالب</td>
                <td>عرض القالب</td>
                <td>طول القالب</td>
                <td>طريقه عرض القالب</td>
                <td>تاريخ الانشاء</td>
                <td>القالب الاساسي</td>
                <td>عناصر القالب</td>
                <td>تعديل القالب</td>
                <td>مسح القالب</td>
            </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($all_invoices as $key => $invoice): ?>
        <tr id="row{{$invoice->inv_temp_id}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-info">
                    <?= $invoice->full_name ?>
                </span>
            </td>
            <td>
                <span class="label label-default">
                    <?= $invoice->template_name ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $invoice->template_width ?>
                </span>
            </td>
            <td>
                <span class="label label-warning">
                    <?= $invoice->template_height ?>
                </span>
            </td>
            <td>
                <span class="label label-warning">
                    <?= $invoice->table_direction ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $invoice->invoice_created_date ?>
                </span>
                <span class="label label-success">
                    <?= \Carbon\Carbon::createFromTimestamp(strtotime($invoice->invoice_created_date))->diffForHumans() ?>
                </span>
            </td>
            <td>
                <?php
                echo
                generate_multi_accepters(
                        $accepturl=url( "/admin/invoices_templates/change_template_default"),
                        $item_obj=$invoice,
                        $item_primary_col="inv_temp_id",
                        $accept_or_refuse_col="set_default",
                        $model='App\models\invoices\invoices_templates_m',
                        $accepters_data=["0"=>"لا","1"=>"نعم"]
                );
                ?>
            </td>
            <td>
                <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/show_invoice_items/'.$invoice->inv_temp_id)}}"> مشاهدة </a>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/invoices_templates","edit_action",$current_user)): ?>
                <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/save_template/'.$invoice->inv_temp_id)}}"> تعديل </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/invoices_templates","delete_action",$current_user)): ?>
                <a href='#' class="general_remove_item" data-tablename="App\models\invoices\invoices_templates_m" data-deleteurl="<?= url("/admin/invoices_templates/remove_template") ?>" data-itemid="<?= $invoice->inv_temp_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                </a>
                <?php else: ?>
                <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(check_permission($user_permissions,"admin/invoices_templates","add_action",$current_user)): ?>
        <div class="col-md-3 col-md-offset-2">
            <a class="btn btn-primary btn-block" href="{{url('/admin/invoices_templates/save_template')}}"> إضافة قالب جديد </a>
        </div>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/print_client_bill","show_action",$current_user)): ?>
        <div class="col-md-3 col-md-offset-2">
            <a class="btn btn-primary btn-block" href="{{url('/print_client_bill')}}"> طباعة فاتورة جديدة </a>
        </div>
    <?php endif; ?>


@endsection