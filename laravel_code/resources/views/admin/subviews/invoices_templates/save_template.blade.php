@extends('admin.main_layout')

@section('subview')

    <!--new_editor-->
    <script src="{{url("/public_html/ckeditor/ckeditor.js")}}" type="text/javascript"></script>
    <script src="{{url("/public_html/ckeditor/adapters/jquery.js")}}" type="text/javascript"></script>

    <?php

    $header_txt = '<i class="fa fa-plus"></i> إضافة قالب فاتورة جديد ';
    $inv_temp_id = "";

    if (is_object($template))
    {
        $header_txt = '<i class="fa fa-edit"></i> تعديل قالب فاتورة '.$template->item_name;
        $inv_temp_id = $template->inv_temp_id;
    }

    ?>

    <div class="">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {!! $header_txt !!}
                    </h3>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/invoices_templates/save_template/".$inv_temp_id)?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <?php if(is_array($all_branches) && count($all_branches)): ?>
                    <div class="panel panel-info">
                        <div class="panel-heading"> إختار الفرع</div>
                        <div class="panel-body">
                            <div class="row">
                                <?php

                                $all_branches_txt = convert_inside_obj_to_arr($all_branches,"full_name");
                                $all_branches_ids = convert_inside_obj_to_arr($all_branches,"user_id");


                                echo generate_select_tags(
                                        $field_name="branch_id",
                                        $label_name="إختار اسم الفرع",
                                        $text=$all_branches_txt,
                                        $values=$all_branches_ids,
                                        $selected_value="",
                                        $class="form-control select_2_class",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = $template);
                                ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="panel panel-info">
                    <div class="panel-heading"> بيانات القالب اي احجام تكون بالسم</div>
                    <div class="panel-body">
                        <div class="row">
                            <?php

                            echo generate_select_tags(
                                    $field_name="table_direction",
                                    $label_name="طريقه عرض البيانات",
                                    $text=["من اليمين للشمال","من الشمال لليمين"],
                                    $values=["rtl","ltr"],
                                    $selected_value="",
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = $template);


                            $normal_tags=array("template_name","template_width","template_height");
                            $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $lang_data=$template,
                                    "yes",
                                    $required = ""
                            );

                            $attrs[0]["template_name"]="اسم القالب";
                            $attrs[0]["template_width"]="عرض القالب بالسم";
                            $attrs[0]["template_height"]="طول القالب بالسم";


                            $attrs[2]["item_name"]="required";

                            $attrs[3]["template_name"]="text";
                            $attrs[3]["template_width"]="number";
                            $attrs[3]["template_height"]="number";


                            echo
                            generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                            );

                            ?>

                        </div>

                    </div>

                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">
                </div>

            </form>


        </div>

    </div>


@endsection