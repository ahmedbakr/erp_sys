<style>
    .ui-resizable-helper { border: 2px dotted #00F; }
</style>


<?php
    $total_orders_money = 0;
?>

<div class="invoice-box invoice-box-container resize_invoice_box" style="direction: {{$template_data->table_direction}};height: auto;">
    <table cellpadding="0" cellspacing="0">
        <?php if($template_items["logo"][0]->item_hide==0): ?>

            <div class="logo_invoice_item logo_item" data-item_id="{{$template_items["logo"][0]->item_id}}"
                 style = "
                         position: absolute;
                         left:{{get_px_base_on_direction($template_items["logo"][0]->item_position_left,$template_data->table_direction)}};
                         top:{{get_px_base_on_direction($template_items["logo"][0]->item_position_top,$template_data->table_direction)}};
                         ">
                <img class="logo_resize_item" src="{{get_image_or_default($template_items["logo"][0]->temp_img_path)}}"
                     style = "
                             position: relative;
                             width: {{$template_items["logo"][0]->item_width}};
                             height: {{$template_items["logo"][0]->item_height}};
                             ">
            </div>

        <?php endif; ?>

    </table>

    <?php if($template_items["branch_name"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["branch_name"][0]->item_width}};
                 height: {{$template_items["branch_name"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["branch_name"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["branch_name"][0]->item_id}}">
        <b style="font-weight: bold;font-size: 24px;">{{$bill_data->branch_full_name}}</b><br>
    </div>
    <?php endif; ?>

    <?php if($template_items["invoice_number"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["invoice_number"][0]->item_width}};
                 height: {{$template_items["invoice_number"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["invoice_number"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["invoice_number"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["invoice_number"][0]->item_id}}">
        <b>{{$template_items["invoice_number"][0]->item_label}}</b>: {{$bill_data->client_bill_id}}<br>
    </div>
    <?php endif; ?>
    <?php if($template_items["cashier_name"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["cashier_name"][0]->item_width}};
                 height: {{$template_items["cashier_name"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["cashier_name"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["cashier_name"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["cashier_name"][0]->item_id}}">
        <b>{{$template_items["cashier_name"][0]->item_label}}</b>: {{$bill_data->cashier_full_name}}<br>
    </div>

    <?php endif; ?>
    <?php if($template_items["customer_tel"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["customer_tel"][0]->item_width}};
                 height: {{$template_items["customer_tel"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["customer_tel"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["customer_tel"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["customer_tel"][0]->item_id}}">
        <b>{{$template_items["customer_tel"][0]->item_label}}</b>: {{$bill_data->client_user_tel}}<br>
    </div>
    <?php endif; ?>
    <?php if($template_items["created"][0]->item_hide==0): ?>
    <div class="invoice_item"
         style = "
                 position: relative;
                 width: {{$template_items["created"][0]->item_width}};
                 height: {{$template_items["created"][0]->item_height}};
                 left:{{get_px_base_on_direction($template_items["created"][0]->item_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_items["created"][0]->item_position_top,$template_data->table_direction)}};
                 "
         data-item_id="{{$template_items["created"][0]->item_id}}">
        <b>{{$template_items["created"][0]->item_label}}</b>: {{$bill_data->client_bill_date}} <br>
    </div>
    <?php endif; ?>


    <div class="invoice_item format_table"
         style="
                 position: relative;
                 left:{{get_px_base_on_direction($template_data->table_position_left,$template_data->table_direction)}};
                 top:{{get_px_base_on_direction($template_data->table_position_top,$template_data->table_direction)}};
                 width: {{$template_data->table_width}};
                 {{--height: {{$template_data->table_height}};--}}
                 " >

        <table cellpadding="0" cellspacing="0">

            <tr class="heading">
                <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_name"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_desc"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_quantity"][0]->item_label}}
                    </td>
                <?php endif; ?>
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        {{$template_items["item_price"][0]->item_label}}
                    </td>
                <?php endif; ?>
            </tr>


            <?php foreach($bill_orders as $key => $order): ?>
                <?php
                    $total_orders_money += ($order->order_quantity * $order->pro_original_price);
                ?>

                <tr>
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td>
                        {{$order->pro_name}}
                    </td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td>
                        <?php if($order->is_gift == 1): ?>
                            منتج هدية
                        <?php endif; ?>
                    </td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td>
                        {{$order->order_quantity}}
                    </td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        {{$order->pro_original_price}}
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endforeach; ?>


            <?php if($template_items["items_pre_total"][0]->item_hide==0): ?>

                <tr class="total">
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{!! $template_items["items_pre_total"][0]->item_label !!}</b>:
                        {{$total_orders_money}}
                    </td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td>
                        <?php
                        $all_quantities = convert_inside_obj_to_arr($bill_orders,"order_quantity");
                        echo array_sum($all_quantities)." X";
                        ?>
                    </td>
                    <?php endif; ?>
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                        <td></td>
                    <?php endif; ?>

                </tr>

            <?php endif; ?>


            <?php
            $bill_data->bill_packages = json_decode($bill_data->bill_packages);
            ?>
            <?php if($template_items["items_coupon"][0]->item_hide==0 && is_array($bill_data->bill_packages) && count($bill_data->bill_packages)): ?>

            <tr class="total">
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                <td colspan="3">
                    <b>Offers </b>:
                    <br>
                    <?php foreach($bill_data->bill_packages as $key => $package_item): ?>
                    {{$package_item}}
                    <br>
                    <?php endforeach; ?>
                </td>
                <?php endif; ?>
                <?php if(false && $template_items["item_name"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if(false && $template_items["item_desc"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
            </tr>

            <tr class="total">
                <?php if($template_items["item_name"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                <td>
                    <b>{!! $template_items["items_discount"][0]->item_label !!}</b>: {{$bill_data->bill_packages_amount}}
                </td>
                <?php endif; ?>
            </tr>

            <?php endif; ?>



            <?php if($bill_type == 0): ?>
                <?php
                    $bill_data->bill_coupon = json_decode($bill_data->bill_coupon);
                ?>
                <?php if($template_items["items_coupon"][0]->item_hide==0 && is_array($bill_data->bill_coupon) && count($bill_data->bill_coupon)): ?>

                    <tr class="total">
                        <?php if($template_items["item_price"][0]->item_hide==0): ?>
                        <td colspan="3">
                            <b>{!! $template_items["items_coupon"][0]->item_label !!}</b>:
                            <br>
                            <?php foreach($bill_data->bill_coupon as $key => $coupon_item): ?>
                                {{$coupon_item}}
                            <br>
                            <?php endforeach; ?>
                        </td>
                        <?php endif; ?>
                        <?php if(false && $template_items["item_name"][0]->item_hide==0): ?>
                        <td></td>
                        <?php endif; ?>
                        <?php if(false && $template_items["item_desc"][0]->item_hide==0): ?>
                        <td></td>
                        <?php endif; ?>
                        <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                        <td></td>
                        <?php endif; ?>
                    </tr>

                <?php endif; ?>



                <?php if($template_items["items_discount"][0]->item_hide==0 && $bill_data->bill_coupon_amount > 0): ?>

                <tr class="total">
                    <?php if($template_items["item_name"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                    <td></td>
                    <?php endif; ?>
                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                    <td>
                        <b>{!! $template_items["items_discount"][0]->item_label !!}</b>: {{$bill_data->bill_coupon_amount}}
                    </td>
                    <?php endif; ?>
                </tr>

                <?php endif; ?>




                <?php if($template_items["items_post_total"][0]->item_hide==0): ?>

                    <tr class="total">
                        <?php if($template_items["item_name"][0]->item_hide==0): ?>
                        <td></td>
                        <?php endif; ?>
                        <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                        <td></td>
                        <?php endif; ?>
                        <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                        <td>
                            <?php
                                $all_quantities = convert_inside_obj_to_arr($bill_orders,"order_quantity");
                                echo array_sum($all_quantities)." X";
                            ?>
                        </td>
                        <?php endif; ?>
                        <?php if($template_items["item_price"][0]->item_hide==0): ?>
                        <td>
                            <b>{!! $template_items["items_post_total"][0]->item_label !!}</b>: {{$bill_data->client_bill_total_amount-$bill_data->bill_tax_value}}
                            <?php if($bill_data->bill_tax_value > 0): ?>
                            <br>
                            <b>5% ضريبه القيمة المضافة</b>: {{$bill_data->bill_tax_value}}
                            <br>
                            <b>{!! $template_items["items_post_total"][0]->item_label !!}</b>: {{$bill_data->client_bill_total_amount}}
                            <?php endif; ?>
                        </td>
                        <?php endif; ?>
                    </tr>

                <?php endif; ?>

            <?php endif; ?>


            <?php if($template_items["total_cash_paid"][0]->item_hide==0): ?>

            <tr class="total">
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                <td colspan="2">
                    <b>{!! $template_items["total_cash_paid"][0]->item_label !!}</b>: {{$bill_data->client_total_paid_amount_in_cash}}
                </td>
                <?php endif; ?>
                <?php if($template_items["total_atm_paid"][0]->item_hide==0): ?>

                    <?php if($template_items["item_price"][0]->item_hide==0): ?>
                        <td colspan="2">
                            <b>{!! $template_items["total_atm_paid"][0]->item_label !!}</b>: {{$bill_data->client_total_paid_amount_in_atm}}
                        </td>
                    <?php endif; ?>

                <?php endif; ?>
                <?php if(false && $template_items["item_desc"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if(false && $template_items["item_quantity"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>

            </tr>

            <?php endif; ?>


            <?php if(false && $template_items["total_paid"][0]->item_hide==0): ?>

            <tr class="total">
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                <td>
                    <b>{!! $template_items["total_paid"][0]->item_label !!}</b>:
                    {{$bill_data->client_total_paid_amount_in_cash + $bill_data->client_total_paid_amount_in_atm}}
                </td>
                <?php endif; ?>
                <?php if($template_items["item_name"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
            </tr>

            <?php endif; ?>

            <?php if($template_items["total_remain"][0]->item_hide==0): ?>

            <tr class="total">
                <?php if($template_items["item_price"][0]->item_hide==0): ?>
                <td>
                    <b>{!! $template_items["total_remain"][0]->item_label !!}</b>:
                    {{$bill_data->client_bill_total_remain_amount}}
                </td>
                <?php endif; ?>
                <?php if($template_items["item_name"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_desc"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
                <?php if($template_items["item_quantity"][0]->item_hide==0): ?>
                <td></td>
                <?php endif; ?>
            </tr>

            <?php endif; ?>

        </table>
        <?php if($template_items["invoice_note"][0]->item_hide==0): ?>
        <div class="invoice_conditions invoice_item"
             data-item_id="{{$template_items["invoice_note"][0]->item_id}}">
            <hr>
            {!! $template_items["invoice_note"][0]->item_label !!}
        </div>
        <?php endif; ?>
    </div>

    <?php if(false && $template_items["invoice_note"][0]->item_hide==0): ?>
        <div class="invoice_conditions invoice_item"
             style = "
                     position: relative;
                     width: {{$template_items["invoice_note"][0]->item_width}};
                     height: {{$template_items["invoice_note"][0]->item_height}};
                     margin-top: 20px;
                     left:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_left,$template_data->table_direction)}};
                     top:{{get_px_base_on_direction($template_items["invoice_note"][0]->item_position_top,$template_data->table_direction)}};
                     "
             data-item_id="{{$template_items["invoice_note"][0]->item_id}}">
            <hr>
            {!! $template_items["invoice_note"][0]->item_label !!}
        </div>
    <?php endif; ?>


</div>