@extends('admin.main_layout')

@section('subview')

    <link rel="stylesheet" href="{{url('public_html/admin/invoices/template1/css/style.css')}}">
    <?php if(false): ?>
    <!-- for resizable and dragable -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="{{url('public_html/jscode/admin/templates.js')}}"></script>
    <?php endif; ?>


    <style>
        .invoice-box{
            width: {{convert_from_cm_to_px($template_data->template_width)}}px;
            height: {{convert_from_cm_to_px($template_data->template_height)}}px;
        }
    </style>


        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center;height: 100px;">
                    <div class="col-md-3">
                        <button class="btn btn-success btn-lg fire_print_event" style="float: right;height: 100px;margin-top: -16px;width: 63%;">طباعة</button>
                    </div>
                    <div class="col-md-4">
                        <h3>
                            قالب
                            {{$template_data->template_name}}
                        </h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{url("admin/client_bills/add_bill?branch_id=".$_GET['branch_id']."&branch_admin_id=".$_GET['branch_admin_id']."")}}" class="btn btn-success btn-lg" style="float: left;height: 100px;margin-top: -16px;width: 63%;line-height: 72px;">إضافة فاتورة جديدة</a>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    بيانات الفاتورة
                </div>
                <div class="panel-body">

                    <?php if(is_array($template_items) && count($template_items)): ?>

                    <div class="col-md-12">

                        <div class="col-md-6 form-group">
                            <label for="">رقم الفاتورة</label>
                            <input type="number" min="0" class="form-control" id="bill_id" value="{{$bill_id}}">
                        </div>

                        <div class="col-md-6 form-group">
                            <?php
                            echo
                            generate_select_tags(
                                    $field_name="bill_type",
                                    $label_name="نوع الفاتورة",
                                    $text=["مرتجع","بيع"],
                                    $values=[1,0],
                                    $selected_value=[$bill_type],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required = "",
                                    $disabled = "",
                                    $data = ""
                            );
                            ?>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 col-md-offset-4" style="text-align: center;">
                            <button class="btn btn-info preview_invoice_before_print">عرض</button>
                        </div>
                        <?php if(isset($_GET['branch_id']) && isset($_GET['branch_admin_id'])): ?>
                            <div class="col-md-2" style="text-align: center;">
                                <a href="{{url("admin/client_bills/add_bill?branch_id=".$_GET['branch_id']."&branch_admin_id=".$_GET['branch_admin_id']."")}}" class="btn btn-info">إضافة فاتورة جديدة</a>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php endif; ?>

                </div>
            </div>

            <?php if(!empty($bill_data)): ?>
                <div class="panel panel-info">
                    <div class="panel-heading" role="button" data-toggle="collapse"
                         href="#template_body" aria-expanded="true" aria-controls="template_body"> شكل قالب الفاتورة
                        <button class="btn btn-info fire_print_event">طباعة</button>
                    </div>
                    <div class="panel-body">
                        <div class="row custom_template_html">
                            @include('admin.subviews.invoices_templates.print_client_bill.template_block')
                        </div>
                    </div>
                    <?php if(1!=1): ?>
                        <button type="button" class="col-md-3 col-md-offset-4 btn btn-primary btn-lg save_template_html" data-inv_temp_id = "{{$template_data->inv_temp_id}}">حفظ</button>
                        <div class="show_template_errors"></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>



        </div>


@endsection