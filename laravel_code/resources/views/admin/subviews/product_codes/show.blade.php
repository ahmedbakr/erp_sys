@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            الأكواد
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الكود بالعربي</td>
                    <td>الكود بالإنجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>الكود بالعربي</td>
                    <td>الكود بالإنجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($codes as $key => $code): ?>

                <tr id="row<?= $code->pro_code_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$code->pro_code_label}}</td>
                    <td>{{$code->pro_code_label_en}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"admin/product_codes","edit_action",$current_user)): ?>
                        <a href="<?= url("admin/product_codes/save_code/$code->pro_code_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/product_codes","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/product_codes/remove_code") ?>" data-tablename="App\models\product\product_codes_m"  data-itemid="<?= $code->pro_code_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/product_codes","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/product_codes/save_code")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
