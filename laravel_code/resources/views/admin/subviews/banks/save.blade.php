@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة بنك جديد";
    $bank_id="";

    if ($bank_data!="") {
        $header_text="تعديل بنك ".$bank_data->bank_name;

        $bank_id=$bank_data->bank_id;
    }

    //dump($pro_data);
    ?>

    <!--new_editor-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
    <!--END new_editor-->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/banks/save_bank/$bank_id")?>" method="POST" enctype="multipart/form-data">

                    <?php

                    $normal_tags=array('bank_name','bank_atm_ratio');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $bank_data,
                            "yes",
                            $required="required"
                    );


                    $attrs[0]["bank_name"]="اسم البنك *";
                    $attrs[0]["bank_atm_ratio"]="نسبه الخصم للبنك علي كل عملية";


                    $attrs[3]["bank_name"]="text";
                    $attrs[3]["bank_atm_ratio"]="number";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

                </form>
            </div>
        </div>
    </div>


@endsection



