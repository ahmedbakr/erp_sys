@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            البنوك
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم البنك</td>
                    <td>نسبه الخصم للبنك علي كل عملية</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم البنك</td>
                    <td>نسبه الخصم للبنك علي كل عملية</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_banks as $key => $bank): ?>

                <tr id="row<?= $bank->bank_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$bank->bank_name}}</td>
                    <td>{{$bank->bank_atm_ratio}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"admin/banks","edit_action",$current_user)): ?>
                        <a href="<?= url("admin/banks/save_bank/$bank->bank_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/banks","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/banks/remove_bank") ?>" data-tablename="App\models\banks_m"  data-itemid="<?= $bank->bank_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/banks","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/banks/save_bank")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
