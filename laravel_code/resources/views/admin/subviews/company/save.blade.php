@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    //dump($pro_data);
    ?>

    <!--new_editor-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
    <!--END new_editor-->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=" تعديل بيانات الشركة "?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/company")?>" method="POST" enctype="multipart/form-data">

                    <?php

                    $normal_tags=array(

                        'company_name','company_name_en','company_short_desc',
                        'company_short_desc_en','company_tel','company_fax','company_email',
                        'company_website','company_owner_name','company_owner_name_en',
                        'company_card_id','company_address','company_address_en','after_logout_text_1',
                        'after_logout_text_2','sell_tax_percent'

                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $company_data,
                        "yes",
                        $required="required"
                    );


                    $attrs[0]["company_name"]="اسم الشركة بالعربي *";
                    $attrs[0]["company_name_en"]="اسم الشركة بالانجليزي";
                    $attrs[0]["company_short_desc"]="نبذة مختصرة عن الشركة بالعربي";
                    $attrs[0]["company_short_desc_en"]="نبذة مختصرة عن الشركة بالانجليزي";
                    $attrs[0]["company_tel"]="تليفون الشركة";
                    $attrs[0]["company_fax"]="فاكس الشركة";
                    $attrs[0]["company_email"]="البريد الإلكتروني للشركة";
                    $attrs[0]["company_website"]="موقع الشركة";
                    $attrs[0]["company_owner_name"]="اسم المالك بالعربي";
                    $attrs[0]["company_owner_name_en"]="اسم المالك بالانجليزي";
                    $attrs[0]["company_card_id"]="رقم بطاقة الأحوال";
                    $attrs[0]["company_address"]="عنوان الشركة بالعربي";
                    $attrs[0]["company_address_en"]="عنوان الشركة بالانجليزي";
                    $attrs[0]["after_logout_text_1"]="صفحة نسجيل الخروج 1";
                    $attrs[0]["after_logout_text_2"]="صفحة نسجيل الخروج 2";
                    $attrs[0]["sell_tax_percent"]="نسبة القيمة المضافة للمبيعات";


                    $attrs[3]["company_name"]="text";
                    $attrs[3]["company_name_en"]="text";
                    $attrs[3]["company_short_desc"]="textarea";
                    $attrs[3]["company_short_desc_en"]="textarea";
                    $attrs[3]["company_tel"]="text";
                    $attrs[3]["company_fax"]="text";
                    $attrs[3]["company_email"]="text";
                    $attrs[3]["company_website"]="text";
                    $attrs[3]["company_owner_name"]="text";
                    $attrs[3]["company_owner_name_en"]="text";
                    $attrs[3]["company_card_id"]="text";
                    $attrs[3]["company_address"]="textarea";
                    $attrs[3]["company_address_en"]="textarea";
                    $attrs[3]["after_logout_text_1"]="textarea";
                    $attrs[3]["after_logout_text_2"]="textarea";
                    $attrs[3]["sell_tax_percent"]="number";


                    $attrs[6]["company_name"]="6";
                    $attrs[6]["company_name_en"]="6";
                    $attrs[6]["company_short_desc"]="12";
                    $attrs[6]["company_short_desc_en"]="12";
                    $attrs[6]["company_tel"]="4";
                    $attrs[6]["company_fax"]="4";
                    $attrs[6]["company_email"]="4";
                    $attrs[6]["company_website"]="4";
                    $attrs[6]["company_owner_name"]="4";
                    $attrs[6]["company_owner_name_en"]="4";
                    $attrs[6]["company_card_id"]="4";
                    $attrs[6]["company_address"]="12";
                    $attrs[6]["company_address_en"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );



                    echo generate_img_tags_for_form(
                        $filed_name="company_logo",
                        $filed_label="company_logo",
                        $required_field="",
                        $checkbox_field_name="company_logo_checkbox",
                        $need_alt_title="no",
                        $required_alt_title="",
                        $old_path_value="",
                        $old_title_value="",
                        $old_alt_value="",
                        $recomended_size="",
                        $disalbed="disabled",
                        $displayed_img_width="100",
                        $display_label="رفع للوجو الشركة",
                        $img_obj = $logo_obj
                    );

                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

                </form>
            </div>
        </div>
    </div>


@endsection



