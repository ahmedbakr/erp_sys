@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            سندات
{{$documents_type}}
            عهدات
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>التاريخ</td>
                    <td>من</td>
                    <td>إلي</td>
                    <td>المبلغ</td>
                    <td>التفقيط</td>
                    <td>بيان</td>
                    <td>عميلة</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>التاريخ</td>
                    <td>من</td>
                    <td>إلي</td>
                    <td>المبلغ</td>
                    <td>التفقيط</td>
                    <td>بيان</td>
                    <td>عميلة</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($documents as $key => $document): ?>
                <tr id="row<?= $document->duty_document_id ?>">
                    <td><?=$key+1?></td>
                    <td><?=$document->duty_document_date ?></td>
                    <td><?=$document->{'from_user_full_name'.$en} ?></td>
                    <td><?=$document->{'to_user_full_name'.$en} ?></td>
                    <td><?=$document->duty_document_money ?></td>
                    <td><?=$document->duty_document_money_text ?></td>
                    <td><?=$document->duty_document_desc ?></td>
                    <td>

                        <?php if($document->from_user_id == $current_user->user_id): ?>

                            <?php if($document->duty_document_received == 1): ?>
                                <span class="label label-info">مستلمة</span>
                                <?php else: ?>
                                <span class="label label-info">غير مستلمة</span>
                            <?php endif; ?>

                            <?php else: ?>

                            <?php if($document->duty_document_received == 1): ?>
                                <span class="label label-info">تم الإستلام</span>
                                <?php else: ?>
                                <?php
                                    echo
                                    generate_multi_accepters(
                                        $accepturl=url( "/admin/duty_documents/change_duty_document_received"),
                                        $item_obj=$document,
                                        $item_primary_col="duty_document_id",
                                        $accept_or_refuse_col="duty_document_received",
                                        $model='App\models\admin\duty_documents_m',
                                        $accepters_data=["0"=>"لا","1"=>"استلام"]
                                    );
                                ?>
                            <?php endif; ?>
                        <?php endif; ?>

                    </td>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"factory/duty_documents","add_action",$current_user)): ?>
                <div class="row">
                    <a href="{{url('admin/duty_documents/save/new')}}" class="btn btn-info col-md-3 col-md-offset-4">تسجيل سند جديد</a>
                </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
