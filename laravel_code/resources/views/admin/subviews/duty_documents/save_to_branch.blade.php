@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <?php

            $header_text="تسجيل سند عهده";

            ?>



            <div class="panel panel-info">
                <div class="panel-heading"><h2><?=$header_text?></h2></div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/duty_documents/save/new")?>" method="POST" enctype="multipart/form-data">


                            <div class="col-md-4 col-md-offset-4">
                                <?php
                                    echo generate_select_tags(
                                        $field_name="to_user_id",
                                        $label_name="إختار من $choose_from *",
                                        $text=convert_inside_obj_to_arr($all_users,"full_name"),
                                        $values=convert_inside_obj_to_arr($all_users,"user_id"),
                                        $selected_value=[""],
                                        $class="form-control select_2_class",
                                        $multiple="",
                                        $required="required",
                                        $disabled = "",
                                        $data = ""
                                    );
                                ?>
                            </div>


                            <?php



                                $normal_tags=array("duty_document_date","duty_document_money","duty_document_desc");

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $user_data="",
                                    "yes",
                                    $required =""
                                );

                                $attrs[0]["duty_document_date"]="التاريخ *";
                                $attrs[0]["duty_document_money"]="المبلغ *";
                                $attrs[0]["duty_document_desc"]="بيان";

                                $attrs[2]["duty_document_date"]="required";
                                $attrs[2]["duty_document_money"]="required";


                                $attrs[3]["duty_document_date"]="date_time";
                                $attrs[3]["duty_document_money"]="text";
                                $attrs[3]["duty_document_desc"]="textarea";

                                $attrs[4]["duty_document_date"]=date("Y-m-d H:i:s");

                                $attrs[6]["duty_document_date"]="6";
                                $attrs[6]["duty_document_money"]="6";
                                $attrs[6]["duty_document_desc"]="12";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>

                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection
