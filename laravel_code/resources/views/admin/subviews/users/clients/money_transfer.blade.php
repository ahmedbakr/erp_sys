@extends('admin.main_layout')

@section('subview')

    <input type="hidden" class="finish_money_transfer_partially" value="{{(int)check_permission($user_permissions,"admin/users","finish_money_transfer_partially",$current_user)}}">

    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                بيانات المعاملة المالية
            </div>
            <div class="panel-body">

                <table class="table table-striped">

                    <tr>
                        <td>من</td>
                        <td>
                            <?php if($from_branch=="yes"): ?>
                            <label class="label label-warning">الفرع</label>
                            <?php else: ?>
                            <label class="label label-success">العميل</label>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>الي</td>
                        <td>
                            <?php if($from_branch=="yes"): ?>
                                <label class="label label-success">العميل</label>
                            <?php else: ?>
                                <label class="label label-warning">الفرع</label>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>قيمة الاموال</td>
                        <td>
                            <label class="label label-primary money_transfer_amount">{{$money_transfer_data->m_t_amount}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>الوصف</td>
                        <td>
                            {{$money_transfer_data->m_t_desc}}
                        </td>
                    </tr>
                    <tr>
                        <td>علي علاقة بالفاتورة رقم</td>
                        <td>
                            <a href="{{url("/admin/client_bills/show_bill_orders/$money_transfer_data->m_t_bill_id")}}">
                                <label class="label label-info">
                                    #{{$money_transfer_data->m_t_bill_id}}
                                </label>
                            </a>
                        </td>
                    </tr>



                </table>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                انهاء المعاملة المالية
            </div>
            <div class="panel-body">
                <form action="{{url("/admin/users/clients/money_transfers/$money_transfer_data->m_t_id")}}" method="post">
                    {{csrf_field()}}
                    <table class="table table-striped">
                        <tr>
                            <td>الكاش</td>
                            <td><input type="number" min="0" value="0" name="money_transfer_cash_money" max="{{$money_transfer_data->m_t_amount}}" class="form-control money_transfer_cash_money"></td>
                        </tr>

                        <?php if(check_permission($user_permissions,"admin/users","finish_money_transfer_with_atm",$current_user)): ?>
                        <tr>
                            <td>الشبكة</td>
                            <td><input type="number" min="0" value="0" name="money_transfer_atm_money" max="{{$money_transfer_data->m_t_amount}}" class="form-control money_transfer_atm_money"></td>
                        </tr>
                        <?php endif; ?>

                        <tr>
                            <td><button type="submit" name="submit" class="btn btn-primary btn-block finish_money_transfer_btn">دفع</button></td>
                            <td>
                                <div class="transfer_money_msgs"></div>
                            </td>
                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>




@endsection