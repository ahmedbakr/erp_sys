@extends('admin.main_layout')

@section('subview')

    
    <div class="col-md-12">

        <div class="col-md-6">

            <div class="panel panel-info">
                <div class="panel-heading">البيانات الاساسية للعميل</div>
                <div class="panel-body">

                    <table class="table table-striped" style="direction: rtl;">
                        <tr>
                            <td>اسم العميل</td>
                            <td>
                                <label class="label label-default">
                                    {{$user_obj->full_name}}
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>ايميل العميل</td>
                            <td>
                                <label class="label label-default">
                                    {{$user_obj->email}}
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>رقم العميل</td>
                            <td>
                                <label class="label label-info">
                                    {{$user_obj->user_tel}}
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار ما دفعه</td>
                            <td>
                                <label class="label label-success">
                                    {{$user_obj->total_payed_cash}} كاش
                                </label>
                                <label class="label label-success">
                                    {{$user_obj->total_payed_atm}} شبكة
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>مقدار ما استلمه</td>
                            <td>
                                <label class="label label-warning">
                                    {{$user_obj->total_received_cash}} كاش
                                </label>
                                <label class="label label-warning">
                                    {{$user_obj->total_received_atm}} شبكة
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td>المتبقي له</td>
                            <td>
                                <label class="label label-danger">
                                    {{$user_obj->total_remain_for_him}}
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td>المتبقي عليه للفرع</td>
                            <td>
                                <label class="label label-primary">
                                    {{$user_obj->total_remain_for_branch}}
                                </label>
                            </td>
                        </tr>

                    </table>
                    
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    اكثر المنتجات شرائا
                </div>
                <div class="panel-body" style="max-height: 515px;overflow-y: scroll;overflow-x: hidden;">
                    <table class="table table-striped table_with_paging" style="direction: rtl;">
                        <thead>
                            <tr>
                                <th>اسم المنتج</th>
                                <th>كمية المنتج</th>
                            </tr>
                        </thead>


                        <tbody>
                        <?php foreach($buyed_pros as $key=>$pro): ?>
                        <tr>
                            <td>
                                <label class="label label-primary">{{$pro["pro_name"]}}</label>
                                <label class="label label-primary">{{$pro["pro_barcode"]}}</label>
                            </td>
                            <td>
                                <label class="label label-success">{{$pro["buyed_quantity"]}}</label>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>



        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    فواتير العميل
                </div>
                <div class="panel-body" style="max-height: 515px;overflow-y: scroll;">

                    <table class="table table-striped table-bordered table_without_paging">

                        <thead>
                            <tr>
                                <th>رقم الفاتورة</th>
                                <th>تاريخ الفاتورة</th>
                                <th>تكلفة الفاتورة</th>
                                <th>المدفوع من الفاتورة</th>
                                <th>المتبقي من الفاتورة</th>
                                <th>قيمة مرجعات الفاتورة</th>
                            </tr>
                        </thead>


                        <tbody>
                            <?php
                                $all_bills_total_amount=0;
                                $all_bills_client_paid=0;
                                $all_bills_remain=0;
                                $all_bills_return=0;
                            ?>
                            <?php foreach($user_bills as $key=>$bill): ?>
                                <?php
                                    $all_bills_total_amount=$all_bills_total_amount+$bill->client_bill_total_amount;
                                    $all_bills_client_paid=$all_bills_client_paid+$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm;
                                    $all_bills_remain=$all_bills_remain+$bill->client_bill_total_remain_amount;
                                    $all_bills_return=$all_bills_return+$bill->client_bill_total_return_amount;
                                ?>
                                <tr>
                                    <td>
                                        <a href="{{url("/admin/client_bills/show_bill_orders/$bill->client_bill_id")}}">
                                            <label class="label label-default">
                                                #{{$bill->client_bill_id}}
                                            </label>
                                        </a>
                                    </td>
                                    <td>
                                        <label class="label label-info">{{date("Y-m-d",strtotime($bill->client_bill_date))}}</label>
                                        <label class="label label-default">{{get_hegri_date(strtotime($bill->client_bill_date))}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-info">{{$bill->client_bill_total_amount}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-info">{{$bill->client_total_paid_amount_in_cash+$bill->client_total_paid_amount_in_atm}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-warning">{{$bill->client_bill_total_remain_amount}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-danger">{{$bill->client_bill_total_return_amount}}</label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>

                        <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <label class="label label-info">{{$all_bills_total_amount}}</label>
                            </td>
                            <td>
                                <label class="label label-info">{{$all_bills_client_paid}}</label>
                            </td>
                            <td>
                                <label class="label label-warning">{{$all_bills_remain}}</label>
                            </td>
                            <td>
                                <label class="label label-danger">{{$all_bills_return}}</label>
                            </td>
                        </tr>
                        </tfoot>


                    </table>

                </div>
            </div>


        </div>

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    المعاملات المالية للعميل
                </div>
                <div class="panel-body"  style="max-height: 515px;overflow-y: scroll;">

                    <label class="label label-info">
                        <input type="checkbox" id="hide_finish_transfers">
                        اخفاء المعاملات المنتهية
                    </label>


                    <table class="table table-striped table-bordered table_without_paging">

                        <thead>
                            <tr>
                                <th>التاريخ</th>
                                <th>من</th>
                                <th>الي</th>
                                <th>نوع الاموال</th>
                                <th>قيمة الاموال</th>
                                <th>استلم الاموال</th>
                                <th>الوصف</th>
                                <th>علي علاقة بالفاتورة رقم</th>
                                <th>دفع</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($money_transfers_to_user as $key=>$money_trans): ?>

                                <tr class="{{($money_trans->m_t_status=="1")?"success transfer_done":""}}">
                                    <td>
                                        <label class="label label-default">{{$money_trans->created_at}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-warning">الفرع</label>
                                    </td>
                                    <td>
                                        <label class="label label-success">العميل</label>
                                    </td>
                                    <td>
                                        <label class="label label-default">
                                            {{($money_trans->m_t_amount_type=="cash")?"كاش":""}}
                                            {{($money_trans->m_t_amount_type=="atm")?"شبكة":""}}
                                        </label>
                                    </td>
                                    <td>
                                        <label class="label label-primary">{{$money_trans->m_t_amount}}</label>
                                    </td>
                                    <td>
                                        <label class="label label-default">
                                            {{($money_trans->m_t_status=="1")?"نعم":"لا"}}
                                        </label>
                                    </td>
                                    <td>
                                        {{$money_trans->m_t_desc}}
                                    </td>
                                    <td>
                                        <a href="{{url("/admin/client_bills/show_bill_orders/$money_trans->m_t_bill_id")}}">
                                            <label class="label label-info">
                                                #{{$money_trans->m_t_bill_id}}
                                            </label>
                                        </a>
                                    </td>
                                    <td>
                                        <?php if($money_trans->m_t_status==0): ?>
                                            <a href="{{url("/admin/users/clients/money_transfers/$money_trans->m_t_id")}}">
                                                <label class="label label-primary">
                                                    دفع
                                                </label>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>

                            <?php endforeach;?>

                            <?php foreach($money_transfers_from_user as $key=>$money_trans): ?>

                            <tr class="{{($money_trans->m_t_status=="1")?"success transfer_done":""}}">
                                <td>
                                    <label class="label label-default">{{$money_trans->created_at}}</label>
                                </td>
                                <td>
                                    <label class="label label-success">العميل</label>
                                </td>
                                <td>
                                    <label class="label label-warning">الفرع</label>
                                </td>
                                <td>
                                    <label class="label label-default">
                                        {{($money_trans->m_t_amount_type=="cash")?"كاش":""}}
                                        {{($money_trans->m_t_amount_type=="atm")?"شبكة":""}}
                                    </label>
                                </td>
                                <td>
                                    <label class="label label-primary">{{$money_trans->m_t_amount}}</label>
                                </td>
                                <td>
                                    <label class="label label-default">
                                        {{($money_trans->m_t_status=="1")?"نعم":"لا"}}
                                    </label>
                                </td>
                                <td>
                                    {{$money_trans->m_t_desc}}
                                </td>
                                <td>
                                    <a href="{{url("/admin/client_bills/show_bill_orders/$money_trans->m_t_bill_id")}}">
                                        <label class="label label-info">
                                            #{{$money_trans->m_t_bill_id}}
                                        </label>
                                    </a>
                                </td>
                                <td>
                                    <?php if($money_trans->m_t_status==0): ?>
                                    <a href="{{url("/admin/users/clients/money_transfers/$money_trans->m_t_id")}}">
                                        <label class="label label-primary">
                                            دفع
                                        </label>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <?php endforeach;?>

                        </tbody>



                    </table>

                </div>
            </div>
        </div>


    </div>



    



@endsection
