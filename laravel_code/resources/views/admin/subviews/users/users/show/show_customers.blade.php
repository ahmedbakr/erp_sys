@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">البحث</div>
        <div class="panel-body">
            <form action="{{url("/admin/users/get_all/customer")}}">
                <div class="form-group">
                    <label for="">البحث عن عميل بواسطة الاسم او رقم التليفون</label>
                    <input type="text" class="form-control" name="search_for_user" value="{{$search_for_user}}">
                </div>
                <button class="btn btn-primary" type="submit">ابحث</button>
            </form>
        </div>
    </div>
    
    <div class="panel panel-info">
        <div class="panel-heading">
            العملاء
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <div class="col-md-3">
                <a href="{{url("/admin/users/export_excel")}}" class="btn btn-info btn-lg">Export Excel</a>
            </div>
            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <th>#</th>
                    <th>كود العميل</th>
                    <th>اسم العميل</th>
                    <th>ايميل العميل</th>
                    <th>تليفون العميل</th>
                    <th>موبايل العميل</th>
                    <th>فاكس العميل</th>
                    <th>تاريخ بدء التعامل العميل</th>
                    <th>عنوان العميل</th>
                    <th>سعر العميل</th>
                    <th>الفئة العمرية للعميل</th>
                    <th>نوع العميل</th>
                    <th>بروفايل العميل</th>
                    <th>عدد العملاء الحاليين من طرفه</th>
                    <th>مقدار ما دفعه</th>
                    <th>مقدار ما استلمه</th>
                    <th>المتبقي له</th>
                    <th>المتبقي عليه</th>
                    <th>عدد العملاء من طرفة</th>
                    <th>تعديل</th>

                    <?php if(check_permission($user_permissions,"factory/users","delete_action",$current_user)): ?>
                    <th>مسح</th>
                    <?php endif; ?>
                </tr>
                </thead>



                <tbody>
                <?php
                    $users_total_payed_cash=0;
                    $users_total_payed_atm=0;

                    $total_user_received_cash=0;
                    $total_user_received_atm=0;

                    $total_user_remain_for_him=0;
                    $total_user_remain_for_branch=0;
                ?>
                <?php foreach ($users as $key => $user): ?>
                    <?php
                        $users_total_payed_cash=$users_total_payed_cash+$user->total_payed_cash;
                        $users_total_payed_atm=$users_total_payed_atm+$user->total_payed_atm;

                        $total_user_received_cash=$total_user_received_cash+$user->total_received_cash;
                        $total_user_received_atm=$total_user_received_atm+$user->total_received_atm;

                        $total_user_remain_for_him=$total_user_remain_for_him+$user->total_remain_for_him;
                        $total_user_remain_for_branch=$total_user_remain_for_branch+$user->total_remain_for_branch;
                    ?>
                <tr id="row<?= $user->user_id ?>">

                    <td><?=$key+1?></td>
                    <td><?=$user->user_id ?></td>
                    <td>
                        <?=$user->full_name ?>
                        <br>
                        <?=$user->full_name_en ?>
                    </td>
                    <td><?=$user->email ?></td>
                    <td><?=$user->user_tel ?></td>
                    <td><?=$user->user_mobile ?></td>
                    <td><?=$user->user_fax ?></td>
                    <td><?=$user->start_join_date ?></td>
                    <td><?=$user->user_address ?></td>
                    <td><?=$user->client_price_title ?></td>
                    <td><?=$user->age_range ?></td>
                    <td><?=$user->user_gender ?></td>
                    <td>
                        <a href="{{url("/admin/users/clients/client_profile/$user->user_id?branch_id=$user->related_id")}}">بروفايل العميل</a>
                    </td>
                    <td>
                        {{$user->user_as_sponsor_count}}
                    </td>

                    <td>
                        <label class="label label-success">{{$user->total_payed_cash}} كاش</label>
                        <label class="label label-success">{{$user->total_payed_atm}} شبكة</label>
                    </td>

                    <td>
                        <label class="label label-warning">{{$user->total_received_cash}} كاش </label>
                        <label class="label label-warning">{{$user->total_received_atm}} شبكة</label>
                    </td>

                    <td><label class="label label-danger">{{$user->total_remain_for_him}}</label></td>
                    <td><label class="label label-warning">{{$user->total_remain_for_branch}}</label></td>

                    <td>
                        <label class="label label-info">
                            {{$user->user_as_sponsor_count}}
                        </label>

                        <?php if($user->user_as_sponsor_count>0): ?>
                        <button class="btn btn-warning make_sponsor_zero_btn" data-custid="{{$user->user_id}}">تصفير الرقم</button>
                        <?php endif; ?>

                    </td>

                    <td>
                        <a href="<?= url("admin/users/save/$user->user_type/$user->user_id") ?>">
                            <span class="label label-info"> تعديل البيانات <i class="fa fa-edit"></i></span>
                        </a>
                    </td>

                    <?php if(check_permission($user_permissions,"factory/users","delete_action",$current_user)): ?>
                    <td>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("/admin/users/remove_customer") ?>" data-tablename="App\User"  data-itemid="<?= $user->user_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                    </td>
                    <?php endif; ?>

                </tr>
                <?php endforeach ?>


                <?php if(false): ?>
                    <tr>
                    <td>الاجمالي</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <label class="label label-success">{{$users_total_payed_cash}} كاش</label>
                        <label class="label label-success">{{$users_total_payed_atm}} شبكة</label>
                    </td>

                    <td>
                        <label class="label label-warning">{{$total_user_received_cash}} كاش</label>
                        <label class="label label-warning">{{$total_user_received_atm}} شبكة</label>
                    </td>

                    <td><label class="label label-danger">{{$total_user_remain_for_him}}</label></td>
                    <td><label class="label label-warning">{{$total_user_remain_for_branch}}</label></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php endif; ?>

                </tbody>




            </table>


            <div class="col-md-12" style="text-align: center;">
                {!! $users_pagination->links() !!}
            </div>


            <?php if(check_permission($user_permissions,"factory/users","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-5">
                <a href="{{url("/admin/users/save/$user_type")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
