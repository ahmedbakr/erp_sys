@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            الموردين
        </div>
        <div class="panel-body">
            <table id="cat_table_1" class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الايميل</th>
                    <th>اسم المورد</th>
                    <th>تليفون المورد</th>
                    <th>بروفايل المورد</th>
                    <th>مقدار ما دفعه</th>
                    <th>مقدار ما استلمه</th>
                    <th>المتبقي له</th>
                    <th>المتبقي عليه</th>
                    <th>تعديل</th>
                </tr>
                </thead>



                <tbody>
                <?php
                    $users_total_payed_cash=0;
                    $users_total_payed_atm=0;

                    $total_user_received_cash=0;
                    $total_user_received_atm=0;

                    $total_user_remain_for_him=0;
                    $total_user_remain_for_branch=0;
                ?>
                <?php foreach ($users as $key => $user): ?>
                    <?php
                        $users_total_payed_cash=$users_total_payed_cash+$user->total_payed_cash;
                        $users_total_payed_atm=$users_total_payed_atm+$user->total_payed_atm;

                        $total_user_received_cash=$total_user_received_cash+$user->total_received_cash;
                        $total_user_received_atm=$total_user_received_atm+$user->total_received_atm;

                        $total_user_remain_for_him=$total_user_remain_for_him+$user->total_remain_for_him;
                        $total_user_remain_for_branch=$total_user_remain_for_branch+$user->total_remain_for_branch;
                    ?>
                <tr id="row<?= $user->user_id ?>">

                    <td><?=$key+1?></td>
                    <td><?=$user->email ?></td>
                    <td><?=$user->full_name ?></td>
                    <td><?=$user->user_tel ?></td>
                    <td>
                        <a href="{{url("/admin/suppliers/profile/$user->user_id")}}">
                            <span class="label label-default">البروفايل</span>
                        </a>
                    </td>

                    <td>
                        <label class="label label-success">{{$user->total_payed_cash}} كاش</label>
                        <label class="label label-success">{{$user->total_payed_atm}} شبكة</label>
                    </td>

                    <td>
                        <label class="label label-warning">{{$user->total_received_cash}} كاش </label>
                        <label class="label label-warning">{{$user->total_received_atm}} شبكة</label>
                    </td>

                    <td><label class="label label-danger">{{$user->total_remain_for_him}}</label></td>
                    <td><label class="label label-warning">{{$user->total_remain_for_branch}}</label></td>


                    <td>
                        <a href="<?= url("admin/users/save/$user->user_type/$user->user_id") ?>">
                            <span class="label label-info"> تعديل البيانات <i class="fa fa-edit"></i></span>
                        </a>
                    </td>

                </tr>
                <?php endforeach ?>
                </tbody>


                <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <label class="label label-success">{{$users_total_payed_cash}} كاش</label>
                        <label class="label label-success">{{$users_total_payed_atm}} شبكة</label>
                    </td>

                    <td>
                        <label class="label label-warning">{{$total_user_received_cash}} كاش</label>
                        <label class="label label-warning">{{$total_user_received_atm}} شبكة</label>
                    </td>

                    <td><label class="label label-danger">{{$total_user_remain_for_him}}</label></td>
                    <td><label class="label label-warning">{{$total_user_remain_for_branch}}</label></td>
                    <td></td>
                </tr>
                </tfoot>

            </table>


            <?php if(check_permission($user_permissions,"factory/users","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-5">
                <a href="{{url("/admin/users/save/$user_type")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
