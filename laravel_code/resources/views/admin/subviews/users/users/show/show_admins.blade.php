@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            الادمنز
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>مسلسل</th>
                        <th>صورة الادمن</th>
                        <th>كود الادمن</th>
                        <th>اسم الادمن</th>
                        <th>ايميل الادمن</th>
                        <th>له كل الصلاحيات</th>
                        <th>تعديل الصلاحيات</th>
                        <th>تعديل</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($users as $key => $user): ?>
                        <tr id="row<?= $user->user_id ?>">
                            <td><?=$key+1?></td>
                            <td>
                                <img src="{{get_image_or_default($user->logo_path)}}" alt="" width="50">
                            </td>
                            <td>{{$user->user_id}}</td>

                            <td>
                                <?=$user->full_name ?>
                                <br>
                                <?=$user->full_name_en ?>
                            </td>
                            <td><?=$user->email ?></td>
                            <td>
                                <?php
                                    echo
                                    generate_multi_accepters(
                                        $accepturl="",
                                        $item_obj=$user,
                                        $item_primary_col="user_id",
                                        $accept_or_refuse_col="have_all_permissions",
                                        $model='App\User',
                                        $accepters_data=["0"=>"لا","1"=>"نعم"],
                                        "1"
                                    );
                                ?>
                            </td>
                            <td>
                                <a href="<?= url("admin/users/assign_permission/$user->user_id") ?>">
                                    <span class="label label-info"> تعديل الاذونات <i class="fa fa-edit"></i></span>
                                </a>
                            </td>
                            <td>
                                <a href="<?= url("admin/users/save/$user->user_type/$user->user_id") ?>">
                                    <span class="label label-info"> تعديل البيانات <i class="fa fa-edit"></i></span>
                                </a>
                            </td>

                        </tr>
                    <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"factory/users","add_action",$current_user)): ?>
                <div class="col-md-3 col-md-offset-5">
                    <a href="{{url("/admin/users/save/$user_type")}}" class="btn btn-primary">إضافة جديد</a>
                </div>
            <?php endif; ?>

        </div>
    </div>

@endsection
