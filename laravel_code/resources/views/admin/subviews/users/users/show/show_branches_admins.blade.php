@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            المستخدمين
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table_1" class="table table-striped table-bordered table_with_paging" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>مسلسل</th>
                    <th>الفرع</th>
                    <th>صورة المستخدم</th>
                    <th>كود المستخدم</th>
                    <th>اسم المستخدم</th>
                    <th>ايميل المستخدم</th>
                    <th>تليفون المستخدم</th>
                    <th>موبايل المستخدم</th>
                    <th>فاكس المستخدم</th>
                    <th>عنوان المستخدم</th>
                    <th>تاريخ بدء العمل</th>
                    <th>الاذونات</th>
                    <th>تعديل</th>

                </tr>
                </thead>



                <tbody>
                    <?php foreach ($users as $key => $user): ?>
                        <tr id="row<?= $user->user_id ?>">
                            <td><?=$key+1?></td>
                            <td>
                                {{$user->related_branch_name}}
                                <br>
                                {{$user->related_branch_name_en}}
                            </td>
                            <td>
                                <img src="{{get_image_or_default($user->logo_path)}}" alt="" width="50">
                            </td>
                            <td>{{$user->user_id}}</td>

                            <td>
                                <?=$user->full_name ?>
                                <br>
                                <?=$user->full_name_en ?>
                            </td>
                            <td><?=$user->email ?></td>

                            <td><?=$user->user_tel ?></td>
                            <td><?=$user->user_mobile ?></td>
                            <td><?=$user->user_fax ?></td>
                            <td><?=$user->user_address ?></td>
                            <td><?=$user->start_join_date ?></td>

                            <td>
                                <a href="<?= url("admin/users/assign_permission/$user->user_id") ?>">
                                    <span class="label label-info"> تعديل الاذونات <i class="fa fa-edit"></i></span>
                                </a>
                            </td>

                            <td>
                                <a href="<?= url("admin/users/save/$user->user_type/$user->user_id") ?>">
                                    <span class="label label-info"> تعديل البيانات <i class="fa fa-edit"></i></span>
                                </a>
                            </td>

                        </tr>
                    <?php endforeach ?>
                </tbody>


            </table>


            <?php if(check_permission($user_permissions,"factory/users","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-5">
                <a href="{{url("/admin/users/save/$user_type")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>

@endsection
