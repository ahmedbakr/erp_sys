@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            المستخدمين
        </div>
        <div class="panel-body">
            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الايميل</td>
                    <td>اسم المستخدم</td>
                    <td>الاسم بالكامل</td>
                    <td>نوع المستخدم</td>
                    <td>تاريخ التسجيل</td>
                    <td>الاذونات</td>
                    <td>تعديل</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>الايميل</td>
                    <td>اسم المستخدم</td>
                    <td>الاسم بالكامل</td>
                    <td>نوع المستخدم</td>
                    <td>تاريخ التسجيل</td>
                    <td>الاذونات</td>
                    <td>تعديل</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($users as $key => $user): ?>
                <tr id="row<?= $user->user_id ?>">
                    <td><?=$key+1?></td>
                    <td><?=$user->email ?></td>
                    <td><?=$user->username ?></td>
                    <td><?=$user->full_name ?></td>
                    <td>
                        <?php
                            if ($user->user_type == "admin")
                                echo "أدمن رئيسي";
                            else if ($user->user_type == "branch")
                                echo "فرع";
                            else if ($user->user_type == "branch_admin")
                                echo "كاشيير";
                            else if ($user->user_type == "factory_admin")
                                echo "أدمن فرعي";
                            else if ($user->user_type == "supplier")
                                echo "مورد";
                            else if ($user->user_type == "customer")
                                echo "عميل";
                            else
                                echo $user->user_type;
                        ?>
                    </td>
                    <td>{{dump_date($user->created_at)}}</td>
                    <td>
                        <a href="<?= url("admin/users/assign_permission/$user->user_id") ?>">
                            <span class="label label-info"> تعديل الاذونات <i class="fa fa-edit"></i></span>
                        </a>
                    </td>
                    <td>
                        <a href="<?= url("admin/users/save/$user->user_type/$user->user_id") ?>">
                            <span class="label label-info"> تعديل البيانات <i class="fa fa-edit"></i></span>
                        </a>
                    </td>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"factory/users","add_action")): ?>
                <div class="col-md-3 col-md-offset-4">
                    <a href="{{url("/admin/users/save/$user_type")}}" class="btn btn-primary">إضافة جديد</a>
                </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
