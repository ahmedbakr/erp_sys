@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }

                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
            ?>


            <?php if($user_type == "customer"): ?>
            <div class="panel panel-primary">
                <div class="panel-heading" data-toggle="collapse" href=".add_client_fron_excel" style="cursor:pointer;">أضف عملاء من ملف اكسيل</div>
                <div class="panel-body add_client_fron_excel collapse">
                    <div class="row">
                        <form id="save_form" action="<?=url("/admin/users/import_customers")?>" method="POST" enctype="multipart/form-data">

                            <div class="col-md-12">

                                <div class="alert alert-info" style="font-weight: bold;">
                                <h3 style="text-align: center;">برجاء الإنتباه قبل تحميل ملف الاكسيل برجاء اتباع ما يلي :-</h3>
                                <ul>
                                    <li> برجاء تحميل هذا الملف وملئ الصفوف قبل الرفع <a href="{{url('uploads/excel_templates/customers_template.xlsx')}}" target="_blank">التحميل من هنا</a></li>
                                    <li>يجيب ان يكون إمتداد الملف واحد من هؤلاء ["xls",".XLS",".xlsx",".XLSX.")</li>
                                    <li>يجب ترك اول صف كما هو دون تغيير او مسح او تعديل اي عمود</li>
                                    <li>اذا تم كتابه عميل موجود مسبقا من رقم التليفون او الايميل سيتم تجاهله وعدم إدخاله</li>
                                    <li>إذا تم كتاب اكتر من صف داخل ملف الاكسيل بنفس الكود للعميل او نفس الايميل سيتم إدخال عميل واحد فقط وهو الاول </li>
                                    <li>برجاء إختيار الفرع لتسجيل العملاء بداخله </li>
                                    <li>برجاء تحديد سعر للعملاء </li>
                                    <li>user gender : <br>(ذكر او انثي)</li>
                                    <li>age range : <br>(15-25 || 26-40 || 41-60 || العمر>60)</li>
                                    <li>start join date : <br>(5/24/2017)</li>
                                </ul>

                            </div>
                            </div>

                            <?php
                                echo generate_select_tags(
                                    $field_name="related_id",
                                    $label_name="إختار الفرع لتسجيل العملاء من ملف الاكسيل",
                                    $text=convert_inside_obj_to_arr($all_branches,"full_name$en"),
                                    $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                    $selected_value="",
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = "",
                                    $grid="6"
                                );

                                echo
                                generate_select_tags(
                                    $field_name="selected_price_id",
                                    $label_name="سعر العميل",
                                    $text=array_merge([""],convert_inside_obj_to_arr($prices_list,"price_title")),
                                    $values=array_merge([""],convert_inside_obj_to_arr($prices_list,"price_id")),
                                    $selected_value="",
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = "",
                                    $grid="6"
                                );

                            ?>


                            <?php

                            echo generate_img_tags_for_form(
                                $filed_name="excel_file",
                                $filed_label="excel_file",
                                $required_field="",
                                $checkbox_field_name="pro_excel_checkbox",
                                $need_alt_title="no",
                                $required_alt_title="",
                                $old_path_value="",
                                $old_title_value="",
                                $old_alt_value="",
                                $recomended_size="",
                                $disalbed="",
                                $displayed_img_width="100",
                                $display_label="رفع ملف اكسيل للكوبونات",
                                $img_obj = ""
                            );

                            ?>

                            {{csrf_field()}}


                            <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary " style="margin-top: 5px;">

                        </form>
                    </div>
                </div>
            </div>
            <?php endif; ?>


            <?php

                $header_text="";
                $password_required = "";

                if ($user_type=="admin"){
                    $header_text="إضافة ادمن جديد";
                    $password_required = "required";
                }
                else if ($user_type=="branch"){
                    $header_text="إضافة فرع جديد";
                }
                else if ($user_type=="branch_admin"){
                    $header_text="إضافة مستخدم جديد";
                    $password_required = "required";
                }
                elseif ($user_type=="supplier"){
                    $header_text="إضافة مورد جديد";
                }
                elseif ($user_type=="customer"){
                    $header_text="إضافة عميل جديد";
                }

                $user_id="";
                if ($user_data!="") {
                    $header_text="تعديل ".$user_data->full_name;
                    $user_id=$user_data->user_id;
                    $password_required="";
                    $user_data->password="";
                }
            ?>



            <div class="panel panel-info">
                <div class="panel-heading">
                    <?=$header_text?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/users/save/$user_type/$user_id")?>" method="POST" enctype="multipart/form-data">

                            <?php if($user_type=="customer"): ?>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">بيانات هامة جدا</div>
                                    <div class="panel-body">
                                        <?php
                                            echo generate_select_tags(
                                                $field_name="related_id",
                                                $label_name="تم تسجيل بيانات العميل في فرع:",
                                                $text=convert_inside_obj_to_arr($all_branches,"full_name$en"),
                                                $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                                $selected_value="",
                                                $class="form-control",
                                                $multiple="",
                                                $required="",
                                                $disabled = "",
                                                $data = $user_data,
                                                $grid="12"
                                            );

                                            #region customer_data_1

                                            $normal_tags=array(
                                                "full_name","full_name_en","user_tel",
                                            );

                                            $attrs = generate_default_array_inputs_html(
                                                $normal_tags,
                                                $user_data,
                                                "yes",
                                                $required = ""
                                            );

                                            $attrs[0]["full_name"]="الاسم *";
                                            $attrs[0]["full_name_en"]="الاسم بالانجليزي";
                                            $attrs[0]["user_tel"]="التليفون";


                                            $attrs[2]["full_name"]="required";

                                            $attrs[6]["full_name"]="4";
                                            $attrs[6]["full_name_en"]="4";
                                            $attrs[6]["user_tel"]="4";

                                            echo
                                            generate_inputs_html(
                                                reformate_arr_without_keys($attrs[0]),
                                                reformate_arr_without_keys($attrs[1]),
                                                reformate_arr_without_keys($attrs[2]),
                                                reformate_arr_without_keys($attrs[3]),
                                                reformate_arr_without_keys($attrs[4]),
                                                reformate_arr_without_keys($attrs[5]),
                                                reformate_arr_without_keys($attrs[6])
                                            );

                                            #endregion

                                        ?>

                                        <?php
                                            echo
                                            generate_select_tags(
                                                $field_name="age_range",
                                                $label_name="الفئة العمرية",
                                                $text=["15-25","26-40","41-60","العمر>60"],
                                                $values=["15-25","26-40","41-60","العمر>60"],
                                                $selected_value="",
                                                $class="form-control",
                                                $multiple="",
                                                $required="required",
                                                $disabled = "",
                                                $data = $user_data,
                                                $grid="6",
                                                true
                                            );


                                            echo
                                            generate_select_tags(
                                                $field_name="user_gender",
                                                $label_name="نوع العميل",
                                                $text=["ذكر","انثي"],
                                                $values=["male","female"],
                                                $selected_value="",
                                                $class="form-control",
                                                $multiple="",
                                                $required="required",
                                                $disabled = "",
                                                $data = $user_data,
                                                $grid="6",
                                                true
                                            );
                                        ?>

                                    </div>
                                </div>

                                <div class="panel panel-default">
                                <div class="panel-heading">بيانات اضافية</div>
                                <div class="panel-body">
                                    <?php
                                        #region customer_data_2

                                        $normal_tags=array(
                                            "email","user_mobile","user_fax","start_join_date","user_address"
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $user_data,
                                            "yes",
                                            $required = ""
                                        );

                                        $attrs[0]["email"]="الايميل *";
                                        $attrs[0]["user_mobile"]="الموبايل";
                                        $attrs[0]["user_fax"]="الفاكس";
                                        $attrs[0]["start_join_date"]="تاريخ بدء التعامل *";
                                        $attrs[0]["user_address"]="العنوان";


                                        $attrs[2]["start_join_date"]="required";

                                        $attrs[3]["start_join_date"]="date";

                                        $attrs[4]["start_join_date"]=($user_data!="")?$user_data->start_join_date:date("Y-m-d");

                                        $attrs[6]["email"]="3";
                                        $attrs[6]["user_address"]="3";
                                        $attrs[6]["user_mobile"]="3";
                                        $attrs[6]["user_fax"]="3";
                                        $attrs[6]["start_join_date"]="3";

                                        echo
                                        generate_inputs_html(
                                            reformate_arr_without_keys($attrs[0]),
                                            reformate_arr_without_keys($attrs[1]),
                                            reformate_arr_without_keys($attrs[2]),
                                            reformate_arr_without_keys($attrs[3]),
                                            reformate_arr_without_keys($attrs[4]),
                                            reformate_arr_without_keys($attrs[5]),
                                            reformate_arr_without_keys($attrs[6])
                                        );

                                        #endregion

                                        echo
                                        generate_select_tags(
                                            $field_name="selected_price_id",
                                            $label_name="سعر العميل",
                                            $text=array_merge([""],convert_inside_obj_to_arr($prices_list,"price_title")),
                                            $values=array_merge([""],convert_inside_obj_to_arr($prices_list,"price_id")),
                                            $selected_value="",
                                            $class="form-control",
                                            $multiple="",
                                            $required="$disabled_select_price_list",
                                            $disabled = "",
                                            $data = $user_data,
                                            $grid="3"
                                        );



                                    ?>


                                    <?php
                                        echo generate_select_tags(
                                            $field_name="how_customer_know_us",
                                            $label_name="كيف تعرف علينا العميل",
                                            $text=["","عن طريق السوشيال ميديا","اعلانات التلفزيون","عن طريق صديق"],
                                            $values=["","عن طريق السوشيال ميديا","اعلانات التلفزيون","عن طريق صديق"],
                                            $selected_value="",
                                            $class="form-control",
                                            $multiple="",
                                            $required="",
                                            $disabled = "",
                                            $data = $user_data,
                                            $grid="3"
                                        );
                                    ?>

                                    <?php if($user_data==""): ?>



                                        <div class="form-group col-md-3">
                                            <?php if(false): ?>
                                            <input type="text" name="original_client_tel_number" class="form-control original_client_tel_number">
                                            <?php endif; ?>

                                            <label for="">من طرف عميل (رقم تليفون العميل الاصلي)</label>
                                            <select name="original_client_tel_number" class="form-control client_tel_number">
                                                <option value="0">0</option>
                                            </select>
                                        </div>

                                    <?php endif; ?>


                                </div>
                            </div>
                            </div>


                            <?php else: ?>


                                <?php

                                    $disabled_branch_admin_user_features = "disabled";
                                    if($current_user->user_type == "admin")
                                    {
                                        $disabled_branch_admin_user_features = "";
                                    }

                                    if ($user_type=="branch_admin"){

                                        $field_text_2=[];
                                        $field_values_2=[];
                                        $field_2_depend_values=[];
//                                    $field_text_2[]="";
//                                    $field_values_2[]=0;
//                                    $field_2_depend_values[] = 0;
                                        foreach($all_branches as $branch){
                                            $field_text_2[]="";
                                            $field_values_2[]=0;
                                            $field_2_depend_values[]=$branch->user_id;
                                        }

                                        if(isset_and_array($all_point_of_sales)){
                                            $field_text_2=array_merge($field_text_2,convert_inside_obj_to_arr($all_point_of_sales,"point_of_sale_desc"));
                                            $field_values_2=array_merge($field_values_2,convert_inside_obj_to_arr($all_point_of_sales,"point_of_sale_id"));
                                            $field_2_depend_values=array_merge($field_2_depend_values,convert_inside_obj_to_arr($all_point_of_sales,"branch_id"));
                                        }



                                        //make it dependent select
                                        echo generate_depended_selects(
                                            $field_name_1="related_id",
                                            $field_label_1="إختار الفرع *",
                                            $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name$en"),
                                            $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                                            $field_selected_value_1=(is_object($user_data)?$user_data->related_id:""),
                                            $field_required_1="$disabled_branch_admin_user_features",
                                            $field_class_1="form-control",
                                            $field_name_2="point_of_sale_id",
                                            $field_label_2="إختار نقطة البيع",
                                            $field_text_2,
                                            $field_values_2,
                                            $field_selected_value_2=(is_object($user_data)?(int)$user_data->point_of_sale_id:""),
                                            $field_2_depend_values,
                                            $field_required_2="$disabled_branch_admin_user_features",
                                            $field_class_2="form-control",
                                            false,
                                            $user_data
                                        );
                                    }
                                    ?>


                                <?php
                                    if($user_type=="admin"){
                                        $normal_tags=array(
                                            "full_name","full_name_en","email","password"
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $user_data,
                                            "yes",
                                            $required = ""
                                        );

                                        $attrs[0]["full_name"]="اسم الادمن*";
                                        $attrs[0]["full_name_en"]="اسم الادمن انجليزي*";
                                        $attrs[0]["email"]="البريد الالكتروني*";
                                        $attrs[0]["password"]="الباسورد";

                                        $attrs[2]["full_name"]="required";
                                        $attrs[2]["full_name_en"]="required";
                                        $attrs[2]["email"]="required";
                                        $attrs[2]["password"]=$password_required;

                                        $attrs[3]["email"]="email";
                                        $attrs[3]["password"]="password";


                                        $attrs[6]["full_name"]="6";
                                        $attrs[6]["full_name_en"]="6";
                                        $attrs[6]["email"]="6";
                                        $attrs[6]["password"]="6";

                                    }
                                    else if($user_type=="branch"){

                                        $normal_tags=array(
                                            "full_name","full_name_en","email",
                                            "user_tel","user_mobile","user_fax",
                                            "user_commission_days","user_address",
                                            "commercial_record_number","commercial_record_expire_date"
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $user_data,
                                            "yes",
                                            $required = ""
                                        );

                                        $attrs[0]["email"]="ايميل الفرع *";
                                        $attrs[0]["full_name"]="اسم الفرع *";
                                        $attrs[0]["full_name_en"]="اسم الفرع بالانجليزي";
                                        $attrs[0]["user_tel"]="تليفون الفرع";
                                        $attrs[0]["user_mobile"]="موبايل الفرع";
                                        $attrs[0]["user_fax"]="فاكس الفرع";
                                        $attrs[0]["user_commission_days"]="تصفية العمولة بعد كام يوم (لو 0 سيتم السماح بتصفية العمولة في نفس اليوم )";
                                        $attrs[0]["user_address"]="عنوان الفرع";
                                        $attrs[0]["commercial_record_number"]="رقم السجل التجاري";
                                        $attrs[0]["commercial_record_expire_date"]="تاريخ إنتهاء السجل التجاري";


                                        $attrs[2]["email"]="required";
                                        $attrs[2]["full_name"]="required";
                                        $attrs[2]["full_name_en"]="required";

                                        $attrs[3]["user_address"]="textarea";
                                        $attrs[3]["user_commission_days"]="number";
                                        $attrs[3]["commercial_record_expire_date"]="date";

                                        $attrs[4]["commercial_record_expire_date"]=($user_data =="" || $user_data->commercial_record_expire_date == "0000-00-00")?date("Y-m-d"):$user_data->commercial_record_expire_date;
                                        $add_class_expire_date_id = "";
                                        if ($user_data !="" && $user_data->commercial_record_expire_date != "0000-00-00")
                                        {

                                            $diff_in_days = \Carbon\Carbon::createFromTimestamp(strtotime($user_data->commercial_record_expire_date))->diffInDays();
                                            if ($diff_in_days <= 60)
                                            {
                                                $add_class_expire_date_id = " red_field ";
                                            }
                                        }

                                        $attrs[5]["commercial_record_expire_date"] .=" $add_class_expire_date_id";


                                        $attrs[6]["email"]="3";
                                        $attrs[6]["full_name"]="3";
                                        $attrs[6]["full_name_en"]="3";
                                        $attrs[6]["user_commission_days"]="3";
                                        $attrs[6]["user_address"]="3";
                                        $attrs[6]["user_tel"]="3";
                                        $attrs[6]["user_mobile"]="3";
                                        $attrs[6]["user_fax"]="3";
                                        $attrs[6]["commercial_record_number"]="6";
                                        $attrs[6]["commercial_record_expire_date"]="3";


                                    }
                                    else if($user_type=="branch_admin"){

                                        $normal_tags=array(
                                            "user_code","full_name","full_name_en","email","password",
                                            "user_tel","user_mobile","user_fax","start_join_date","user_address",
                                            "identification_number","nationality","identification_expire_date",
                                            "user_job_on_passport","user_job_on_company"
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $user_data,
                                            "yes",
                                            $required = ""
                                        );

                                        $attrs[0]["user_code"]="الكود";
                                        $attrs[0]["email"]="الايميل *";
                                        $attrs[0]["full_name"]="الاسم *";
                                        $attrs[0]["full_name_en"]="الاسم بالانجليزي";
                                        $attrs[0]["user_address"]="العنوان";
                                        $attrs[0]["user_tel"]="التليفون";
                                        $attrs[0]["user_mobile"]="الموبايل";
                                        $attrs[0]["user_fax"]="الفاكس";
                                        $attrs[0]["start_join_date"]="تاريخ بدء العمل *";
                                        $attrs[0]["password"]="كلمة السر";

                                        $attrs[0]["identification_number"]="رقم الهوية الوطنية او الإقامة";
                                        $attrs[0]["nationality"]="الجنسية";
                                        $attrs[0]["identification_expire_date"]="تاريخ إنتهاء الهوية او الإقامة";
                                        $attrs[0]["user_job_on_passport"]="المهنه في الإقامة";
                                        $attrs[0]["user_job_on_company"]="المهنه في الشركة";


                                        $attrs[2]["user_code"]="$disabled_branch_admin_user_features";
                                        $attrs[2]["email"]="required";
                                        $attrs[2]["full_name"]="required";
                                        $attrs[2]["start_join_date"]=($disabled_branch_admin_user_features == "")?"required":$disabled_branch_admin_user_features;
                                        $attrs[2]["password"]=$password_required;

                                        $attrs[3]["email"]="email";
                                        $attrs[3]["user_address"]="textarea";
                                        $attrs[3]["start_join_date"]="date";
                                        $attrs[3]["identification_expire_date"]="date";
                                        $attrs[3]["password"]="password";

                                        $attrs[4]["start_join_date"]=($user_data!="")?$user_data->start_join_date:date("Y-m-d");
                                        $attrs[4]["identification_expire_date"]=($user_data =="" || $user_data->identification_expire_date == "0000-00-00")?date("Y-m-d"):$user_data->identification_expire_date;
                                        $attrs[4]["password"]="";

                                        $add_class_expire_date_id = "";
                                        if ($user_data !="" && $user_data->identification_expire_date != "0000-00-00")
                                        {

                                            $diff_in_days = \Carbon\Carbon::createFromTimestamp(strtotime($user_data->identification_expire_date))->diffInDays();
                                            if ($diff_in_days <= 60)
                                            {
                                                $add_class_expire_date_id = " red_field ";
                                            }
                                        }

                                        $attrs[5]["identification_expire_date"] .=" $add_class_expire_date_id";

                                        $attrs[6]["user_code"]="4";
                                        $attrs[6]["full_name"]="4";
                                        $attrs[6]["full_name_en"]="4";
                                        $attrs[6]["email"]="6";
                                        $attrs[6]["password"]="6";
                                        $attrs[6]["user_address"]="4";
                                        $attrs[6]["user_tel"]="4";
                                        $attrs[6]["user_mobile"]="4";
                                        $attrs[6]["user_fax"]="4";
                                        $attrs[6]["start_join_date"]="4";
                                        $attrs[6]["identification_number"]="4";
                                        $attrs[6]["nationality"]="4";
                                        $attrs[6]["identification_expire_date"]="4";
                                        $attrs[6]["user_job_on_passport"]="4";
                                        $attrs[6]["user_job_on_company"]="4";

                                    }
                                    else if($user_type=="supplier"){

                                        $normal_tags=array(
                                            "full_name","full_name_en",
                                            "email",
                                            "user_tel","user_mobile","user_fax","start_join_date","user_address"
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $user_data,
                                            "yes",
                                            $required = ""
                                        );

                                        $attrs[0]["email"]="الايميل *";
                                        $attrs[0]["full_name"]="الاسم *";
                                        $attrs[0]["full_name_en"]="الاسم بالانجليزي";
                                        $attrs[0]["user_address"]="العنوان";
                                        $attrs[0]["user_tel"]="التليفون";
                                        $attrs[0]["user_mobile"]="الموبايل";
                                        $attrs[0]["user_fax"]="الفاكس";
                                        $attrs[0]["start_join_date"]="تاريخ بدء التعامل *";


                                        $attrs[2]["email"]="required";
                                        $attrs[2]["full_name"]="required";
                                        $attrs[2]["start_join_date"]="required";

                                        $attrs[3]["user_address"]="textarea";
                                        $attrs[3]["start_join_date"]="date";

                                        $attrs[4]["start_join_date"]=($user_data!="")?$user_data->start_join_date:date("Y-m-d");

                                        $attrs[6]["email"]="3";
                                        $attrs[6]["full_name"]="3";
                                        $attrs[6]["full_name_en"]="3";
                                        $attrs[6]["user_address"]="3";
                                        $attrs[6]["user_tel"]="3";
                                        $attrs[6]["user_mobile"]="3";
                                        $attrs[6]["user_fax"]="3";
                                        $attrs[6]["start_join_date"]="3";

                                    }

                                    echo
                                    generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5]),
                                        reformate_arr_without_keys($attrs[6])
                                    );
                                ?>

                            <?php endif; ?>

                            <hr>

                            <?php
                                $img_obj = isset($user_data->user_img_file) ? $user_data->user_img_file : "";

                                echo generate_img_tags_for_form(
                                    $filed_name="user_img_file",
                                    $filed_label="user_img_file",
                                    $required_field="",
                                    $checkbox_field_name="user_img_checkbox",
                                    $need_alt_title="no",
                                    $required_alt_title="",
                                    $old_path_value="",
                                    $old_title_value="",
                                    $old_alt_value="",
                                    $recomended_size="",
                                    $disalbed="",
                                    $displayed_img_width="100",
                                    $display_label="الصورة",
                                    $img_obj
                                );

                            ?>


                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                        <style>
                            .red_field{
                                background-color: red !important;
                            }
                        </style>

                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection
