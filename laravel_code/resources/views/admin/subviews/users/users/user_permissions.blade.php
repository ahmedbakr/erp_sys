@extends('admin.main_layout')

@section('subview')


    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!-- END Select 2 -->

    <script>
        $(function(){
            $('.select_user_permissions').select2();
        });
    </script>

    <div class="">
        <div class="row">
            <div class="col-md-12">

                <style>
                    hr{
                        width: 100%;
                        height:1px;
                    }
                </style>
                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }

                if (isset($success)&&!empty($success)) {
                    echo $success;
                }

                ?>


                <div class="panel panel-info">
                    <div class="panel-heading"><h2>تعديلات إذونات ({{$user_obj->full_name}}) </h2></div>
                    <div class="panel-body">
                        <div class="row">
                            <form id="save_form" action="<?=url("admin/users/assign_permission/$user_obj->user_id")?>" method="POST" enctype="multipart/form-data">


                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist" style="float: right;">
                                        <?php $counter = 0; ?>
                                        <?php foreach($all_user_permissions_groups as $key => $permissions_group): ?>
                                            <?php
                                                if(in_array($key,["hide","الانتاج","الموردين"]))continue;
                                            ?>
                                            <li role="presentation" class="{{($counter==0)?"active":""}}">
                                                <a href="#permission_group_{{$counter+1}}" aria-controls="home" role="tab" data-toggle="tab">{{$key}}</a>
                                            </li>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <?php $counter = 0; ?>
                                        <?php foreach($all_user_permissions_groups as $key => $permissions_group): ?>
                                            <?php
                                                if(in_array($key,["hide","الانتاج","الموردين"]))continue;
                                            ?>
                                            <div role="tabpanel" class="tab-pane {{($counter==0)?"active":""}}" id="permission_group_{{$counter+1}}">
                                                <?php
                                                    $permissions_group = collect($permissions_group)->groupBy("per_page_id");
                                                    $all_user_permissions = $permissions_group->all();
                                                ?>
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>الصلاحية</th>
                                                        <th>المشاهدة</th>
                                                        <th>الاضافة</th>
                                                        <th>التعديل</th>
                                                        <th>المسح</th>
                                                        <th style="    width: 30%;">أذونات اخري</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    <?php foreach($all_user_permissions as $user_per_key=>$user_per_val): ?>
                                                    <?php
                                                    $user_per_val = $user_per_val->all();
                                                    $user_per_val = $user_per_val[0];

                                                    if(!isset($all_permission_pages[$user_per_key])){
                                                        continue;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <th>{{$all_permission_pages[$user_per_key]->page_label}}</th>
                                                        <th>
                                                            <?php
                                                            echo generate_multi_accepters(
                                                                $accepturl="",
                                                                $item_obj=$user_per_val,
                                                                $item_primary_col="per_id",
                                                                $accept_or_refuse_col="show_action",
                                                                $model='App\models\permissions\permissions_m',
                                                                $accepters_data=["0"=>"رفض","1"=>"موافقة"]
                                                            );
                                                            ?>
                                                        </th>
                                                        <th>
                                                            <?php
                                                            echo generate_multi_accepters(
                                                                $accepturl="",
                                                                $item_obj=$user_per_val,
                                                                $item_primary_col="per_id",
                                                                $accept_or_refuse_col="add_action",
                                                                $model='App\models\permissions\permissions_m',
                                                                $accepters_data=["0"=>"رفض","1"=>"موافقة"]
                                                            );
                                                            ?>
                                                        </th>
                                                        <th>
                                                            <?php
                                                            echo generate_multi_accepters(
                                                                $accepturl="",
                                                                $item_obj=$user_per_val,
                                                                $item_primary_col="per_id",
                                                                $accept_or_refuse_col="edit_action",
                                                                $model='App\models\permissions\permissions_m',
                                                                $accepters_data=["0"=>"رفض","1"=>"موافقة"]
                                                            );
                                                            ?>
                                                        </th>
                                                        <th>
                                                            <?php
                                                            echo generate_multi_accepters(
                                                                $accepturl="",
                                                                $item_obj=$user_per_val,
                                                                $item_primary_col="per_id",
                                                                $accept_or_refuse_col="delete_action",
                                                                $model='App\models\permissions\permissions_m',
                                                                $accepters_data=["0"=>"رفض","1"=>"موافقة"]
                                                            );
                                                            ?>
                                                        </th>
                                                        <th>

                                                            <?php
                                                            $all_additional_permissions=$all_permission_pages[$user_per_key]->all_additional_permissions;
                                                            $all_additional_permissions=json_decode($all_additional_permissions,true);

                                                            if(!isset_and_array($all_additional_permissions)){
                                                                $all_additional_permissions=[];
                                                            }

                                                            $selected_additional_permissions=json_decode($user_per_val->additional_permissions);
                                                            if(!isset_and_array($selected_additional_permissions)){
                                                                $selected_additional_permissions=[];
                                                            }

                                                            $all_additional_permissions_ar=$all_permission_pages[$user_per_key]->all_additional_permissions_ar;
                                                            $all_additional_permissions_ar=json_decode($all_additional_permissions_ar,true);

                                                            $combined = array_map(
                                                                function($a, $b) { return $a . ' : ' . $b; }, $all_additional_permissions_ar, $all_additional_permissions
                                                            );

                                                            echo generate_select_tags(
                                                                $field_name="additional_perms_new".$user_per_val->per_id,
                                                                $label_name="إختار",
                                                                $text=$combined,
                                                                $values=$all_additional_permissions,
                                                                $selected_value=$selected_additional_permissions,
                                                                $class="form-control select_user_permissions",
                                                                $multiple="multiple",
                                                                $required="",
                                                                $disabled = "",
                                                                $data = ""
                                                            );

                                                            ?>

                                                        </th>
                                                    </tr>
                                                    <?php endforeach;?>
                                                    </tbody>
                                                </table>
                                                <?php $counter++; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                </div>


                                {{csrf_field()}}
                                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
                            </form>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>

@endsection
