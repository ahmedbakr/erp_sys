@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">



            <form id="save_form" action="<?=url("/admin/users/change_password")?>" method="POST" enctype="multipart/form-data">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        تغيير باسورد المستخدم
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-6">
                            <label for="">الباسورد القديم</label>
                            <input type="password" name="old_password" class="form-control">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="">الباسورد الجديد</label>
                            <input type="password" name="new_password" class="form-control">
                        </div>



                        {{csrf_field()}}

                        <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary " style="margin-top: 5px;">

                    </div>
                </div>

            </form>
        </div>
    </div>


@endsection