@extends('admin.main_layout')

@section('subview')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <div id="page-wrapper">
        <div class="graphs">
            <h3 class="blank1"><img src="{{get_profile_logo_or_default($supplier_data->path)}}"> {{$supplier_data->full_name}}</h3>

            <div class="col_3">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-shopping-cart" style="color: #8BC34A"></i>
                            <div class="stats">
                                <h5>{{$total_purchases_orders}}</h5>
                                <div class="grow">
                                    <p>عدد العناصر للمشتريات</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase"></i>
                            <div class="stats">
                                <h5>{{$total_paid}}</h5>
                                <div class="grow grow2">
                                    <p>مجموع المدفوع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red;"></i>
                            <div class="stats">
                                <h5>{{$total_remained}}</h5>
                                <div class="grow grow3">
                                    <p>مجموع المتبقي</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3 col-md-offset-1 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-shopping-cart" style="color: red;"></i>
                            <div class="stats">
                                <h5>{{$total_returns_orders}}</h5>
                                <div class="grow grow3">
                                    <p>عدد العناصر للمرتجع</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase"></i>
                            <div class="stats">
                                <h5>{{$total_returns_received}}</h5>
                                <div class="grow grow2">
                                    <p>مجموع المستلم</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 widget">
                        <div class="r3_counter_box">
                            <i class="fa fa-briefcase" style="color: red;"></i>
                            <div class="stats">
                                <h5>{{$total_returns_remained}}</h5>
                                <div class="grow grow3">
                                    <p>مجموع المتبقي</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="clearfix"></div>
            </div>

            <div class="xs">
                <div class="col-md-4 scrollbar email-list1" style="height: 680px;" id="style-2">
                    <ul class="collection">
                        <li class="collection-item avatar email-unread">
                            <i class="fa fa-tag icon_1 Role"></i>
                            <div class="avatar_left">
                                <span class="email-title">دورة</span>
                                <p class="truncate grey-text ultra-small">دورة في هذا النظام.</p>
                            </div>
                            <a class="secondary-content"><span class="new badge1 blue">{{$supplier_data->role}}</span></a>
                            <div class="clearfix"> </div>
                        </li>
                        <li class="collection-item avatar email-unread">
                            <i class="fa fa-exclamation-circle icon_2"></i>
                            <div class="avatar_left">
                                <span class="email-title">الحالة</span>
                                <p class="truncate grey-text ultra-small">هل هو نشط ام لا.</p>
                            </div>
                            <a class="secondary-content">

                                <?php if($supplier_data->user_active && $supplier_data->user_can_login): ?>
                                    <span class="new badge1 blue">نشظ</span>
                                    <?php else: ?>
                                    <span class="new badge1 red">غير نشط</span>
                                <?php endif; ?>

                            </a>
                            <div class="clearfix"> </div>
                        </li>
                        <li class="collection-item avatar email-unread">
                            <i class="fa fa-tag icon_3"></i>
                            <div class="avatar_left">
                                <span class="email-title">بدأ من </span>
                                <p class="truncate grey-text ultra-small">تاريخ بدايته في هذا النظام</p>
                            </div>
                            <a class="secondary-content">
                                <span class="new badge1 blue1" style="font-size: 11px;">
                                    {{$supplier_data->created_at}}
                                    <br>
                                    {{\Carbon\Carbon::createFromTimestamp(strtotime($supplier_data->created_at))->diffForHumans()}}
                                </span>
                            </a>
                            <div class="clearfix"> </div>
                        </li>
                        <?php if(isset($supplier_data->contacts->email) && !empty($supplier_data->contacts->email)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-envelope icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title">{{$supplier_data->contacts->email}}</span>
                                    <p class="truncate grey-text ultra-small">الايميل</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->address) && !empty($supplier_data->contacts->address)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-home icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title">{{$supplier_data->contacts->address}}</span>
                                    <p class="truncate grey-text ultra-small">العنوان</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->telephone) && !empty($supplier_data->contacts->telephone)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-phone icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title">{{$supplier_data->contacts->telephone}}</span>
                                    <p class="truncate grey-text ultra-small">التليفون</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->mobile) && !empty($supplier_data->contacts->mobile)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-mobile-phone icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title">{{$supplier_data->contacts->mobile}}</span>
                                    <p class="truncate grey-text ultra-small">رقم الموبايل</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->fax) && !empty($supplier_data->contacts->fax)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-phone icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title">{{$supplier_data->contacts->fax}}</span>
                                    <p class="truncate grey-text ultra-small">الفاكس</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->website) && !empty($supplier_data->contacts->website)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="fa fa-globe icon_3"></i>
                                <div class="avatar_left">
                                    <span class="email-title"><a href="{{$supplier_data->contacts->website}}" target="_blank"> زيارة الموقع</a></span>
                                    <p class="truncate grey-text ultra-small">الموقع الشخصي</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->google_plus) && !empty($supplier_data->contacts->google_plus)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="icon_4">G</i>
                                <div class="avatar_left">
                                    <a href="{{$supplier_data->contacts->google_plus}}"><span class="email-title">الجيميل</span></a>
                                    <p class="truncate grey-text ultra-small">زيارة الحساب الشخصي علي جوجل بلس.</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->facebook) && !empty($supplier_data->contacts->facebook)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="icon_4 icon_5">F</i>
                                <div class="avatar_left">
                                    <a href="{{$supplier_data->contacts->facebook}}"><span class="email-title">الفيسبوك</span></a>
                                    <p class="truncate grey-text ultra-small">زيارة الحساب الشخصي علي فيسبوك.</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                        <?php if(isset($supplier_data->contacts->twitter) && !empty($supplier_data->contacts->twitter)): ?>
                            <li class="collection-item avatar email-unread">
                                <i class="icon_4 icon_6">T</i>
                                <div class="avatar_left">
                                    <a href="{{$supplier_data->contacts->twitter}}"><span class="email-title">تويتر</span></a>
                                    <p class="truncate grey-text ultra-small">زيارة الحساب الشخصي علي تويتر.</p>
                                </div>
                                <div class="clearfix"> </div>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
                <div class="col-md-8 inbox_right">
                    <div class="mail-toolbar clearfix">

                        <div class="col-md-12">
                            <?php if(count($material_purchases_orders_names)): ?>
                                <input type="hidden" class="material_purchases_orders_names" value="{{html_entity_decode(json_encode($material_purchases_orders_names))}}">
                                <div id="material_purchases_quantity" style="width: 800px; height: 300px;"></div>
                            <?php endif; ?>

                        </div>
                        <div class="col-md-12">
                            <?php if(count($material_returns_orders_names)): ?>
                                <input type="hidden" class="material_returns_orders_names" value="{{html_entity_decode(json_encode($material_returns_orders_names))}}">
                                <div id="material_returns_quantity" style="width: 800px; height: 300px;"></div>
                            <?php endif; ?>

                        </div>

                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col_1">
                <div class="col-md-12">
                    <div class="activity_box activity_box1">
                        <h3>جميع الفواتير الخاصه به</h3>
                        <div class="scrollbar" id="style-2">
                            <?php if(is_array($bills_data) && count($bills_data) && isset($bills_data[0]->supplier_bill_id)): ?>

                                <table class="table table-striped table-bordered table_without_paging">
                                    <thead>
                                        <th>الرقم</th>
                                        <th>كمية المشتريات</th>
                                        <th>سعر</th>
                                        <th>المدفوع</th>
                                        <th>المتبقي</th>
                                        <th>كميه المرتجع</th>
                                        <th>السعر</th>
                                        <th>المستلم</th>
                                        <th>المتبقي</th>
                                        <th>مصاريف اخري</th>
                                        <th>تاريخ الفاتورة</th>
                                        <th>تاريخ التسجيل</th>
                                    </thead>

                                    <tbody>
                                        <?php foreach($bills_data as $key => $bill): ?>

                                        <tr>
                                            <td>{{$bill->supplier_bill_id}}</td>
                                            <td>
                                                <span class="label label-default">
                                                    {{$bill->total_purchases_amount}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-info">
                                                    {{$bill->total_purchases_price}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-success">
                                                    {{$bill->total_paid_money}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-danger">
                                                    {{$bill->total_remain_money}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-default">
                                                    {{$bill->total_returns_amount}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-info">
                                                    {{$bill->total_returns_price}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-success">
                                                    {{$bill->return_received_money}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-danger">
                                                    {{$bill->return_remained_money}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-warning">
                                                    {{$bill->supplier_bill_extra_cost}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-info">
                                                    {{$bill->supplier_bill_date}}
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-info">
                                                    {{$bill->created_at}}
                                                </span>
                                                <span class="label label-success">
                                                    {{\Carbon\Carbon::createFromTimestamp(strtotime($bill->created_at))->diffForHumans()}}
                                                </span>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>


                                <?php else: ?>
                                {!! get_warning(url('/admin/order/supplier_materials'),"لا توجد فواتير له اضف من هنا") !!}

                            <?php endif; ?>

                            
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>

            </div>
        </div>
    </div>


@endsection