@extends('admin.main_layout')

@section('subview')


    <?php

    $header_text = "<i class='fa fa-plus'></i> إضافة وحدة قياس جديدة";
    $pro_unit_id = "";
    if (is_object($product_unit_data))
    {
        $pro_unit_id = $product_unit_data->pro_unit_id;
        $header_text = "<i class='fa fa-edit'></i> تعديل وحدة قياس رقم.$pro_unit_id";
    }

    ?>


    <div class="container">

        <div class="col-md-8 col-md-offset-2">

            <form id="save_form" action="<?=url("/admin/product_units/save/$pro_unit_id")?>" method="POST" enctype="multipart/form-data">
                <h1>{!! $header_text !!}</h1>
                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }

                $normal_tags=array("pro_unit_name","pro_unit_name_en");
                $attrs = generate_default_array_inputs_html(
                    $normal_tags,
                    $product_unit_data,
                    "yes",
                    $required = "required"
                );

                $attrs[0]["pro_unit_name"]="اسم الوحدة بالعربي";
                $attrs[0]["pro_unit_name_en"]="اسم الوحدة بالانجليزي";


                $attrs[6]["pro_unit_name"]="6";
                $attrs[6]["pro_unit_name_en"]="6";

                echo
                generate_inputs_html(
                    reformate_arr_without_keys($attrs[0]),
                    reformate_arr_without_keys($attrs[1]),
                    reformate_arr_without_keys($attrs[2]),
                    reformate_arr_without_keys($attrs[3]),
                    reformate_arr_without_keys($attrs[4]),
                    reformate_arr_without_keys($attrs[5]),
                    reformate_arr_without_keys($attrs[6])
                );

                ?>

                {{csrf_field()}}
                <input id="submit" type="submit" value="حفظ" class="col-md-4 btn btn-primary btn-lg">
                <a href="{{url('/admin/product_units/save')}}" class="col-md-4 col-md-offset-2 btn btn-primary btn-lg"><i class="fa fa-plus"></i> أضف جديد</a>
            </form>

        </div>

    </div>


@endsection