@extends('admin.main_layout')

@section('subview')


    <div class="container">

        <div class="col-md-12">


            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>اسم الوحدة بالعربي</td>
                        <td>اسم الوحدة بالإنجليزي</td>
                        <?php if(check_permission($user_permissions,"admin/product_units","edit_action",$current_user)): ?>
                            <td>تعديل</td>
                        <?php endif; ?>
                        <?php if(check_permission($user_permissions,"admin/product_units","delete_action",$current_user)): ?>
                            <td>مسح</td>
                        <?php endif; ?>

                    </tr>
                </thead>

                <tbody>

                    <?php foreach($product_units as $key => $product_unit): ?>

                        <tr id="row{{$product_unit->pro_unit_id}}">
                            <td>{{$key+1}}</td>
                            <td>
                                <span class="label label-info">
                                    {{$product_unit->pro_unit_name}}
                                </span>
                            </td>
                            <td>
                                <span class="label label-info">
                                    {{$product_unit->pro_unit_name_en}}
                                </span>
                            </td>

                            <?php if(check_permission($user_permissions,"admin/product_units","edit_action",$current_user)): ?>
                                <td>
                                    <a class="btn btn-primary btn-block" href="{{url('/admin/product_units/save/'.$product_unit->pro_unit_id)}}"> تعديل </a>
                                </td>
                            <?php endif; ?>

                            <?php if(check_permission($user_permissions,"admin/product_units","delete_action",$current_user)): ?>
                                <td>
                                    <a href='#' class="general_remove_item" data-deleteurl="<?= url("/admin/product_units/remove_product_unit") ?>" data-tablename="App\models\product\product_units_m"  data-itemid="<?= $product_unit->pro_unit_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                                </td>
                            <?php endif; ?>

                        </tr>

                    <?php endforeach; ?>

                </tbody>

            </table>


            <?php if(check_permission($user_permissions,"admin/product_units","add_action",$current_user)): ?>
                <div class="col-md-3 col-md-offset-3">
                    <a class="btn btn-primary btn-block" href="{{url('/admin/product_units/save')}}"> إضافة وحدة قياس جديدة </a>
                </div>
            <?php endif; ?>

        </div>

    </div>


@endsection