@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <?php

            $header_text="تسجيل تنقل جديد";

            ?>



            <div class="panel panel-info">
                <div class="panel-heading"><h2><?=$header_text?></h2></div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/users_transits/save")?>" method="POST" enctype="multipart/form-data">


                            <div class="col-md-4">
                                <?php
                                echo generate_select_tags(
                                    $field_name="user_id",
                                    $label_name="إختار المستخدم *",
                                    $text=convert_inside_obj_to_arr($all_cashiers,"full_name"),
                                    $values=convert_inside_obj_to_arr($all_cashiers,"user_id"),
                                    $selected_value=[""],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required = "required",
                                    $disabled = "",
                                    $data = ""
                                );

                                ?>
                            </div>

                            <div class="col-md-8">
                                <?php
//                                echo generate_select_tags(
//                                    $field_name="to_branch",
//                                    $label_name="إختار الفرع *",
//                                    $text=convert_inside_obj_to_arr($all_branches,"full_name$en"),
//                                    $values=convert_inside_obj_to_arr($all_branches,"user_id"),
//                                    $selected_value=[""],
//                                    $class="form-control",
//                                    $multiple="",
//                                    $required="required",
//                                    $disabled = "",
//                                    $data = ""
//                                );

                                $field_text_2=[];
                                $field_values_2=[];
                                $field_2_depend_values=[];
                                foreach($all_branches as $branch){
                                    $field_text_2[]="";
                                    $field_values_2[]=0;
                                    $field_2_depend_values[]=$branch->user_id;
                                }

                                if(isset_and_array($all_point_of_sales)){
                                    $field_text_2=array_merge($field_text_2,convert_inside_obj_to_arr($all_point_of_sales,"point_of_sale_desc"));
                                    $field_values_2=array_merge($field_values_2,convert_inside_obj_to_arr($all_point_of_sales,"point_of_sale_id"));
                                    $field_2_depend_values=array_merge($field_2_depend_values,convert_inside_obj_to_arr($all_point_of_sales,"branch_id"));
                                }



                                //make it dependent select
                                echo generate_depended_selects(
                                    $field_name_1="to_branch",
                                    $field_label_1="إختار الفرع *",
                                    $field_text_1=convert_inside_obj_to_arr($all_branches,"full_name"),
                                    $field_values_1=convert_inside_obj_to_arr($all_branches,"user_id"),
                                    $field_selected_value_1="",
                                    $field_required_1="",
                                    $field_class_1="form-control",
                                    $field_name_2="point_of_sale_id",
                                    $field_label_2="إختار نقطة البيع",
                                    $field_text_2,
                                    $field_values_2,
                                    $field_selected_value_2="",
                                    $field_2_depend_values,
                                    $field_required_2="",
                                    $field_class_2="form-control",
                                    false,
                                    ""
                                );

                                ?>
                            </div>


                            <?php

                                $normal_tags=array(
                                    "user_transit_date","user_transit_reason"
                                );

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $user_data="",
                                    "yes",
                                    $required = ""
                                );

                                $attrs[0]["user_transit_date"]="تاريخ النقل *";
                                $attrs[0]["user_transit_reason"]="سبب النقل";


                                $attrs[2]["user_transit_date"]="required";

                                $attrs[3]["user_transit_date"]="date";
                                $attrs[3]["user_transit_reason"]="textarea";

                                $attrs[4]["user_transit_date"]=date("Y-m-d");

                                $attrs[6]["user_transit_date"]="4";
                                $attrs[6]["user_transit_reason"]="8";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>


                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection
