@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url('admin/users_transits')}}" method="GET">
                <div class="col-md-4 col-md-offset-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="user_id",
                        $label_name="إختار المستخدم",
                        $text=convert_inside_obj_to_arr($all_users,"full_name"),
                        $values=convert_inside_obj_to_arr($all_users,"user_id"),
                        $selected_value=(isset($_GET['user_id'])?[$_GET['user_id']]:[""]),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" name="start_date" value="{{(isset($_GET["start_date"])?$_GET["start_date"]:"")}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" name="end_date" value="{{(isset($_GET["end_date"])?$_GET["end_date"]:"")}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
 تنقلات المستخدمين
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>التاريخ</td>
                    <td>المستخدم</td>
                    <td>من فرع</td>
                    <td>إلي فرع</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>التاريخ</td>
                    <td>المستخدم</td>
                    <td>من فرع</td>
                    <td>إلي فرع</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_transits as $key => $transit): ?>
                <tr id="row<?= $transit->user_transit_id ?>">
                    <td><?=$key+1?></td>
                    <td><?=$transit->user_transit_date ?></td>
                    <td><?=$transit->{'user_data_full_name'.$en} ?></td>
                    <td><?=$transit->{'from_branch_data_full_name'.$en} ?></td>
                    <td><?=$transit->{'to_branch_data_full_name'.$en} ?></td>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"factory/users_transits","add_action",$current_user)): ?>
            <div class="row">
                <a href="{{url('admin/users_transits/save')}}" class="btn btn-info col-md-3 col-md-offset-4">تسجيل تنقل جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
