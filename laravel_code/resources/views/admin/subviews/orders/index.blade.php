@extends('admin.main_layout')

@section('subview')


    <div class="col_3">

        <div class="col-md-1 widget widget1"></div>

        <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
                <i class="fa fa-users"></i>
                <div class="stats">
                    <h5>
                        {{$supplier_orders}}
                        <?php if($user_permissions['orders/suppliers'][0]->show_action): ?>
                            <small><a href="{{url('/admin/order/supplier_materials')}}" style="font-size: 12px !important;"> <i class="fa fa-wrench" style="font-size: 12px !important;width: 14px !important;"></i> التحكم </a></small>
                        <?php endif; ?>

                    </h5>
                    <div class="grow">
                        <p>الموردين</p>
                    </div>
                </div>

            </div>

        </div>
        <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
                <i class="fa fa-leaf"></i>
                <div class="stats">
                    <h5>
                        {{$branch_orders}}
                        <?php if(check_permission($user_permissions,"branch_orders","show_action",$current_user)): ?>
                            <small><a href="{{url('/admin/order/branch_orders')}}" style="font-size: 12px !important;"> <i class="fa fa-wrench" style="font-size: 12px !important;width: 14px !important;"></i> التحكم </a></small>
                        <?php endif; ?>
                    </h5>
                    <div class="grow grow1">
                        <p>الفروع</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
                <i class="fa fa-users"></i>
                <div class="stats">
                    <h5>
                        {{$branch_clients_orders}}
                        <?php if(1!=1&&check_permission($user_permissions,"branch_clients_orders","show_action",$current_user)): ?>
                            <small><a href="{{url('/admin/order/branch_clients')}}" style="font-size: 12px !important;"> <i class="fa fa-wrench" style="font-size: 12px !important;width: 14px !important;"></i> مشاهدة </a></small>
                        <?php endif; ?>
                    </h5>
                    <div class="grow grow3">
                        <p>عملاء الفروع</p>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection