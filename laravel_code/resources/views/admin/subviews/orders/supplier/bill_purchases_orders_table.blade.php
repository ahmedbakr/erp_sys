<?php

    $return_orders = collect($return_orders);
    $return_orders = $return_orders->groupBy("mat_name");
//    dump(array_sum(convert_inside_obj_to_arr($return_orders["oil"],"sup_mat_amount")));

?>

<table class="table table-striped">

    <th>#</th>
    <th>اسم الماده الخام</th>
    <th>كمية الماده الخام</th>
    <th>سعر الوحده للماده الخام</th>
    <th>الكمية المتاحه لعمل مرتجع</th>
    <th>الكمية في المخزن</th>
    <th>الكمية التي تريد ارجاعها</th>

    <?php foreach($orders as $key => $order): ?>
    <tr>
        <td>{{$key+1}}</td>
        <td>
            <span class="label label-default">
                <?= $order->mat_name ?>
            </span>
        </td>
        <td>
            <span class="label label-info"><?= $order->sup_mat_amount ?></span>
            <span class="label label-success"><?= $order->mat_type ?></span>
        </td>
        <td>
            <span class="label label-warning">
                <?= $order->sup_mat_price ?>
            </span>
        </td>
        <td>
            <?php if(isset($return_orders[$order->mat_name])): ?>
                <span class="label label-info"><?= ($order->sup_mat_amount - array_sum(convert_inside_obj_to_arr($return_orders[$order->mat_name],"sup_mat_amount"))) ?></span>
                <span class="label label-success"><?= $order->mat_type ?></span>

                <?php else: ?>
                <span class="label label-info"><?= ($order->sup_mat_amount) ?></span>
                <span class="label label-success"><?= $order->mat_type ?></span>
            <?php endif; ?>
        </td>
        <td>
            <span class="label label-info"><?= $product_materials[$order->mat_id][0]->mat_amount ?></span>
            <span class="label label-success"><?= $order->mat_type ?></span>
        </td>
        <td>
            <?php if(isset($return_orders[$order->mat_name])): ?>
                <input type='number' min='0' max='{{($order->sup_mat_amount - array_sum(convert_inside_obj_to_arr($return_orders[$order->mat_name],"sup_mat_amount")))}}' value='0' class='form-control' name='returned_quantity[]' >
                <?php else: ?>
                <input type='number' min='0' max='{{$order->sup_mat_amount}}' value='0' class='form-control' name='returned_quantity[]' >
            <?php endif; ?>

            <input type='hidden' value='{{$order->sup_mat_id}}' class='form-control' name='order_id[]' >
        </td>
    </tr>

    <?php endforeach; ?>

</table>