@extends('admin.main_layout')

@section('subview')


    <div class="container add_return_supplier_bil">


        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        <i class="fa fa-plus"></i> إضافة مرتجع علي الفاتورة</h3>
                </div>
            </div>


            <?php if($count_bills > 0): ?>

            <div class="panel panel-info">
                <div class="panel-heading">أدخل رقم الفاتورة واضغط علي زر إدخال</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="input-group">
                                <input type="text" placeholder="أدخل رقم الفاتورة واضغط علي زر إدخال" name="bill_number" value="{{$bill_id}}" class="form-control type_bill_number">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary get_bill_orders_submit" type="button">بحث عن الفاتورة</button>
                                    </span>
                            </div><!-- /input-group -->

                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-info bill_short_info hide_row">
                <div class="panel-heading">بيانات الفاتورة</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-info content_body" style="text-align: center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/order/supplier_materials/make_return_bill")?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>



                <div class="bill_returns_containers hide_row">

                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">العناصر التي تم ارجاعها من الفاتورة</div>
                        <div class="panel-body collapse_body collapse in">

                        </div>
                    </div>

                </div>

                <br>
                <div class="bill_orders_containers hide_row">

                    <div class="panel panel-primary bill_order_item">
                        <div class="panel-heading show_collapse_hand collapse_action">العناصر التي تم شرائها فالفاتورة</div>
                        <div class="panel-body collapse_body collapse in">

                        </div>
                    </div>

                    {{csrf_field()}}
                    <div class="form_submit_btns">
                        <input id="submit" type="submit" value="حفظ" class="col-md-2 col-md-offset-10 btn btn-primary btn-lg" >
                    </div>

                </div>


            </form>

            <?php else: ?>
            {!! get_warning($url = url('/admin/order/supplier_materials/save_purchase_bill_orders') , $text = "أضف فاتورة مشتريات اولا") !!}

            <?php endif; ?>

        </div>

    </div>


@endsection