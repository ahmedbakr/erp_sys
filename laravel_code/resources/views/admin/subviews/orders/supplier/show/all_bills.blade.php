@extends('admin.main_layout')

@section('subview')

    <input type="hidden" id="group_by_id" value="{{$group_by}}">

    <div class="panel panel-primary">
        <div class="panel-heading">
            بحث في كل الفواتير
        </div>
        <div class="panel-body">

            <div class="col-md-12">
                <div class="col-md-4">

                    <?php
                        echo
                        generate_select_tags(
                            $field_name="select_user_id",
                            $label_name="اختار المورد",
                            $text=array_merge(["All"],convert_inside_obj_to_arr($all_suppliers,"full_name")),
                            $values=array_merge(["0"],convert_inside_obj_to_arr($all_suppliers,"user_id")),
                            $selected_value=[$selected_supplier_id],
                            $class="form-control",
                            $multiple="",
                            $required="",
                            $disabled = "",
                            $data = ""
                        );
                    ?>

                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">من</label>
                        <input type="date" id="filter_date_from" value="{{$bill_date_from}}" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">الي</label>
                        <input type="date" id="filter_date_to" value="{{$bill_date_to}}" class="form-control">
                    </div>
                </div>

            </div>

            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                    <button class="btn btn-info show_supplier_bills_filter_btn">نتائج البحث</button>
                </div>
            </div>

        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            طلبات الموردين
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <?php if(empty($group_by)): ?>

                <?php
                    $sum_of_all_bills_cost=0;
                    $sum_of_all_bills_paid=0;
                    $sum_of_all_bills_extra_cost=0;
                    $sum_of_all_bills_total_return=0;
                ?>
                <table class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%" >
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>رقم الفاتورة</td>
                            <td>اسم المورد</td>
                            <td>طلبات الفاتورة</td>
                            <td>تاريخ الفاتورة</td>
                            <td>تكلفة الفاتورة</td>
                            <td>المدفوع من الفاتورة</td>
                            <td>المتبقي من الفاتورة</td>
                            <td>التكلفة الاضافية علي الفاتورة</td>
                            <td>مرجعات الفاتورة</td>
                            <td>رابط التحويلات المالية</td>
                            <td>رابط المرتجع</td>
                        </tr>
                    </thead>


                    <tbody>
                        <?php foreach ($all_supplier_bills as $key => $bill): ?>
                            <?php
                                $sum_of_all_bills_cost=$sum_of_all_bills_cost+$bill->supplier_bill_total_amount;
                                $sum_of_all_bills_paid=$sum_of_all_bills_paid+$bill->supplier_bill_total_paid;
                                $sum_of_all_bills_extra_cost=$sum_of_all_bills_extra_cost+$bill->supplier_bill_extra_cost;
                                $sum_of_all_bills_total_return=$sum_of_all_bills_total_return+$bill->supplier_bill_total_return_money;
                            ?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td>
                                    <span class="label label-info">#<?= $bill->supplier_bill_id ?></span>
                                </td>
                                <td>
                                    <a href="{{url("/admin/suppliers/profile/$bill->supplier_id")}}">
                                        <span class="label label-default">{{$bill->full_name}}</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url('/admin/order/supplier_materials/bill/'.$bill->supplier_bill_id )}}" title="مشاهدة الطلبات">
                                        <label class="label label-default"><i class="fa fa-shopping-cart"></i> مشاهدة الطلبات</label>
                                    </a>
                                </td>

                                <td>
                                    <span class="label label-info">{{$bill->supplier_bill_date}}</span>
                                    <span class="label label-success">
                                        {{\Carbon\Carbon::createFromTimestamp(strtotime($bill->supplier_bill_date))->diffForHumans()}}
                                    </span>

                                </td>

                                <td>
                                    <label class="label label-danger">
                                        {{$bill->supplier_bill_total_amount}}
                                    </label>
                                </td>
                                <td>
                                    <label class="label label-success">
                                        {{$bill->supplier_bill_total_paid}}
                                    </label>
                                </td>
                                <td>
                                    <label class="label label-warning">
                                        {{$bill->supplier_bill_total_amount-$bill->supplier_bill_total_paid}}
                                    </label>
                                </td>
                                <td>
                                    <label class="label label-info">
                                        {{$bill->supplier_bill_extra_cost}}
                                    </label>
                                </td>
                                <td>
                                    <label class="label label-primary">
                                        {{$bill->supplier_bill_total_return_money}}
                                    </label>
                                </td>


                                <td>
                                    <a href="{{url("/admin/money_transfer/$bill->supplier_id/$bill->supplier_bill_id")}}">
                                        <label class="label label-default">مشاهدة التحويلات المالية</label>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url("/admin/order/supplier_materials/save_return_bill_orders/$bill->supplier_bill_id")}}">
                                        <label class="label label-default">اضف مرتجع للفاتورة</label>
                                    </a>
                                </td>



                            </tr>
                        <?php endforeach ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><label class="label label-danger">{{$sum_of_all_bills_cost}}</label></td>
                            <td><label class="label label-success">{{$sum_of_all_bills_paid}}</label></td>
                            <td><label class="label label-warning">{{$sum_of_all_bills_cost-$sum_of_all_bills_paid}}</label></td>
                            <td><label class="label label-info">{{$sum_of_all_bills_extra_cost}}</label></td>
                            <td><label class="label label-primary">{{$sum_of_all_bills_total_return}}</label></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>

                </table>

            <?php elseif($group_by=="user"): ?>

                <?php
                    $all_supplier_bills_grouped=collect($all_supplier_bills)->groupBy("supplier_id")->all();
                ?>

                <div class="panel-group" id="accordion">
                    <?php foreach($all_supplier_bills_grouped as $key=>$all_supplier_bills): ?>

                    <?php
                        if(count($all_supplier_bills)==0){
                            continue;
                        }
                    ?>

                    <div class="panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                            <h4 class="panel-title" >
                                <a>{{$all_supplier_bills[0]->full_name}} طلبات المورد</a>
                            </h4>
                        </div>
                        <div id="collapse{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">

                                <?php
                                    $sum_of_all_bills_cost=0;
                                    $sum_of_all_bills_paid=0;
                                    $sum_of_all_bills_extra_cost=0;
                                ?>
                                <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>رقم الفاتورة</td>
                                        <td>اسم المورد</td>
                                        <td>طلبات الفاتورة</td>
                                        <td>تاريخ الفاتورة</td>
                                        <td>تكلفة الفاتورة</td>
                                        <td>المدفوع من الفاتورة</td>
                                        <td>المتبقي من الفاتورة</td>
                                        <td>التكلفة الاضافية علي الفاتورة</td>
                                        <td>رابط التحويلات المالية</td>
                                        <td>رابط المرتجع</td>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php foreach ($all_supplier_bills as $key => $bill): ?>
                                    <?php
                                    $sum_of_all_bills_cost=$sum_of_all_bills_cost+$bill->supplier_bill_total_amount;
                                    $sum_of_all_bills_paid=$sum_of_all_bills_paid+$bill->supplier_bill_total_paid;
                                    $sum_of_all_bills_extra_cost=$sum_of_all_bills_extra_cost+$bill->supplier_bill_extra_cost;
                                    ?>
                                    <tr>
                                        <td><?=$key+1?></td>
                                        <td>
                                            <span class="label label-info">#<?= $bill->supplier_bill_id ?></span>
                                        </td>
                                        <td>
                                            <a href="{{url("/admin/suppliers/profile/$bill->supplier_id")}}">
                                                <span class="label label-default">{{$bill->full_name}}</span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{url('/admin/order/supplier_materials/bill/'.$bill->supplier_bill_id )}}" title="مشاهدة الطلبات">
                                                <i class="fa fa-shopping-cart"></i> مشاهدة الطلبات
                                            </a>
                                        </td>

                                        <td>
                                            <span class="label label-info">{{$bill->supplier_bill_date}}</span>
                                            <span class="label label-success">
                                        {{\Carbon\Carbon::createFromTimestamp(strtotime($bill->supplier_bill_date))->diffForHumans()}}
                                    </span>

                                        </td>

                                        <td>{{$bill->supplier_bill_total_amount}}</td>
                                        <td>{{$bill->supplier_bill_total_paid}}</td>
                                        <td>{{$bill->supplier_bill_total_amount-$bill->supplier_bill_total_paid}}</td>
                                        <td>{{$bill->supplier_bill_extra_cost}}</td>
                                        <td>
                                            <a href="{{url("/admin/money_transfer/$bill->supplier_id/$bill->supplier_bill_id")}}">
                                                مشاهدة التحويلات المالية
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{url("/admin/order/supplier_materials/save_return_bill_orders/$bill->supplier_bill_id")}}">
                                                اضف مرتجع للفاتورة
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><label class="label label-danger">{{$sum_of_all_bills_cost}}</label></td>
                                        <td><label class="label label-success">{{$sum_of_all_bills_paid}}</label></td>
                                        <td><label class="label label-warning">{{$sum_of_all_bills_cost-$sum_of_all_bills_paid}}</label></td>
                                        <td><label class="label label-info">{{$sum_of_all_bills_extra_cost}}</label></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tfoot>

                                </table>


                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>

                </div>


            <?php endif; ?>



        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-body">
            <div class="col-md-12">
                <?php if(check_permission($user_permissions,"orders/suppliers","add_purchase_bill")): ?>
                    <div class="col-md-3 col-md-offset-2">
                        <a class="btn btn-primary btn-block" href="{{url('/admin/order/supplier_materials/save_purchase_bill_orders')}}"> إضافة فاتورة مشتريات </a>
                    </div>
                <?php endif; ?>

                <?php if(check_permission($user_permissions,"orders/suppliers","add_return_bill")): ?>
                    <div class="col-md-3 col-md-offset-2">
                        <a class="btn btn-primary btn-block" href="{{url('/admin/order/supplier_materials/save_return_bill_orders')}}"> إضافة مرتجع علي الفاتورة </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>





@endsection