@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading">
            {{$bill_data->supplier_bill_id}}            فاتورة مورد رقم
        </div>
        <div class="panel-body">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>الماده الخام</td>
                    <td>الكمية</td>
                    <td>السعر</td>
                    <td>نوع العملية</td>
                    <td>التاريخ</td>
                </tr>
                </thead>


                <tbody id="sortable">
                <?php foreach ($all_orders as $key => $order): ?>
                <tr>
                    <td><?=$key+1?></td>

                    <td>
                        <span class="label label-default"><?= $order->mat_name ?></span>
                    </td>
                    <td>
                        <span class="label label-info"><?= $order->sup_mat_amount ?></span>
                        <span class="label label-success"><?= $order->mat_type ?></span>
                    </td>
                    <td>
                        <span class="label label-warning"><?= $order->sup_mat_price ?></span>
                    </td>
                    <td>{{($order->sup_mat_return)?"مرتجع":"شراء"}}</td>
                    <td>
                        <span class="label label-info">{{$order->sup_mat_created}}</span>
                        <span class="label label-success">
                            {{\Carbon\Carbon::createFromTimestamp(strtotime($order->sup_mat_created))->diffForHumans()}}
                        </span>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>


@endsection