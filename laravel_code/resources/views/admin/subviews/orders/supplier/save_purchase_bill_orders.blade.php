@extends('admin.main_layout')

@section('subview')


<div class="container">

    <div class="col-md-12">

        <div class="row">
            <div class="alert alert-info" style="text-align: center">
                <h3>
                    <i class="fa fa-plus"></i> Add New Purchase Bill</h3>
            </div>
        </div>

        <?php if(count($suppliers)): ?>

            <?php if(count($materials)): ?>

                <form id="save_form" action="<?=url("/admin/order/supplier_materials/save_purchase_bill_orders")?>" method="POST" enctype="multipart/form-data">

                <?php

                    if (count($errors->all()) > 0)
                    {
                        $dump = "<div class='alert alert-danger'>";
                        foreach ($errors->all() as $key => $error)
                        {
                            $dump .= $error." <br>";
                        }
                        $dump .= "</div>";

                        echo $dump;
                    }


                    if (isset($success)&&!empty($success)) {
                        echo $success;
                    }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading">بيانات المورد</div>
                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-1">
                                <?php
                                echo
                                generate_select_tags(
                                    $field_name="sup_mat_user_id",
                                    $label_name="إختار المورد",
                                    $text=convert_inside_obj_to_arr($suppliers,"full_name"),
                                    $values=convert_inside_obj_to_arr($suppliers,"user_id"),
                                    $selected_value=array(),
                                    $class="form-control select_2_class"
                                );
                                ?>
                            </div>
                            <div class="col-md-4  col-md-offset-1">
                                <label for="">تاريخ الفاتورة</label>
                                <input type="date" required name="supplier_bill_date" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-3">
                                <label for="">السعر النهائي</label>
                                <input type="number" disabled value="0" data-old_value="0" class="form-control total_bill_amount">
                            </div>
                            <div class="col-md-3">
                                <label for=""> المدفوع كاش</label>
                                <input type="number" min="0" step="0.1" name="supplier_bill_total_paid_in_cash" value="0" class="form-control supplier_bill_total_paid_in_cash">
                            </div>
                            <div class="col-md-3">
                                <?php if(check_permission($user_permissions,"factory/atm_permission","show_action")): ?>
                                    <label for="">المدفوع شبكة</label>
                                    <input type="number" min="0" step="0.1" name="supplier_bill_total_paid_in_atm" value="0" class="form-control supplier_bill_total_paid_in_atm">
                                <?php endif; ?>
                            </div>

                            <div class="col-md-2">
                                <label for="">المتبقي</label>
                                <p class="amount_remain"></p>
                            </div>


                            <div class="col-md-1">
                                <label for="">مصاريف اخري</label>
                                <input type="checkbox" class="form-control extra_costs">
                            </div>
                        </div>

                        <div class="row additional_extra_costs_fields hide_row">
                            <div class="col-md-3">
                                <label for="">اسم المصروف</label>
                                <input type="text" name="expense_name" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="">السعر</label>
                                <input type="number" min="0" step="0.1" name="expense_amount" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="">ملخص عن المصروف</label>
                                <input type="text" name="expense_desc" class="form-control">
                            </div>
                        </div>

                    </div>

                </div>

                <div class="bill_orders_containers">

                    <div class="panel panel-primary bill_order_item">
                        <a href="" class="btn btn-danger pull-right delete_order_item"> X </a>
                        <div class="panel-heading show_collapse_hand collapse_action">
                            عنصر الفاتورة

                        </div>
                        <div class="panel-body collapse_body collapse">

                            <?php

                                echo generate_select_tags(
                                        $field_name="mat_id[]",
                                        $label_name="إختار الماده الخام",
                                        $text=convert_inside_obj_to_arr($materials,"mat_name"),
                                        $values=convert_inside_obj_to_arr($materials,"mat_id"),
                                        $selected_value=array(),
                                        $class="form-control");

                                $normal_tags=array("sup_mat_amount[]","sup_mat_price[]","sup_mat_barcode[]","sup_mat_desc[]");
                                $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data="",
                                        "yes",
                                        $required = "required"
                                );

                                $attrs[0]["sup_mat_amount[]"]="الكمية";
                                $attrs[0]["sup_mat_price[]"]="سعر الوحده";
                                $attrs[0]["sup_mat_barcode[]"]="الباركود";
                                $attrs[0]["sup_mat_desc[]"]="ملخص عن الماده الخام";

                                $attrs[2]["sup_mat_barcode[]"]="";
                                $attrs[2]["sup_mat_desc[]"]="";

                                $attrs[3]["sup_mat_amount[]"]="number";
                                $attrs[3]["sup_mat_price[]"]="number";
                                $attrs[3]["sup_mat_barcode[]"]="text";
                                $attrs[3]["sup_mat_desc[]"]="textarea";

                                $attrs[5]["sup_mat_amount[]"] .=" add_bill_order_quantity";
                                $attrs[5]["sup_mat_price[]"] .=" add_bill_order_price";

                                echo
                                generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                );

                            ?>

                        </div>

                    </div>
                </div>

                    {{csrf_field()}}
                    <a href="#" class="col-md-3 col-md-offset-1 btn btn-info count_bill_orders"><i class="fa fa-refresh"></i> احسب الفاتورة</a>
                    <a href="#" class="col-md-3 col-md-offset-1 btn btn-info clone_order_item"><i class="fa fa-plus"></i> عنصر جديد</a>
                    <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg purchase_bill_form_submit">
                </form>

                <?php else: ?>
                {!! get_warning($url = url('/admin/materials/save') , $text = "أضف ماده خام اولا") !!}

            <?php endif; ?>

        <?php else: ?>
        {!! get_warning($url = url('/admin/users/save/supplier') , $text = "أضف موردين اولا") !!}

        <?php endif; ?>
    </div>

</div>


@endsection