
<?php
    $orders = collect($orders);
    $orders = $orders->groupBy("mat_name");
    $counter = 0;
?>

<table class="table table-striped">

    <th>#</th>
    <th>اسم الماده الخام</th>
    <th>كمية الماده الخام</th>
    <th>سعر الوحده</th>
    <th>التاريخ</th>

    <?php foreach($orders as $key => $order): ?>
        <tr class="info">
            <td>{{$counter+1}}</td>
            <td>
                <span class="label label-default"><?=$key ?></span>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach($order as $key2 => $value): ?>

            <tr>
                <td></td>
                <td></td>
                <td>
                    <span class="label label-info"><?= $value->sup_mat_amount ?></span>
                    <span class="label label-success"><?= $value->mat_type ?></span>
                </td>
                <td>
                    <span class="label label-warning">
                        <?= $value->sup_mat_price ?>
                    </span>
                </td>
                <td>
                    <span class="label label-info">{{$value->sup_mat_created}}</span>
                    <span class="label label-success">
                        {{\Carbon\Carbon::createFromTimestamp(strtotime($value->sup_mat_created))->diffForHumans()}}
                    </span>
                </td>
            </tr>

        <?php endforeach; ?>

        <?php $counter++; ?>

    <?php endforeach; ?>

</table>