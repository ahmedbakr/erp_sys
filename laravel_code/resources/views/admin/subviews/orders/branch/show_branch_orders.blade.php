@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">
         عمليات
        </div>
        <div class="panel-body" style="text-align: center;">

            <?php if(check_permission($user_permissions,"branch_orders","add_action",$current_user)): ?>
                <a class="btn btn-primary" href="{{url('/admin/branch_bills/add_bill')}}"> إضافة فاتورة طلبات للفرع </a>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"branch_orders","add_return_bill",$current_user)): ?>
                <a class="btn btn-primary" href="{{url('/admin/branch_bills/add_return_bill')}}"> إضافة فاتورة مرتجع لطلبات للفرع </a>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"branch_orders","show_bill_orders",$current_user)): ?>
                <a class="btn btn-primary" href="{{url('/admin/branch_bills/show_all')}}"> استلام طلبات الفروع </a>
            <?php endif; ?>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url('admin/order/branch_orders')}}" method="GET">

                <div class="col-md-4 col-md-offset-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=array_merge(["الكل"],convert_inside_obj_to_arr($all_branches,"full_name")),
                        $values=array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id")),
                        $selected_value=(isset($_GET['branch_id'])?[$_GET['branch_id']]:[""]),
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" name="start_date" value="{{(isset($_GET["start_date"])?$_GET["start_date"]:"")}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" name="end_date" value="{{(isset($_GET["end_date"])?$_GET["end_date"]:"")}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>



    <div class="panel panel-info">
        <div class="panel-heading"> <i class="fa fa-shopping-cart"></i> الفواتير</div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>رقم الفاتورة</td>
                    <td>لوجو الفرع</td>
                    <td>اسم الفرع</td>
                    <td>حالة الفاتورة</td>
                    <td>طلبات الفاتورة</td>
                    <td>مرتجع الفاتورة</td>
                    <td>تاريخ الفاتورة</td>
                    <?php if(check_permission($user_permissions,"branch_orders","edit_action",$current_user)): ?>
                        <td>تعديل</td>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"branch_orders","delete_action",$current_user)): ?>
                        <td>مسح</td>
                    <?php endif; ?>

                </tr>
                </thead>


                <tbody id="sortable">
                <?php foreach ($branch_bills as $key => $bill): ?>
                <tr id="row<?= $bill->branch_bill_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                <span class="label label-info">
                    #<?= $bill->branch_bill_id ?>
                </span>
                    </td>
                    <td>
                        <img width="100" src="{!! get_image_or_default($bill->path) !!}">
                    </td>
                    <td>
                <span class="label label-default">
                    {{$bill->full_name}}
                </span>
                    </td>
                    <td>
                <span class="label label-info">
                    <?= $bill->current_status ?>
                </span>
                    </td>
                    <td>
                <span class="label label-info">
                    <a href="{{url('/admin/order/show_branch_bill_orders/'.$bill->branch_bill_id )}}" title="Show Bill Orders">
                        <i class="fa fa-shopping-cart"></i> مشاهدة الطلبات
                    </a>
                </span>
                    </td>
                    <td>
                <span class="label label-info">
                    <a href="{{url('/admin/order/show_branch_bill_returns/'.$bill->branch_bill_id )}}" title="Show Bill Orders">
                        <i class="fa fa-shopping-cart"></i> مشاهدة المرتجع
                    </a>
                </span>
                    </td>
                    <td>
                        <span class="label label-info">{{$bill->branch_bill_date}}</span>
                        <span class="label label-success">
                    {{\Carbon\Carbon::createFromTimestamp(strtotime($bill->branch_bill_date))->diffForHumans()}}
                </span>

                    </td>


                    <?php if(check_permission($user_permissions,"branch_orders","edit_action",$current_user)): ?>
                        <td>
                            <?php if($bill->status_id < 4): ?>

                                <a href="{{url('/admin/branch_bills/add_bill/'.$bill->branch_bill_id )}}" title="تعديل الفاتورة">
                                    <i class="fa fa-shopping-cart"></i> تعديل الفاتورة
                                </a>
                                <?php else: ?>
                                <span class="label label-danger">غير مسموح</span>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"branch_orders","delete_action",$current_user)): ?>
                        <td>
                            <?php if($bill->status_id < 4): ?>
                                <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/orders/remove_branch_bill") ?>" data-tablename="App\models\bills\branch_bills_m"  data-itemid="<?= $bill->branch_bill_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                                <?php else: ?>
                            <span class="label label-danger">غير مسموح</span>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>


        </div>
    </div>



@endsection