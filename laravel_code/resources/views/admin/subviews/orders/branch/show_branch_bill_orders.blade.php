@extends('admin.main_layout')

@section('subview')

    <style>
        /*Start print print_branch_bill_requests*/

        .company_logo{
            position: absolute;
            float: right;
        }

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }


        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
        }

        @media print {
            body * {
                visibility: hidden;
            }

            .print_branch_bill_requests , .print_branch_bill_requests *{
                visibility: visible;
            }

            .print_branch_bill_requests {
            position: absolute !important;
            width: 100%;
            left: 0px;
            top: -250px;
            }

            .print_page , .print_page *{
                visibility: hidden;
            }

            h3{
                font-weight:bold;
                font-size: 26px;
            }

            table{
                font-weight:bold;
                font-size: 26px;
            }


        }

        /*End print print_branch_bill_requests*/
    </style>
    <div class="print_branch_bill_requests">

        <div class="row">
            <div class="alert alert-info" style="text-align: center">

                <div class="company_logo">
                    <img src="{{get_image_or_default($company->path)}}" width="100">
                    <br>
                    <b>{{$company->company_name}}</b>
                </div>

                <h3>
                    <i class="fa fa-shopping-cart"></i>
                    مشاهدة طلبات الفاتورة رقم.{{$branch_bill_data->branch_bill_id}} <br>
                    <small>
                        <i class="fa fa-wrench"></i>
                        حالة الفاتورة <span class="label label-info">{{$branch_bill_data->current_status}} </span>
                        &nbsp;&nbsp;أنشأت من
                        <span class="label label-success" title="{{$branch_bill_data->updated_at}}">
                                {{\Carbon\Carbon::createFromTimestamp(strtotime($branch_bill_data->updated_at))->diffForHumans()}}
                        </span>
                    </small>
                    <br><br>
                    <i class="fa fa-user"></i>
                    للفرع >> <img width="30" src="{{get_image_or_default($branch_bill_data->path)}}" alt=""> {{$branch_bill_data->full_name}}<br>
                    <small>
                        <i class="fa fa-calendar"></i>
                        بتاريخ
                        <span class="label label-info">{{$branch_bill_data->branch_bill_date}}</span>
                        من
                        <span class="label label-success">
                            {{\Carbon\Carbon::createFromTimestamp(strtotime($branch_bill_data->branch_bill_date))->diffForHumans()}}
                        </span>
                    </small>


                    <?php if(check_permission($user_permissions,"branch_orders","manage_bill_orders",$current_user)): ?>

                        <?php if(in_array($branch_bill_data->status_id,[1,2])): ?>
                            <br><br> تغيير حالة الفاتورة :
                            <?php
                                    echo
                                    generate_multi_accepters(
                                            $accepturl=url("/admin/orders/change_bill_status"),
                                            $item_obj=$branch_bill_data,
                                            $item_primary_col="branch_bill_id",
                                            $accept_or_refuse_col="status_id",
                                            $model='App\models\bills\branch_bills_m',
                                            $accepters_data=array_combine(convert_inside_obj_to_arr($all_status,"status_id"),convert_inside_obj_to_arr($all_status,"status_label"))
                                    );
                            ?>

                        <?php endif; ?>

                    <?php endif; ?>

                </h3>
            </div>
            <button class="btn btn-info btn-lg print_page"> طباعة <i class="fa fa-print"></i></button>
        </div>

        <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>#</td>
                <td>اسم المنتج</td>
                <td>كمية المنتج</td>
            </tr>
            </thead>


            <tbody id="sortable">
            <?php foreach ($branch_bill_orders as $key => $order): ?>
            <tr>
                <td><?=$key+1?></td>
                <td>
                    <span class="label label-default">
                        {{$order->pro_name}}
                    </span>
                </td>
                <td>
                    <span class="label label-success">
                        <?= $order->b_o_quantity ?>
                    </span>
                </td>

            </tr>
            <?php endforeach ?>
            </tbody>

        </table>

    </div>

@endsection