@extends('admin.main_layout')


@section('subview')

    <script src="{{url("/public_html/jscode/admin/branch_bills.js")}}"></script>


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    ?>

    <div class="panel panel-info add_permission_to_stock_parent_div">
        <div class="panel-heading">
            <?=" تحويل منتجات للفروع "?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/branch_transfers/save")?>" method="POST" enctype="multipart/form-data">

                    <div class="panel panel-primary">
                        <div class="panel-heading">بيانات اساسية</div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <?php
                                echo generate_select_tags(
                                    $field_name="from_branch",
                                    $label_name="تحويل من فرع *",
                                    $text=convert_inside_obj_to_arr($from_branches,"full_name"),
                                    $values=convert_inside_obj_to_arr($from_branches,"user_id"),
                                    $selected_value=[""],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>

                            <div class="col-md-6">
                                <?php
                                echo generate_select_tags(
                                    $field_name="to_branch",
                                    $label_name="تحويل إلي فرع *",
                                    $text=convert_inside_obj_to_arr($to_branches,"full_name"),
                                    $values=convert_inside_obj_to_arr($to_branches,"user_id"),
                                    $selected_value=[""],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary pro_advanced_search">
                                <div class="panel-heading" data-toggle="collapse" href=".choose_product" style="cursor:pointer;">
                                    اضف منتج عن طريق الاختيار
                                </div>
                                <div class="panel-body choose_product collapse">

                                    <div class="col-md-9 load_cats_div">
                                        <div class="row">
                                            <button class="btn btn-primary show_parent_0_cats"> اظهر التصنيفات الرئيسية</button>
                                        </div>


                                        {!! $cats_view !!}
                                    </div>

                                    <div class="col-md-3" style="border-right: 1px solid #CCC;margin-bottom: 10px;">
                                        <div class="form-group">
                                            <label for="">ابحث بواسطة الباركود او الاسم</label>
                                            <input type="text"  class="form-control search_for_product_by_name_or_barcode">
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="col-md-12 load_products_div">

                                        {!! $products_view !!}
                                    </div>

                                    <div class="col-md-12 search_res">

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">المنتجات</div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped orders_table">
                                        <tr>
                                            <th>المنتج</th>
                                            <th>الكمية</th>
                                            <th>حذف</th>
                                        </tr>
                                    </table>

                                    <div class="all_pros_count"></div>
                                </div>
                            </div>
                        </div>

                    </div>




                    <?php if(false): ?>
                    <div class="row">

                        <div class="bill_orders_containers">

                            <div class="panel panel-primary bill_order_item" data-itemid="1">
                                <div class="panel-heading show_collapse_hand collapse_action">
                                    المنتج
                                </div>
                                <div class="panel-body collapse_body collapse">

                                    <button
                                            type="button"
                                            class="btn btn-primary show_tree_modal center-block"
                                            data-select_what="item"
                                            data-select_type="single"
                                            data-div_to_show_selection="[data-itemid='1'] .select_product"
                                            data-parent_div_to_show_selection=""
                                            data-input_field_name="pro_id[]"
                                    >
                                        اظهر الشجرة لتختار الاصناف
                                    </button>
                                    <div class="select_product" style="text-align: center;">
                                    </div>

                                    <?php


                                    $normal_tags=array("pro_quantity[]");
                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data="",
                                        "yes",
                                        $required = "required"
                                    );

                                    $attrs[0]["pro_quantity[]"]="الكمية";
                                    $attrs[2]["pro_quantity[]"]="required";
                                    $attrs[3]["pro_quantity[]"]="number";
                                    $attrs[5]["pro_quantity[]"].=" check_quantity";


                                    echo
                                    generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                    );

                                    ?>


                                    <button class="btn btn-danger delete_order_item">مسح</button>

                                </div>

                            </div>
                        </div>

                    </div>
                    <a href="#" class="col-md-3 col-md-offset-2 btn btn-info clone_order_item"><i class="fa fa-plus"></i> New Item</a>
                    <?php endif; ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

                </form>
            </div>
        </div>
    </div>


@endsection



