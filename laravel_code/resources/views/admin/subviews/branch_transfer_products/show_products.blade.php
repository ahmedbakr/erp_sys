@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            منتجات طلب التحويل
            من فرع
            {{$transfer_data->from_branch_full_name}}
            الي فرع
            {{$transfer_data->to_branch_full_name}}
            بتاريخ
            {{$transfer_data->created_at}}
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم المنتج</td>
                    <td>الكمية</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم المنتج</td>
                    <td>الكمية</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($products as $key => $product): ?>

                <tr id="row<?= $product->pro_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$product->pro_name}}</td>
                    <td>{{$product->transfer_pro_quantity}}</td>

                </tr>

                <?php endforeach ?>
                </tbody>

            </table>


        </div>
    </div>




@endsection
