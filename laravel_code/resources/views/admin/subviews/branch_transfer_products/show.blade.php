@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url('admin/branch_transfers')}}" method="GET">
                <div class="col-md-4 col-md-offset-2">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="from_branch",
                        $label_name="إختار الفرع المحول منه",
                        $text=convert_inside_obj_to_arr($branches,"full_name"),
                        $values=convert_inside_obj_to_arr($branches,"user_id"),
                        $selected_value=(isset($_GET['from_branch'])?[$_GET['from_branch']]:[""]),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>
                <div class="col-md-4 col-md-offset-2">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="to_branch",
                        $label_name="إختار الفرع المحول الية",
                        $text=convert_inside_obj_to_arr($to_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($to_branches,"user_id"),
                        $selected_value=(isset($_GET['to_branch'])?[$_GET['to_branch']]:[""]),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" name="from_date" value="{{(isset($_GET["from_date"])?$_GET["from_date"]:"")}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" name="to_date" value="{{(isset($_GET["to_date"])?$_GET["to_date"]:"")}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            تحويلات الفروع
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>من فرع</td>
                    <td>إلي فرع</td>
                    <td>حاله التحويل</td>
                    <td>مشاهدة المنتجات</td>
                    <td>تاريخ الطلب</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>من فرع</td>
                    <td>إلي فرع</td>
                    <td>حاله التحويل</td>
                    <td>مشاهدة المنتجات</td>
                    <td>تاريخ الطلب</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_transfers as $key => $transfer): ?>

                <tr id="row<?= $transfer->b_t_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$transfer->from_branch_full_name}}</td>
                    <td>{{$transfer->to_branch_full_name}}</td>
                    <td>

                        <?php if($current_user->user_type == "admin"): ?>

                            <?php if($transfer->b_t_status == 0): ?>
                                <?php if(check_permission($user_permissions,"branch_transfer_products","receive_transfers",$current_user)): ?>
                                    <?php
                                        echo
                                        generate_multi_accepters(
                                            $accepturl=url( "/admin/branch_transfers/receive_branch_transfer"),
                                            $item_obj=$transfer,
                                            $item_primary_col="b_t_id",
                                            $accept_or_refuse_col="b_t_status",
                                            $model='App\models\branch_transfers\branch_transfers_m',
                                            $accepters_data=["1"=>"استلام التحويل"]
                                        );
                                    ?>
                                    <?php else: ?>
                                    <span class="label label-danger">ليس لديك الصلاحية لإستلام التحويل</span>
                                <?php endif; ?>
                            <?php else: ?>
                            <span class="label label-success">تم الإستلام</span>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if($current_user->user_type == "branch_admin"): ?>
                            <?php if($transfer->b_t_status == 0): ?>

                                <?php if($transfer->to_branch != $branch_data->user_id): ?>
                                    <span class="label label-danger">لم يستلم حتي الان</span>
                                <?php else: ?>
                                <?php if(check_permission($user_permissions,"branch_transfer_products","receive_transfers",$current_user)): ?>
                                    <?php
                                        echo
                                        generate_multi_accepters(
                                            $accepturl=url( "/admin/branch_transfers/receive_branch_transfer"),
                                            $item_obj=$transfer,
                                            $item_primary_col="b_t_id",
                                            $accept_or_refuse_col="b_t_status",
                                            $model='App\models\branch_transfers\branch_transfers_m',
                                            $accepters_data=["1"=>"استلام التحويل"]
                                        );
                                        ?>
                                    <?php else: ?>
                                    <span class="label label-danger">ليس لديك الصلاحية لإستلام التحويل</span>
                                <?php endif; ?>
                                <?php endif; ?>

                                <?php else: ?>
                                <span class="label label-success">تم الإستلام</span>
                            <?php endif; ?>
                        <?php endif; ?>

                    </td>

                    <td>
                        <a href="<?= url("admin/branch_transfers/show_products/$transfer->b_t_id") ?>"><span class="label label-info"> مشاهدة <i class="fa fa-send"></i></span></a>
                    </td>

                    <td>
                        {{$transfer->created_at}}
                    </td>

                    <td>
                        <?php if($transfer->b_t_status == 0): ?>

                        <?php if($current_user->user_type == "admin"): ?>
                            <?php if(check_permission($user_permissions,"branch_transfer_products","delete_action",$current_user)): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/branch_transfers/remove_branch_transfers") ?>" data-tablename="App\models\branch_transfers\branch_transfers_m"  data-itemid="<?= $transfer->b_t_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if($current_user->user_type == "branch_admin" && $transfer->from_branch == $branch_data->user_id): ?>
                            <?php if(check_permission($user_permissions,"branch_transfer_products","delete_action",$current_user)): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/branch_transfers/remove_branch_transfers") ?>" data-tablename="App\models\branch_transfers\branch_transfers_m"  data-itemid="<?= $transfer->b_t_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                            <?php endif; ?>
                        <?php endif; ?>


                        <?php else: ?>
                        <span class="label label-danger">لا يمكن مسح الطلب بعد الإستلام</span>

                        <?php endif; ?>


                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"branch_transfer_products","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/branch_transfers/save")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>

@endsection
