@extends('admin.main_layout')

@section('subview')

    <link rel="stylesheet" href="{{url('/public_html/admin/css/timeline.css')}}">


    <?php if(is_array($all_users) && count($all_users)): ?>



        <header class="page-header">
            <div class="alert alert-success" style="text-align: center">
                <h1 style="color: #00ACED">
                    <i class="fa fa-wrench"></i>
                    حركات المستخدم داخل النظام
                </h1>
                <br>
                <form action="{{url('/admin/tracking')}}" method="POST">
                    <div class="row">

                        <div class="col-md-4">
                            <?php
                                echo generate_select_tags(
                                        $field_name="user_id",
                                        $label_name="إختار اسم المستخدم",
                                        $text=convert_inside_obj_to_arr($all_users,"full_name"),
                                        $values=convert_inside_obj_to_arr($all_users,"user_id"),
                                        $selected_value=array(intval($submitted_user)),
                                        $class="form-control select_2_class",
                                        $multiple = "",
                                        $required = "required",
                                        $disabled = "");
                            ?>
                        </div>

                        <div class="col-md-4">
                            <label for="">إختار التاريخ </label>
                            <input type="date" name="date" class="form-control" value="{{(!empty($submitted_date))?$submitted_date:date('Y-m-d')}}">
                        </div>

                        <div class="col-md-4">
                            <br>
                            {{csrf_field()}}
                            <input type="submit" value="عرض" class="btn btn-primary btn-lg">
                        </div>

                    </div>
                </form>
            </div>
        </header>

        <?php

        if (count($errors->all()) > 0) {
            $dump = "<div class='alert alert-danger'>";
            foreach ($errors->all() as $key => $error) {
                $dump .= $error . " <br>";
            }
            $dump .= "</div>";

            echo $dump;
        }


        if (isset($success) && !empty($success)) {
            echo $success;
        }

        ?>

        <?php if(is_array($tracking_data) && count($tracking_data)): ?>

        <ul class="timeline">

            <?php foreach($tracking_data as $key => $value): ?>

                <li>
                    <div class="tldate">
                        At {{$key}} {{($key >= 12)?" PM ":" AM "}}
                    </div>
                </li>

                <?php foreach($value as $index => $item): ?>

                    <li class="{{($index%2==0)?"":"timeline-inverted"}}">
                        <div class="tl-circ"></div>
                        <div class="timeline-panel">
                            <div class="tl-heading">
                                <h4>{{$item->full_name}}</h4>
                                <p>
                                    <small class="text-muted"><i class="glyphicon glyphicon-time"></i>
                                     {{$item->created_at}} &nbsp;&nbsp;
                                     {{\Carbon\Carbon::createFromTimestamp(strtotime($item->created_at))->diffForHumans()}}
                                    </small>
                                </p>
                            </div>
                            <div class="tl-body">
                                <p>{!! $item->action_desc !!}</p>
                            </div>
                        </div>
                    </li>

                <?php endforeach; ?>

            <?php endforeach; ?>

        </ul>

        <?php endif; ?>


    <?php else: ?>
    {!! get_warning(url('/admin/dashboard'),"You Don't Have any users on your system") !!}
    <?php endif; ?>

@endsection