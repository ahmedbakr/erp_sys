@extends('admin.main_layout')

@section('subview')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }

                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>


                <?php

                $header_text="إضافة اصل جديد";
                $asset_id="";
                if ($asset_data!="") {
                    $header_text="تعديل الاصل  ".$asset_data->asset_name;
                    $asset_id=$asset_data->asset_id;
                }

                ?>



                <div class="panel panel-info">
                    <div class="panel-heading"><h2><?=$header_text?></h2></div>
                    <div class="panel-body">
                        <div class="row">
                            <form id="save_form" action="<?=url("admin/assets/save_asset/$asset_id")?>" method="POST" enctype="multipart/form-data">

                                <?php
                                    echo
                                    generate_select_tags(
                                        $field_name="user_id",
                                        $label_name="الاصل خاص ب:",
                                        $text=array_merge(["Factory"],convert_inside_obj_to_arr($all_branches,"username")),
                                        $values=array_merge(["2"],convert_inside_obj_to_arr($all_branches,"user_id")),
                                        $selected_value="",
                                        $class="form-control",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = $asset_data
                                    );
                                ?>


                                <hr>

                                <?php
                                $normal_tags=array(
                                    "asset_name","asset_desc","asset_cost","asset_original_cost",
                                    "asset_broken_percentage","asset_buy_date","last_calculated_date",
                                    "asset_sell_amount","asset_sell_date","asset_sell_checkbox",
                                    "period_in_years"
                                );

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $asset_data,
                                    "yes",
                                    ""
                                );


                                $attrs[0]["asset_name"]="اسم الاصل";
                                $attrs[0]["asset_desc"]="الوصف";
                                $attrs[0]["asset_cost"]="القيمة الحالية للاصل";
                                $attrs[0]["asset_original_cost"]="القيمة الاصليه للاصل";
                                $attrs[0]["asset_broken_percentage"]="نسبه الاهلاك";
                                $attrs[0]["asset_buy_date"]="تاريخ الشراء";
                                $attrs[0]["last_calculated_date"]="تاريخ اخر حساب لنسبه الاهلاك";
                                $attrs[0]["asset_sell_amount"]="تقدير قيمة الاصل للبيع (اذا تم ادخال قيمة لن يتم حساب نسبه اهلاك مرة اخري له)";
                                $attrs[0]["asset_sell_date"]="تاريخ البيع للاصل (اذا تم ادخال قيمة لن يتم حساب نسبه اهلاك مرة اخري له)";
                                $attrs[0]["asset_sell_checkbox"]="الغاء قيمة البيع او تاريخ البيع لو تم ادخالها في الاعلي بالخطأ";
                                $attrs[0]["period_in_years"]="الفترة علي هيئه عدد سنوات (بعد انتهاء هذة الفترة سيتم ارسال ايميل لتقييم الاصل وبيعه)";

                                $attrs[2]["asset_name"]="required";
                                $attrs[2]["asset_cost"]="required";
                                $attrs[2]["asset_original_cost"]="required";
                                $attrs[2]["asset_broken_percentage"]="required";
                                $attrs[2]["asset_buy_date"]="required";
                                $attrs[2]["asset_buy_date"]="required";
                                $attrs[2]["period_in_years"]="required";

                                $attrs[3]["asset_desc"]="textarea";
                                $attrs[3]["asset_cost"]="number";
                                $attrs[3]["asset_original_cost"]="number";
                                $attrs[3]["asset_broken_percentage"]="number";
                                $attrs[3]["asset_buy_date"]="date";
                                $attrs[3]["asset_sell_amount"]="number";
                                $attrs[3]["asset_sell_date"]="date";
                                $attrs[3]["asset_sell_checkbox"]="checkbox";
                                $attrs[3]["asset_buy_date"]="date";
                                $attrs[3]["last_calculated_date"]="date";
                                $attrs[3]["period_in_years"]="number";



                                echo
                                generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                                );
                                ?>

                                <hr>


                                {{csrf_field()}}
                                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">

                            </form>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>

@endsection
