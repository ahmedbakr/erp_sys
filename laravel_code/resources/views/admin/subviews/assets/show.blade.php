@extends('admin.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">
            كل الاصول

        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>المالك</td>
                    <td>اسم الاصل</td>
                    <td>الوصف</td>
                    <td>القيمة الحالية</td>
                    <td>القيمة الاصلية</td>
                    <td>نسبه الاهلاك</td>
                    <td>تاريخ الشراء</td>
                    <td>مبلغ الاصل للبيع</td>
                    <td>تاريخ البيع</td>
                    <td>عدد السنوات لاعاده تقييم الاصل</td>
                    <td>الفترة المنقضية</td>
                    <td>حساب الفترة الماضية</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>



                <tbody>
                <?php foreach ($all_assets as $key => $asset_row): ?>
                <tr id="row<?= $asset_row->asset_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$asset_row->full_name}}</td>
                    <td>{{$asset_row->asset_name}}</td>
                    <td>{{$asset_row->asset_desc}}</td>
                    <td>{{$asset_row->asset_cost}}</td>
                    <td>{{$asset_row->asset_original_cost}}</td>
                    <td>{{$asset_row->asset_broken_percentage}}</td>
                    <td>{{$asset_row->asset_buy_date}}</td>
                    <td>{{$asset_row->asset_sell_amount}}</td>
                    <td>{{$asset_row->asset_sell_date}}</td>
                    <td>{{$asset_row->period_in_years}}</td>
                    <td>{{$asset_row->used_period}}</td>

                    <td>
                        <div class="alert alert-info">

                            تاريخ اخر حساب: {{$asset_row->last_calculated_date}}
                        </div>

                        <button class="btn btn-primary general_ajax_method" data-url="{{url("/admin/assets/calc_years_of_asset")}}" data-send_data_arr="{{json_encode(["asset_id"=>"$asset_row->asset_id"])}}" data-parent_div="td" data-show_msg_div=".show_msgs">
                            حساب الفترة الماضية
                        </button>
                        <div class="show_msgs"></div>
                    </td>
                    <td>
                        <a href="<?= url("admin/assets/save_asset/$asset_row->asset_id") ?>">
                            <span class="btn btn-info"> تعديل <i class="fa fa-edit"></i></span>
                        </a>
                    </td>
                    <td>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("/admin/assets/remove_asset") ?>" data-itemid="<?= $asset_row->asset_id ?>">
                            <span class="btn btn-danger"> مسح <i class="fa fa-remove"></i></span>
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>




@endsection
