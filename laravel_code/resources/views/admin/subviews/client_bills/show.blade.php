@extends('admin.main_layout')

@section('subview')

    <?php if(is_array($all_branches) && count($all_branches)): ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <div class="col-md-4 col-md-offset-4">

                <?php
                echo
                generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $selected_value=array($selected_branch),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                );
                ?>

            </div>

            <div class="col-md-12">

                <div class="col-md-6 form-group">
                    <label for="">من التاريخ</label>
                    <input type="date" class="form-control" id="from_date_id" value="{{$start_date}}">
                </div>

                <div class="col-md-6 form-group">
                    <label for="">الي التاريخ</label>
                    <input type="date" class="form-control" id="to_date_id" value="{{$end_date}}">
                </div>

            </div>

            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                    <button class="btn btn-info show_non_reviewed_bills_filter">ابحث</button>
                </div>
            </div>

        </div>
    </div>

    <?php endif; ?>

    <div class="panel panel-info">
        <div class="panel-heading">
           فواتير لم يتم مراجعتها
        </div>
        <div class="panel-body" style="overflow-y: hidden;overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>رقم الفاتورة</td>
                    <td>إجمالي المفدوع كاش</td>
                    <td>إجمالي المدفوع شبكة</td>
                    <td>الخصم</td>
                    <td>إجمالي الفاتورة</td>
                    <td>إجمالي المتبقي</td>
                    <td>إجمالي المرتجع</td>
                    <td>تاريخ الفاتورة</td>
                    <td>نسبه الشبكه</td>
                    <td>تم الموافقة عليها</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>رقم الفاتورة</td>
                    <td>إجمالي المفدوع كاش</td>
                    <td>إجمالي المدفوع شبكة</td>
                    <td>الخصم</td>
                    <td>إجمالي الفاتورة</td>
                    <td>إجمالي المتبقي</td>
                    <td>إجمالي المرتجع</td>
                    <td>تاريخ الفاتورة</td>
                    <td>نسبه الشبكه</td>
                    <td>تم الموافقة عليها</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_bills as $key => $bill): ?>

                <tr id="row<?= $bill->client_bill_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        <span class="label label-default">
                            {{$bill->client_bill_id}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$bill->client_total_paid_amount_in_cash}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$bill->client_total_paid_amount_in_atm}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$bill->bill_coupon_amount}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$bill->client_bill_total_amount}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-danger">
                            {{$bill->client_bill_total_remain_amount}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$bill->client_bill_total_return_amount}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$bill->client_bill_date}}
                        </span>
                        <br>
                        <span class="label label-success">
                            {{\Carbon\Carbon::createFromTimestamp(strtotime($bill->client_bill_date))->diffForHumans()}}
                        </span>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/review_client_bills","edit_atm_ratio",$current_user)): ?>

                            <span class="label label-info">
                                {{$bill->bank_atm_ratio_value}}
                            </span>
                            <br>
                            <br>
                            <?php

                            echo generate_self_edit_input(
                                    $url=url("/admin/review_client_bills/set_atm_ratio"),
                                    $item_obj = $bill,
                                    $item_primary_col = "client_bill_id",
                                    $item_edit_col = "bank_atm_ratio_value",
                                    $table='App\models\bills\clients_bills_m',
                                    $input_type="number",
                                    $label="Click To Edit");

                            ?>
                            <?php else: ?>
                            <span class="label label-warning">
                                ليس لديك الصلاحية
                            </span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                        echo
                        generate_multi_accepters(
                                $accepturl=url( "/admin/review_client_bills/bill_is_reviewed"),
                                $item_obj=$bill,
                                $item_primary_col="client_bill_id",
                                $accept_or_refuse_col="is_reviewed",
                                $model='App\models\bills\clients_bills_m',
                                $accepters_data=["0"=>"لا","1"=>"نعم"]
                        );
                        ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

        </div>
    </div>




@endsection
