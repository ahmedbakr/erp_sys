@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة سعر جديد";
    $price_id="";

    if ($price_data!="") {
        $header_text="تعديل سعر ".$price_data->price_title;

        $price_id=$price_data->price_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/prices/save_price/$price_id")?>" method="POST" enctype="multipart/form-data">


                    <?php

                    $normal_tags=array('price_title');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $price_data,
                            "yes",
                            $required="required"
                    );


                    $attrs[0]["price_title"]="اسم السعر *";
                    $attrs[6]["price_title"]="6";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5]),
                            reformate_arr_without_keys($attrs[6])
                    );
                    ?>


                    <?php
                        echo
                        generate_select_tags(
                            $field_name="default_price",
                            $label_name="السعر الافتراضي?",
                            $text=["لا","نعم"],
                            $values=["0","1"],
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="",
                            $disabled = "",
                            $data = $price_data,
                            $grid="6"
                        );

                    ?>

                    {{csrf_field()}}
                    <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

                </form>
            </div>
        </div>
    </div>


@endsection



