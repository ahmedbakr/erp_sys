@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            قائمة الاسعار
        </div>
        <div class="panel-body">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>عنوان السعر</td>
                    <td>السعر الافتراضي؟</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>



                <tbody>
                <?php foreach ($all_price_list as $key => $price): ?>

                <tr id="row<?= $price->price_id ?>">
                    <td><?=$key+1?></td>
                    <td>{{$price->price_title}}</td>
                    <td>{{($price->default_price)?"نعم":"لا"}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"admin/price_list","edit_action",$current_user)): ?>
                        <a href="<?= url("admin/prices/save_price/$price->price_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/price_list","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/prices/remove_price") ?>" data-tablename="App\models\admin\prices_m"  data-itemid="<?= $price->price_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/price_list","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/prices/save_price")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>

        </div>
    </div>




@endsection
