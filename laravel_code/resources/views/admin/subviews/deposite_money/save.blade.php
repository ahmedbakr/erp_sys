@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة إيداع جديد";

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            المطلوب إيداعه حتي هذا الوقت هو
        </div>
        <div class="panel-body">

            <div class="col-md-12">


                <div class="col-md-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $selected_value=[""],
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-4 form-group">
                    <label for="">التاريخ</label>
                    <input type="text" disabled class="form-control" id="to_date_id" value="{{$target_day}}">
                </div>

                <div class="col-md-4 show_original_deposite_money"></div>

            </div>

            <div class="col-md-2 col-md-offset-5 form-group">
                <button class="btn btn-info cal_original_deposite_money">ابحث</button>
            </div>

        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">

            <form id="save_form" action="<?=url("admin/deposite_money/save_deposite_money")?>" method="POST" enctype="multipart/form-data">

                <div class="col-md-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $selected_value=[""],
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <?php

                $normal_tags=array('deposite_money','deposite_desc');
                $attrs = generate_default_array_inputs_html(
                    $normal_tags,
                    $branch_target_data="",
                    "yes",
                    $required="required"
                );


                $attrs[0]["deposite_money"]="مبلغ الايداع * ";
                $attrs[0]["deposite_desc"]="تفاصيل اخري";


                $attrs[3]["deposite_money"]="number";
                $attrs[3]["deposite_desc"]="textarea";


                $attrs[6]["deposite_money"]="4";
                $attrs[6]["deposite_desc"]="12";

                echo
                generate_inputs_html(
                    reformate_arr_without_keys($attrs[0]),
                    reformate_arr_without_keys($attrs[1]),
                    reformate_arr_without_keys($attrs[2]),
                    reformate_arr_without_keys($attrs[3]),
                    reformate_arr_without_keys($attrs[4]),
                    reformate_arr_without_keys($attrs[5]),
                    reformate_arr_without_keys($attrs[6])
                );


                echo generate_img_tags_for_form(
                    $filed_name="attach_file",
                    $filed_label="attach_file",
                    $required_field="",
                    $checkbox_field_name="attach_file_checkbox",
                    $need_alt_title="no",
                    $required_alt_title="",
                    $old_path_value="",
                    $old_title_value="",
                    $old_alt_value="",
                    $recomended_size="",
                    $disalbed="disabled",
                    $displayed_img_width="100",
                    $display_label="رفع صورة الايداع",
                    $img_obj = ""
                );

                ?>

                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-3 col-md-offset-4 btn btn-primary btn-lg">

            </form>

        </div>
    </div>


@endsection



