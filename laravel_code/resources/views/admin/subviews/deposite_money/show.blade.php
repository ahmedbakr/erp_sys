@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">
            المطلوب إيداعه حتي هذا الوقت هو
        </div>
        <div class="panel-body">

            <div class="col-md-12">


                <div class="col-md-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $selected_value=[""],
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-4 form-group">
                    <label for="">التاريخ</label>
                    <input type="text" disabled class="form-control" id="to_date_id" value="{{$target_day}}">
                </div>

                <div class="col-md-4 show_original_deposite_money"></div>

            </div>

            <div class="col-md-2 col-md-offset-5 form-group">
                <button class="btn btn-info cal_original_deposite_money">ابحث</button>
            </div>

        </div>
    </div>

    <?php if(is_array($all_branches) && count($all_branches)): ?>

        <div class="panel panel-info">
            <div class="panel-heading">
                فلتر النتائج حسب :
            </div>
            <div class="panel-body">

                <div class="col-md-4 col-md-offset-4">

                    <?php
                        echo
                        generate_select_tags(
                                $field_name="branch_id",
                                $label_name="إختار الفرع",
                                $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                                $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                $selected_value=array($selected_branch),
                                $class="form-control select_2_class select_branch",
                                $multiple="",
                                $required = "",
                                $disabled = "",
                                $data = ""
                        );
                    ?>

                </div>

                <div class="col-md-12">

                    <div class="col-md-6 form-group">
                        <label for="">من التاريخ</label>
                        <input type="date" class="form-control" id="from_date_id" value="{{$start_date}}">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="">الي التاريخ</label>
                        <input type="date" class="form-control" id="to_date_id" value="{{$end_date}}">
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button class="btn btn-info show_deposite_money_filter">ابحث</button>
                    </div>
                </div>

            </div>
        </div>

    <?php endif; ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            إيداعات الفروع
        </div>
        <div class="panel-body"  style="overflow-y: hidden;overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>المبلغ المطلوب ايداعه</td>
                    <td>المبلغ الحالي فالصندوق</td>
                    <td>المبلغ الذي تم ايداعة</td>
                    <td>الفرق</td>
                    <td>تاريخ الايداع</td>
                    <td>المرفق</td>
                    <td>تفاصيل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>المبلغ المطلوب ايداعه</td>
                    <td>المبلغ الحالي فالصندوق</td>
                    <td>المبلغ الذي تم ايداعة</td>
                    <td>الفرق</td>
                    <td>تاريخ الايداع</td>
                    <td>المرفق</td>
                    <td>تفاصيل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($deposite_money as $key => $money): ?>

                <tr id="row<?= $money->b_d_money_id ?>" style="{{($money->lose_money == 0)?'background-color: green !important;':'background-color: red !important;'}}">
                    <td><?=$key+1?></td>
                    <td>{{$money->full_name}}</td>
                    <td>{{$money->origin_money}}</td>
                    <td>{{$money->total_money_on_box}}</td>
                    <td>{{$money->deposite_money}}</td>
                    <td>{{$money->lose_money}}</td>
                    <td>
                        <span class="label label-info">
                            {{$money->deposite_date}}
                        </span>
                        <br>
                        <span class="label label-success">
                            {{\Carbon\Carbon::createFromTimestamp(strtotime($money->deposite_date))->diffForHumans()}}
                        </span>
                    </td>

                    <td>
                        <?php if(empty($money->attach_file_path)): ?>
                        <span class="label label-warning">لا يوجد</span>
                        <?php else: ?>
                            <a href="<?= url('/'.$money->attach_file_path) ?>"><span class="label label-info"> مشاهدة<i class="fa fa-cloud-upload"></i></span></a>
                        <?php endif; ?>

                    </td>
                    <td>
                        {{$money->deposite_desc}}
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/deposite_money","delete_action",$current_user)): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/deposite_money/remove_deposite_money") ?>" data-tablename="App\models\branch_deposite_money_m"  data-itemid="<?= $money->b_d_money_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/deposite_money","add_action",$current_user)): ?>
                <a href='{{url("admin/deposite_money/save_deposite_money")}}' class="btn btn-primary" >إضافة إيداع فرع</a>
            <?php endif; ?>

        </div>
    </div>




@endsection
