@extends('admin.main_layout')

@section("subview")

    <?php if(false): ?>
    <div class="graphs">
        @include('admin.components.index_statistics')
    </div>
    <?php endif; ?>

    <div class="col-md-12">

        <?php
        $url = "";
        if ($current_user->user_type == "admin")
        {
            $url = url("/admin/users/save/admin/$current_user->user_id");
        }
        else if ($current_user->user_type == "branch_admin")
        {
            $url = url("/admin/users/change_password");
        }
        ?>

        <div class="col-md-2 display_dashboard_profile_img">
            <a href="{{$url}}">
                <img src="{{get_image_or_default($current_user->path)}}">
            </a>
        </div>


        <div class="col-md-5">
            <div>
                <label>اسم المستخدم : </label>
                <span>
                    <a href="{{$url}}">
                        {{$current_user->full_name}}
                    </a>
                </span>
            </div>
            <br>
            <div>
                <label>الإيميل : </label>
                <span>{{$current_user->email}}</span>
            </div>
            <br>
            <div>
                <label>اخر دخول : </label>
                <span>{{$current_user->last_login_date}}</span>
            </div>
            <br>
        </div>

        <div class="col-md-5">
            <div>
                <label>العنوان : </label>
                <span>{{$current_user->user_address}}</a>
                </span>
            </div>
            <div>
                <label>الفرع الحالي : </label>
                <?php if($current_user->user_type == "admin"): ?>
                <span>{{$current_user->full_name}}</span>
                <?php endif; ?>

                <?php if($current_user->user_type == "branch_admin"): ?>
                <span>{{$branch_data->full_name}}</span>
                <?php endif; ?>

            </div>
            <div>
                <label>متابعه التنقلات : </label>
                <span>
                    <a href="{{url("admin/users_transits?user_id=$current_user->user_id&start_date=&end_date=")}}">
                        مشاهدة <i class="fa fa-arrow-circle-o-left"></i>
                    </a>
                </span>

            </div>
            <div>
                <label>متابعه الحركات : </label>
                <span>
                    <a href="{{url("admin/tracking")}}">
                        مشاهدة <i class="fa fa-arrow-circle-o-left"></i>
                    </a>
                </span>

            </div>
        </div>

        <?php
        // today bills vars
        if ($today_total_commissions >= $today_bills["cash"])
        {
            $today_cash = ($today_total_commissions - $today_bills["cash"]);
        }
        else{
            $today_cash = ($today_bills["cash"] - $today_total_commissions);
        }

        $today_cash-=$today_bills["return_cash"];
        $today_remain = $today_bills["remain"];
        $today_atm = $today_bills["atm"]-$today_bills["return_atm"];
        $today_return = $today_bills["return"];
        $today_total = (($today_bills["cash"] + $today_bills["atm"]) - ($today_return + $today_total_commissions));

        $total_tax = ($today_bills["tax"] - $today_bills["tax_return"]);

        ?>


        <div class="col-md-6">
            <div class="send-mail text_align_right">
                <h3 class="heading_panel">المبيعات اليومية
                    <a href="{{url("print_today_bills?today_cash=$today_cash&today_remain=$today_remain&today_atm=$today_atm&today_return=$today_return&today_total_before_tax=".($today_total - $total_tax)."&total_tax=$total_tax&today_total=$today_total")}}" class="btn btn-info" style="float:left;">طباعة</a>
                </h3>

                <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                    <thead>
                    <tr>
                        <td>
                            من {{$start_day_date}}
                        </td>
                        <td>
                            الي{{$end_day_date}}
                        </td>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>نقدي : </td>
                        <td>
                            {{$today_cash}}
                        </td>
                    </tr>
                    <tr>
                        <td>شبكة : </td>
                        <td>{{$today_atm}} ريال</td>
                    </tr>
                    <tr>
                        <td>أجل : </td>
                        <td>{{$today_remain}} ريال</td>
                    </tr>
                    <tr>
                        <td>مرتجع : </td>
                        <td>{{$today_return}} ريال</td>
                    </tr>
                    <?php if(false): ?>
                    <tr>
                        <td>الخصومات : </td>
                        <td>{{$today_bills["discount"]}} ريال</td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>الإجمالي قبل القيمة المضافة : </td>
                        <td>{{($today_total - $total_tax)}} ريال</td>
                    </tr>

                    <tr>
                        <td>إجمالي القيمة المضافة : </td>
                        <td>{{$total_tax}} ريال</td>
                    </tr>

                    <tr>
                        <td>الإجمالي : </td>
                        <td>{{$today_total}} ريال</td>
                    </tr>

                    </tbody>

                </table>
            </div>
        </div>

        <div class="col-md-6 ">
            <div class="send-mail text_align_right">
                <h4 class="heading_panel" style="font-size: 16px !important;">مبيعات هذا الشهر
                    ({{$start_month_date.' - '.$current_date}})
                </h4>

                <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                    <tbody>
                    <tr>
                        <td>نقدي : </td>
                        <td>
                            <?php if($this_month_total_commissions >= $this_month_bills["cash"] ): ?>
                            {{($this_month_total_commissions - $this_month_bills["cash"])-$this_month_bills["return_cash"]}}  ريال
                            <?php else: ?>
                            {{($this_month_bills["cash"] - $this_month_total_commissions)-$this_month_bills["return_cash"]}} ريال
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>شبكة : </td>
                        <td>{{$this_month_bills["atm"]-$this_month_bills["return_atm"]}} ريال</td>
                    </tr>
                    <tr>
                        <td>أجل : </td>
                        <td>{{$this_month_bills["remain"]}} ريال</td>
                    </tr>
                    <tr>
                        <td>مرتجع : </td>
                        <td>{{$this_month_bills["return"]}} ريال</td>
                    </tr>
                    <?php if(false): ?>
                    <tr>
                        <td>الخصومات : </td>
                        <td>{{$this_month_bills["discount"]}} ريال</td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>الإجمالي قبل القيمة المضافة : </td>
                        <?php
                        $this_month_total = (($this_month_bills["cash"] + $this_month_bills["atm"]) - ($this_month_bills["return"] + $this_month_total_commissions));
                        ?>
                        <td>{{$this_month_total-($this_month_bills["tax"]-$this_month_bills["tax_return"])}} ريال</td>
                    </tr>
                    <tr>
                        <td>إجمالي القيمة المضافة : </td>
                        <td>{{($this_month_bills["tax"]-$this_month_bills["tax_return"])}} ريال</td>
                    </tr>
                    <tr>
                        <td>الإجمالي : </td>
                        <?php
                        $this_month_total = (($this_month_bills["cash"] + $this_month_bills["atm"]) - ($this_month_bills["return"] + $this_month_total_commissions));
                        ?>
                        <td>{{$this_month_total}} ريال</td>
                    </tr>
                    <tr>
                        <td>متوسط المبيعات : </td>
                        <?php
                            $current_day = intval(date("d"));
                            $avg_total_in_month = round(($this_month_total/$current_day),2);
                        ?>
                        <td>{{$avg_total_in_month}} ريال</td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>

        <?php if(!empty($branch_target)): ?>
        <div class="col-md-12 padd-left padd-right">
            <div class="send-mail text_align_right">
                <h3 class="heading_panel">التارجت الشهري </h3>

                <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                    <tbody>
                    <tr>
                        <td>المطلوب تحقيقة : </td>
                        <td>{{$branch_target}} ريال</td>
                    </tr>
                    <tr>
                        <td>المحقق : </td>
                        <td>{{$this_month_total}} ريال</td>
                    </tr>
                    <tr>
                        <td>الباقي : </td>
                        <td>
                            <?php if($branch_target >= $this_month_total): ?>
                            {{$branch_target - $this_month_total}} ريال
                            <?php else: ?>
                            تم تجاوز التارجت بقيمة
                            + {{$this_month_total - $branch_target}}
                            <?php endif; ?>


                        </td>
                    </tr>
                    <tr>
                        <td>متابعة </td>
                        <td>
                            <?php if(isset($branch_data)): ?>
                            <a href="{{url("admin/branch_target/branch_target/get_branch_target?branch_id=$branch_data->user_id&target_type=year&month=0&year=0")}}">
                                مشاهدة <i class="fa fa-arrow-circle-o-left"></i>
                            </a>
                            <?php else: ?>
                            <a href="{{url("admin/branch_target/branch_target/get_branch_target")}}">
                                مشاهدة <i class="fa fa-arrow-circle-o-left"></i>
                            </a>
                            <?php endif; ?>

                        </td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>
        <?php endif; ?>


    </div>


@endsection