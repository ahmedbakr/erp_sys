@extends('admin.main_layout')

@section('subview')

    <?php

    $header_txt = '<i class="fa fa-plus"></i> طباعه الباركود';

    ?>

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {!! $header_txt !!}
                    </h3>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/products_barcode/print/".$barcode_date->barcode_id)?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading">أدخل بيانات الطباعه , اي احجام تكون بالسم</div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-6">
                                <label for=""> إبحث عن المنتج بالاسم او الباركود * </label>
                                <select name="pro_id[]" class="form-control branch_get_products_without_search">
                                    <option value="0">0</option>
                                </select>
                            </div>

                            <?php


                            $normal_tags=array("expire_date");
                            $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $lang_data=$barcode_date,
                                    "yes",
                                    $required = ""
                            );

                            $attrs[0]["expire_date"]="تاريخ الصلاحية للمنتج *";
//                            $attrs[0]["paper_height"]="عدد الحبات *";

                            $attrs[2]["expire_date"]="required";
//                            $attrs[2]["paper_height"]="required";

                            $attrs[3]["expire_date"]="date";
//                            $attrs[3]["paper_height"]="number";

                            $attrs[4]["expire_date"]=(isset($expire_date)?$expire_date:date('Y-m-d'));
//                            $attrs[4]["paper_height"]=(isset($paper_height)?convert_from_cm_to_px($paper_height,true):'0');


                            $attrs[6]["expire_date"]="6";
//                            $attrs[6]["paper_height"]="6";

                            echo
                            generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5]),
                                    reformate_arr_without_keys($attrs[6])
                            );

                            ?>

                        </div>
                    </div>




                    {{csrf_field()}}
                    <input id="submit" type="submit" value="عرض" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">
                    <button class="col-md-3 col-md-offset-1 btn btn-primary btn-lg rotate_barcode">قلب القالب <i class="fa fa-rotate-90"></i></button>
                    <button class="print_page col-md-3 col-md-offset-1 btn btn-primary btn-lg">طباعه <i class="fa fa-print"></i></button>
                </div>

            </form>
            <div style="margin-top: 100px;">
                <?php if(isset($load_image_panel)): ?>

                    @include("admin.subviews.product.barcode.print_components.generate_image")

                <?php endif; ?>
            </div>

        </div>

    </div>


@endsection