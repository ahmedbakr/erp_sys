@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
     جميع قوالب الباركود الخاصة بالمنتجات
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم القالب</td>
            <td>عرض الباركود</td>
            <td>طول الباركود</td>
            <td>تعديل</td>
            <td>طباعة</td>
            <td>مسح</td>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($products_barcodes as $key => $barcode): ?>
        <tr id="row{{$barcode->barcode_id}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-default">
                    <?= $barcode->barcode_title ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $barcode->barcode_width ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $barcode->barcode_height ?>
                </span>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/product_barcode","edit_action",$current_user)): ?>
                    <a class="btn btn-primary btn-block" href="{{url('/admin/products_barcode/save/'.$barcode->barcode_id)}}"> تعديل </a>
                    <?php else: ?>
                    <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/product_barcode","print_barcode",$current_user)): ?>
                    <a class="btn btn-primary btn-block" href="{{url('/admin/products_barcode/print/'.$barcode->barcode_id)}}"> طباعة الباركود </a>
                    <?php else: ?>
                    <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"admin/product_barcode","delete_action",$current_user)): ?>
                    <a href='#' class="general_remove_item" data-tablename="App\models\product\product_barcode_m" data-deleteurl="<?= url("/admin/product_barcode/remove_barcode") ?>" data-itemid="<?= $barcode->barcode_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                    </a>
                    <?php else: ?>
                    <span class="alert alert-danger">
                        غير مسموح !!
                    </span>
                <?php endif; ?>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(check_permission($user_permissions,"admin/product_barcode","add_action",$current_user)): ?>
        <div class="col-md-3 col-md-offset-4">
            <a class="btn btn-primary btn-block" href="{{url('/admin/products_barcode/save')}}"> إضافة قالب باركود جديد </a>
        </div>
    <?php endif; ?>


@endsection