
<style>
    .hide_generated_image_panel{
        display: none;
    }
</style>


<input type="hidden" class="paper_width" value="{{$paper_width}}">
<input type="hidden" class="paper_height" value="{{$paper_height}}">


<div class="image_to_be_taken" style="width: {{$paper_width}}px;padding: 0px;margin: 0 auto;    display: inline-block;">

    <?php

        $end_loop = 0;


        $count_each_box_width = (
            convert_from_cm_to_px($barcode_date->barcode_width) +
            convert_from_cm_to_px($barcode_date->barcode_margin_right) +
            convert_from_cm_to_px($barcode_date->barcode_margin_left)
        );

        $count_each_box_height = (
            convert_from_cm_to_px($barcode_date->barcode_height) +
            convert_from_cm_to_px($barcode_date->barcode_margin_top) +
            convert_from_cm_to_px($barcode_date->barcode_margin_bottom)
        );

        $total_boxes_in_line = intval($paper_width/$count_each_box_width);
        $total_boxes_lines = intval($paper_height/$count_each_box_height);

        $end_loop = ($total_boxes_in_line*$total_boxes_lines);

        //$_POST['paper_height']
    ?>

    <?php for ($ind = 1;$ind <= (1); $ind++){ ?>

        <style>

            .date_block_{{$ind}}{
                font-size: 11px;
                font-weight: bold;
                position: absolute;
                margin-left: {{convert_from_cm_to_px($barcode_date->barcode_date_left_position)}}px;
                margin-top: {{convert_from_cm_to_px($barcode_date->barcode_date_top_position)}}px;
            }

            .img_block_{{$ind}}
            {
                position: absolute;
                margin-left: {{convert_from_cm_to_px($barcode_date->barcode_code_left_position)}}px;
                margin-top: {{convert_from_cm_to_px($barcode_date->barcode_code_top_position)}}px;
            }

            .reformat_barcode_block
             {

                /*display: inline;*/
                position: relative;
                float: left;
                width: {{convert_from_cm_to_px($barcode_date->barcode_width)}}px;
                height: {{convert_from_cm_to_px($barcode_date->barcode_height)}}px;
                margin-top: {{convert_from_cm_to_px($barcode_date->barcode_margin_top)}}px;
                margin-right: {{convert_from_cm_to_px($barcode_date->barcode_margin_right)}}px;
                margin-bottom: {{convert_from_cm_to_px($barcode_date->barcode_margin_bottom)}}px;
                margin-left: {{convert_from_cm_to_px($barcode_date->barcode_margin_left)}}px;


            }

            .rotate_barcode_block{
                transform: rotate(-90deg);
            }

            @media print {
                body * {
                    visibility: hidden;
                }

                .reformat_barcode_block , .reformat_barcode_block *{
                    visibility: visible;
                }
                .reformat_barcode_block {
                    position: absolute;
                    left: 0px;
                    top: 0px;
                }
                .reformat_barcode_block table tr.top table td{
                    width:100%;
                    /*display:block;*/
                    /*text-align:center;*/
                }

                .reformat_barcode_block table tr.information table td{
                    width:100%;
                    /*display:block;*/
                    /*text-align:center;*/
                }


            }

        </style>

        <div class="reformat_barcode_block" data-rotation="0">
            <span class="date_block_{{$ind}}" style="font-size: 7px !important;">{{$expire_date}}</span>
            <span class="date_block_{{$ind}}" style="margin-right: 40px;font-size: 7px !important;width: 50%;">{{$get_pro->pro_name}}</span>
            <img class="img_block_{{$ind}}" src="{{url('admin/barcode/get_barcode_img/'.$pro_barcode)}}" />
        </div>

    <?php } ?>

</div>


