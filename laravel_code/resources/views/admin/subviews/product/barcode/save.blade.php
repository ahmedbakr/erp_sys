@extends('admin.main_layout')

@section('subview')

    <?php

    $header_txt = '<i class="fa fa-plus"></i> إضافة قالب باركود جديد';
    $barcode_id = "";

    if (is_object($barcode_date))
    {
        $header_txt = '<i class="fa fa-edit"></i> تعديل قالب الباركود '.$barcode_date->pro_name;
        $barcode_id = $barcode_date->barcode_id;
    }

    ?>

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {!! $header_txt !!}
                    </h3>
                </div>
            </div>

            <form id="save_form" action="<?=url("/admin/products_barcode/save/".$barcode_id)?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading"> بيانات قالب الباركود اي احجام تكون بالسم</div>
                    <div class="panel-body">
                        <div class="row">
                            <?php


                            $normal_tags=array("barcode_title","paper_width","barcode_width","barcode_height","barcode_margin_top","barcode_margin_right","barcode_margin_bottom",
                                    "barcode_margin_left","barcode_date_top_position","barcode_date_left_position",
                                    "barcode_code_top_position","barcode_code_left_position");
                            $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $lang_data=$barcode_date,
                                    "yes",
                                    $required = ""
                            );

                            $attrs[0]["barcode_title"]="اسم قالب الباركود * ";
                            $attrs[0]["paper_width"]="عرض ورقة الباركود * ";
                            $attrs[0]["barcode_width"]="عرض بوكس او لابل الباركود * ";
                            $attrs[0]["barcode_height"]="طول بوكس او لابل الباركود * ";

                            $attrs[0]["barcode_margin_top"]="المسافة البينية للبوكس من الاعلي";
                            $attrs[0]["barcode_margin_right"]="المسافة البينية للبوكس من اليمين";
                            $attrs[0]["barcode_margin_bottom"]="المسافة البينية للبوكس من تحت";
                            $attrs[0]["barcode_margin_left"]="المسافة البينية للبوكس من اليسار";

                            $attrs[0]["barcode_date_top_position"]="بعد التاريخ عن البوكس من الاعلي";
                            $attrs[0]["barcode_date_left_position"]="بعد التاريخ عن البوكس من اليسار";

                            $attrs[0]["barcode_code_top_position"]="بعد الكود عن البوكس من الاعلي";
                            $attrs[0]["barcode_code_left_position"]="بعد الكود عن البوكس من اليسار";

                            $attrs[2]["barcode_width"]="required";
                            $attrs[2]["barcode_height"]="required";

                            $attrs[3]["barcode_size"]="number";
                            $attrs[3]["barcode_margin_top"]="number";
                            $attrs[3]["barcode_margin_right"]="number";
                            $attrs[3]["barcode_margin_bottom"]="number";
                            $attrs[3]["barcode_margin_left"]="number";
                            $attrs[3]["barcode_date_top_position"]="number";
                            $attrs[3]["barcode_date_left_position"]="number";
                            $attrs[3]["barcode_code_top_position"]="number";
                            $attrs[3]["barcode_code_left_position"]="number";

                            echo
                            generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                            );

                            ?>

                        </div>

                    </div>

                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">
                </div>

            </form>


        </div>

    </div>


@endsection