@extends('admin.main_layout')

@section('subview')


    <?php

    $header_text = "<i class='fa fa-plus'></i> إضافة هالك جديد للماده الخام";

    ?>


    <div class="container">

        <div class="col-md-8 col-md-offset-2">

            <?php if(is_array($materials) && count($materials)): ?>

                <?php if(is_array($product_materials) && count($product_materials)): ?>

                <form id="save_form" action="<?=url("/admin/product/save_broken_product_materials")?>" method="POST" enctype="multipart/form-data">
                    <h1>{!! $header_text !!}</h1>
                    <?php

                    if (count($errors->all()) > 0)
                    {
                        $dump = "<div class='alert alert-danger'>";
                        foreach ($errors->all() as $key => $error)
                        {
                            $dump .= $error." <br>";
                        }
                        $dump .= "</div>";

                        echo $dump;
                    }


                    if (isset($success)&&!empty($success)) {
                        echo $success;
                    }


                    echo generate_select_tags(
                            $field_name="mat_id",
                            $label_name="إختار الماده الخام",
                            $text=convert_inside_obj_to_arr($materials,"mat_name"),
                            $values=convert_inside_obj_to_arr($materials,"mat_id"),
                            $selected_value=array(),
                            $class="form-control",
                            $multiple = "",
                            $required = "required",
                            $disabled = "");

                    $normal_tags=array("b_m_quantity","b_m_date");
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $material_data = "",
                            "yes",
                            $required = ""
                    );

                    $attrs[0]["b_m_quantity"]="كمية الهالك";
                    $attrs[0]["b_m_date"]="التاريخ";

                    $attrs[3]["b_m_quantity"]="number";
                    $attrs[3]["b_m_date"]="date";

                    $attrs[2]["b_m_quantity"]="required";
                    $attrs[2]["b_m_date"]="required";

                    $attrs[4]["b_m_quantity"]=1;

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );

                    ?>

                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-4 btn btn-primary btn-lg">
                </form>

                <?php else: ?>
                {!! get_warning($url = url('/admin/product/save_product_materials') , $text = "أضف كمية للمواد الخام اولا") !!}
                <?php endif; ?>

            <?php else: ?>
            {!! get_warning($url = url('/admin/materials/save') , $text = "أضف مواد خام اولا") !!}

            <?php endif; ?>

        </div>

    </div>


@endsection