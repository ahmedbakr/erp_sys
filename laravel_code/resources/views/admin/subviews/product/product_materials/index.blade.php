@extends('admin.main_layout')

@section('subview')


    <div class="alert alert-info" style="text-align: center">
        <h3>
            كمية المواد الخام للمنتجات
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم الماده الخام</td>
            <td>الكمية</td>
            <td>السعر</td>
            <td>حد الطلب</td>
            <td>البار كود</td>
            <td>التاريخ</td>
            <?php if($user_permissions["product/product_materials"][0]->edit_action): ?>
            <td>تعديل</td>
            <?php endif; ?>
            <?php if($user_permissions["product/product_materials"][0]->delete_action): ?>
            <td>مسح</td>
            <?php endif; ?>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($all_materials as $key => $material): ?>
        <tr id="row<?= $material->pro_mat_id ?>">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-info">
                    <?= $material->mat_name ?>
                </span>
            </td>
            <td>
                <span class="label label-info"><?= $material->mat_amount ?></span>
                <span class="label label-success"><?= $material->mat_type ?></span>
            </td>
            <td>
                <span class="label label-warning">
                    <?= $material->mat_price ?>
                </span>
            </td>
            <td>
                <span class="label label-danger">
                    <?= $material->mat_pro_limit ?>
                </span>
            </td>
            <td>
                <?php if(!empty($material->mat_barcode)): ?>
                    <span class="label label-default">
                        <?= $material->mat_barcode ?>
                    </span>
                <?php endif; ?>

            </td>
            <td>
                <span class="label label-info">{{$material->pro_mat_created}}</span>
                <span class="label label-success">
                    {{\Carbon\Carbon::createFromTimestamp(strtotime($material->pro_mat_created))->diffForHumans()}}
                </span>

            </td>

            <?php if($user_permissions["product/product_materials"][0]->edit_action): ?>
                <td>
                    <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_product_materials/'.$material->pro_mat_id)}}"> تعديل </a>
                </td>
            <?php endif; ?>

            <?php if($user_permissions["product/product_materials"][0]->delete_action): ?>
                <td>
                    <a href='#' class="general_remove_item" data-tablename="App\models\product\product_materials_m" data-deleteurl="<?= url("/admin/product/remove_product_materials") ?>" data-itemid="<?= $material->pro_mat_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                    </a>
                </td>
            <?php endif; ?>


        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if($user_permissions["product/product_materials"][0]->add_action): ?>
        <div class="col-md-3 col-md-offset-4">
            <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_product_materials')}}"> إضافة كمية جديدة </a>
        </div>
    <?php endif; ?>


@endsection