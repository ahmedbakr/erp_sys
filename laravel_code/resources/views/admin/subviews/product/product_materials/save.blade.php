@extends('admin.main_layout')

@section('subview')


    <?php

    $header_text = "<i class='fa fa-plus'></i> إضافة كمية ماده خام للمنتجات جديدة";
    $id = "";
    $mat_id = "";
    $select_mat_disabled = "";
    if (is_object($material_data))
    {
        $id = $material_data->pro_mat_id;
        $mat_id = $material_data->mat_id;
        $select_mat_disabled = "disabled";
        $header_text = "<i class='fa fa-edit'></i> تعديل كمية الماده الخام رقم.$id";
    }

    ?>


    <div class="container">

        <div class="col-md-8 col-md-offset-2">

            <?php if(is_array($materials) && count($materials)): ?>

                <form id="save_form" action="<?=url("/admin/product/save_product_materials/$id")?>" method="POST" enctype="multipart/form-data">
                    <h1>{!! $header_text !!}</h1>
                    <?php

                    if (count($errors->all()) > 0)
                    {
                        $dump = "<div class='alert alert-danger'>";
                        foreach ($errors->all() as $key => $error)
                        {
                            $dump .= $error." <br>";
                        }
                        $dump .= "</div>";

                        echo $dump;
                    }


                    if (isset($success)&&!empty($success)) {
                        echo $success;
                    }


                    echo generate_select_tags(
                            $field_name="mat_id",
                            $label_name="إختار الماده الخام",
                            $text=convert_inside_obj_to_arr($materials,"mat_name"),
                            $values=convert_inside_obj_to_arr($materials,"mat_id"),
                            $selected_value=array($mat_id),
                            $class="form-control",
                            $multiple = "",
                            $required = "required",
                            $disabled = $select_mat_disabled);

                    $normal_tags=array("mat_amount","mat_price","mat_pro_limit","mat_desc","mat_barcode");
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $material_data,
                            "yes",
                            $required = ""
                    );

                    $attrs[0]["mat_amount"]="الكمية";
                    $attrs[0]["mat_price"]="سعر الوحده";
                    $attrs[0]["mat_pro_limit"]="حد الطلب";
                    $attrs[0]["mat_barcode"]="الباركود";
                    $attrs[0]["mat_desc"]="ملخص عن الماده الخام";

                    $attrs[3]["mat_amount"]="number";
                    $attrs[3]["mat_price"]="number";
                    $attrs[3]["mat_pro_limit"]="number";
                    $attrs[3]["mat_barcode"]="text";
                    $attrs[3]["mat_desc"]="textarea";

                    $attrs[2]["mat_amount"]="required";
                    $attrs[2]["mat_price"]="required";
                    $attrs[2]["mat_pro_limit"]="required";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5])
                    );

                    ?>

                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-4 btn btn-primary btn-lg">
                    <a href="{{url('/admin/product/save_product_materials')}}" class="col-md-4 col-md-offset-2 btn btn-primary btn-lg"><i class="fa fa-plus"></i> إضافة جديد</a>
                </form>


            <?php else: ?>
            {!! get_warning($url = url('/admin/materials/save') , $text = "إضافة مواد خام اولا") !!}

            <?php endif; ?>

        </div>

    </div>


@endsection