@extends('admin.main_layout')

@section('subview')

    <div class="alert alert-info" style="text-align: center">
        <h3>
            الهالك من المواد الخام
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>اسم الماده الخام</td>
            <td>الكمية</td>
            <td>التاريخ</td>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($broken_materials as $key => $material): ?>
        <tr>
            <td><?=$key+1?></td>
            <td>
                <span class="label label-info">
                    <?= $material->mat_name ?>
                </span>
            </td>
            <td>
                <span class="label label-info"><?= $material->b_m_quantity ?></span>
                <span class="label label-success"><?= $material->mat_type ?></span>
            </td>
            <td>
                <span class="label label-info">{{$material->b_m_date}}</span>
                <span class="label label-success">
                    {{\Carbon\Carbon::createFromTimestamp(strtotime($material->b_m_date))->diffForHumans()}}
                </span>

            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if($user_permissions["product/broken_product_materials"][0]->add_action): ?>
    <div class="col-md-3 col-md-offset-4">
        <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_broken_product_materials')}}"> إضافة هالك جديد للماده الخام </a>
    </div>
    <?php endif; ?>


@endsection