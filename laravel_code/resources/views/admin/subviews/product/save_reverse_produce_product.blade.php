@extends('admin.main_layout')

@section('subview')

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        <i class="fa fa-plus"></i>سحب منتجات أنتجت بالخطأ
                    </h3>
                </div>
                <div class="alert alert-warning" style="text-align: center">
                    <h5>
                        لسحب منتجات تم إنتاجها بالخطأ ادخل الكمية بالاسفل واختار المنتج
                    </h5>
                </div>
            </div>

            <?php if(is_array($all_products) && count($all_products)): ?>

            <form id="save_form" action="<?=url("/admin/product/save_reverse_produce_product")?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading">سحب منتجات أنتجت بالخطأ</div>
                    <div class="panel-body">
                        <div class="row">
                            <?php

                            echo generate_select_tags(
                                    $field_name="pro_id",
                                    $label_name="إختار المنتج",
                                    $text=convert_inside_obj_to_arr($all_products,"pro_name"),
                                    $values=convert_inside_obj_to_arr($all_products,"pro_id"),
                                    $selected_value=array(),
                                    $class="form-control");


                            $normal_tags=array("pro_quantity");
                            $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $lang_data="",
                                    "yes",
                                    $required = "required"
                            );

                            $attrs[0]["pro_quantity"]="الكمية المطلوب سحبها";
                            $attrs[3]["pro_quantity"]="number";
                            $attrs[4]["pro_quantity"]=1;


                            echo
                            generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                            );


                            ?>

                        </div>

                    </div>

                </div>

                {{csrf_field()}}
                <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">
            </form>

            <?php else: ?>
            {!! get_warning($url = url('/admin/product/save_products_on_stock') , $text = "أضف منتجات اولا") !!}

            <?php endif; ?>

        </div>

    </div>


@endsection