@extends('admin.main_layout')

@section('subview')

    <?php

        $header_txt = '<i class="fa fa-plus"></i> إضافة منتج جديد';
        $pro_id = "";
        $pro_img_path = "";
        $disabled_upload_imgs="";
        if (is_object($product_data))
        {
            $header_txt = '<i class="fa fa-edit"></i> تعديل المنتج '.$product_data->pro_name;
            $header_txt .= '<br><small><i class="fa fa-calendar"></i> أنشأ '.$product_data->created_at.' من '.\Carbon\Carbon::createFromTimestamp(strtotime($product_data->created_at))->diffForHumans().'</small>';
            $pro_id = $product_data->pro_id;
            if (!empty($product_data->pro_img_path))
            {
                $pro_img_path = url($product_data->pro_img_path);
                $disabled_upload_imgs="disabled";
            }
        }

    ?>

    <div class="panel panel-info">
        <div class="panel-heading">{!! $header_txt !!}</div>
        <div class="panel-body">

            <div class="col-md-12">

                    <?php if(true||is_array($materials) && count($materials)): ?>


                        <?php

                        if (count($errors->all()) > 0)
                        {
                            $dump = "<div class='alert alert-danger'>";
                            foreach ($errors->all() as $key => $error)
                            {
                                $dump .= $error." <br>";
                            }
                            $dump .= "</div>";

                            echo $dump;
                        }


                        if (isset($success)&&!empty($success)) {
                            echo $success;
                        }
                        ?>


                        <div class="panel panel-primary">
                            <div class="panel-heading">أضف منتجات من ملف اكسيل</div>
                            <div class="panel-body">
                                <div class="row">
                                    <form id="save_form" action="<?=url("/admin/product/import_products_to_stock/$cat_id")?>" method="POST" enctype="multipart/form-data">

                                        <div class="alert alert-info" style="font-weight: bold;">
<h3 style="text-align: center;">برجاء الإنتباه قبل تحميل ملف الاكسيل برجاء اتباع ما يلي :-</h3>
                                            <ul>
                                                <li> برجاء تحميل هذا الملف وملئ الصفوف قبل الرفع <a href="{{url('uploads/excel_templates/products_template.xlsx')}}" target="_blank">التحميل من هنا</a></li>
                                                <li>يجيب ان يكون إمتداد الملف واحد من هؤلاء ["xls",".XLS",".xlsx",".XLSX.")</li>
                                                <li>يجب ترك اول صف كما هو دون تغيير او مسح او تعديل اي عمود</li>
                                                <li>اذا تم كتابه منتج الباركود او اسمه موجود مسبقا سيتم زياده الكمية للمنتج</li>
                                                <li>إذا تم كتاب اكتر من صف داخل ملف الاكسيل بنفس اسم المنتج سيتم إدخال منتج واحد فقط وهو الاول مع جمع الكميات للكل </li>
                                            </ul>

                                        </div>


                                        <?php

                                        echo generate_img_tags_for_form(
                                            $filed_name="pro_excel_file",
                                            $filed_label="pro_excel_file",
                                            $required_field="",
                                            $checkbox_field_name="pro_excel_checkbox",
                                            $need_alt_title="no",
                                            $required_alt_title="",
                                            $old_path_value="",
                                            $old_title_value="",
                                            $old_alt_value="",
                                            $recomended_size="",
                                            $disalbed=$disabled_upload_imgs,
                                            $displayed_img_width="100",
                                            $display_label="رفع ملف اكسيل للمنتجات",
                                            $img_obj = ""
                                        );

                                        ?>

                                            {{csrf_field()}}

                                            <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-2 btn btn-primary btn-lg" style="margin-top: 5px;">

                                    </form>
                                </div>
                            </div>
                        </div>

                        <form id="save_form" action="<?=url("/admin/product/save_products_on_stock/$cat_id/".$pro_id)?>" method="POST" enctype="multipart/form-data">

                        <div class="panel panel-primary">
                            <div class="panel-heading">بيانات المنتج</div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php


                                    $normal_tags=array("pro_name","pro_quantity","pro_limit","pro_barcode","pro_desc");
                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data=$product_data,
                                        "yes",
                                        $required = ""
                                    );

                                    $attrs[0]["pro_name"]="اسم المنتج";
                                    $attrs[0]["pro_quantity"]="كميه المنتج";
                                    $attrs[0]["pro_limit"]="حد الطلب للمنتج";
                                    $attrs[0]["pro_barcode"]="الباركود للمنتج";
                                    $attrs[0]["pro_desc"]="وصف المنتج";

                                    $attrs[2]["pro_name"]="required";


                                    $attrs[3]["pro_name"]="text";
                                    $attrs[3]["pro_quantity"]="number";
                                    $attrs[3]["pro_limit"]="number";
                                    $attrs[3]["pro_barcode"]="text";
                                    $attrs[3]["pro_desc"]="textarea";

                                    $attrs[6]["pro_name"]="3";
                                    $attrs[6]["pro_quantity"]="3";
                                    $attrs[6]["pro_limit"]="3";
                                    $attrs[6]["pro_barcode"]="3";
                                    $attrs[6]["pro_desc"]="12";

                                    echo
                                    generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5]),
                                        reformate_arr_without_keys($attrs[6])
                                    );
                                ?>

                                <?php if(count($product_units)): ?>
                                    <div class="col-md-6">
                                        <?php
                                        echo generate_select_tags(
                                            $field_name="pro_unit_id",
                                            $label_name="إختار الوحدة ",
                                            $text=array_merge(convert_inside_obj_to_arr($product_units,"pro_unit_name")),
                                            $values=array_merge(convert_inside_obj_to_arr($product_units,"pro_unit_id")),
                                            $selected_value=[""],
                                            $class="form-control select_2_class",
                                            $multiple="",
                                            $required="",
                                            $disabled = "",
                                            $data = $product_data
                                        );
                                        ?>
                                    </div>
                                <?php endif; ?>

                                <?php
                                    echo generate_img_tags_for_form(
                                        $filed_name="pro_img_file",
                                        $filed_label="pro_img_file",
                                        $required_field="",
                                        $checkbox_field_name="pro_img_checkbox",
                                        $need_alt_title="no",
                                        $required_alt_title="",
                                        $old_path_value=$pro_img_path,
                                        $old_title_value="",
                                        $old_alt_value="",
                                        $recomended_size="",
                                        $disalbed=$disabled_upload_imgs,
                                        $displayed_img_width="100",
                                        $display_label="رفع صورة المنتج",
                                        $img_obj = ""
                                    );

                                    ?>

                                </div>
                            </div>




                            <?php if(count($product_codes)): ?>
                            <div class="panel panel-info">
                                <div class="panel-heading">بيانات أكواد المنتجات</div>
                                <div class="panel-body">

                                    <?php foreach($product_codes as $key => $product_code): ?>

                                        <div class="col-md-6">
                                            <label for="">اسم الكود</label>
                                            <input type="text" disabled="disabled" class="form-control" name="pro_code_label[]" value="{{$product_code->pro_code_label}}">
                                            <input type="hidden" class="form-control" name="pro_code_id[]" value="{{$product_code->pro_code_id}}">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">قيمة الكود لا يمكن تكرارة !</label>
                                            <input type="text" class="form-control" name="pro_code_val[]" value="{{$product_code->pro_code_val}}">
                                        </div>

                                    <?php endforeach; ?>

                                </div>
                            </div>
                            <?php endif; ?>

                            <?php if(false): ?>

                                <div class="bill_orders_containers">

                                    <?php if((is_array($product_components) && count($product_components))): ?>

                                        <?php foreach($product_components as $key => $value): ?>

                                        <div class="panel panel-primary bill_order_item">
                                            <a href="" class="btn btn-danger pull-right delete_order_item"> X </a>
                                            <div class="panel-heading show_collapse_hand collapse_action">
                                                كمية الماده للخام لتصنيع المنتج
                                            </div>
                                            <div class="panel-body collapse_body collapse">

                                                <?php

                                                    echo generate_select_tags(
                                                        $field_name="mat_id[]",
                                                        $label_name="إختار الماده الخام",
                                                        $text=convert_inside_obj_to_arr($materials,"mat_name"),
                                                        $values=convert_inside_obj_to_arr($materials,"mat_id"),
                                                        $selected_value=array($value->mat_id),
                                                        $class="form-control");

                                                    $normal_tags=array("pro_comp_amount[]");
                                                    $attrs = generate_default_array_inputs_html(
                                                        $normal_tags,
                                                        $lang_data="",
                                                        "yes",
                                                        $required = "required"
                                                    );

                                                    $attrs[0]["pro_comp_amount[]"]="كمية الماده الخام";
                                                    $attrs[4]["pro_comp_amount[]"]=$value->pro_comp_amount;


                                                    $attrs[3]["pro_comp_amount[]"]="number";

                                                    echo
                                                    generate_inputs_html(
                                                        reformate_arr_without_keys($attrs[0]),
                                                        reformate_arr_without_keys($attrs[1]),
                                                        reformate_arr_without_keys($attrs[2]),
                                                        reformate_arr_without_keys($attrs[3]),
                                                        reformate_arr_without_keys($attrs[4]),
                                                        reformate_arr_without_keys($attrs[5])
                                                    );

                                                ?>

                                            </div>

                                        </div>

                                        <?php endforeach; ?>

                                    <?php else: ?>
                                        <div class="panel panel-primary bill_order_item">
                                        <a href="" class="btn btn-danger pull-right delete_order_item"> X </a>
                                        <div class="panel-heading show_collapse_hand collapse_action">
                                            كمية الماده للخام لتصنيع المنتج

                                        </div>
                                        <div class="panel-body collapse_body collapse">

                                            <?php

                                            echo generate_select_tags(
                                                $field_name="mat_id[]",
                                                $label_name="إختار الماده الخام",
                                                $text=convert_inside_obj_to_arr($materials,"mat_name"),
                                                $values=convert_inside_obj_to_arr($materials,"mat_id"),
                                                $selected_value=array(),
                                                $class="form-control");

                                            $normal_tags=array("pro_comp_amount[]");
                                            $attrs = generate_default_array_inputs_html(
                                                $normal_tags,
                                                $lang_data="",
                                                "yes",
                                                $required = "required"
                                            );

                                            $attrs[0]["pro_comp_amount[]"]="كمية الماده الخام";


                                            $attrs[3]["pro_comp_amount[]"]="number";

                                            echo
                                            generate_inputs_html(
                                                reformate_arr_without_keys($attrs[0]),
                                                reformate_arr_without_keys($attrs[1]),
                                                reformate_arr_without_keys($attrs[2]),
                                                reformate_arr_without_keys($attrs[3]),
                                                reformate_arr_without_keys($attrs[4]),
                                                reformate_arr_without_keys($attrs[5])
                                            );

                                            ?>

                                        </div>

                                    </div>

                                    <?php endif; ?>

                                </div>

                                <a href="#" class="col-md-3 col-md-offset-1 btn btn-info clone_order_item">
                                    <i class="fa fa-plus"></i> ماده خام جديدة
                                </a>
                            <?php endif; ?>



                        </div>

                        <?php if(is_object($product_data)): ?>

                            <div class="panel panel-info">
                                <div class="panel-heading">تحويل الي تصنيف آخر</div>
                                <div class="panel-body">
                                    @include("common.category.accounts_tree.tree_modal")



                                    <div class="alert alert-info" style="text-align: center;">
                                        ملحوظة اذا لم تختار اي تصنيف . لن يحدث اي تغيير
                                    </div>


                                    <button
                                            type="button"
                                            class="btn btn-primary show_tree_modal center-block"
                                            data-select_what="category"
                                            data-select_type="single"
                                            data-div_to_show_selection=".move_to_new_category_div"
                                            data-input_field_name="cat_id"
                                    >
                                        اظهر الشجرة لتختار التصنيف المراد النقل اليه
                                    </button>


                                    <div class="move_to_new_category_div" style="text-align: center;">

                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>


                        {{csrf_field()}}

                        <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-2 btn btn-primary btn-lg" style="margin-top: 5px;">
                        <a href="{{url('admin/product/save_products_on_stock/'.$cat_id)}}" class="col-md-3 col-md-offset-2 btn btn-primary btn-lg"  style="margin-top: 5px;"> إضافة منتج جديد</a>




                    </form>

                    <?php else: ?>
                        {!! get_warning($url = url('/admin/materials/save') , $text = "أضف مواد خام اولا") !!}
                    <?php endif; ?>

                </div>


        </div>
    </div>





@endsection