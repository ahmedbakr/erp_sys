@extends('admin.main_layout')

@section('subview')

    <style>
        .general_self_edit{
            display: block;
        }
        .general_self_edit:hover{
            border: 2px dashed #0000cc;
        }
    </style>




    <div class="panel panel-info">
        <div class="panel-heading">العمليات</div>
        <div class="panel-body">
            <form action="{{url("/admin/product/products_on_branches_stock")}}" method="get">

                <?php
                    echo generate_select_tags(
                        $field_name="branch_id",
                        $label_name="اختار الفرع",
                        $text=array_merge([""],convert_inside_obj_to_arr($all_branches,"full_name")),
                        $values=array_merge([""],convert_inside_obj_to_arr($all_branches,"user_id")),
                        $selected_value=[$branch_id],
                        $class="form-group select_2_class",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = "",
                        $grid="6"
                    );

                ?>

                <div class="form-group col-md-6">
                    <label for="">البحث بواسطة الباركود او اسم المنتج</label>
                    <input type="text" class="form-control" name="search_product" value="{{(isset($search_product))?$search_product:""}}">
                </div>

                <button type="submit" class="btn btn-primary">ابحث</button>

            </form>
        </div>
    </div>
    
    <div class="panel panel-info">
        <div class="panel-heading">المنتجات في المخزن</div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>صورة المنتج</td>
                    <td>اسم المنتج</td>
                    <td>كمية المنتج</td>
                    <td>حد الطلب</td>
                    <td>وحدة القياس</td>
                    <td>الباركود</td>
                </tr>
                </thead>


                <tbody id="sortable">
                <?php foreach ($all_products as $key => $product): ?>
                <tr id="row<?= $product->pro_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        <img width="100" src="{{get_image_or_default($product->pro_img_path)}}">
                    </td>
                    <td>
                        <span class="label label-info"><?= $product->pro_name ?></span>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"product/products_on_stock","edit_action",$current_user)): ?>
                            <span
                                   title="إضغط بالماوس للتعديل"
                                   class="general_self_edit"
                                   data-tablename="App\models\product\branches_products_m"
                                   data-field_name="b_p_quantity"
                                   data-row_primary_col="b_p_id"
                                   data-input_type="number"
                                   data-field_value="<?= $product->b_p_quantity ?>"
                                   data-model=""
                                   data-url="<?= url("/general_self_edit") ?>"
                                   data-row_id="{{$product->b_p_id}}">
                                <?= $product->b_p_quantity ?>
                                <i class="fa fa-edit"></i>
                            </span>
                        <?php else: ?>
                            <span class="label label-default">
                                <?= $product->b_p_quantity ?>
                            </span>
                        <?php endif; ?>

                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"product/products_on_stock","edit_action",$current_user)): ?>
                        <span
                                title="إضغط بالماوس للتعديل"
                                class="general_self_edit"
                                data-tablename="App\models\product\branches_products_m"
                                data-field_name="b_p_quantity_limit"
                                data-row_primary_col="b_p_id"
                                data-input_type="number"
                                data-field_value="<?= $product->b_p_quantity_limit ?>"
                                data-model=""
                                data-url="<?= url("/general_self_edit") ?>"
                                data-row_id="{{$product->b_p_id}}">
                                <?= $product->b_p_quantity_limit ?>
                            <i class="fa fa-edit"></i>
                            </span>
                        <?php else: ?>
                        <span class="label label-danger">
                                <?= $product->b_p_quantity_limit ?>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <span class="label label-info">
                            <?= $product->pro_unit_name ?>
                        </span>
                    </td>
                    <td>
                        <?php if(!empty($product->pro_barcode)): ?>
                        <span class="label label-default">
                        <?= $product->pro_barcode ?>
                    </span>
                        <?php endif; ?>
                    </td>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(!empty($products_pagination)): ?>
            <div class="col-md-12" style="text-align: center;">
                {{$products_pagination->appends(\Illuminate\Support\Facades\Input::except('page'))}}
            </div>
            <?php endif; ?>


        </div>
    </div>







@endsection