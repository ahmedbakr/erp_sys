@extends('admin.main_layout')

@section('subview')

    <?php

//    $header_txt = '<i class="fa fa-plus"></i> Add Product Prices of Branch >> "'.$branch_data->full_name.'" And Product >> "'.$product_data->pro_name.'" ';
    if (is_object($product_data))
    {
        $header_txt = '<i class="fa fa-wrench"></i> أسعار المنتج <br><br> للفرع :- "'.$branch_data->full_name.'" <br><br> والمنتج :- ';
        $header_txt .= '<br> الاسم :- "'.$product_data->pro_name.'"';
        $header_txt .= '<br> الكمية :- "'.$product_data->pro_quantity.'"';
        $header_txt .= '<br><small><i class="fa fa-calendar"></i> أنشأ <span class="label label-success">'.$product_data->created_at.'</span> from <span class="label label-info">'.\Carbon\Carbon::createFromTimestamp(strtotime($product_data->created_at))->diffForHumans().'</span></small>';
    }

    ?>

    <div class="container">

        <div class="col-md-12">

            <div class="row">
                <div class="alert alert-info" style="text-align: center">
                    <h3>
                        {!! $header_txt !!}
                    </h3>
                </div>
                {{--<div class="alert alert-warning" style="text-align: center">--}}
                    {{--<h5>--}}
                        {{--ملاحظة : اذا كنت لا تريد الا اضافة سعر اساسي فقط ولا تريد اضافة اسعار خاصة او عروض خاصه حاليا فيجب ترك الحقول الخاصه بالعروض الخاص فارغه--}}
                    {{--</h5>--}}
                {{--</div>--}}
            </div>

            <form id="save_form" action="<?=url("/admin/product/save_branch_product_prices/$branch_data->user_id/$product_data->pro_id")?>" method="POST" enctype="multipart/form-data">

                <?php

                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    echo $dump;
                }


                if (isset($success)&&!empty($success)) {
                    echo $success;
                }
                ?>

                <div class="panel panel-info">
                    <div class="panel-heading">السعر الاساسي</div>
                    <div class="panel-body">
                        <div class="row">
                            <?php


                            $normal_tags=array("original_pro_price_id","original_pro_price_label","original_pro_price_price","original_pro_price_quantity");
                            $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $lang_data=$original_price_data,
                                    "yes",
                                    $required = ""
                            );

                            $attrs[0]["original_pro_price_id"]="";
                            $attrs[0]["original_pro_price_label"]="مسمي السعر";
//                            $attrs[0]["original_pro_price_quantity"]="Product Quantity";
                            $attrs[0]["original_pro_price_quantity"]="";
                            $attrs[0]["original_pro_price_price"]="السعر";

                            $attrs[2]["original_pro_price_quantity"]="required";
                            $attrs[2]["original_pro_price_price"]="required";


                            $attrs[3]["original_pro_price_id"]="hidden";
                            $attrs[3]["original_pro_price_label"]="text";
                            $attrs[3]["original_pro_price_quantity"]="hidden";
                            $attrs[3]["original_pro_price_price"]="number";

                            if (is_object($original_price_data) && !empty($original_price_data))
                            {
                                $attrs[4]["original_pro_price_id"]=$original_price_data->pro_price_id;
                                $attrs[4]["original_pro_price_label"]=$original_price_data->pro_price_label;
                                $attrs[4]["original_pro_price_quantity"]=$original_price_data->pro_price_quantity;
                                $attrs[4]["original_pro_price_price"]=$original_price_data->pro_price_price;
                            }


                            echo
                            generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                            );


                            ?>

                        </div>

                    </div>

                    <?php if(1!=1): ?>

                    <div class="bill_orders_containers">

                        <?php if(is_array($product_promotions) && count($product_promotions)): ?>

                        <?php foreach($product_promotions as $key => $value): ?>

                        <div class="panel panel-primary bill_order_item">
                            <a href="" class="btn btn-danger pull-right delete_order_item"> X </a>
                            <div class="panel-heading show_collapse_hand collapse_action">
                                سعر خاص او عرض خاص للمنتج
                            </div>
                            <div class="panel-body collapse_body collapse">

                                <?php


                                $normal_tags=array("pro_price_id[]","pro_price_label[]","pro_price_price[]","pro_price_from[]","pro_price_to[]","pro_price_quantity[]");
                                $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data="",
                                        "yes",
                                        $required = ""
                                );

                                $attrs[0]["pro_price_id[]"]="";
                                $attrs[4]["pro_price_id[]"]=$value->pro_price_id;

                                $attrs[0]["pro_price_label[]"]="مسمي السعر";
                                $attrs[4]["pro_price_label[]"]=$value->pro_price_label;

//                                $attrs[0]["pro_price_quantity[]"]="Product Quantity";
                                $attrs[0]["pro_price_quantity[]"]="";
                                $attrs[4]["pro_price_quantity[]"]=$value->pro_price_quantity;

                                $attrs[0]["pro_price_price[]"]="السعر";
                                $attrs[4]["pro_price_price[]"]=$value->pro_price_price;

                                $attrs[0]["pro_price_from[]"]="التاريخ من :";
                                $attrs[4]["pro_price_from[]"]=$value->pro_price_from;

                                $attrs[0]["pro_price_to[]"]="التاريخ الي : ";
                                $attrs[4]["pro_price_to[]"]=$value->pro_price_to;


                                $attrs[3]["pro_price_id[]"]="hidden";
                                $attrs[3]["pro_price_label[]"]="text";
                                $attrs[3]["pro_price_quantity[]"]="hidden";
                                $attrs[3]["pro_price_price[]"]="number";
                                $attrs[3]["pro_price_from[]"]="date";
                                $attrs[3]["pro_price_to[]"]="date";

                                $attrs[5]["pro_price_id[]"].=" new_pro_price_id";

                                echo
                                generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                );

                                ?>

                            </div>

                        </div>

                        <?php endforeach; ?>

                        <?php else: ?>
                        <div class="panel panel-primary bill_order_item">
                            <a href="" class="btn btn-danger pull-right delete_order_item"> X </a>
                            <div class="panel-heading show_collapse_hand collapse_action">
                                سعر خاص او عرض خاص للمنتج

                            </div>
                            <div class="panel-body collapse_body collapse">

                                <?php

                                $normal_tags=array("pro_price_id[]","pro_price_label[]","pro_price_quantity[]","pro_price_price[]","pro_price_from[]","pro_price_to[]");
                                $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $lang_data="",
                                        "yes",
                                        $required = ""
                                );

                                $attrs[0]["pro_price_id[]"]="";
                                $attrs[0]["pro_price_label[]"]="مسمي السعر";
                                $attrs[0]["pro_price_quantity[]"]="كمية المنتج";
                                $attrs[0]["pro_price_price[]"]="السعر";
                                $attrs[0]["pro_price_from[]"]="التاريخ من :";
                                $attrs[0]["pro_price_to[]"]="التاريخ الي :";


                                $attrs[3]["pro_price_id[]"]="hidden";
                                $attrs[3]["pro_price_label[]"]="text";
                                $attrs[3]["pro_price_quantity[]"]="number";
                                $attrs[3]["pro_price_price[]"]="number";
                                $attrs[3]["pro_price_from[]"]="date";
                                $attrs[3]["pro_price_to[]"]="date";

                                echo
                                generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5])
                                );

                                ?>

                            </div>

                        </div>

                        <?php endif; ?>

                    </div>

                    <?php endif; ?>

                    {{csrf_field()}}
                    {{--<a href="#" class="col-md-3 col-md-offset-1 btn btn-info custom_clone_order_item"><i class="fa fa-plus"></i> عرض جديد اخر</a>--}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-3 col-md-offset-1 btn btn-primary btn-lg">
            </form>

        </div>

    </div>


@endsection