@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="{{url("/admin/product/products_on_stock")}}" method="get">
                <div class="form-group">
                    <label for="">البحث بواسطة الباركود او اسم المنتج</label>
                    <input type="text" class="form-control" name="search_product" value="{{$search_product}}">
                </div>
                <button class="btn btn-primary" type="submit">ابحث</button>

            </form>
        </div>
    </div>
    
    <div class="panel panel-info">
        <div class="panel-heading">المنتجات في المخزن</div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>صورة المنتج</td>
                    <td>اسم المنتج</td>
                    <td>كمية المنتج</td>
                    <td>حد الطلب</td>
                    <td>وحدة القياس</td>
                    <td>الباركود</td>
                    <?php if(check_permission($user_permissions,"product/products_on_stock","edit_action",$current_user)): ?>

                    <td>تعديل</td>
                    <?php endif; ?>
                    <?php if(1!=1&&check_permission($user_permissions,"product/products_on_stock","delete_action",$current_user)): ?>
                    <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </thead>


                <tbody id="sortable">
                <?php foreach ($all_products as $key => $product): ?>
                <tr id="row<?= $product->pro_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        <img width="100" src="{{get_image_or_default($product->pro_img_path)}}">
                    </td>
                    <td>
                        <span class="label label-info"><?= $product->pro_name ?></span>
                    </td>
                    <td>
                        <span class="label label-default">
                            <?= $product->pro_quantity ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-danger">
                            <?= $product->pro_limit ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            <?= $product->pro_unit_name ?>
                        </span>
                    </td>
                    <td>
                        <?php if(!empty($product->pro_barcode)): ?>
                        <span class="label label-default">
                        <?= $product->pro_barcode ?>
                    </span>
                        <?php endif; ?>
                    </td>

                    <?php if(check_permission($user_permissions,"product/products_on_stock","edit_action",$current_user)): ?>
                    <td>
                        <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_products_on_stock/'."$product->cat_id/".$product->pro_id)}}"> تعديل </a>
                    </td>
                    <?php endif; ?>

                    <?php if(1!=1&&check_permission($user_permissions,"product/products_on_stock","delete_action",$current_user)): ?>
                    <td>
                        <a href='#' class="general_remove_item" data-tablename="App\models\product\products_m" data-deleteurl="<?= url("/admin/product/remove_product") ?>" data-itemid="<?= $product->pro_id ?>">
                    <span class="label label-danger">
                        مسح <i class="fa fa-times"></i>
                    </span>
                        </a>
                    </td>
                    <?php endif; ?>


                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <div class="col-md-12" style="text-align: center;">
                {{$products_pagination->appends(\Illuminate\Support\Facades\Input::except('page'))}}
            </div>

            <div class="col-md-12" style="text-align: center;">
                <a class="btn btn-primary " href="{{url('/common/category/show_tree')}}"> شجرة التصنيفات </a>
            </div>

        </div>
    </div>







@endsection