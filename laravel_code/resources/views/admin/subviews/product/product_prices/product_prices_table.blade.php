<?php if(isset_and_array($all_products)): ?>

    <table class="table table-responsive table-hover">

        <thead>
            <tr>
                <td>اسم المنتج</td>
                <?php foreach($all_price_list_items as $key=>$price): ?>
                <td>
                    <div class="col-md-12">
                        {{$price->price_title}}
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="">يمكنك ان تضع سعر هنا وسوف يكتب تلقائيا في جميع المدخلات في نفس العمود</label>
                        <input type="text" class="form-control general_input" data-inputs="price_val_{{$price->price_id}}[]">
                    </div>

                </td>
                <?php endforeach;?>
            </tr>
        </thead>


        <tbody>

            <?php foreach($all_products as $key=>$pro): ?>
                <tr>
                    <td>
                        <input type="hidden" name="pro_ids[]" value="{{$pro->pro_id}}">
                        {{$pro->pro_name}}
                    </td>
                    <?php foreach($all_price_list_items as $key=>$price): ?>
                        <?php
                            $pro_price=0;

                            if(isset($old_prices_values[$pro->pro_id])&&count($old_prices_values[$pro->pro_id])){
                                $prices=$old_prices_values[$pro->pro_id]->groupBy("price_id")->all();

                                if(isset($prices[$price->price_id])){
                                    $pro_price=$prices[$price->price_id]->all()[0]->ppv_value;
                                }
                            }
                        ?>

                        <td>
                            <input type="number" step="0.01" class="form-control" name="price_val_{{$price->price_id}}[]" value="{{$pro_price}}">
                        </td>
                    <?php endforeach;?>
                </tr>
            <?php endforeach;?>


        </tbody>



    </table>


<?php endif; ?>