@extends('admin.main_layout')

@section('subview')

    @include("common.category.accounts_tree.tree_modal")


    <div class="panel panel-primary add_product_prices">
        <div class="panel-heading">
            تحديد اسعار المنتجات
        </div>
        <div class="panel-body">

            <div class="panel panel-info">
                <div class="panel-heading"> تحديد التصنيفات</div>
                <div class="panel-body">

                    <div class="alert alert-info" style="text-align: center;">
                        يجب ان تختار التصنيفات لكي يعرض لك المنتجات الموجود بداخلها لتضع الاسعار
                    </div>

                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="multi"
                            data-div_to_show_selection=".select_cats"
                            data-input_field_name="cat_ids"
                    >
                        اظهر الشجرة لتختار التصنيفات
                    </button>


                    <div class="select_cats" style="text-align: center;">

                    </div>

                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    اضافة|تعديل الاسعار
                </div>
                <div class="panel-body" style="overflow-x: scroll;">

                    <div class="col-md-12" style="text-align: center;">
                        <button type="button" class="btn btn-primary load_products">تحميل المنتجات</button>
                    </div>

                    <form action="{{url("/admin/product/set_products_prices")}}" method="post">
                        <div class="load_products_div">

                        </div>


                        <div class="col-md-12" style="text-align: center;">
                            {!! csrf_field() !!}
                            <input id="submit"  type="submit" value="حفظ" disabled class="btn btn-primary btn-lg form_submit_btn" style="margin-top: 5px;">
                        </div>

                    </form>



                </div>
            </div>










        </div>
    </div>


@endsection