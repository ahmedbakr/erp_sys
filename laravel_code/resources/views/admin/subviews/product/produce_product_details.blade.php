@extends('admin.main_layout')

@section('subview')

    <div class="alert alert-info" style="text-align: center">
        <h3>
تفاصيل إنتاج
    {{$produce_quantity}}
            منتج
            من
            {{$get_product_components[0]->pro_name}}
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>إسم الماده الخام</td>
            <td>كمية الماده الخام فالمخزن</td>
            <td>الكمية لانتاج منتج واحد</td>
            <td>كمية الماده الخام لانتاج المنتج</td>
            <td>التكلفة لإنتاج منتج واحد</td>
            <td>تكلفة المنتج</td>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($get_product_components as $key => $material): ?>

        <?php
            $bg_color = "";
            if (($produce_quantity * $material->pro_comp_amount) > $material->mat_amount)
            {
                $bg_color = "background-color: brown;";
            }
        ?>

        <tr style="{{$bg_color}}">
            <td><?=$key+1?></td>
            <td>
                <span class="label label-info"><?= $material->mat_name ?></span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $material->mat_amount ?>
                </span>
                <span class="label label-success"><?= $material->mat_type ?></span>
            </td>
            <td>
                <span class="label label-info"><?= $material->pro_comp_amount ?></span>
                <span class="label label-success"><?= $material->mat_type ?></span>
            </td>
            <td>
                <?php if(($produce_quantity * $material->pro_comp_amount) > $material->mat_amount): ?>
                    <span class="label label-danger">
                    <?php else: ?>
                    <span class="label label-default">
                <?php endif; ?>
                    {{($produce_quantity * $material->pro_comp_amount)}}
                </span>
            </td>
            <td>
                <span class="label label-default">
                    {{$material->mat_price}}
                </span>
            </td>
            <td>
                <span class="label label-info">
                    {{($produce_quantity * $material->mat_price)}}
                </span>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

@endsection