@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-primary">
        <div class="panel-heading">الفلترة</div>
        <div class="panel-body">
            <form action="" method="get">

                <?php

                $branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
                $branches_values = convert_inside_obj_to_arr($all_branches,"user_id");

                if ($current_user->user_type == "admin")
                {
                    $branches_text = array_merge(["الفرع الرئيسي"],convert_inside_obj_to_arr($all_branches,"full_name"));
                    $branches_values = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
                }

                echo
                generate_select_tags(
                    $field_name="branch_id",
                    $label_name="إختار الفرع",
                    $text=$branches_text,
                    $values=$branches_values,
                    $selected_value=[""],
                    $class="form-control select_2_class",
                    $multiple="",
                    $required = "",
                    $disabled = "",
                    $data = $all_post_data,
                    "4"
                );
                ?>

                <div class="form-group col-md-4">
                    <label for="">من</label>
                    <input type="date" name="start_date" class="form-control" value="{{(isset($all_post_data->start_date))?$all_post_data->start_date:""}}">
                </div>

                <div class="form-group col-md-4">
                    <label for="">الي</label>
                    <input type="date" name="end_date" class="form-control" value="{{(isset($all_post_data->end_date))?$all_post_data->end_date:""}}">
                </div>


                <button class="btn btn-primary">البحث</button>

            </form>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">المنتجات الهالكه</div>
        <div class="panel-body">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم المنتج</td>
                    <td>كمية الهالك</td>
                    <td>التاريخ</td>
                </tr>
                </thead>


                <tbody id="sortable">
                <?php foreach ($broken_products as $key => $product): ?>
                <tr>
                    <td><?=$key+1?></td>
                    <td>
                <span class="label label-info">
                    <?= $product->pro_name ?>
                </span>
                    </td>
                    <td>
                        <span class="label label-info"><?= $product->b_p_quantity ?></span>
                    </td>
                    <td>
                        <span class="label label-info">{{$product->b_p_date}}</span>
                    </td>

                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"product/broken_products_on_stock","add_action",$current_user)): ?>
            <div class="col-md-4 col-md-offset-4">
                <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_broken_products_on_stock')}}"> إضافة منتج هالك جديد </a>
            </div>
            <?php endif; ?>

        </div>
    </div>



@endsection