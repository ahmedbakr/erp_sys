@extends('admin.main_layout')

@section('subview')

    <div class="alert alert-info" style="text-align: center">
        <h3>
طلبات المنتجات ليتم إنتاجها
        </h3>
    </div>

    <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>#</td>
            <td>صورة المنتج</td>
            <td>اسم المنتج</td>
            <td>الكمية في المخزن</td>
            <td>الكمية المراد إنتاجها</td>
            <td>حد الطلب</td>
            <td>الباركود</td>
            <td>تاريخ الطلب</td>
            <td>تفاصيل الإنتاج</td>
            <td>حالة الطلب</td>
            <td>مسح الطلب</td>
        </tr>
        </thead>


        <tbody id="sortable">
        <?php foreach ($products as $key => $product): ?>
        <tr id="row<?= $product->ppr_id ?>">
            <td><?=$key+1?></td>
            <td>
                <img width="100" src="{{get_image_or_default($product->pro_img_path)}}">
            </td>
            <td>
                <span class="label label-info"><?= $product->pro_name ?></span>
            </td>
            <td>
                <span class="label label-default">
                    <?= $product->pro_quantity ?>
                </span>
            </td>
            <td>
                <span class="label label-info">
                    <?= $product->produce_quantity ?>
                </span>
            </td>
            <td>
                <span class="label label-danger">
                    <?= $product->pro_limit ?>
                </span>
            </td>
            <td>
                <?php if(!empty($product->pro_barcode)): ?>
                    <span class="label label-default">
                        <?= $product->pro_barcode ?>
                    </span>
                <?php endif; ?>
            </td>
            <td>
                <span class="label label-info">{{$product->produce_request_date}}</span>
                <span class="label label-success">
                    {{\Carbon\Carbon::createFromTimestamp(strtotime($product->produce_request_date))->diffForHumans()}}
                </span>

            </td>

            <td>
                <a class="btn btn-primary btn-block" href="{{url('/admin/product/produce_product_details/'.$product->ppr_id)}}"> مشاهدة </a>
            </td>

            <td>
                <?php if(false): ?>
                    <span class="label label-danger">الكمية غير متاحة

                    <?php else: ?>
                    <?php if($product->is_produced == 1): ?>
                        <span class="label label-success">تم الإنتاج بنجاح</span>
                        <?php else: ?>
                        <?php if(check_permission($user_permissions,"product/review_produce_products","add_action",$current_user)): ?>
                            <?php
                                echo
                                generate_multi_accepters(
                                    $accepturl=url("/admin/product/change_produce_product_status"),
                                    $item_obj=$product,
                                    $item_primary_col="ppr_id",
                                    $accept_or_refuse_col="is_produced",
                                    $model='App\models\product\produce_product_review_m',
                                    $accepters_data=["0"=>"غير موافق","1"=>"موافق"]
                                );
                            ?>
                            <?php else: ?>
                            <span class="label label-danger">غير مسموح</span>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td>
                <?php if(check_permission($user_permissions,"product/review_produce_products","delete_action",$current_user)): ?>
                    <a href='#' class="general_remove_item" data-tablename="App\models\product\produce_product_review_m" data-deleteurl="<?= url("/admin/product/remove_product_produce_product_request") ?>" data-itemid="<?=$product->ppr_id ?>">
                        <span class="label label-danger">
                            مسح <i class="fa fa-times"></i>
                        </span>
                    </a>
                <?php endif; ?>
            </td>

        </tr>
        <?php endforeach ?>
        </tbody>

    </table>

    <?php if(check_permission($user_permissions,"product/products_on_stock","add_action",$current_user)): ?>
    <div class="col-md-3 col-md-offset-4">
        <a class="btn btn-primary btn-block" href="{{url('/admin/product/save_products_on_stock')}}"> إضافة منتج جديد </a>
    </div>
    <?php endif; ?>


@endsection