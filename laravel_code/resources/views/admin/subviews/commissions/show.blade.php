@extends('admin.main_layout')

@section('subview')


    <?php if(check_permission($user_permissions,"admin/commission_rules","show_action",$current_user)): ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
       قواعد العمولات
        </div>
        <div class="panel-body">

            <div class="col-md-3 col-md-offset-4">
                <a class="btn btn-info" href="{{url("/admin/commissions/show_rules")}}">اضافة ومشاهدة قواعد العمولات</a>
            </div>

        </div>
    </div>


    <?php endif; ?>


    <?php if(is_array($all_users) && count($all_users)): ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <div class="col-md-4 col-md-offset-4">

                <?php

                    $text_arr = convert_inside_obj_to_arr($all_users,"full_name");
                    $values_arr = convert_inside_obj_to_arr($all_users,"user_id");

                    if ($current_user->user_type == "admin")
                    {
                        $text_arr = array_merge(["الكل"],convert_inside_obj_to_arr($all_users,"full_name"));
                        $values_arr = array_merge([0],convert_inside_obj_to_arr($all_users,"user_id"));
                    }

                echo
                generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$text_arr,
                        $values=$values_arr,
                        $selected_value=array($selected_user),
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                );
                ?>

            </div>

            <div class="col-md-12">

                <div class="col-md-6 form-group">
                    <label for="">من التاريخ</label>
                    <input type="date" class="form-control" id="from_date_id" value="{{$start_date}}">
                </div>

                <div class="col-md-6 form-group">
                    <label for="">الي التاريخ</label>
                    <input type="date" class="form-control" id="to_date_id" value="{{$end_date}}">
                </div>

            </div>

            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                    <button class="btn btn-info show_commission_filter">ابحث</button>
                </div>
            </div>

        </div>
    </div>

    <?php endif; ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            عمولات الفروع
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="text-align: center;">
                <button type="button" class="btn btn-primary change_all_commissions">تصفية كل العمولات المعروضة في الجدول</button>
            </div>


            <table id="cat_table_1" class="table table-striped table-bordered table_without_paging" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اليوم</td>
                    <td>مبيعات الفرع(مجموع المبيعات/عدد المبيعات)</td>
                    <td>عمولة الفرع</td>
                    <td>عمولة الادارة</td>
                    <td>العمولة الكلية</td>
                    <td>تفاصيل العمولة</td>
                    <?php if(check_permission($user_permissions,"admin/commission","receive_commission",$current_user)): ?>
                        <td>تصفية العمولة</td>
                    <?php endif; ?>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>اليوم</td>
                    <td>مبيعات الفرع(مجموع المبيعات/عدد المبيعات)</td>
                    <td>عمولة الفرع</td>
                    <td>عمولة الادارة</td>
                    <td>العمولة الكلية</td>
                    <td>تفاصيل العمولة</td>
                    <?php if(check_permission($user_permissions,"admin/commission","receive_commission",$current_user)): ?>
                    <td>تصفية العمولة</td>
                    <?php endif; ?>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_commissions as $key => $commission): ?>

                <tr id="row<?= $commission->day_commission_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        {{$commission->full_name}}
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$commission->day_date}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-default">
                            {{$commission->total_day_bills_money}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$commission->branch_commission_value}}
                        </span>
                    </td>

                    <td>
                        <span class="label label-success">
                            {{$commission->factory_commission_value}}
                        </span>
                    </td>

                    <td>
                        <span class="label label-success">
                            {{$commission->total_commission_value}}
                        </span>
                    </td>


                    <td>
                        <a href="{{url('/admin/commissions/show_rules/'.$commission->commission_id)}}"><span class="label label-info"> مشاهدة<i class="fa fa-cloud-upload"></i></span></a>
                    </td>
                    <?php if(check_permission($user_permissions,"admin/commission","receive_commission",$current_user)): ?>
                    <td>

                        <?php if($commission->is_received == 0): ?>

                            <?php
                                echo
                                generate_multi_accepters(
                                    $accepturl=url( "/admin/commissions/change_commission_received"),
                                    $item_obj=$commission,
                                    $item_primary_col="day_commission_id",
                                    $accept_or_refuse_col="is_received",
                                    $model='App\models\commission\day_commission_m',
                                    $accepters_data=["0"=>"لا","1"=>"نعم"],
                                    "1"
                                );
                            ?>

                        <?php else: ?>
                            <span class="label label-success">مستلمه بالفعل</span>
                        <?php endif; ?>

                    </td>
                    <?php endif; ?>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/commission","delete_action",$current_user)): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/commissions/remove_day_commission") ?>" data-tablename="App\models\commission\day_commission_m"  data-itemid="<?= $commission->day_commission_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

        </div>
    </div>




@endsection
