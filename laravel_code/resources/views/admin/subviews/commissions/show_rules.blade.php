@extends('admin.main_layout')

@section('subview')


    <?php if(is_array($all_branches) && count($all_branches)): ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url("admin/commissions/show_rules/$commission_id")}}" method="GET">
                <div class="col-md-4 col-md-offset-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                        $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                        $selected_value=(isset($_GET["branch_id"]))?[$_GET["branch_id"]]:"",
                        $class="form-control select_2_class select_branch",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <?php endif; ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            قواعد العمولات
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>نوع العمولة</td>
                    <td>قيمة محصلة البيع/عدد المنتجات المباعة</td>
                    <td>عموله الفرع</td>
                    <td>عموله الادارة</td>
                    <td>مجموع العموله</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>نوع العمولة</td>
                    <td>قيمة محصلة البيع/عدد المنتجات المباعة</td>
                    <td>عموله الفرع</td>
                    <td>عموله الادارة</td>
                    <td>مجموع العموله</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($commission_rules as $key => $commission_rule): ?>

                <tr id="row<?= $commission_rule->commission_id ?>">
                    <td><?=$key+1?></td>
                    <td>
                        <span class="label label-default">
                            {{$commission_rule->full_name}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-default">
                            {{($commission_rule->commission_rule_type=="0")?"علي طريقة تجميع الربح":"علي طريقة تجميع العدد علي اساس منتجات"}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$commission_rule->minimum_day_earn}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$commission_rule->branch_commission_value}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$commission_rule->factory_commission_value}}
                        </span>
                    </td>
                    <td>
                        <span class="label label-success">
                            {{$commission_rule->total_commission_value}}
                        </span>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/commission_rules","edit_action",$current_user)): ?>
                        <a href="<?= url("admin/commissions/save_commission_rule/$commission_rule->commission_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/commission_rules","delete_action",$current_user)): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("admin/commissions/remove_commission_rule") ?>" data-tablename="App\models\commission\commission_rules_m"  data-itemid="<?= $commission_rule->commission_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <?php if(check_permission($user_permissions,"admin/commission_rules","add_action",$current_user)): ?>
            <a href="<?= url("admin/commissions/save_commission_rule") ?>" class="btn btn-lg btn-primary">إضافة قاعدة جديدة</a>
            <?php endif; ?>

        </div>
    </div>




@endsection
