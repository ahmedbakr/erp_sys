@extends('admin.main_layout')


@section('subview')

    @include("common.category.accounts_tree.tree_modal")

    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة قاعده عمولة جديدة";
    $commission_id="";

    if ($commission_rule!="") {
        $header_text="تعديل قاعده عمولة رقم ".$commission_rule->commission_id;

        $commission_id=$commission_rule->commission_id;
    }

    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">


            <form id="save_form" action="<?=url("admin/commissions/save_commission_rule/$commission_id")?>" method="POST" enctype="multipart/form-data">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        البيانات الاساسية
                    </div>
                    <div class="panel-body">

                        <?php
                            echo generate_select_tags(
                                $field_name="commission_rule_type",
                                $label_name="اختار نوع القاعدة *",
                                $text=["علي طريقة تجميع الربح","علي طريقة تجميع العدد علي اساس منتجات"],
                                $values=["0","1"],
                                $selected_value="",
                                $class="form-control",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = $commission_rule,
                                $grid="6"
                            );
                        ?>

                        <?php
                            echo generate_select_tags(
                                $field_name="branch_id",
                                $label_name="إختار الفرع *",
                                $text=convert_inside_obj_to_arr($all_branches,"full_name$en"),
                                $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                $selected_value="",
                                $class="form-control",
                                $multiple="",
                                $required="",
                                $disabled = "",
                                $data = $commission_rule,
                                $grid="6"
                            );
                        ?>

                        <?php
                            $normal_tags=array('minimum_day_earn','branch_commission_value','factory_commission_value');
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $commission_rule,
                                "yes",
                                $required="required"
                            );


                            $attrs[0]["minimum_day_earn"]="قيمة محصلة البيع لاخذ عمولة/عدد المنتجات المباعة لاخد عمولة *";
                            $attrs[0]["branch_commission_value"]="عمولة الفرع *";
                            $attrs[0]["factory_commission_value"]="عمولة الإدارة *";


                            $attrs[3]["minimum_day_earn"]="number";
                            $attrs[3]["branch_commission_value"]="number";
                            $attrs[3]["factory_commission_value"]="number";

                            $attrs[6]["minimum_day_earn"]="4";
                            $attrs[6]["branch_commission_value"]="4";
                            $attrs[6]["factory_commission_value"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );
                        ?>


                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        اختار التصنيف للطريقة الثانية
                    </div>
                    <div class="panel-body">

                        <div class="alert alert-info" style="text-align: center;">
                            <p>يجب ان تختار التصنيفات لكي يعرض لك المنتجات الموجود بداخلها</p>
                            <p>عند اختيار تصنيف السيستم بشكل تلقائي هيجيب كل الاصناف المباشرة فقط تحت هذا التصنيف </p>
                        </div>

                        <button
                                type="button"
                                class="btn btn-primary show_tree_modal center-block"
                                data-select_what="category"
                                data-select_type="single"
                                data-div_to_show_selection=".select_cats"
                                data-input_field_name="commission_rule_cat_ids"
                        >
                            اظهر الشجرة لتختار التصنيفات
                        </button>


                        <div class="select_cats" style="text-align: center;">
                            <?php if(is_object($commission_rule)&&isset($commission_rule->commission_rule_cats)): ?>
                            <?php foreach($commission_rule->commission_rule_cats as $key=>$cat): ?>
                            <label class="label label-primary selected_individual_item" data-item_id="{{$cat->cat_id}}">
                                {{$cat->cat_name}}
                                <a href="#" class="remove_selected_individual_item">×</a>
                                <input type="hidden" name="commission_rule_cat_ids" value="{{$cat->cat_id}}">
                            </label>
                            <?php endforeach;?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>

                <?php if($commission_rule==""): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        طبق العرض علي فروع اخري
                    </div>
                    <div class="panel-body">
                        <?php
                        echo generate_select_tags(
                            $field_name="other_branches",
                            $label_name="اختار الفروع الاخري",
                            $text=convert_inside_obj_to_arr($all_branches,"full_name$en"),
                            $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="multiple",
                            $required="",
                            $disabled = "",
                            $data = "",
                            $grid="12"
                        );
                        ?>
                    </div>
                </div>
                <?php endif; ?>






                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4  btn btn-primary btn-lg">

            </form>


        </div>
    </div>


@endsection



