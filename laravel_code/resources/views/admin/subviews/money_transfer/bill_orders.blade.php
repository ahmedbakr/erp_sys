
<?php

    $total = 0;
    $remained = 0;
    $gained = 0;

?>

<div class="panel panel-info">
    <div class="panel-heading">{{$header}}</div>
    <div class="panel-body">
        <div class="row">

            <table class="table table-striped">

                <th>#</th>
                <th>مبلغ الطلب</th>
                <th>نوع المبلغ</th>
                <th>حالة الطلب</th>
                <th>الوصف</th>
                <th>التاريخ</th>
                <th>تنفيذ</th>

                <?php foreach($orders as $key => $order): ?>

                <tr>
                    <td>{{$key+1}}</td>
                    <td>
                        <span class="label label-default">
                            <?= $order->m_t_amount ?>
                            <?php $total += $order->m_t_amount; ?>
                        </span>
                    </td>
                    <td>
                        <?php
                            if($order->m_t_amount_type=="cash"){
                                echo "كاش";
                            }
                            elseif($order->m_t_amount_type=="atm"){
                                echo "شبكة";
                            }

                        ?>

                    </td>
                    <td>
                        <?php if($order->m_t_status == 1): ?>
                            <span class="label label-info">تم الاستلام</span>
                            <?php $gained += $order->m_t_amount; ?>
                            <?php else: ?>
                            <span class="label label-danger">لم يتم الاستلام</span>
                            <?php $remained += $order->m_t_amount; ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <span class="label label-default">
                            <?= $order->m_t_desc ?>
                        </span>
                    </td>
                    <td>
                        <span class="label label-info">
                            {{$order->created_at}}
                        </span>
                        <span class="label label-success">
                            {{Carbon\Carbon::createFromTimestamp(strtotime($order->created_at))->diffForHumans()}}
                        </span>
                    </td>
                    <td>
                        <?php if($order->m_t_status == 0): ?>

                            <?php
//                            echo
//                            generate_multi_accepters(
//                                $accepturl=url( "/admin/money_transfer/change_bill_orders_money_status"),
//                                $item_obj=$order,
//                                $item_primary_col="m_t_id",
//                                $accept_or_refuse_col="m_t_status",
//                                $model='App\models\money_transfers_m',
//                                $accepters_data=["0"=>"لا","1"=>"$status_label"]
//                            );
                            ?>


                            <input type="hidden" class="max_value" value="{{$order->m_t_amount}}">

                            <input type="number" placeholder="كاش" class="money_transfer_cash form-control">
                            <br>
                            <?php if($allow_atm): ?>
                                <input type="number" placeholder="شبكة" class="money_transfer_atm form-control">
                            <?php endif; ?>
                            <p class="money_transfer_msgs"></p>

                            <button data-money_transfer_id="{{$order->m_t_id}}" class="btn btn-primary make_transfer_money">{{$status_label}}</button>

                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach; ?>

            </table>

            <div class="alert alert-info" style="height: 57px">

                <div class="col-md-4">
                    <b>المبلغ الاجمالي :</b> <span class="label label-info">{{$total}}</span>
                </div>

                <div class="col-md-4">
                    <b>إجمالي المستلم :</b> <span class="label label-info">{{$gained}}</span>
                </div>

                <div class="col-md-4">
                    <b>المبلغ المتبقي :</b> <span class="label label-info">{{$remained}}</span>
                </div>

            </div>

        </div>
    </div>
</div>
<br><br>