@extends('admin.main_layout')

@section('subview')

    <div class="container">



        <?php if(is_array($suppliers) && count($suppliers)): ?>


            <div class="alert alert-success" style="text-align: center">
                <h1 style="color: #00ACED">
                    التحويلات المالية للنظام
                </h1>
                <br>
                <div class="row">

                    <div class="col-md-3 col-md-offset-2">
                        <?php
                        echo generate_select_tags(
                                $field_name="user_id",
                                $label_name="إختار المورد",
                                $text=convert_inside_obj_to_arr($suppliers,"full_name"),
                                $values=convert_inside_obj_to_arr($suppliers,"user_id"),
                                $selected_value=array($supplier_id),
                                $class="form-control select_supplier",
                                $multiple = "",
                                $required = "required",
                                $disabled = "");
                        ?>
                    </div>

                    <div class="col-md-3">
                        <label for="">أدخل رقم الفاتورة</label>
                        <input type="number" class="form-control get_transfers_bill_number" value="{{$bill_id}}">
                    </div>

                    <div class="col-md-2">
                        <br>
                        <button type="button" class="btn btn-primary btn-lg get_transfers">
                            احصل علي النتائج
                        </button>
                    </div>

                </div>
            </div>

            <div class="row show_bill_transfers_body">

            </div>

        <?php else: ?>
        {!! get_warning(url('/admin/dashboard'),"لا يوجد لديك موردين حاليين") !!}

        <?php endif; ?>

    </div>


@endsection