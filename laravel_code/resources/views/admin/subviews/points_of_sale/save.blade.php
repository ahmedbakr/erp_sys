@extends('admin.main_layout')

@section('subview')

    <div class="row">
        <div class="col-md-12">

            <style>
                hr{
                    width: 100%;
                    height:1px;
                }
            </style>
            <?php

            if (count($errors->all()) > 0)
            {
                $dump = "<div class='alert alert-danger'>";
                foreach ($errors->all() as $key => $error)
                {
                    $dump .= $error." <br>";
                }
                $dump .= "</div>";

                echo $dump;
            }

            if (isset($success)&&!empty($success)) {
                echo $success;
            }
            ?>


            <?php

            $header_text="تسجيل نقطة بيع جديدة";

            $id = "";
            $set_cash_on_my_balance_checked = "";
            $is_exist_password_for_return_checked = "";
            if ($point_data != "")
            {
                $header_text = "تعديل ".$point_data->point_of_sale_desc;
                $id = $point_data->point_of_sale_id;
                if ($point_data->set_cash_on_my_balance == 1)
                {
                    $set_cash_on_my_balance_checked = "checked";
                }
                if ($point_data->is_exist_password_for_return == 1)
                {
                    $is_exist_password_for_return_checked = "checked";
                }
            }

            ?>


            <div class="panel panel-info">
                <div class="panel-heading"><h2><?=$header_text?></h2></div>
                <div class="panel-body">
                    <div class="row">
                        <form id="save_form" action="<?=url("admin/points_of_sale/save/".$id)?>" method="POST" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <?php
                                    echo generate_select_tags(
                                        $field_name="branch_id",
                                        $label_name="إختار الفرع",
                                        $text=array_merge(convert_inside_obj_to_arr($all_branches,"full_name")),
                                        $values=array_merge(convert_inside_obj_to_arr($all_branches,"user_id")),
                                        $selected_value=[""],
                                        $class="form-control",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = $point_data
                                    );
                                    ?>
                                </div>
                            </div>

                            <?php

                            $normal_tags=array("point_of_sale_code","point_of_sale_desc","point_of_sale_desc_en");

                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $user_data=$point_data,
                                "yes",
                                $required =""
                            );

                            $attrs[0]["point_of_sale_code"]="الكود *";
                            $attrs[0]["point_of_sale_desc"]="الوصف بالعربي *";
                            $attrs[0]["point_of_sale_desc_en"]="الوصف بالإنجليزي";

                            $attrs[2]["point_of_sale_code"]="required";
                            $attrs[2]["point_of_sale_desc"]="required";


                            $attrs[3]["point_of_sale_code"]="text";
                            $attrs[3]["point_of_sale_desc"]="textarea";
                            $attrs[3]["point_of_sale_desc_en"]="textarea";


                            $attrs[6]["point_of_sale_code"]="4";
                            $attrs[6]["point_of_sale_desc"]="4";
                            $attrs[6]["point_of_sale_desc_en"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );

                            ?>

                            <div class="row" style="visibility: hidden;">

                                <div class="col-md-4">
                                    <label for="set_cash_on_my_balance">درج النقدية</label>
                                    <input type="checkbox" id="set_cash_on_my_balance" class="form-control" name="set_cash_on_my_balance" {{$set_cash_on_my_balance_checked}} >
                                </div>
                                <div class="col-md-4">
                                    <label for="is_exist_password_for_return">كلمة سر مردودات المبيعات</label>
                                    <input type="checkbox" id="is_exist_password_for_return" class="form-control" name="is_exist_password_for_return" {{$is_exist_password_for_return_checked}} >
                                </div>
                                <div class="col-md-4">
                                    <label for="set_cash_on_my_balance">كلمة السر</label>
                                    <input type="password" id="set_cash_on_my_balance" class="form-control" name="return_password">
                                </div>

                            </div>


                            {{csrf_field()}}
                            <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary">
                        </form>

                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection
