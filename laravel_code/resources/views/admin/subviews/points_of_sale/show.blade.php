@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            فلتر النتائج حسب :
        </div>
        <div class="panel-body">

            <form action="{{url('admin/points_of_sale')}}" method="GET">
                <div class="col-md-4 col-md-offset-4">

                    <?php
                    echo
                    generate_select_tags(
                        $field_name="branch_id",
                        $label_name="إختار الفرع",
                        $text=$all_branches_text,
                        $values=$all_branches_val,
                        $selected_value=(isset($_GET["branch_id"])?[$_GET["branch_id"]]:[""]),
                        $class="form-control select_2_class",
                        $multiple="",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );
                    ?>

                </div>

                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                        <button type="submit" class="btn btn-info">ابحث</button>
                    </div>
                </div>
            </form>

        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
نقاط البيع
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>الكود</td>
                    <td>الوصف بالعربي</td>
                    <td>الوصف بالإنجليزي</td>
                    <?php if(check_permission($user_permissions,"admin/points_of_sale","edit_action",$current_user)): ?>
                        <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>اسم الفرع</td>
                    <td>الكود</td>
                    <td>الوصف بالعربي</td>
                    <td>الوصف بالإنجليزي</td>
                    <?php if(check_permission($user_permissions,"admin/points_of_sale","edit_action",$current_user)): ?>
                        <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($points as $key => $point): ?>
                <tr id="row<?= $point->point_of_sale_id ?>">
                    <td><?=$key+1?></td>
                    <td><?=$point->full_name ?></td>
                    <td><?=$point->point_of_sale_code ?></td>
                    <td><?=$point->point_of_sale_desc ?></td>
                    <td><?=$point->point_of_sale_desc_en ?></td>
                    <?php if(check_permission($user_permissions,"admin/points_of_sale","edit_action",$current_user)): ?>
                    <td>
                        <span class="label label-info">
                            <a href="{{url('admin/points_of_sale/save/'.$point->point_of_sale_id)}}">تعديل</a>
                        </span>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"admin/points_of_sale","add_action",$current_user)): ?>
            <div class="row">
                <a href="{{url('admin/points_of_sale/save')}}" class="btn btn-info col-md-3 col-md-offset-2">تسجيل نقطة بيع </a>
            </div>
            <?php endif; ?>

        </div>
    </div>


@endsection
