@extends('admin.main_layout')

@section('subview')

    <script src="{{url("/public_html/tinymce/tinymce.min.js")}}" type="text/javascript"></script>
    <script>
        tinymce.init({
            selector: '.tinymce'
        });
    </script>


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>



    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }


    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="اضف عرض جديد";
    $package_id="";

    if ($package_data!="")
    {
        $header_text="تعديل '".$package_data->package_name."'";
        $package_id=$package_data->package_id;
    }




    ?>

    @include("common.category.accounts_tree.tree_modal")


    <!--END new_editor-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">

            <?php if(is_array($all_products) && count($all_products)): ?>


            <div class="">
                <form id="save_form" action="<?=url("admin/packages/save/$package_id")?>" method="POST" enctype="multipart/form-data">

                    {{--main Data--}}
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            بيانات العرض
                        </div>
                        <div class="panel-body">

                            <?php
                                $normal_tags=array(
                                        "package_name","package_start_date",
                                        "package_end_date","min_products_to_get_offer"
                                );

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $package_data,
                                    "yes",
                                    "",
                                    "4"
                                );



                                $attrs[0]["package_name"]="اسم العرض";
                                $attrs[0]["package_start_date"]="تاريخ بداية العرض";
                                $attrs[0]["package_end_date"]="تاريخ نهاية العرض";
                                $attrs[0]["min_products_to_get_offer"]="اقل عدد من المنتجات لدخول العرض";


                                $attrs[3]["package_start_date"]="date";
                                $attrs[3]["package_end_date"]="date";
                                $attrs[3]["min_products_to_get_offer"]="number";

                                echo
                                generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5]),
                                    reformate_arr_without_keys($attrs[6])
                                );

                                echo generate_select_tags(
                                    $field_name="package_type",
                                    $label_name="نوع العرض",
                                    $text=["نسبة","قيمة"],
                                    $values=["0","1"],
                                    $selected_value=[""],
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = $package_data,
                                    $grid="4",
                                    $show_label_as_option=false
                                );


                            $normal_tags=array(
                                "discount_percentage_for_product"
                            );

                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $package_data,
                                "yes",
                                "",
                                "4"
                            );

                            $attrs[0]["discount_percentage_for_product"]="نسبة/قيمة الخصم ";

                            $attrs[3]["discount_percentage_for_product"]="number";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );


                            ?>




                        </div>
                    </div>

                    {{--Select pros--}}
                    <div class="panel panel-info">
                        <div class="panel-heading">المنتجات الموجودة في العرض</div>
                        <div class="panel-body">
                            <?php

//                                echo
//                                generate_select_tags(
//                                    $field_name="package_select_pros",
//                                    $label_name="Select Products",
//                                    $text=convert_inside_obj_to_arr($all_products,"pro_name"),
//                                    $values=convert_inside_obj_to_arr($all_products,"pro_id"),
//                                    $selected_value=isset($package_data->package_pros)?convert_inside_obj_to_arr($package_data->package_pros,"pro_id"):"",
//                                    $class="form-control select_2_class",
//                                    $multiple="multiple",
//                                    $required = "",
//                                    $disabled = "",
//                                    $data = ""
//                                );
                            ?>


                                <div class="alert alert-info" style="text-align: center;">
                                    <p>يجب ان تختار التصنيفات لكي يعرض لك المنتجات الموجود بداخلها</p>
                                    <p>عند اختيار تصنيف السيستم بشكل تلقائي هيجيب كل الاصناف الموجودة تحته سواء كان تحته مباشرة او غير مباشرة</p>
                                </div>

                                <button
                                        type="button"
                                        class="btn btn-primary show_tree_modal center-block"
                                        data-select_what="category"
                                        data-select_type="multi"
                                        data-div_to_show_selection=".select_cats"
                                        data-input_field_name="package_cats"
                                >
                                    اظهر الشجرة لتختار التصنيفات
                                </button>


                                <div class="select_cats" style="text-align: center;">
                                    <?php if(is_object($package_data)&&count($package_data->package_cats)): ?>
                                        <?php foreach($package_data->package_cats as $key=>$cat): ?>
                                            <label class="label label-primary selected_individual_item" data-item_id="{{$cat->cat_id}}">
                                                {{$cat->cat_name}}
                                                <a href="#" class="remove_selected_individual_item">×</a>
                                                <input type="hidden" name="package_cats[]" value="{{$cat->cat_id}}">
                                            </label>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                </div>


                        </div>
                    </div>

                    {{--select branches--}}
                    <div class="panel panel-info">
                        <div class="panel-heading">الفروع المتوفر بها العرض</div>
                        <div class="panel-body">
                            <?php

                            echo
                            generate_select_tags(
                                $field_name="package_branches",
                                $label_name="اختار الفروع",
                                $text=convert_inside_obj_to_arr($all_branches,"full_name"),
                                $values=convert_inside_obj_to_arr($all_branches,"user_id"),
                                $selected_value=isset($package_data->package_branches)?convert_inside_obj_to_arr($package_data->package_branches,"user_id"):"",
                                $class="form-control select_2_class",
                                $multiple="multiple",
                                $required = "",
                                $disabled = "",
                                $data = ""
                            );
                            ?>

                        </div>
                    </div>

                    <?php if(false): ?>
                    {{--gift--}}
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            هدية العرض
                        </div>
                        <div class="panel-body">

                            <?php

                                echo
                                generate_select_tags(
                                    $field_name="product_gift_id",
                                    $label_name="اختار الهدية",
                                    $text=convert_inside_obj_to_arr($all_gift_products,"pro_name"),
                                    $values=convert_inside_obj_to_arr($all_gift_products,"pro_id"),
                                    $selected_value=array(""),
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required = "",
                                    $disabled = "",
                                    $data = $package_data
                                );
                            ?>

                            <hr>
                            <?php
                                $normal_tags=array(
                                    "min_products_to_get_gift"
                                );

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $package_data,
                                    "yes",
                                    ""
                                );

                                $attrs[0]["min_products_to_get_gift"]="اقل عدد من المنتجات المشتراة لتاخد الهدية";
                                $attrs[3]["min_products_to_get_gift"]="number";

                                echo
                                generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                                );
                            ?>

                        </div>
                    </div>
                    <?php endif; ?>





                    {{csrf_field()}}
                    <input id="submit" type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">


                </form>
            </div>


            <?php else: ?>
            {!! get_warning(url("admin/product/products_on_stock"),"لا توجد منتجات متاحه لاضافتها في عرض جديد") !!}

            <?php endif; ?>

        </div>
    </div>



@endsection



