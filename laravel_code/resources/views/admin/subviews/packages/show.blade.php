@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            العروض
        </div>
        <div class="panel-body">

            <div class="col-md-12">


                <div class="col-md-6 form-group">
                    <label for="">من التاريخ</label>
                    <input type="date" class="form-control" id="from_date_id" value="{{$start_date}}">
                </div>

                <div class="col-md-6 form-group">
                    <label for="">الي التاريخ</label>
                    <input type="date" class="form-control" id="to_date_id" value="{{$end_date}}">
                </div>

            </div>

            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                    <button class="btn btn-info show_packages_filter_btn">ابحث</button>
                </div>
            </div>


        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            العروض

        </div>
        <div class="panel-body" style="overflow-y: hidden;overflow-x: scroll;">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>اسم العرض</td>
                        <td>تاريخ بداية العرض</td>
                        <td>تاريخ نهاية العرض</td>
                        <td>اقل كمية من المنتجات المشتراة لدخول العرض</td>

                        <?php if(false): ?>
                        <td>اقل كمية من المنتجات المشتراة لتاخد الهدية</td>
                        <td>الهدية</td>
                        <?php endif; ?>

                        <td>نوع العرض</td>
                        <td>نسبة/قيمة العرض</td>
                        <td>تعديل</td>
                        <td>مسح</td>
                    </tr>
                </thead>


                <tbody>
                <?php foreach ($all_packages as $key => $package): ?>
                <tr id="row<?= $package->package_id ?>">
                    <td><?=$key+1?></td>

                    <td>{{$package->package_name}}</td>
                    <td>{{$package->package_start_date}}</td>
                    <td>{{$package->package_end_date}}</td>
                    <td>{{$package->min_products_to_get_offer}}</td>

                    <?php if(false): ?>
                    <td>{{$package->min_products_to_get_gift}}</td>
                    <td>
                        {{$package->pro_name}} <br>
                        {{$package->pro_barcode}}
                    </td>
                    <?php endif; ?>

                    <td>{{($package->package_type==0)?"نسبة":"قيمة"}}</td>
                    <td>{{$package->discount_percentage_for_product}}{{($package->package_type==0)?"%":""}}</td>


                    <td>
                        <a class="btn btn-info" href="<?= url("admin/packages/save/$package->package_id") ?>">
                            تعديل
                        </a>
                    </td>
                    <td>
                        <a href='#' class="btn btn-danger general_remove_item" data-tablename="" data-deleteurl="<?= url("/admin/packages/remove_package") ?>" data-itemid="<?= $package->package_id ?>">
                            مسح
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

            <?php if(check_permission($user_permissions,"factory/packages","add_action",$current_user)): ?>
            <div class="col-md-3 col-md-offset-4">
                <a href="{{url("/admin/packages/save")}}" class="btn btn-primary">إضافة جديد</a>
            </div>
            <?php endif; ?>


        </div>
    </div>




@endsection
