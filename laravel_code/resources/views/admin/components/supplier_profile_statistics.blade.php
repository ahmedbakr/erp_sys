<div class="col_3">
    <div class="row">
        <div class="col-md-3 col-md-offset-1 widget">
            <div class="r3_counter_box">
                <i class="fa fa-shopping-cart" style="color: #8BC34A"></i>
                <div class="stats">
                    <h5>{{$total_purchases_orders}}</h5>
                    <div class="grow">
                        <p>عدد العناصر للمشتريات</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 widget">
            <div class="r3_counter_box">
                <i class="fa fa-usd"></i>
                <div class="stats">
                    <h5>{{$total_paid}}</h5>
                    <div class="grow grow2">
                        <p>مجموع المدفوع</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 widget">
            <div class="r3_counter_box">
                <i class="fa fa-usd" style="color: red;"></i>
                <div class="stats">
                    <h5>{{$total_remained}}</h5>
                    <div class="grow grow3">
                        <p>مجموع المتبقي</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-3 col-md-offset-1 widget">
            <div class="r3_counter_box">
                <i class="fa fa-shopping-cart" style="color: red;"></i>
                <div class="stats">
                    <h5>{{$total_returns_orders}}</h5>
                    <div class="grow grow3">
                        <p>عدد العناصر للمرتجع</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 widget">
            <div class="r3_counter_box">
                <i class="fa fa-usd"></i>
                <div class="stats">
                    <h5>{{$total_returns_received}}</h5>
                    <div class="grow grow2">
                        <p>مجموع المستلم</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 widget">
            <div class="r3_counter_box">
                <i class="fa fa-usd" style="color: red;"></i>
                <div class="stats">
                    <h5>{{$total_returns_remained}}</h5>
                    <div class="grow grow3">
                        <p>مجموع المتبقي</p>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="clearfix"></div>
</div>