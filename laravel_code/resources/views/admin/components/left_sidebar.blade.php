<div class="left-side sticky-left-side">

    <!--logo and iconic logo start-->
    <div class="logo">
        <h1><a href="{{url('admin/dashboard')}}">الصفحة <span>الرئيسية</span></a></h1>
    </div>
    <div class="logo-icon text-center">
        <a href="{{url('admin/dashboard')}}"><i class="lnr lnr-home"></i> </a>
    </div>

    <!--logo and iconic logo end-->
    <div class="left-side-inner">

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
            <li class="active"><a href="{{url('/admin/dashboard')}}"><i class="lnr lnr-power-switch"></i><span>الرئيسية</span></a>
            </li>
            <li class="menu-list">
                <a><i class="lnr lnr-cog"></i>
                    <span>المواد الخام</span></a>
                <ul class="sub-menu-list">
                    <?php if(check_permission($user_permissions,"factory/materials","show_action")): ?>
                        <li><a href="{{url('/admin/materials')}}">المواد الخام</a></li>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"factory/materials","add_action")): ?>
                        <li><a href="{{url('/admin/materials/save')}}">إضافة ماده خام جديدة</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"product/product_materials","show_action")): ?>
                        <li><a href="{{url('/admin/product/product_materials')}}">كميات المواد الخام</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"product/broken_product_materials","show_action")): ?>
                        <li><a href="{{url('/admin/product/broken_product_materials')}}">الهالك من المواد الخام</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <li class="menu-list">
                <a href="{{url('/admin/order')}}"><i class="fa fa-shopping-cart"></i>
                    <span>الطلبات</span></a>
                <ul class="sub-menu-list">
                    <?php if(check_permission($user_permissions,"orders/suppliers","show_action")): ?>
                        <li><a href="{{url('/admin/order/supplier_materials/show_bills')}}">الطلبات من الموردين</a></li>
                        <li><a href="{{url('/admin/order/supplier_materials/show_bills/0/0/0/user')}}">الطلبات من الموردين تقسيم حسب المورد</a></li>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"branch_orders","show_action")): ?>
                        <li><a href="{{url('/admin/order/branch_orders')}}">طلبات الفروع</a></li>
                    <?php endif; ?>

                </ul>
            </li>

            <?php if(check_permission($user_permissions,"factory/users","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-users"></i> <span>المستخدمين</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/users/get_all_admins")}}">مشاهدة كل المديرين</a></li>
                    <li><a href="{{url("/admin/users/get_all_suppliers")}}">مشاهدة كل الموردين</a></li>

                    <?php if(check_permission($user_permissions,"factory/users","add_action")): ?>

                    <li><a href="{{url("/admin/users/save/admin")}}">إضافة مدير جديد</a></li>
                    <li><a href="{{url("/admin/users/save/supplier")}}">إضافة مورد جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/coupons","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-star"></i> <span>الكوبونات</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/coupons")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/coupons","add_action")): ?>

                    <li><a href="{{url("/admin/coupons/save_coupon")}}">إضافة كوبون جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            <li class="menu-list">
                <a><i class="fa fa-leaf"></i>
                    <span>المنتجات</span></a>
                <ul class="sub-menu-list">



                    <?php if(check_permission($user_permissions,"product/products_on_stock","show_action")): ?>
                        <li><a href="{{url('/admin/product/products_on_stock')}}">المنتجات في المخزن</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"product/broken_products_on_stock","show_action")): ?>
                        <li><a href="{{url('/admin/product/broken_products_on_stock')}}">الهالك من المنتجات</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"product/produce","show_action")): ?>
                        <li><a href="{{url('/admin/product/save_produce_product')}}">إنتاج منتج جديد</a></li>
                    <?php endif; ?>

                        <?php if(check_permission($user_permissions,"product/reverse_produce","add_action")): ?>
                        <li><a href="{{url('/admin/product/save_reverse_produce_product')}}">سحب منتجات أنتجت بالخطأ</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"product/review_produce_products","show_action")): ?>
                        <li><a href="{{url('/admin/product/review_produce_products')}}">مراجعه طلبات الانتاج</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"factory/packages","show_action")): ?>
                        <li><a href="{{url('/admin/packages/show')}}">العروض</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"factory/packages","add_action")): ?>
                        <li><a href="{{url('/admin/packages/save')}}">اضف عرض</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"admin/product_barcode","show_action")): ?>
                        <li><a href="{{url('/admin/products_barcode')}}">باركود المنتجات</a></li>
                    <?php endif; ?>




                </ul>
            </li>
            <?php if(check_permission($user_permissions,"uploader","show_action")): ?>
            <li class="menu-list">
                <a><i class="fa fa-upload"></i>
                    <span>رافع الملفات</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/uploader')}}">ارفع الملفات من هنا</a></li>

                </ul>
            </li>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/money_transfer","show_action")): ?>
            <li class="menu-list">
                <a><i class="fa fa-dollar"></i>
                    <span>التحويلات المالية</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/money_transfer')}}">التحويلات المالية</a></li>

                </ul>
            </li>
            <?php endif; ?>


            <?php if(check_permission($user_permissions,"factory/expenses","show_action")): ?>
            <li class="menu-list">
                <a href="#"><i class="lnr lnr-briefcase"></i>
                    <span>المصاريف</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/expenses/show_all_expenses_types")}}">مشاهدة أنواع المصاريف</a></li>
                    <li><a href="{{url("/admin/expenses/show_all_expenses/0/0/0")}}">مشاهدة المصاريف</a></li>
                    <li><a href="{{url("/admin/expenses/show_all_expenses/0/0/0/user")}}">مشاهدة المصاريف لكل مستخدم</a></li>
                    <?php if(check_permission($user_permissions,"factory/expenses","add_action")): ?>
                        <li><a href="{{url("/admin/expenses/save_expenses_type")}}">إضافة نوع مصروف جديد</a></li>
                        <li><a href="{{url("/admin/expenses/save_expenses")}}">إضافة مصروف جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>


            <?php if(check_permission($user_permissions,"factory/assets","show_action")): ?>
            <li class="menu-list">
                <a href="#"><i class="lnr lnr-apartment"></i>
                    <span>الأصول الثابتة</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/assets/show_all_assets")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"factory/assets","add_action")): ?>
                        <li><a href="{{url("/admin/assets/save_asset")}}">إضافة اصل جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>


            <?php if(check_permission($user_permissions,"admin/invoices_templates","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-chart-bars"></i> <span>قوالب الفاتورة</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/invoices_templates")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/invoices_templates","add_action")): ?>
                    <li><a href="{{url("/admin/invoices_templates/save_template")}}">إضافة قالب جديد</a></li>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"admin/print_client_bill","show_action")): ?>
                        <li><a href="{{url("/print_client_bill")}}">طباعة فاتورة جديدة</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/cheque_templates","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-chart-bars"></i> <span>قوالب الشيكات</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/cheques")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/cheque_templates","add_action")): ?>
                    <li><a href="{{url("/admin/cheques/save_template")}}">إضافة شيك جديد</a></li>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"admin/print_cheques","show_action")): ?>
                    <li><a href="{{url("/prepare_print_cheques")}}">طباعة شيك جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/banks","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-chart-bars"></i> <span>البنوك</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/banks")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/banks","add_action")): ?>
                    <li><a href="{{url("/admin/banks/save_bank")}}">إضافة بنك جديد</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/branch_target","show_action")): ?>

            <li class="menu-list"><a><i class="lnr lnr-briefcase"></i> <span>تارجت الفروع</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/branch_target")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/branch_target","add_action")): ?>
                    <li><a href="{{url("/admin/branch_target/save_target")}}">إضافة تارجت جديد</a></li>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"admin/branch_target","show_branch_target")): ?>
                    <li><a href="{{url("branch/branch_target/branch_target/get_branch_target/0/0/0/0")}}">اظهر تارجت الفرع</a></li>
                    <?php endif; ?>



                </ul>
            </li>

            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/deposite_money","show_action")): ?>

            <li class="menu-list"><a><i class="fa fa-dollar"></i> <span>إيداعات الفروع</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/deposite_money")}}">مشاهدة الكل</a></li>
                </ul>
            </li>

            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/commission","show_action")): ?>

            <li class="menu-list"><a><i class="fa fa-dollar"></i> <span>عمولات الفروع</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/commissions")}}">مشاهدة الكل</a></li>
                    <?php if(check_permission($user_permissions,"admin/commission_rules","show_action")): ?>
                    <li><a href="{{url("/admin/commissions/show_rules")}}">قواعد العمولات</a></li>
                    <?php endif; ?>
                </ul>
            </li>

            <?php endif; ?>

            <?php if(check_permission($user_permissions,"admin/review_client_bills","show_action")): ?>

            <li class="menu-list"><a><i class="fa fa-dollar"></i> <span>مراجعه فواتير العملاء</span></a>
                <ul class="sub-menu-list">
                    <li><a href="{{url("/admin/review_client_bills")}}">مشاهدة الكل</a></li>
                </ul>
            </li>

            <?php endif; ?>

        </ul>
        <!--sidebar nav end-->
    </div>
</div>