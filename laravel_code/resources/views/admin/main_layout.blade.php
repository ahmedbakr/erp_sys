<!DOCTYPE HTML>
<html>
<head>
    <title>{{$meta_title}}</title>
    <meta name="description" content="<?php echo $meta_desc ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public_html/admin')}}/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/login.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/general.css" rel='stylesheet' type='text/css'/>

    <!-- font-awesome CSS -->
    <link href="{{url('/public_html/admin')}}/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <!-- lined-icons -->
    <link rel="stylesheet" href="{{url('/public_html/admin')}}/css/icon-font.min.css" type='text/css'/>
    <!-- //lined-icons -->

    <!--datatable css-->
    <link href="<?= url('public_html/admin/js/datatables/css/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= url('public_html/admin/js/datatables/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet" type="text/css">


    <!----webfonts--->
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <!---//webfonts--->

    <link href="{{url('/public_html/admin')}}/css/font-awesome.css" rel="stylesheet">
    <link href="{{url('/public_html/admin')}}/css/btm_style.css" rel="stylesheet">



    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/public_html/admin')}}/js/bootstrap.min.js"></script>


    <!--datatable js-->
    <script src="<?= url('public_html/admin/js/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= url('public_html/admin/js/datatables/js/dataTables.bootstrap.min.js') ?>"></script>


    <!-- Toastr -->
    <link href="{{url('/public_html/toastr')}}/toastr.css" rel="stylesheet">
    <script src="{{url('/public_html/toastr')}}/toastr.js"></script>


    <!--download excel from datatable -->
    <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>



    <!--istree -->
    <link href="<?= url('public_html/admin/tree/themes/default/style.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= url('public_html/admin/tree/js/jstree.js') ?>"></script>

    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!-- END Select 2 -->

    <script src="<?= url('public_html/admin/js/moment.js') ?>"></script>
    <script src="<?= url('public_html/admin/js/bootstrap-datetimepicker.js') ?>"></script>
    <link href="<?= url('public_html/admin/css/bootstrap-datetimepicker.css') ?>" rel="stylesheet" type="text/css">


    <script src="<?= url('public_html/jscode/admin/config.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/admin.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/factory.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/utility.js') ?>"></script>
    <script src="<?= url('public_html/jscode/homepage.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/tree.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/select_tree_items.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/cheque.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/factory_charts.js') ?>"></script>
    <?php if(false): ?>
        <script src="<?= url('public_html/jscode/admin/templates.js') ?>"></script>
    <?php endif; ?>

    <script src="<?= url('public_html/jscode/admin/branch.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/client_bill.js') ?>"></script>




</head>

<body>

<!-- hidden csrf -->
<input type="hidden" class="csrf_input_class" value="{{csrf_token()}}">
<!-- /hidden csrf -->
<!-- hidden base url -->
<input type="hidden" class="url_class" value="<?= url("/") ?>">
<!-- /hidden base url -->

<input type="hidden" class="sell_tax_percent" value="{{$company->sell_tax_percent}}">

<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-md-4 top_nav_dates">
                <div class="date">
                    هجري :
                    {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                </div>
                <div class="date">
                    ميلادي :
                    {{$current_date}}
                </div>
                <div class="date direction_rtl">
                    الوقت :
                    <span class="show_time"></span>
                </div>
            </div>

            <div class="col-md-3">
                <?php if(check_permission($user_permissions,"admin/client_orders","show_action",$current_user,false)): ?>
                    <div class="col-md-6 lang">
                        <a class="btn logout_link" style="background-color: aqua !important;" href="{{url("/admin/client_bills/show_all")}}">فواتير البيع</a></li>
                    </div>
                <?php endif; ?>
                <?php if(check_permission($user_permissions,"admin/commission","show_action",$current_user,false)): ?>
                    <div class="col-md-6 lang">
                        <a class="btn logout_link" style="background-color: aqua !important;" href="{{url("/count_commissions/true")}}">تحديث العمولة</a></li>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-3">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" style="margin-top: 5px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        إشعارات النظام
                        <span class="caret"></span>
                    </button>
                    <?php if(isset($current_notifications) && count($current_notifications)): ?>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="max-height: 300px;overflow-y: scroll;">
                        <?php foreach($current_notifications as $key => $notification): ?>
                        <li>
                            <div class="alert alert-{{$notification->not_type}}" title="{{$notification->not_title}}">
                                <span style="display: block;">{{$notification->not_title}}</span>
                                <br>
                                <span class="label label-default" style="margin-top: 5px;">{{$notification->created_at}}</span>
                            </div>
                        </li>
                        <?php endforeach; ?>
                        <li role="separator" class="divider"></li>
                        <li>
                            <div class="alert alert-primary" style="text-align: center;">
                                <a href="{{url("admin/notifications")}}" style="color: #fff;">مشاهدة الكل</a>
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-1">
                <div class="lang">
                    <a href="#" class="btn">English</a>
                </div>
            </div>

            <div class="col-md-1">
                <div class="lang">
                    <a href="{{url('/logout')}}" class="btn logout_link"><i class="fa fa-lock"></i> خروج  </a>
                </div>
            </div>

        </div>
    </div>
</div>


<header>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="box-logout color_white">
                    <h3 style="line-height: 32px;font-weight: bold;">
                        {{$current_user->full_name}}
                        <?php if($current_user->user_type == "branch_admin" && isset($branch_data)): ?>
                        <br>
                        ({{$branch_data->full_name}})
                        <?php else: ?>
                        <br>
                        (الفرع الرئيسي)
                        <?php endif; ?>
                    </h3>
                </div>
            </div>
            <div class="col-md-8 header_logo">
                <div class="logo">
                    <img style="float:left;" src="{{get_image_or_default("$company->path")}}" />
                </div>
            </div>
        </div>
    </div>
</header>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header float_right">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

    </div>
</nav>

<script>
    $(function () {

        $('.tree_click_link a').click(function () {

            var link = $(this).attr('href');
            location.href = link;

            return false;
        });

    });
</script>


<div class="col-md-12">
    <div class="">
        <?php

            $msg=\Session::get("msg");

            if($msg==""){
                if (count($errors->all()) > 0)
                {
                    $dump = "<div class='alert alert-danger'>";
                    foreach ($errors->all() as $key => $error)
                    {
                        $dump .= $error." <br>";
                    }
                    $dump .= "</div>";

                    $msg=$dump;
                }
            }

            $perms_msg=\Session::get("perms_msg");


        ?>

        <input type="hidden" class="get_flash_message" value="{!! $msg !!}">

        <?php if($perms_msg!=""): ?>
        <div class="panel panel-info">
            <div class="panel-heading">رسائل الصلاحيات</div>
            <div class="panel-body">
                {!! $perms_msg !!}
            </div>
        </div>
        <?php endif; ?>

        <div class="col-md-3" style="position: absolute;right: 15px;z-index: 7;top: -67px;">

            <div class="panel panel-default">
                <div class="panel-heading main_menu_panel_heading" data-toggle="collapse" href=".main_menu">
                    القائمة الرئيسية
                </div>
                <div class="panel-body collapse main_menu">

                    <input type="text" placeholder="إبحث هنا ..." class="form-control filter_left_menu" style="margin-bottom: 15px;">

                    <div class="jstree_demo_div tree_click_link" style="direction: rtl;">
                        <ul>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/home.png')}}"}'>
                                <a href="{{url('/admin/dashboard')}}"><i class="lnr lnr-power-switch"></i><span>الرئيسية</span></a>
                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/general_settings.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>الإعدادات العامة</span></a>
                                <ul>

                                    <?php if(check_permission($user_permissions,"company","edit_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/user_transit.png')}}"}' ><a href="{{url("/admin/company")}}">بيانات الشركة</a></li>
                                    <?php endif; ?>

                                    <?php if($current_user->user_type == "branch_admin"): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/admins.png')}}"}' ><a href="{{url("/admin/users/change_password")}}">تغيير الباسورد</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/users","show_action",$current_user,false) && $current_user->user_type == "admin"): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/admins.png')}}"}' ><a href="{{url("/admin/users/get_all/admin")}}">الادمنز(المديرين)</a></li>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/branches.png')}}"}' ><a href="{{url("/admin/users/get_all/branch")}}">الفروع</a></li>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/users.png')}}"}' ><a href="{{url("/admin/users/get_all/branch_admin")}}">المستخدميين</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/users_transits","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/user_transit.png')}}"}' ><a href="{{url("/admin/users_transits")}}">تنقلات المستخدمين</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"admin/invoices_templates","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/invoice_template.png')}}"}' ><a href="{{url("/admin/invoices_templates")}}">قوالب الفاتورة</a></li>
                                    <?php endif; ?>


                                    <?php if(1!=1 && check_permission($user_permissions,"admin/cheque_templates","show_action",$current_user,false)): ?>
                                        <li><a href="{{url("/admin/cheques")}}">الشيكات</a></li>
                                    <?php endif; ?>

                                    <?php if(false && check_permission($user_permissions,"admin/banks","add_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/banks.png')}}"}' ><a href="{{url("/admin/banks")}}">البنوك</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/points_of_sale","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/cashier.png')}}"}' ><a href="{{url("/admin/points_of_sale")}}">نقاط البيع</a></li>
                                    <?php endif; ?>


                                </ul>
                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>البيانات الأساسية</span></a>
                                <ul>


                                    <?php if(false && check_permission($user_permissions,"admin/product_codes","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/payment_methods.png')}}"}' ><a href="{{url("/admin/product_codes")}}">أنواع الأكواد</a></li>
                                    <?php endif; ?>



                                    <?php if(check_permission($user_permissions,"admin/product_units","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/payment_methods.png')}}"}' ><a href="{{url("/admin/product_units")}}">وحدات القياس</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"factory/payment_methods","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/payment_methods.png')}}"}' ><a href="{{url("/admin/payment_methods")}}">أنواع السداد</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"admin/price_list","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/price_list.png')}}"}' ><a href="{{url("/admin/prices")}}">قائمة الأسعار</a></li>
                                    <?php endif; ?>



                                    <?php if(check_permission($user_permissions,"product/branch_product_prices","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/price_list.png')}}"}' ><a href="{{url('/admin/product/set_products_prices')}}">اسعار المنتجات</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"factory/packages","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/packages.png')}}"}' ><a href="{{url('/admin/packages/show')}}">العروض</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/coupons","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/coupons.png')}}"}' ><a href="{{url("/admin/coupons/cats")}}">تصنيفات الكوبونات</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/coupons","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/coupons.png')}}"}' ><a href="{{url("/admin/coupons")}}">الكوبونات</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/commission","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/commissions.png')}}"}' ><a href="{{url("/admin/commissions")}}"> العمولات</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"admin/product_barcode","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/barcode.png')}}"}' ><a href="{{url('/admin/products_barcode')}}">باركود المنتجات</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/branch_target","add_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/target.png')}}"}' ><a href="{{url("/admin/branch_target")}}">تارجت الفروع</a></li>
                                    <?php endif; ?>


                                </ul>

                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>الأدلة</span></a>
                                <ul>

                                    <?php if(check_permission($user_permissions,"factory/users","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/customers.png')}}"}' ><a href="{{url("/admin/users/get_all/customer")}}">العملاء</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/category","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/categories.png')}}"}' ><a href="{{url('/common/category/show_tree/false/expense')}}">تصنيفات مصاريف للفروع</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/category","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/categories.png')}}"}' ><a href="{{url('/common/category/show_tree/false/product')}}">بيانات الأصناف</a></li>
                                    <?php endif; ?>

                                </ul>

                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>عمليات المخازن</span></a>
                                <ul>


                                    <?php if(check_permission($user_permissions,"branch/products_first_time","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/product_first_time.png')}}"}' ><a href="{{url("/admin/branch_bills/save_product_first_time")}}">بضاعه أول مدة</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"product/broken_products_on_stock","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/broken_products.png')}}"}' ><a href="{{url('/admin/product/broken_products_on_stock')}}">الهالك من المنتجات</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"branch_orders","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_requests.png')}}"}' ><a href="{{url('/admin/order/branch_orders')}}">طلبات الفروع</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"add_permission_to_stock","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_requests.png')}}"}' ><a href="{{url('/admin/branch_bills/show_permission_to_stock_bills/add')}}">أذون إضافة بضاعه</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"get_permission_from_stock","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_requests.png')}}"}' ><a href="{{url('/admin/branch_bills/show_permission_to_stock_bills/get')}}">أذون صرف بضاعه</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"branch_transfer_products","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_requests.png')}}"}' ><a href="{{url('/admin/branch_transfers')}}">تحويلات الفروع</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/review_client_bills","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/review_customer_bill.png')}}"}' ><a href="{{url("/admin/review_client_bills")}}">مراجعه فواتير العملاء</a></li>
                                    <?php endif; ?>

                                </ul>

                            </li>



                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>عمليات البيع</span></a>
                                <ul>

                                    <?php if(check_permission($user_permissions,"admin/client_orders","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/cashier.png')}}"}' ><a href="{{url("/admin/client_bills/show_all")}}">فواتير البيع</a></li>
                                    <?php endif; ?>

                                </ul>

                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'>
                                <a><i class="lnr lnr-power-switch"></i><span>العمليات المالية للفروع</span></a>
                                <ul>

                                    <?php if(check_permission($user_permissions,"factory/duty_documents","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/send_duty.png')}}"}' ><a href="{{url("/admin/duty_documents/send")}}">سندات تسليم عهد</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/duty_documents","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/receive_duty.png.png')}}"}' ><a href="{{url("/admin/duty_documents/receive")}}">سندات إستلام عهد</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/duty_documents","add_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/send_duty.png')}}"}' ><a href="{{url("/admin/duty_documents/save/new")}}">تسجيل سند تسليم</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"factory/expenses","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_expenses.png')}}"}' ><a href="{{url("/common/expenses/show_all_expenses")}}">مصاريف الفروع</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"admin/deposite_money","show_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/branche_deposite.png')}}"}' ><a href="{{url("/admin/deposite_money")}}">إيداعات الفروع</a></li>
                                    <?php endif; ?>


                                    <?php if(check_permission($user_permissions,"factory/expenses","add_action",$current_user,false)): ?>
                                    <li data-jstree='{"icon":"{{url('public_html/tree/images/save_branche_expense.png')}}"}' ><a href="{{url("/common/expenses/save_expenses")}}">تسجيل مصروفات الفروع</a></li>
                                    <?php endif; ?>

                                </ul>

                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"{{url('public_html/tree/images/default_menu.png')}}"}'><a><i class="fa fa-dollar"></i> <span>التقارير</span></a>
                                <ul>
                                    <?php if(check_permission($user_permissions,"factory/reports","sell_bills",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/total_branches_clients_bills")}}">مبيعات الفروع</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","sell_bills",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/total_cashiers_clients_bills")}}">مبيعات الكاشيير</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","products_details",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/products_on_branches_stock")}}">بيانات الأصناف في المعارض</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","products_on_factory_details",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/products_on_factory_stock")}}">بيانات الأصناف في الفرع الرئيسي</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","products_sells",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/products_most_sell")}}">حركة الأصناف</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","review_products_stock",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/review_products_stock")}}">ميزان مراجعه الأصناف</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","gain_products_stock",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/gain_products_stock")}}">ربحية الأصناف</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","broken_products",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/broken_products")}}">الهالك من المنتجات</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","report_branch_orders",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_branch_orders")}}">طلبات الفرع</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","report_branch_transfers",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_branch_transfers")}}">طلبات تحويلات الفرع</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","report_permission_bills",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_permission_bills")}}"> اذون الصرف و الاضافة</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","commissions_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/commissions_report")}}">عمولات الفروع</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","daily_bills",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/daily_bills")}}">يومية الفواتير</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","report_clients_data",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_clients_data")}}">بيانات العملاء</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","report_clients_products",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_clients_products")}}">مبيعات العملاء</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","most_products_bought",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/most_products_bought")}}">مبيعات العملاء بالنسبة للمنتجات</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","most_products_bought",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/report_products_bought")}}">مبيعات المنتجات</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","duty_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/duty_report")}}">العهد المستلمة / المرسلة</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","expenses_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/expenses_report")}}">كشف حساب مصروف تفصيلي</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","review_expenses_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/review_expenses_report")}}">ميزان مراجعه المصروفات</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","packages_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/packages_report")}}">تقرير العروض </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","coupons_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/coupons_report")}}">تقرير الكوبونات  </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"admin/branch_target","show_branch_target",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/branch_target/branch_target/get_branch_target")}}">تقرير تارجت الفروع  </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","branches_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/branches_report")}}">تقرير بيانات الفروع  </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","cashiers_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/cashiers_report")}}">تقرير بيانات الموظفين  </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"factory/reports","best_sell_bills_report",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' >
                                            <a href="{{url("/admin/reports/best_sell_bills_report")}}">تقرير المبيعات لأفضل وقت</a>
                                        </li>
                                    <?php endif; ?>




                                    <?php if(false): ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills")}}">المبيعات</a></li>
                                        <?php endif; ?>

                                        <?php if(check_permission($user_permissions,"admin/sell_bills_session","show_action",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_session")}}">المبيعات حسب الجلسات</a></li>
                                        <?php endif; ?>
                                    <?php endif; ?>


                                    <?php if(false): ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_customer",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_customer")}}">المبيعات حسب العميل</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_branch",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_branch")}}">المبيعات حسب الفرع</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_cashier",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_cashier")}}">المبيعات حسب المستخدم</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_user_age",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_user_age")}}">المبيعات حسب العمر للعميل</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_user_gender",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_user_gender")}}">المبيعات حسب الجنس للعميل</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/reports","sell_bills_depend_on_product",$current_user,false)): ?>
                                        <li data-jstree='{"icon":"{{url('public_html/tree/images/reports.png')}}"}' ><a href="{{url("/admin/reports/sell_bills_depend_on_product")}}">المبيعات حسب المنتج</a></li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </ul>
                            </li>


                            <?php if(false): ?>
                                <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>
                                    <a><i class="lnr lnr-cog"></i>
                                        <span>المواد الخام</span></a>
                                    <ul >
                                        <?php if(check_permission($user_permissions,"factory/materials","show_action",$current_user,false)): ?>
                                        <li><a href="{{url('/admin/materials')}}">المواد الخام</a></li>
                                        <?php endif; ?>
                                        <?php if(check_permission($user_permissions,"factory/materials","add_action",$current_user,false)): ?>
                                        <li><a href="{{url('/admin/materials/save')}}">إضافة ماده خام جديدة</a></li>
                                        <?php endif; ?>

                                        <?php if(check_permission($user_permissions,"product/product_materials","show_action",$current_user,false)): ?>
                                        <li><a href="{{url('/admin/product/product_materials')}}">كميات المواد الخام</a></li>
                                        <?php endif; ?>

                                        <?php if(check_permission($user_permissions,"product/broken_product_materials","show_action",$current_user,false)): ?>
                                        <li><a href="{{url('/admin/product/broken_product_materials')}}">الهالك من المواد الخام</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>


                        </ul>
                    </div>
                </div>
            </div>



        </div>

        <div class="col-md-12">
            @yield('subview')
        </div>



    </div>
</div>



</body>
</html>