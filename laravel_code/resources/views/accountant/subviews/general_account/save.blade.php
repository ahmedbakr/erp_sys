@extends('accountant.main_layout')

@section('subview')


    <?php
        $header_text="اضف حساب جديد-".$selected_cat_type_text[$selected_cat_type];
        $acc_id="";
        if (is_object($acc_obj)){
            $header_text="تعديل حساب";
            $acc_id=$acc_obj->acc_id;
        }

        if (count($errors->all()) > 0)
        {
            $dump = "<div class='alert alert-danger'>".implode(" <br> ",$errors->all())."</div>";
            echo $dump;
        }

        if (isset($success)&&!empty($success)) {
            echo $success;
        }
    ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            {{$header_text}}
        </div>
        <div class="panel-body save_general_account">

            <form action="<?=url("accountant/general_account/save/$selected_cat_type/$cat_id/$acc_id")?>" method="POST" enctype="multipart/form-data">

                <?php
                    echo
                    generate_select_tags(
                        $field_name="acc_money_type",
                        $label_name="اختار نوع الحساب*",
                        $text=["دائن","مدين"],
                        $values=["credit","debit"],
                        $selected_value="",
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = (is_object($acc_obj)?"disabled":""),
                        $data = $acc_obj,
                        $grid="6"
                    );


                    echo
                    generate_select_tags(
                        $field_name="acc_go_away",
                        $label_name="مرحل او غير مرحل*",
                        $text=["غير مرحل","مرحل"],
                        $values=["0","1"],
                        $selected_value="",
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $acc_obj,
                        $grid="6"
                    );

                ?>



                <?php
                $normal_tags=array(
                    'acc_code','acc_name', 'acc_name_en',
                    'acc_path', 'acc_path_en'
                );

                $attrs = generate_default_array_inputs_html(
                    $normal_tags,
                    $acc_obj,
                    "yes",
                    ""
                );


                $attrs[0]["acc_code"]="كود الحساب*";
                $attrs[0]["acc_name"]="اسم الحساب*";
                $attrs[0]["acc_name_en"]="اسم الحساب بالانجليزي*";
                $attrs[0]["acc_path"]="مسار الحساب";
                $attrs[0]["acc_path_en"]="مسار الحساب انجليزي";

                $attrs[2]["acc_name"]="required";
                $attrs[2]["acc_name_en"]="required";





                $attrs[6]["acc_code"]="12";
                $attrs[6]["acc_name"]="6";
                $attrs[6]["acc_name_en"]="6";
                $attrs[6]["acc_path"]="6";
                $attrs[6]["acc_path_en"]="6";


                echo
                generate_inputs_html(
                    reformate_arr_without_keys($attrs[0]),
                    reformate_arr_without_keys($attrs[1]),
                    reformate_arr_without_keys($attrs[2]),
                    reformate_arr_without_keys($attrs[3]),
                    reformate_arr_without_keys($attrs[4]),
                    reformate_arr_without_keys($attrs[5]),
                    reformate_arr_without_keys($attrs[6])
                );
                ?>


                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>





        </div>
    </div>
@endsection