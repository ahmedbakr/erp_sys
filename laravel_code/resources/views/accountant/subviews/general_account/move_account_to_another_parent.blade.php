@extends('accountant.main_layout')

@section('subview')

    @include("accountant.subviews.general_account_category.accounts_tree.tree_modal")

    <?php
        $header_text="نقل حساب";


        if (count($errors->all()) > 0)
        {
            $dump = "<div class='alert alert-danger'>".implode(" <br> ",$errors->all())."</div>";
            echo $dump;
        }

        if (isset($success)&&!empty($success)) {
            echo $success;
        }
    ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            {{$header_text}}
        </div>
        <div class="panel-body save_general_account_category">

            <form action="<?=url("accountant/general_account/move_account_to_another_parent/$selected_acc_type/$acc_data->acc_id")?>" method="POST" enctype="multipart/form-data">


                <div class="alert alert-primary" style="text-align: center;">
                    <p><?=$acc_data->{"acc_name".$en}?></p>
                </div>

                <div class="form-group">

                    <div class="alert alert-info" style="text-align: center;">
                        ملحوظة اذا لم تختار اي تصنيف . الحساب المراد نقله سيكون في اول الشجرة
                    </div>


                    <button
                            type="button"
                            class="btn btn-primary show_tree_modal center-block"
                            data-select_what="category"
                            data-select_type="single"
                            data-div_to_show_selection=".move_to_new_category_div"
                            data-input_field_name="cat_id"
                    >
                        اظهر شجرة الحسابات لتختار التصنيف المراد النقل اليه
                    </button>


                    <div class="move_to_new_category_div" style="text-align: center;">

                    </div>

                </div>





                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">

            </form>





        </div>
    </div>
@endsection