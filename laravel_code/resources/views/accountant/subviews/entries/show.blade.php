@extends('accountant.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading">فلتر القيود</div>
        <div class="panel-body">
            <form action="{{url("/accountant/entry/show_entries")}}" method="get">
                <div class="form-group">
                    <label for="">
                        القيود الغير موافق عليها فقط
                        <input type="checkbox" name="un_approved_entry">
                    </label>
                </div>

                <button class="btn btn-info" type="submit">ايحث</button>
            </form>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            القيود
        </div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>تاريخ القيد</td>
                        <td>اليومية المساعدة</td>
                        <td>مميز القيد</td>
                        <td>بيان</td>
                        <td>موافق عليه؟</td>
                        <td>تفاصيل القيد</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($all_year_entries as $key=>$entry): ?>
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{date("Y-m-d",strtotime($entry->entry_created_at))}}</td>
                            <td>{{$entry->entry_day_help}}</td>
                            <td>{{$entry->entry_type}}</td>
                            <td>{{$entry->entry_desc}}</td>
                            <td>
                                <?php if($entry->entry_approved==0): ?>
                                    <button type="button" class="btn btn-primary approve_entry_btn"  data-entry_id="{{$entry->entry_id}}">وافق </button>
                                <?php else: ?>
                                    تم الموافقة عليه
                                <?php endif; ?>
                            </td>
                            <td>
                                <a href="{{url("/accountant/entry/show_entry_items/$entry->entry_id")}}">تفاصيل القيد</a>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>


            </table>

            {{$all_year_entries->links()}}

        </div>
    </div>

@endsection