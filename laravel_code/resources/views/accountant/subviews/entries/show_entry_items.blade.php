@extends('accountant.main_layout')

@section('subview')



    <div class="panel panel-info main_entry_data_div">
        <div class="panel-heading">
            البيانات الاساسية للقيد
        </div>
        <div class="panel-body">
            <?php

            $normal_tags=array(
                'entry_day_help','entry_type','entry_created_at', 'entry_desc'
            );

            $attrs = generate_default_array_inputs_html(
                $normal_tags,
                $entry_obj,
                "yes",
                "readonly"
            );


            $attrs[0]["entry_day_help"]="اليومية المساعدة";
            $attrs[0]["entry_type"]="مميز القيد";
            $attrs[0]["entry_desc"]="بيان القيد";
            $attrs[0]["entry_created_at"]="تاريخ القيد *";

            $attrs[2]["entry_created_at"]="required";


            $attrs[3]["entry_desc"]="textarea";
            $attrs[3]["entry_created_at"]="date";


            $attrs[6]["entry_day_help"]="4";
            $attrs[6]["entry_created_at"]="4";
            $attrs[6]["entry_type"]="4";
            $attrs[6]["entry_desc"]="12";


            echo
            generate_inputs_html(
                reformate_arr_without_keys($attrs[0]),
                reformate_arr_without_keys($attrs[1]),
                reformate_arr_without_keys($attrs[2]),
                reformate_arr_without_keys($attrs[3]),
                reformate_arr_without_keys($attrs[4]),
                reformate_arr_without_keys($attrs[5]),
                reformate_arr_without_keys($attrs[6])
            );
            ?>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">

            بنود القيد

        </div>
        <div class="panel-body">

            <table class="table table-bordered entries_table">
                <thead>
                <tr class="primary">
                    <th>الحساب</th>
                    <th>دائن</th>
                    <th>مدين</th>
                    <th>مراكز التكلفة</th>
                </tr>
                </thead>

                <tbody>

                    <?php if(isset($entry_items)&&is_array($entry_items)): ?>
                        <?php foreach($entry_items as $key=>$item): ?>
                            <tr class="entry_row">
                                <td class="select_account">
                                    <div class="accounts_div">
                                        <label class="label label-primary selected_individual_item" data-item_id="{{$item->acc_id}}">
                                            <?=$item->{"acc_name".$en}?>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" class="form-control credit_cell" name="credit_cell[]" value="{{($item->credit_or_debit==1)?$item->item_amount:0}}">
                                </td>
                                <td>
                                    <input type="number" class="form-control debit_cell" name="debit_cell[]" value="{{($item->credit_or_debit==0)?$item->item_amount:0}}">
                                </td>
                                <td class="select_consuming_accounts">
                                    <div class="consuming_div">
                                        <?php if(isset($entry_consuming_items[$item->item_id])): ?>
                                            <?php foreach($entry_consuming_items[$item->item_id] as $key=>$cons_acc): ?>
                                                <label class="label label-primary selected_individual_item" data-item_id="{{$cons_acc->acc_id}}">
                                                    <?=$cons_acc->{"acc_name".$en}?>
                                                    <input type="number" readonly="" value="{{$cons_acc->acc_value}}">
                                                </label>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

@endsection