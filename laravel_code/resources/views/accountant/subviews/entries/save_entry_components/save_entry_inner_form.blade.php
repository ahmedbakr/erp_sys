
<?php
    $data_index_prefix="gen_acc_";
?>
@include("accountant.subviews.general_account_category.accounts_tree.tree_modal")


<?php
    $data_index_prefix="cons_acc_";
?>
@include("accountant.subviews.general_account_category.accounts_tree.tree_modal")

<div class="panel panel-primary">
    <div class="panel-heading">
        نماذج القيود
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <?php
                echo generate_select_tags(
                    $field_name="select_entry_template",
                    $label_name="اخنار نموذج للقيود",
                    $text=array_merge([""],convert_inside_obj_to_arr($entry_templates,"template_name")),
                    $values=array_merge([""],convert_inside_obj_to_arr($entry_templates,"template_id")),
                    $selected_value="",
                    $class="form-control select_2_class",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="12"
                );
            ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">حفظ القيود كنموذج جديد
                    <input type="checkbox" name="save_this_entries_as_new_template">
                </label>
                <label for="">مجرد نموذج
                    <input type="checkbox" class="save_template_only_class" name="save_template_only">
                </label>
            </div>
            <div class="form-group">
                <label for="">اسم النموذج</label>
                <input type="text" class="form-control" name="template_name">
            </div>
        </div>
    </div>
</div>



<div class="panel panel-info main_entry_data_div">
    <div class="panel-heading">
        البيانات الاساسية للقيد
    </div>
    <div class="panel-body">
        <?php

            $normal_tags=array(
                'entry_day_help','entry_type','entry_created_at', 'entry_desc'
            );

            $attrs = generate_default_array_inputs_html(
                $normal_tags,
                "",
                "yes",
                ""
            );


            $attrs[0]["entry_day_help"]="اليومية المساعدة";
            $attrs[0]["entry_type"]="مميز القيد";
            $attrs[0]["entry_desc"]="بيان القيد";
            $attrs[0]["entry_created_at"]="تاريخ القيد *";

            $attrs[2]["entry_created_at"]="required";


            $attrs[3]["entry_desc"]="textarea";
            $attrs[3]["entry_created_at"]="date";


            $attrs[6]["entry_day_help"]="4";
            $attrs[6]["entry_created_at"]="4";
            $attrs[6]["entry_type"]="4";
            $attrs[6]["entry_desc"]="12";


            echo
            generate_inputs_html(
                reformate_arr_without_keys($attrs[0]),
                reformate_arr_without_keys($attrs[1]),
                reformate_arr_without_keys($attrs[2]),
                reformate_arr_without_keys($attrs[3]),
                reformate_arr_without_keys($attrs[4]),
                reformate_arr_without_keys($attrs[5]),
                reformate_arr_without_keys($attrs[6])
            );
        ?>
    </div>
</div>


<div class="panel panel-primary">
    <div class="panel-heading">

        بنود القيد

    </div>
    <div class="panel-body">

        <table class="table table-bordered entries_table">
            <thead>
                <tr class="primary">
                    <th>الحساب</th>
                    <th>دائن</th>
                    <th>مدين</th>
                    <th>مراكز التكلفة</th>
                    <th>مسح</th>
                </tr>
            </thead>

            <tbody>
                @include("accountant.subviews.entries.save_entry_components.entry_item_tr")
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2">
                        <button type="button" class="btn btn-info add_entry_item">
                            اضف بند جديد
                        </button>
                    </td>
                    <td colspan="3">
                        <div class="entry_msgs"></div>
                    </td>
                </tr>
            </tfoot>


        </table>

    </div>
</div>