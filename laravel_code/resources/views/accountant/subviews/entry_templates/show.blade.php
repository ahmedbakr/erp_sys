@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            نماذج القيود
        </div>
        <div class="panel-body">

            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>اسم نموذج القيد</td>
                        <td>بنود النموذج</td>
                        <td>مسح</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($entry_templates as $key=>$template): ?>
                        <tr id="row{{$template->template_id}}">
                            <td>{{$key+1}}</td>
                            <td>{{$template->template_name}}</td>
                            <td>
                                <a href="{{url("/accountant/entry/show_template_items/$template->template_id")}}">بنود النموذج</a>
                            </td>
                            <td>
                                <a href="#" class="btn btn-danger general_remove_item" data-deleteurl="{{url("/general_remove_item")}}" data-tablename="App\models\accountant\entries\entry_templates_m"  data-itemid="<?= $template->template_id ?>" >مسح</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>


            </table>


        </div>
    </div>

@endsection