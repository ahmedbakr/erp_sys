@extends('accountant.main_layout')

@section('subview')



    <div class="panel panel-primary">
        <div class="panel-heading">
            بنود النموذج
        </div>
        <div class="panel-body">

            <table class="table table-bordered entries_table">
                <thead>
                    <tr class="primary">
                        <th>الحساب</th>
                        <th>دائن</th>
                        <th>مدين</th>
                    </tr>
                </thead>

                <tbody>

                    <?php if(isset($template_items)&&is_array($template_items)): ?>
                        <?php foreach($template_items as $key=>$item): ?>
                            <tr class="entry_row">
                                <td class="select_account">
                                    <div class="accounts_div">
                                        <label class="label label-primary selected_individual_item" data-item_id="{{$item->t_item_acc_id}}">
                                            <?=$item->{"acc_name".$en}?>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" readonly="readonly" class="form-control credit_cell" name="credit_cell[]" value="{{($item->t_item_acc_debit_or_credit==1)?$item->t_item_value:0}}">
                                </td>
                                <td>
                                    <input type="number" readonly="readonly" class="form-control debit_cell" name="debit_cell[]" value="{{($item->t_item_acc_debit_or_credit==0)?$item->t_item_value:0}}">
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

@endsection