
<?php
    $data_index_prefix="gen_acc_";
?>
@include("accountant.subviews.general_account_category.accounts_tree.tree_modal")


<div class="panel panel-primary">
    <div class="panel-heading">
        نماذج سابقة
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <?php
                echo generate_select_tags(
                    $field_name="select_entry_template",
                    $label_name="اخنار نموذج للقيود",
                    $text=array_merge([""],convert_inside_obj_to_arr($entry_templates,"template_name")),
                    $values=array_merge([""],convert_inside_obj_to_arr($entry_templates,"template_id")),
                    $selected_value="",
                    $class="form-control select_2_class",
                    $multiple="",
                    $required="",
                    $disabled = "",
                    $data = "",
                    $grid="12"
                );
            ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">اسم النموذج</label>
                <input type="text" class="form-control" name="template_name">
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">

        بنود القيد

    </div>
    <div class="panel-body">

        <table class="table table-bordered entries_table">
            <thead>
                <tr class="primary">
                    <th>الحساب</th>
                    <th>دائن</th>
                    <th>مدين</th>
                    <th>مسح</th>
                </tr>
            </thead>

            <tbody>
                @include("accountant.subviews.entry_templates.save_entry_components.entry_item_tr")
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2">
                        <button type="button" class="btn btn-info add_entry_item">
                            اضف بند جديد
                        </button>
                    </td>
                    <td colspan="3">
                        <div class="entry_msgs"></div>
                    </td>
                </tr>
            </tfoot>


        </table>

    </div>
</div>