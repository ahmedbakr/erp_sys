<?php if(isset($template_items)&&is_array($template_items)): ?>
    <?php foreach($template_items as $key=>$item): ?>
        <tr class="entry_row">

            <td class="select_account">
                <button
                        type="button"
                        class="btn btn-primary show_tree_modal center-block"
                        data-modalclass=".gen_acc_tree_modal"
                        data-select_what="account"
                        data-select_type="single"
                        data-div_to_show_selection=".select_account .accounts_div"
                        data-parent_div_to_show_selection=".entry_row "
                        data-use_this="true"
                        data-input_field_name="selected_account[]"
                >
                    اظهر شجرة الحسابات لتختار الحساب
                </button>
                <div class="accounts_div">

                    <label class="label label-primary selected_individual_item" data-item_id="{{$item->acc_id}}">
                        <?=$item->{"acc_name".$en}?>
                        <a href="#" class="remove_selected_individual_item">×</a>
                        <input type="hidden" name="selected_account[]" value="{{$item->acc_id}}">
                    </label>

                </div>
            </td>
            <td>
                <input type="number" class="form-control credit_cell" name="credit_cell[]" value="{{($item->t_item_acc_debit_or_credit==1)?$item->t_item_value:0}}">
            </td>
            <td>
                <input type="number" class="form-control debit_cell" name="debit_cell[]" value="{{($item->t_item_acc_debit_or_credit==0)?$item->t_item_value:0}}">
            </td>

            <td>
                <button  type="button"  class="btn btn-danger remove_entry_item">مسح البند</button>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr class="entry_row">

    <td class="select_account">
        <button
                type="button"
                class="btn btn-primary show_tree_modal center-block"
                data-modalclass=".gen_acc_tree_modal"
                data-select_what="account"
                data-select_type="single"
                data-div_to_show_selection=".select_account .accounts_div"
                data-parent_div_to_show_selection=".entry_row "
                data-use_this="true"
                data-input_field_name="selected_account[]"
        >
            اظهر شجرة الحسابات لتختار الحساب
        </button>
        <div class="accounts_div"></div>
    </td>
    <td>
        <input type="number" class="form-control credit_cell" name="credit_cell[]">
    </td>
    <td>
        <input type="number" class="form-control debit_cell" name="debit_cell[]">
    </td>
    <td>
        <button  type="button"  class="btn btn-danger remove_entry_item">مسح البند</button>
    </td>
</tr>
<?php endif; ?>
