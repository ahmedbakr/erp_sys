@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            اضف قيد جديد
        </div>
        <div class="panel-body">
            <form class="save_entry_template" action="{{url("/accountant/entry/add_template")}}" method="post">
                @include("accountant.subviews.entry_templates.save_entry_components.save_entry_inner_form")


                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>
        </div>
    </div>


@endsection