@extends('accountant.main_layout')

@section('subview')


    <div class="col-md-12 padd-left padd-right">

        <div class="send-mail text_align_right direction_rtl">
            <h3 class="heading_panel">أرصدة الحسابات</h3>
            <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                <tbody>
                <tr>
                    <td>الرصيد الحالي للخزينة : </td>
                    <td>50000 ريال</td>
                </tr>
                <tr>
                    <td>الرصيد الحالي للبنك الاهلي : </td>
                    <td>10000 ريال</td>
                </tr>
                <tr>
                    <td>الرصيد الحالي لبنك الرياض : </td>
                    <td>40000 ريال</td>
                </tr>
                <tr>
                    <td>رصيد شبكات تحت التحصيل : </td>
                    <td>40000 ريال</td>
                </tr>
                <tr>
                    <td>الرصيد الحالي لصناديق المعارض : </td>
                    <td>40000 ريال</td>
                </tr>
                <tr>
                    <td>إجمالي رصيد العملاء الاجلين : </td>
                    <td>40000 ريال</td>
                </tr>
                <tr>
                    <td>إجمالي رصيد الموردين : </td>
                    <td>40000 ريال</td>
                </tr>
                </tbody>

            </table>
        </div>
    </div>
    <div class="col-md-12 padd-left padd-right">
        <div class="send-mail text_align_right direction_rtl">
            <h3 class="heading_panel">الموظفين</h3>
            <table class="table table-hover paragraph_content direction_rtl remove_margin_bottom">

                <tbody>
                <tr>
                    <td>عدد طلبات الاجازة : </td>
                    <td>200 اجازة</td>
                </tr>
                <tr>
                    <td>عدد الموظفين الغائبين : </td>
                    <td>300 موظف</td>
                </tr>
                </tbody>

            </table>
        </div>
    </div>


@endsection
