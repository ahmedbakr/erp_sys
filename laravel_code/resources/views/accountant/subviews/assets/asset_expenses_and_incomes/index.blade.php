@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات
     {{$expense_or_income}}
            الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/asset_expenses_and_incomes/'.$expense_or_income_url); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>التاريخ</td>
                    <td>القيمة</td>
                    <td>رقم الإيصال</td>
                    <td>طريقة الدفع</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>التاريخ</td>
                    <td>القيمة</td>
                    <td>رقم الإيصال</td>
                    <td>طريقة الدفع</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_exp_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->asset_exp_date}}</td>
                    <td>{{$asset->asset_exp_value}}</td>
                    <td>{{$asset->receipt_number}}</td>
                    <td>
                        <?php if($asset->payment_method == "cash"): ?>
                        نقدي
                        <?php else: ?>
                        شبكة
                        <?php endif; ?>
                    </td>
                    <td>{{$asset->asset_exp_desc}}</td>
                    <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","delete_action")): ?>
                    <td>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/asset_expenses_and_incomes/remove") ?>" data-tablename="App\models\accountant\assets\asset_expenses_and_incomes_m"  data-itemid="<?= $asset->asset_exp_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                    </td>
                    <?php endif; ?>

                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","add_action")): ?>
                <a href="<?= url("accountant/assets/asset_expenses_and_incomes/save/$expense_or_income_url") ?>" class="btn btn-info btn-lg">
                    تسجيل
                    {{$expense_or_income}}
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
