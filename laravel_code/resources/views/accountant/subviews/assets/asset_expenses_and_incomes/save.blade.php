@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل $expense_or_income أصل";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/asset_expenses_and_incomes/save/$expense_or_income_url")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-6">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="payment_method",
                            $label_name="طريقة الدفع *",
                            $text=["نقدي","شبكة"],
                            $values=["cash","network"],
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'asset_exp_value',
                        'receipt_number','asset_exp_date','asset_exp_desc'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $asset_data="",
                        "yes",
                        $required=""
                    );



                    $attrs[0]["asset_exp_value"]="القيمة *";
                    $attrs[0]["asset_exp_desc"]="البيان";
                    $attrs[0]["receipt_number"]="رقم الإيصال";
                    $attrs[0]["asset_exp_date"]="التاريخ *";

                    $attrs[2]["asset_exp_value"]="required";
                    $attrs[2]["asset_exp_date"]="required";

                    $attrs[3]["asset_exp_value"]="number";
                    $attrs[3]["asset_exp_desc"]="textarea";
                    $attrs[3]["receipt_number"]="text";
                    $attrs[3]["asset_exp_date"]="date";

                    $attrs[4]["asset_exp_date"]=date('Y-m-d');


                    $attrs[6]["asset_exp_value"]="4";
                    $attrs[6]["asset_exp_date"]="4";
                    $attrs[6]["receipt_number"]="4";
                    $attrs[6]["asset_exp_desc"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



