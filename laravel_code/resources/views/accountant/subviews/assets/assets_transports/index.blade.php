@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات تنقلات الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_transports'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ بداية النقل</td>
                    <td>نوع النقل</td>
                    <td>من موقع</td>
                    <td>الي موقع</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_transports","edit_action")): ?>
                        <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ بداية النقل</td>
                    <td>نوع النقل</td>
                    <td>من موقع</td>
                    <td>الي موقع</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_transports","edit_action")): ?>
                    <td>تعديل</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_transport_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->asset_buy_date}}</td>
                    <td>{{$asset->asset_buy_amount}}</td>
                    <td>{{$asset->asset_expected_sell_amount}}</td>
                    <td>{{$asset->asset_transport_date}}</td>
                    <td>
                        <?php if($asset->asset_transport_type == "temp"): ?>
                        مؤقت
                            <?php else: ?>
                            دائم
                        <?php endif; ?>
                    </td>
                    <td><?= $asset->{'transport_location_from_name'.$en} ?></td>
                    <td><?= $asset->{'transport_location_to_name'.$en} ?></td>
                    <td>{{$asset->asset_transport_desc}}</td>

                    <?php if(check_permission($user_permissions,"assets/assets_transports","edit_action")): ?>
                    <td>
                        <a href="<?= url("accountant/assets/assets_transports/save/$asset->asset_transport_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                    </td>
                    <?php endif; ?>

                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/assets_transports","add_action")): ?>
                <a href="<?= url("accountant/assets/assets_transports/save") ?>" class="btn btn-info btn-lg">
                    تسجيل عملية نقل أصل
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
