@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل عملية نقل أصل";
    $asset_transport_id="";

    if ($transports!="") {
        $header_text="تعديل نقل للأصل ".$transports->{"asset_title".$en};

        $asset_transport_id=$transports->asset_transport_id;
    }


    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_transports/save/$asset_transport_id")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required=($transports=="")?"required":"disabled",
                            $disabled = "",
                            $data = $transports
                        );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_transport_location_to",
                            $label_name="إختار الموقع *",
                            $text=array_merge(convert_inside_obj_to_arr($all_locations,"asset_location_name$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_locations,"asset_location_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = $transports
                        );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_transport_type",
                            $label_name="نوع النقل *",
                            $text=["مؤقت","دائم"],
                            $values=["temp","always"],
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = $transports
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'asset_transport_date',
                        'asset_transport_desc'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $transports,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_transport_date"]="تاريخ النقل *";
                    $attrs[0]["asset_transport_desc"]="بيان";


                    $attrs[2]["maintenance_start_date"]="required";

                    $attrs[3]["asset_transport_date"]="date";
                    $attrs[3]["asset_transport_desc"]="textarea";


                    $attrs[6]["asset_transport_date"]="6";
                    $attrs[6]["asset_transport_desc"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



