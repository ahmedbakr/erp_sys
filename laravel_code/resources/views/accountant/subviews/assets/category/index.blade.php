@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            المجموعات والفئات للأصول
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>عرض المجموعات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>عرض المجموعات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_cats as $key => $cat): ?>

                <tr id="row<?= $cat->asset_cat_id ?>">
                    <td>{{$cat->asset_cat_code}}</td>
                    <td>{{$cat->asset_cat_name}}</td>
                    <td>{{$cat->asset_cat_name_en}}</td>
                    <td>
                        <?php if($cat->parent_id > 0): ?>
                        مجموعه ليس لها فئات
                        <?php else: ?>
                            <a href="{{url("accountant/assets/category/show/$cat->asset_cat_id")}}">
                                عرض المجموعات
                            </a>

                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"assets/category","edit_action")): ?>
                            <a href="<?= url("accountant/assets/category/save/$cat->asset_cat_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"assets/category","delete_action")): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/category/remove") ?>" data-tablename="App\models\accountant\assets\asset_category_m"  data-itemid="<?= $cat->asset_cat_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/category","add_action")): ?>
                    <a href="<?= url("accountant/assets/category/save") ?>" class="btn btn-info btn-lg">
                        إضافة فئة او مجموعة جديدة
                    </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
