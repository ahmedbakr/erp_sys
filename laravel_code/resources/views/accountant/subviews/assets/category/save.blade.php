@extends('accountant.main_layout')


@section('subview')



    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة فئة او مجموعة جديدة";
    $asset_cat_id="";

    if ($cat_data!="") {
        $header_text="تعديل ".$cat_data->{'asset_cat_name'.$en};

        $asset_cat_id=$cat_data->asset_cat_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/category/save/$asset_cat_id")?>" method="POST" enctype="multipart/form-data">

                    <?php
                        echo
                        generate_select_tags(
                            $field_name="parent_id",
                            $label_name="إختار الفئة لو مجموعة",
                            $text=array_merge(["فئة رئيسية"],convert_inside_obj_to_arr($parent_cats,"asset_cat_name")),
                            $values=array_merge([0],convert_inside_obj_to_arr($parent_cats,"asset_cat_id")),
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="",
                            $disabled = "",
                            $data = $cat_data
                        );
                    ?>

                    <?php

                    $normal_tags=array('asset_cat_code','asset_cat_name','asset_cat_name_en');
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $cat_data,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_cat_code"]="الكود *";
                    $attrs[0]["asset_cat_name"]="الاسم باللغه العربية *";
                    $attrs[0]["asset_cat_name_en"]="الاسم باللغه الانجليزية";


                    $attrs[3]["asset_cat_code"]="text";
                    $attrs[3]["asset_cat_name"]="text";
                    $attrs[3]["asset_cat_name_en"]="text";

                    $attrs[2]["asset_cat_code"]="required";
                    $attrs[2]["asset_cat_name"]="required";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-4">
                            <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/category/save')}}" class="btn btn-primary btn-lg">جديد</a>
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/category/show')}}" class="btn btn-primary btn-lg">الرجوع</a>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>


@endsection



