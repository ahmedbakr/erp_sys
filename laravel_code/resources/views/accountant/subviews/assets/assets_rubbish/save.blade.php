@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text=" عملية تكهين أصل";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_rubbish/save")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <?php

                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'asset_rubbish_date',
                        'asset_rubbish_amount','asset_rubbish_reasons','asset_rubbish_desc'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $installment_data="",
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_rubbish_date"]="تاريخ التكهين *";
                    $attrs[0]["asset_rubbish_amount"]="المبلغ *";
                    $attrs[0]["asset_rubbish_reasons"]="الأسباب";
                    $attrs[0]["asset_rubbish_desc"]="بيان";


                    $attrs[2]["asset_rubbish_date"]="required";
                    $attrs[2]["asset_rubbish_amount"]="required";

                    $attrs[3]["asset_rubbish_date"]="date";
                    $attrs[3]["asset_rubbish_amount"]="number";
                    $attrs[3]["asset_rubbish_reasons"]="textarea";
                    $attrs[3]["asset_rubbish_desc"]="textarea";


                    $attrs[6]["asset_rubbish_date"]="6";
                    $attrs[6]["asset_rubbish_amount"]="6";
                    $attrs[6]["asset_rubbish_reasons"]="12";
                    $attrs[6]["asset_rubbish_desc"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



