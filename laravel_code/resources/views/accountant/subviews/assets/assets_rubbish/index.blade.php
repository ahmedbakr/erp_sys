@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات الأصول التي تم تكهينها
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_rubbish'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ التكهين</td>
                    <td>مبلغ التكهين</td>
                    <td>الاسباب</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rubbish","delete_action")): ?>
                        <td>الغاء التكهين</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ التكهين</td>
                    <td>مبلغ التكهين</td>
                    <td>الاسباب</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rubbish","delete_action")): ?>
                        <td>الغاء التكهين</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_rubbish_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->asset_buy_date}}</td>
                    <td>{{$asset->asset_buy_amount}}</td>
                    <td>{{$asset->asset_expected_sell_amount}}</td>
                    <td>{{$asset->asset_rubbish_date}}</td>
                    <td>{{$asset->asset_rubbish_amount}}</td>
                    <td>{{$asset->asset_rubbish_reasons}}</td>
                    <td>{{$asset->asset_rubbish_desc}}</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rubbish","delete_action")): ?>
                        <td>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/assets_rubbish/remove") ?>" data-tablename="App\models\accountant\assets\assets_rubbish_m"  data-itemid="<?= $asset->asset_rubbish_id ?>"><span class="label label-danger"> إلغاء <i class="fa fa-remove"></i></span></a>
                        </td>
                    <?php endif; ?>
                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/assets_rubbish","add_action")): ?>
                <a href="<?= url("accountant/assets/assets_rubbish/save") ?>" class="btn btn-info btn-lg">
                    تكهين أصل جديد
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
