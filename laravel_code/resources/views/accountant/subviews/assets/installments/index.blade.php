@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات أقساط الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/installments'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>كود الأصل</td>
                        <td>اسم الأصل</td>
                        <td>القسط</td>
                        <td>نوع القسط</td>
                        <td>مده السماح</td>
                        <td>تاريخ الاستحقاق</td>
                        <td>قيمة القسط</td>
                        <td>الإضافات</td>
                        <td>غرامة تأخير</td>
                        <td>الخصميات</td>
                        <td>الإجمالي</td>
                        <td>تاريخ اخر السداد</td>
                        <td>ما تم سدادة</td>
                        <td>المتبقي</td>
                        <?php if(check_permission($user_permissions,"assets/installment","pay_get_installment")): ?>
                            <td>تحصيل/دفع</td>
                        <?php endif; ?>
                        <?php if(check_permission($user_permissions,"assets/installment","edit_action")): ?>
                            <td>تعديل</td>
                        <?php endif; ?>
                        <?php if(check_permission($user_permissions,"assets/installment","delete_action")): ?>
                            <td>مسح</td>
                        <?php endif; ?>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td>#</td>
                        <td>كود الأصل</td>
                        <td>اسم الأصل</td>
                        <td>القسط</td>
                        <td>نوع القسط</td>
                        <td>مده السماح</td>
                        <td>تاريخ الاستحقاق</td>
                        <td>قيمة القسط</td>
                        <td>الإضافات</td>
                        <td>غرامة تأخير</td>
                        <td>الخصميات</td>
                        <td>الإجمالي</td>
                        <td>تاريخ اخر السداد</td>
                        <td>ما تم سدادة</td>
                        <td>المتبقي</td>
                        <?php if(check_permission($user_permissions,"assets/installment","pay_get_installment")): ?>
                            <td>تحصيل/دفع</td>
                        <?php endif; ?>
                        <?php if(check_permission($user_permissions,"assets/installment","edit_action")): ?>
                            <td>تعديل</td>
                        <?php endif; ?>
                        <?php if(check_permission($user_permissions,"assets/installment","delete_action")): ?>
                            <td>مسح</td>
                        <?php endif; ?>
                    </tr>
                </tfoot>

                <tbody>
                <?php foreach ($installments as $key => $installment): ?>

                <tr id="row<?= $installment->asset_install_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $installment->asset_code ?></td>
                    <td><?= $installment->{'asset_title'.$en} ?></td>
                    <td>{{$installment->asset_install_desc}}</td>
                    <td>
                        <?php if($installment->asset_install_type == "sell"): ?>
                        بيع
                            <?php else: ?>
                            شراء
                        <?php endif; ?>
                    </td>
                    <td>{{$installment->asset_install_allowed_days}}</td>
                    <td>{{$installment->asset_install_date}}</td>
                    <td>{{$installment->asset_install_value}}</td>
                    <td>{{$installment->asset_install_additions}}</td>
                    <td>{{$installment->asset_install_delay_fine}}</td>
                    <td>{{$installment->asset_install_discount}}</td>
                    <td>{{$installment->asset_install_total_value}}</td>
                    <td>
                        <?php if(is_null($installment->asset_install_payed_date)): ?>
                        لم يسدد بعد
                            <?php else: ?>
                            {{$installment->asset_install_payed_date}}
                        <?php endif; ?>
                    </td>
                    <td>{{$installment->asset_install_payed_value}}</td>
                    <td>{{$installment->asset_install_remained}}</td>

                    <?php if(check_permission($user_permissions,"assets/installment","pay_get_installment")): ?>
                        <td>
                            <?php if($installment->asset_install_remained == 0): ?>
                            تم السداد
                                <?php else: ?>
                                <a href="<?= url("accountant/assets/installments/pay_get_installment/$installment->asset_install_id") ?>"><span class="label label-info"> تحصيل/دفع <i class="fa fa-money"></i></span></a>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>

                    <?php if(check_permission($user_permissions,"assets/installment","edit_action")): ?>
                        <td>
                            <a href="<?= url("accountant/assets/installments/save/$installment->asset_install_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        </td>
                    <?php endif; ?>


                    <?php if(check_permission($user_permissions,"assets/installment","delete_action")): ?>
                        <td>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/installments/remove") ?>" data-tablename="App\models\accountant\assets\asset_installment_m"  data-itemid="<?= $installment->asset_install_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        </td>
                    <?php endif; ?>

                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <br>
            <div class="col-md-4">
                <?php if(check_permission($user_permissions,"assets","add_action")): ?>
                <a href="<?= url("accountant/assets/save") ?>" class="btn btn-info btn-lg">
                    إضافة أصل جديد
                </a>
                <?php endif; ?>
            </div>

            <div class="col-md-4">
                <?php if(check_permission($user_permissions,"assets/installment","add_action")): ?>
                    <a href="<?= url("accountant/assets/installments/save") ?>" class="btn btn-info btn-lg">
                        تسجيل أقساط محددة لأصل
                    </a>
                <?php endif; ?>
            </div>

            <div class="col-md-4">
                <?php if(check_permission($user_permissions,"assets/installment","add_action")): ?>
                    <a href="<?= url("accountant/assets/installments/save_generated_installments") ?>" class="btn btn-info btn-lg">
                        تسجيل أقساط دورية لأصل
                    </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
