@extends('accountant.main_layout')

@section('subview')


    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل قسط جديد";
    $asset_install_id="";

    if ($installment_data!="") {
        $header_text="تعديل القسط للأصل ".$installment_data->{"asset_title".$en};

        $asset_install_id=$installment_data->asset_install_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/installments/save/$asset_install_id")?>" method="POST" enctype="multipart/form-data">


                    <div class="row">
                        <div class="col-md-6">
                            <?php

                            echo
                            generate_select_tags(
                                $field_name="asset_id",
                                $label_name="إختار الأصل *",
                                $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                                $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                                $selected_value="",
                                $class="form-control select_2_class",
                                $multiple="",
                                $required="required",
                                $disabled = "",
                                $data = $installment_data
                            );
                            ?>
                        </div>

                        <div class="col-md-6">
                            <?php

                            echo
                            generate_select_tags(
                                $field_name="asset_install_type",
                                $label_name="إختار نوع القسط *",
                                $text=["بيع","شراء"],
                                $values=["sell","purchase"],
                                $selected_value="",
                                $class="form-control select_2_class",
                                $multiple="",
                                $required="required",
                                $disabled = "",
                                $data = $installment_data
                            );
                            ?>
                        </div>

                    </div>

                    <div class="row">

                    <?php

                    $normal_tags=array(
                        'asset_install_desc','asset_install_date','asset_install_value',
                        'asset_install_additions','asset_install_discount','asset_install_allowed_days',
                        'asset_install_delay_fine'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $installment_data,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_install_desc"]="بيان";
                    $attrs[0]["asset_install_date"]="تاريخ الاستحقاق *";
                    $attrs[0]["asset_install_value"]="قيمة القسط *";
                    $attrs[0]["asset_install_additions"]="الإضافات";
                    $attrs[0]["asset_install_discount"]="الخصميات";
                    $attrs[0]["asset_install_allowed_days"]="فترة السماح بالأيام";
                    $attrs[0]["asset_install_delay_fine"]="غرامة التأخير بال % ";


                    $attrs[3]["asset_install_desc"]="textarea";
                    $attrs[3]["asset_install_date"]="date";
                    $attrs[3]["asset_install_value"]="number";
                    $attrs[3]["asset_install_additions"]="number";
                    $attrs[3]["asset_install_discount"]="number";
                    $attrs[3]["asset_install_allowed_days"]="number";
                    $attrs[3]["asset_install_delay_fine"]="number";

                    $attrs[2]["asset_install_date"]="required";
                    $attrs[2]["asset_install_value"]="required";

                    $attrs[4]["asset_install_date"]=($installment_data=="")?date('Y-m-d'):$installment_data->asset_install_date;


                    $attrs[6]["asset_install_desc"]="4";
                    $attrs[6]["asset_install_date"]="4";
                    $attrs[6]["asset_install_value"]="4";
                    $attrs[6]["asset_install_additions"]="3";
                    $attrs[6]["asset_install_discount"]="3";
                    $attrs[6]["asset_install_allowed_days"]="3";
                    $attrs[6]["asset_install_delay_fine"]="3";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                    </div>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-4 col-md-offset-3">
                            <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/installments/save')}}" class="btn btn-primary btn-lg">قسط جديد</a>
                        </div>


                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection



