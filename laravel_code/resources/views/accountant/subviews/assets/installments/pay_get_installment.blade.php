@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تحصيل او دفع قسط";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/installments/pay_get_installment/$asset_install_id")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="alert alert-warning" style="text-align: center">
                        <b>
                        اذا كان تاريخ الدفع او التحصيل تجاوز تاريخ استحقاق القسط سيتم زياده القيمة الغرامه علي الاجمالي للقسط وعلي الاضافات لهذا القسط
                        <br>

                        اذا تم ادخال مبلغ بقيمة اعلي من المتبقي سيتم اخذ المتبقي فقط واهمال الباقي
                        </b>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'target_date','target_amount'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $installment_data="",
                        "yes",
                        $required=""
                    );


                    $attrs[0]["target_date"]="تاريخ القسط *";
                    $attrs[0]["target_amount"]="المبلغ *";


                    $attrs[3]["target_date"]="date";
                    $attrs[3]["target_amount"]="number";

                    $attrs[4]["target_date"]=date("Y-m-d");

                    $attrs[6]["target_date"]="6";
                    $attrs[6]["target_amount"]="6";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



