@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل أقساط دورية";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/installments/save_generated_installments")?>" method="POST" enctype="multipart/form-data">

                <div class="panel panel-info">
                    <div class="panel-heading show_collapse_hand collapse_action">البيانات الاساسية</div>
                    <div class="panel-body  collapse collapse_body">

                        <div class="row">
                            <div class="col-md-6">
                                <?php

                                echo
                                generate_select_tags(
                                    $field_name="asset_id",
                                    $label_name="إختار الأصل *",
                                    $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                                    $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                                    $selected_value="",
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>

                            <div class="col-md-6">
                                <?php

                                echo
                                generate_select_tags(
                                    $field_name="asset_install_type",
                                    $label_name="إختار نوع القسط *",
                                    $text=["بيع","شراء"],
                                    $values=["sell","purchase"],
                                    $selected_value="",
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = ""
                                );
                                ?>
                            </div>

                        </div>

                        <div class="row">

                            <?php

                            $normal_tags=array(
                                'asset_install_desc',
                                'asset_install_additions','asset_install_discount','asset_install_allowed_days',
                                'asset_install_delay_fine'
                            );
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $installment_data="",
                                "yes",
                                $required=""
                            );


                            $attrs[0]["asset_install_desc"]="بيان";
                            $attrs[0]["asset_install_additions"]="الإضافات";
                            $attrs[0]["asset_install_discount"]="الخصميات";
                            $attrs[0]["asset_install_allowed_days"]="فترة السماح بالأيام";
                            $attrs[0]["asset_install_delay_fine"]="غرامة التأخير بال % ";


                            $attrs[3]["asset_install_desc"]="textarea";
                            $attrs[3]["asset_install_additions"]="number";
                            $attrs[3]["asset_install_discount"]="number";
                            $attrs[3]["asset_install_allowed_days"]="number";
                            $attrs[3]["asset_install_delay_fine"]="number";


                            $attrs[6]["asset_install_desc"]="6";
                            $attrs[6]["asset_install_additions"]="6";
                            $attrs[6]["asset_install_discount"]="4";
                            $attrs[6]["asset_install_allowed_days"]="4";
                            $attrs[6]["asset_install_delay_fine"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );
                            ?>

                        </div>

                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading show_collapse_hand collapse_action">إنشاء الاقساط *</div>
                    <div class="panel-body  collapse collapse_body">

                        <div class="row">

                            <div class="col-md-3">
                                <label for="">قيمة القسط</label>
                                <input type="number" name="custom_installment_value" required min="0" value="0" step="0.1" class="form-control custom_installment_value">
                            </div>
                            <div class="col-md-3">
                                <label for="">عدد الأقساط</label>
                                <input type="number" name="custom_installment_numbers" required min="1" value="1" step="1" class="form-control custom_installment_numbers">
                            </div>
                            <div class="col-md-3">
                                <label for="">تاريخ أول قسط</label>
                                <input type="date" name="custom_installment_first_date" required class="form-control custom_installment_first_date" value="{{date("Y-m-d")}}">
                            </div>
                            <div class="col-md-2">
                                <label for="">عدد أيام القسط التالي</label>
                                <input type="number" name="custom_next_installment_days" required min="1" value="1" step="1" class="form-control custom_next_installment_days">
                            </div>
                            <div class="col-md-1" style="margin-top: 25px">
                                <button type="button" class="btn btn-success generate_installments_btn">إنشاء</button>
                            </div>

                        </div>
                        <br>
                        <div class="show_generated_installments">

                            <div class="generated_installments_item hide_div">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">قيمة القسط</label>
                                        <input type="text" disabled class="form-control generated_installment_value">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">تاريخ الاستحقاق</label>
                                        <input type="text" disabled class="form-control generated_installment_date">
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" disabled class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



