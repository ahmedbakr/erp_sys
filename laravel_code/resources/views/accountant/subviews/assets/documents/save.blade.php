@extends('accountant.main_layout')

@section('subview')


    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة مستند جديد";
    $doc_id="";

    if ($document_data!="") {
        $header_text="تعديل ".$document_data->doc_code;

        $doc_id=$document_data->doc_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/documents/save/$doc_id")?>" method="POST" enctype="multipart/form-data">


                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <?php

                                echo
                                generate_select_tags(
                                    $field_name="asset_id",
                                    $label_name="إختار الأصل *",
                                    $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                                    $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                                    $selected_value=["$asset_id"],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = ""
                                );
                            ?>
                        </div>

                    </div>


                    <?php

                    $normal_tags=array(
                                        'doc_type','doc_code','doc_start_date',
                                        'doc_end_date','doc_source','doc_desc'
                                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $document_data,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["doc_type"]="نوعة الوثيقة / المستند";
                    $attrs[0]["doc_code"]="الكود *";
                    $attrs[0]["doc_start_date"]="تاريخ الإصدار *";
                    $attrs[0]["doc_end_date"]="تاريخ الإنتهاء *";
                    $attrs[0]["doc_source"]="جهة الإصدار";
                    $attrs[0]["doc_desc"]="بيان";


                    $attrs[3]["doc_type"]="text";
                    $attrs[3]["doc_code"]="text";
                    $attrs[3]["doc_start_date"]="date";
                    $attrs[3]["doc_end_date"]="date";
                    $attrs[3]["doc_source"]="text";
                    $attrs[3]["doc_desc"]="textarea";

                    $attrs[2]["doc_code"]="required";
                    $attrs[2]["doc_start_date"]="required";
                    $attrs[2]["doc_end_date"]="required";

                    $attrs[4]["doc_start_date"]=($document_data=="")?date('Y-m-d'):$document_data->doc_start_date;
                    $attrs[4]["doc_end_date"]=($document_data=="")?date('Y-m-d'):$document_data->doc_end_date;


                    $attrs[6]["doc_type"]="4";
                    $attrs[6]["doc_code"]="4";
                    $attrs[6]["doc_start_date"]="4";
                    $attrs[6]["doc_end_date"]="4";
                    $attrs[6]["doc_source"]="4";
                    $attrs[6]["doc_desc"]="4";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                    <div class="row" style="margin-bottom: 50px;">
                        <?php
                            echo
                            generate_slider_imgs_tags(
                                $slider_photos=(isset($document_data->slider_imgs)&&isset_and_array($document_data->slider_imgs))?$document_data->slider_imgs:"",
                                $field_name="doc_files",
                                $field_label="الملفات",
                                $field_id="doc_files_id",
                                $accept="",
                                $need_alt_title="no",
                                $additional_inputs_arr=array(),
                                $show_as_link=true,
                                $add_item_label="Add File"
                            );
                        ?>
                    </div>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-4 col-md-offset-3">
                            <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/documents/save')}}" class="btn btn-primary btn-lg">جديد</a>
                        </div>


                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection



