@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات مستندات الأصل
            <?= $asset_data->{'asset_title'.$en} ?>
            &nbsp;&nbsp;--&nbsp;&nbsp;&nbsp;
            كود
            {{$asset_data->asset_code}}

        </div>
        <div class="panel-body">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>كود / رقم المستند</td>
                    <td>نوع المستند</td>
                    <td>تاريخ الإصدار</td>
                    <td>تاريخ الإنتهاء</td>
                    <td>جهه الإصدار</td>
                    <td>بيان</td>
                    <td>ملفات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>كود / رقم المستند</td>
                    <td>نوع المستند</td>
                    <td>تاريخ الإصدار</td>
                    <td>تاريخ الإنتهاء</td>
                    <td>جهه الإصدار</td>
                    <td>بيان</td>
                    <td>ملفات</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($documents as $key => $document): ?>

                <tr id="row<?= $document->doc_id ?>">
                    <td>{{$document->doc_code}}</td>
                    <td>{{$document->doc_type}}</td>
                    <td>{{$document->doc_start_date}}</td>
                    <td>
                        <?php if($document->doc_end_date < date('Y-m-d')): ?>

                        <span class="label label-danger">
                            {{$document->doc_end_date}}
                        </span>
                        <?php else: ?>
                        {{$document->doc_end_date}}
                        <?php endif; ?>
                    </td>
                    <td>{{$document->doc_source}}</td>
                    <td>{{$document->doc_desc}}</td>
                    <td>
                        <a href="#" class="show_document_files" data-items="{{json_encode($document->slider_imgs)}}">
                            <span class="label label-success">
                                عرض <i class="fa fa-files-o"></i>
                            </span>
                        </a>
                    </td>

                    <td>
                        <?php if(check_permission($user_permissions,"assets/documents","edit_action")): ?>
                        <a href="<?= url("accountant/assets/documents/save/$document->doc_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"assets/documents","delete_action")): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/documents/remove") ?>" data-tablename="App\models\accountant\documents_m"  data-itemid="<?= $document->doc_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>
            <br>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-info asset_doc_attachment_files hide_div" style="margin-bottom: 50px;">
                        <div class="panel-heading show_collapse_hand collapse_action">ملفات المستند
                        </div>

                        <div class="panel-body collapse in collapse_body container_items">
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/documents","add_action")): ?>
                <a href="<?= url("accountant/assets/documents/save") ?>" class="btn btn-info btn-lg">
                    إضافة مستند جديد
                </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
