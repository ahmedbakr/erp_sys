@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات الأصول التي في الصيانة
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_on_maintenance'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ بداية الصيانة</td>
                    <td>مده الصيانة</td>
                    <td>وصف العطل</td>
                    <td>ما تم إصلاحة</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","is_maintenance_finish")): ?>
                        <td>إنتهاء الصيانة ؟</td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","edit_action")): ?>
                        <td>تعديل</td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>القيمة التخريدية</td>
                    <td>تاريخ بداية الصيانة</td>
                    <td>مده الصيانة</td>
                    <td>وصف العطل</td>
                    <td>ما تم إصلاحة</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","is_maintenance_finish")): ?>
                        <td>إنتهاء الصيانة ؟</td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","edit_action")): ?>
                        <td>تعديل</td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_o_m_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->asset_buy_date}}</td>
                    <td>{{$asset->asset_buy_amount}}</td>
                    <td>{{$asset->asset_expected_sell_amount}}</td>
                    <td>{{$asset->maintenance_start_date}}</td>
                    <td>{{$asset->maintenance_days}}</td>
                    <td>{{$asset->broken_desc}}</td>
                    <td>{{$asset->what_is_fix}}</td>
                    <td>{{$asset->maintenance_note}}</td>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","is_maintenance_finish")): ?>
                        <td>

                            <?php if($asset->is_maintenance_finish == 1): ?>
                                إنتهت
                                <?php else: ?>
                                <?php
                                    echo
                                    generate_multi_accepters(
                                        $accepturl=url( "/accountant/assets/assets_on_maintenance/is_maintenance_finish"),
                                        $item_obj=$asset,
                                        $item_primary_col="asset_o_m_id",
                                        $accept_or_refuse_col="is_maintenance_finish",
                                        $model='App\models\accountant\assets\assets_on_maintenance_m',
                                        $accepters_data=["0"=>"لا","1"=>"نعم"]
                                    );
                                    ?>
                            <?php endif; ?>

                        </td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","edit_action")): ?>
                        <td>
                            <a href="<?= url("accountant/assets/assets_on_maintenance/save/$asset->asset_o_m_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        </td>
                    <?php endif; ?>
                    <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","delete_action")): ?>
                        <td>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/assets_on_maintenance/remove") ?>" data-tablename="App\models\accountant\assets\assets_on_maintenance_m"  data-itemid="<?= $asset->asset_o_m_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        </td>
                    <?php endif; ?>
                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","add_action")): ?>
                <a href="<?= url("accountant/assets/assets_on_maintenance/save") ?>" class="btn btn-info btn-lg">
                   تسجيل عملية صيانة أصل
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
