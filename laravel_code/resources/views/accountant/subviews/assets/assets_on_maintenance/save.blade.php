@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل عملية صيانة أصل";
    $asset_o_m_id="";

    if ($maintenance!="") {
        $header_text="تعديل صيانة للأصل ".$maintenance->{"asset_title".$en};

        $asset_o_m_id=$maintenance->asset_o_m_id;
    }
    

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_on_maintenance/save/$asset_o_m_id")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <?php
                            echo
                            generate_select_tags(
                                $field_name="asset_id",
                                $label_name="إختار الأصل *",
                                $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                                $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                                $selected_value="",
                                $class="form-control select_2_class",
                                $multiple="",
                                $required=($maintenance=="")?"required":"disabled",
                                $disabled = "",
                                $data = $maintenance
                            );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'maintenance_start_date',
                        'maintenance_days','broken_desc','what_is_fix','maintenance_note'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $maintenance,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["maintenance_start_date"]="تاريخ البداية *";
                    $attrs[0]["maintenance_days"]="مدة الصيانة بالأيام *";
                    $attrs[0]["broken_desc"]="وصف العطل";
                    $attrs[0]["what_is_fix"]="ما تم إصلاحة";
                    $attrs[0]["maintenance_note"]="بيان";


                    $attrs[2]["maintenance_start_date"]=($maintenance != "" && $maintenance->is_maintenance_finish ==1)?"disabled":"required";
                    $attrs[2]["maintenance_days"]=($maintenance != "" && $maintenance->is_maintenance_finish ==1)?"disabled":"required";

                    $attrs[3]["maintenance_start_date"]="date";
                    $attrs[3]["maintenance_days"]="number";
                    $attrs[3]["broken_desc"]="textarea";
                    $attrs[3]["what_is_fix"]="textarea";
                    $attrs[3]["maintenance_note"]="textarea";


                    $attrs[6]["maintenance_start_date"]="6";
                    $attrs[6]["maintenance_days"]="6";
                    $attrs[6]["broken_desc"]="12";
                    $attrs[6]["what_is_fix"]="12";
                    $attrs[6]["maintenance_note"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



