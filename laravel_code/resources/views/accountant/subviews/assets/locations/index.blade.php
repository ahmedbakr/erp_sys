@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات المواقع
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_locations as $key => $location): ?>

                <tr id="row<?= $location->asset_location_id ?>">
                    <td>{{$location->asset_location_code}}</td>
                    <td>{{$location->asset_location_name}}</td>
                    <td>{{$location->asset_location_name_en}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"assets/locations","edit_action")): ?>
                            <a href="<?= url("accountant/assets/locations/save/$location->asset_location_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"assets/locations","delete_action")): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/locations/remove") ?>" data-tablename="App\models\accountant\assets\asset_locations_m"  data-itemid="<?= $location->asset_location_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/locations","add_action")): ?>
                    <a href="<?= url("accountant/assets/locations/save") ?>" class="btn btn-info btn-lg">
                        إضافة موقع جديد
                    </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
