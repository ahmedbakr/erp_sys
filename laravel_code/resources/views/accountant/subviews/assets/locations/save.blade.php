@extends('accountant.main_layout')


@section('subview')



    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة موقع جديدة";
    $asset_location_id="";

    if ($location_data!="") {
        $header_text="تعديل ".$location_data->{'asset_location_name'.$en};

        $asset_location_id=$location_data->asset_location_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/locations/save/$asset_location_id")?>" method="POST" enctype="multipart/form-data">

                    <?php

                    $normal_tags=array('asset_location_code','asset_location_name','asset_location_name_en');
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $location_data,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_location_code"]="الكود *";
                    $attrs[0]["asset_location_name"]="الاسم باللغه العربية *";
                    $attrs[0]["asset_location_name_en"]="الاسم باللغه الانجليزية";


                    $attrs[3]["asset_location_code"]="text";
                    $attrs[3]["asset_location_name"]="text";
                    $attrs[3]["asset_location_name_en"]="text";

                    $attrs[2]["asset_location_code"]="required";
                    $attrs[2]["asset_location_name"]="required";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-4">
                            <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/locations/save')}}" class="btn btn-primary btn-lg">جديد</a>
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/locations/show')}}" class="btn btn-primary btn-lg">الرجوع</a>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>


@endsection



