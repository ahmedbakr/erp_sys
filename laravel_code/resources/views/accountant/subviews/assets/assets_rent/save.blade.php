@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل إيجار أصل";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_rent/save")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="payment_method",
                            $label_name="طريقة الدفع *",
                            $text=["نقدي","شبكة"],
                            $values=["cash","network"],
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="assets_rent_period_type",
                            $label_name="نوع مدة الإيجار *",
                            $text=["يوم","شهر","سنة"],
                            $values=["day","month","year"],
                            $selected_value="",
                            $class="form-control",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'assets_rent_start_date',
                        'assets_rent_period_number','assets_rent_amount','assets_rent_tenant_name',
                        'assets_rent_desc'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $asset_data="",
                        "yes",
                        $required=""
                    );



                    $attrs[0]["assets_rent_start_date"]="تاريخ البداية *";
                    $attrs[0]["assets_rent_period_number"]="المدة *";
                    $attrs[0]["assets_rent_amount"]="القيمة *";
                    $attrs[0]["assets_rent_tenant_name"]="اسم المستأجر";
                    $attrs[0]["assets_rent_desc"]="بيان";

                    $attrs[2]["assets_rent_start_date"]="required";
                    $attrs[2]["assets_rent_period_number"]="required";
                    $attrs[2]["assets_rent_amount"]="required";

                    $attrs[3]["assets_rent_start_date"]="date";
                    $attrs[3]["assets_rent_period_number"]="number";
                    $attrs[3]["assets_rent_amount"]="number";
                    $attrs[3]["assets_rent_tenant_name"]="text";
                    $attrs[3]["assets_rent_desc"]="textarea";

                    $attrs[4]["assets_rent_start_date"]=date('Y-m-d');


                    $attrs[6]["assets_rent_start_date"]="3";
                    $attrs[6]["assets_rent_period_number"]="3";
                    $attrs[6]["assets_rent_amount"]="3";
                    $attrs[6]["assets_rent_tenant_name"]="3";
                    $attrs[6]["assets_rent_desc"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



