@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
إيجارات الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_rent'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ البداية</td>
                    <td>مدة الإيجار</td>
                    <td>نوع المده</td>
                    <td>تاريخ الإنتهاء</td>
                    <td>القيمة</td>
                    <td>طريقة الدفع</td>
                    <td>اسم المستأجر</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rent","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ البداية</td>
                    <td>مدة الإيجار</td>
                    <td>نوع المده</td>
                    <td>تاريخ الإنتهاء</td>
                    <td>القيمة</td>
                    <td>طريقة الدفع</td>
                    <td>اسم المستأجر</td>
                    <td>بيان</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rent","delete_action")): ?>
                        <td>مسح</td>
                    <?php endif; ?>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_rent_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->assets_rent_start_date}}</td>
                    <td>{{$asset->assets_rent_period_number}}</td>
                    <td>
                        <?php if($asset->assets_rent_period_type == "day"): ?>
                        يوم
                        <?php endif; ?>
                        <?php if($asset->assets_rent_period_type == "month"): ?>
                        شهر
                        <?php endif; ?>
                        <?php if($asset->assets_rent_period_type == "year"): ?>
                        سنة
                        <?php endif; ?>
                    </td>
                    <td>{{$asset->assets_rent_end_date}}</td>
                    <td>{{$asset->assets_rent_amount}}</td>
                    <td>
                        <?php if($asset->payment_method == "cash"): ?>
                        نقدي
                        <?php else: ?>
                        شبكة
                        <?php endif; ?>
                    </td>
                    <td>{{$asset->assets_rent_tenant_name}}</td>
                    <td>{{$asset->assets_rent_desc}}</td>
                    <?php if(check_permission($user_permissions,"assets/assets_rent","delete_action")): ?>
                    <td>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/assets_rent/remove") ?>" data-tablename="App\models\accountant\assets\assets_rent_m"  data-itemid="<?= $asset->asset_rent_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                    </td>
                    <?php endif; ?>

                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/assets_rent","add_action")): ?>
                <a href="<?= url("accountant/assets/assets_rent/save") ?>" class="btn btn-info btn-lg">
إيجار أصل جديد
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
