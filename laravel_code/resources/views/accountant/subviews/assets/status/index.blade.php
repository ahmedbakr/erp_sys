@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات حالات الاصل
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تعديل</td>
                    <td>مسح</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($all_status as $key => $status): ?>

                <tr id="row<?= $status->asset_status_id ?>">
                    <td>{{$status->asset_status_code}}</td>
                    <td>{{$status->asset_status_name}}</td>
                    <td>{{$status->asset_status_name_en}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"assets/status","edit_action")): ?>
                            <a href="<?= url("accountant/assets/status/save/$status->asset_status_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"assets/status","delete_action")): ?>
                        <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/status/remove") ?>" data-tablename="App\models\accountant\assets\asset_status_m"  data-itemid="<?= $status->asset_status_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/status","add_action")): ?>
                    <a href="<?= url("accountant/assets/status/save") ?>" class="btn btn-info btn-lg">
                        إضافة حالة جديد
                    </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
