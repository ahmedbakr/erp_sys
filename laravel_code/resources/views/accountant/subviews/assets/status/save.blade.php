@extends('accountant.main_layout')


@section('subview')



    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة حالة جديدة";
    $asset_status_id="";

    if ($status_data!="") {
        $header_text="تعديل ".$status_data->{'asset_status_name'.$en};

        $asset_status_id=$status_data->asset_status_id;
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/status/save/$asset_status_id")?>" method="POST" enctype="multipart/form-data">

                    <?php

                    $normal_tags=array('asset_status_code','asset_status_name','asset_status_name_en');
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $status_data,
                        "yes",
                        $required=""
                    );


                    $attrs[0]["asset_status_code"]="الكود *";
                    $attrs[0]["asset_status_name"]="الاسم باللغه العربية *";
                    $attrs[0]["asset_status_name_en"]="الاسم باللغه الانجليزية";


                    $attrs[3]["asset_status_code"]="text";
                    $attrs[3]["asset_status_name"]="text";
                    $attrs[3]["asset_status_name_en"]="text";

                    $attrs[2]["asset_status_code"]="required";
                    $attrs[2]["asset_status_name"]="required";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5])
                    );
                    ?>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-4">
                            <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/status/save')}}" class="btn btn-primary btn-lg">جديد</a>
                        </div>

                        <div class="col-md-4">
                            <a href="{{url('accountant/assets/status/show')}}" class="btn btn-primary btn-lg">الرجوع</a>
                        </div>

                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection



