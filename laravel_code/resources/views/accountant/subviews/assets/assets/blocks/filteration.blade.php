<div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" href="#results_filteration" style="cursor: pointer;"  aria-controls="collapseExample">
   <i class="fa fa-filter"></i>     فلتر النتائج
    </div>
    <div class="panel-body collapse" id="results_filteration">
        <div class="row">

            <form action="{{$url}}">

                <div class="col-md-3">
                    <label for="">الكود</label>
                    <input type="text" name="asset_code" class="form-control" value="{{$asset_code}}">
                </div>

                <div class="col-md-3">
                    <label for="">تاريخ الشراء من :</label>
                    <input type="date" name="from_asset_buy_date" class="form-control"
                           value="{{($from_asset_buy_date == "")?date("Y-m-d"):$from_asset_buy_date}}">
                </div>

                <div class="col-md-3">
                    <label for="">تاريخ الشراء إلي :</label>
                    <input type="date" name="to_asset_buy_date" class="form-control"
                           value="{{($to_asset_buy_date == "")?date("Y-m-d"):$to_asset_buy_date}}">
                </div>

                <div class="col-md-3">
                    <?php

                    echo
                    generate_select_tags(
                        $field_name = "asset_location_id",
                        $label_name = "إختار الموقع",
                        $text = array_merge(["فارغ"], convert_inside_obj_to_arr($all_locations, "asset_location_name")),
                        $values = array_merge([0], convert_inside_obj_to_arr($all_locations, "asset_location_id")),
                        $selected_value = [$asset_location_id],
                        $class = "form-control select_2_class",
                        $multiple = "",
                        $required = "",
                        $disabled = "",
                        $data = ""
                    );

                    ?>
                </div>


                <div class="col-md-4">
                    <label for="">الاسم</label>
                    <input type="text" name="asset_title" class="form-control" value="{{$asset_title}}">
                </div>

                <div class="col-md-4">
                    <label for="">القيمة الدفترية</label>
                    <input type="number" min="0" step="0.01" name="asset_buy_amount" class="form-control" value="{{$asset_buy_amount}}">
                </div>

                <div class="col-md-4" style="margin-top: 22px;">
                    {{csrf_field()}}
                    <input type="submit" class="btn btn-success" value="عرض النتائج">
                </div>


            </form>

        </div>
    </div>
</div>
