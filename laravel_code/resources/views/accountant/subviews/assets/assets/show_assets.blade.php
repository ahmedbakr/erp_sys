@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>مجمع الإهلاك</td>
                    <td>العمر الإفتراضي</td>
                    <td>تعديل</td>
                    <?php if(check_permission($user_permissions,"assets/documents","show_action")): ?>
                        <td>عرض المستندات</td>
                    <?php endif; ?>
                    <!--<td>مسح</td>-->
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>الكود</td>
                    <td>الاسم بالعربي</td>
                    <td>الاسم بالانجليزي</td>
                    <td>تاريخ الشراء</td>
                    <td>القيمة الدفترية</td>
                    <td>مجمع الإهلاك</td>
                    <td>العمر الإفتراضي</td>
                    <td>تعديل</td>
                    <?php if(check_permission($user_permissions,"assets/documents","show_action")): ?>
                        <td>عرض المستندات</td>
                    <?php endif; ?>
                    <!--<td>مسح</td>-->
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_id ?>">
                    <td>{{$asset->asset_code}}</td>
                    <td>{{$asset->asset_title}}</td>
                    <td>{{$asset->asset_title_en}}</td>
                    <td>{{$asset->asset_buy_date}}</td>
                    <td>{{$asset->asset_buy_amount}}</td>
                    <td>{{$asset->asset_all_deprecation_values}}</td>
                    <td>{{$asset->asset_age}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"assets","edit_action")): ?>
                        <a href="<?= url("accountant/assets/save/$asset->asset_id") ?>"><span class="label label-info"> تعديل <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>

                    <?php if(check_permission($user_permissions,"assets/documents","show_action")): ?>
                        <td>
                            <a href="<?= url("accountant/assets/documents/$asset->asset_id") ?>"><span class="label label-info"> عرض <i class="fa fa-send-o"></i></span></a>
                        </td>
                    <?php endif; ?>

                    <?php if(1!=1): ?>
                        <td>
                            <?php if(check_permission($user_permissions,"assets","delete_action")): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("accountant/assets/remove") ?>" data-tablename="App\models\accountant\assets\asset_m"  data-itemid="<?= $asset->asset_id ?>"><span class="label label-danger"> مسح <i class="fa fa-remove"></i></span></a>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

            <br>
            <div class="col-md-4 col-md-offset-2">
                <?php if(check_permission($user_permissions,"assets","add_action")): ?>
                <a href="<?= url("accountant/assets/save") ?>" class="btn btn-info btn-lg">
                    إضافة أصل جديد
                </a>
                <?php endif; ?>
            </div>

            <div class="col-md-4 col-md-offset-2">
                <?php if(check_permission($user_permissions,"assets/documents","add_action")): ?>
                <a href="<?= url("accountant/assets/documents/save") ?>" class="btn btn-info btn-lg">
                    إضافة مستند أصل
                </a>
                <?php endif; ?>
            </div>

        </div>
    </div>




@endsection
