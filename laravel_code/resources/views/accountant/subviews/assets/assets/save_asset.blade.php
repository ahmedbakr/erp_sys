@extends('accountant.main_layout')


@section('subview')

    @include("accountant.subviews.general_account_category.accounts_tree.tree_modal")

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="إضافة أصل جديد";
    $asset_id="";
    $asset_img_path = "";
    $disabled_upload_imgs="";
    if ($asset_data!="")
    {
        $header_text="تعديل ".$asset_data->{'asset_title'.$en};

        $asset_id=$asset_data->asset_id;
        if (!empty($asset_data->asset_img_path))
        {
            $asset_img_path = url($asset_data->asset_img_path);
            $disabled_upload_imgs="disabled";
        }
    }

    //dump($pro_data);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body" style="text-align: center">
            <div class="">
                <form id="save_form" action="<?=url("accountant/assets/save/$asset_id")?>" method="POST" enctype="multipart/form-data">


                    <div class="panel panel-info" style="margin-bottom: 50px;margin-top: 25px;">
                        <div class="panel-heading show_collapse_hand collapse_action">البيانات الأساسية</div>
                        <div class="panel-body collapse collapse_body" id="asset_basic">

                            <div class="row">
                                <div class="col-md-6" style="float: right;">
                                    <?php

                                    echo generate_img_tags_for_form(
                                        $filed_name="asset_img_file",
                                        $filed_label="asset_img_file",
                                        $required_field="",
                                        $checkbox_field_name="asset_img_checkbox",
                                        $need_alt_title="no",
                                        $required_alt_title="",
                                        $old_path_value=$asset_img_path,
                                        $old_title_value="",
                                        $old_alt_value="",
                                        $recomended_size="",
                                        $disalbed=$disabled_upload_imgs,
                                        $displayed_img_width="100",
                                        $display_label="إرفع صورة للأصل",
                                        $img_obj = ""
                                    );

                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php if($asset_data == ""): ?>

                                                <button
                                                        type="button"
                                                        class="btn btn-primary show_tree_modal center-block"
                                                        data-select_what="account"
                                                        data-select_type="single"
                                                        data-div_to_show_selection=".move_to_new_asset_acc_div"
                                                        data-input_field_name="asset_acc">
                                                    إختار حساب الأصل
                                                </button>
                                                <div class="move_to_new_asset_acc_div" style="text-align: center;">

                                                </div>

                                                <?php else: ?>
                                                <label for="">حساب الأصل</label>
                                                <div class="move_to_new_asset_acc_div" style="text-align: center;">
                                                    <input type="text" class="form-control" name="asset_acc"
                                                           disabled="disabled" value="<?= $asset_data->{"asset_account_name".$en} ?>">
                                                </div>

                                            <?php endif; ?>




                                        </div>
                                        <div class="col-md-6">

                                            <?php if($asset_data == ""): ?>

                                                <button
                                                        type="button"
                                                        class="btn btn-primary show_tree_modal center-block"
                                                        data-select_what="account"
                                                        data-select_type="single"
                                                        data-div_to_show_selection=".move_to_new_asset_deprecation_acc_div"
                                                        data-input_field_name="asset_deprecation_acc">
                                                    إختار حساب مجمع الإهلاك
                                                </button>


                                                <div class="move_to_new_asset_deprecation_acc_div" style="text-align: center;">

                                                </div>

                                            <?php else: ?>
                                            <div class="move_to_new_asset_deprecation_acc_div" style="text-align: center;">
                                                <label for="">حساب مجمع الاهلاك</label>
                                                <input type="text" class="form-control" disabled="disabled"
                                                       name="asset_deprecation_acc" value="<?= $asset_data->{"asset_deprecation_account_name".$en} ?>">
                                            </div>

                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php

                                $parent_cats_text = convert_inside_obj_to_arr($parent_cats,"asset_cat_name$en");
                                $parent_cats_ids = convert_inside_obj_to_arr($parent_cats,"asset_cat_id");

                                $child_cats_text = convert_inside_obj_to_arr($child_cats,"asset_cat_name$en");
                                $child_cats_ids = convert_inside_obj_to_arr($child_cats,"asset_cat_id");

                                echo generate_depended_selects(
                                    $field_name_1="parent_id",
                                    $field_label_1="إختار الفئة *",
                                    $field_text_1=$parent_cats_text,
                                    $field_values_1=$parent_cats_ids,
                                    $field_selected_value_1=(is_object($asset_data)?$asset_data->asset_parent_cat_id:""),
                                    $field_required_1="",
                                    $field_class_1="form-control",
                                    $field_name_2="asset_cat_id",
                                    $field_label_2="إختار المجموعة *",
                                    $field_text_2=$child_cats_text,
                                    $field_values_2=$child_cats_ids,
                                    $field_selected_value_2=(is_object($asset_data)?$asset_data->asset_child_cat_id:""),
                                    $field_2_depend_values=$parent_cats_ids,
                                    $field_required_2="",
                                    $field_class_2="form-control"
                                );

                                ?>
                            </div>

                            <div class="col-md-4">
                                <?php
                                echo
                                generate_select_tags(
                                    $field_name="asset_location_id",
                                    $label_name="إختار الموقع *",
                                    $text=array_merge(convert_inside_obj_to_arr($all_locations,"asset_location_name$en")),
                                    $values=array_merge(convert_inside_obj_to_arr($all_locations,"asset_location_id")),
                                    $selected_value="",
                                    $class="form-control",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = $asset_data
                                );
                                ?>
                            </div>

                            <div class="col-md-4">
                                <?php
                                echo
                                generate_select_tags(
                                    $field_name="asset_status_id",
                                    $label_name="إختار الحالة *",
                                    $text=array_merge(convert_inside_obj_to_arr($all_status,"asset_status_name$en")),
                                    $values=array_merge(convert_inside_obj_to_arr($all_status,"asset_status_id")),
                                    $selected_value="",
                                    $class="form-control",
                                    $multiple="",
                                    $required="required",
                                    $disabled = "",
                                    $data = $asset_data
                                );
                                ?>
                            </div>

                            <div class="col-md-4">
                                <?php
                                echo
                                generate_select_tags(
                                    $field_name="asset_deprecation_type",
                                    $label_name="إختار طريقة الاهلاك *",
                                    $text=array_values($deprecation_types),
                                    $values=array_keys($deprecation_types),
                                    $selected_value="",
                                    $class="form-control select_asset_deprecation_type",
                                    $multiple="",
                                    $required="$disable_fields",
                                    $disabled = "",
                                    $data = $asset_data
                                );
                                ?>
                            </div>

                            <?php


                            $normal_tags=array(
                                'asset_code','asset_title','asset_title_en',
                                'asset_buy_source','asset_buy_source_en','asset_buy_date',
                                'asset_total_expenses','asset_buy_amount','asset_expected_sell_amount','asset_age',
                                'asset_all_deprecation_values','asset_changes_value','asset_assurnace_period',
                                'asset_working_units','asset_desc','asset_desc_en',
                            );
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $asset_data,
                                "yes",
                                $required=""
                            );


                            $attrs[0]["asset_code"]="الكود *";
                            $attrs[0]["asset_title"]="الاسم باللغه العربية *";
                            $attrs[0]["asset_title_en"]="الاسم باللغه الانجليزية";

                            $attrs[0]["asset_desc"]="الوصف باللغه العربية";
                            $attrs[0]["asset_desc_en"]="الوصف باللغه الانجليزية";

                            $attrs[0]["asset_buy_source"]="جهه الشراء باللغه العربية";
                            $attrs[0]["asset_buy_source_en"]="جهه الشراء باللغه الانجليزية";

                            $attrs[0]["asset_buy_date"]="تاريخ الشراء *";
                            $attrs[0]["asset_total_expenses"]="مصاريف الأصل";
                            $attrs[0]["asset_buy_amount"]="القيمة الدفترية *";
                            $attrs[0]["asset_expected_sell_amount"]="القيمة التخريدية *";
                            $attrs[0]["asset_age"]="العمر الإفتراضي *";
                            $attrs[0]["asset_all_deprecation_values"]="مجمع الإهلاك";
                            $attrs[0]["asset_changes_value"]="فيمةالتغييرات";
                            $attrs[0]["asset_assurnace_period"]="فترة الضمان";
                            $attrs[0]["asset_working_units"]="ساعات التشغيل / وحدات الانتاج *";


                            $attrs[3]["asset_code"]="text";
                            $attrs[3]["asset_title"]="text";
                            $attrs[3]["asset_title_en"]="text";
                            $attrs[3]["asset_desc"]="textarea";
                            $attrs[3]["asset_desc_en"]="textarea";
                            $attrs[3]["asset_buy_source"]="text";
                            $attrs[3]["asset_buy_source_en"]="text";

                            $attrs[3]["asset_buy_date"]="date";
                            $attrs[3]["asset_total_expenses"]="number";
                            $attrs[3]["asset_buy_amount"]="number";
                            $attrs[3]["asset_expected_sell_amount"]="number";
                            $attrs[3]["asset_age"]="number";
                            $attrs[3]["asset_all_deprecation_values"]="number";
                            $attrs[3]["asset_changes_value"]="number";
                            $attrs[3]["asset_assurnace_period"]="number";
                            $attrs[3]["asset_working_units"]="number";

                            $attrs[2]["asset_buy_date"]="$disable_fields";
                            $attrs[2]["asset_total_expenses"]="disabled";
                            $attrs[2]["asset_buy_amount"]="$disable_fields";
                            $attrs[2]["asset_expected_sell_amount"]="$disable_fields";
                            $attrs[2]["asset_age"]="$disable_fields";
                            $attrs[2]["asset_all_deprecation_values"]=($asset_data!= "")?"disabled":"";
                            $attrs[2]["asset_working_units"]="$disable_fields";

                            $attrs[2]["asset_code"]="required";
                            $attrs[2]["asset_title"]="required";

                            $attrs[4]["asset_buy_date"]=($asset_data=="")?date('Y-m-d'):$asset_data->asset_buy_date;
                            $attrs[4]["asset_total_expenses"]=($asset_data=="")?0:$asset_data->asset_total_expenses;

                            $attrs[5]["asset_buy_date"] .=" asset_buy_date";
                            $attrs[5]["asset_age"] .=" asset_age";

                            $attrs[6]["asset_code"]="4";
                            $attrs[6]["asset_title"]="4";
                            $attrs[6]["asset_title_en"]="4";
                            $attrs[6]["asset_desc"]="4";
                            $attrs[6]["asset_desc_en"]="4";
                            $attrs[6]["asset_buy_source"]="4";
                            $attrs[6]["asset_buy_source_en"]="4";

                            $attrs[6]["asset_buy_date"]="4";
                            $attrs[6]["asset_total_expenses"]="3";
                            $attrs[6]["asset_buy_amount"]="3";
                            $attrs[6]["asset_expected_sell_amount"]="3";
                            $attrs[6]["asset_age"]="3";
                            $attrs[6]["asset_all_deprecation_values"]="4";
                            $attrs[6]["asset_changes_value"]="4";
                            $attrs[6]["asset_assurnace_period"]="4";
                            $attrs[6]["asset_working_units"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );
                            ?>
                        </div>
                    </div>


                    <div class="panel panel-info" style="margin-bottom: 50px;">
                        <div class="panel-heading show_collapse_hand collapse_action">البيانات الإضافية</div>

                        <div class="panel-body collapse collapse_body" id="asset_additional">

                            <?php

                            $normal_tags=array(
                                'asset_piece_number',
                                'asset_model_num','asset_responsable','asset_guard_name',
                                'asset_address','asset_license_number','asset_east_edge',
                                'asset_east_edge_en','asset_west_edge','asset_west_edge_en',
                                'asset_north_edge','asset_north_edge_en','asset_south_edge','asset_south_edge_en'
                            );
                            $attrs = generate_default_array_inputs_html(
                                $normal_tags,
                                $asset_data,
                                "yes",
                                $required=""
                            );



                            $attrs[0]["asset_piece_number"]="رقم القطعة / اللوحة";
                            $attrs[0]["asset_model_num"]="الموديل";
                            $attrs[0]["asset_responsable"]="المشغل / امين العهدة";
                            $attrs[0]["asset_guard_name"]="اسم الحارس";
                            $attrs[0]["asset_address"]="العنوان";
                            $attrs[0]["asset_license_number"]="الرخصه";
                            $attrs[0]["asset_east_edge"]="الحد الشرقي بالعربي";
                            $attrs[0]["asset_east_edge_en"]="الحد الشرقي بالانجليزي";
                            $attrs[0]["asset_west_edge"]="الحد الغربي بالانجليزي";
                            $attrs[0]["asset_west_edge_en"]="الحد الغربي بالانجليزي";
                            $attrs[0]["asset_north_edge"]="الحد الشمالي بالانجليزي";
                            $attrs[0]["asset_north_edge_en"]="الحد الشمالي بالانجليزي";
                            $attrs[0]["asset_south_edge"]="الحد الجنوبي بالانجليزي";
                            $attrs[0]["asset_south_edge_en"]="الحد الجنوبي بالانجليزي";


                            $attrs[3]["asset_piece_number"]="number";
                            $attrs[3]["asset_model_num"]="number";
                            $attrs[3]["asset_responsable"]="text";
                            $attrs[3]["asset_guard_name"]="text";
                            $attrs[3]["asset_address"]="text";
                            $attrs[3]["asset_license_number"]="text";
                            $attrs[3]["asset_east_edge"]="text";
                            $attrs[3]["asset_east_edge_en"]="text";
                            $attrs[3]["asset_west_edge"]="text";
                            $attrs[3]["asset_west_edge_en"]="text";
                            $attrs[3]["asset_north_edge"]="text";
                            $attrs[3]["asset_north_edge_en"]="text";
                            $attrs[3]["asset_south_edge"]="text";
                            $attrs[3]["asset_south_edge_en"]="text";



                            $attrs[6]["asset_piece_number"]="4";
                            $attrs[6]["asset_model_num"]="4";
                            $attrs[6]["asset_responsable"]="4";
                            $attrs[6]["asset_guard_name"]="4";
                            $attrs[6]["asset_address"]="4";
                            $attrs[6]["asset_license_number"]="4";
                            $attrs[6]["asset_east_edge"]="4";
                            $attrs[6]["asset_east_edge_en"]="4";
                            $attrs[6]["asset_west_edge"]="4";
                            $attrs[6]["asset_west_edge_en"]="4";
                            $attrs[6]["asset_north_edge"]="4";
                            $attrs[6]["asset_north_edge_en"]="4";
                            $attrs[6]["asset_south_edge"]="4";
                            $attrs[6]["asset_south_edge_en"]="4";

                            echo
                            generate_inputs_html(
                                reformate_arr_without_keys($attrs[0]),
                                reformate_arr_without_keys($attrs[1]),
                                reformate_arr_without_keys($attrs[2]),
                                reformate_arr_without_keys($attrs[3]),
                                reformate_arr_without_keys($attrs[4]),
                                reformate_arr_without_keys($attrs[5]),
                                reformate_arr_without_keys($attrs[6])
                            );
                            ?>

                        </div>
                    </div>


                    <div class="panel panel-info" style="margin-bottom: 50px;">
                        <div class="panel-heading show_collapse_hand collapse_action">مصاريف الأصل

                            <?php if($asset_data==""): ?>
                                <a href="#" class="btn btn-success pull-right clone_item_to_be_repeated"
                                   style="margin-top: -7px"> إضافة جديد <i class="fa fa-plus"></i> </a>
                            <?php endif; ?>

                        </div>

                        <div class="panel-body collapse collapse_body container_items">

                            <?php if(count($asset_expenses)): ?>

                                <?php foreach($asset_expenses as $key => $expense): ?>

                                    <div class="panel panel-info item_to_be_repeated" style="margin-bottom: 15px;">
                                        <div class="panel-heading show_collapse_hand collapse_action">عنصر رقم {{($key+1)}}</div>

                                        <div class="panel-body collapse_body collapse">

                                            <div class="row">

                                                <div class="col-md-2">
                                                    <?php
                                                    echo
                                                    generate_select_tags(
                                                        $field_name="payment_method[]",
                                                        $label_name="طريقة الدفع",
                                                        $text=["نقدي","شبكة"],
                                                        $values=["cash","network"],
                                                        $selected_value=[$expense->payment_method],
                                                        $class="form-control",
                                                        $multiple="",
                                                        $required="required",
                                                        $disabled = "disabled",
                                                        $data = ""
                                                    );
                                                    ?>
                                                </div>

                                                <?php

                                                $normal_tags=array(
                                                    'asset_exp_value[]',
                                                    'asset_exp_desc[]','receipt_number[]','asset_exp_date[]'
                                                );
                                                $attrs = generate_default_array_inputs_html(
                                                    $normal_tags,
                                                    $temp_data="",
                                                    "yes",
                                                    $required=""
                                                );



                                                $attrs[0]["asset_exp_value[]"]="القيمة";
                                                $attrs[0]["asset_exp_desc[]"]="البيان";
                                                $attrs[0]["receipt_number[]"]="رقم الإيصال";
                                                $attrs[0]["asset_exp_date[]"]="التاريخ";


                                                $attrs[2]["asset_exp_value[]"]="disabled";
                                                $attrs[2]["asset_exp_desc[]"]="disabled";
                                                $attrs[2]["receipt_number[]"]="disabled";
                                                $attrs[2]["asset_exp_date[]"]="disabled";

                                                $attrs[3]["asset_exp_value[]"]="number";
                                                $attrs[3]["asset_exp_desc[]"]="text";
                                                $attrs[3]["receipt_number[]"]="text";
                                                $attrs[3]["asset_exp_date[]"]="date";


                                                $attrs[4]["asset_exp_value[]"]=$expense->asset_exp_value;
                                                $attrs[4]["asset_exp_desc[]"]=$expense->asset_exp_desc;
                                                $attrs[4]["receipt_number[]"]=$expense->receipt_number;
                                                $attrs[4]["asset_exp_date[]"]=$expense->asset_exp_date;

                                                $attrs[6]["asset_exp_value[]"]="2";
                                                $attrs[6]["asset_exp_desc[]"]="3";
                                                $attrs[6]["receipt_number[]"]="2";
                                                $attrs[6]["asset_exp_date[]"]="3";


                                                echo
                                                generate_inputs_html(
                                                    reformate_arr_without_keys($attrs[0]),
                                                    reformate_arr_without_keys($attrs[1]),
                                                    reformate_arr_without_keys($attrs[2]),
                                                    reformate_arr_without_keys($attrs[3]),
                                                    reformate_arr_without_keys($attrs[4]),
                                                    reformate_arr_without_keys($attrs[5]),
                                                    reformate_arr_without_keys($attrs[6])
                                                );
                                                ?>

                                            </div>

                                        </div>
                                    </div>

                                <?php endforeach; ?>

                                <?php else: ?>
                                <?php if($asset_data!=""): ?>
                                    <div class="alert alert-danger">لا توجد مصروفات لهذا الأصل</div>
                                <?php endif; ?>

                            <?php endif; ?>


                            <?php if($asset_data==""): ?>

                                <div class="panel panel-info item_to_be_repeated" style="margin-bottom: 15px;">
                                    <a href="" class="btn btn-danger pull-right delete_item_to_be_repeated"> X </a>
                                    <div class="panel-heading show_collapse_hand collapse_action">عنصر</div>

                                    <div class="panel-body collapse_body collapse" id="asset_expenses">

                                        <div class="row">

                                            <div class="col-md-2">
                                                <?php
                                                echo
                                                generate_select_tags(
                                                    $field_name="payment_method[]",
                                                    $label_name="طريقة الدفع",
                                                    $text=["نقدي","شبكة"],
                                                    $values=["cash","network"],
                                                    $selected_value="",
                                                    $class="form-control",
                                                    $multiple="",
                                                    $required="required",
                                                    $disabled = "",
                                                    $data = ""
                                                );
                                                ?>
                                            </div>

                                            <?php

                                            $normal_tags=array(
                                                'asset_exp_value[]',
                                                'asset_exp_desc[]','receipt_number[]','asset_exp_date[]'
                                            );
                                            $attrs = generate_default_array_inputs_html(
                                                $normal_tags,
                                                $asset_data,
                                                "yes",
                                                $required=""
                                            );



                                            $attrs[0]["asset_exp_value[]"]="القيمة";
                                            $attrs[0]["asset_exp_desc[]"]="البيان";
                                            $attrs[0]["receipt_number[]"]="رقم الإيصال";
                                            $attrs[0]["asset_exp_date[]"]="التاريخ";

                                            $attrs[3]["asset_exp_value[]"]="number";
                                            $attrs[3]["asset_exp_desc[]"]="text";
                                            $attrs[3]["receipt_number[]"]="text";
                                            $attrs[3]["asset_exp_date[]"]="date";

                                            $attrs[4]["asset_exp_date[]"]=date('Y-m-d');


                                            $attrs[6]["asset_exp_value[]"]="2";
                                            $attrs[6]["asset_exp_desc[]"]="3";
                                            $attrs[6]["receipt_number[]"]="2";
                                            $attrs[6]["asset_exp_date[]"]="3";

                                            echo
                                            generate_inputs_html(
                                                reformate_arr_without_keys($attrs[0]),
                                                reformate_arr_without_keys($attrs[1]),
                                                reformate_arr_without_keys($attrs[2]),
                                                reformate_arr_without_keys($attrs[3]),
                                                reformate_arr_without_keys($attrs[4]),
                                                reformate_arr_without_keys($attrs[5]),
                                                reformate_arr_without_keys($attrs[6])
                                            );
                                            ?>


                                        </div>

                                    </div>
                                </div>

                            <?php endif; ?>


                        </div>
                    </div>


                    <div class="panel panel-info asset_deprecation_rules {{(count($asset_deprecation_rules)?'':'hide_div')}}" style="margin-bottom: 50px;">
                        <div class="panel-heading show_collapse_hand collapse_action">قواعد إهلاكات الأصل

                            <?php if(!count($asset_deprecation_rules)): ?>
                                <a href="#" class="btn btn-success pull-right regenerate_deprecation_rules"
                                   style="margin-top: -7px"> تجديد <i class="fa fa-refresh"></i> </a>
                            <?php endif; ?>

                        </div>

                        <div class="panel-body collapse collapse_body container_items">

                            <?php if(count($asset_deprecation_rules)): ?>

                                <?php foreach($asset_deprecation_rules as $key => $asset_deprecation_rule): ?>

                                    <div class="row">

                                        <?php

                                        $normal_tags=array(
                                            'asset_rule_from_date[]',
                                            'asset_rule_to_date[]','real_working_units[]'
                                        );
                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $temp_data="",
                                            "yes",
                                            $required=""
                                        );

                                        $disabled_attr = "";
                                        if ($asset_deprecation_rule->is_closed_to_edit)
                                        {
                                            $disabled_attr = "readonly";
                                        }


                                        $attrs[0]["asset_rule_from_date[]"]="من تاريخ ";
                                        $attrs[0]["asset_rule_to_date[]"]="إلي تاريخ ";
                                        $attrs[0]["real_working_units[]"]="ساعات/وحدات العمل الفعلية ";


                                        $attrs[2]["asset_rule_from_date[]"]="disabled";
                                        $attrs[2]["asset_rule_to_date[]"]="disabled";
                                        $attrs[2]["real_working_units[]"]="$disabled_attr";

                                        $attrs[3]["asset_rule_from_date[]"]="date";
                                        $attrs[3]["asset_rule_to_date[]"]="date";
                                        $attrs[3]["real_working_units[]"]="number";


                                        $attrs[4]["asset_rule_from_date[]"]=$asset_deprecation_rule->asset_rule_from_date;
                                        $attrs[4]["asset_rule_to_date[]"]=$asset_deprecation_rule->asset_rule_to_date;
                                        $attrs[4]["real_working_units[]"]=$asset_deprecation_rule->real_working_units;

                                        $attrs[6]["asset_rule_from_date[]"]="4";
                                        $attrs[6]["asset_rule_to_date[]"]="4";
                                        $attrs[6]["real_working_units[]"]="4";


                                        echo
                                        generate_inputs_html(
                                            reformate_arr_without_keys($attrs[0]),
                                            reformate_arr_without_keys($attrs[1]),
                                            reformate_arr_without_keys($attrs[2]),
                                            reformate_arr_without_keys($attrs[3]),
                                            reformate_arr_without_keys($attrs[4]),
                                            reformate_arr_without_keys($attrs[5]),
                                            reformate_arr_without_keys($attrs[6])
                                        );
                                        ?>

                                    </div>

                                <?php endforeach; ?>

                            <?php else: ?>

                            <?php if($asset_data == ""): ?>

                                <div class="row asset_deprecation_rules_item">
                                    <?php

                                    $normal_tags=array(
                                        'asset_rule_from_date[]',
                                        'asset_rule_to_date[]','real_working_units[]'
                                    );
                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $temp_data="",
                                        "yes",
                                        $required=""
                                    );


                                    $attrs[0]["asset_rule_from_date[]"]="من تاريخ ";
                                    $attrs[0]["asset_rule_to_date[]"]="إلي تاريخ ";
                                    $attrs[0]["real_working_units[]"]="ساعات/وحدات العمل الفعلية ";

                                    $attrs[3]["asset_rule_from_date[]"]="date";
                                    $attrs[3]["asset_rule_to_date[]"]="date";
                                    $attrs[3]["real_working_units[]"]="number";

                                    $attrs[2]["asset_rule_from_date[]"]="disabled";
                                    $attrs[2]["asset_rule_to_date[]"]="disabled";

                                    $attrs[6]["asset_rule_from_date[]"]="4";
                                    $attrs[6]["asset_rule_to_date[]"]="4";
                                    $attrs[6]["real_working_units[]"]="4";;

                                    $attrs[5]["asset_rule_from_date[]"]="asset_rule_from_date";
                                    $attrs[5]["asset_rule_to_date[]"]="asset_rule_to_date";


                                    echo
                                    generate_inputs_html(
                                        reformate_arr_without_keys($attrs[0]),
                                        reformate_arr_without_keys($attrs[1]),
                                        reformate_arr_without_keys($attrs[2]),
                                        reformate_arr_without_keys($attrs[3]),
                                        reformate_arr_without_keys($attrs[4]),
                                        reformate_arr_without_keys($attrs[5]),
                                        reformate_arr_without_keys($attrs[6])
                                    );
                                    ?>
                                </div>

                            <?php endif; ?>


                            <?php endif; ?>

                        </div>
                    </div>

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-12">
                            <div class="col-md-4">
                                <input type="submit" value="حفظ" class="btn btn-primary btn-lg">
                            </div>

                            <div class="col-md-4">
                                <a href="{{url('accountant/assets/save')}}" class="btn btn-primary btn-lg">جديد</a>
                            </div>

                            <div class="col-md-4">
                                <a href="{{url('accountant/assets')}}" class="btn btn-primary btn-lg">الرجوع</a>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>


@endsection



