@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل التغييرات في الأصل";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_that_reevaluated/save")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <?php
                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'value_before_change',
                        'change_value','change_date','operation_desc','note_desc'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $asset_data="",
                        "yes",
                        $required=""
                    );



                    $attrs[0]["value_before_change"]="القيمة قبل التغيير *";
                    $attrs[0]["change_value"]="قيمة التغيير";
                    $attrs[0]["change_date"]="التاريخ *";
                    $attrs[0]["operation_desc"]="وصف العميلة";
                    $attrs[0]["note_desc"]="بيان";

                    $attrs[2]["value_before_change"]="required";
                    $attrs[2]["change_value"]="required";
                    $attrs[2]["change_date"]="required";

                    $attrs[3]["value_before_change"]="number";
                    $attrs[3]["change_value"]="number";
                    $attrs[3]["change_date"]="date";
                    $attrs[3]["operation_desc"]="textarea";
                    $attrs[3]["note_desc"]="textarea";

                    $attrs[4]["change_date"]=date('Y-m-d');


                    $attrs[6]["value_before_change"]="4";
                    $attrs[6]["change_value"]="4";
                    $attrs[6]["change_date"]="4";
                    $attrs[6]["operation_desc"]="12";
                    $attrs[6]["note_desc"]="12";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



