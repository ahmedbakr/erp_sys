@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
التغييرات في الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_that_reevaluated'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ التغيير</td>
                    <td>القيمة قبل التغيير</td>
                    <td>قيمة التغيير</td>
                    <td>القيمة بعد التغيير</td>
                    <td>وصف العملية</td>
                    <td>بيان</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>تاريخ التغيير</td>
                    <td>القيمة قبل التغيير</td>
                    <td>قيمة التغيير</td>
                    <td>القيمة بعد التغيير</td>
                    <td>وصف العملية</td>
                    <td>بيان</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->reev_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$asset->change_date}}</td>
                    <td>{{$asset->value_before_change}}</td>
                    <td>{{$asset->change_value}}</td>
                    <td>{{$asset->value_after_change}}</td>
                    <td>{{$asset->operation_desc}}</td>
                    <td>{{$asset->note_desc}}</td>

                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>
            <div class="col-md-4 col-md-offset-4">
                <?php if(check_permission($user_permissions,"assets/assets_that_reevaluated","add_action")): ?>
                <a href="<?= url("accountant/assets/assets_that_reevaluated/save") ?>" class="btn btn-info btn-lg">
                    تسجيل تغييرات في الأصل
                </a>
                <?php endif; ?>
            </div>


        </div>
    </div>


@endsection
