@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <div class="panel panel-info">
        <div class="panel-heading">
            بيانات
           إهلاكات
            الأصول
        </div>
        <div class="panel-body">

            <?php $url = url('accountant/assets/assets_deprecation'); ?>
            @include('accountant.subviews.assets.assets.blocks.filteration')

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>طريقة الإهلاك</td>
                    <td>الفترة من</td>
                    <td>الفترة الي</td>
                    <td>عدد الشهور</td>
                    <td>الساعات الفعلية</td>
                    <td>القيمة الدفترية الأولي</td>
                    <td>معدل الإهلاك</td>
                    <td>قسط الإهلاك</td>
                    <td>مجمع الإهلاك</td>
                    <td>القيمة الدفترية اخر المدة</td>
                    <td>تاريخ الحساب</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>#</td>
                    <td>كود الأصل</td>
                    <td>اسم الأصل</td>
                    <td>طريقة الإهلاك</td>
                    <td>الفترة من</td>
                    <td>الفترة الي</td>
                    <td>عدد الشهور</td>
                    <td>الساعات الفعلية</td>
                    <td>القيمة الدفترية الأولي</td>
                    <td>معدل الإهلاك</td>
                    <td>قسط الإهلاك</td>
                    <td>مجمع الإهلاك</td>
                    <td>القيمة الدفترية اخر المدة</td>
                    <td>تاريخ الحساب</td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($assets as $key => $asset): ?>

                <tr id="row<?= $asset->asset_rule_id ?>">
                    <td><?= $key+1; ?></td>
                    <td><?= $asset->asset_code ?></td>
                    <td><?= $asset->{'asset_title'.$en} ?></td>
                    <td>{{$deprecation_types[$asset->asset_deprecation_type]}}</td>
                    <td>{{$asset->asset_rule_from_date}}</td>
                    <td>{{$asset->asset_rule_to_date}}</td>
                    <td>{{$asset->asset_deprecation_months}}</td>
                    <td>{{$asset->real_working_units}}</td>
                    <td>{{$asset->asset_deprecation_start_amount}}</td>
                    <td>{{$asset->assets_deprecation_rate}}</td>
                    <td>{{$asset->assets_deprecation_value}}</td>
                    <td>{{$asset->assets_deprecation_total_value}}</td>
                    <td>{{$asset->asset_deprecation_end_amount}}</td>
                    <td>{{$asset->created_at}}</td>
                </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <br>

            <div class="panel panel-primary" style="text-align: center;">
                <div class="panel-heading">حساب الإهلاك</div>
                <div class="panel-body">


                    <div class="alert alert-warning">
                        <b>سيتم حساب الإهلاك لكل انواع ومعادلات طرق الإهلاك فيما عدا طريقة اعاده التقييم حيث يوجد صفحه خاصه لإعاده تقييم هذة الأصول التي من هذا النوع وحساب الإهلاك لها يدويا</b>

                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <?php if(check_permission($user_permissions,"assets/assets_deprecation","add_action")): ?>
                            <a href="#" class="btn btn-primary calc_assets_deprecations">حساب الإهلاك</a>
                            <div class="show_deprecation_msg"></div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


@endsection
