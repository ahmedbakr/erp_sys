@extends('accountant.main_layout')

@section('subview')

    <script src="{{url('public_html/jscode/accountant/assets.js')}}"></script>

    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="تسجيل عملية بيع أصل";

    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <form id="save_form" action="<?=url("accountant/assets/assets_sold/save")?>" method="POST" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <?php

                        echo
                        generate_select_tags(
                            $field_name="asset_id",
                            $label_name="إختار الأصل *",
                            $text=array_merge(convert_inside_obj_to_arr($all_assets,"asset_title$en")),
                            $values=array_merge(convert_inside_obj_to_arr($all_assets,"asset_id")),
                            $selected_value="",
                            $class="form-control select_2_class",
                            $multiple="",
                            $required="required",
                            $disabled = "",
                            $data = ""
                        );
                        ?>
                    </div>
                </div>

                <div class="row">

                    <?php

                    $normal_tags=array(
                        'sell_date',
                        'sell_amount','buyer_name','desc_note'
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $installment_data="",
                        "yes",
                        $required=""
                    );


                    $attrs[0]["sell_date"]="تاريخ البيع *";
                    $attrs[0]["sell_amount"]="المبلغ *";
                    $attrs[0]["buyer_name"]="المشتري";
                    $attrs[0]["desc_note"]="بيان";


                    $attrs[2]["desc_note"]="required";
                    $attrs[2]["sell_date"]="required";

                    $attrs[3]["desc_note"]="textarea";
                    $attrs[3]["sell_date"]="date";
                    $attrs[3]["sell_amount"]="number";
                    $attrs[3]["buyer_name"]="text";


                    $attrs[6]["sell_date"]="4";
                    $attrs[6]["sell_amount"]="4";
                    $attrs[6]["buyer_name"]="4";
                    $attrs[6]["desc_note"]="4";

                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                </div>

                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" value="حفظ" class="btn btn-primary btn-lg submit_installment">
                    </div>

                </div>

            </form>

        </div>
    </div>


@endsection



