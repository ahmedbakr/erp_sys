<div class="menu">
    @include("accountant.subviews.general_account_category.accounts_tree.tree_node")


    <hr>
    <a href="{{url("accountant/general_account_category/save/".$selected_cat_type."/0")}}" class="add_new_acc_category">
        اضف تصنيف رئيسي
        ({{$selected_cat_type_text[$selected_cat_type]}})
    </a>
</div>