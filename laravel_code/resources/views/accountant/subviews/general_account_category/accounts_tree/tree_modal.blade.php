



<div id="tree_modal" class="modal fade {{(isset($data_index_prefix)?$data_index_prefix:"")}}tree_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">شجرة الحسابات</h4>
            </div>
            <div class="modal-body">


                <div class="menu">
                    @include("accountant.subviews.general_account_category.accounts_tree.tree_node")
                </div>

                <hr>

                <div class="selected_items_div center-block" style="text-align: center;">
                    <p>
                        لقد اخترت
                    </p>
                    <br>

                    <div class="selected_items"></div>

                    <br>


                    <button type="button" class="btn btn-info approve_selected_items_btn">
                        تم الاختيار
                    </button>
                </div>



            </div>
        </div>

    </div>
</div>