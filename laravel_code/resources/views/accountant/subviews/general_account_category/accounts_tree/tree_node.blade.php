<?php
    if (isset($data_index_prefix)){
        $categories_str=$data_index_prefix."categories";
        $categories=$$categories_str;

        $accounts_str=$data_index_prefix."accounts";
        $accounts=$$accounts_str;
    }

?>




<?php foreach($categories as $key=>$cat): ?>

    <div class="accordion load_cat_childs" data-catid="{{$cat->cat_id}}" data-loadchilds="false" data-cattype="{{$cat->cat_type}}">
        <div class="accordion-group">
            <div class="accordion-heading accordion-toggle account_category" data-toggle="collapse" href=".cat_{{$cat->cat_id}}" data-catid="{{$cat->cat_id}}" data-catname="<?=$cat->{"cat_name".$en}?>" >
                <a>
                    <?=$cat->{"cat_name".$en}?>
                </a>

                <a class="dropdown-toggle category_links" data-toggle="dropdown" href="#" style="font-style: italic">
                    ...
                </a>

                <ul class="dropdown-menu category_links_dropdown">

                    <li>
                        <a href="{{url("/accountant/general_account_category/save/$cat->cat_type/$cat->parent_id/$cat->cat_id")}}">تعديل بيانات التصنيف</a>
                    </li>
                    <li class="divider"></li>

                    <li>
                        <a href="{{url("/accountant/general_account_category/save/$cat->cat_type/$cat->cat_id")}}">اضف تصنيف جديد</a>
                    </li>
                    <li class="divider"></li>

                    <li>
                        <a href="{{url("/accountant/general_account_category/move_category_to_another_parent/$cat->cat_type/$cat->cat_id")}}">نقل التصنيف</a>
                    </li>
                    <li class="divider"></li>

                    <li>
                        <a href="{{url("/accountant/general_account/save/$cat->cat_type/$cat->cat_id")}}">اضف حساب جديد</a>
                    </li>
                    <li class="divider"></li>


                    <li>
                        <a href="#" class="general_remove_item"
                           data-deleteurl="{{url("/accountant/general_account_category/remove_general_account_category")}}"
                           data-itemid="{{$cat->cat_id}}" data-parentdiv=".load_cat_childs">
                            مسح التصنيف
                        </a>
                    </li>


                    <li class="select_li" style="display: none;" data-item_id="{{$cat->cat_id}}" data-item_name="<?=$cat->{"cat_name".$en}?>">
                        <a href="#">اختر</a>
                    </li>
                </ul>
            </div>

            <div class="accordion-body collapse cat_{{$cat->cat_id}}">
                <div class="accordion-inner">

                    {{-- check if there is accounts or not  --}}
                    <?php if(isset($accounts[$cat->cat_id])): ?>
                        <ul class="nav nav-list">
                            <?php foreach($accounts[$cat->cat_id] as $key=>$account): ?>
                                <li class="account_itself" data-accountit="{{$account->acc_id}}" data-accountname="<?=$account->{"acc_name".$en}?>">
                                    <a>
                                        <i class="icon-chevron-right"></i> <?=$account->{"acc_name".$en}?>
                                    </a>

                                    <a class="dropdown-toggle account_links" data-toggle="dropdown" href="#" style="font-style: italic" aria-expanded="false">...</a>

                                    <ul class="dropdown-menu account_links_dropdown">
                                        <li>
                                            <a href="{{url("/accountant/general_account/save/$cat->cat_type/$cat->cat_id/$account->acc_id")}}">تعديل الحساب</a>
                                        </li>

                                        <li class="divider"></li>


                                        <li>
                                            <a href="{{url("/accountant/general_account/move_account_to_another_parent/$account->acc_type/$account->acc_id")}}">نقل الي فرع آخر</a>
                                        </li>



                                        <?php if(false): ?>

                                            <li class="divider"></li>
                                            <li>
                                                <a href="#" class="general_remove_item"
                                                   data-deleteurl="{{url("/accountant/general_account/remove_general_account")}}"
                                                   data-itemid="{{$account->acc_id}}" data-parentdiv=".account_itself">
                                                    مسح الحساب
                                                </a>
                                            </li>

                                        <?php endif; ?>


                                        <li class="select_li" style="display: none;" data-item_id="{{$account->acc_id}}" data-item_name="<?=$account->{"acc_name".$en}?>">
                                            <a href="#">اختر</a>
                                        </li>
                                    </ul>
                                </li>

                            <?php endforeach;?>
                        </ul>
                    <?php endif; ?>


                    <!--load here other childs-->

                </div>
            </div>
        </div>
    </div>

<?php endforeach;?>
