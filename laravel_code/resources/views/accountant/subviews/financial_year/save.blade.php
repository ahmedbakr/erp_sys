@extends('accountant.main_layout')

@section('subview')


    <?php
        $header_text="اضف سنة مالية جديدة";
        $fin_id="";
        if (is_object($fin_obj)){
            $header_text="تعديل سنة مالية";
            $fin_id=$fin_obj->fin_id;
        }

        if (count($errors->all()) > 0)
        {
            $dump = "<div class='alert alert-danger'>".implode(" <br> ",$errors->all())."</div>";
            echo $dump;
        }

        if (isset($success)&&!empty($success)) {
            echo $success;
        }
    ?>



    <div class="panel panel-info">
        <div class="panel-heading">
            {{$header_text}}
        </div>
        <div class="panel-body">

            <form action="<?=url("accountant/financial_year/save_financial_year/$fin_id")?>" method="POST" enctype="multipart/form-data">


                <?php
                    $normal_tags=array(
                        'fin_label', 'fin_start_range', 'fin_end_range'
                    );

                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $fin_obj,
                        "yes",
                        ""
                    );


                    $attrs[0]["fin_label"]="عنوان السنة المالية*";
                    $attrs[0]["fin_start_range"]="تاريخ بداية السنة المالية*";
                    $attrs[0]["fin_end_range"]="تاريخ نهاية السنة المالية*";

                    $attrs[2]["fin_label"]="required";
                    $attrs[2]["fin_start_range"]="required";
                    $attrs[2]["fin_end_range"]="required";

                    if (is_object($fin_obj)){
                        $attrs[2]["fin_start_range"]="readonly";
                        $attrs[2]["fin_end_range"]="readonly";
                    }

                    $attrs[3]["fin_start_range"]="date";
                    $attrs[3]["fin_end_range"]="date";


                    $attrs[6]["fin_label"]="12";
                    $attrs[6]["fin_start_range"]="6";
                    $attrs[6]["fin_end_range"]="6";


                    echo
                    generate_inputs_html(
                        reformate_arr_without_keys($attrs[0]),
                        reformate_arr_without_keys($attrs[1]),
                        reformate_arr_without_keys($attrs[2]),
                        reformate_arr_without_keys($attrs[3]),
                        reformate_arr_without_keys($attrs[4]),
                        reformate_arr_without_keys($attrs[5]),
                        reformate_arr_without_keys($attrs[6])
                    );
                ?>


                {{csrf_field()}}
                <input type="submit" value="حفظ" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>




        </div>
    </div>





@endsection