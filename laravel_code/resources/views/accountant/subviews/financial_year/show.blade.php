@extends('accountant.main_layout')

@section('subview')


    <div class="panel panel-primary">
        <div class="panel-heading">
            السنوات المالية
        </div>
        <div class="panel-body">


            <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <td>#</td>
                        <td>عنوان السنة المالية</td>
                        <td>البداية</td>
                        <td>النهاية</td>
                        <td>غلق السنة المالية</td>
                        <td>تعديل السنة المالية</td>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach($all_financial_years as $key=>$year): ?>
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$year->fin_label}}</td>
                            <td>{{$year->fin_start_range}}</td>
                            <td>{{$year->fin_end_range}}</td>
                            <td>
                                <?php if($year->fin_closed==0): ?>
                                    <a class="btn btn-warning" href="{{url("/accountant/financial_year/close_financial_year/$year->fin_id")}}">
                                        غلق السنة المالية
                                    </a>
                                <?php endif; ?>
                            </td>

                            <td>
                                <a class="btn btn-primary" href="{{url("/accountant/financial_year/save_financial_year/$year->fin_id")}}">
                                    تعديل السنة المالية
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>

                </tbody>


            </table>


        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            فترة مالية جديدة
        </div>
        <div class="panel-body">
            <a class="btn btn-info" href="{{url("accountant/financial_year/save_financial_year")}}">
                اضف سنة مالية جديدة
            </a>
        </div>
    </div>


@endsection