@extends('accountant.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            تقفيل سنة مالية
        </div>
        <div class="panel-body">

            <form action="<?=url("accountant/financial_year/close_financial_year/$fin_obj->fin_id")?>" method="POST" enctype="multipart/form-data">



                {{csrf_field()}}
                <input type="submit" value="قفل السنة المالية" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
            </form>

        </div>
    </div>



@endsection