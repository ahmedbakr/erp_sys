<!DOCTYPE HTML>
<html>
<head>
    <title>{{$meta_title}}</title>
    <meta name="description" content="<?php echo $meta_desc ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public_html/admin')}}/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/login.css" rel='stylesheet' type='text/css'/>
    <link href="{{url('/public_html/admin')}}/css/general.css" rel='stylesheet' type='text/css'/>

    <!-- font-awesome CSS -->
    <link href="{{url('/public_html/admin')}}/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <!-- lined-icons -->
    <link rel="stylesheet" href="{{url('/public_html/admin')}}/css/icon-font.min.css" type='text/css'/>
    <!-- //lined-icons -->

    <!--datatable css-->
    <link href="<?= url('public_html/admin/js/datatables/css/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= url('public_html/admin/js/datatables/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet" type="text/css">


    <!----webfonts--->
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <!---//webfonts--->

    <link href="{{url('/public_html/admin')}}/css/font-awesome.css" rel="stylesheet">



    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/public_html/admin')}}/js/bootstrap.min.js"></script>


    <!--datatable js-->
    <script src="<?= url('public_html/admin/js/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= url('public_html/admin/js/datatables/js/dataTables.bootstrap.min.js') ?>"></script>

    <!--istree -->
    <link href="<?= url('public_html/admin/tree/themes/default/style.min.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= url('public_html/admin/tree/js/jstree.js') ?>"></script>

    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!-- END Select 2 -->

    <script src="<?= url('public_html/admin/js/moment.js') ?>"></script>
    <script src="<?= url('public_html/admin/js/bootstrap-datetimepicker.js') ?>"></script>
    <link href="<?= url('public_html/admin/css/bootstrap-datetimepicker.css') ?>" rel="stylesheet" type="text/css">


    <script src="<?= url('public_html/jscode/admin/config.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/utility.js') ?>"></script>
    <script src="<?= url('public_html/jscode/homepage.js') ?>"></script>

    <?php if(isset($load_accounts_tree)): ?>
        <script src="<?= url('public_html/jscode/accountant/accounts_tree.js') ?>"></script>
    <?php endif; ?>

    <?php if(isset($load_tree_js)): ?>
        <script src="<?= url('public_html/jscode/admin/tree.js') ?>"></script>
    <?php endif; ?>



    <script src="<?= url('public_html/jscode/accountant/general_accounts.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/select_tree_items.js') ?>"></script>

    <script src="<?= url('public_html/jscode/accountant/entry.js') ?>"></script>




</head>

<body>

<!-- hidden csrf -->
<input type="hidden" class="csrf_input_class" value="{{csrf_token()}}">
<!-- /hidden csrf -->
<!-- hidden base url -->
<input type="hidden" class="url_class" value="<?= url("/") ?>">
<!-- /hidden base url -->

<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <div class="lang">
                    <a href="{{url('/logout')}}" class="btn logout_link"><i class="fa fa-lock"></i> خروج  </a>
                </div>
            </div>
            <div class="col-md-1">
                <div class="lang">
                    <a href="#" class="btn">English</a>
                </div>
            </div>

            <div class="col-md-10 top_nav_dates">
                <div class="date">
                    هجري :
                    {{get_hegri_date(strtotime(date("j/ n/ Y")))}}
                </div>
                <div class="date">
                    ميلادي :
                    {{$current_date}}
                </div>
                <div class="date direction_rtl">
                    الوقت :
                    <span class="show_time"></span>
                </div>
            </div>
        </div>
    </div>
</div>


<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="box-logout color_white">
                    <h3 style="line-height: 52px;">الحسابات</h3>

                </div>
            </div>
            <div class="col-md-9 header_logo">
                <div class="logo">
                    <img src="{{get_image_or_default('public_html/img/seoera.png')}}" />
                </div>
            </div>

        </div>
    </div>
</header>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header float_right">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        
        <?php if(1!=1): ?>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav float_right">
                <li><a href="#">تقرير المبيعات اليومية</a></li>
                <li><a href="#">شاشة طلب أجازة</a></li>
                <li><a href="#">تحويلات مخازن</a></li>
                <li><a href="#">إدخال بضاعه</a></li>
                <li><a href="#">أمر توريد</a></li>
                <li><a href="#">فتح الدرج</a></li>
                <li><a href="#">مبيعات الكاشيير</a></li>
                <li class="active"><a href="#">الرئيسية</a></li>
            </ul>
        </div>
        <?php endif; ?>

    </div>
</nav>

<script>
    $(function () {

        $('.tree_click_link a').click(function () {

            var link = $(this).attr('href');
            location.href = link;

            return false;
        });

    });
</script>


<div class="col-md-12">
    <div class="">
        {!! \Session::get("msg") !!}


        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-heading main_menu_panel_heading" data-toggle="collapse" href=".main_menu">
                    القائمة الرئيسية
                </div>
                <div class="panel-body collapse in main_menu">

                    <div class="jstree_demo_div tree_click_link" style="direction: rtl;">
                        <ul>
                            <?php if(1!=1): ?>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاعدادات العامة
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>بيانات الشركة</li>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>الاعدادات العامة</li>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>صلاحيات المستخدمين</li>
                                </ul>
                            </li>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الأصناف والمخازن
                                <ul>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>أكواد أساسية
                                        <ul>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>الأكواد</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>بيانات المخزن</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>مستندات الاضافة والصرف</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>مجموعات الاصناف</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>المصروفات المباشرة المصروفات الغير مباشرة</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>عمليات المخازن
                                        <ul>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>اذن اضافة</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>اذن صرف</li>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تحويلات المخازن</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تصنيع المنتجات
                                        <ul>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>امر تصنيع</li>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>التقارير
                                        <ul>
                                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>تقارير الاًصناف
                                                <ul>
                                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقارير حركة الاصناف</li>
                                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>تقارير تفاصيل الاصناف</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>العملاء و المبيعات</li>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الموردين والمشتريات</li>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>شئون الموظفين</li>

                            <?php endif; ?>


                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>
                                السنة المالية
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/financial_year/show_all_financial_years")}}">كل السنين المالية</a>
                                    </li>

                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/financial_year/save_financial_year")}}">اضف سنة مالية جديدة</a>
                                    </li>
                                </ul>
                            </li>


                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الحسابات العامة
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/general_account_category/show_tree/false/general")}}">الحسابات العامة</a>
                                    </li>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/general_account_category/show_tree/false/consuming")}}">مراكز التكلفة</a>
                                    </li>
                                </ul>
                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>القيود
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/entry/show_entries")}}">اظهر القيود</a>
                                    </li>

                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/entry/add_entry")}}">اضف قيد</a>
                                    </li>

                                </ul>
                            </li>

                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>نماذج القيود
                                <ul>
                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/entry/show_all_templates")}}">اظهر كل النماذج</a>
                                    </li>

                                    <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                        <a href="{{url("/accountant/entry/add_template")}}">اضف نموذج</a>
                                    </li>

                                </ul>
                            </li>



                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الاصول الثابته

                                <ul>

                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>البيانات الأساسية
                                        <ul>
                                            <?php if(check_permission($user_permissions,"assets/locations","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/locations/show')}}">بيانات المواقع</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/category","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/category/show')}}">بيانات الفئات</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/status","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/status/show')}}">بيانات حالات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets')}}">بيانات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/asset_expenses_and_incomes/expense')}}">مصروفات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/asset_expenses_and_incomes/income')}}">إيرادات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_sold","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_sold')}}">الأصول التي تم بيعها</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_rubbish","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_rubbish')}}">الأصول التي تم تكهينها</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_rent","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_rent')}}">الأصول التي تم تأجيرها</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_on_maintenance')}}">الأصول التي في الصيانة</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_transports","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_transports')}}">تنقلات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_deprecation","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_deprecation')}}">إهلاكات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_that_reevaluated","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_that_reevaluated')}}">التغييرات في قيمة الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/documents","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/documents/save')}}">إضافة مستند</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/installment","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/installments')}}">الأقساط</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/installment","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/installments/save')}}">تسجيل الأقساط المحددة</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/installment","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/installments/save_generated_installments')}}">تسجيل الأقساط الدورية</a>
                                            </li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                    <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>العمليات
                                        <ul>
                                            <?php if(check_permission($user_permissions,"assets","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/save')}}">إضافة أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_sold","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_sold/save')}}">بيع أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_rubbish","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_rubbish/save')}}">تكهين أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/asset_expenses_and_incomes/save/expense')}}">تسجيل مصروف أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/asset_expenses_and_incomes","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/asset_expenses_and_incomes/save/income')}}">تسجيل إيراد أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_rent","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_rent/save')}}">تأجير أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_on_maintenance","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_on_maintenance/save')}}">عملية صيانة أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_transports","add_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_transports/save')}}">نقل أصل</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_deprecation","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_deprecation')}}">حساب إهلاكات الأصول</a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if(check_permission($user_permissions,"assets/assets_that_reevaluated","show_action")): ?>
                                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf"}'>
                                                <a href="{{url('/accountant/assets/assets_that_reevaluated/save')}}">تسجيل تغيير في قيمة أصل</a>
                                            </li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                </ul>

                            </li>

                            <?php if(check_permission($user_permissions,"accountant/expenses","show_action")): ?>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>
                                <a href="#"><i class="lnr lnr-briefcase"></i>
                                    <span>المصاريف</span></a>
                                <ul >

                                    <?php if(check_permission($user_permissions,"accountant/category","show_action")): ?>
                                    <li><a href="{{url('/common/category/show_tree/false/expense')}}">تصنيفات المصاريف</a></li>
                                    <?php endif; ?>

                                    <?php if(check_permission($user_permissions,"accountant/expenses","add_action")): ?>
                                    <li><a href="{{url("/common/expenses/show_all_expenses")}}">مشاهدة المصاريف</a></li>
                                    <?php endif; ?>

                                </ul>
                            </li>
                            <?php endif; ?>


                            <?php if(1!=1): ?>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الحركة المالية</li>
                            <li data-jstree='{"opened":true,"selected":true,"icon":"//jstree.com/tree.png"}'>الشيكات</li>
                            <?php endif; ?>

                        </ul>
                    </div>
                </div>
            </div>



        </div>

        <div class="col-md-9">
            @yield('subview')
        </div>
        


    </div>
</div>



</body>
</html>