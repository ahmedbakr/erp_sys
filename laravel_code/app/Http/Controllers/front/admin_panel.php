<?php

namespace App\Http\Controllers\front;

use App\models\company_m;
use App\support_messages_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class admin_panel extends Controller
{
    public function __construct(){

        parent::__construct();

    }

    public function index()
    {

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="admin"){
            return Redirect::to('admin/dashboard')->send();
        }

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="dev"){
            return Redirect::to('dev/dashboard')->send();
        }

        if(is_object(\Auth::user())&& (\Auth::user()->user_type=="branch" || \Auth::user()->user_type=="branch_admin") ){
            return Redirect::to('admin/dashboard')->send();
        }

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="accountant"){
            return Redirect::to('accountant/dashboard')->send();
        }



        return view("front.subviews.admin_panel",$this->data);
    }

    public function try_login(Request $request){

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="admin"){
            return Redirect::to('admin/dashboard')->send();
        }

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="dev"){
            return Redirect::to('dev/dashboard')->send();
        }

        if(is_object(\Auth::user())&& (\Auth::user()->user_type=="branch" || \Auth::user()->user_type=="branch_admin") ){
            return Redirect::to('admin/dashboard')->send();
        }

        if(is_object(\Auth::user())&&\Auth::user()->user_type=="accountant"){
            return Redirect::to('accountant/dashboard')->send();
        }


        $request["remember"]="on";

        $email_login=\Auth::attempt([
            "email"=>$request->get("email"),
            "password"=>$request->get("password"),
            "user_active"=>1,
            "user_can_login"=>1
        ],
        $request->get("remember"));



        if($email_login){
            Auth::login(\Auth::user());
            $request->session()->save();
            $user_obj = \Auth::user();

            if ($user_obj->user_type == "admin")
            {
                return redirect()->intended('admin/dashboard');
            }
            elseif($user_obj->user_type == "dev"){
                return redirect()->intended('dev/dashboard');
            }
            elseif ($user_obj->user_type == "branch" || $user_obj->user_type == "branch_admin")
            {
                return redirect()->intended('admin/dashboard');
            }
            elseif ($user_obj->user_type == "accountant")
            {
                return redirect()->intended('accountant/dashboard');
            }


        }else{
            $msg="<div class='alert alert-danger'>خطأ في بيانات التسجيل</div>";
            return redirect()->back()->with(["msg"=>$msg]);
        }
    }


    public function send_custom_email(Request $request)
    {

        $output = [];
        $output["msg"] = "";

        $receiver_email = clean($request->get("receiver_email"));
        $email_body = clean($request->get("email_body"));

        $rules_values=[
            "receiver_email" => $receiver_email,
            "email_body" => $email_body,
        ];

        $rules_itself=[
            "receiver_email" => "required|email",
            "email_body" => "required",
        ];

        $validator = Validator::make($rules_values,$rules_itself);

        \Debugbar::disable();
        if (count($validator->messages()) > 0)
        {
            $output["msg"] = "<div class='alert alert-danger'>";
            foreach ($validator->messages()->all() as $key => $error)
            {
                $output["msg"] .= $error." <br>";
            }
            $output["msg"] .= "</div>";

            echo json_encode($output);
            return;
        }
        else{

            $this->_send_email_to_custom(
                $emails = array($receiver_email) ,
                $data = "$email_body" ,
                $subject = "",
                $sender = "seoera@seoera.net"
            );

            $output["msg"] = "<div class='alert alert-success'>";
            $output["msg"] .= "تم إرسال الرسالة بنجاح";
            $output["msg"] .= "</div>";

        }

        
        echo json_encode($output);
        return;
    }

}
