<?php

namespace App\Http\Controllers\front;

use App\models\notification_m;
use App\support_messages_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class pages extends Controller
{
    public function __construct(){

        parent::__construct();

    }

    public function dashboard()
    {
        return view("front.subviews.dashboard",$this->data);
    }

    public function sales_manager()
    {
        return view("front.subviews.sales_manager",$this->data);
    }

    public function admin_page()
    {
        return view("front.subviews.admin_page",$this->data);
    }

    public function cashier()
    {
        return view("front.subviews.cashier",$this->data);
    }

}
