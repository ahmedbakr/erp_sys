<?php

namespace App\Http\Controllers\front;


use App\blocks_m;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\category_m;
use App\models\pages_m;
use App\models\settings_m;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang_title="")
    {

        $slider_arr = array();
        $slider_arr["edit_index_page"]=["slider1"];
        $slider_arr["index_blocks"]=["slider1"];

        $this->general_get_content([
            "edit_index_page","subscribe_block","index_blocks"
        ],$slider_arr);

        #region get all continent
            $this->data["all_continent"] = category_m::get_all_cats($additional_where = " AND cat.cat_type = 'continent' ", $order_by = " order by cat.cat_order asc " , $limit = "",$make_it_hierarchical=false,$default_lang_id=$this->lang_id);
        #endregion

        $this->data["meta_title"]=$this->data["edit_index_page"]->meta_title;
        $this->data["meta_desc"]=$this->data["edit_index_page"]->meta_desc;
        $this->data["meta_keywords"]=$this->data["edit_index_page"]->meta_keywords;


        return view("front.subviews.index",$this->data);
    }



}
