<?php

namespace App\Http\Controllers;

use App\models\accountant\general_accounts\general_account_category_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\models\admin\category_m;
use App\models\admin\points_of_sale_m;
use App\models\attachments_m;
use App\models\bills\clients_bills_m;
use App\models\cheques\cheque_template_items_m;
use App\models\cheques\cheque_templates_m;
use App\models\invoices\invoices_template_content_m;
use App\models\invoices\invoices_templates_m;
use App\models\money_transfers_m;
use App\models\notification_m;
use App\models\orders\clients_orders_m;
use App\models\product\products_m;
use App\models\site_tracking_m;
use App\models\user_messages_m;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use SSP;

class dashbaord_controller extends Controller
{

    public $user_permissions;
    public $current_user_data;
    public $en="";
    public $main_layout="";
    public $syb_sys="";
    public $page_permission="";

    public function __construct()
    {
        parent::__construct();

        // get all user permissions

        $this->user_permissions = $this->get_user_permissions();
        $this->data["user_permissions"] = $this->user_permissions;

        $this->current_user_data=$this->data["current_user"];

        $date = date('Y-m-d');


        if (check_permission($this->user_permissions,"site_tracking","show_action",$this->data["current_user"]))
        {
            $this->data["site_tracking"] = site_tracking_m::get_tracking(" where date(track.created_at) = '$date' order by track.created_at desc");
        }

        $this->data["notifications"] = notification_m::get_notifications(" where date(created_at) = '$date' AND not_to_userid = $this->user_id order by created_at desc");
        $this->data["user_messages"] = user_messages_m::get_messages(" where date(msg.created_at) = '$date' AND msg.to_user_id = $this->user_id order by msg.created_at desc");


        $en="";
        if (\Session::get("select_en")=="en"){
            $en="_en";
        }


        $this->data["en"]=$en;

        $this->data["selected_cat_type_text"]=[
            "general"=>"الحسابات العامة",
            "consuming"=>"مراكز التكلفة",
            "product"=>"المنتجات",
            "expense"=>"المصروفات",
        ];

        $this->data["main_layout"]="admin";
    }

    public function get_branches_and_other_data(){

        $this->data["all_branches"]=[];
        $this->data["all_point_of_sales"]=[];
        $this->data["all_branch_admins"]=[];
        $this->data["all_customers"]=[];

        if($this->current_user_data->user_type=="admin"){
            //load all branches
            $this->data["all_branches"]=User::get_all_users(" AND user.user_type='branch' ");

            //load all point of sales
            $this->data["all_point_of_sales"]=points_of_sale_m::all();

            //load all branch admins
            $this->data["all_branch_admins"]=User::get_all_users(" AND user.user_type='branch_admin' ");

            //load all customers
            $this->data["all_customers"]=User::get_all_users(" AND user.user_type='customer' ");
        }
        elseif($this->current_user_data->user_type=="branch_admin"){
            //load his branch only
            $this->data["all_branches"]=User::get_all_users(" AND user.user_type='branch' AND user.user_id=".$this->current_user_data->related_id);

            //load his point of sale only
            $this->data["all_point_of_sales"]=points_of_sale_m::find($this->current_user_data->point_of_sale_id);

            //get his data
            $this->data["all_branch_admins"]=User::get_all_users(" AND user.user_type='branch_admin' AND user.user_id=".$this->current_user_data->user_id);

            //load all customers of his branch
            $this->data["all_customers"]=User::get_all_users(" AND user.user_type='customer' AND user.related_id=".$this->current_user_data->related_id);
        }




    }


    public function pre_call_show_category_tree(){
        if(!in_array($this->current_user_data->user_type,["accountant","admin","factory_admin","branch","branch_admin"])){
            return Redirect::to("/")->send();
        }


        $this->data["page_permission"]=$this->page_permission;
        $this->data["syb_sys"]=$this->syb_sys;
        $this->data["main_layout"]=$this->main_layout;
    }

    public function show_category_tree($just_load_data=false,$cat_type="product",$data_index_prefix="")
    {


        if($this->current_user_data->user_type=="branch"||$this->current_user_data->user_type=="branch_admin"){

            $this->page_permission="branch/expenses";
            $this->syb_sys="branch";
            if($cat_type=="product"){
                $this->page_permission="true";
                $this->syb_sys="branch";
            }

        }

        $this->data["load_tree_js"]="yes";

        if($just_load_data==="true"||$just_load_data===true){
            $just_load_data=true;
        }
        else{
            $just_load_data=false;
        }

        if(!in_array($just_load_data,[true,false])){
            return;
        }

        if(!in_array($cat_type,["product","expense"])){
            return;
        }



        $this->data["data_index_prefix"]=$data_index_prefix;
        $this->data[$data_index_prefix."selected_cat_type"]=$cat_type;



        $parent_id=Input::get("parent_id",0);
        $return_to_ajax=Input::get("return_to_ajax",false);
        $cat_type=Input::get("cat_type",$cat_type);


        //get categories with parent_id=$parent_id
        $categories=category_m::get_cats(" AND cat.parent_id=$parent_id AND cat.cat_type='$cat_type'");
        $this->data[$data_index_prefix."categories"]=$categories;


        //get items of there categories
        $category_items=[];
        if(isset_and_array($categories)){
            if($cat_type=="product"){
                $categories_ids=convert_inside_obj_to_arr($categories,"cat_id");
                $category_items=products_m::whereIn("cat_id",$categories_ids)->get()->groupBy("cat_id")->all();
            }

            if($cat_type=="expenses") {
                //i do not want wo show expenses items
            }

        }
        $this->data[$data_index_prefix."category_items"]=$category_items;

        $view_data["categories"]=$categories;
        $view_data["category_items"]=$category_items;
        $view_data["en"]=$this->data["en"];
        $view_data["selected_cat_type"]=$this->data[$data_index_prefix."selected_cat_type"];
        $view_data["selected_cat_type_text"]=$this->data["selected_cat_type_text"];
        $view_data["main_layout"]=$this->data["main_layout"];
        $view_data["current_user"]=$this->data["current_user"];
        $view_data["data_arr"]=$this->data;

        $tree_html=\View::make(
            "common.category.accounts_tree.".(($parent_id==0)?"tree_main_div":"tree_node"),
            $view_data
        )->render();

        $this->data[$data_index_prefix."tree_html"] = $tree_html;


        if($return_to_ajax){

            $output=[];
            $output["categories"]=$categories;
            $output["category_items"]=$category_items;
            $output["tree_html"]=$tree_html;

            echo json_encode($output);
            return;
        }


        if (!$just_load_data){
            return view("common.category.show_accounts_tree",$this->data);
        }
    }



    public function general_remove_item(Request $request,$model_name="")
    {

        $output = array();
        $item_id = (int)$request->get("item_id");

        if($model_name==""){
            $model_name = $request->get("table_name"); // App\User
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";
            }

        }

        echo json_encode($output);

    }


    public function reorder_items(Request $request) {

        $items=  $request->get("items");
        $model_name=  $request->get("table_name");  // App\User
        $field_name=  $request->get("field_name");

        $output=array();

        if (is_array($items)&&  (count($items)>0)) {
            foreach ($items as $key => $value) {
                $item_id=$value[0];
                $item_order=$value[1];

                $returned_check=$model_name::find($item_id)->update([
                    "$field_name"   =>  $item_order
                ]);

                if ($returned_check != true) {

                    $output["error"]="error";
                    echo json_encode($output);
                    return;
                }

            }
            $output["success"]="success";
        }
        else{
            $output["error"]="bad array";
        }

        echo json_encode($output);
    }


    public function accept_item(Request $request) {

        $output = array();
        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");

        $output["success"] = "";
        $output["status"] = "";

        if ($item_id > 0) {

            if ($accept == 0) {

                $return_statues=$model_name::find($item_id)->update(["$field_name"=>"1"]);
                if($return_statues){
                    $output["success"] = "success";
                    $output["status"] = '<span class="label label-success"> This is accepted, Reject <i class="fa fa-close"></i> ?</span>';
                    $output["new_accept"] = 1;
                }

            }
            else
            {
                $return_statues=$model_name::find($item_id)->update(["$field_name"=>"0"]);
                if($return_statues){
                    $output["success"] = "success";
                    $output["status"] = '<span class="label label-warning"> This is rejected,Accepted <i class="fa fa-check"></i>?</span>';
                    $output["new_accept"] = 0;
                }

            }

        }
        else{
            $output["success"] = "error";
        }

        echo json_encode($output);
    }

    public function new_accept_item(Request $request) {

        $output = array();
        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $return_statues=$obj->update(["$field_name"=>"$accept"]);

            $output["msg"]=generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
        }


        echo json_encode($output);
    }



    public function general_self_edit(Request $request) {

        $output = array();
        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $value = $request->get("value");
        $input_type= $request->get("input_type");
        $row_primary_col= $request->get("row_primary_col");

        $output["success"] = "";
        $output["status"] = "";

        if ($item_id > 0) {

            $item_obj=$model_name::find($item_id);
            $return_statues=$item_obj->update(["$field_name"=>$value]);
            if($return_statues){
                $output["success"] = "success";
            }

            $output["msg"]=generate_self_edit_input(
                    $url="",
                    $item_obj,
                    $item_primary_col=$row_primary_col,
                    $item_edit_col=$field_name,
                    $table=$model_name,
                    $input_type=$input_type
                );

        }
        else{
            $output["success"] = "error";
        }



        echo json_encode($output);
    }


    public function remove_admin(Request $request) {
        $output = array();
        $admin_id = $request->get("item_id");
        $output["dump"]["id"] = $admin_id;

        if (!$admin_id > 0) {
            $output["deleted"] = "admin id !>0";
            echo json_encode($output);
            return;
        }

        if ($admin_id == $this->user_id) {
            $output["deleted"] = "can not remove this admin";
            echo json_encode($output);
            return;
        }

        // check if there is another admins or not
        if (count(user::where("user_type","admin")->get()) > 1) {

            $admin = user::where("user_id",$admin_id)->get();

            if (count($admin) > 0) {
                user::destroy($admin_id);
                $output["deleted"] = "yes";
            }
            else {
                $output["deleted"] = "you have no admin with this id";
            }
        }
        else {
            $output["deleted"] = "you can not delete the last admin";
        }

        echo json_encode($output);
    }


    public function generate_table_cols($table_name,$primaryKey,$db_fileds){

        $columns = [];

        foreach ($db_fileds as $db_key=>$db_col){
            $columns[]=[
                "db"=>$db_col,
                "dt"=>$db_col
            ];
        }

        $sql_details = array(
            'user' => env("DB_USERNAME"),
            'pass' => env("DB_PASSWORD"),
            'db'   => env("DB_DATABASE"),
            'host' => env("DB_HOST")
        );


        return json_encode(
            SSP::simple( $_GET, $sql_details, $table_name,"", $primaryKey, $columns)
        );
    }


    public function print_cheques()
    {

        if (!check_permission($this->user_permissions,"admin/print_cheques","show_action",$this->data["current_user"]))
        {
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["template_data"] = "";
        $this->data["cheque_date"] = date("Y-m-d");
        $this->data["receiver_name"] = "";
        $this->data["money_in_numbers"] = 0;
        $this->data["money_in_text"] = "";
        $this->data["template_items"] = [];
        $subview_main_layout = "admin";


        if(check_permission($this->user_permissions,"admin/print_cheques","show_action",$this->data["current_user"]))
        {
            $subview_main_layout = "branch";
            $branch_id = $this->current_user_data->user_id;
            if ($this->current_user_data->related_id > 0)
            {
                $branch_id = $this->current_user_data->related_id;
            }

            $template_data = cheque_templates_m::
                where("set_default",1)
                    ->where("branch_id",$branch_id)
                    ->get()->first();
        }
        else{
            $template_data = cheque_templates_m::
            where("set_default",1)->get()->first();
        }


        if (is_object($template_data))
        {

            $this->data["template_data"] = $template_data;

            // get template items
            $template_items = cheque_template_items_m::where("cheque_template_id",$template_data->cheque_template_id)->get();
            $template_items = collect($template_items)->groupBy("item_name");
            $template_items = $template_items->all();
            $this->data["template_items"] = $template_items;

            if (is_array($_GET) && count($_GET))
            {
//                dump($_GET);
                if (isset($_GET["cheque_date"]))
                {
                    $template_items["cheque_date"][0]->item_name = clean($_GET["cheque_date"]);
                    $this->data["cheque_date"] = clean($_GET["cheque_date"]);
                }
                if (isset($_GET["receiver_name"]))
                {
                    $template_items["receiver_name"][0]->item_name = $_GET["receiver_name"];
                    $this->data["receiver_name"] = clean($_GET["receiver_name"]);
                }
                if (isset($_GET["money_in_numbers"]))
                {
                    $template_items["money_in_numbers"][0]->item_name = $_GET["money_in_numbers"];
                    $this->data["money_in_numbers"] = clean($_GET["money_in_numbers"]);
                }
                if (isset($_GET["money_in_text"]))
                {
                    $template_items["money_in_text"][0]->item_name = $_GET["money_in_text"];
                    $this->data["money_in_text"] = clean($_GET["money_in_text"]);
                }
            }

//            dump($template_items);

        }
        else{
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود او لا يحتوي علي عناصر !!! </div>"
            ])->send();
        }


        return view("$subview_main_layout.subviews.cheque_templates.print.print_cheque",$this->data);
    }


    public function print_client_bill()
    {
        if (!check_permission($this->user_permissions,"admin/print_client_bill","show_action",$this->data["current_user"]))
        {
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["bill_data"] = "";
        $this->data["bill_orders"] = [];
        $this->data["bill_id"] = "";
        $this->data["bill_type"] = 0; // بيع
        $this->data["template_data"] = "";
        $this->data["template_items"] = "";

        if (is_array($_GET) && count($_GET))
        {
//            dump($_GET);

//            if (isset($_GET["bill_id"]) && $_GET["bill_id"] > 0 && !isset($_GET["bill_type"]))
//            {
//                $bill_id = $_GET["bill_id"];
//                return Redirect::to("/print_client_bill?bill_id=$bill_id&bill_type=0")->with([
//                    "msg" => "<div class='alert alert-success'> بيانات الفاتورة بالأسفل للطباعه  </div>"
//                ])->send();
//            }

            if (isset($_GET["bill_id"]) && $_GET["bill_id"] > 0 && isset($_GET["bill_type"]))
            {
                $bill_id = clean($_GET["bill_id"]);
                $bill_type = clean($_GET["bill_type"]);
                $this->data["bill_id"] = $bill_id;
                $this->data["bill_type"] = $bill_type;

                $get_bill_data = clients_bills_m::get_clients_bills("
                    AND bill.client_bill_id = $bill_id
                ");

                if(is_array($get_bill_data) && count($get_bill_data) && isset($get_bill_data[0]))
                {
                    $get_bill_data = $get_bill_data[0];
                    $this->data["bill_data"] = $get_bill_data;
//                    dump($get_bill_data);

                    #region get bill orders based on bill_type

                    $get_bill_orders = clients_orders_m::get_bill_orders("
                        AND clients_order.client_bill_id = $bill_id
                        AND clients_order.order_return = $bill_type
                    ");

                    #endregion

                    if (is_array($get_bill_orders) && count($get_bill_orders))
                    {
                        $this->data["bill_orders"] = $get_bill_orders;
//                        dump($get_bill_orders);

                        #region if return orders get money

                        if ($bill_type == 1)
                        {
                            $get_bill_data->client_total_paid_amount_in_cash = 0;
                            $get_bill_data->client_total_paid_amount_in_atm = 0;
                            $get_bill_data->client_bill_total_remain_amount = 0;

                            $get_money = money_transfers_m::get_data("
                                AND money.m_t_bill_id = $bill_id
                                AND money.from_user_id = $get_bill_data->branch_id
                            ");

                            if (is_array($get_money) && count($get_money))
                            {

                                foreach($get_money as $key => $money)
                                {

                                    if ($money->m_t_status == 1)
                                    {

                                        if ($money->m_t_amount_type == "atm")
                                        {
                                            $get_bill_data->client_total_paid_amount_in_atm += $money->m_t_amount;
                                        }
                                        else
                                        {
                                            $get_bill_data->client_total_paid_amount_in_cash += $money->m_t_amount;
                                        }

                                    }
                                    else{
                                        $get_bill_data->client_bill_total_remain_amount += $money->m_t_amount;
                                    }

                                }
                            }

                        }

                        #endregion

                    }
                    else{

                        $orders_type = "بيع";
                        if ($bill_type == 1)
                        {
                            $orders_type = "مرتجع";
                        }

                        return Redirect::to('/print_client_bill')->with([
                            "msg" => "<div class='alert alert-danger'>  لا توجد $orders_type للفاتورة رقم $bill_id </div>"
                        ])->send();
                    }

                }
                else{
                    return Redirect::to('/print_client_bill')->with([
                        "msg" => "<div class='alert alert-danger'> الفاتورة غير موجوده  </div>"
                    ])->send();
                }

            }

        }

//        dump($this->data["bill_id"]);
//        dump($this->data["bill_type"]);


        $subview_main_layout = "admin";


        if(check_permission($this->user_permissions,"admin/print_cheques","show_action",$this->data["current_user"]))
        {

            $subview_main_layout = "branch";
            if ($this->data["current_user"]->user_type != "admin")
            {
                // old that get template depend on branch_id on bill data
//                $template_data = invoices_templates_m::
//                where("set_default",1)
//                    ->where("branch_id",$get_bill_data->branch_id)->get()->first();

                $template_data = invoices_templates_m::
                where("set_default",1)->get()->first();
            }
            else{
                $template_data = invoices_templates_m::
                where("set_default",1)->get()->first();
            }

        }
        else{
            $template_data = invoices_templates_m::
            where("set_default",1)->get()->first();
        }

        if (!is_object($template_data))
        {
            return Redirect::to('/admin/invoices_templates')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد قالب اساسي مختار برجاء تحديد قالب اساسي واحد علي الاقل </div>"
            ])->send();
        }

        $inv_temp_id = $template_data->inv_temp_id;

        if (is_object($template_data))
        {

            $this->data["template_data"] = $template_data;

            // get template items
            $template_items = invoices_template_content_m::get_data(" AND itc.inv_temp_id = $inv_temp_id");
            $template_items = collect($template_items)->groupBy("item_name");
            $template_items = $template_items->all();
            $this->data["template_items"] = $template_items;
        }
        else{
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود او لا يحتوي علي عناصر !!! </div>"
            ])->send();
        }

//        dump($template_data);
//        dump($template_items);

        return view("admin.subviews.invoices_templates.print_client_bill.preview",$this->data);
    }

    public function print_today_bills()
    {
        if (!check_permission($this->user_permissions,"admin/print_client_bill","show_action",$this->data["current_user"]))
        {
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $current_hour = date("H");

        $get_current_date = date("Y-m-d");
        $get_start_month_date = date('Y-m-1');


        $get_current_datetime = date("Y-m-d H:i:s");
        $get_start_month_datetime = date('Y-m-1 05:00:00');
        $current_data=Carbon::now();
        if ($current_hour >= 5)
        {
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day+1, $hour = 4,59);
        }
        else{
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }

        $start_day=$start_day->toDateTimeString();
        $end_day=$end_day->toDateTimeString();

        $this->data["start_day_date"]=$start_day;
        $this->data["end_day_date"]=$end_day;


        $template_data = invoices_templates_m::
        where("set_default",1)->get()->first();

        if (!is_object($template_data))
        {
            return Redirect::to('/admin/invoices_templates')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد قالب اساسي مختار برجاء تحديد قالب اساسي واحد علي الاقل </div>"
            ])->send();
        }

        $inv_temp_id = $template_data->inv_temp_id;

        if (is_object($template_data))
        {

            $this->data["template_data"] = $template_data;

            // get template items
            $template_items = invoices_template_content_m::get_data(" AND itc.inv_temp_id = $inv_temp_id");
            $template_items = collect($template_items)->groupBy("item_name");
            $template_items = $template_items->all();
            $this->data["template_items"] = $template_items;
        }
        else{
            return Redirect::to('/')->with([
                "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود او لا يحتوي علي عناصر !!! </div>"
            ])->send();
        }

        if($this->data["current_user"]->user_type=="admin"){
            $this->data["branch_name"]="الفرع الرئيسي";
        }

        else if($this->data["current_user"]->user_type=="branch_admin") {
            $branch_id=$this->data["current_user"]->related_id;
            $branch_data=User::get_users(" AND u.user_id=$branch_id");
            $branch_data=$branch_data[0];

            $this->data["branch_name"]=$branch_data->full_name;
        }

        return view("admin.subviews.invoices_templates.print_today_bills.preview",$this->data);
    }
}
