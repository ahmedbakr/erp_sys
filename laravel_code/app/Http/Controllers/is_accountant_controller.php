<?php

namespace App\Http\Controllers;

use App\models\accountant\general_accounts\consuming_center_account_m;
use App\models\accountant\general_accounts\consuming_center_category_m;
use App\models\accountant\general_accounts\general_account_category_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\models\admin\category_m;
use App\models\product\products_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Redirect;

class is_accountant_controller extends dashbaord_controller
{


    public function __construct()
    {
        parent::__construct();
        $this->middleware("check_accountant");


        $this->data["main_layout"]="accountant";
    }



    public function show_tree($just_load_data=false,$cat_type="general",$data_index_prefix="")
    {

        $this->data["load_accounts_tree"]="yes";


        if($just_load_data==="true"||$just_load_data===true){
            $just_load_data=true;
        }
        else{
            $just_load_data=false;
        }

        if(!in_array($just_load_data,[true,false])){
            return;
        }

        if(!in_array($cat_type,["general","consuming"])){
            return;
        }

        $this->data["data_index_prefix"]=$data_index_prefix;


        $this->data[$data_index_prefix."selected_cat_type"]=$cat_type;


        $parent_id=Input::get("parent_id",0);
        $return_to_ajax=Input::get("return_to_ajax",false);
        $cat_type=Input::get("cat_type",$cat_type);


        //get categories with parent_id=$parent_id
        $categories=general_account_category_m::get_cats(" AND cat.parent_id=$parent_id AND cat.cat_type='$cat_type'");
        $this->data[$data_index_prefix."categories"]=$categories;


        //get account of there categories
        $accounts=[];
        if(isset_and_array($categories)){
            $categories_ids=convert_inside_obj_to_arr($categories,"cat_id");
            $accounts=general_accounts_m::get_general_accounts(" AND gen_acc.cat_id in (".implode(",",$categories_ids).") ");
            $accounts=collect($accounts)->groupBy("cat_id")->all();
        }
        $this->data[$data_index_prefix."accounts"]=$accounts;


        $view_data["categories"]=$categories;
        $view_data["accounts"]=$accounts;
        $view_data["en"]=$this->data["en"];
        $view_data["selected_cat_type"]=$this->data[$data_index_prefix."selected_cat_type"];
        $view_data["selected_cat_type_text"]=$this->data["selected_cat_type_text"];
        $view_data["main_layout"]=$this->data["main_layout"];

        $tree_html=\View::make(
            "accountant.subviews.general_account_category.accounts_tree.".(($parent_id==0)?"tree_main_div":"tree_node"),
            $view_data
        )->render();

        $this->data[$data_index_prefix."tree_html"] = $tree_html;


        if($return_to_ajax){

            $output=[];
            $output["categories"]=$categories;
            $output["accounts"]=$accounts;
            $output["tree_html"]=$tree_html;

            echo json_encode($output);
            return;
        }


        if (!$just_load_data){
            return view("accountant.subviews.general_account_category.show_accounts_tree",$this->data);
        }
    }


}
