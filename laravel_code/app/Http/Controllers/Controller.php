<?php

namespace App\Http\Controllers;

use App\models\company_m;
use App\models\money_transfers_m;
use App\models\notification_m;
use App\models\permissions\permissions_m;
use App\models\settings_m;
use App\models\site_tracking_m;
use App\models\sortable_menus_m;
use App\User;
use Cache;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

//models
use App\models\generate_site_content_methods_m;
use App\models\site_content_m;
use App\models\attachments_m;
use Request;
use Schema;
use View;

//END models


class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    public $data=array();
    public $user_id=1;
    public $related_user_id=1;
    public $lang_id=1;
    public $tax_percent=4.75;

    public function __construct()
    {
        $current_user = Auth::user();
        $this->data["current_user"] = null;
        if (isset($current_user))
        {
            $this->data["current_user"] = User::get_users(" AND u.user_id = ".Auth::user()->user_id." ");
            $this->data["current_user"] = $this->data["current_user"][0];
            $this->user_id = $this->data["current_user"]->user_id;
            $this->related_user_id = $this->data["current_user"]->related_id;
        }
        

        //csrf increase time
        $config = config('session');
        $config["lifetime"] = 7200;

        $this->data["meta_title"]="Points of sale";
        $this->data["meta_desc"]="Points of sale";
        $this->data["meta_keywords"]="Points of sale";


        #region get current date
        $this->get_current_date();
        #endregion

        $this->data["current_notifications"] = notification_m::get_notifications(" where not_to_userid = $this->user_id order by created_at desc limit 10 ");



        $company = company_m::get_data(" AND comp.company_id = 1 ");
        $this->data["company"] = $company[0];

        if (isset($this->data["company"]->sell_tax_percent))
        {
            $this->tax_percent = $this->data["company"]->sell_tax_percent;
        }

    }

    /**
     * @param $request >> received by form
     * @param int $user_id >> from current session
     * @param $file_name >> from input file name
     * @param $folder >> /folder_name under uploads
     * @param int $width
     * @param int $height
     * @param array $ext_arr >> additional array of allowed extensions
     * @param bool $return_only_name
     * @param string $absolute_upload_path
     * @return array|string >> array if uploaded
     */
    public function cms_upload($request, $user_id = 0, $file_name, $folder, $width = 0, $height = 0, $ext_arr = array(), $return_only_name=false, $absolute_upload_path="")
    {

        $uploaded = array();
        if (!empty($file_name) && isset($request))
        {

            if ($file_objs = $request->file($file_name))
            {
                if(!is_array($file_objs)){
                    $file_objs=array($file_objs);
                }

                foreach ($file_objs as $key => $file_obj) {

                    if ($file_obj == null){
                        continue;
                    }

                    $uploaded_file_ext = $file_obj->getClientOriginalExtension();
                    $uploaded_origin_file_name = $file_obj->getClientOriginalName().'.'.$uploaded_file_ext;
                    $uploaded_file_encrypted_name = md5($user_id.time().$file_name.$file_obj->getClientOriginalName()).".".$uploaded_file_ext;
                    $uploaded_file_path = "uploads".$folder;

                    $uploaded_full_path_to_file = $uploaded_file_path.'/'.$uploaded_file_encrypted_name;

                    if ($absolute_upload_path != "")
                    {
                        $uploaded_file_path = $absolute_upload_path;
                    }

                    if (in_array($uploaded_file_ext, array("mp3","mp4","jpeg","png","jpg","MP4","JPEG","PNG","JPG","xls","XLS","doc","docx","zip","rar","xlsx","XLSX","csv","CSV","pdf","PDF"))||(count($ext_arr)>0 && in_array($uploaded_file_ext, $ext_arr)))
                    {
                        $file_obj->move($uploaded_file_path,$uploaded_file_encrypted_name);

                        if ($width >0 && $height >0)
                        {
                            $img = Image::make(($uploaded_full_path_to_file))->resize($width, $height);
                            $img->save(($uploaded_full_path_to_file),70);
                        }

                        if ($return_only_name == true || $return_only_name == "true")
                        {
                            $uploaded[] = $uploaded_file_encrypted_name;
                        }
                        else{
                            $uploaded[] = $uploaded_full_path_to_file;
                        }

                    }
                    else
                    {
                        return "not allowed type";
                    }

                }


            }
            else{
                return "There is not file to upload";
            }


        }
        else{
            return "There is not input file or comming request !!";
        }

        return $uploaded;

    }


    /**
     * @param $request >> received by form
     * @param null $item_id >> null for insert || id for edit
     * @param $img_file_name >> from input file name
     * @param $new_title
     * @param $new_alt
     * @param $upload_new_img_check
     * @param $upload_file_path >> /folder_name
     * @param $width
     * @param $height
     * @param $photo_id_for_edit
     * @param array $ext_arr
     * @return int|string
     */
    public function general_save_img($request , $item_id=null, $img_file_name, $new_title, $new_alt, $upload_new_img_check, $upload_file_path, $width, $height, $photo_id_for_edit, $ext_arr=array())
    {

        $new_title=($new_title==null)?"":$new_title;
        $new_alt=($new_alt==null)?"":$new_alt;

        //$item_id could be pro id , cat_id any thing
        $photo_id="not_enter";

        $upload_img=$this->cms_upload($request,$this->user_id,$img_file_name,$upload_file_path,$width,$height,$ext_arr);

        if ($item_id==null)
        {
            //save attachment first

            if ((!(count($upload_img)>0) && !is_array($upload_img)) || (!(count($upload_img)>0) && is_array($upload_img)) )
            {
                return 0;
            }

            //save main photo
            $upload_img=$upload_img[0];

            $photo_id=attachments_m::save_img(null,$new_title,$new_alt,$upload_img);

            return $photo_id;
        }//end check of upload file


        if ($item_id!=null&&$photo_id_for_edit>0) {
            //edit photo data
            //update image info

            if (is_array($upload_img) && $upload_new_img_check=="on")
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt);
        }

        if ($item_id!=null&&$photo_id_for_edit==0) {
            //add new photo data if edit item has new image
            if (is_array($upload_img) && $upload_new_img_check=="on")
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            elseif (is_array($upload_img) && count($upload_img) > 0)
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            else{
                return $photo_id_for_edit;
            }

        }

            return $photo_id;
    }


    /**
     * @param $request >> from form
     * @param string $field_name >> form_input_file_name
     * @param int $width
     * @param int $height
     * @param $new_title_arr
     * @param $new_alt_arr
     * @param string $json_values_of_slider
     * @param string $path >> /folder_name
     * @param string $old_title_arr old values of existing imgages
     * @param string $old_alt_arr old values of existing images
     * @return array|string
     */
    public function general_save_slider($request, $field_name="", $width=0, $height=0, $new_title_arr, $new_alt_arr, $json_values_of_slider="",$old_title_arr,$old_alt_arr,$path="")
    {

        if ($path=="") {
            $path=$field_name;
        }
        //upload new files
        $slider_file = $this->cms_upload($request , $this->user_id,"$field_name",$folder="$path",$width,$height);//array

        //update old_photos
        if (is_array($json_values_of_slider)&&count($json_values_of_slider)) {
            foreach ($json_values_of_slider as $key => $value) {
                $save_img_title="";
                if(isset($old_title_arr[$key])){
                    $save_img_title=$old_title_arr[$key];
                }

                $save_img_alt="";
                if(isset($old_alt_arr[$key])){
                    $save_img_alt=$old_alt_arr[$key];
                }

                $old_photo_id = attachments_m::save_img($value,$save_img_title,$save_img_alt);
            }
        }

        //add new photos
        if (count($slider_file)&&is_array($slider_file)) {
            foreach ($slider_file as $key => $value) {
                $save_img_title="";
                if(isset($new_title_arr[$key])){
                    $save_img_title=$new_title_arr[$key];
                }

                $save_img_alt="";
                if(isset($new_alt_arr[$key])){
                    $save_img_alt=$new_alt_arr[$key];
                }

                $json_values_of_slider[] = attachments_m::save_img(null,$save_img_title,$save_img_alt,$value);
            }//end foreach
        }

        return $json_values_of_slider;
    }

    /**
     * @param arr_of_str $content_row_title array of content_titles
     * important note the row you can fetch coreectly is the row the saved
     * by general_save_content
     *
     * $slider_imgs_field_arr== $slider_imgs_arr["edit_index_page"]=array("slider1","slider2","slider3")
     *
     */
    public function general_get_content($content_row_title=array(),$slider_imgs_field_arr=array()) {

        foreach ($content_row_title as $key => $title) {

            $cache_data=Cache::get($title."_".$this->lang_id);
            if($cache_data!=null){
                $this->data["$title"]=json_decode($cache_data);
                continue;
            }

            $this->data["$title"]="";
            $edit_content_row=site_content_m::where([
                "content_title"=>"$title",
                "lang_id"=>"$this->lang_id"
            ])->first();
            if(!is_object($edit_content_row)){
                continue;
            }
            $edit_content_row=  json_decode($edit_content_row->content_json);

            $generate_site_content_method=generate_site_content_methods_m::where("method_name","=","$title")->first();

            if(!is_object($generate_site_content_method)){
                return;
            }

            $generate_site_content_method=json_decode($generate_site_content_method->method_requirments);

            //get imgs data
            //check if there is imgs in $edit_content_row
            if (isset($edit_content_row->img_ids)&&  is_object($edit_content_row->img_ids)) {
                foreach ($edit_content_row->img_ids as $img_key => $img_id) {
                    $img_var_name=$img_key;
                    $edit_content_row->$img_var_name=attachments_m::find($img_id);
                    if(!is_object($edit_content_row->$img_var_name)){
                        $edit_content_row->$img_var_name=new \stdClass();
                        $edit_content_row->$img_var_name->path="";
                        $edit_content_row->$img_var_name->title="";
                        $edit_content_row->$img_var_name->alt="";
                    }
                }
            }

            //get slider data

            if (isset($slider_imgs_field_arr["$title"])&&  is_array($slider_imgs_field_arr["$title"])) {
                foreach ($slider_imgs_field_arr["$title"] as $key => $slider) {

                    if(!isset($edit_content_row->$slider)){
                        continue;
                    }

                    $slider_imgs_ids=$edit_content_row->$slider->img_ids;
                    $edit_content_row->$slider->imgs = array();

                    if (is_array($slider_imgs_ids) && count($slider_imgs_ids)) {
                        $edit_content_row->$slider->imgs=attachments_m::get_imgs_from_arr($slider_imgs_ids);
                    }

                }
            }

            //get selected data
            if(isset($generate_site_content_method->select_fields->fields)&&is_array($generate_site_content_method->select_fields->fields)){
                $select_fields=$generate_site_content_method->select_fields->fields;
                $select_tables=$generate_site_content_method->select_fields->tables;

                foreach ($select_fields as $key => $field) {
                    if(isset($edit_content_row->$field)){
                        //get field_value,model
                        $field_value=$edit_content_row->$field;
                        $model_name=$select_tables->$field->model;

                        $edit_content_row->$field=$model_name::find($field_value);
                    }
                }


            }
            //END get selected data

            $this->data["$title"]=$edit_content_row;

            Cache::put($title."_".$this->lang_id,json_encode($edit_content_row),60*60*30);

        }//end foreach

    }

    /**
     * @param array $emails >> array("aa@aa.com","cc@cc.com")
     * @param string $data >> string for default , array for advanced view
     * @param string $subject >> subject of your emails
     * @param string $path_to_file >> valid full path to attachment if exist
     * @return mixed
     */
    public function _send_email_to_custom($emails = array() , $data = "" , $subject = "", $sender = "seoera@seoera.net" , $path_to_file = "" )
    {

        $get_company = company_m::find(1);
        $sender = $get_company->company_email;

        if (is_array($emails) && count($emails) > 0)
        {

            if (is_array($data) && count($data) > 0)
            {
                $view = "email.advanced";
            }
            else{
                $data = ["default"=>$data];
                $view = "email.default";
            }

            $check = Mail::send($view,$data,function ($message) use ($emails , $subject, $sender, $path_to_file) {

                // changed once for every site
                $message->from($address = $sender);
                $message->sender($address = $sender);

                if ($path_to_file != "" && is_file($path_to_file))
                {
                    $message->attach($path_to_file);
                }

                $message->to($emails)->subject($subject);

            });

        }

        return Mail:: failures();


    }


    public function _send_email_to_all_users_type($user_type = "" , $data = "" , $subject = "", $sender = "seoera@seoera.net" , $path_to_file = "" )
    {

        $get_company = company_m::find(1);
        $sender = $get_company->company_email;

        if (!empty($user_type))
        {

            if ($user_type == "admin")
            {
                $all_users = User::get_users(" AND u.user_type = 'admin' OR u.user_type = 'factory_admin' ");
            }
            elseif ($user_type == "branch")
            {
                $all_users = User::get_users(" AND u.user_type = 'branch' OR u.user_type = 'branch_admin' ");
            }
            else{
                $all_users = User::where("user_type",$user_type)->get()->all();
            }

            if (is_array($all_users) && count($all_users))
            {
                $all_users_email = convert_inside_obj_to_arr($all_users,'email');
                if (is_array($all_users_email) && count($all_users_email))
                {
                    $this->_send_email_to_custom($emails = $all_users_email , $data, $subject, $sender, $path_to_file);
                }

            }

        }

    }

    public function _sms_send($phone,$verfication_code= "",$message = "You have registered in D.M.T") {

        $username = "D.M.T";
        $password = "123456";
        $user_phone = $phone;
        if (!empty($verfication_code))
        {
            $message .= "Verification Code : ".$verfication_code;
        }
        $sender = "D.M.T";

        $new_url = "http://altamayozsms.com/api/sendsms.php";
        $fields = array(
            "username"=>$username,
            'password' => $password,
            'numbers' => $user_phone,
            'message' => $message,
            'sender' => $sender
        );

        $ch = curl_init();
//
        curl_setopt($ch, CURLOPT_URL,$new_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
        $response = curl_exec ($ch);
//                    //dump($response);
        curl_close ($ch);

        $response_not_body="";

        switch ($response) {
            case 100:
                $response_not_body = "تم إرسال رسالة للرقم $user_phone";
                break;
            case 104:
                $response_not_body = "لا يوجد رصيد فى الحساب  ";
                break;
            case 105:
                $response_not_body = "الرصيد لا يكفى ";
                break;
            case 108:
                $response_not_body = "لا يوجد ارقام صالحة للارسال لرقم التليفون ";
                $response_not_body .= $user_phone;
                break;
            case 110:
                $response_not_body = "خطا فى الارسال من فضلك حاول مرة اخرى ";
                break;
            case 111:
                $response_not_body = "الارسال مغلق ";
                break;
            case 113:
                $response_not_body = "الحساب غير مفعل ";
                break;
            case 114:
                $response_not_body = "الحساب موقوف  ";
                break;
            case 115:
                $response_not_body = "غير مفعل جوال  ";
                break;
            case 116:
                $response_not_body = "غير مفعل بريد الكترونى  ";
                break;
        }



    }

    public function general_ajax_loader($model="",$model_static_function="",$func_params=[],$return_data_var_name="rows",$view_path=""){

        if($model==""||$model_static_function==""||$view_path==""){
            return "";
        }

        $this->data["$return_data_var_name"]=call_user_func_array("$model::$model_static_function",$func_params);

        return View::make($view_path,$this->data)->render();
    }


    public function track_my_action($action_desc = "")
    {

        if(!empty($action_desc))
        {
            site_tracking_m::create([
                "user_id" => $this->user_id,
                "action_desc" => $action_desc
            ]);
        }

    }


    public function send_user_notification($not_title = "" , $not_type = "" , $user_id = "")
    {
        if (!empty($not_title) && !empty($user_id))
        {
            notification_m::create([
                "not_title" => $not_title,
                "not_type" => $not_type,
                "not_to_userid" => $user_id
            ]);
        }
    }

    public function send_all_user_type_notifications($not_title = "" , $not_type = "" , $user_type = "")
    {
        if (!empty($not_title) && !empty($user_type))
        {

            if ($user_type == "admin")
            {
                $all_users = User::get_users(" AND u.user_type = 'admin' OR u.user_type = 'factory_admin' ");
            }
            elseif ($user_type == "branch")
            {
                $all_users = User::get_users(" AND u.user_type = 'branch' OR u.user_type = 'branch_admin' ");
            }
            else{
                $all_users = User::where("user_type",$user_type)->get()->all();
            }


            foreach($all_users as $key => $user)
            {
                $this->send_user_notification($not_title, $not_type, $user->user_id);
            }

        }
    }

    public function get_user_permissions()
    {

        $get_permissions = permissions_m::get_permissions( " where per.user_id =  ".$this->user_id." " );
        $get_permissions = collect($get_permissions)->groupBy('page_name');
        $get_permissions = $get_permissions->all();

        return $get_permissions;
    }

    public function check_user_permission($page = "" , $action = "")
    {
        if (!empty($page) && !empty($action))
        {
            $get_permission = permissions_m::get_permissions( " where per.user_id =  ".$this->user_id." 
                        AND per_page.page_name = '$page'" );
            if (is_array($get_permission) && count($get_permission))
            {
                $get_permission = $get_permission[0];
                if (isset($get_permission->$action) && $get_permission->$action)
                {
                    return true;
                }

                $additional_permissions=json_decode($get_permission->additional_permissions);
                if (is_array($additional_permissions)&&in_array($action,$additional_permissions))
                {
                    return true;
                }

            }
        }
        return false;
    }

    public function cleaning_input($request_data, $except = array())
    {
        foreach($request_data as $key => $value)
        {
            if (count($except) && in_array($key,$except))
            {
                continue;
            }
            $request_data[$key] = clean($value);
        }

        return $request_data;
    }

    public function save_transfer_money($to_user_id, $bill_id , $total_amount , $paid  , $desc = "",$amount_type="cash")
    {

        // delete last transfer money to this bill_id if exist
//        money_transfers_m::where("m_t_bill_id",$bill_id)->forceDelete();

        $input_arr = array();
        $input_arr["from_user_id"] = $this->related_user_id;
        $input_arr["to_user_id"] = $to_user_id;
        $input_arr["m_t_bill_id"] = $bill_id;
        $input_arr["m_t_amount"] = $paid;
        $input_arr["m_t_amount_type"] = $amount_type;
        $input_arr["m_t_status"] = 1;
        $input_arr["m_t_desc"] = $desc;

        if (!empty($paid))
        {
            // if you pay part or anything

            money_transfers_m::create($input_arr);
        }

        if ($total_amount > $paid)
        {

            $input_arr["m_t_amount"] = ($total_amount - $paid);
            $input_arr["m_t_status"] = 0;
            money_transfers_m::create($input_arr);

        }

    }


    public function get_current_date()
    {
        $current_date = date("j/ M/ Y");
        $this->data["current_date"] = $current_date;

        $monthes_arr = [
            "Jan"=>"يناير",
            "Feb"=>"فبراير",
            "Mar"=>"مارس",
            "Apr"=>"ابريل",
            "May"=>"مايو",
            "Jun"=>"يونيو",
            "Jul"=>"يوليو",
            "Aug"=>"أغسطس",
            "Sep"=>"سبتمبر",
            "Oct"=>"أكتوبر",
            "Nov"=>"نوفمبر",
            "Dec"=>"ديسمبر",
        ];

        preg_match('/(\d+)\/ ([a-zA-Z]+)\/ (\d+)/', $current_date, $output_array);

        if (!isset($this->data["current_lang"]) && isset($output_array[2]))
        {
            $get_month_name = $monthes_arr[$output_array[2]];
            $current_date = preg_replace('/(\d+)\/ ([a-zA-Z]+)\/ (\d+)/', '$1/ '.$get_month_name.'/ $3', $current_date);
            $this->data["current_date"] = $current_date;
        }
    }

}
