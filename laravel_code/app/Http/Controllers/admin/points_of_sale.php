<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\points_of_sale_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class points_of_sale extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if (!check_permission($this->user_permissions,"admin/points_of_sale","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["all_branches_text"] = [];
        $this->data["all_branches_val"] = [];
        $this->data["points"] = [];


        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");
            $all_branches_text = array_merge(["الكل"],convert_inside_obj_to_arr($all_branches,"full_name"));
            $all_branches_val = array_merge([0],convert_inside_obj_to_arr($all_branches,"user_id"));
        }
        else{

            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $all_branches_text = convert_inside_obj_to_arr($all_branches,"full_name");
            $all_branches_val = convert_inside_obj_to_arr($all_branches,"user_id");
        }

        if(!count($all_branches_text)){
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يوجد فروع حاليا أضف فرع اولا</div>"
            )->send();
        }

        $this->data["all_branches_text"] = $all_branches_text;
        $this->data["all_branches_val"] = $all_branches_val;

        if (isset($_GET["branch_id"]))
        {
            $branch_id = $_GET["branch_id"];
            if ($branch_id == 0 && $this->data["current_user"]->user_type == "admin")
            {
                $this->data["points"] = points_of_sale_m::get_data();
            }
            else{
                $this->data["points"] = points_of_sale_m::get_data(" AND point.branch_id = $branch_id ");
            }
        }

        return view("admin.subviews.points_of_sale.show",$this->data);
    }

    public function save(Request $request,$point_of_sale_id = null)
    {

        $default_action = "add_action";
        if ($point_of_sale_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/points_of_sale","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["all_branches"] = [];
        $this->data["point_data"] = "";

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        if(!count($all_branches)){
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يوجد فروع حاليا أضف فرع اولا</div>"
            )->send();
        }

        $this->data["all_branches"] = $all_branches;

        if ($point_of_sale_id != null)
        {
            $point_data = points_of_sale_m::findOrFail($point_of_sale_id);
            $this->data["point_data"] = $point_data;
        }

        if ($request->method()=="POST")
        {

            $this->validate($request,
                [
                    "point_of_sale_code"=>"required|unique:points_of_sale,point_of_sale_code,".$point_of_sale_id.",point_of_sale_id,deleted_at,NULL",
                    "point_of_sale_desc"=>"required",
                    "branch_id"=>"required"
                ],
                [
                    "point_of_sale_code.required" => "الكود مطلوب إدخالة",
                    "point_of_sale_code.unique" => "الكود موجود مسبقا",
                    "point_of_sale_desc.required" => "الوصف مطلوب إدخالة",
                    "branch_id.required" => "الفرع مطلوب إدخالة",
                ]
            );

            $request = clean($request->all());

            if ( isset($request["is_exist_password_for_return"]) && !empty($request["return_password"]))
            {
                $request["is_exist_password_for_return"] = 1;
                $request["return_password"] = bcrypt($request["return_password"]);
            }
            else{
                $request["is_exist_password_for_return"] = 0;
                if ($point_of_sale_id != null)
                {
                    $request["return_password"] = $point_data->return_password;
                }
            }

            if (isset($request["set_cash_on_my_balance"]))
            {
                $request["set_cash_on_my_balance"] = 1;
            }
            else{
                $request["set_cash_on_my_balance"] = 0;
            }


            if ($point_of_sale_id == null)
            {
                $point_data = points_of_sale_m::create($request);
                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم تسجيل نقطه بيع جديدة "
                );
                #endregion
            }
            else{
                $point_data->update($request);
                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم تعديل بيانات نقطه بيع "
                );
                #endregion
            }

            $msg = "<div class='alert alert-success'>  تم حفظ نقطه البيع بنجاح ..  </div>";

            return Redirect::to('admin/points_of_sale')->with([
                "msg"=>$msg
            ])->send();
        }


        return view("admin.subviews.points_of_sale.save")->with($this->data);
    }

}
