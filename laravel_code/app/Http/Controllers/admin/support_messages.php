<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\dashbaord_controller;
use App\models\support_messages_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class support_messages extends dashbaord_controller
{

    public function __construct()
    {
        parent::__construct();

        if (!in_array("support_messages",Auth::user()->permissions))
        {
            Redirect::to('admin/dashboard')->send();
        }

    }

    public function index()
    {
        $this->data["all_messages"] = support_messages_m::all();
        return view("admin.subviews.support_messages.show")->with($this->data);
    }

}
