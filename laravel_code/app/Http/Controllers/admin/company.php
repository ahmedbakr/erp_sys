<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\attachments_m;
use App\models\company_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class company extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function save_company(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/banks","edit_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $company_data = company_m::find(1);

        if (!is_object($company_data))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>لا يوجد بيانات للشركه لعرضها !!</div>"]
            )->send();
        }

        $this->data["company_data"] = $company_data;
        $company_logo_id = $company_data->company_logo_id;
        $this->data["logo_obj"] = attachments_m::find($company_logo_id);


        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "company_name" => "required"
            ]);

            $request["company_logo_id"] = $this->general_save_img($request , $item_id=null, "company_logo",
                $new_title = "", $new_alt = "",
                $upload_new_img_check = $request->get("company_logo_checkbox"), $upload_file_path = "/company/",
                $width = 0, $height = 0, $photo_id_for_edit = $company_logo_id);

            $request = clean($request->all());


            $company_data->update($request);


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم تعديل بيانات الشركة "
            );
            #endregion

            return  Redirect::to('admin/company')->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.company.save")->with($this->data);
    }

}
