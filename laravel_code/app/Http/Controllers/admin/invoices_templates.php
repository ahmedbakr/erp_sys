<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\invoices\invoices_template_content_m;
use App\models\invoices\invoices_templates_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class invoices_templates extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if (!check_permission($this->user_permissions,"admin/invoices_templates","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $get_all_templates = invoices_templates_m::get_template_data();
        $this->data["all_invoices"] = $get_all_templates;


        return view("admin.subviews.invoices_templates.show_templates",$this->data);

    }


    public function save_template(Request $request , $inv_temp_id = null)
    {

        $default_action = "add_action";

        if ($inv_temp_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/invoices_templates","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["template"] = "";
        if ($inv_temp_id != null)
        {
            $template = invoices_templates_m::find($inv_temp_id);
            if (is_object($template))
            {
                $this->data["template"] = $template;
            }
            else{
                return Redirect::to('admin/invoices_templates/save_template')->with([
                    "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود يمكنك إضافة جديد من هنا  </div>"
                ])->send();
            }
        }


        $this->data["all_branches"] = "";
        $get_all_branches = User::where("user_type","branch")->get();
        $this->data["all_branches"] = $get_all_branches->all();

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "template_name" => "required|unique:invoices_templates,template_name,".$inv_temp_id.",inv_temp_id,deleted_at,NULL",
                "template_width" => "required|numeric|min:0",
                "template_height" => "required|numeric|min:0",
            ]);

            $request = clean($request->all());

            if ($inv_temp_id == null)
            {

                $request["table_position_left"] = "0px";
                $request["table_position_top"] = "0px";
                $request["table_width"] = "100%";
                $request["table_height"] = "auto";

                // insert
                $new_obj = invoices_templates_m::create($request);
                $inv_temp_id = $new_obj->inv_temp_id;
                $desc_msg = "تم إضافة قالب فاتورة جديد برقم $inv_temp_id ";

                #region save default items

                $items_arr = [
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "logo",
                        "item_type" => "image",
                        "item_label" => "",
                        "item_width" => "100px",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "invoice_number",
                        "item_type" => "static",
                        "item_label" => "Invoice #",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "cashier_name",
                        "item_type" => "static",
                        "item_label" => "Cashier",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "customer_tel",
                        "item_type" => "static",
                        "item_label" => "Customer Tel",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "created",
                        "item_type" => "static",
                        "item_label" => "Created",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "item_name",
                        "item_type" => "static",
                        "item_label" => "Item Name",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "item_desc",
                        "item_type" => "static",
                        "item_label" => "Item Desc",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "item_quantity",
                        "item_type" => "static",
                        "item_label" => "Item Quantity",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "item_price",
                        "item_type" => "static",
                        "item_label" => "Item Price",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "items_pre_total",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "items_coupon",
                        "item_type" => "static",
                        "item_label" => "Coupon",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "items_discount",
                        "item_type" => "static",
                        "item_label" => "Discount",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "items_post_total",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "total_cash_paid",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "total_atm_paid",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "total_paid",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "total_remain",
                        "item_type" => "static",
                        "item_label" => "Total",
                        "item_width" => "0",
                    ],
                    [
                        "inv_temp_id" => $inv_temp_id,
                        "item_name" => "invoice_note",
                        "item_type" => "static",
                        "item_label" => "المرتجع بحد اقصي 15 يوم من تاريخ الشراء",
                        "item_width" => "0",
                    ],
                ];
                invoices_template_content_m::insert($items_arr);

                #endregion

            }
            else{

                // update
                $check = invoices_templates_m::find($inv_temp_id)->update($request);
                $desc_msg = "تم تعديل قالب الفاتورة رقم $inv_temp_id";
            }

            $desc_msg .= "يمكنك تعديل العناصر للقالب من هنا ";

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion


            return Redirect::to('admin/invoices_templates/show_invoice_items/'.$inv_temp_id)->with([
                "msg" => "<div class='alert alert-success'> $desc_msg </div>"
            ])->send();

        }

        return view("admin.subviews.invoices_templates.save_template",$this->data);
    }

    
    public function show_invoice_items($inv_temp_id)
    {

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $get_invoice_data = invoices_templates_m::find($inv_temp_id);
        if (is_object($get_invoice_data))
        {
            $this->data["invoice_data"] = $get_invoice_data;
        }
        else{
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> القالب غير موجود  </div>"
            ])->send();
        }

        $get_all_items = invoices_templates_m::get_data(" AND inv_temp.inv_temp_id = $inv_temp_id ");
        $this->data["invoice_items"] = $get_all_items;
//        dump($get_all_items);

        return view("admin.subviews.invoices_templates.items.show",$this->data);

    }


    public function save_template_item(Request $request , $inv_temp_id , $item_id = null)
    {

        $default_action = "add_action";
        if ($item_id != null)
        {
            $default_action = "edit_action";
        }
        else{
            return Redirect::to('admin/invoices_templates')->with([
                "msg" => "<div class='alert alert-danger'> لا توجد إضافة عناصر لقالب الفاتورة الان !!  </div>"
            ])->send();
        }

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["item_obj"] = "";
        $this->data["inv_temp_id"] = $inv_temp_id;
        $item_img = 0;

        if ($item_id != null)
        {
            $item_obj = invoices_template_content_m::get_data(" AND itc.item_id = $item_id ");
            if (is_array($item_obj) && count($item_obj) && isset($item_obj[0]))
            {
                $item_obj = $item_obj[0];

                if ($item_obj->inv_temp_id == $inv_temp_id)
                {
                    $this->data["item_obj"] = $item_obj;
                    $item_img = $item_obj->item_img;
                }
                else{
                    return Redirect::to('invoices_templates/show_invoice_items/'.$inv_temp_id)->with([
                        "msg" => "<div class='alert alert-danger'> العنصر خاص ببيانات هذا القالب !!  </div>"
                    ])->send();
                }

            }
            else{
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> عنصر القالب غير موجود!!  </div>"
                ])->send();
            }
        }

        if ($request->method() == "POST")
        {

            $request["item_img"] = $this->general_save_img($request , $item_id, "item_img_file",
                $new_title = "", $new_alt = "",
                $upload_new_img_check = $request["item_img_checkbox"], $upload_file_path = "/invoices_templates",
                $width = 0, $height = 0, $photo_id_for_edit = $item_img);

            $request = $request->all();

            $request["inv_temp_id"] = $inv_temp_id;



            if ($item_id == null)
            {
                // insert
                $new_obj = invoices_template_content_m::create($request);
                $item_id = $new_obj->item_id;
                $desc_msg = "$inv_temp_id تم إضافة عنصر جديد لقالب الفاتورة رقم ";

            }
            else{
                //update
                $check = invoices_template_content_m::find($item_id)->update($request);
                $desc_msg = " تم تعديل العنصر رقم $item_id لقالب الفاتورة رقم $inv_temp_id";

            }


            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion


            return Redirect::to('admin/invoices_templates/save_template_item/'.$inv_temp_id.'/'.$item_id)->with([
                "msg" => "<div class='alert alert-success'> $desc_msg </div>"
            ])->send();

        }

        return view("admin.subviews.invoices_templates.items.save",$this->data);
    }


    public function change_template_default(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/invoices_templates","edit_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $desc_msg = "تم تغيير حالة القالب رقم $item_id";

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

            }

        }

        echo json_encode($output);
    }

    public function change_template_item_status(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","edit_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $desc_msg = "تم تغيير حالة ظهور العنصر رقم $item_id";

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

            }

        }

        echo json_encode($output);
    }


    public function remove_template(Request $request)
    {

        $output = array();
        $item_id = (int)$request->get("item_id");
        $model_name = $request->get("table_name"); // App\User

        if (!check_permission($this->user_permissions,"admin/invoices_templates","delete_action",$this->data["current_user"]))
        {
            $output["deleted"] = "ليس لديك الصلاحية لفعل هذا الامر";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";

                $desc_msg = "لقد تم مسح قالب فاتورة رقم #$item_id";

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

            }

        }

        echo json_encode($output);

    }


    public function remove_template_item(Request $request)
    {

        $output = array();
        $item_id = (int)$request->get("item_id");
        $model_name = $request->get("table_name"); // App\User

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","delete_action",$this->data["current_user"]))
        {
            $output["deleted"] = "ليس لديك الصلاحية لفعل هذا الامر";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";

                $desc_msg = "لقد تم مسح عنصر رقم $item_id من الفاتورة";

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

            }

        }

        echo json_encode($output);

    }


    #region template print


    public function preview_template_items($inv_temp_id)
    {

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","save_items_position",$this->data["current_user"]))
        {
            return Redirect::to('admin/invoices_templates')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["template_data"] = "";
        $this->data["template_items"] = "";

        $template_data = invoices_templates_m::find($inv_temp_id);

        if (is_object($template_data))
        {

            $this->data["template_data"] = $template_data;

            // get template items
            $template_items = invoices_template_content_m::get_data(" AND itc.inv_temp_id = $inv_temp_id");
            $template_items = collect($template_items)->groupBy("item_name");
            $template_items = $template_items->all();
            $this->data["template_items"] = $template_items;
        }
        else{
            return Redirect::to('admin/invoices_templates')->with([
                "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود او لا يحتوي علي عناصر !!! </div>"
            ])->send();
        }

//        dump($template_data);
//        dump($template_items);

        return view("admin.subviews.invoices_templates.print.save_items_position",$this->data);
    }


    public function save_template_items_position(Request $request)
    {
        $output = [];
        $output["success"] = "";
        $output["error"] = "";

        if (!check_permission($this->user_permissions,"admin/invoice_template_items","save_items_position",$this->data["current_user"]))
        {
            $output["error"] = "ليس لديك الصلاحية للدخول لهذة الصفحة !!";
            echo json_encode($output);
            return;
        }

        $items = clean($request->all());

        if (isset($items["inv_temp_id"]))
        {
            $inv_temp_id = intval($items["inv_temp_id"]);
            $template_obj = invoices_templates_m::find($inv_temp_id);
            unset($items["inv_temp_id"]);
        }
        else{
            $output["error"] = "بيانات القالب غير موجودة !!";
            echo json_encode($output);
            return;
        }

        foreach($items as $key => $item)
        {

            if($key == "table")
            {
                $template_obj->update([
                    "table_position_left" => $item["left"],
                    "table_position_top" => $item["top"],
                    "table_width" => $item["width"],
                    "table_height" => $item["height"],
                ]);
                continue;
            }

            $item_obj = invoices_template_content_m::find($key);
            if (is_object($item_obj))
            {
                $item_obj->update([
                    "item_position_left" => $item["left"],
                    "item_position_top" => $item["top"],
                    "item_width" => $item["width"],
                    "item_height" => $item["height"],
                ]);
            }

        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تعديل فاتورة القالب رقم $inv_temp_id"
        );
        #endregion

        $output["success"] = "success";
        echo json_encode($output);
        return;
    }


    #endregion

}
