<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;

use App\Http\Requests;
use App\models\bills\clients_bills_m;
use App\models\branch_target_m;
use App\models\commission\day_commission_m;
use App\models\money_transfers_m;
use App\models\product\product_materials_m;
use App\models\product\products_m;
use App\User;
use Carbon\Carbon;

class dashboard extends is_admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {

        $all_users = User::get_users(" AND u.user_active = 1 AND u.user_can_login = 1");
        $all_users = collect($all_users)->groupBy('user_type');
        $all_users = $all_users->all();

        $this->data["branches_count"] = (isset($all_users["branch"])?count($all_users["branch"]->all()):0);
        $this->data["suppliers_count"] = (isset($all_users["supplier"])?count($all_users["supplier"]->all()):0);
        $this->data["products_count"] = products_m::get_count();
        $this->data["product_materials_count"] = product_materials_m::get_count();

        $this->data["today_bills"] = [
            "total" => 0,
            "return" => 0,
            "return_cash" => 0,
            "return_atm" => 0,
            "cash" => 0,
            "atm" => 0,
            "remain" => 0,
            "discount" => 0,
            "tax" => 0,
            "tax_return" => 0,
        ];

        $this->data["this_month_bills"] = [
            "total" => 0,
            "return" => 0,
            "return_cash" => 0,
            "return_atm" => 0,
            "cash" => 0,
            "atm" => 0,
            "remain" => 0,
            "discount" => 0,
            "tax" => 0,
            "tax_return" => 0,
        ];

        $this->data["branch_target"] = "";

        $current_hour = date("H");

        $get_current_date = date("Y-m-d");
        $get_start_month_date = date('Y-m-1');


        $get_current_datetime = date("Y-m-d H:i:s");
        $get_start_month_datetime = date('Y-m-1 05:00:00');
        $current_data=Carbon::now();
        if ($current_hour >= 5)
        {
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day+1, $hour = 4,59);
        }
        else{
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }

        $start_day=$start_day->toDateTimeString();
        $end_day=$end_day->toDateTimeString();

        $this->data["start_day_date"]=$start_day;
        $this->data["end_day_date"]=$end_day;


        $this->data["current_date"] = $get_current_date;
        $this->data["start_month_date"] = $get_start_month_date;


        $this->data["today_total_commissions"] = 0;
        $this->data["this_month_total_commissions"] = 0;

        if ($this->data["current_user"]->user_type == "admin")
        {

            $today_client_bills = clients_bills_m::
            whereBetween("client_bill_date",[$start_day,$end_day])->
            where("is_reviewed",1)->get()->all();
            $this->data["today_bills"] = $this->_count_bills($today_client_bills,$this->data["today_bills"]);

            $this->data["today_bills"]["return"] = 0;
            $this->data["today_bills"]["return_cash"] = 0;
            $this->data["today_bills"]["return_atm"] = 0;

            $get_all_branches = User::where("user_type","branch")->get()->all();
            $get_all_branches_ids = convert_inside_obj_to_arr($get_all_branches,"user_id");

            #region get today return amount for previous bills

                if(count($get_all_branches_ids))
                {
                    $get_all_branches_ids = implode(',',$get_all_branches_ids);

                    $get_all_today_return_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.from_user_id in ($get_all_branches_ids)
                                )
                                AND (money.created_at) >= '$start_day'
                                AND (money.created_at) <= '$end_day' 
                            ");
                    if(count($get_all_today_return_money))
                    {
                        foreach($get_all_today_return_money as $key => $value)
                        {
                            if ($value->m_t_amount_type == "cash")
                            {
                                $this->data["today_bills"]["return_cash"] += $value->m_t_amount;
                                $this->data["today_bills"]["return"] += $value->m_t_amount;
                            }
                            else{
                                $this->data["today_bills"]["return_atm"] += $value->m_t_amount;
                                $this->data["today_bills"]["return"] += $value->m_t_amount;
                            }

                        }
                    }
                }

            #endregion

            $this_month_client_bills = clients_bills_m::
                whereDate("client_bill_date",">=","$get_start_month_date")
                ->whereDate("client_bill_date","<=","$get_current_date")
                ->where("is_reviewed",1)->get()->all();


            $this->data["this_month_bills"] = $this->_count_bills($this_month_client_bills,$this->data["this_month_bills"]);

            $this->data["this_month_bills"]["return"] = 0;
            $this->data["this_month_bills"]["return_cash"] = 0;
            $this->data["this_month_bills"]["return_atm"] = 0;

            #region get month return amount for previous bills

            if(isset($get_all_branches_ids) && !empty($get_all_branches_ids))
            {
                $get_all_month_return_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.from_user_id in ($get_all_branches_ids)
                                )
                                AND date(money.created_at) >= '$get_start_month_date'
                                AND date(money.created_at) <= '$get_current_date' 
                            ");
                if(count($get_all_month_return_money))
                {
                    foreach($get_all_month_return_money as $key => $value)
                    {
                        if ($value->m_t_amount_type == "cash")
                        {
                            $this->data["this_month_bills"]["return_cash"] += $value->m_t_amount;
                            $this->data["this_month_bills"]["return"] += $value->m_t_amount;
                        }
                        else{
                            $this->data["this_month_bills"]["return_atm"] += $value->m_t_amount;
                            $this->data["this_month_bills"]["return"] += $value->m_t_amount;
                        }

                    }
                }
            }

            #endregion

            $get_current_month = date('m');
            $get_current_year = date('Y');

            $get_branch_target_for_this_month = branch_target_m::
                where("year",$get_current_year)
                ->where("month",$get_current_month)
                ->get()->all();


            if (is_array($get_branch_target_for_this_month) && count($get_branch_target_for_this_month))
            {
                $this->data["branch_target"] = array_sum(convert_inside_obj_to_arr($get_branch_target_for_this_month,"target_amount"));
            }

            #region today_total_commissions
                $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$start_day'
                    AND commission.updated_at <= '$end_day'
                 ");

                if (count($get_today_total_commissions))
                {
                    $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                    $get_today_total_commissions = array_sum($get_today_total_commissions);
                    $this->data["today_total_commissions"] = $get_today_total_commissions;
                }
            #endregion

            #region this_month_total_commissions
                $get_this_month_total_commissions = day_commission_m::get_data(" 
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$get_start_month_datetime'
                    AND commission.updated_at <= '$get_current_datetime'
                 ");

                if (count($get_this_month_total_commissions))
                {
                    $get_this_month_total_commissions = convert_inside_obj_to_arr($get_this_month_total_commissions,"total_commission_value");
                    $get_this_month_total_commissions = array_sum($get_this_month_total_commissions);
                    $this->data["this_month_total_commissions"] = $get_this_month_total_commissions;
                }
            #endregion

        }
        else{

            // get bills for branch

            if ($this->data["current_user"]->user_type == "branch_admin")
            {

                $today_client_bills = clients_bills_m::
                    where("branch_id",$this->branch_id)->
                    whereBetween("client_bill_date",[$start_day,$end_day])->
                    where("is_reviewed",1)->get()->all();
                $this->data["today_bills"] = $this->_count_bills($today_client_bills,$this->data["today_bills"]);


                $this->data["today_bills"]["return"] = 0;
                $this->data["today_bills"]["return_cash"] = 0;
                $this->data["today_bills"]["return_atm"] = 0;

                #region get today return amount for previous bills

                $get_all_today_return_money = money_transfers_m::get_data("
                            AND money.m_t_bill_type = 'client_bill'
                            AND money.m_t_status = 1
                            AND (
                                money.from_user_id = $this->branch_id
                            )
                            AND (money.created_at) >= '$start_day'
                            AND (money.created_at) <= '$end_day' 
                        ");

                if(count($get_all_today_return_money)) {
                    foreach ($get_all_today_return_money as $key => $value) {
                        if ($value->m_t_amount_type == "cash") {
                            $this->data["today_bills"]["return_cash"] += $value->m_t_amount;
                            $this->data["today_bills"]["return"] += $value->m_t_amount;
                        } else {
                            $this->data["today_bills"]["return_atm"] += $value->m_t_amount;
                            $this->data["today_bills"]["return"] += $value->m_t_amount;
                        }

                    }
                }

                #endregion

                $get_current_month = date('m');
                $get_current_year = date('Y');


                $this_month_client_bills = clients_bills_m::
                    where("branch_id",$this->branch_id)
                    ->whereDate("client_bill_date",">=","$get_start_month_date")
                    ->whereDate("client_bill_date","<=","$get_current_date")
                    ->where("is_reviewed",1)->get()->all();

                $this->data["this_month_bills"] = $this->_count_bills($this_month_client_bills,$this->data["this_month_bills"]);


                $this->data["this_month_bills"]["return"] = 0;
                $this->data["this_month_bills"]["return_cash"] = 0;
                $this->data["this_month_bills"]["return_atm"] = 0;

                #region get month return amount for previous bills

                    $get_all_month_return_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.from_user_id = $this->branch_id
                                )
                                AND date(money.created_at) >= '$get_start_month_date'
                                AND date(money.created_at) <= '$get_current_date' 
                            ");
                    if(count($get_all_month_return_money))
                    {
                        foreach($get_all_month_return_money as $key => $value)
                        {
                            if ($value->m_t_amount_type == "cash")
                            {
                                $this->data["this_month_bills"]["return_cash"] += $value->m_t_amount;
                                $this->data["this_month_bills"]["return"] += $value->m_t_amount;
                            }
                            else{
                                $this->data["this_month_bills"]["return_atm"] += $value->m_t_amount;
                                $this->data["this_month_bills"]["return"] += $value->m_t_amount;
                            }

                        }
                    }

                #endregion


                $get_branch_target_for_this_month = branch_target_m::
                    where("branch_id",$this->branch_id)
                    ->where("year",$get_current_year)
                    ->where("month",$get_current_month)
                    ->get()->first();
                if (is_object($get_branch_target_for_this_month))
                {
                    $this->data["branch_target"] = $get_branch_target_for_this_month->target_amount;
                }

                #region today_total_commissions
                $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.user_id = $this->branch_id
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$start_day'
                    AND commission.updated_at <= '$end_day'
                 ");

                if (count($get_today_total_commissions))
                {
                    $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                    $get_today_total_commissions = array_sum($get_today_total_commissions);
                    $this->data["today_total_commissions"] = $get_today_total_commissions;
                }
                #endregion

                #region this_month_total_commissions
                $get_this_month_total_commissions = day_commission_m::get_data(" 
                    AND commission.user_id = $this->branch_id
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$get_start_month_datetime'
                    AND commission.updated_at <= '$get_current_datetime'
                 ");

                if (count($get_this_month_total_commissions))
                {
                    $get_this_month_total_commissions = convert_inside_obj_to_arr($get_this_month_total_commissions,"total_commission_value");
                    $get_this_month_total_commissions = array_sum($get_this_month_total_commissions);
                    $this->data["this_month_total_commissions"] = $get_this_month_total_commissions;
                }
                #endregion

            }

        }



        return view("admin.subviews.dashboard",$this->data);
    }


    private function _count_bills($bills,$return_arr)
    {

        foreach($bills as $key => $bill)
        {

            $return_arr["total"] += (
                $bill->client_bill_total_amount -
                $bill->client_bill_total_return_amount
            );
            $return_arr["return"] += $bill->client_bill_total_return_amount;
            $return_arr["return_cash"] += $bill->client_bill_return_cash_amount;
            $return_arr["return_atm"] += $bill->client_bill_return_atm_amount;
            $return_arr["cash"] += $bill->client_total_paid_amount_in_cash;
            $return_arr["atm"] += $bill->client_total_paid_amount_in_atm;
            $return_arr["remain"] += $bill->client_bill_total_remain_amount;
            $return_arr["discount"] += $bill->bill_coupon_amount;
            if (!isset($return_arr["tax"]))
            {
                $return_arr["tax"] = 0;
            }
            if (!isset($return_arr["tax_return"]))
            {
                $return_arr["tax_return"] = 0;
            }

            $return_arr["tax"] += $bill->bill_tax_value;
            $return_arr["tax_return"] += $bill->bill_tax_return_value;

        }
        return $return_arr;
    }

}
