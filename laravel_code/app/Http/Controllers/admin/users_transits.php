<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\points_of_sale_m;
use App\models\admin\users_transits_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class users_transits extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if (!check_permission($this->user_permissions,"factory/users_transits","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $cond = "";
        if (isset($_GET["user_id"]))
        {
            $user_id = $_GET["user_id"];
            $cond .= " AND transit.user_id = $user_id ";
        }

        if (isset($_GET["start_date"]) && !empty($_GET["start_date"]))
        {
            $start_date = $_GET["start_date"];
            $cond .= " AND transit.user_transit_date >= '$start_date' ";
        }

        if (isset($_GET["end_date"]) && !empty($_GET["end_date"]))
        {
            $end_date = $_GET["end_date"];
            $cond .= " AND transit.user_transit_date <= '$end_date' ";
        }

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_users = User::get_users(" AND u.user_type = 'branch_admin' ");
            $this->data["all_transits"] = users_transits_m::get_data(" $cond ");
        }
        else{
            $all_users = User::get_users(" AND u.user_id = $this->user_id ");
            $this->data["all_transits"] = users_transits_m::get_data(" AND transit.user_id = $this->user_id $cond ");
        }

        $this->data["all_users"] = $all_users;

        return view("admin.subviews.users_transits.show",$this->data);
    }

    public function save(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/users_transits","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        $this->data["all_branches"] = [];
        $this->data["all_cashiers"] = [];
        $this->data["all_point_of_sales"] = points_of_sale_m::all()->all();

        if ($this->data["current_user"]->user_type == "admin")
        {
            $get_all_branches = User::get_users(" AND u.user_type = 'branch' ");
            $all_cashiers = User::get_users(" AND u.user_type = 'branch_admin' ");
        }
        else{
            $get_all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $all_cashiers = User::get_users(" AND u.user_id = $this->user_id ");
        }


        if (!count($get_all_branches))
        {
            return Redirect::to('admin/users/save/branch')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد فروع  يمكنك إضافة الفروع من هنا   </div>"
            ])->send();
        }

        if (!count($all_cashiers))
        {
            return Redirect::to('admin/users/save/branch_admin')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد بائعين  يمكنك إضافة من هنا   </div>"
            ])->send();
        }

        $this->data["all_branches"] = $get_all_branches;
        $this->data["all_cashiers"] = $all_cashiers;

        if ($request->method()=="POST")
        {

            $this->validate($request,
                [
                    "user_id"=>"required",
                    "to_branch"=>"required",
                    "user_transit_date"=>"required|date",
                ],
                [
                    "user_id.required" => "البائع مطلوب إدخالة",
                    "to_branch.required" => "الفرع مطلوب إدخالة",
                    "user_transit_date.required" => "التاريخ مطلوب إدخالة",
                ]
            );


            $request = clean($request->all());

            // check if user not have any duty
            $get_user = User::findOrFail($request["user_id"]);
            if ($get_user->user_duty_balance > 0)
            {
                return Redirect::to('admin/users_transits/save')->with([
                    "msg" => "<div class='alert alert-danger'>  البائع يوجد معه عهده لا يمكن نقله الأن !!  </div>"
                ])->send();
            }

            $request["from_branch"] = $get_user->related_id;

            users_transits_m::create($request);

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تسجيل تنقل جديد لمستخدم "
            );
            #endregion

            // update related_id to this user
            $get_user->update([
                "related_id" => $request["to_branch"],
                "point_of_sale_id" => $request["point_of_sale_id"],
            ]);

            $msg = "<div class='alert alert-success'> تم تسجيل التنقل بنجاح .. </div>";

            return Redirect::to('admin/users_transits')->with([
                "msg"=>$msg
            ])->send();
        }


        return view("admin.subviews.users_transits.save")->with($this->data);
    }

}
