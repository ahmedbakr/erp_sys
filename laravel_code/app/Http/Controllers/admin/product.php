<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\common\category;
use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\admin\price_list_m;
use App\models\attachments_m;
use App\models\product\branches_products_m;
use App\models\product\broken\broken_materials_m;
use App\models\product\broken\broken_products_m;
use App\models\product\materials_m;
use App\models\product\produce_product_review_m;
use App\models\product\product_codes_m;
use App\models\product\product_components_m;
use App\models\product\product_materials_m;
use App\models\product\product_prices_m;
use App\models\product\product_prices_values_m;
use App\models\product\product_select_codes_m;
use App\models\product\product_units_m;
use App\models\product\products_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class product extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    #region product_materials

    public function product_materials()
    {
        if (!check_permission($this->user_permissions,"product/product_materials","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_materials"] = product_materials_m::get_data();

        return view("admin.subviews.product.product_materials.index",$this->data);
    }

    public function save_product_materials(Request $request , $pro_mat_id = null)
    {
        $default_action = "add_action";
        $mat_id_required = "required";
        if (isset($pro_mat_id) && $pro_mat_id > 0)
        {
            $default_action = "edit_action";
            $mat_id_required = "";
        }

        if (!check_permission($this->user_permissions,"product/product_materials","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["materials"] = materials_m::all()->all();

        $this->data["material_data"] = "";
        if (isset($pro_mat_id) && $pro_mat_id > 0)
        {
            $pro_mat_data = product_materials_m::find($pro_mat_id);
            if (!is_object($pro_mat_data) || empty($pro_mat_data))
            {
                return Redirect::to('admin/product/save_product_materials')->with([
                    "msg" => "<div class='alert alert-danger'> الماده الخام غير موجودة , يمكنك اضافة جديد من هنا </div>"
                ])->send();
            }
            else{
                $this->data["material_data"] = $pro_mat_data;
            }
        }

        if ($request->method() == "POST")
        {
            $this->validate($request,[
                "mat_id" => "$mat_id_required",
                "mat_amount" => "required|min:0|numeric",
                "mat_price" => "required|min:0|numeric",
                "mat_pro_limit" => "required|min:0|numeric",
            ]);


            $request_data = $this->cleaning_input($request->all());


            // check if exist redirect to make edit
            if($pro_mat_id == null)
            {
                $check_exist = product_materials_m::where("mat_id",$request_data["mat_id"])->get()->first();
                if (is_object($check_exist))
                {
                    return Redirect::to('admin/product/save_product_materials/'.$check_exist->pro_mat_id)->with([
                        "msg" => "<div class='alert alert-danger'> هذة الماده الخام موجودة مسبقا يمكنك التعديل من هنا </div>"
                    ])->send();
                }
            }



            if ($pro_mat_id == null)
            {
                // add
                $pro_mat_obj = product_materials_m::create($request_data);
                $desc_msg = "اضافة كمية ماده خام فالمخزن للماده رقم..$pro_mat_obj->pro_mat_id";

            }
            else{
                //edit
                unset($request_data["_token"],$request_data["mat_id"]);
                $pro_mat_check = product_materials_m::find($pro_mat_id)->update($request_data);
                $desc_msg = "تعديل كمية الماده الخام رقم.$pro_mat_id";
            }


            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

            return Redirect::to('admin/product/save_product_materials')->with([
                "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح  </div>"
            ])->send();

        }


        return view("admin.subviews.product.product_materials.save",$this->data);
    }

    public function remove_product_materials(Request $request)
    {

        $output = array();
        $item_id = (int)$request->get("item_id");
        $model_name = $request->get("table_name"); // App\User

        if (!check_permission($this->user_permissions,"product/product_materials","delete_action",$this->data["current_user"]))
        {
            $output["deleted"] = "ليس لديك الصلاحية لفعل هذا الامر";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";


                $desc_msg = "لقد تم مسح الماده الخام رقم #$item_id";

                #region save in notification
                    $this->send_all_user_type_notifications(
                        $not_title = $desc_msg ,
                        $not_type = "info" ,
                        $user_type = "admin"
                    );
                #endregion


                #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = $desc_msg
                    );
                #endregion


                #region send email
                    $this->_send_email_to_all_users_type(
                        $user_type = "admin" ,
                        $data = $desc_msg,
                        $subject = "مسح كمية الماده الخام من المخزن"
                    );
                #endregion

            }

        }

        echo json_encode($output);

    }

    #endregion

    #region broken_product_materials

    public function broken_product_materials()
    {

        if (!check_permission($this->user_permissions,"product/broken_product_materials","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["broken_materials"] = broken_materials_m::get_data();

        return view("admin.subviews.product.product_materials.broken_index",$this->data);

    }

    public function save_broken_product_materials(Request $request)
    {

        $this->data["materials"] = materials_m::all()->all();
        $this->data["product_materials"] = product_materials_m::all()->all();
        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "mat_id" => "required",
                "b_m_quantity" => "required|min:0|numeric",
                "b_m_date" => "required"
            ]);

            $request_data = $this->cleaning_input($request->all());

            $get_product_material_data = product_materials_m::get_data(" AND pro_mat.mat_id = ".$request_data["mat_id"]);

            if (is_array($get_product_material_data) && count($get_product_material_data))
            {
                $get_product_material_data = $get_product_material_data[0];
                if ($get_product_material_data->mat_amount >= $request["b_m_quantity"])
                {

                    $old_quantity_on_stock = ($get_product_material_data->mat_amount);
                    $new_quantity_on_stock = ($get_product_material_data->mat_amount - $request["b_m_quantity"]);
                    // add new broken item
                    broken_materials_m::create($request_data);
                    $desc_msg = "إضافة هالك للماده الخام($get_product_material_data->mat_name) بكمية ".$request["b_m_quantity"];
                    #region save in notification
                        $this->send_all_user_type_notifications(
                            $not_title = $desc_msg ,
                            $not_type = "info" ,
                            $user_type = "admin"
                        );
                    #endregion


                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = $desc_msg
                        );
                    #endregion


                    #region send email
                        $this->_send_email_to_all_users_type(
                            $user_type = "admin" ,
                            $data = $desc_msg,
                            $subject = "إضافة هالك للماده الخام"
                        );
                    #endregion

                    // update product material quantity
                    product_materials_m::find($get_product_material_data->pro_mat_id)->update(
                        [
                            "mat_amount" => $new_quantity_on_stock
                        ]
                    );
                    $desc_msg = "تعديل كمية الماده الخام ($get_product_material_data->mat_name) من $old_quantity_on_stock الي $new_quantity_on_stock";
                    #region save in notification
                    $this->send_all_user_type_notifications(
                        $not_title = $desc_msg ,
                        $not_type = "info" ,
                        $user_type = "admin"
                    );
                    #endregion


                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = $desc_msg
                    );
                    #endregion


                    // check product material limit
                    if($new_quantity_on_stock < $get_product_material_data->mat_pro_limit)
                    {
                        $desc_msg = "كمية الماده الخام($get_product_material_data->mat_name) تجاوزت حد الطلب";
                        #region save in notification
                            $this->send_all_user_type_notifications(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_type = "admin"
                            );
                        #endregion


                        #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                        #endregion

                        #region send email
                        $this->_send_email_to_all_users_type(
                            $user_type = "admin" ,
                            $data = $desc_msg,
                            $subject = "تجاوز حد الطلب لكمية الماده الخام"
                        );
                        #endregion
                    }

                    return Redirect::to('admin/product/save_broken_product_materials')->with([
                        "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح  </div>"
                    ])->send();

                }
                else{
                    return Redirect::back()->withErrors([
                        "msg" => "<div class='alert alert-danger'> لا يمكنك إضافة هالك بمكية اكبر من المتاحه , انت لديك في المخزن  $get_product_material_data->mat_amount عنصر</div>"
                    ])->send();
                }

            }
            else{
                return Redirect::back()->withErrors([
                    "msg" => "<div class='alert alert-danger'> الماده الخام غير موجوده لاضافة هالك لها  </div>"
                ])->send();
            }


        }


        return view("admin.subviews.product.product_materials.broken_save",$this->data);
    }

    #endregion

    #region products_on_stock

    public function products_on_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/products_on_stock","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $search_product=$request->get("search_product","");
        $this->data["search_product"]=$search_product;

        if($search_product!=""){
            $get_products = products_m::
            where("pro_name","like","%$search_product%")->
            orWhere("pro_name_en","like","%$search_product%")->
            orWhere("pro_barcode","like","%$search_product%")->
            paginate(50);
        }
        else{
            $get_products = products_m::paginate(50);
        }

        $get_products->appends(Input::except("page"));

        $this->data["products_pagination"] = $get_products;
        $get_products_ids = convert_inside_obj_to_arr($get_products,'pro_id');

        $this->data["all_products"]=[];
        if(isset_and_array($get_products_ids)){
            $get_products_ids = implode(',',$get_products_ids);

            $this->data["all_products"] = products_m::get_pros(" 
                AND pro.pro_id in ($get_products_ids)
             ");
        }


        $this->data["all_branches"] = User::get_users(" AND user_type = 'branch' AND user_active = 1 AND user_can_login = 1 ");
        return view("admin.subviews.product.products_on_stock.index",$this->data);
    }

    public function import_products_to_stock(Request $request ,$cat_id)
    {

        if ($request->method() == "POST")
        {

            if (!check_permission($this->user_permissions,"product/products_on_stock","add_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }

            $name = $_FILES['pro_excel_file']['name'];
            $ext = (explode(".", $name));

            if (empty($_FILES) || !is_array($ext) || !count($ext) || !isset($ext[1]) || !in_array($ext[1],["xls","XLS","xlsx","XLSX"]))
            {
                return Redirect::to('admin/product/save_products_on_stock/'.$cat_id)->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك ارفع ملف اكسيل فقط  </div>"
                ])->send();
            }

            $this->data['cat_id'] = $cat_id;
            $this->data['input_arr'] = [];


            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

            $this->data["edit_count"] = 0;

            Excel::load($_FILES['pro_excel_file']['tmp_name'],function($reader)
            {

                // Getting all results
                $results = $reader->toArray();
                if (is_array($results) && count($results) > 0)
                {

                    foreach ($results as $key => $value)
                    {

                        $value['barcode'] = clean($value['barcode']);
                        $value['product_name'] = clean($value['product_name']);
                        $value['product_name_en'] = clean($value['product_name_en']);
                        $value['product_short_description'] = clean($value['product_short_description']);
                        $value['barcode'] = clean($value['barcode']);
                        $value['quantity'] = clean($value['quantity']);
                        $value['limit'] = clean($value['limit']);

                        $check_unique_barcode = products_m::where("pro_barcode",$value['barcode'])
                            ->orWhere("pro_name",$value['product_name'])
                            ->get()->all();

                        if (is_array($check_unique_barcode) && count($check_unique_barcode))
                        {
                            $check_unique_barcode = $check_unique_barcode[0];
                            products_m::find($check_unique_barcode->pro_id)->update([
                                "pro_quantity" => ($check_unique_barcode->pro_quantity + $value['quantity'])
                            ]);
                            $this->data["edit_count"] += 1;
                            continue;
                        }

                        if (empty($value['product_name']))
                        {
                            continue;
                        }

                        if (isset($this->data['input_arr'][$value['product_name']]))
                        {
                            $this->data['input_arr'][$value['product_name']]['pro_quantity'] += $value['quantity'];
                        }
                        else{
                            $this->data['input_arr'][$value['product_name']] = [
                                "cat_id" => $this->data['cat_id'],
                                "pro_name" => $value['product_name'],
                                "pro_name_en" => $value['product_name_en'],
                                "pro_desc" => $value['product_short_description'],
                                "pro_barcode" => $value['barcode'],
                                "pro_quantity" => $value['quantity'],
                                "pro_limit" => $value['limit'],
                            ];
                        }

                    }


                }

            });

            if (!count($this->data['input_arr']) && $this->data["edit_count"] == 0)
            {
                return Redirect::to('admin/product/save_products_on_stock/'.$cat_id)->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحه للإدخال برجاء الإلتزام بالملف المرفق وعدم تغيير اسماء اول صف  </div>"
                ])->send();
            }
            else if(!count($this->data['input_arr']) && $this->data["edit_count"] > 0){
                return Redirect::to('admin/product/save_products_on_stock/'.$cat_id)->with([
                    "msg" => "<div class='alert alert-success'> تم تعديل الكميات بنجاح  </div>"
                ])->send();

            }

            $split_arr = array_chunk($this->data['input_arr'],500);

            foreach ($split_arr as $key => $arr)
            {
                products_m::insert($arr);
            }



            $desc_msg = ' لقد تم تسجيل المنتجات بنجاح ';

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

            return Redirect::to('admin/product/save_products_on_stock/'.$cat_id)->with([
                "msg" => "<div class='alert alert-success'>$desc_msg  </div>"
            ])->send();


        }


    }


    public function save_products_on_stock(Request $request ,$cat_id ,$pro_id = null)
    {

        $this->data["cat_id"] = $cat_id;

        $default_action = "add_action";
        if (isset($pro_id) && $pro_id > 0)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"product/products_on_stock","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["product_codes"] = [];

        $this->data["product_units"] = product_units_m::all();

        $this->data["materials"] = materials_m::all()->all();
        $this->data["product_data"] = "";
        $this->data["product_components"] = "";
        $pro_img = 0;
        if (isset($pro_id) && $pro_id > 0)
        {
            $product_data = products_m::get_pros(" AND pro.pro_id = $pro_id ");
            $this->data["product_components"] = product_components_m::get_data(" AND pro.pro_id = $pro_id ");


            if (is_array($product_data) && count($product_data))
            {
                $product_data = $product_data[0];
                $pro_img = $product_data->pro_img;
                $this->data["product_data"] = $product_data;
            }
            else{
                return Redirect::to('admin/product/save_products_on_stock')->with([
                    "msg" => "<div class='alert alert-danger'> المنتج غير موجود , أضف جديد من هنا  </div>"
                ])->send();
            }

            $this->show_category_tree(true,"product");

            $this->data["product_codes"] = product_codes_m::get_data(" AND p_s_c.pro_id = $pro_id ");
            if(count($this->data["product_codes"]) == 0)
            {
                $this->data["product_codes"] = product_codes_m::all();
            }

        }
        else{
            $this->data["product_codes"] = product_codes_m::all();
        }


        if ($request->method() == "POST")
        {

            //TODO
            // i've comment all lines start with # symbol
            // we will check them again after customer approve this phase and

            $this->validate($request,
                [
//                    "mat_id.*" => "required",
    //#               "pro_comp_amount.*" => "required|numeric|min:0",
                    "pro_name" => "required",
                    "pro_quantity" => "min:0|numeric",
                    "pro_limit" => "min:0|numeric",
                    "pro_code_val.*" => "unique:product_select_codes,pro_code_val,".$pro_id.",pro_id,deleted_at,NULL",
                    "pro_barcode" => "unique:products,pro_barcode,".$pro_id.",pro_id,deleted_at,NULL",
                ],
                [
                    'pro_name.required' => ' اسم المنتج مطلوب إدخالة ',
                    'pro_barcode.unique' => ' كود المنتج موجود مسبقا ',
                ]
            );


            $request_data = $this->cleaning_input($request->all());
            $request_data["pro_quantity"] = intval($request_data["pro_quantity"]);
            $request_data["pro_limit"] = intval($request_data["pro_limit"]);


//#
//            if (count($request_data["mat_id"]) != count($request_data["pro_comp_amount"]))
//            {
//                return Redirect::back()->withErrors([
//                    "msg" => "<div class='alert alert-danger'> البيانات غير صالحه , حاول مرة اخري </div>"
//                ])->send();
//            }

            $request_data["pro_img"] = $this->general_save_img($request , $item_id=$pro_id, "pro_img_file",
                $new_title = "", $new_alt = "",
                $upload_new_img_check = $request["pro_img_checkbox"], $upload_file_path = "/products/",
                $width = 0, $height = 0, $photo_id_for_edit = $pro_img);


            if ($pro_id == null)
            {
                $request_data["cat_id"]=$cat_id;


                $new_pro_obj = products_m::create($request_data);
                $pro_id = $new_pro_obj->pro_id;

                $desc_msg = "إضافة منتج جديد ($new_pro_obj->pro_name) برقم.$pro_id";

            }
            else{
                unset($request_data["_token"]);

                $new_cat_id=$request->get("cat_id");
                if(!empty($new_cat_id)){
                    $cat_id=$new_cat_id;
                    $request_data["cat_id"]=$new_cat_id;
                }

                products_m::find($pro_id)->update($request_data);
                $desc_msg = "تعديل المنتج رقم.$pro_id";
            }


            if (isset($request['pro_code_val']) && isset($request['pro_code_id']) &&
                    (count($request['pro_code_val']) == count($request['pro_code_id']))
                )
            {
                $pro_code_val_arr = $request['pro_code_val'];
                $pro_code_id_arr = $request['pro_code_id'];
                $check_unique_arr = [];
                foreach($pro_code_val_arr as $key => $value)
                {



                    if (in_array($value,$check_unique_arr) && !empty($value))
                    {
                        return Redirect::to('admin/product/save_products_on_stock/'.$cat_id.'/'.$pro_id)->with([
                            "msg" => "<div class='alert alert-warning'> لكن الاكواد مكررة برجاء تغييرها ,  لقد تم الحفظ بنجاح  </div>"
                        ])->send();
                    }
                    $check_unique_arr[] = $value;

                    $get_old_pro_code = product_select_codes_m::
                        where("pro_id",$pro_id)
                        ->where("pro_code_id",$pro_code_id_arr[$key])->get()->first();
                    if (is_object($get_old_pro_code))
                    {

                        if (empty($value))
                        {
                            $get_old_pro_code->delete();
                        }
                        else{
                            $get_old_pro_code->update([
                                "pro_code_val" => $value
                            ]);
                        }

                    }
                    else{
                        product_select_codes_m::create([
                            "pro_code_id" => $pro_code_id_arr[$key],
                            "pro_id" => $pro_id,
                            "pro_code_val" => $value
                        ]);
                    }


                }

            }

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion


            /*#
            $input_product_components = [];
            foreach ($request_data["mat_id"] as $key => $value)
            {
                if ($request_data["pro_comp_amount"][$key] > 0)
                {
                    if (isset($input_product_components[$value]))
                    {
                        $input_product_components[$value]["pro_comp_amount"] += $request_data["pro_comp_amount"][$key];
                        continue;
                    }
                    $input_product_components[$value] = [
                        "pro_id" => $pro_id,
                        "mat_id" => $value,
                        "pro_comp_amount" => $request_data["pro_comp_amount"][$key],
                    ];
                }

            }

            if (is_array($input_product_components) && count($input_product_components))
            {
                product_components_m::where("pro_id",$pro_id)->forceDelete();

                product_components_m::insert($input_product_components);

                #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " حفظ مكونات المنتج "
                    );
                #endregion
            }
            */



            return Redirect::to('admin/product/save_products_on_stock/'.$cat_id.'/'.$pro_id)->with([
                "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح  </div>"
            ])->send();

        }

        return view("admin.subviews.product.products_on_stock.save",$this->data);
    }

    public function remove_product_from_stock(Request $request){

        if(!check_permission($this->user_permissions,"product/products_on_stock","delete_action",$this->data["current_user"])){
            $output["msg"]="<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>";
            echo json_encode($output);
            return;
        }
        $pro_id = (int)$request->get("item_id");

        //check if a branch has this product or not
        $branch_has_products=branches_products_m::where("pro_id",$pro_id)->where("b_p_quantity",">","0")->get()->all();


        if(count($branch_has_products)){

            $output["msg"]="<div class='alert alert-danger'> لا يمكنك ان تحذف هذا المنتج وذلك لوجوده في احدي الفروع</div>";
            echo json_encode($output);
            return;
        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = " تم حذف منتج من المخزن "
        );
        #endregion


        $this->general_remove_item($request,'App\models\product\products_m');
    }

    public function products_on_branches_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/products_on_stock","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->get_branches_and_other_data();

        $branch_id=$request->get("branch_id","0");
        $this->data["branch_id"]=$branch_id;

        $this->data["products_pagination"] = "";
        $this->data["all_products"] = [];
        if ($branch_id > 0)
        {
            $get_products = branches_products_m::where("branch_id",$branch_id)->paginate(1);

            $search_product=$request->get("search_product","");
            $this->data["search_product"]=$search_product;

            if($search_product!=""){
                $get_products =branches_products_m::
                join("products","products.pro_id","=","branches_products.pro_id")->
                where("branch_id",$branch_id)->
                where(function ($query)use($search_product) {
                    $query->where("pro_name","like","%$search_product%")->
                    orWhere("pro_name_en","like","%$search_product%")->
                    orWhere("pro_barcode","like","%$search_product%");
                })->
                paginate(50);

            }
            else{
                $get_products = branches_products_m::where("branch_id",$branch_id)->paginate(50);
            }

            $get_products->appends(Input::except("page"));



            $this->data["products_pagination"] = $get_products;
            $get_products_ids = convert_inside_obj_to_arr($get_products,'pro_id');

            if(isset_and_array($get_products_ids)){
                $get_products_ids = implode(',',$get_products_ids);
                $this->data["all_products"] = branches_products_m::get_branches_pros(" 
                    AND branch_pro.pro_id in ($get_products_ids)
                    AND branch_pro.branch_id=$branch_id
                 ");
            }

        }



//        $this->data["all_products"] = branches_products_m::get_branches_pros(" AND branch_pro.branch_id=$branch_id");

        return view("admin.subviews.product.products_on_stock.products_on_branches_stock",$this->data);
    }



    #endregion


    public function set_products_prices(Request $request){

        if (!check_permission($this->user_permissions,"product/branch_product_prices","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $all_price_list_items=price_list_m::all()->all();


        if(!isset_and_array($all_price_list_items)){
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> يجب ان تضيف قائمة الاسعار قبل الدخول الي هذه الصفحة</div>"
            ])->send();
        }

        if($request->method()=="POST"){

            //get all pro ids array
            $pro_ids=$request->get("pro_ids");

            if(!isset_and_array($pro_ids)){
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> خطأ</div>"
                ])->send();
            }


            //get all prices items array
            $price_list=[];
            foreach($all_price_list_items as  $item_key=>$item){
                $price_list["price_val_".$item->price_id]=$request->get("price_val_".$item->price_id);
            }



            //save them at product_price table
            //make sync
            $price_list_data=[];

            product_prices_values_m::whereIn("pro_id",$pro_ids)->delete();

            foreach($pro_ids as $pro_key=>$pro_id){

                foreach($all_price_list_items as $item){
                    $price_list_data[]=[
                        "pro_id"=>$pro_id,
                        "price_id"=>$item->price_id,
                        "ppv_value"=>$price_list["price_val_".$item->price_id][$pro_key]
                    ];
                }


                if($pro_key%100==0){
                    product_prices_values_m::insert($price_list_data);

                    $price_list_data=[];
                }

            }


            if(count($price_list_data)){
                product_prices_values_m::insert($price_list_data);
            }


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم تحديد قائمة سعر للمنتجات "
            );
            #endregion

//            return Redirect::to('admin/product/set_products_prices')->with([
//                "msg" => "<div class='alert alert-success'> تمت العملية بنجاح</div>"
//            ])->send();
        }


        return view("admin.subviews.product.product_prices.set_product_prices",$this->data);
    }

    public function get_products_prices(Request $request){
        $output=[];
        $cat_ids=$request->get("cat_ids");
        $cat_ids=json_decode($cat_ids);
        $cat_ids=array_map("intval",$cat_ids);

        if(!isset_and_array($cat_ids)){
            $output["products_table"]="<div class='alert alert-danger'>يجب ان تختار تصنيفات اولا</div>";
            echo json_encode($output);
            return;
        }

        //get all sub cats in these cats_ids
        $all_cats_ids=category_m::get_all_child_cats($cat_ids);


        //get unique cats ids
        $all_cats_ids=array_unique($all_cats_ids);

        //get all products related with these cats ids
        $all_products=products_m::whereIn("cat_id",$all_cats_ids)->get()->all();


        if(!isset_and_array($all_products)){
            $output["products_table"]="<div class='alert alert-danger'>لا يوجد اي منتجات في التصنيفات المختارة</div>";
            echo json_encode($output);
            return;
        }

        //generate table with all price list with these products
        $all_price_list_items=price_list_m::all();

        //get old values of product prices
        $old_prices_values=product_prices_values_m::
        whereIn("pro_id",convert_inside_obj_to_arr($all_products,"pro_id"))
            ->get()->groupBy("pro_id")->all();

        $output["products_table"]=
            \View::make("admin.subviews.product.product_prices.product_prices_table",[
                "all_products"=>$all_products,
                "all_price_list_items"=>$all_price_list_items,
                "old_prices_values"=>$old_prices_values,
            ])->render();

        echo json_encode($output);
    }


    //unused function
    public function save_branch_product_prices(Request $request , $branch_id  , $pro_id )
    {

        if (!check_permission($this->user_permissions,"product/branch_product_prices","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $branch_data = User::get_users(" AND u.user_id = $branch_id AND u.user_type = 'branch' AND u.user_active = 1 AND u.user_can_login = 1 ");
        if (is_array($branch_data) && count($branch_data) && isset($branch_data[0]))
        {
            $branch_data = $branch_data[0];
            $this->data["branch_data"] = $branch_data;
        }
        else{
            return Redirect::to('admin/product/products_on_stock')->with([
                "msg" => "<div class='alert alert-danger'> الفرع غير موجود  </div>"
            ])->send();
        }

        $product_data = products_m::get_pros(" AND pro.pro_id = $pro_id ");
        if (is_array($product_data) && count($product_data) && isset($product_data[0]))
        {
            $product_data = $product_data[0];
            $this->data["product_data"] = $product_data;
        }
        else{
            return Redirect::to('admin/product/products_on_stock')->with([
                "msg" => "<div class='alert alert-danger'> المنتج غير موجود  </div>"
            ])->send();
        }

        $this->data["original_price_data"] = "";
        $this->data["product_promotions"] = "";

        $get_original_price = product_prices_m::where("pro_id",$pro_id)->where("branch_id",$branch_id)->where("pro_price_original",1)->get()->first();
        if(is_object($get_original_price))
        {
            $this->data["original_price_data"] = $get_original_price;
        }

        $get_promotions = product_prices_m::where("pro_id",$pro_id)->where("branch_id",$branch_id)->where("pro_price_original",0)->get();
        $get_promotions = $get_promotions->all();
        $get_promotions_ids = [];
        if(is_array($get_promotions) && count($get_promotions))
        {
            $this->data["product_promotions"] = $get_promotions;
            $get_promotions_ids = convert_inside_obj_to_arr($get_promotions, "pro_price_id");
        }
//        dump($get_promotions_ids);
        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "original_pro_price_label" => "required",
//                "original_pro_price_quantity" => "required|min:1|numeric",
                "original_pro_price_price" => "required|min:0|numeric",
//                "pro_price_quantity.*" => "min:1|numeric",
                "pro_price_price.*" => "min:0|numeric",
            ]);

            $request_data = $this->cleaning_input($request->all());
            $request_data["original_pro_price_quantity"] = intval($request_data["original_pro_price_quantity"]);
//            dump($request_data);
//            die();

            #region orginal_price

                $original_price_arr = [];
                $original_price_arr["pro_id"] = $pro_id;
                $original_price_arr["branch_id"] = $branch_id;
                $original_price_arr["pro_price_label"] = $request_data["original_pro_price_label"];
//                $original_price_arr["pro_price_quantity"] = $request_data["original_pro_price_quantity"];
                $original_price_arr["pro_price_quantity"] = 1;
                $original_price_arr["pro_price_price"] = $request_data["original_pro_price_price"];
                $original_price_arr["pro_price_original"] = 1;

                if (isset($request_data["original_pro_price_id"]) && !empty($request_data["original_pro_price_id"])
                    && $request_data["original_pro_price_id"] > 0)
                {
                    // edit original price
                    $origin_old_obj = product_prices_m::find($request_data["original_pro_price_id"]);
                    if (is_object($origin_old_obj) && !empty($origin_old_obj))
                    {
                        product_prices_m::find($request_data["original_pro_price_id"])->update($original_price_arr);
                        $desc_msg = " تعديل السعر الاصلي للمنتج '".$product_data->pro_name."' للفرع '".$branch_data->full_name."' ";
                    }
                    else{
                        return Redirect::to('admin/product/save_branch_product_prices/'.$branch_id.'/'.$pro_id)->with([
                            "msg" => "<div class='alert alert-danger'> السعر الاصلي غير موجود  </div>"
                        ])->send();
                    }

                }
                else{
                    // add new original price
                    $new_origin_price = product_prices_m::create($original_price_arr);

                    $desc_msg = " إضافة سعر اساسي للمنتج '".$product_data->pro_name."' للفرع '".$branch_data->full_name."' ";
                }

                #region save in notification
                    $this->send_all_user_type_notifications(
                        $not_title = $desc_msg ,
                        $not_type = "info" ,
                        $user_type = "admin"
                    );
                #endregion


                #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = $desc_msg
                    );
                #endregion

            #endregion


            #region promotions_prices

            if (isset($request_data["pro_price_label"]) && count($request_data["pro_price_label"]))
            {
                foreach ($request_data["pro_price_label"] as $key => $label)
                {
                    $promotions_prices = [];

                    if (isset($request_data["pro_price_quantity"][$key]) && !empty($request_data["pro_price_quantity"][$key]) && $request_data["pro_price_quantity"][$key] > 0 &&
                        isset($request_data["pro_price_price"][$key]) && !empty($request_data["pro_price_price"][$key]) && $request_data["pro_price_price"][$key] > 0)
                    {
                        $promotions_prices["pro_id"] = $pro_id;
                        $promotions_prices["branch_id"] = $branch_id;
                        $promotions_prices["pro_price_original"] = 0;
                        $promotions_prices["pro_price_label"] = $label;
//                        $promotions_prices["pro_price_quantity"] = intval($request_data["pro_price_quantity"][$key]);
                        $promotions_prices["pro_price_quantity"] = 1;
                        $promotions_prices["pro_price_price"] = $request_data["pro_price_price"][$key];
                        $promotions_prices["pro_price_from"] = $request_data["pro_price_from"][$key];
                        $promotions_prices["pro_price_to"] = $request_data["pro_price_to"][$key];

                        if (isset($request_data["pro_price_id"][$key]) && !empty($request_data["pro_price_id"][$key])
                        && $request_data["pro_price_id"][$key] > 0)
                        {

                            // edit promotion
                            $old_promotion = product_prices_m::find($request_data["pro_price_id"][$key]);
                            if (is_object($old_promotion))
                            {

                                if(($edit_id_key = array_search($request_data["pro_price_id"][$key], $get_promotions_ids)) !== false) {
                                    unset($get_promotions_ids[$edit_id_key]);
                                }

                                product_prices_m::find($request_data["pro_price_id"][$key])->update($promotions_prices);
                                $desc_msg = " تعديل سعر خاص للمنتج '".$product_data->pro_name."' للفرع '".$branch_data->full_name."' ";
                            }
                        }
                        else{

                            // add new promotion
                            product_prices_m::create($promotions_prices);
                            $desc_msg = " إضافة سعر خاص جديد للمنتج '".$product_data->pro_name."' للفرع '".$branch_data->full_name."' ";

                        }

                        #region save in notification
                            $this->send_all_user_type_notifications(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_type = "admin"
                            );
                        #endregion


                        #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                        #endregion

                    }
                }

            }

            #endregion

            #region delete_other_promotions

                if(count($get_promotions_ids))
                {
                    product_prices_m::whereIn("pro_price_id",$get_promotions_ids)->delete();
                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = " مسح الاسعار الخاصه الاخري "
                        );
                    #endregion
                }

            #endregion


            return Redirect::to('admin/product/save_branch_product_prices/'.$branch_id.'/'.$pro_id)->with([
                "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح  </div>"
            ])->send();

        }


        return view("admin.subviews.product.products_on_stock.save_product_prices",$this->data);
    }

    public function search_for_product(Request $request){

        $search_keyword=clean($request->get("q"));

        $products = products_m::get_pros(" 
            AND pro.pro_quantity>0
            AND (pro.pro_name like '%$search_keyword%' or pro.pro_barcode like '%$search_keyword%') 
        ");


        $output=[];

        $output["incomplete_results"]=false;
        $output["items"]=$products;
        $output["total_count"]=count($products);

        return $output;

    }

    public function search_for_product_without_quantity(Request $request){

        $search_keyword=clean($request->get("q"));

        $products = products_m::get_pros(" 
            AND (pro.pro_name like '%$search_keyword%' or pro.pro_barcode like '%$search_keyword%') 
        ");


        $output=[];

        $output["incomplete_results"]=false;
        $output["items"]=$products;
        $output["total_count"]=count($products);

        return $output;

    }

    #region produce_product

    public function save_produce_product(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/produce","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_products"] = products_m::all()->all();


        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "pro_id" => "required|numeric",
                "pro_quantity" => "required|min:1|numeric",
            ]);

            $request_data = $this->cleaning_input($request->all());
            $request_data["pro_quantity"] = intval($request_data["pro_quantity"]);

            // check if product is exist
            $product_data = products_m::get_pros(" AND pro.pro_id = ".$request_data["pro_id"]);
            if (count($product_data) && isset($product_data[0]) && is_object($product_data[0]))
            {

                $product_data = $product_data[0];

                $get_product_components = product_components_m::get_data_with_product_materials(" AND pro.pro_id = $product_data->pro_id ");

                if (is_array($get_product_components) && count($get_product_components))
                {
                    $mat_unit_amount_arr = [];
                    $mat_total_amount_arr = [];
                    $all_allowed_product_produced = [];

                    foreach($get_product_components as $key => $value)
                    {
                        if ($value->pro_comp_amount > 0)
                        {
                            $mat_unit_amount_arr[$key] = $value->pro_comp_amount;
                            $mat_total_amount_arr[$key] = $value->mat_amount;

                            $all_allowed_product_produced[$key] = intval($mat_total_amount_arr[$key]/$mat_unit_amount_arr[$key]);
                        }

                    }

                    $not_allowed = true;

                    if (count($all_allowed_product_produced))
                    {

                        $pro_quantity = $request_data["pro_quantity"];
                        $pro_id = $request_data["pro_id"];

                        if ($pro_quantity <= min($all_allowed_product_produced))
                        {
                            $not_allowed = false;

                            // save into produce_product_review
                            produce_product_review_m::create([
                                "pro_id" => $pro_id,
                                "produce_quantity" => $pro_quantity,
                                "is_produced" => 0,
                            ]);

                            $desc_msg = "لقد تم طلب عمل إنتاجيه جديده من المنتج رقم.$pro_id بكمية $pro_quantity برجاء مراجعتها ";
                            #region save in notification
                                $this->send_all_user_type_notifications(
                                    $not_title = $desc_msg ,
                                    $not_type = "info" ,
                                    $user_type = "admin"
                                );
                            #endregion

                            #region send email
                                $this->_send_email_to_all_users_type(
                                    $user_type = "admin" ,
                                    $data = $desc_msg,
                                    $subject = "إنتاج المنتجات"
                                );
                            #endregion

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = " إنتاج المنتجات "
                            );
                            #endregion

                            return Redirect::to('admin/product/save_produce_product')->with([
                                "msg" => "<div class='alert alert-success'> لقد تم إرسال الطلب بنجاح وفي إنتظار المراجعه </div>"
                            ])->send();

                        }

                    }

                    if ($not_allowed)
                    {
                        return Redirect::to('admin/product/save_produce_product')->with([
                            "msg" => "<div class='alert alert-danger'> كمية الماده الخام غير كافية لانتاج منتج , يمكنك فقط انتاج ".min($all_allowed_product_produced)." منتج !! </div>"
                        ])->send();
                    }

//                    dump($mat_unit_amount_arr);
//                    dump($mat_total_amount_arr);
//                    dump($all_allowed_product_produced);
//                    dump(min($all_allowed_product_produced));

                }
                else{
                    return Redirect::to('admin/product/save_produce_product')->with([
                        "msg" => "<div class='alert alert-danger'> لا توجد بيانات للمنتج او المواد الخام </div>"
                    ])->send();
                }

            }
            else{
                return Redirect::to('admin/product/save_produce_product')->with([
                    "msg" => "<div class='alert alert-danger'> المنتج غير موجود </div>"
                ])->send();
            }

        }

        return view("admin.subviews.product.produce_product",$this->data);
    }

    public function save_reverse_produce_product(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/produce","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/reverse_produce')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_products"] = products_m::all()->all();


        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "pro_id" => "required|numeric",
                "pro_quantity" => "required|min:1|numeric",
            ]);

            $request_data = $this->cleaning_input($request->all());
            $request_data["pro_quantity"] = intval($request_data["pro_quantity"]);

            // check if product is exist
            $product_data = products_m::get_pros(" AND pro.pro_id = ".$request_data["pro_id"]);
            if (count($product_data) && isset($product_data[0]) && is_object($product_data[0]))
            {

                $product_data = $product_data[0];

                $get_product_components = product_components_m::get_data_with_product_materials(" AND pro.pro_id = $product_data->pro_id ");

                if (is_array($get_product_components) && count($get_product_components))
                {
                    $mat_unit_amount_arr = [];
                    $mat_total_amount_arr = [];
                    $all_allowed_product_produced = [];

                    foreach($get_product_components as $key => $value)
                    {
                        if ($value->pro_comp_amount > 0)
                        {
                            $mat_unit_amount_arr[$key] = $value->pro_comp_amount;
                            $mat_total_amount_arr[$key] = $value->mat_amount;

                            $all_allowed_product_produced[$key] = intval($mat_total_amount_arr[$key]/$mat_unit_amount_arr[$key]);
                        }

                    }


                    if (count($all_allowed_product_produced))
                    {

                        $pro_quantity = $request_data["pro_quantity"];
                        $pro_id = $request_data["pro_id"];

                        if ($pro_quantity <= $product_data->pro_quantity)
                        {

                            foreach ($get_product_components as $key => $value)
                            {

                                // update product_material quantity
                                $get_product_material_obj = product_materials_m::where("mat_id", $value->mat_id)->get()->first();
                                $new_mat_quantity = ($get_product_material_obj->mat_amount + ($pro_quantity * $value->pro_comp_amount));
                                product_materials_m::where("mat_id", $value->mat_id)->get()->first()->update([
                                    "mat_amount" => $new_mat_quantity
                                ]);


                                #region save in site_tracking
                                $this->track_my_action(
                                    $action_desc = " تعديل كمية الماده الخام '" . $value->mat_name . "' من $get_product_material_obj->mat_amount الي $new_mat_quantity "
                                );
                                #endregion

                                // check if exceed limit
                                if ($get_product_material_obj->mat_pro_limit >= $new_mat_quantity)
                                {

                                    $desc_msg = "كمية الماده الخام ($value->mat_name) تجاوزت حد الطلب";
                                    #region save in notification
                                    $this->send_all_user_type_notifications(
                                        $not_title = $desc_msg,
                                        $not_type = "info",
                                        $user_type = "admin"
                                    );
                                    #endregion


                                    #region save in site_tracking
                                    $this->track_my_action(
                                        $action_desc = $desc_msg
                                    );
                                    #endregion

                                    #region send email
                                    $this->_send_email_to_all_users_type(
                                        $user_type = "admin",
                                        $data = $desc_msg,
                                        $subject = "تجاوز حد الطلب لكمية الماده الخام"
                                    );
                                    #endregion

                                }

                            }

                            // update product quantity
                            $new_pro_quantity = ($product_data->pro_quantity - $pro_quantity);
                            products_m::where("pro_id", $pro_id)->get()->first()->update([
                                "pro_quantity" => $new_pro_quantity
                            ]);

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = " تعديل كمية المنتج '" . $product_data->pro_name . "' من $product_data->pro_quantity الي $new_pro_quantity "
                            );
                            #endregion


                            $this->send_all_user_type_notifications(
                                $not_title = " تعديل كمية المنتج '" . $product_data->pro_name . "' من $product_data->pro_quantity الي $new_pro_quantity ",
                                $not_type = "info",
                                $user_type = "admin"
                            );
                            #endregion


                            return Redirect::to('admin/product/save_reverse_produce_product')->with([
                                "msg" => "<div class='alert alert-success'> لقد تم سحب المنتجات بنجاح </div>"
                            ])->send();


                        }
                        else
                        {
                            return Redirect::to('admin/product/save_reverse_produce_product')->with([
                                "msg" => "<div class='alert alert-danger'> كمية المنتج غير كافية للسحب , يمكنك فقط سحب ".$product_data->pro_quantity." منتج !! </div>"
                            ])->send();
                        }

                    }


                }
                else{
                    return Redirect::to('admin/product/save_reverse_produce_product')->with([
                        "msg" => "<div class='alert alert-danger'> لا توجد بيانات للمنتج او المواد الخام </div>"
                    ])->send();
                }

            }
            else{
                return Redirect::to('admin/product/save_reverse_produce_product')->with([
                    "msg" => "<div class='alert alert-danger'> المنتج غير موجود </div>"
                ])->send();
            }

        }

        return view("admin.subviews.product.save_reverse_produce_product",$this->data);
    }

    public function review_produce_products()
    {

        if (!check_permission($this->user_permissions,"product/review_produce_products","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $get_all_products_to_produce = produce_product_review_m::get_pros();
        $this->data["products"] = $get_all_products_to_produce;
//        dump($get_all_products_to_produce);


        return view("admin.subviews.product.review_produce_products",$this->data);
    }

    public function produce_product_details($ppr_id)
    {

        if (!check_permission($this->user_permissions,"product/review_produce_products","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $produce_request = produce_product_review_m::get_pros(" AND ppr.ppr_id = $ppr_id ");
        if (is_array($produce_request) && count($produce_request) && isset($produce_request[0]))
        {
            $produce_request = $produce_request[0];
            $pro_id = $produce_request->pro_id;
            $product_data = products_m::get_pros(" AND pro.pro_id = $pro_id ");

            if(is_array($product_data) && count($product_data) && isset($product_data[0]))
            {
                $product_data = $product_data[0];

                $get_product_components = product_components_m::get_data_with_product_materials(" AND pro.pro_id = $pro_id ");
                $this->data["get_product_components"] = $get_product_components;
                $this->data["produce_quantity"] = $produce_request->produce_quantity;
//                dump($get_product_components);
            }
            else{
                return Redirect::to('admin/product/review_produce_products')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات المنتج لهذا الطلب غير موجودة !!  </div>"
                ])->send();
            }


        }
        else{
            return Redirect::to('admin/product/review_produce_products')->with([
                "msg" => "<div class='alert alert-danger'> بيانات الطلب غير موجودة !!  </div>"
            ])->send();
        }

        return view("admin.subviews.product.produce_product_details",$this->data);
    }

    public function change_produce_product_status(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"product/review_produce_products","add_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");



        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            if (is_object($obj))
            {
                $pro_id = $obj->pro_id;
                $pro_quantity = $obj->produce_quantity;

                if($accept == 0)
                {
                    $output["msg"]=generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
                    echo json_encode($output);
                    return;
                }

                if ($obj->is_produced == 1)
                {
                    $output["msg"]="<div class='alert alert-danger'> لقد تم الموافقه مسبقا علي هذا المنتج وتم انتاجه !!</div>";
                    echo json_encode($output);
                    return;
                }

                $pro_obj = products_m::find($pro_id);
//                if($obj->produce_quantity > $pro_obj->pro_quantity)
//                {
//                    $output["msg"]="<div class='alert alert-danger'> الكمية غير متاحة لعمل الانتاج</div>";
//                    echo json_encode($output);
//                    return;
//                }


                // make the produce

                $get_product_components = product_components_m::get_data_with_product_materials(" AND pro.pro_id = $pro_id ");

                if (is_array($get_product_components) && count($get_product_components))
                {
                    $mat_unit_amount_arr = [];
                    $mat_total_amount_arr = [];
                    $all_allowed_product_produced = [];

                    foreach ($get_product_components as $key => $value)
                    {
                        if ($value->pro_comp_amount > 0) {
                            $mat_unit_amount_arr[$key] = $value->pro_comp_amount;
                            $mat_total_amount_arr[$key] = $value->mat_amount;

                            $all_allowed_product_produced[$key] = intval($mat_total_amount_arr[$key] / $mat_unit_amount_arr[$key]);
                        }

                    }

                    $make_produce = false;

                    if (count($all_allowed_product_produced))
                    {

                        if ($pro_quantity <= min($all_allowed_product_produced))
                        {

                            $make_produce = true;

                            foreach ($get_product_components as $key => $value)
                            {

                                // update product_material quantity
                                $get_product_material_obj = product_materials_m::where("mat_id", $value->mat_id)->get()->first();
                                $new_mat_quantity = ($get_product_material_obj->mat_amount - ($pro_quantity * $value->pro_comp_amount));
                                product_materials_m::where("mat_id", $value->mat_id)->get()->first()->update([
                                    "mat_amount" => $new_mat_quantity
                                ]);


                                #region save in site_tracking
                                $this->track_my_action(
                                    $action_desc = " تعديل كمية الماده الخام '" . $value->mat_name . "' من $get_product_material_obj->mat_amount الي $new_mat_quantity "
                                );
                                #endregion

                                // check if exceed limit
                                if ($get_product_material_obj->mat_pro_limit >= $new_mat_quantity)
                                {

                                    $desc_msg = "كمية الماده الخام ($value->mat_name) تجاوزت حد الطلب";
                                    #region save in notification
                                    $this->send_all_user_type_notifications(
                                        $not_title = $desc_msg,
                                        $not_type = "info",
                                        $user_type = "admin"
                                    );
                                    #endregion


                                    #region save in site_tracking
                                    $this->track_my_action(
                                        $action_desc = $desc_msg
                                    );
                                    #endregion

                                    #region send email
                                    $this->_send_email_to_all_users_type(
                                        $user_type = "admin",
                                        $data = $desc_msg,
                                        $subject = "تجاوز حد الطلب لكمية الماده الخام"
                                    );
                                    #endregion

                                }

                            }

                            // update product quantity
                            $new_pro_quantity = ($pro_obj->pro_quantity + $pro_quantity);
                            products_m::where("pro_id", $pro_id)->get()->first()->update([
                                "pro_quantity" => $new_pro_quantity
                            ]);

                            #region save in site_tracking
                                $this->track_my_action(
                                    $action_desc = " تعديل كمية المنتج '" . $pro_obj->pro_name . "' من $pro_obj->pro_quantity الي $new_pro_quantity "
                                );
                            #endregion

                            // update produce_product_review
                            $obj->update([
                                "is_produced" => "1"
                            ]);

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = "تمت الموافقه علي الطلب رقم.$obj->ppr_id لانتاج المنتج"
                            );
                            #endregion

                            $this->send_all_user_type_notifications(
                                $not_title = "تمت الموافقه علي الطلب رقم.$obj->ppr_id لانتاج المنتج",
                                $not_type = "info",
                                $user_type = "admin"
                            );
                            #endregion

                        }

                    }

                }


                if (!$make_produce)
                {
                    $output["msg"]="<div class='alert alert-danger'> لا يمكن إنتاج هذا المنتج لنقص كميات المواد الخام</div>";
                    echo json_encode($output);
                    return;
                }
                else{
                    $output["msg"]="<div class='alert alert-success'> لقد تم إنتاج هذا المنتج بنجاح</div>";
                    echo json_encode($output);
                    return;
                }


            }
            else{
                $output["msg"]="<div class='alert alert-danger'> هذا الطلب غير موجود !!</div>";
                echo json_encode($output);
                return;
            }

        }


        echo json_encode($output);
    }

    public function remove_product_produce_product_request(Request $request)
    {
        $output = array();
        $item_id = (int)$request->get("item_id");
        $model_name = $request->get("table_name"); // App\User

        if (!check_permission($this->user_permissions,"product/review_produce_products","delete_action",$this->data["current_user"]))
        {
            $output["deleted"] = "ليس لديك الصلاحية لفعل هذا الامر";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";

                $desc_msg = "لقد تم مسح طلب انتاج المنتج رقم #$item_id";

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

            }

        }

        echo json_encode($output);
    }

    #endregion


    #region broken_products_on_stock

    public function broken_products_on_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/broken_products_on_stock","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->get_branches_and_other_data();

        $this->data["all_post_data"]=(object)$request->all();


        $this->data["broken_products"]=[];

        $selected_branch_id=$request->get("branch_id","");
        $start_date=$request->get("start_date","");
        $end_date=$request->get("end_date","");

        if($selected_branch_id!=""){
            $addtional_where="AND bro_pro.user_id =$selected_branch_id";

            if($start_date!=""){
                $start_date=date("Y-m-d",strtotime($start_date));
                $addtional_where.=" AND bro_pro.b_p_date>=$start_date";
            }

            if($end_date!=""){
                $end_date=date("Y-m-d",strtotime($end_date));
                $addtional_where.=" AND bro_pro.b_p_date<=$start_date";
            }

            $this->data["broken_products"] = broken_products_m::get_data($addtional_where);
        }

        return view("admin.subviews.product.broken_products",$this->data);
    }

    public function save_broken_products_on_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"product/broken_products_on_stock","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $branch_orders_obj=new branch_orders();
        $new_this_data=$branch_orders_obj->get_cat_child_cats_and_products($request,$cat_id="0",true);
        $this->data=array_merge($new_this_data,$this->data);

        $this->get_branches_and_other_data();

        if($request->method() == "POST")
        {

            $this->validate($request,[
                "b_p_date" => "required",
            ]);

            $request_data = $this->cleaning_input($request->all());

            $pro_id_arr = $request_data["pro_id"];
            $b_p_quantity_arr = array_map("intval",$request_data["pro_quantity"]);
            $b_p_date = $request_data["b_p_date"];

            $selected_branch_id=$request->get("branch_id");

            $desc_msg="";
            $error_msgs="";

            $broken_rows=[];
            foreach($pro_id_arr as $pro_key=>$pro_id){

                $b_p_quantity=$b_p_quantity_arr[$pro_key];

                //check if user select branch or factory
                if($selected_branch_id=="0"){
                    // check if product is exist
                    $get_product_obj = products_m::get_pros(" AND pro.pro_id =$pro_id ");
                    $pro_quantity_col_name="pro_quantity";
                }
                else{
                    $get_product_obj = branches_products_m::get_branches_pros(" 
                    AND branch_pro.branch_id=$selected_branch_id 
                    AND pro.pro_id =$pro_id "
                    );

                    $pro_quantity_col_name="b_p_quantity";
                }



                if (is_array($get_product_obj) && count($get_product_obj) && isset($get_product_obj[0]))
                {
                    $get_product_obj = $get_product_obj[0];

                    if(!($b_p_quantity>0)){
                        $error_msgs.="<div class='alert alert-danger'>لم يتم ادخال اهلاك ".$get_product_obj->pro_name." لان كيمة الاهلاك كانت صفر"."</div>";
                        continue;
                    }


                    if ($b_p_quantity <= $get_product_obj->{$pro_quantity_col_name})
                    {

                        $new_quantity = ($get_product_obj->{$pro_quantity_col_name} - $b_p_quantity);

                        if($selected_branch_id=="0") {
                            $new_product_obj =
                                products_m::
                                where("pro_id",$get_product_obj->pro_id)->
                                get()->
                                first()->
                                update([
                                    "pro_quantity" => $new_quantity
                                ]);
                        }
                        else{
                            $new_product_obj =
                                branches_products_m::
                                where("branch_id",$selected_branch_id)->
                                where("pro_id",$get_product_obj->pro_id)->
                                get()->
                                first()->
                                update([
                                    "b_p_quantity" => $new_quantity
                                ]);
                        }



                        //$desc_msg .= "تعديل كمية المنتج ($get_product_obj->pro_name) من ".$get_product_obj->{$pro_quantity_col_name}." الي $new_quantity";

                        #region commented
                        // check if exceed limit
                        if (false&&$new_quantity <= $get_product_obj->pro_limit)
                        {

                            $desc_msg = "كمية المنتج ($get_product_obj->pro_name) تجوزت حد الطلب >> $get_product_obj->pro_limit";
                            #region save in notification
                            $this->send_all_user_type_notifications(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_type = "admin"
                            );
                            #endregion


                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                            #endregion

                            #region send email
                            $this->_send_email_to_all_users_type(
                                $user_type = "admin" ,
                                $data = $desc_msg,
                                $subject = "تجاوز حد الطلب للمنتج"
                            );
                            #endregion

                        }
                        #endregion

                        // add in broken products
                        $broken_rows[]=[
                            'user_id'=>$selected_branch_id,
                            'pro_id'=>$pro_id ,
                            'b_p_quantity'=>$b_p_quantity ,
                            'b_p_date'=>$b_p_date,
                            'created_at'=>Carbon::now(),
                            'updated_at'=>Carbon::now()
                        ];

                        $desc_msg .= " إضافة هالك للمنتج $get_product_obj->pro_name بكمية ".$b_p_quantity;

                    }
                    else{
                        $error_msgs.="<div class='alert alert-danger'>لم يتم ادخال اهلاك ".$get_product_obj->pro_name." لان كميته الحالية لا تسمح "."</div>";
                    }
                }
            }//end product foreach

            broken_products_m::insert($broken_rows);


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

            #region send email
            $this->_send_email_to_all_users_type(
                $user_type = "admin" ,
                $data = $desc_msg,
                $subject = "إضافة هالك للمنتج"
            );
            #endregion


            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            return Redirect::to('admin/product/broken_products_on_stock')->with([
                "msg" => "<div class='alert alert-success'>لقد تم الحفظ بنجاح </div>".$error_msgs
            ])->send();


        }


        return view("admin.subviews.product.save_broken_products",$this->data);
    }

    #endregion

}


