<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\branch_transfers\branch_transfer_products_m;
use App\models\branch_transfers\branch_transfers_m;
use App\models\product\branches_products_m;
use App\models\product\products_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class branch_transfer_products extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if (!check_permission($this->user_permissions,"branch_transfer_products","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }


        $this->data["all_transfers"] = [];
        $cond = "";

        if (is_array($_GET) && count($_GET))
        {

            if (isset($_GET['from_branch']) && !empty($_GET['from_branch']))
            {
                $from_branch = $_GET['from_branch'];
                $cond .= " AND transfer.from_branch = $from_branch ";
            }

            if (isset($_GET['to_branch']) && !empty($_GET['to_branch']))
            {
                $to_branch = $_GET['to_branch'];
                $cond .= " AND transfer.to_branch = $to_branch ";
            }

            if (isset($_GET['from_date']) && !empty($_GET['to_date'])
                && isset($_GET['to_date']) && !empty($_GET['to_date']))
            {
                $from_date = $_GET['from_date'];
                $to_date = $_GET['to_date'];
                $cond .= " AND date(transfer.created_at) >= '$from_date' ";
                $cond .= " AND date(transfer.created_at) <= '$to_date' ";
            }

        }


        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_transfers = branch_transfers_m::get_data(" $cond ");
            $this->data["branches"] = User::where("user_type","branch")->get()->all();
            $this->data["to_branches"] = $this->data["branches"];
        }
        else{
            $all_transfers = branch_transfers_m::get_data(" 
                AND transfer.to_branch = $this->branch_id
                $cond
            ");
            $this->data["branches"] = User::where("user_id","$this->branch_id")->get()->all();
            $this->data["to_branches"] = User::where("user_type","branch")->get()->all();
        }
        $this->data["all_transfers"] = $all_transfers;


        return view("admin.subviews.branch_transfer_products.show")->with($this->data);

    }


    public function show_products($b_t_id)
    {

        if (!check_permission($this->user_permissions,"branch_transfer_products","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_transfers = branch_transfers_m::get_data(" AND transfer.b_t_id = $b_t_id ");
        }
        else{
            $all_transfers = branch_transfers_m::get_data(" 
                AND transfer.to_branch = $this->branch_id
                AND transfer.b_t_id = $b_t_id
            ");
        }

        if (!count($all_transfers))
        {
            return Redirect::to('admin/branch_transfers')->with(
                [
                    "msg" => "<div class='alert alert-danger'>لا يوجد تحويلات !!</div>"
                ]
            )->send();
        }

        $this->data["transfer_data"] = $all_transfers[0];
        $get_products = branch_transfer_products_m::get_data(" 
            AND transfer_pros.b_t_id = $b_t_id
        ");
        $this->data["products"] = $get_products;


        return view("admin.subviews.branch_transfer_products.show_products")->with($this->data);

    }


    public function save(Request $request)
    {

        if (!check_permission($this->user_permissions,"branch_transfer_products","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $branch_orders_obj=new branch_orders();
        $new_this_data=$branch_orders_obj->get_cat_child_cats_and_products($request,$cat_id="0",true);
        $this->data=array_merge($new_this_data,$this->data);

        $this->data["from_branches"] = [];
        $this->data["to_branches"] = [];

        $get_branches = User::where("user_type","branch")->get()->all();
        if (!count($get_branches))
        {
            return Redirect::to('admin/dashboard')->with(
                [
                    "msg"=>"<div class='alert alert-danger'>لا يوجد فروع</div>"
                ]
            )->send();
        }
        $this->data["to_branches"] = $get_branches;


        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["from_branches"] = $get_branches;
        }
        else{

            $get_my_branche = User::where("user_id",$this->branch_id)->get()->all();
            $this->data["from_branches"] = $get_my_branche;

        }

        //$this->show_category_tree(true,"product");


        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "from_branch"=>"required",
                    "to_branch"=>"required",
                    "pro_id.0"=>"required",
                    "pro_quantity.0"=>"required",
                ],
                [
                    "from_branch.required" => "برجاء إختيار الفرع المحول منه",
                    "to_branch.required" => "برجاء إختيار الفرع المحول اليه",
                    "pro_id.0.required" => "برجاء إختيار منتج واحد علي الافل",
                    "pro_quantity.0.required" => "برجاء وضع كمية للمنتج",
                ]
            );

            $request = clean($request->all());

            if (count($request['pro_id']) != count($request['pro_quantity']))
            {
                return Redirect::to('admin/branch_transfers/save')->with(
                    [
                        "msg"=>"<div class='alert alert-danger'>بيانات غير صالحه للإدخال</div>"
                    ]
                )->send();
            }

            if ($request['from_branch'] == $request['to_branch'])
            {
                return Redirect::to('admin/branch_transfers/save')->with(
                    [
                        "msg"=>"<div class='alert alert-danger'>لا يمكن التحويل لنفس الفرع</div>"
                    ]
                )->send();
            }

            if ($this->data["current_user"]->user_type == "branch_admin" &&
                $this->branch_id != $request['from_branch']
            )
            {
                return Redirect::to('admin/branch_transfers/save')->with(
                    [
                        "msg"=>"<div class='alert alert-danger'>غير مسموح بعمل طلب تحويل من فرع غير الفرع الخاص بك</div>"
                    ]
                )->send();
            }

            $pro_ids = $request['pro_id'];
            $pro_quantities = $request['pro_quantity'];
            $from_branch = $request['from_branch'];
            $to_branch = $request['to_branch'];

            $check_status = $this->_check_stock_products_availability($pro_ids , $pro_quantities ,$from_branch);

            if ($check_status != true)
            {
                return Redirect::to('admin/branch_transfers/save')->with(
                    [
                        "msg"=>"<div class='alert alert-danger'>$check_status</div>"
                    ]
                )->send();
            }

            $new_obj = branch_transfers_m::create([
                "from_branch" => $from_branch,
                "to_branch" => $to_branch
            ]);

            $input_arr = [];
            foreach($pro_ids as $key => $pro)
            {
                $input_arr[] = [
                    "b_t_id" => $new_obj->b_t_id,
                    "pro_id" => $pro,
                    "pro_quantity" => $pro_quantities[$key],
                ];
            }

            branch_transfer_products_m::insert($input_arr);

            #region save in site_tracking
            $this->track_my_action(
                " تم عمل طلب تحويل منتجات من فرع لفرع "
            );
            #endregion

            return Redirect::to('admin/branch_transfers/save')->with(
                [
                    "msg"=>"<div class='alert alert-success'>تم الحفظ بنجاح</div>"
                ]
            )->send();


        }

        return view("admin.subviews.branch_transfer_products.save")->with($this->data);
    }


    public function receive_branch_transfer(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"branch_transfer_products","receive_transfers",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }

        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");

        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);

            if (!is_object($obj))
            {
                $output["msg"]="<div class='alert alert-danger'> هذا الطلب غير موجود !! </div>";
                echo json_encode($output);
                return;
            }

            if ($obj->b_t_status == 1)
            {
                $output["msg"]="<div class='alert alert-danger'> هذا الطلب تم إستلامه بالفعل </div>";
                echo json_encode($output);
                return;
            }

            $get_orders = branch_transfer_products_m::where("b_t_id",$obj->b_t_id)->get()->all();
            if (!count($get_orders))
            {
                $output["msg"]="<div class='alert alert-danger'> لا يوجد منتجات لهذا الطلب </div>";
                echo json_encode($output);
                return;
            }

            $pro_ids = convert_inside_obj_to_arr($get_orders, "pro_id");
            $pro_quantities = convert_inside_obj_to_arr($get_orders, "pro_quantity");

            $from_branch = $obj->from_branch;
            $to_branch = $obj->to_branch;

            if ($this->data["current_user"]->user_type == "admin")
            {


                $return_check = $this->_check_stock_products_availability($pro_ids , $pro_quantities ,$from_branch);

                if ($return_check != true)
                {
                    $output["msg"]="<div class='alert alert-danger'> $return_check </div>";
                    echo json_encode($output);
                    return;
                }


            }
            else{

                if ($obj->to_branch != $this->branch_id)
                {
                    $output["msg"]="<div class='alert alert-danger'> لا يمكنك إستلام هذا الطلب </div>";
                    echo json_encode($output);
                    return;
                }

                $return_check = $this->_check_stock_products_availability($pro_ids , $pro_quantities ,$from_branch);

                if ($return_check != true)
                {
                    $output["msg"]="<div class='alert alert-danger'> $return_check </div>";
                    echo json_encode($output);
                    return;
                }


            }


            $this->_save_products_on_branch_to_stock($pro_ids , $pro_quantities ,$from_branch , $to_branch);

            $return_statues=$obj->update(["$field_name"=>1]);

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم استلام منتجات محوله من مخزن "
            );
            #endregion

            $output["msg"]="<div class='alert alert-success'> $action_desc </div>";
            echo json_encode($output);
            return;


        }

        echo json_encode($output);
    }


    // check if all products quantities is available on branch stock
    private function _check_stock_products_availability($pro_ids , $pro_quantities ,$from_branch)
    {

        if (!count($pro_ids) || (count($pro_ids) != count($pro_quantities)))
        {
            return " لا توجد منتجات !! ";
        }

        foreach($pro_ids as $key => $pro)
        {

            // check quantity for each product on order
            $get_this_pro_on_branch_stock = branches_products_m::get_branches_pros("
                AND branch_pro.branch_id = $from_branch
                AND branch_pro.pro_id = $pro
            ");
            if (!isset($get_this_pro_on_branch_stock[0]) || !is_object($get_this_pro_on_branch_stock[0]))
            {
                $return_msg = " المنتج ";
                $return_msg .= $get_this_pro_on_branch_stock->pro_name;
                $return_msg .= " غير موجود في الفرع المحول منه ";
                return $return_msg;
            }

            $get_this_pro_on_branch_stock = $get_this_pro_on_branch_stock[0];

            if ($get_this_pro_on_branch_stock->b_p_quantity < $pro_quantities[$key])
            {
                $return_msg = " المنتج ";
                $return_msg .= $get_this_pro_on_branch_stock->pro_name;
                $return_msg .= " كميته لا تسمح للسحب ";
                return $return_msg;
            }

        }

        return true;
    }


    // save products on branch_to >> target
    private function _save_products_on_branch_to_stock($pro_ids , $pro_quantities ,$from_branch , $to_branch)
    {

        // minus quantity from >> from_branch and check pro_limit

        foreach($pro_ids as $key => $pro)
        {

            $get_branch_pro_on_stock = branches_products_m::
                where("branch_id",$from_branch)
                ->where("pro_id",$pro)->get()->first();
            if ( ($get_branch_pro_on_stock->b_p_quantity - $pro_quantities[$key]) <= $get_branch_pro_on_stock->b_p_quantity_limit )
            {

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = "وصول كمية المنتج لحد الطلب" ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion

                #region send email
                $this->_send_email_to_all_users_type(
                    $user_type = "admin" ,
                    $data = "وصول كمية المنتج لحد الطلب",
                    $subject = "وصول كمية المنتج لحد الطلب"
                );
                #endregion
            }

            $curent_date = date("Y-m-d H:i:s");

            $get_branch_pro_on_stock->update([
                "b_p_date" => $curent_date,
                "b_p_quantity" => ($get_branch_pro_on_stock->b_p_quantity - $pro_quantities[$key])
            ]);

            // send them to stock of >> branch_to
            $get_branch_to_pro_on_stock = branches_products_m::
            where("branch_id",$to_branch)
                ->where("pro_id",$pro)->get()->first();
            if (!is_object($get_branch_to_pro_on_stock))
            {
                branches_products_m::create([
                    "branch_id" => $to_branch,
                    "pro_id" => $pro,
                    "b_p_quantity" => $pro_quantities[$key],
                    "b_p_date" => $curent_date,
                    "b_p_first_time" => 1
                ]);
            }
            else{
                $get_branch_to_pro_on_stock->update([
                    "b_p_date" => $curent_date,
                    "b_p_quantity" => ($get_branch_to_pro_on_stock->b_p_quantity+$pro_quantities[$key])
                ]);
            }

        }

    }


    public function remove_branch_transfers(Request $request)
    {

        if (!check_permission($this->user_permissions,"branch_transfer_products","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $item_id = $request->get("item_id");
        if ($item_id > 0)
        {
            $obj = branch_transfers_m::find($item_id);
            if (!is_object($obj))
            {
                echo json_encode(["msg"=>"<div class='alert alert-danger'>هذا الطلب غير موجود</div>"]);
                return;
            }

            if ($obj->b_t_status == 1)
            {
                echo json_encode(["msg"=>"<div class='alert alert-danger'>لا يمكن مسح الطلب بعد الإستلام</div>"]);
                return;
            }

            if ($this->data["current_user"]->user_type == "admin")
            {
                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم مسح طلب تحويل منتجات بين الفروع "
                );
                #endregion

                $this->general_remove_item($request,'App\models\branch_transfers\branch_transfers_m');
                return;
            }

            if ($this->data["current_user"]->user_type == "branch_admin" &&
                    $this->branch_id != $obj->from_branch
                )
            {
                echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح بمسح هذا الطلب</div>"]);
                return;
            }

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم مسح طلب تحويل منتجات بين الفروع "
            );
            #endregion

            $this->general_remove_item($request,'App\models\branch_transfers\branch_transfers_m');
            return;
        }

    }
}
