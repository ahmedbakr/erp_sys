<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\site_tracking_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class site_tracking extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {

        if (!check_permission($this->user_permissions,"site_tracking","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحه  </div>"
            ])->send();
        }

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_users"] = User::get_users(" AND (u.user_type = 'branch_admin' OR u.user_type = 'admin') ");
        }
        else{
            $this->data["all_users"] = User::get_users(" AND u.user_id = $this->user_id");
        }

        $this->data["tracking_data"] = "";
        $this->data["submitted_user"] = "";
        $this->data["submitted_date"] = "";

        if ($request->method() == "POST")
        {

            $this->validate($request,[

                "user_id" => "required|numeric|min:1",
                "date" => "required|date",

            ]);

            $request_data = $this->cleaning_input($request->all());
            $date = $request_data["date"];
            $user_id = $request_data["user_id"];

            $this->data["submitted_user"] = $user_id;
            $this->data["submitted_date"] = $date;

            $get_tracking = site_tracking_m::get_user_tracking(" where date(track.created_at) = '$date' And track.user_id = $user_id order by track.created_at desc ");

            if (is_array($get_tracking) && count($get_tracking))
            {

                $get_tracking = collect($get_tracking)->groupBy('hours');
                $get_tracking = $get_tracking->all();
//                dump($get_tracking);
//                die();
                $this->data["tracking_data"] = $get_tracking;

                $this->data["success"] = "<div class='alert alert-success'> بيانات حركات ها المستخدم </div>";
            }
            else{


                $this->data["success"] = "<div class='alert alert-danger'> لا توجد حركات لهذا المستخدم في هذا اليوم </div>";
            }

        }


        return view("admin.subviews.site_tracking.index",$this->data);
    }

}
