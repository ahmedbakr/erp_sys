<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\bills\clients_bills_m;
use App\models\branch_deposite_money_m;
use App\models\branch_target_m;
use App\models\commission\day_commission_m;
use App\models\money_transfers_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class deposite_money extends is_admin_controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/deposite_money","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->data["start_date"] = date("Y-m-d");
        $this->data["end_date"] = date("Y-m-d");
        $this->data["selected_branch"] = "";

        $current_data=Carbon::now();
        $current_hour = date("H");
        if ($current_hour >= 5)
        {
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }
        else{
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-2, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 4,59);
        }

        $star_day=$star_day->toDateTimeString();
        $target_day=$target_day->toDateTimeString();
        $this->data["star_day"] = $star_day;
        $this->data["target_day"] = $target_day;

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $this->branch_id ");
        }


        $this->data["deposite_money"] = [];

        return view("admin.subviews.deposite_money.show")->with($this->data);
    }


    public function filter_results($branch_id , $from_date , $to_date)
    {
        if (!check_permission($this->user_permissions,"admin/deposite_money","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->data["start_date"] = clean($from_date);
        $this->data["end_date"] = clean($to_date);
        $this->data["selected_branch"] = clean($branch_id);

        $current_data=Carbon::now();
        $current_hour = date("H");
        if ($current_hour >= 5)
        {
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }
        else{
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-2, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 4,59);
        }

        $star_day=$star_day->toDateTimeString();
        $target_day=$target_day->toDateTimeString();
        $this->data["star_day"] = $star_day;
        $this->data["target_day"] = $target_day;

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        $this->data["deposite_money"] = branch_deposite_money_m::get_data(" 
                AND deposite_money.branch_id = $branch_id 
                AND deposite_money.deposite_date >= '$from_date'
                AND deposite_money.deposite_date <= '$to_date' 
                order by deposite_money.deposite_date desc
            ");

        return view("admin.subviews.deposite_money.show")->with($this->data);
    }



    public function save_deposite_money(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/deposite_money","add_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $current_data=Carbon::now();
        $current_hour = date("H");
        if ($current_hour >= 5)
        {
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }
        else{
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 4,59);
        }

        $target_day=$target_day->toDateTimeString();
        $this->data["target_day"] = $target_day;

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        $current_data=Carbon::now();
        $current_hour = date("H");
        if ($current_hour >= 5)
        {
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }
        else{
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-2, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 4,59);
        }

        $star_day=$star_day->toDateTimeString();
        $target_day=$target_day->toDateTimeString();
        $this->data["star_day"] = $star_day;
        $this->data["target_day"] = $target_day;


        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "deposite_money" => "required|numeric|min:0",
                "branch_id" => "required"
            ]);

            $branch_id = $request["branch_id"];
            $branch_obj = User::find($branch_id);


            $request["attach_file_id"] = $this->general_save_img($request , $item_id=null, "attach_file",
                $new_title = "", $new_alt = "",
                $upload_new_img_check = $request->get("attach_file_checkbox"), $upload_file_path = "/deposite_money/",
                $width = 0, $height = 0, $photo_id_for_edit = 0);

            $request = clean($request->all());
            $lose_money = 0;

            $deposite_money = $request['deposite_money'];


            // check and count original price
            $last_branch_deposite_money = branch_deposite_money_m::get_data(" 
                AND deposite_money.branch_id = $branch_id 
                order by deposite_money.deposite_date desc limit 1
            ");

            $deposite_date = $target_day;
            $request["deposite_date"] = date("Y-m-d",strtotime($deposite_date));

            $request["created_at"] = $target_day;
            $request["updated_at"] = $target_day;

            if (is_array($last_branch_deposite_money) && count($last_branch_deposite_money) && isset($last_branch_deposite_money[0]))
            {

                $last_branch_deposite_money = $last_branch_deposite_money[0];
                if ($deposite_date <= $last_branch_deposite_money->deposite_created_at)
                {
                    return  Redirect::to('admin/deposite_money/save_deposite_money')->with(
                        ["msg"=>"<div class='alert alert-danger'>يوجد إيداع سابق بتاريخ مساوي لهذا التاريخ او اقل منه</div>"]
                    )->send();
                }

                // count bills <= $deposite_date he entered and > $last_branch_deposite_money->deposite_date
                $get_bills = clients_bills_m::where("branch_id",$branch_id)
                    ->where("client_bill_date",">","$last_branch_deposite_money->deposite_created_at")
                    ->where("client_bill_date","<=","$deposite_date")->get();
                $get_bills = $get_bills->all();
                if (is_array($get_bills) && count($get_bills))
                {
                    $get_bills_ids = convert_inside_obj_to_arr($get_bills,"client_bill_id");
                    $get_bills_purchase = convert_inside_obj_to_arr($get_bills,"client_total_paid_amount_in_cash");
                    $get_bills_returns = convert_inside_obj_to_arr($get_bills,"client_bill_total_return_amount");

                    $orgin_total = (array_sum($get_bills_purchase) - array_sum($get_bills_returns));
//                    $orgin_total += $last_branch_deposite_money->lose_money;
                    $lose_money = ($orgin_total-$deposite_money);
                    $request["origin_money"] = $orgin_total;
                    $request["lose_money"] = $lose_money;

                    #region today_total_commissions
                    $get_today_total_commissions = day_commission_m::get_data(" 
                        AND commission.user_id = $branch_id
                        AND commission.is_received = 1
                        AND commission.updated_at > '$last_branch_deposite_money->deposite_created_at'
                        AND commission.updated_at <= '$deposite_date'
                     ");
                    $get_today_total_commissions_value = 0;
                    if (count($get_today_total_commissions))
                    {
                        $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                        $get_today_total_commissions_value = array_sum($get_today_total_commissions);
                    }
                    #endregion

                    #region get money transfers

                    $get_bills_ids = implode(',',$get_bills_ids);
                    $get_money_transfers = money_transfers_m::get_data("
                        AND money.m_t_bill_type = 'client_bill'
                        AND money.m_t_status = 1
                        AND money.m_t_amount_type = 'cash'
                        AND money.m_t_bill_id in ($get_bills_ids)
                    ");

                    $get_all_money = collect($get_money_transfers)->groupBy('to_user_id');
                    $total_money_on_box = 0;
                    foreach ($get_all_money as $money_key => $money_items)
                    {
                        if ($money_key == $branch_id)
                        {
                            // عمليات بيع
                            $temp_money = convert_inside_obj_to_arr($money_items,"m_t_amount");
                            $total_money_on_box += array_sum($temp_money);
                        }
                        else{

                            // عمليات مرتجع
                            $temp_money = convert_inside_obj_to_arr($money_items,"m_t_amount");
                            $total_money_on_box -= array_sum($temp_money);
                        }
                    }

                    $request["total_money_on_box"] = ($total_money_on_box - $get_today_total_commissions_value);
                    $request["origin_money"] = ($request["origin_money"] - $get_today_total_commissions_value);

                    #endregion

                    $request["origin_money"] = $request["origin_money"] + $last_branch_deposite_money->lose_money;

                    if ($request["origin_money"] > $branch_obj->user_balance)
                    {
                        return  Redirect::to('admin/deposite_money/save_deposite_money')->with(
                            ["msg"=>"<div class='alert alert-danger'>رصيد الفرع لا يسمح لعمل إيداع</div>"]
                        )->send();
                    }

                    User::find($branch_id)->update([
                        "user_balance" => ($branch_obj->user_balance - $deposite_money)
                    ]);

                    branch_deposite_money_m::create($request);

                }
                else{
                    $request["origin_money"] = $last_branch_deposite_money->lose_money;
                    $lose_money = 0;
                    if ($last_branch_deposite_money->lose_money > $deposite_money)
                    {
                        $lose_money = ($last_branch_deposite_money->lose_money - $deposite_money);
                    }
                    $request["lose_money"] = $lose_money;

                    if ($request["origin_money"] > $branch_obj->user_balance)
                    {
                        return  Redirect::to('admin/deposite_money/save_deposite_money')->with(
                            ["msg"=>"<div class='alert alert-danger'>رصيد الفرع لا يسمح لعمل إيداع</div>"]
                        )->send();
                    }
                    branch_deposite_money_m::create($request);

                }

            }
            else{
                // case that is first deposite

                // count bills until $deposite_date he entered >> (<=)
                $get_bills = clients_bills_m::where("branch_id",$branch_id)
                    ->where("client_bill_date","<=","$deposite_date")->get();
                $get_bills = $get_bills->all();
                if (is_array($get_bills) && count($get_bills))
                {
                    $get_bills_ids = convert_inside_obj_to_arr($get_bills,"client_bill_id");
                    $get_bills_purchase = convert_inside_obj_to_arr($get_bills,"client_total_paid_amount_in_cash");
                    $get_bills_returns = convert_inside_obj_to_arr($get_bills,"client_bill_total_return_amount");

                    #region today_total_commissions
                    $get_today_total_commissions = day_commission_m::get_data(" 
                        AND commission.user_id = $branch_id
                        AND commission.is_received = 1
                        AND commission.updated_at <= '$deposite_date'
                     ");
                    $get_today_total_commissions_value = 0;
                    if (count($get_today_total_commissions))
                    {
                        $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                        $get_today_total_commissions_value = array_sum($get_today_total_commissions);
                    }
                    #endregion

                    $orgin_total = (array_sum($get_bills_purchase) - array_sum($get_bills_returns));
                    $orgin_total = ($orgin_total - $get_today_total_commissions_value);
                    $lose_money = ($orgin_total-$deposite_money);
                    $request["origin_money"] = $orgin_total;
                    $request["lose_money"] = $lose_money;


                    #region get money transfers

                    $get_bills_ids = implode(',',$get_bills_ids);
                    $get_money_transfers = money_transfers_m::get_data("
                        AND money.m_t_bill_type = 'client_bill'
                        AND money.m_t_status = 1
                        AND money.m_t_amount_type = 'cash'
                        AND money.m_t_bill_id in ($get_bills_ids)
                    ");

                    $get_all_money = collect($get_money_transfers)->groupBy('to_user_id');
                    $total_money_on_box = 0;
                    foreach ($get_all_money as $money_key => $money_items)
                    {
                        if ($money_key == $branch_id)
                        {
                            // عمليات بيع
                            $temp_money = convert_inside_obj_to_arr($money_items,"m_t_amount");
                            $total_money_on_box += array_sum($temp_money);
                        }
                        else{

                            // عمليات مرتجع
                            $temp_money = convert_inside_obj_to_arr($money_items,"m_t_amount");
                            $total_money_on_box -= array_sum($temp_money);
                        }
                    }

                    $request["total_money_on_box"] = ($total_money_on_box - $get_today_total_commissions_value);

                    #endregion

                    if ($request["origin_money"] > $branch_obj->user_balance)
                    {
                        return  Redirect::to('admin/deposite_money/save_deposite_money')->with(
                            ["msg"=>"<div class='alert alert-danger'>رصيد الفرع لا يسمح لعمل إيداع</div>"]
                        )->send();
                    }

                    User::find($branch_id)->update([
                        "user_balance" => ($branch_obj->user_balance - $deposite_money)
                    ]);

                    branch_deposite_money_m::create($request);

                }
                else{
                    $request["origin_money"] = 0;
                    $request["lose_money"] = 0;

                    if ($request["origin_money"] > $branch_obj->user_balance)
                    {
                        return  Redirect::to('admin/deposite_money/save_deposite_money')->with(
                            ["msg"=>"<div class='alert alert-danger'>رصيد الفرع لا يسمح لعمل إيداع</div>"]
                        )->send();
                    }
                    branch_deposite_money_m::create($request);

                }

            }

            $action_desc = "لقد تم إيداع مبلغ ".$deposite_money;
            $action_desc .= " للفرع ".$branch_obj->full_name;
            $action_desc .= " والقيمة الاصلية المطلوب إيداعها كانت ".$request["origin_money"];
            $action_desc .= " والمتبقي ".$request["lose_money"];

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $action_desc
            );
            #endregion

            return  Redirect::to('admin/deposite_money')->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.deposite_money.save")->with($this->data);


    }

    public function cal_original_deposite_money(Request $request)
    {

        $output = [];
        $output["msg"] = "";

        $deposite_date =clean($request->get("to_date_id"));
        $branch_id =clean($request->get("branch_id"));

        $branch_obj = User::find($branch_id);

        $current_data=Carbon::now();
        $current_hour = date("H");
        if ($current_hour >= 5)
        {
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }
        else{
            $star_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-2, $hour = 5,00);
            $target_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 4,59);
        }

        $star_day=$star_day->toDateTimeString();
        $target_day=$target_day->toDateTimeString();
        $this->data["star_day"] = $star_day;
        $this->data["target_day"] = $target_day;

        $last_branch_deposite_money = branch_deposite_money_m::get_data(" 
                AND deposite_money.branch_id = $branch_id 
                order by deposite_money.deposite_date desc limit 1
            ");

        $deposite_date = $target_day;

        if (is_array($last_branch_deposite_money) && count($last_branch_deposite_money) && isset($last_branch_deposite_money[0]))
        {

            $last_branch_deposite_money = $last_branch_deposite_money[0];
            if ($deposite_date <= $last_branch_deposite_money->deposite_created_at)
            {
                $output["msg"] = "<div class='alert alert-danger'>يوجد إيداع سابق بتاريخ مساوي لهذا التاريخ او اقل منه</div>";
                echo json_encode($output);
                return;
            }

            // count bills <= $deposite_date he entered and > $last_branch_deposite_money->deposite_date
            $get_bills = clients_bills_m::where("branch_id",$branch_id)
                ->where("client_bill_date",">","$last_branch_deposite_money->deposite_created_at")
                ->where("client_bill_date","<=","$deposite_date")->get();
            $get_bills = $get_bills->all();
            if (is_array($get_bills) && count($get_bills))
            {
                $get_bills_purchase = convert_inside_obj_to_arr($get_bills,"client_total_paid_amount_in_cash");
                $get_bills_returns = convert_inside_obj_to_arr($get_bills,"client_bill_total_return_amount");

                $orgin_total = (array_sum($get_bills_purchase) - array_sum($get_bills_returns));
//                $orgin_total += $last_branch_deposite_money->lose_money;

                #region today_total_commissions
                $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.user_id = $branch_id
                    AND commission.is_received = 1
                    AND commission.updated_at > '$last_branch_deposite_money->deposite_created_at'
                    AND commission.updated_at <= '$deposite_date'
                 ");
                $get_today_total_commissions_value = 0;
                if (count($get_today_total_commissions))
                {
                    $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                    $get_today_total_commissions_value = array_sum($get_today_total_commissions);
                }
                #endregion

                $orgin_total = $orgin_total + $last_branch_deposite_money->lose_money;

                $action_desc = " القيمة الاصلية المطلوب إيداعها هي ".($orgin_total - $get_today_total_commissions_value);
                $output["msg"] = "<div class='alert alert-info' style='font-size: 21px;'><b>$action_desc</b></div>";
                echo json_encode($output);
                return;

            }
            else{

                $action_desc = "القيمة الاصلية المطلوب إيداعها هي $last_branch_deposite_money->lose_money";
                $output["msg"] = "<div class='alert alert-info' style='font-size: 21px;'><b>$action_desc</b></div>";
                echo json_encode($output);
                return;

            }

        }
        else{
            // case that is first deposite

            // count bills until $deposite_date he entered >> (<=)
            $get_bills = clients_bills_m::where("branch_id",$branch_id)
                ->where("client_bill_date","<=","$deposite_date")->get();
            $get_bills = $get_bills->all();
            if (is_array($get_bills) && count($get_bills))
            {
                $get_bills_purchase = convert_inside_obj_to_arr($get_bills,"client_total_paid_amount_in_cash");
                $get_bills_returns = convert_inside_obj_to_arr($get_bills,"client_bill_total_return_amount");

                $orgin_total = (array_sum($get_bills_purchase) - array_sum($get_bills_returns));

                #region today_total_commissions
                $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.user_id = $branch_id
                    AND commission.is_received = 1
                    AND commission.updated_at <= '$deposite_date'
                 ");
                $get_today_total_commissions_value = 0;
                if (count($get_today_total_commissions))
                {
                    $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                    $get_today_total_commissions_value = array_sum($get_today_total_commissions);
                }
                #endregion

                $action_desc = " القيمة الاصلية المطلوب إيداعها هي ".($orgin_total - $get_today_total_commissions_value);
                $output["msg"] = "<div class='alert alert-info' style='font-size: 21px;'><b>$action_desc</b></div>";
                echo json_encode($output);
                return;

            }
            else{

                $action_desc = "القيمة الاصلية المطلوب إيداعها هي 0";
                $output["msg"] = "<div class='alert alert-info' style='font-size: 21px;'><b>$action_desc</b></div>";
                echo json_encode($output);
                return;

            }

        }

    }


    public function remove_deposite_money(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/deposite_money","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $item_id = $request->get("item_id");

        $get_deposite_obj = branch_deposite_money_m::get_data(" AND deposite_money.b_d_money_id = $item_id ");

        if (is_array($get_deposite_obj) && count($get_deposite_obj) && isset($get_deposite_obj[0]))
        {
            $get_deposite_obj = $get_deposite_obj[0];

            $action_desc = "تم مسح إيداع رقم ".$item_id." للفرع ".$get_deposite_obj->full_name;

            #region save in site_tracking
            $this->track_my_action(
                $action_desc
            );
            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region send user notification

            $this->send_user_notification(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_id = $get_deposite_obj->branch_id);

            #endregion

            $this->general_remove_item($request,'App\models\branch_deposite_money_m');
        }
        else{
            echo json_encode(["msg"=>"<div class='alert alert-danger'>بيانات غير صالحه !!</div>"]);
            return;
        }

    }


}
