<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\commission\commission_rules_m;
use App\models\commission\day_commission_m;
use App\models\product\products_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class commissions extends is_admin_controller
{
    public function __construct()
    {
            parent::__construct();
    }

    public function index(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/commission","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_commissions"] = [];
        $this->data["start_date"] = date("Y-m-d");
        $this->data["end_date"] = date("Y-m-d");
        $this->data["selected_user"] = "";

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_users"] = User::get_users(" AND (u.user_type = 'branch' ) ");
        }
        else{
            $branch_id = $this->data["current_user"]->related_id;
            $this->data["all_users"] = User::get_users(" AND (u.user_id = $branch_id ) ");
        }

        return view("admin.subviews.commissions.show",$this->data);

    }

    public function filter_results($branch_id , $from_date , $to_date)
    {
        if (!check_permission($this->user_permissions,"admin/commission","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->data["start_date"] = clean($from_date);
        $this->data["end_date"] = clean($to_date);
        $this->data["selected_user"] = clean($branch_id);


        $this->data["all_users"] = User::get_users(" AND (u.user_type = 'branch' OR u.user_type = 'admin' ) ");

        $cond = "";
        if ($this->data["selected_user"] > 0)
        {
            $cond = " AND commission.user_id = $branch_id  ";
        }

        $this->data["all_commissions"] = day_commission_m::get_data(" 
                $cond
                AND commission.day_date >= '$from_date'
                AND commission.day_date <= '$to_date' 
                order by commission.day_date desc
            ");

        return view("admin.subviews.commissions.show")->with($this->data);
    }

    public function show_rules($commission_id = null)
    {

        if (!check_permission($this->user_permissions,"admin/commission_rules","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["commission_id"] = $commission_id;
        $cond = "";

        if (is_array($_GET) && count($_GET) && isset($_GET["branch_id"]))
        {
            $branch_id = clean($_GET["branch_id"]);
            $cond = " AND rule.branch_id = $branch_id ";
        }


        if ($commission_id == null)
        {
            $this->data["commission_rules"] = commission_rules_m::get_data(" $cond ");
        }
        else{
            $this->data["commission_rules"] = commission_rules_m::get_data("
                AND rule.commission_id = $commission_id
                $cond
            ");
        }


        $this->get_branches_and_other_data();
        $get_all_branches = $this->data["all_branches"];
        if (!count($this->data["all_branches"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد فروع !!!   </div>"
            ])->send();
        }


        return view("admin.subviews.commissions.show_rules",$this->data);
    }

    public function save_commission_rule(Request $request , $commission_id = null)
    {
        $default_action = "add_action";
        if ($commission_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/commission_rules","$default_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->show_category_tree("true","product");

        $this->get_branches_and_other_data();
        $get_all_branches = $this->data["all_branches"];
        if (!count($this->data["all_branches"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد فروع !!!   </div>"
            ])->send();
        }

        $this->data["commission_rule"] = "";
        if ($commission_id != null)
        {
            $commission_rule = commission_rules_m::get_data(" AND rule.commission_id=$commission_id");
            if (isset_and_array($commission_rule))
            {
                $this->data["commission_rule"] = $commission_rule[0];
            }
            else{
                return  Redirect::to('admin/commissions/save_commission_rule')->with(
                    ["msg"=>"<div class='alert alert-danger'>القاعده غير موجوده , يمكنك اضافة جديد من هنا </div>"]
                )->send();
            }
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "branch_id" => "required",
                    "minimum_day_earn" => "required|numeric|min:0",
                    "branch_commission_value" => "required|numeric|min:0",
                    "factory_commission_value" => "required|numeric|min:0"
                ],
                [
                    "branch_id.required" => "الفرع مطلوب إدخالة",
                    "minimum_day_earn.required" => "محصلة البيع مطلوب إدخالة",
                    "branch_commission_value.required" => "عمولة الفرع مطلوب إدخالة",
                    "factory_commission_value.required" => "عمولة الإدارة مطلوب إدخالة",
                ]
            );

            $cat_ids=$request->get("commission_rule_cat_ids");
            $request["commission_rule_cat_ids"]=$cat_ids;

            $request = clean($request->all());
            $request["total_commission_value"] = (
                $request["branch_commission_value"] + $request["factory_commission_value"]
            );


            if ($commission_id == null)
            {
                $other_branches=[];
                if (isset($request["other_branches"])&&isset_and_array($request["other_branches"])){
                    $other_branches=$request["other_branches"];
                    unset($request["other_branches"]);
                }

                // insert
                $new_obj = commission_rules_m::create($request);
                $commission_id = $new_obj->commission_id;
                $action_desc = " تم حفظ قاعده عمولة جديدة برقم $commission_id";

                if (isset_and_array($other_branches)){

                    $other_rules=[];
                    foreach ($other_branches as $branch_id){
                        $request["branch_id"]=$branch_id;
                        $other_rules[]=[
                            "branch_id"=>$request["branch_id"],
                            "commission_rule_type"=>$request["commission_rule_type"],
                            "commission_rule_cat_ids"=>$request["commission_rule_cat_ids"],
                            'minimum_day_earn'=>$request["minimum_day_earn"],
                            'total_commission_value'=>$request["total_commission_value"],
                            'branch_commission_value'=>$request["branch_commission_value"],
                            'factory_commission_value'=>$request["factory_commission_value"],
                            'created_at'=>Carbon::now(),
                            'updated_at'=>Carbon::now()
                        ];
                    }

                    commission_rules_m::insert($other_rules);
                }


            }
            else{
                // update
                $new_obj=commission_rules_m::find($commission_id);
                $new_obj->update($request);
                $action_desc = " تم تعديل قاعده عمولة رقم $commission_id";
            }



            #region save in site_tracking
            $this->track_my_action(
                $action_desc
            );
            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            return  Redirect::to('admin/commissions/save_commission_rule')->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.commissions.save_commission_rule",$this->data);
    }

    public function remove_day_commission(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/commission","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $item_id = $request->get("item_id");

        $get_obj = day_commission_m::get_data(" AND commission.day_commission_id = $item_id ");

        if (is_array($get_obj) && count($get_obj) && isset($get_obj[0]))
        {
            $get_obj = $get_obj[0];

            $action_desc = "تم مسح العمولة رقم ".$item_id." للمستخدم ".$get_obj->full_name;

            #region save in site_tracking
            $this->track_my_action(
                $action_desc
            );
            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region send user notification

            $this->send_user_notification(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_id = $get_obj->user_id);

            #endregion

            $this->general_remove_item($request,'App\models\commission\day_commission_m');
        }
        else{
            echo json_encode(["msg"=>"<div class='alert alert-danger'>بيانات غير صالحه !!</div>"]);
            return;
        }

    }

    public function remove_commission_rule(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/commission_rules","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $item_id = $request->get("item_id");

        $get_obj = commission_rules_m::find($item_id);

        if (is_object($get_obj))
        {

            $action_desc = "تم مسح قاعده العموله رقم ".$item_id;

            #region save in site_tracking
            $this->track_my_action(
                $action_desc
            );
            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion

            $this->general_remove_item($request,'App\models\commission\commission_rules_m');
        }
        else{
            echo json_encode(["msg"=>"<div class='alert alert-danger'>بيانات غير صالحه !!</div>"]);
            return;
        }

    }

    public function change_commission_received(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/commission","receive_commission",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;

            if ($obj->is_received == 1)
            {
                $output["msg"] = "<div class='alert alert-danger'>غير مسموح !! </div>";
                echo json_encode($output);
                return;
            }

            if($make_update)
            {

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم إستلام العموله "
                );
                #endregion

                $user_obj = User::findOrFail($obj->user_id);
                $get_commission_with_rule = day_commission_m::get_data(" 
                    AND commission.day_commission_id = $obj->day_commission_id
                 ");
                $get_commission_with_rule = $get_commission_with_rule[0];


                if ($get_commission_with_rule->total_commission_value > $user_obj->user_balance)
                {
                    $output["msg"] = "<div class='alert alert-danger'>رصيد الفرع لا يسمح</div>";
                    echo json_encode($output);
                    return;
                }


                // check if commission updated_at date is valid from user_commission_days to receive
                $user_commission_days = $user_obj->user_commission_days;
                if ($user_commission_days > 0)
                {
                    $start_date=Carbon::createFromTimestamp(strtotime($obj->day_date));
                    $target_day=Carbon::create($year = $start_date->year, $month = $start_date->month, $day = $start_date->day+$user_commission_days)->toDateTimeString();
                    $target_only_date = date("Y-m-d",strtotime($target_day));

                    if ($target_only_date > date("Y-m-d"))
                    {

                        $diff_in_days = " باقي ";
                        $diff_in_days .= Carbon::createFromTimestamp(strtotime($target_day))->diffInDays();
                        $diff_in_days .= " يوم ";
                        $output["msg"] = "<div class='alert alert-danger'> غير مسموح بالتصفيه الان $diff_in_days</div>";
                        echo json_encode($output);
                        return;
                    }

                }



                $user_obj->update([
                    "user_balance" => ($user_obj->user_balance - $get_commission_with_rule->total_commission_value)
                ]);

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

            }

        }

        echo json_encode($output);
    }

}
