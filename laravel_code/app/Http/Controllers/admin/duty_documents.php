<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\duty_documents_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class duty_documents extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($send_or_recieve = "send")
    {

        if (!check_permission($this->user_permissions,"factory/duty_documents","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        if ($send_or_recieve == "send")
        {
            $documents = duty_documents_m::get_data(" AND duty.from_user_id = $this->user_id ");
            $this->data["documents_type"] = "تسليم";
        }
        else{
            $documents = duty_documents_m::get_data(" AND duty.to_user_id = $this->user_id ");
            $this->data["documents_type"] = "إستلام";
        }

        $this->data["documents"] = $documents;

        return view("admin.subviews.duty_documents.show",$this->data);
    }

    public function change_duty_document_received(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"factory/duty_documents","change_duty_document_received",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }

        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");

        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);

            if ($obj->to_user_id != $this->user_id)
            {
                $output["msg"]="<div class='alert alert-danger'> بيانات هذا السند غير موجودة </div>";
                echo json_encode($output);
                return;
            }

            if(is_object($obj))
            {

                if ($obj->duty_document_received == 0 && $accept == 1)
                {
                    $return_statues=$obj->update(["$field_name"=>"$accept"]);

                    // get sender user
                    $get_sender = User::findOrFail($obj->from_user_id);
                    $get_sender->update([
                        "user_balance" => ($get_sender->user_balance - $obj->duty_document_money),
                        "user_duty_balance" => ($get_sender->user_balance - $obj->duty_document_money)
                    ]);

                    // increase user_duty_balance, user_balance
                    User::findOrFail($this->user_id)->update([
                        "user_balance" => ($this->data["current_user"]->user_balance + $obj->duty_document_money),
                        "user_duty_balance" => ($this->data["current_user"]->user_duty_balance + $obj->duty_document_money)
                    ]);

                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " تم إستلام العهدة "
                    );
                    #endregion

                    $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

                }
                else{
                    $output["msg"]="<div class='alert alert-danger'> غير مسموح بتغيير حاله العهده بعد الإستلام </div>";
                    echo json_encode($output);
                    return;
                }


            }
            else{
                $output["msg"]="<div class='alert alert-danger'> بيانات هذا السند غير موجودة </div>";
                echo json_encode($output);
                return;
            }

        }

        echo json_encode($output);
    }


    public function save(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/duty_documents","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        $this->data["all_users"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_users = User::get_users(" AND u.user_type = 'branch_admin' ");
            $msg_col = "مستخدمين";
        }
        else{
            $all_users = User::get_users(" AND u.user_type = 'admin' ");
            $msg_col = "ادمن";
        }

        $this->data["choose_from"] = $msg_col;

        if (!count($all_users))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد $msg_col   </div>"
            ])->send();
        }

        $this->data["all_users"] = $all_users;

        if ($request->method()=="POST")
        {

            $this->validate($request,
                [
                    "to_user_id"=>"required",
                    "duty_document_money"=>"required",
                    "duty_document_date"=>"required|date",
                ],
                [
                    "to_user_id.required" => "الفرع مطلوب إدخالة",
                    "duty_document_money.required" => "المبلغ مطلوب إدخالة",
                    "duty_document_date.required" => "التاريخ مطلوب إدخالة",
                ]
            );


            $request = clean($request->all());

            $request["duty_document_date"] = date("Y-m-d H:i:s",strtotime($request["duty_document_date"]));

            $request["from_user_id"] = $this->user_id;

            include('phar://ArPHP.phar/Arabic.php');
            $obj = new \I18N_Arabic('Numbers');

            $request["duty_document_money_text"] = $obj->int2str($request["duty_document_money"]);

            duty_documents_m::create($request);
            $msg = "<div class='alert alert-success'>  تم تسجيل المستند بنجاح ..  </div>";

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم تسجيل مستند عهده "
            );
            #endregion

            return Redirect::to('admin/duty_documents/send')->with([
                "msg"=>$msg
            ])->send();
        }


        return view("admin.subviews.duty_documents.save_to_branch")->with($this->data);
    }

}
