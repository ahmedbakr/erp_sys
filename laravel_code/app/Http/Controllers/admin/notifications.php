<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\dashbaord_controller;
use App\Http\Controllers\is_admin_controller;
use App\models\ads_m;
use App\models\coupons_m;
use App\models\notification_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class notifications extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $get_notifications = notification_m::where("not_to_userid",$this->user_id)->orderBy("created_at","desc")->paginate(20);

        $this->data["notifications_pagination"] = $get_notifications;
        $this->data["all_notifications"] = [];

        if (count($get_notifications))
        {
            $get_notifications_ids = convert_inside_obj_to_arr($get_notifications,"not_id");
            $get_notifications_ids = implode($get_notifications_ids,',');
            $this->data["all_notifications"] = notification_m::get_notifications(" where not_id in ($get_notifications_ids) ");

        }

        return view("admin.subviews.notifications.show",$this->data);
    }


}
