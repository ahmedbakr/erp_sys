<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\bills\branch_bills_m;
use App\models\bills\clients_bills_m;
use App\models\bills\permission_bill_orders_m;
use App\models\bills\permission_bills_m;
use App\models\branch_transfers\branch_transfer_products_m;
use App\models\branch_transfers\branch_transfers_m;
use App\models\client_bill_used\client_bill_used_coupon_m;
use App\models\client_bill_used\client_bill_used_package_m;
use App\models\orders\branches_orders_m;
use App\models\orders\clients_orders_m;
use App\models\packages\packages_m;
use App\models\product\products_m;
use App\models\status_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class reports2 extends is_admin_controller
{

    public $fiter_branch_id;

    public function __construct()
    {
        parent::__construct();

        $this->data["results"] = [
            "number_of_bills" => 0,
            "number_of_waiting_review_bills" => 0,
            "number_of_reviewed_bills" => 0,
            "total_amount_of_bills" => 0,
            "total_discount_of_bills" => 0,
            "total_amount_of_bills_return" => 0,
            "total_amount_of_bills_in_cash" => 0,
            "total_amount_of_bills_in_atm" => 0,
            "total_remain_amount_of_bills" => 0,
        ];

        $this->data["age_range"] = [
            "15-25" => 0,
            "26-40" => 0,
            "41-60" => 0,
            "العمر>60" => 0
        ];

        $this->data["user_gender"] = [
            "male" => 0,
            "female" => 0
        ];

        $this->get_branches_and_other_data();

    }

    #region branches orders

    public function report_branch_orders(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","report_branch_orders",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]="تقرير طلبات الفروع";

        $this->data["all_post_data"]=(object)$request->all();
        
        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();

        $branch_id=$request->get("branch_id","");
        $branch_orders=[];

        $this->data["all_status"]=status_m::all();

        $all_status_grouped_by_status_id=collect($this->data["all_status"])->groupBy("status_id")->all();

        $this->data["all_status_grouped_by_status_id"]=$all_status_grouped_by_status_id;

        if($branch_id!=""){

            $bills_additional_where=[];

            if($branch_id>0){
                $bills_additional_where[]=" AND bill.branch_id=$branch_id";
                $this->data["meta_title"].=" - "." فرع ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
            }

            if(($bill_status_id=$request->get("bill_status_id",""))!=""){
                $bills_additional_where[]=" AND bill.status_id=$bill_status_id";
                $this->data["meta_title"].=" - ".$all_status_grouped_by_status_id[$bill_status_id][0]->status_name;
            }


            if(($bill_start_date=$request->get("bill_start_date",""))!="") {
                $bill_start_date=date("Y-m-d",strtotime($bill_start_date));
                $bills_additional_where[]=" AND bill.branch_bill_date>='$bill_start_date'";
                $this->data["meta_title"].=" - "." من التاريخ".$bill_start_date;
            }

            if(($bill_end_date=$request->get("bill_end_date",""))!="") {
                $bill_end_date=date("Y-m-d",strtotime($bill_end_date));
                $bills_additional_where[]=" AND bill.branch_bill_date<='$bill_end_date'";
                $this->data["meta_title"].=" - "." الي التاريخ".$bill_start_date;
            }


            $bills_additional_where=implode(" ",$bills_additional_where);
            $all_branch_bills= branch_bills_m::get_branch_bills($bills_additional_where);


            $branch_orders=[];
            if(isset_and_array($all_branch_bills)){
                $branch_bill_ids=convert_inside_obj_to_arr($all_branch_bills,"branch_bill_id");
                $orders_additional_where=[" AND branch_bill.branch_bill_id in (".implode(",",$branch_bill_ids).")"];

                if(($pro_search=$request->get("pro_search",""))!=""){
                    $orders_additional_where[]=" AND (pro.pro_name like '%$pro_search%' or pro.pro_barcode='$pro_search')";
                }

                $orders_additional_where=implode(" ",$orders_additional_where);

                $branch_orders=branches_orders_m::get_bill_orders($orders_additional_where);
            }
        }


        $this->data["branch_orders"]=$branch_orders;
        

        return view("admin..subviews.reports.report_branch_orders",$this->data);
    }

    #endegion

    #region branches transfers

    public function report_branch_transfers(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","report_branch_transfers",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]="تقرير تحويلات الفروع";

        $this->data["all_post_data"]=(object)$request->all();

        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();

        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;

        $branch_transfer_additional_where=[];
        $branch_transfer=[];
        $branch_transfer_products=[];


        if(($from_branch=$request->get("from_branch","0"))!="0"){
            $branch_transfer_additional_where[]=" AND transfer.from_branch=$from_branch";
            $this->data["meta_title"].=" - "." من فرع ".$all_branches_grouped_by_id[$from_branch][0]->full_name;
        }

        if(($to_branch=$request->get("to_branch","0"))!="0"){
            $branch_transfer_additional_where[]=" AND transfer.to_branch=$to_branch";
            $this->data["meta_title"].=" - "." الي فرع ".$all_branches_grouped_by_id[$to_branch][0]->full_name;
        }


        if(($start_date=$request->get("start_date",""))!="") {
            $start_date=date("Y-m-d",strtotime($start_date));
            $branch_transfer_additional_where[]=" AND date(transfer.created_at)>='$start_date'";
            $this->data["meta_title"].=" - "." من التاريخ".$start_date;
        }

        if(($end_date=$request->get("end_date",""))!="") {
            $end_date=date("Y-m-d",strtotime($end_date));
            $branch_transfer_additional_where[]=" AND date(transfer.created_at)<='$end_date'";
            $this->data["meta_title"].=" - "." من التاريخ".$end_date;
        }

        $branch_transfer=branch_transfers_m::get_data(implode(" ",$branch_transfer_additional_where));

        if(isset_and_array($branch_transfer)){

            $branch_transfer_ids=convert_inside_obj_to_arr($branch_transfer,"b_t_id");

            $branch_transfer_products_additional_where=[" AND transfer_pros.b_t_id in (".implode(",",$branch_transfer_ids).") "];

            if(($pro_search=$request->get("pro_search",""))!=""){
                $branch_transfer_products_additional_where[]=" AND (pro.pro_name like '%$pro_search%' or pro.pro_barcode='$pro_search')";
            }

            $branch_transfer_products=branch_transfer_products_m::get_data(implode(" ",$branch_transfer_products_additional_where));
        }

        $this->data["branch_transfer"]=$branch_transfer;
        $this->data["branch_transfer_products"]=$branch_transfer_products;


        return view("admin..subviews.reports.report_branch_transfers",$this->data);
    }

    #endregion


    #region permissions

    public function report_permission_bills(Request $request){


        if (!check_permission($this->user_permissions,"factory/reports","report_permission_bills",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]=" تقرير اذون الفروع ";

        $this->data["all_post_data"]=(object)$request->all();

        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();

        $branch_id=$request->get("branch_id","");
        $all_branch_bills=[];
        $permission_bill_orders=[];
        $permission_additional_where=[];

        if($branch_id!=""){
            $permission_additional_where[]=" AND bill.branch_id=$branch_id";
            $this->data["meta_title"].=" - "." فرع ".(($branch_id=="0")?"الفرع الرئيسي":$all_branches_grouped_by_id[$branch_id][0]->full_name);
        }

        if(($permission_type=$request->get("permission_type",""))!=""){
            $permission_additional_where[]=" AND bill.permission_type='$permission_type'";
            $this->data["meta_title"].=" - ".($permission_type=="add")?"اذن اضافة":"اذن صرف";
        }


        if(($bill_start_date=$request->get("bill_start_date",""))!="") {
            $bill_start_date=date("Y-m-d",strtotime($bill_start_date));
            $permission_additional_where[]=" AND bill.permission_date>='$bill_start_date'";
            $this->data["meta_title"].=" - "." من التاريخ".$bill_start_date;
        }

        if(($bill_end_date=$request->get("bill_end_date",""))!="") {
            $bill_end_date=date("Y-m-d",strtotime($bill_end_date));
            $permission_additional_where[]=" AND bill.permission_date<='$bill_end_date'";
            $this->data["meta_title"].=" - "." الي التاريخ".$bill_start_date;
        }


        $permission_additional_where=implode(" ",$permission_additional_where);
        $all_branch_bills=permission_bills_m::get_data($permission_additional_where);


        if(isset_and_array($all_branch_bills)){
            $branch_bill_ids=convert_inside_obj_to_arr($all_branch_bills,"permission_id");

            $orders_additional_where=[" AND o.permission_id in (".implode(",",$branch_bill_ids).")"];

            if(($pro_search=$request->get("pro_search",""))!=""){
                $orders_additional_where[]=" AND (pro.pro_name like '%$pro_search%' or pro.pro_barcode='$pro_search')";
            }

            $orders_additional_where=implode(" ",$orders_additional_where);

            $permission_bill_orders=permission_bill_orders_m::get_data($orders_additional_where);
        }


        $this->data["all_branch_bills"]=$all_branch_bills;
        $this->data["permission_bill_orders"]=$permission_bill_orders;


        return view("admin.subviews.reports.report_permission_bills",$this->data);

    }

    #endregion


    #region clients

    public function report_clients_data(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","report_clients_data",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]=" تقرير بيانات العملاء ";
        $this->get_branches_and_other_data();

        $this->data["all_post_data"]=(object)$request->all();
        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();
        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;

        $clients_additional_where=[" AND client.user_type in ('customer') "];

        $this->data['get_client_bills'] = [];

        if(($branch_id=$request->get("branch_id",""))!=""){
            if(isset($all_branches_grouped_by_id[$branch_id])){
                $this->data["meta_title"].=" - ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
            }

            $clients_additional_where[]=" AND client.related_id=$branch_id";

            // get each customer visits to branch
            $get_client_bills = clients_bills_m::where("branch_id",$branch_id)->get();
            $get_client_bills = collect($get_client_bills)->groupBy('customer_id')->all();
            $this->data['get_client_bills'] = $get_client_bills;

        }


        if(isset($_GET['age_range']) && $_GET['age_range'] != "0")
        {
            $age_range = $_GET['age_range'];
            $clients_additional_where[]=" AND client.age_range='$age_range'";
        }

        if(isset($_GET['user_gender']) && $_GET['user_gender'] != "0")
        {
            $user_gender = $_GET['user_gender'];
            $clients_additional_where[]=" AND client.user_gender='$user_gender'";
        }


        $all_clients=[];
        if(count($request->all())){
            $all_clients=User::get_user_money_transfer_statistics(implode(" ",$clients_additional_where));
        }

        $this->data["all_clients"]=$all_clients;

        return view("admin.subviews.reports.report_clients_data",$this->data);
    }

    #endregion

    #region clients bills

    public function report_clients_products(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","report_clients_products",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]=" تقرير مبيعات العملاء ";

        $this->data["all_post_data"]=(object)$request->all();

        $bills_additional_where=[];
        $all_client_bills_orders=[];

        if($request->method()=="GET"){

            if(($user_tel=$request->get("user_tel",""))!=""){
                $user_obj=User::where("user_tel",$user_tel)->get()->first();
                if(is_object($user_obj)){
                    $bills_additional_where[]=" AND client.user_id=$user_obj->user_id ";
                }
                $limit = "";
            }
            else{
                $limit = "limit 100";
            }
    
    

            $bills_additional_where=implode(" ",$bills_additional_where);
            $all_client_bills=clients_bills_m::get_clients_bills($bills_additional_where." $limit ");
            if(isset_and_array($all_client_bills)){
                $all_client_bills_orders=clients_orders_m::get_bill_orders(" 
                    AND client_bill.client_bill_id in (".implode(",",convert_inside_obj_to_arr($all_client_bills,"client_bill_id")).")
                ");
            }
    
            $all_client_bills=collect($all_client_bills)->groupBy("client_bill_id")->all();
        }

        $this->data["all_client_bills"]=$all_client_bills;
        $this->data["all_client_bills_orders"]=$all_client_bills_orders;

        return view("admin.subviews.reports.report_clients_products",$this->data);
    }

    public function most_products_bought(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","most_products_bought",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]="تقرير مبيعات العملاء - بالنسبة للمنتجات";

        $this->data["all_post_data"]=(object)$request->all();

        $bills_additional_where=[];
        $all_client_bills_orders=[];

        if($request->method()=="GET"){

            if(($user_tel=$request->get("user_tel",""))!=""){
                $user_obj=User::where("user_tel",$user_tel)->get()->first();
                if(is_object($user_obj)){
                    $bills_additional_where[]=" AND client.user_id=$user_obj->user_id ";
                }
                $limit = "";
            }
            else{
                $limit = "limit 100";
            }

            $bills_additional_where=implode(" ",$bills_additional_where);
            $all_client_bills=clients_bills_m::get_clients_bills($bills_additional_where." $limit ");
            if(isset_and_array($all_client_bills)){
                $all_client_bills_orders=
                    clients_orders_m::get_bill_orders_group_by_product(" 
                    AND client_bill.client_bill_id in (".implode(",",convert_inside_obj_to_arr($all_client_bills,"client_bill_id")).")
                ");
            }

            $all_client_bills=collect($all_client_bills)->groupBy("client_bill_id")->all();
        }

        $this->data["all_client_bills"]=$all_client_bills;
        $this->data["all_client_bills_orders"]=$all_client_bills_orders;

        return view("admin.subviews.reports.most_products_bought",$this->data);
    }

    public function report_products_bought(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","most_products_bought",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"]="تقرير مبيعات المنتجات";

        $pro_barcode = $request->get('pro_barcode','');

        $this->data["pro_data"] = "";
        $this->data["all_client_bills"] = [];
        $this->data["all_client_bills_orders"] = [];

        $this->data["all_post_data"]=(object)$request->all();

        $bills_additional_where=[];
        $all_client_bills_orders=[];

        if($request->method()=="GET" && !empty($pro_barcode)){


            $get_pro = products_m::get_pros("
                AND pro.pro_barcode = '$pro_barcode'
            ");

            if (count($get_pro))
            {
                $get_pro = $get_pro[0];
                $this->data["pro_data"] = $get_pro;


                $cond = "";
                if(isset($_GET['start_date']) && !empty($_GET['start_date']) &&
                    isset($_GET['end_date']) && !empty($_GET['end_date'])) {
                    $date_from = $_GET['start_date'];
                    $date_to = $_GET['end_date'];


                    $date_from = Carbon::createFromTimestamp(strtotime($date_from));
                    $date_to = Carbon::createFromTimestamp(strtotime($date_to));

                    $date_from = Carbon::create($date_from->year, $date_from->month, $date_from->day, 5)->toDateTimeString();
                    $date_to = Carbon::create($date_to->year, $date_to->month, $date_to->day, 4)->toDateTimeString();


                    $cond = "
                        AND clients_order.created_at between '$date_from' AND '$date_to'
                    ";

                }

                $all_client_bills_orders =
                    clients_orders_m::get_bill_orders_group_by_bill(" 
                            AND clients_order.pro_id = $get_pro->pro_id 
                            $cond
                        ");

                if (count($all_client_bills_orders))
                {
                    $bill_ids = convert_inside_obj_to_arr($all_client_bills_orders,"client_bill_id");
                    $bill_ids = implode(',',$bill_ids);
                    $all_client_bills=clients_bills_m::get_clients_bills("  
                        AND client_bill_id in ($bill_ids)
                    ");

                    $all_client_bills=collect($all_client_bills)->groupBy("client_bill_id")->all();
                    $this->data["all_client_bills"]=$all_client_bills;
                    $this->data["all_client_bills_orders"]=$all_client_bills_orders;

                }

            }

        }

        return view("admin.subviews.reports.report_products_bought",$this->data);
    }

//    public function packages_report(Request $request){
//
//        if (!check_permission($this->user_permissions,"factory/reports","packages_report",$this->data["current_user"]))
//        {
//            return Redirect::to('admin/dashboard')->with([
//                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
//            ])->send();
//        }
//
//        $this->data["all_post_data"]=(object)$request->all();
//        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();
//        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;
//
//        $this->data["meta_title"]="تقرير العروض ";
//
//        $this->data["all_post_data"]=(object)$request->all();
//
//        $this->data["all_packages"]=packages_m::all()->groupBy("package_id")->all();
//
//        if($request->method()=="GET"){
//            $client_bill_used_package=
//                client_bill_used_package_m::
//                join("clients_bills","clients_bills.client_bill_id","=","client_bill_used_package.client_bill_id")->
//                where("cbup_id",">","0");
//
//
//            if(($branch_id=$request->get("branch_id",""))!=""){
//                $this->data["meta_title"].=" - "." فرع ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
//                $client_bill_used_package=$client_bill_used_package->where("clients_bills.branch_id",$branch_id);
//            }
//
//
//            if(($start_date=$request->get("start_date",""))!=""){
//                $this->data["meta_title"]." - "." من التاريخ ".date("Y-m-d",strtotime($start_date));
//                $client_bill_used_package=$client_bill_used_package->where("used_at",">=",date("Y-m-d",strtotime($start_date)));
//            }
//
//            if(($end_date=$request->get("end_date",""))!=""){
//                $this->data["meta_title"]." - "." الي التاريخ ".date("Y-m-d",strtotime($end_date));
//                $client_bill_used_package=$client_bill_used_package->where("used_at","<=",date("Y-m-d",strtotime($end_date)));
//            }
//
//            $client_bill_used_package=$client_bill_used_package->get()->groupBy("package_id")->all();
//
//            $this->data["client_bill_used_package"]=$client_bill_used_package;
//        }
//
//
//        return view("admin.subviews.reports.packages_report",$this->data);
//    }

//    public function coupons_report(Request $request){
//
//        if (!check_permission($this->user_permissions,"factory/reports","coupons_report",$this->data["current_user"]))
//        {
//            return Redirect::to('admin/dashboard')->with([
//                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
//            ])->send();
//        }
//
//        $this->data["all_post_data"]=(object)$request->all();
//        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();
//        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;
//
//        $this->data["meta_title"]="تقرير الكوبونات ";
//
//        $this->data["all_post_data"]=(object)$request->all();
//
//        $this->data["all_coupons_cats"]=category_m::where("cat_type","coupon")->get()->groupBy("cat_id")->all();
//
//        if($request->method()=="GET"){
//            $client_bill_used_coupon=
//                client_bill_used_coupon_m::
//                join("clients_bills","clients_bills.client_bill_id","=","client_bill_used_coupon.client_bill_id")->
//                where("cbuc_id",">","0");
//
//
//            if(($branch_id=$request->get("branch_id",""))!=""){
//                $this->data["meta_title"].=" - "." فرع ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
//                $client_bill_used_coupon=$client_bill_used_coupon->where("clients_bills.branch_id",$branch_id);
//            }
//
//
//            if(($start_date=$request->get("start_date",""))!=""){
//                $this->data["meta_title"]." - "." من التاريخ ".date("Y-m-d",strtotime($start_date));
//                $client_bill_used_coupon=$client_bill_used_coupon->where("used_at",">=",date("Y-m-d",strtotime($start_date)));
//            }
//
//            if(($end_date=$request->get("end_date",""))!=""){
//                $this->data["meta_title"]." - "." الي التاريخ ".date("Y-m-d",strtotime($end_date));
//                $client_bill_used_coupon=$client_bill_used_coupon->where("used_at","<=",date("Y-m-d",strtotime($end_date)));
//            }
//
//            $client_bill_used_coupon=$client_bill_used_coupon->get()->groupBy("coupon_cat_id")->all();
//
//            $this->data["client_bill_used_coupon"]=$client_bill_used_coupon;
//        }
//
//
//        return view("admin.subviews.reports.coupons_report",$this->data);
//    }


    #endregion

}
