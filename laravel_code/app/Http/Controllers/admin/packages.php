<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\orders\branches_orders_m;
use App\models\packages\package_products_m;
use App\models\packages\packages_m;
use App\models\product\branches_products_m;
use App\models\product\products_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;

class packages extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index($start_date="",$end_date="")
    {

        if (!check_permission($this->user_permissions,"factory/packages","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->send();
        }

        $this->data["start_date"]="";
        $this->data["end_date"]="";

        $cond=[];
        if($start_date!=""&&$start_date!="0"){
            $cond[]=" AND pack.package_start_date>='$start_date'";

            $this->data["start_date"]=$start_date;
        }

        if($end_date!=""&&$end_date!="0"){
            $cond[]=" AND pack.package_end_date<'$end_date'";
            $this->data["end_date"]=$end_date;

        }


        $this->data["all_packages"]=packages_m::get_packages(implode(" ",$cond));

        return view("admin.subviews.packages.show",$this->data);
    }


    public function save(Request $request, $package_id= null)
    {
        if($package_id==null){
            if (!check_permission($this->user_permissions,"factory/packages","add_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>You can not access here</div>"])->send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"factory/packages","edit_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>You can not access here</div>"])->send();
            }
        }


        $all_products = products_m::all();
        $all_products = $all_products->all();
        $this->data["all_products"]=$all_products;
        $this->data["all_gift_products"]=$all_products;
        $this->data["all_branches"]=User::where("user_type","branch")->get()->all();





        $package_data="";


        if ($package_id != null)
        {
            $package_data = packages_m::get_packages(" and pack.package_id = $package_id ");

            if(isset_and_array($package_data)){
                $package_data=$package_data[0];
            }
            else{
                abort(404);
            }
        }
        $this->data["package_data"] = $package_data;

        /*
        if (is_array($all_products) && count($all_products))
        {
            $all_products_ids = convert_inside_obj_to_arr($all_products,"pro_id");

            // get other products on other offers
            if ($package_id != null)
            {
                $other_offers_products = package_products_m::where("package_id","!=",$package_id)->get();
            }
            else{
                $other_offers_products = package_products_m::all();
            }

            $other_offers_products = $other_offers_products->all();
            if (is_array($other_offers_products) && count($other_offers_products))
            {
                $other_offers_products_ids = convert_inside_obj_to_arr($other_offers_products,"pro_id");

                $final_allowed_products_ids = array_diff($all_products_ids,$other_offers_products_ids);
                if (is_array($final_allowed_products_ids) && count($final_allowed_products_ids))
                {
                    $this->data["all_products"] = products_m::whereIn("pro_id",$final_allowed_products_ids)->get();
                    $this->data["all_products"] = $this->data["all_products"]->all();
                }
                else{
                    $this->data["all_products"] = [];
                }

//                dump($final_allowed_products_ids);
            }

//            dump($other_offers_products);
        }
*/

        $this->show_category_tree("true","product");

        if ($request->method()=="POST")
        {

            $validator_value = [
                "package_start_date"=>$request->get("package_start_date"),
                "package_end_date"=>$request->get("package_end_date"),
                "min_products_to_get_offer"=>$request->get("min_products_to_get_offer"),
                "discount_percentage_for_product"=>$request->get("discount_percentage_for_product"),
            ];
            $validator_rule = [
                "package_start_date"=>"required",
                "package_end_date"=>"required",
                "min_products_to_get_offer"=>"required",
                "discount_percentage_for_product"=>"required",
            ];


            $validator = Validator::make(
                $validator_value,$validator_rule
            );


            if (count($validator->messages()) == 0)
            {

                if(false){
                    $product_gift_id = $request->get("product_gift_id");
                    if (!empty($product_gift_id) && $product_gift_id > 0)
                    {
                        $allowed_branch_ids = [];
                        foreach($request->get("package_branches") as $key => $branch_item_id)
                        {
                            $branch_products = branches_products_m::where("branch_id",$branch_item_id)->get();
                            $branch_products = $branch_products->all();
                            if (is_array($branch_products) && count($branch_products))
                            {
                                $branch_products_pro_ids = convert_inside_obj_to_arr($branch_products,"pro_id");
                                if(in_array($product_gift_id,$branch_products_pro_ids))
                                {
                                    $allowed_branch_ids[] = $branch_item_id;
                                }

                            }

                        }

                        if(isset($request["package_branches"]))
                        {
                            $request["package_branches"] = $allowed_branch_ids;
                        }

                    }
                }
                $cat_ids=$request->get("package_cats");
                $request["package_cats"]=json_encode($cat_ids);

                $package_obj="";

                // update
                if ($package_id != null)
                {
                    $package_obj=packages_m::find($package_id);
                    $check = $package_obj->update($request->all());

                    if ($check == true)
                    {
                        $this->data["success"] = "<div class='alert alert-success'> Data Successfully Edit </div>";
                        $return_id = $package_id;
                    }
                    else{
                        $this->data["success"] = "<div class='alert alert-danger'> Something Is Wrong !!!!</div>";
                    }

                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " تم تعديل بيانات عرض "
                    );
                    #endregion

                }
                else{

                    // insert
                    $package_obj = packages_m::create($request->all());



                    if (is_object($package_obj))
                    {
                        $this->data["success"] = "<div class='alert alert-success'> Data Successfully Inserted </div>";
                        $return_id = $package_obj->package_id;

                    }
                    else{
                        $this->data["success"] = "<div class='alert alert-danger'> Something Is Wrong !!!!</div>";
                    }

                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " تم تسجيل عرض جديد "
                    );
                    #endregion

                }


                #region add || edit package products


                //get all sub cats in these cats_ids
                if(isset_and_array($cat_ids)){
                    $all_cats_ids=category_m::get_all_child_cats($cat_ids);

                    //get unique cats ids
                    $all_cats_ids=array_unique($all_cats_ids);

                    //get all products related with these cats ids
                    $package_select_pros=products_m::whereIn("cat_id",$all_cats_ids)->get()->lists("pro_id")->all();

                    $package_obj->package_products()->sync($package_select_pros);
                    #endregion

                    #region add || edit package_branches
                    $package_branches=[];
                    if($request->get("package_branches")){
                        $package_branches=$request->get("package_branches");
                    }
                    $package_obj->package_branches()->sync($package_branches);
                }

                #endregion



                return Redirect::to("admin/packages/save")->with(["msg"=>"<div class='alert alert-success'> Data Successfully Updated </div>"])->send();

            }
            else{
                $this->data["errors"] = $validator->messages();
            }

        }

        return view("admin.subviews.packages.save")->with($this->data);
    }


    public function remove_package(Request $request){

        if (!check_permission($this->user_permissions,"factory/packages","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>You can not access here</div>"]);
            return;
        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = " تم مسح بيانات عرض "
        );
        #endregion

        $this->general_remove_item($request,'App\models\packages\packages_m');
    }




}
