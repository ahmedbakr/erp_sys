<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\admin\duty_documents_m;
use App\models\admin\points_of_sale_m;
use App\models\admin\price_list_m;
use App\models\bills\clients_bills_m;
use App\models\client_bill_used\client_bill_used_coupon_m;
use App\models\client_bill_used\client_bill_used_package_m;
use App\models\commission\day_commission_m;
use App\models\expenses\expenses_m;
use App\models\money_transfers_m;
use App\models\orders\clients_orders_m;
use App\models\packages\packages_m;
use App\models\product\branches_products_m;
use App\models\product\broken\broken_products_m;
use App\models\product\product_prices_values_m;
use App\models\product\products_m;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class reports extends is_admin_controller
{

    public $fiter_branch_id;

    public function __construct()
    {
        parent::__construct();

        $this->data["results"] = [
            "number_of_bills" => 0,
            "number_of_waiting_review_bills" => 0,
            "number_of_reviewed_bills" => 0,
            "total_amount_of_bills" => 0,
            "total_discount_of_bills" => 0,
            "total_amount_of_bills_return" => 0,
            "total_amount_of_bills_in_cash" => 0,
            "total_amount_of_bills_in_atm" => 0,
            "total_remain_amount_of_bills" => 0,
        ];

        $this->data["age_range"] = [
            "15-25" => 0,
            "26-40" => 0,
            "41-60" => 0,
            "العمر>60" => 0
        ];

        $this->data["user_gender"] = [
            "male" => 0,
            "female" => 0
        ];

        $this->get_branches_and_other_data();

    }

    // المبيعات

    public function total_branches_clients_bills(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->get_branches_and_other_data();

        $this->data["post_data"]=(object)$request->all();

        $branch_id=$request->get("select_branch_id");
        $date_from=$request->get("date_from");
        $date_to=$request->get("date_to");


        $date_from=Carbon::createFromTimestamp(strtotime($date_from));
        $date_to=Carbon::createFromTimestamp(strtotime($date_to));

        $date_from=Carbon::create($date_from->year,$date_from->month,$date_from->day,5);
        $date_to=Carbon::create($date_to->year,$date_to->month,$date_to->day,4);

        $date_from=$date_from->toDateTimeString();
        $date_to=$date_to->toDateTimeString();


        $this->data["meta_title"]="تقارير المبيعات اليومية من 
        $date_from
        الي 
     $date_to   ";

        $this->data["bills_data"]=[];
        $this->data["today_total_commissions"]=0;
        $this->data["return_money"] = [];


        if($branch_id=="all"){

            $this->data["bills_data"]=clients_bills_m::get_client_bills_count("
                AND bill.client_bill_date between '$date_from' AND '$date_to'
            ");

            #region get range return amount for previous bills

            $get_all_branches = User::where("user_type","branch")->get()->all();
            if(count($get_all_branches))
            {
                $get_all_branches_ids = convert_inside_obj_to_arr($get_all_branches,"user_id");
                $get_all_branches_ids = implode(",",$get_all_branches_ids);


                $get_all_month_return_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.from_user_id in ($get_all_branches_ids)
                                )
                                AND (money.created_at) >= '$date_from'
                                AND (money.created_at) <= '$date_to' 
                            ");


                if(count($get_all_month_return_money))
                {
                    foreach($get_all_month_return_money as $key => $value)
                    {

                        if (!isset($this->data["return_money"][$value->from_user_id]))
                        {
                            $this->data["return_money"][$value->from_user_id]["return_cash"] = 0;
                            $this->data["return_money"][$value->from_user_id]["return_atm"] = 0;
                            $this->data["return_money"][$value->from_user_id]["total"] = 0;
                        }

                        if ($value->m_t_amount_type == "cash")
                        {
                            $this->data["return_money"][$value->from_user_id]["return_cash"] += $value->m_t_amount;
                            $this->data["return_money"][$value->from_user_id]["total"] += $value->m_t_amount;
                        }
                        else{
                            $this->data["return_money"][$value->from_user_id]["return_atm"] += $value->m_t_amount;
                            $this->data["return_money"][$value->from_user_id]["total"] += $value->m_t_amount;
                        }

                    }
                }
            }

            #endregion



            #region today_total_commissions
            $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$date_from'
                    AND commission.updated_at <= '$date_to'
                 ");

            $get_today_total_commissions=collect($get_today_total_commissions)->groupBy("user_id");
            $this->data["today_total_commissions"]=$get_today_total_commissions;

            #endregion
        }
        elseif($branch_id>0){

            #region today_total_commissions
            $get_today_total_commissions = day_commission_m::get_data(" 
                    AND commission.user_id = $branch_id
                    AND commission.is_received = 1
                    AND commission.updated_at >= '$date_from'
                    AND commission.updated_at <= '$date_to'
                 ");

            if (count($get_today_total_commissions))
            {
                $get_today_total_commissions = convert_inside_obj_to_arr($get_today_total_commissions,"total_commission_value");
                $get_today_total_commissions = array_sum($get_today_total_commissions);
                $this->data["today_total_commissions"] = $get_today_total_commissions;
            }
            #endregion

            $this->data["bills_data"]=clients_bills_m::get_client_bills_count("
                AND bill.client_bill_date between '$date_from' AND '$date_to' AND bill.branch_id=$branch_id
            ");


            #region get range return amount for previous bills

            $get_all_branches = User::where("user_type","branch")->get()->all();
            if(count($get_all_branches))
            {

                $get_all_month_return_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.from_user_id = $branch_id
                                )
                                AND (money.created_at) >= '$date_from'
                                AND (money.created_at) <= '$date_to' 
                            ");


                if(count($get_all_month_return_money))
                {
                    foreach($get_all_month_return_money as $key => $value)
                    {

                        if (!isset($this->data["return_money"][$value->from_user_id]))
                        {
                            $this->data["return_money"][$value->from_user_id]["return_cash"] = 0;
                            $this->data["return_money"][$value->from_user_id]["return_atm"] = 0;
                            $this->data["return_money"][$value->from_user_id]["total"] = 0;
                        }

                        if ($value->m_t_amount_type == "cash")
                        {
                            $this->data["return_money"][$value->from_user_id]["return_cash"] += $value->m_t_amount;
                            $this->data["return_money"][$value->from_user_id]["total"] += $value->m_t_amount;
                        }
                        else{
                            $this->data["return_money"][$value->from_user_id]["return_atm"] += $value->m_t_amount;
                            $this->data["return_money"][$value->from_user_id]["total"] += $value->m_t_amount;
                        }

                    }
                }
            }

            #endregion

        }


        return view("admin.subviews.reports.total_branches_clients_bills",$this->data);
    }


    public function total_cashiers_clients_bills(Request $request){

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->get_branches_and_other_data();

        $this->data["post_data"]=(object)$request->all();

        $branch_admin_id=$request->get("branch_admin_id","");
        $branch_id=$request->get("branch_id","");
        $date_from=$request->get("date_from");
        $date_to=$request->get("date_to");


        $date_from=Carbon::createFromTimestamp(strtotime($date_from));
        $date_to=Carbon::createFromTimestamp(strtotime($date_to));

        $date_from=Carbon::create($date_from->year,$date_from->month,$date_from->day,5);
        $date_to=Carbon::create($date_to->year,$date_to->month,$date_to->day,4);

        $date_from=$date_from->toDateTimeString();
        $date_to=$date_to->toDateTimeString();


        $this->data["meta_title"]="تقارير مبيعات الكاشيير اليومية من 
        $date_from
        الي 
     $date_to   ";

        $this->data["bills_data"]=[];
        $this->data["today_total_commissions"]=0;
        $this->data["return_money"] = [];

        $this->data["selected_branch_id"] = "";
        $this->data["selected_branch_admin_id"] = "";


        if(!empty($branch_admin_id)){


            $this->data["selected_branch_id"] = $branch_id;
            $this->data["selected_branch_admin_id"] = $branch_admin_id;


            $this->data["bills_data"]=clients_bills_m::get_client_bills_count("
                AND bill.client_bill_date between '$date_from' AND '$date_to' AND bill.cashier_id=$branch_admin_id
            ");

        }

        return view("admin.subviews.reports.total_cashiers_clients_bills",$this->data);
    }



    public function sell_bills()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];

        $this->data["branches"] = [];
        $this->data["branch_details"] = [];
        $this->data["points_of_sale"] = [];
        $this->data["products"] = [];
        $this->data["pro_details"] = [];
        $this->data["product_statistics"] = [];

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];
        $this->data["cashiers"] = $this->data["all_branch_admins"];
        $this->data["customers"] = $this->data["all_customers"];


        #region filteration

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["bills"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;

            if(isset($get_filter_values['cust_id']) && $get_filter_values['cust_id']>0)
            {
                $cond .= " AND bill.customer_id = ".$get_filter_values['cust_id']." ";
            }


            if(isset($get_filter_values['branch_id']) && $get_filter_values['branch_id']>0)
            {
                $cond .= " AND bill.branch_id = ".$get_filter_values['branch_id']." ";
            }


            if(isset($get_filter_values['cashier_id']) && $get_filter_values['cashier_id']>0)
            {
                $cond .= " AND bill.cashier_id = ".$get_filter_values['cashier_id']." ";
            }

            if(isset($get_filter_values['age_range']) && $get_filter_values['age_range'] != "")
            {
                $cond .= " AND client.age_range = '".$get_filter_values['age_range']."' ";
            }


            if(isset($get_filter_values['user_gender']) && $get_filter_values['user_gender'] != "")
            {
                $cond .= " AND client.user_gender = '".$get_filter_values['user_gender']."' ";
            }

            if(isset($get_filter_values['pro_id']) && $get_filter_values['pro_id'] != "")
            {
                $cond .= " AND clients_order.pro_id = '".$get_filter_values['pro_id']."' ";
            }

            if($this->current_user_data->user_type=="branch_admin")
            {
                $cond .= " AND bill.branch_id =".$this->current_user_data->related_id;
            }

            if($from_date != "")
            {
                $from_date = date('Y-m-d H:i:s',strtotime($from_date));
                $to_date = date('Y-m-d H:i:s',strtotime($to_date));

                $cond .= " AND (bill.client_bill_date) >= '$from_date'
                            AND (bill.client_bill_date) <= '$to_date' ";
            }

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion


                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                if(isset($get_filter_values['branch_id']) && $get_filter_values['branch_id']>0)
                {
                    $get_branch = User::where("user_id",$get_filter_values['branch_id'])->get()->first();

                    $total_balance_of_point_of_sale = 0;
                    if (count($this->data["all_point_of_sales"]))
                    {
                        $this->data["points_of_sale"] = $this->data["all_point_of_sales"];
                        $total_balance_of_point_of_sale = convert_inside_obj_to_arr($this->data["all_point_of_sales"],"point_of_sale_balance");
                        $total_balance_of_point_of_sale = array_sum($total_balance_of_point_of_sale);
                    }

                    $this->data["branch_details"] = [
                        "branch_name" => $get_branch->full_name,
                        "branch_balance" => $get_branch->user_balance,
                        "branch_duty_balance" => $get_branch->user_duty_balance,
                        "number_of_cashiers" => count($this->data["all_branch_admins"]),
                        "number_of_clients" => count($this->data["all_customers"]),
                        "number_of_point_of_sale" => count($this->data["all_point_of_sales"]),
                        "total_balance_of_point_of_sale" => $total_balance_of_point_of_sale,
                    ];
                }

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills",$this->data);
    }


    public function sell_bills_session()
    {

        if (!check_permission($this->user_permissions,"admin/sell_bills_session","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];
        $this->data["cashiers"] = $this->data["all_branch_admins"];


        #region filteration

        $this->data["group_bills"] = [];
        $this->data["branch_details"] = [];
        $this->data["points_of_sale"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;


            if(isset($get_filter_values['branch_id']) && $get_filter_values['branch_id']>0)
            {
                $cond .= " AND bill.branch_id = ".$get_filter_values['branch_id']." ";
            }


            if(isset($get_filter_values['cashier_id']) && $get_filter_values['cashier_id']>0)
            {
                $cond .= " AND bill.cashier_id = ".$get_filter_values['cashier_id']." ";
            }


            if($this->current_user_data->user_type=="branch_admin")
            {
                $cond .= " AND bill.branch_id =".$this->current_user_data->related_id;
            }

            if($from_date != "")
            {

                $from_date = date('Y-m-d H:i:s',strtotime($from_date));
                $to_date = date('Y-m-d H:i:s',strtotime($to_date));

                $cond .= " AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' ";
            }


            $results = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date ");

            if (count($results))
            {

                $this->data["group_bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");
                $this->data["group_bills"] = collect($this->data["group_bills"])->groupBy('client_bill_date_only')->all();

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion

                if(isset($get_filter_values['branch_id']) && $get_filter_values['branch_id']>0)
                {
                    $get_branch = User::where("user_id",$get_filter_values['branch_id'])->get()->first();

                    $total_balance_of_point_of_sale = 0;
                    if (count($this->data["all_point_of_sales"]))
                    {
                        $this->data["points_of_sale"] = $this->data["all_point_of_sales"];
                        $total_balance_of_point_of_sale = convert_inside_obj_to_arr($this->data["all_point_of_sales"],"point_of_sale_balance");
                        $total_balance_of_point_of_sale = array_sum($total_balance_of_point_of_sale);
                    }

                    $this->data["branch_details"] = [
                        "branch_name" => $get_branch->full_name,
                        "branch_balance" => $get_branch->user_balance,
                        "branch_duty_balance" => $get_branch->user_duty_balance,
                        "number_of_cashiers" => count($this->data["all_branch_admins"]),
                        "number_of_clients" => count($this->data["all_customers"]),
                        "number_of_point_of_sale" => count($this->data["all_point_of_sales"]),
                        "total_balance_of_point_of_sale" => $total_balance_of_point_of_sale,
                    ];
                }

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_session",$this->data);
    }


    public function sell_bills_depend_on_customer()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_customer",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        if($this->current_user_data->user_type=="branch_admin"){
            $get_customers = User::where("user_type","customer")
                ->where("related_id",$this->current_user_data->related_id)->get();
        }
        else{
            $get_customers = User::where("user_type","customer")->get();
        }


        $get_customers = $get_customers->all();
        if (!count($get_customers))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا توجد عملاء حاليين !!</div>"
            )->send();
        }

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["user_id"] = "";
        $this->data["customers"] = $get_customers;
        $this->data["bills"] = [];
        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $user_id = $get_filter_values["user_id"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["user_id"] = $user_id;

            if($this->current_user_data->user_type=="branch_admin") {
                $cond .= " AND bill.branch_id = $this->branch_id ";
            }

            $cond .= " AND bill.customer_id = $user_id ";

            ($from_date != "")?
                ($cond .= " 
                            AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion


            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_depend_on_customer",$this->data);
    }


    public function sell_bills_depend_on_branch()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_branch",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        $this->get_branches_and_other_data();
        $get_branches = $this->data["all_branches"];

        if (!count($get_branches))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا توجد فروع حاليين !!</div>"
            )->send();
        }

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["user_id"] = "";
        $this->data["branches"] = $get_branches;
        $this->data["bills"] = [];
        $this->data["branch_details"] = [];
        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];
        $this->data["points_of_sale"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $user_id = $get_filter_values["user_id"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["user_id"] = $user_id;

            $cond .= " AND bill.branch_id = $user_id ";

            ($from_date != "")?
                ($cond .= " 
                            AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $get_branch = User::where("user_id",$results[0]->branch_id)->get()->first();

                $get_cashiers = User::where("related_id",$results[0]->branch_id)
                    ->where("user_type","branch_admin")->get();
                $get_cashiers = $get_cashiers->all();

                $get_clients = User::where("related_id",$results[0]->branch_id)
                    ->where("user_type","customer")->get();
                $get_clients = $get_clients->all();

                $get_points_of_sale = points_of_sale_m::where("branch_id",$results[0]->branch_id)->get();
                $get_points_of_sale = $get_points_of_sale->all();

                $total_balance_of_point_of_sale = 0;
                if (count($get_points_of_sale))
                {
                    $this->data["points_of_sale"] = $get_points_of_sale;
                    $total_balance_of_point_of_sale = convert_inside_obj_to_arr($get_points_of_sale,"point_of_sale_balance");
                    $total_balance_of_point_of_sale = array_sum($total_balance_of_point_of_sale);
                }

                $this->data["branch_details"] = [
                    "branch_name" => $get_branch->full_name,
                    "branch_balance" => $get_branch->user_balance,
                    "branch_duty_balance" => $get_branch->user_duty_balance,
                    "number_of_cashiers" => count($get_cashiers),
                    "number_of_clients" => count($get_clients),
                    "number_of_point_of_sale" => count($get_points_of_sale),
                    "total_balance_of_point_of_sale" => $total_balance_of_point_of_sale,
                ];





                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_depend_on_branch",$this->data);
    }


    public function sell_bills_depend_on_cashier()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_cashier",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        $this->get_branches_and_other_data();
        $get_cashiers = $this->data["all_customers"];
        if (!count($get_cashiers))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يوجد مستخدميين حاليين !!</div>"
            )->send();
        }

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["user_id"] = "";
        $this->data["cashiers"] = $get_cashiers;
        $this->data["bills"] = [];
        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $user_id = $get_filter_values["user_id"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["user_id"] = $user_id;

            $cond .= " AND bill.cashier_id = $user_id ";

            ($from_date != "")?
                ($cond .= " 
                            AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_depend_on_cashier",$this->data);
    }


    public function sell_bills_depend_on_user_age()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_user_age",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["age_range"] = "";
        $this->data["bills"] = [];
        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $age_range = $get_filter_values["age_range"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["age_range"] = $age_range;

            if($this->current_user_data->user_type=="branch_admin") {
                $cond .= " AND bill.branch_id =".$this->current_user_data->related_id;
            }

            $cond .= " AND client.age_range = '$age_range' ";

            ($from_date != "")?
                ($cond .= " 
                            AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_depend_on_user_age",$this->data);
    }


    public function sell_bills_depend_on_user_gender()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_user_gender",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["user_gender"] = "";
        $this->data["bills"] = [];
        $this->data["sell_products"] = [];
        $this->data["return_products"] = [];


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $user_gender = $get_filter_values["user_gender"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["user_gender"] = $user_gender;

            if($this->current_user_data->user_type=="branch_admin") {
                $cond .= " AND bill.branch_id =".$this->current_user_data->related_id;
            }

            $cond .= " AND client.user_gender = '$user_gender' ";

            ($from_date != "")?
                ($cond .= " 
                            AND bill.client_bill_date >= '$from_date'
                            AND bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_bills_m::get_clients_bills(" $cond ");

            if (count($results))
            {

                $this->data["bills"] = clients_bills_m::get_clients_bills(" $cond order by bill.client_bill_date desc limit 20 ");

                #region get products that client sell and return

                $this->get_products_sell_return_count($results,10);

                #endregion

                #region Get bill Statistics

                $this->get_bills_statistics($results);

                #endregion

            }

        }

        #endregion


        return view("admin.subviews.reports.sell_bills_depend_on_user_gender",$this->data);
    }


    public function sell_bills_depend_on_product()
    {

        if (!check_permission($this->user_permissions,"factory/reports","sell_bills_depend_on_product",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        #region filteration

        $get_products = products_m::all();
        $get_products = $get_products->all();
        if (!count($get_products))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا توجد منتجات حاليين !!</div>"
            )->send();
        }

        $this->data["from_date"] = "";
        $this->data["to_date"] = "";
        $this->data["pro_details"] = [];
        $this->data["product_statistics"] = [];
        $this->data["pro_id"] = "";
        $this->data["products"] = $get_products;


        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $from_date = $get_filter_values["from_date"];
            $to_date = $get_filter_values["to_date"];
            $pro_id = $get_filter_values["pro_id"];

            $this->data["from_date"] = $from_date;
            $this->data["to_date"] = $to_date;
            $this->data["pro_id"] = $pro_id;

            if($this->current_user_data->user_type=="branch_admin") {
                $cond .= " AND bill.branch_id =".$this->current_user_data->related_id;
            }

            $cond .= " AND clients_order.pro_id = $pro_id ";

            ($from_date != "")?
                ($cond .= " 
                            AND client_bill.client_bill_date >= '$from_date'
                            AND client_bill.client_bill_date <= '$to_date' "):($cond .= "");

            $results = clients_orders_m::get_bill_orders(" $cond ");

            if (count($results))
            {
                $this->data["product_statistics"] = [
                    "إجمالي المبيعات" => 0,
                    "إجمالي المرتجع" => 0
                ];

                $this->data["pro_details"] = [
                    "pro_name" => "",
                    "pro_desc" => "",
                    "pro_barcode" => "",
                    "pro_limit" => "",
                    "pro_expire_date" => "",
                    "total_sell_pro_quantity" => 0,
                    "total_return_pro_quantity" => 0,
                    "total_pro_quantity_on_stock" => 0,
                    "total_pro_sell_as_gift_quantity" => 0,
                ];


                foreach($results as $key => $pro)
                {
                    $this->data["pro_details"]["pro_name"] = $pro->pro_name;
                    $this->data["pro_details"]["pro_desc"] = $pro->pro_desc;
                    $this->data["pro_details"]["pro_barcode"] = $pro->pro_barcode;
                    $this->data["pro_details"]["pro_limit"] = $pro->pro_limit;
                    $this->data["pro_details"]["pro_expire_date"] = $pro->pro_expire_date;
                    $this->data["pro_details"]["total_pro_quantity_on_stock"] = $pro->pro_quantity;

                    if ($pro->order_return == 0)
                    {
                        $this->data["pro_details"]["total_sell_pro_quantity"] += $pro->order_quantity;
                        $this->data["product_statistics"]["إجمالي المبيعات"] += $pro->pro_sell_price;
                    }
                    else{
                        $this->data["pro_details"]["total_return_pro_quantity"] += $pro->order_quantity;
                        $this->data["product_statistics"]["إجمالي المرتجع"] += $pro->pro_sell_price;
                    }

                    if ($pro->is_gift == 1)
                    {
                        $this->data["pro_details"]["total_pro_sell_as_gift_quantity"] += $pro->order_quantity;
                    }

                }

            }

        }

        #endregion

        return view("admin.subviews.reports.sell_bills_depend_on_product",$this->data);
    }




    private function get_bills_statistics($bills)
    {

        foreach($bills as $key => $value)
        {

            if (isset($this->data["age_range"][$value->client_age_range]))
            {
                $this->data["age_range"][$value->client_age_range] += $value->client_bill_total_amount;
            }
            if (isset($this->data["user_gender"][$value->client_user_gender]))
            {
                $this->data["user_gender"][$value->client_user_gender] += $value->client_bill_total_amount;
            }


            $this->data["results"]["number_of_bills"] += 1;

            if ($value->is_reviewed == 1)
            {
                $this->data["results"]["number_of_reviewed_bills"] += 1;
            }
            else{
                $this->data["results"]["number_of_waiting_review_bills"] += 1;
            }


            $this->data["results"]["total_amount_of_bills"] += ($value->client_bill_total_amount);
            $this->data["results"]["total_discount_of_bills"] += ($value->bill_coupon_amount);
            $this->data["results"]["total_amount_of_bills_return"] += $value->client_bill_total_return_amount;
            $this->data["results"]["total_amount_of_bills_in_cash"] += $value->client_total_paid_amount_in_cash;
            $this->data["results"]["total_amount_of_bills_in_atm"] += $value->client_total_paid_amount_in_atm;
            $this->data["results"]["total_remain_amount_of_bills"] += $value->client_bill_total_remain_amount;

        }
    }

    private function get_products_sell_return_count($bills , $limit = -1)
    {
        $get_bill_ids = convert_inside_obj_to_arr($bills,"client_bill_id");

        $get_client_orders_sell = clients_orders_m::whereIn("client_bill_id",$get_bill_ids)
            ->where("order_return",0)->get();
        $get_client_orders_sell = $get_client_orders_sell->all();

        $get_client_orders_return = clients_orders_m::whereIn("client_bill_id",$get_bill_ids)
            ->where("order_return",1)->get();
        $get_client_orders_return = $get_client_orders_return->all();

        $total_pro_ids = [];

        if (count($get_client_orders_sell))
        {
            $get_pro_ids_sell = convert_inside_obj_to_arr($get_client_orders_sell,"pro_id");
            $get_pro_ids_sell_unique = array_unique($get_pro_ids_sell);
            $get_pro_ids_sell_unique_with_count = array_count_values($get_pro_ids_sell);
            $total_pro_ids = array_merge($total_pro_ids,$get_pro_ids_sell_unique);
            arsort($get_pro_ids_sell_unique_with_count);
        }

        if (count($get_client_orders_return))
        {
            $get_pro_ids_return = convert_inside_obj_to_arr($get_client_orders_return,"pro_id");
            $get_pro_ids_return_unique = array_unique($get_pro_ids_return);
            $get_pro_ids_return_with_count = array_count_values($get_pro_ids_return);
            $total_pro_ids = array_merge($total_pro_ids,$get_pro_ids_return_unique);
            arsort($get_pro_ids_return_with_count);
        }


        $total_pro_ids = array_unique($total_pro_ids);

        $get_pros = products_m::whereIn("pro_id",$total_pro_ids)->get();
        $get_pros = $get_pros->all();
        $get_pros = collect($get_pros)->groupBy("pro_id");
        $get_pros = $get_pros->all();

        if (count($get_client_orders_sell))
        {
            $first_counter = 0;
            foreach($get_pro_ids_sell_unique_with_count as $key => $pro)
            {

                if ($limit != -1 && ($limit == $first_counter))
                {
                    break;
                }

                if (!isset($get_pros[$key]) || !isset($get_pros[$key][0]))
                {
                    continue;
                }
                $this->data["sell_products"][$get_pros[$key][0]->pro_name] = $pro;

                $first_counter++;
            }
        }

        if (count($get_client_orders_return))
        {
            $second_counter = 0;

            foreach($get_pro_ids_return_with_count as $key => $pro)
            {

                if ($limit != -1 && ($limit == $second_counter))
                {
                    break;
                }

                if (!isset($get_pros[$key]) || !isset($get_pros[$key][0]))
                {
                    continue;
                }
                $this->data["return_products"][$get_pros[$key][0]->pro_name] = $pro;

                $second_counter ++;
            }
        }
    }

    public function show_bill_orders($bill_id){

        if (!check_permission($this->user_permissions,"factory/reports","show_bill_orders",$this->data["current_user"]))
        {
            return Redirect::to('factory/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس لديك الصلاحية للدخول هنا</div>"])->send();
        }

        $this->data["bill_data"]=clients_bills_m::get_clients_bills(" AND bill.client_bill_id=$bill_id");
        if (!isset_and_array($this->data["bill_data"])){
            return Redirect::to('factory/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>حدث خطأ</div>"]
            )->send();
        }
        $this->data["bill_data"]=$this->data["bill_data"][0];

        $this->data["all_bill_orders"]=clients_orders_m::get_bill_orders(" 
            AND client_bill.client_bill_id=$bill_id"
        );


        return view("admin.subviews.reports.show_bill_orders",$this->data);
    }


    #products

    // بيانات الاصناف في المعارض
    public function products_on_branches_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","products_details",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " تقرير بيانات الأصناف ";

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];

        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $branch_id=$request->get("branch_id","0");
        $this->data["branch_id"]="";

        $cond = "";
        $get_cat = "";
        $this->data["all_products"] = [];

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        if(isset($_GET["branch_id"]))
        {
            $this->data["branch_id"]=$branch_id;
            if($this->data["current_user"]->user_type == "admin")
            {

                if($branch_id > 0)
                {
                    $branch_data = \App\User::find($branch_id);
                    $this->data["meta_title"] .= " | ".$branch_data->full_name;
                    $cond .= " AND branch_pro.branch_id=$branch_id ";
                }

            }
            else{

                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;
                $cond .= " AND branch_pro.branch_id=".$this->branch_id;

            }

            $this->data["all_products"] = branches_products_m::get_branches_pros(" $cond ");

        }
        
        if(!empty($get_cat))
        {
            $this->data["meta_title"] .= " | تصنيف ".$get_cat->cat_name;
        }

        return view("admin.subviews.reports.products_on_branches_stock",$this->data);
    }

    // حركه الاصناف
    public function products_most_sell(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","products_sells",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " حركة الأصناف ";

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];


        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $branch_id=$request->get("branch_id","0");
        $this->data["branch_id"]="";

        // price lists
        $this->data["price_id"] = -1;
        $this->data["prices_list"] = price_list_m::get()->all();
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();


        $pro_cond = "";
        $cond = " AND bill.is_reviewed = 1 ";
        $get_cat = "";
        $this->data["all_products"] = [];
        $this->data["from_date"] = "";
        $this->data["to_date"] = "";

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $pro_cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        if(isset($_GET["from_date"]) && !empty($_GET["from_date"]) &&
            isset($_GET["to_date"]) && !empty($_GET["to_date"])
        )
        {
            $from_date = date("Y-m-d",strtotime($_GET["from_date"]));
            $to_date = date("Y-m-d",strtotime($_GET["to_date"]));
            $cond .= " AND date(bill.client_bill_date) >= '$from_date'
                        AND date(bill.client_bill_date) <= '$to_date'
                        ";

            $this->data["meta_title"] .= " | من ".$from_date;
            $this->data["meta_title"] .= " | الي ".$to_date;
        }

        if(isset($_GET["branch_id"]))
        {
            $this->data["branch_id"]=$branch_id;
            if($this->data["current_user"]->user_type == "admin")
            {

                if($branch_id > 0)
                {
                    $branch_data = \App\User::find($branch_id);
                    $this->data["meta_title"] .= " | ".$branch_data->full_name;
                    $cond .= " AND bill.branch_id=$branch_id ";
                }

            }
            else{

                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;
                $cond .= " AND bill.branch_id=".$this->branch_id;

            }

            $all_bills = clients_bills_m::get_clients_bills($cond);

            if (count($all_bills))
            {
                $all_bills_ids = convert_inside_obj_to_arr($all_bills,"client_bill_id");

                $all_bills_orders = [];
                $all_bills_ids = collect($all_bills_ids)->chunk(100);
                foreach($all_bills_ids as $key => $ids)
                {
                    $implode_ids = implode(',',$ids->all());

                    $temp_arr = clients_orders_m::get_bill_orders("
                         $pro_cond
                        and clients_order.client_bill_id in ($implode_ids)
                    ");

                    $all_bills_orders = array_merge($all_bills_orders,$temp_arr);
                }

                $products = [];

                if(count($all_bills_orders))
                {

                $all_bills_orders_chunk = $all_bills_orders;
                $all_bills_orders_chunk = collect($all_bills_orders_chunk)->chunk(100);

                foreach ($all_bills_orders_chunk as $key1 => $all_bills_orders_arr)
                {

                    foreach ($all_bills_orders_arr as $key => $order)
                    {
                        $sell_quantity = 0;
                        $sell_amount = 0;
                        $return_quantity = 0;
                        $return_amount = 0;

                        $sell_gain = 0;
                        $return_gain = 0;

                        if($order->order_return == 1)
                        {
                            $return_quantity = $order->order_quantity;
                            $return_amount = ($return_quantity*$order->pro_sell_price);
                            if(isset($_GET["price_id"]) && $_GET["price_id"] != -1)
                            {
                                $price_id = $_GET["price_id"];
                                $this->data["price_id"] = $price_id;
                                $price_value = collect($prices_values[$order->pro_id])->groupBy("price_id")->all();
                                $price_amount = 0;
                                if(isset($price_value[$price_id]) && isset($price_value[$price_id][0]))
                                {
                                    $price_amount = $price_value[$price_id][0]->ppv_value;
                                }

                                $return_gain = ($return_quantity * $price_amount);
                            }
                            else{
                                $return_gain = ($return_quantity * $order->pro_original_price);
                            }

                        }
                        else{
                            $sell_quantity = $order->order_quantity;
                            $sell_amount = ($sell_quantity*$order->pro_sell_price);
                            if(isset($_GET["price_id"]) && $_GET["price_id"] != -1)
                            {
                                $price_id = $_GET["price_id"];
                                $price_value = collect($prices_values[$order->pro_id])->groupBy("price_id")->all();
                                $price_amount = 0;
                                if(isset($price_value[$price_id]) && isset($price_value[$price_id][0]))
                                {
                                    $price_amount = $price_value[$price_id][0]->ppv_value;
                                }

                                $sell_gain = ($sell_quantity * $price_amount);
                            }
                            else{
                                $sell_gain = ($sell_quantity * $order->pro_original_price);
                            }
                        }

                        if(isset($products[$order->pro_id]))
                        {
                            $products[$order->pro_id]=
                            [
                                "pro_name" => $order->pro_name,
                                "pro_barcode" => $order->pro_barcode,
                                "sell_quantity" => ($products[$order->pro_id]["sell_quantity"]+$sell_quantity),
                                "sell_amount" => ($products[$order->pro_id]["sell_amount"]+$sell_amount),
                                "return_quantity" => ($products[$order->pro_id]["return_quantity"]+$return_quantity),
                                "return_amount" => ($products[$order->pro_id]["return_amount"]+$return_amount),
                                "sell_gain" => ($products[$order->pro_id]["sell_gain"]+$sell_gain),
                                "return_gain" => ($products[$order->pro_id]["return_gain"]+$return_gain),
                            ];
                        }
                        else{
                            $products[$order->pro_id] =
                            [
                                "pro_name" => $order->pro_name,
                                "pro_barcode" => $order->pro_barcode,
                                "sell_quantity" => $sell_quantity,
                                "sell_amount" => ($sell_amount ),
                                "return_quantity" => $return_quantity,
                                "return_amount" => ($return_amount),
                                "sell_gain" => ($sell_gain),
                                "return_gain" => ($return_gain),
                            ];
                        }

                    }

                }

                }

                $this->data["all_products"] = $products;
//                dump($products);
//                die();

            }

        }

        return view("admin.subviews.reports.products_most_sell",$this->data);
    }

    // بيانات الاصناف في الفرع الرئيسي
    public function products_on_factory_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","products_on_factory_details",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " تقرير الأصناف في الفرع الرئيسي ";


        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $cond = "";
        $get_cat = "";
        $this->data["all_products"] = [];

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        $this->data["all_products"] = products_m::get_pros($cond);

        if(!empty($get_cat))
        {
            $this->data["meta_title"] .= " | تصنيف ".$get_cat->cat_name;
        }

        return view("admin.subviews.reports.products_on_factory_stock",$this->data);
    }

    // ميزان مراجعه الاصناف
    public function review_products_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","review_products_stock",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " ميزان مراجعه الأصناف ";

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];

        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $branch_id=$request->get("branch_id","0");

        // price lists
        $this->data["price_ids"] = [""];
        $prices_list = price_list_m::get()->all();
        $this->data["prices_list"] = $prices_list;
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();

        $this->data["branch_id"] = "";
        $cond = "";
        $get_cat = "";
        $this->data["table_headers"] = [
            "",
            "الفرع",
            "التصنيف",
            "المنتج",
            "الباركود",
            "الكمية",
            "حد الطلب"
        ];
        $this->data["all_products"] = [];

        $price_ids = [];
        if(isset($_GET["price_id"]) && count($_GET["price_id"]))
        {
            $price_ids = $_GET["price_id"];
            $price_ids = array_values($price_ids);
            $this->data["price_ids"] = $price_ids;
            $price_titles = [];
            foreach ($prices_list as $key => $price_item)
            {
                if(in_array($price_item->price_id,$price_ids))
                {
                    $price_titles[] = $price_item->price_title;
                }
            }
            $this->data["table_headers"] = array_merge($this->data["table_headers"],$price_titles);
        }
//        dump($this->data["table_headers"]);

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        if(isset($_GET["branch_id"]))
        {
            $this->data["branch_id"]=$branch_id;
            if($this->data["current_user"]->user_type == "admin")
            {
                if($branch_id == -1)
                {
                    $this->data["meta_title"] .= " | "."الفرع الرئيسي";
                    $all_products = products_m::get_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="products",$products=$all_products,$branch="الفرع الرئيسي",$prices_values,$price_ids);
                }
                elseif($branch_id == 0)
                {
                    $this->data["meta_title"] .= " | "." كل المعارض ";
                    $all_products = branches_products_m::get_branches_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
                }
                elseif($branch_id > 0)
                {
                    $branch_data = \App\User::find($branch_id);
                    $this->data["meta_title"] .= " | ".$branch_data->full_name;
                    $cond .= " AND branch_pro.branch_id=$branch_id ";
                    $all_products = branches_products_m::get_branches_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
                }

            }
            else{

                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;
                $cond .= " AND branch_pro.branch_id=".$this->branch_id;
                $all_products = branches_products_m::get_branches_pros(" $cond ");
                $this->data["all_products"] = $this->return_products_row(
                    $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
            }

        }

        return view("admin.subviews.reports.review_products_stock",$this->data);
    }

    //  خاصه بميزان مراجعه الاصناف
    private function return_products_row($table,$products,$branch,$prices_values,$price_ids)
    {

        $all_pro = [];
        if($table == "products")
        {
            foreach ($products as $key => $pro)
            {

                $price_cols = [];
                foreach($price_ids as $key2 => $price_id)
                {
                    $price_value = collect($prices_values[$pro->pro_id])->groupBy("price_id")->all();
                    if(isset($price_value[$price_id]) && isset($price_value[$price_id][0]))
                    {
                        $price_cols[] = ($price_value[$price_id][0]->ppv_value*$pro->pro_quantity);
                    }
                }

                $all_pro[] = array_merge([
                    "",
                    $branch,
                    $pro->cat_name,
                    $pro->pro_name,
                    $pro->pro_barcode,
                    $pro->pro_quantity,
                    $pro->pro_limit
                ],$price_cols);

            }

        }
        else{

            foreach ($products as $key => $pro)
            {

                $branch_name = "";
                if($branch == "self")
                {
                    $branch_name = $pro->full_name;
                }
                else{
                    $branch_name = $branch;
                }

                $price_cols = [];
                foreach($price_ids as $key2 => $price_id)
                {
                    $price_value = collect($prices_values[$pro->pro_id])->groupBy("price_id")->all();
                    if(isset($price_value[$price_id]) && isset($price_value[$price_id][0]))
                    {
                        $price_cols[] = ($price_value[$price_id][0]->ppv_value*$pro->b_p_quantity);
                    }
                }

                $all_pro[] = array_merge([
                    "",
                    $branch_name,
                    $pro->cat_name,
                    $pro->pro_name,
                    $pro->pro_barcode,
                    $pro->b_p_quantity,
                    $pro->b_p_quantity_limit
                ],$price_cols);

            }

        }
        

        return $all_pro;
    }

    // ربحية الاصناف
    public function gain_products_stock(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","gain_products_stock",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " ربحية الأصناف ";

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];

        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $branch_id=$request->get("branch_id","0");

        // price lists
        $this->data["price_ids"] = [""];
        $prices_list = price_list_m::get()->all();
        $this->data["prices_list"] = $prices_list;
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();

        $this->data["branch_id"] = "";
        $cond = "";
        $get_cat = "";
        $this->data["table_headers"] = [
            "",
            "الفرع",
            "التصنيف",
            "المنتج",
            "الباركود",
            "الكمية",
            "حد الطلب"
        ];
        $this->data["all_products"] = [];

        $price_ids = [];
        if(isset($_GET["price_id"]) && count($_GET["price_id"]))
        {
            $price_ids = $_GET["price_id"];
            $price_ids = array_values($price_ids);
            $this->data["price_ids"] = $price_ids;
            $price_titles = [];
            foreach ($prices_list as $key => $price_item)
            {
                if(in_array($price_item->price_id,$price_ids))
                {
                    $price_titles[] = $price_item->price_title;
                }
            }
            $this->data["table_headers"] = array_merge($this->data["table_headers"],$price_titles);
        }
//        dump($this->data["table_headers"]);

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        if(isset($_GET["branch_id"]))
        {
            $this->data["branch_id"]=$branch_id;
            if($this->data["current_user"]->user_type == "admin")
            {
                if($branch_id == -1)
                {
                    $this->data["meta_title"] .= " | "."الفرع الرئيسي";
                    $all_products = products_m::get_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="products",$products=$all_products,$branch="الفرع الرئيسي",$prices_values,$price_ids);
                }
                elseif($branch_id == 0)
                {
                    $this->data["meta_title"] .= " | "." كل المعارض ";
                    $all_products = branches_products_m::get_branches_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
                }
                elseif($branch_id > 0)
                {
                    $branch_data = \App\User::find($branch_id);
                    $this->data["meta_title"] .= " | ".$branch_data->full_name;
                    $cond .= " AND branch_pro.branch_id=$branch_id ";
                    $all_products = branches_products_m::get_branches_pros(" $cond ");
                    $this->data["all_products"] = $this->return_products_row(
                        $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
                }

            }
            else{

                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;
                $cond .= " AND branch_pro.branch_id=".$this->branch_id;
                $all_products = branches_products_m::get_branches_pros(" $cond ");
                $this->data["all_products"] = $this->return_products_row(
                    $table="branches_products",$products=$all_products,$branch="self",$prices_values,$price_ids);
            }

        }

        return view("admin.subviews.reports.gain_products_stock",$this->data);
    }

    #endregion


    #region commissions

    public function commissions_report(Request $request)
    {
        if (!check_permission($this->user_permissions,"factory/reports","commissions_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_commissions"] = [];
        $this->data["start_date"] = date("Y-m-d");
        $this->data["end_date"] = date("Y-m-d");
        $this->data["selected_user"] = "";
        $this->data["meta_title"] = " عمولات الفروع ";

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_users"] = \App\User::get_users(" AND (u.user_type = 'branch' ) ");
        }
        else{
            $branch_id = $this->data["current_user"]->related_id;
            $this->data["all_users"] = \App\User::get_users(" AND (u.user_id = $branch_id ) ");
        }


        if(isset($_GET["from_date"]) && isset($_GET["to_date"]) && isset($_GET["branch_id"]))
        {

            $from_date = $_GET["from_date"];
            $to_date = $_GET["to_date"];
            $this->data["meta_title"] .= " | من ".$from_date;
            $this->data["meta_title"] .= " | الي ".$to_date;

            if($this->data["current_user"]->user_type == "admin")
            {
                $branch_id = $_GET["branch_id"];
            }

            $this->data["start_date"] = clean($from_date);
            $this->data["end_date"] = clean($to_date);
            $this->data["selected_user"] = clean($branch_id);

            $cond = "";
            if ($this->data["selected_user"] > 0)
            {
                $cond = " AND commission.user_id = $branch_id ";
                $branch_data = \App\User::find($branch_id);
                $this->data["meta_title"] .= " | ".$branch_data->full_name;
            }
            else{
                $this->data["meta_title"] .= " | "."كل الفروع";
            }

            $this->data["all_commissions"] = day_commission_m::get_data(" 
                $cond
                AND commission.day_date >= '$from_date'
                AND commission.day_date <= '$to_date' 
                order by commission.day_date desc
            ");

        }


        return view("admin.subviews.reports.commissions_report",$this->data);
    }

    #endregion


    #region daily bills

    public function daily_bills(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","daily_bills",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " يومية الفواتير ";


        $this->get_branches_and_other_data();

        $branch_id=$request->get("select_branch_id","");
        $customer_id=$request->get("customer_id","");
        $branch_admin_id=$request->get("select_branch_admin_id","");
        $from_date=$request->get("from_date","");
        $to_date=$request->get("to_date","");


        $this->data["request_data"]=(object)$request->all();

        $bill_objs=clients_bills_m::whereNull("deleted_at");

        $cond_arr=[""];

        if(!empty($branch_id)){
            $bill_objs=$bill_objs->where("branch_id",$branch_id);
            $branch_data = \App\User::find($branch_id);
            $this->data["meta_title"] .= " | ".$branch_data->full_name;
        }
        elseif($this->data["current_user"]->user_type == "branch_admin")
        {
            $current_branch_id = $this->data["current_user"]->related_id;
            $bill_objs=$bill_objs->where("branch_id",$current_branch_id);

            $branch_data = \App\User::find($current_branch_id);
            $this->data["meta_title"] .= " | ".$branch_data->full_name;
        }

        if(!empty($branch_admin_id)){
            $bill_objs=$bill_objs->where("cashier_id",$branch_admin_id);

            $cashier_data = \App\User::find($branch_admin_id);
            $this->data["meta_title"] .= " | كاشيير ".$cashier_data->full_name;
        }

        if(!empty($customer_id)){
            $bill_objs=$bill_objs->where("customer_id",$customer_id);
            $customer_data = \App\User::find($customer_id);
            $this->data["meta_title"] .= " | عميل ".$customer_data->full_name;
        }

        $this->data["bill_date_from"]=Carbon::now();
        if(!empty($from_date)){
            $from_date=date('Y-m-d',strtotime($from_date));
            $this->data["bill_date_from"]="$from_date";
            $bill_objs=$bill_objs->where("client_bill_date",">=",$from_date);
            $this->data["meta_title"] .= " | من ".$from_date;
        }

        $this->data["bill_date_to"]=Carbon::now();
        if(!empty($to_date)){
            $to_date=date('Y-m-d',strtotime($to_date));
            $this->data["bill_date_to"]="$to_date";
            $bill_objs=$bill_objs->where("client_bill_date","<=",$to_date);
            $this->data["meta_title"] .= " | الي ".$to_date;
        }

        //get all clients_bills
        $this->data["all_bills"]=[];
        if (count($_GET)){
            $bill_objs = $bill_objs->get();
            if(count($bill_objs->all()))
            {
                $this->data["all_bills"]=clients_bills_m::get_clients_bills(" AND bill.client_bill_id in (".implode(",",$bill_objs->pluck("client_bill_id")->all()).")");
            }
        }

        return view("admin.subviews.reports.daily_bills",$this->data);

    }

    #endregion


    // العهد المستلمة
    public function duty_report(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","duty_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["meta_title"] = " العهد ";
        $this->data["documents"] = [];
        $cond = "";

        $this->get_branches_and_other_data();

        if(count($_GET))
        {
            $duty_type = $_GET['duty_type'];
            $select_branch_id = $_GET['select_branch_id'];
            $user_id = "";
            $from_date = $_GET['from_date'];
            $to_date = $_GET['to_date'];

            if($select_branch_id > 0)
            {

                if(isset($_GET['select_branch_admin_id']) && $_GET['select_branch_admin_id'] > 0)
                {
                    $user_id = $_GET['select_branch_admin_id'];
                }
                else{
                    $get_branch = \App\User::findOrFail($select_branch_id);
                    $get_all_branch_admins = \App\User::get_users(" AND u.user_type='branch_admin' AND u.related_id = $select_branch_id ");
                    if(count($get_all_branch_admins))
                    {
                        $get_all_branch_admins_ids = convert_inside_obj_to_arr($get_all_branch_admins,"user_id");
                        $get_all_branch_admins_ids = implode(",",$get_all_branch_admins_ids);

                        if ($duty_type == -1)
                        {
                            $cond .= " AND (duty.from_user_id in ($get_all_branch_admins_ids) OR duty.to_user_id in ($get_all_branch_admins_ids) ) ";
                            $this->data["meta_title"] .= " | "." المستلمة والمرسلة "." | "." ل ".$get_branch->full_name;
                        }
                        else if ($duty_type == 1)
                        {
                            $cond .= " AND duty.to_user_id in ($get_all_branch_admins_ids) ";
                            $this->data["meta_title"] .= " | "." المستلمة "." | "." ل ".$get_branch->full_name;
                        }
                        else if ($duty_type == 0)
                        {
                            $cond .= " AND duty.from_user_id in ($get_all_branch_admins_ids) ";
                            $this->data["meta_title"] .= " | "." المرسلة "." | "." ل ".$get_branch->full_name;
                        }
                    }
                }

            }

            if(!empty($user_id))
            {
                $get_user = \App\User::findOrFail($user_id);

                if ($duty_type == -1)
                {
                    $cond .= " AND (duty.from_user_id = $user_id OR duty.to_user_id = $user_id) ";
                    $this->data["meta_title"] .= " | "." المستلمة والمرسلة "." | "." ل ".$get_user->full_name;
                }
                else if ($duty_type == 1)
                {
                    $cond .= " AND duty.to_user_id = $user_id ";
                    $this->data["meta_title"] .= " | "." المستلمة "." | "." ل ".$get_user->full_name;
                }
                else if ($duty_type == 0)
                {
                    $cond .= " AND duty.from_user_id = $user_id ";
                    $this->data["meta_title"] .= " | "." المرسلة "." | "." ل ".$get_user->full_name;
                }

            }

            if(!empty($from_date))
            {
                $from_date = date("Y-m-d",strtotime($from_date));
                $cond .= " AND date(duty_document_date) >= '$from_date' ";
                $this->data["meta_title"] .= " | "." من ".$from_date;
            }

            if(!empty($to_date))
            {
                $to_date = date("Y-m-d",strtotime($to_date));
                $cond .= " AND date(duty_document_date) <= '$to_date' ";
                $this->data["meta_title"] .= " | "." الي ".$to_date;
            }

            $documents = duty_documents_m::get_data(" $cond ");

            $this->data["documents"] = $documents;
        }

        return view("admin.subviews.reports.duty_report",$this->data);
    }


    // كشف حساب مصروف تفصيلي
    public function expenses_report(Request $request)
    {
        if (!check_permission($this->user_permissions,"factory/reports","expenses_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["meta_title"] = " كشف حساب مصروف تفصيلي ";
        $this->data["all_expenses_rows"] = [];
        $cond_arr = [];
        $this->data["expenses_date_from"] = 0;
        $this->data["expenses_date_to"] = 0;
        $cond = "";

        $this->get_branches_and_other_data();

        $this->show_category_tree(true,"expense");

        $user_id=$request->get("user_id",0);// branch
        $expenses_maker=$request->get("expenses_maker",0);
        $expenses_type_id=$request->get("expenses_type_id","");
        $expenses_date_from=$request->get("expenses_date_from","");
        $expenses_date_to=$request->get("expenses_date_to","");


        $this->data["group_by"]="";

        $this->data["all_expenses_types"]=category_m::where("cat_type","expense")->get()->all();

        $this->data["select_expenses_type"]="0";

        if(count($_GET)) {

            if($expenses_maker > 0)
            {
                $cond_arr[] = " AND ex.expenses_maker = $expenses_maker ";
                $get_cashier = \App\User::findOrFail($expenses_maker);
                $this->data["meta_title"] .= " | للمستخدم ".$get_cashier->full_name;
            }

            if ($expenses_type_id > 0) {

                //get all cat childs of $expenses_type_id
                $all_cats_ids = category_m::get_all_child_cats([$expenses_type_id]);

                if (isset_and_array($all_cats_ids)) {
                    $cond_arr[] = " AND ex.expense_type_id in (" . implode(",", $all_cats_ids) . ")";
                }


                $this->data["expense_cat_data"] = category_m::find($expenses_type_id);
                $this->data["select_expenses_type"] = $expenses_type_id;
                $this->data["meta_title"] .= " | تصنيف : ".$this->data["expense_cat_data"]->cat_name;
            }

            if ($expenses_date_from != "" && $expenses_date_from != 0) {
                $cond_arr[] = " AND ex.expense_date >='$expenses_date_from'";
                $this->data["expenses_date_from"] = $expenses_date_from;
                $this->data["meta_title"] .= " | من ".$expenses_date_from;
            }

            if ($expenses_date_to != "" && $expenses_date_to != 0) {
                $cond_arr[] = " AND ex.expense_date <= '$expenses_date_to'";
                $this->data["expenses_date_to"] = $expenses_date_to;
                $this->data["meta_title"] .= " | الي ".$expenses_date_to;
            }

            if ($this->data["current_user"]->user_type != "admin") {
                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;

                $cond_arr[] = " AND ex.user_id = $this->branch_id ";
            } else {

                if ($user_id > 0) {
                    $get_branch_data = \App\User::findOrFail($user_id);
                    $this->data["meta_title"] .= " | ".$get_branch_data->full_name;

                    $cond_arr[] = " AND ex.user_id = $user_id ";
                }
            }

            $this->data["all_expenses_rows"] = expenses_m::get_expenses(implode(" ", $cond_arr));
            if (is_object($this->data["all_expenses_rows"])) {
                $this->data["all_expenses_rows"] = [$this->data["all_expenses_rows"]];
            }

        }

        return view("admin.subviews.reports.expenses_report",$this->data);
    }

    
    // ميزان مراجعه المصروفات
    public function review_expenses_report(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/reports","review_expenses_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["meta_title"] = " ميزان مراجعه المصروفات ";
        $this->data["all_expenses_rows"] = [];
        $cond_arr = [];
        $this->data["expenses_date_from"] = 0;
        $this->data["expenses_date_to"] = 0;

        $duty_cond = "";

        $expenses_date_from = $request->get("expenses_date_from","");
        $expenses_date_to = $request->get("expenses_date_to","");

        $this->get_branches_and_other_data();

        if(count($_GET))
        {

            if ($expenses_date_from != "" && $expenses_date_from != 0) {
                $cond_arr[] = " AND ex.expense_date >='$expenses_date_from'";
                $this->data["expenses_date_from"] = $expenses_date_from;
                $this->data["meta_title"] .= " | من ".$expenses_date_from;
                $duty_cond .= " AND date(duty.duty_document_date) >= '$expenses_date_from' ";
            }

            if ($expenses_date_to != "" && $expenses_date_to != 0) {
                $cond_arr[] = " AND ex.expense_date <= '$expenses_date_to'";
                $this->data["expenses_date_to"] = $expenses_date_to;
                $this->data["meta_title"] .= " | الي ".$expenses_date_to;
                $duty_cond .= " AND date(duty.duty_document_date) <= '$expenses_date_to' ";
            }
        }

        if ($this->data["current_user"]->user_type != "admin") {
            $this->data["meta_title"] .= " | ".$this->branch_data->full_name;

            $cond_arr[] = " AND ex.user_id = $this->branch_id ";
        }

        $all_expenses_rows = expenses_m::get_expenses(implode(" ", $cond_arr));
        if (is_object($all_expenses_rows))
        {
            $all_expenses_rows = [$all_expenses_rows];
        }

        if(count($all_expenses_rows))
        {
            foreach ($all_expenses_rows as $key => $item)
            {
                
                if(isset($this->data["all_expenses_rows"][$item->user_id]))
                {
                    $this->data["all_expenses_rows"][$item->user_id]["total_expenses"] = 
                        ($this->data["all_expenses_rows"][$item->user_id]["total_expenses"] + $item->expense_amount);
                }
                else{

                    $total_received_duty = 0;
                    $total_current_duty = 0;

                    // get cashiers of this branch from expenses
                    $get_cashiers = expenses_m::where("user_id",$item->user_id)->get()->all();

                    if(count($get_cashiers))
                    {
                        $get_cashiers_ids = convert_inside_obj_to_arr($get_cashiers,"expenses_maker");
                        $get_cashiers_ids = array_unique($get_cashiers_ids);

                        $get_cashiers_ids_implode = implode(",",$get_cashiers_ids);
                        $duty_cond .= " AND duty.to_user_id in($get_cashiers_ids_implode) AND duty.duty_document_received = 1 ";

                        $get_duty = duty_documents_m::get_data(" $duty_cond ");
                        if(count($get_duty))
                        {
                            $get_duty_money = convert_inside_obj_to_arr($get_duty,"duty_document_money");
                            $get_duty_money = array_sum($get_duty_money);
                            $total_received_duty = $get_duty_money;
                        }

                        // get total_current_duty
                        $get_cashiers_duty = \App\User::whereIn("user_id",$get_cashiers_ids)->get()->all();
                        if(count($get_cashiers_duty))
                        {
                            $get_cashiers_duty_money = convert_inside_obj_to_arr($get_cashiers_duty,"user_duty_balance");
                            $total_current_duty = array_sum($get_cashiers_duty_money);
                        }

                    }

                    $this->data["all_expenses_rows"][$item->user_id] =
                        [
                            "branch_name" => $item->branch_full_name,
                            "total_expenses" => $item->expense_amount,
                            "total_received_duty" => $total_received_duty,
                            "total_current_duty" => $total_current_duty
                        ];
                }

            }
        }

        return view("admin.subviews.reports.review_expenses_report",$this->data);
    }


    // تقرير عن الهالك من المنتجات
    public function broken_products(Request $request)
    {
        if (!check_permission($this->user_permissions,"factory/reports","broken_products",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = " الهالك من المنتجات ";

        $this->get_branches_and_other_data();
        $this->data["branches"] = $this->data["all_branches"];

        $this->pre_call_show_category_tree();
        $this->show_category_tree(true,"product");

        $branch_id=$request->get("branch_id","0");

        // price lists
        $this->data["price_ids"] = [""];
        $prices_list = price_list_m::get()->all();
        $this->data["prices_list"] = $prices_list;
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();

        $this->data["branch_id"] = "";
        $cond = "";
        $get_cat = "";
        $this->data["table_headers"] = [
            "",
            "التصنيف",
            "المنتج",
            "الباركود",
            "الكمية",
            "التاريخ"
        ];
        $this->data["all_products"] = [];

        $price_ids = [];
        if(isset($_GET["price_id"]) && !empty($_GET["price_id"]))
        {
            $price_ids = [$_GET["price_id"]];
            $price_ids = array_values($price_ids);
            $this->data["price_ids"] = $price_ids;
            $price_titles = [];
            foreach ($prices_list as $key => $price_item)
            {
                if(in_array($price_item->price_id,$price_ids))
                {
                    $price_titles[] = $price_item->price_title;
                }
            }
            $this->data["table_headers"] = array_merge($this->data["table_headers"],$price_titles);
        }
//        dump($this->data["table_headers"]);

        if(isset($_GET["cat_ids"]) && !empty($_GET["cat_ids"]))
        {
            $cat_ids = [$_GET["cat_ids"]];
            //get all cat childs
            $cat_ids = category_m::get_all_child_cats($cat_ids);
            $cat_ids = implode(",",$cat_ids);

            $cond .= " AND pro.cat_id in ($cat_ids)";
            $get_cat = category_m::find($_GET["cat_ids"]);
            $this->data["meta_title"] .= " | تصنيف : ".$get_cat->cat_name;
        }

        $from_date = $request->get("from_date","");
        $to_date = $request->get("to_date","");

        if(!empty($from_date))
        {
            $from_date = date("Y-m-d",strtotime($from_date));
            $cond .= " AND bro_pro.b_p_date >= '$from_date' ";
            $this->data["meta_title"] .= " | "." من ".$from_date;
        }

        if(!empty($to_date))
        {
            $to_date = date("Y-m-d",strtotime($to_date));
            $cond .= " AND bro_pro.b_p_date <= '$to_date' ";
            $this->data["meta_title"] .= " | "." الي ".$to_date;
        }

        if(isset($_GET["branch_id"]))
        {
            $this->data["branch_id"]=$branch_id;
            if($this->data["current_user"]->user_type == "admin")
            {
                if($branch_id == -1)
                {
                    $cond .= " AND bro_pro.user_id = 0 ";
                    $this->data["meta_title"] .= " | "."الفرع الرئيسي";
                }
                elseif($branch_id == 0)
                {
                    $cond .= " AND bro_pro.user_id > 0 ";
                    $this->data["meta_title"] .= " | "." كل المعارض ";
                }
                elseif($branch_id > 0)
                {
                    $cond .= " AND bro_pro.user_id = $branch_id ";
                    $branch_data = \App\User::find($branch_id);
                    $this->data["meta_title"] .= " | ".$branch_data->full_name;
                }

            }
            else{

                $cond .= " AND bro_pro.user_id = $this->branch_id ";
                $this->data["meta_title"] .= " | ".$this->branch_data->full_name;
            }

            $all_products = broken_products_m::get_data(" $cond ");
            $this->data["all_products"] = $this->return_broken_products_row($products=$all_products,$prices_values,$price_ids);

        }

        return view("admin.subviews.reports.broken_products",$this->data);
    }


    //  خاص بالهالك
    private function return_broken_products_row($products,$prices_values,$price_ids)
    {

        $all_pro = [];
        foreach ($products as $key => $pro)
        {

            $price_cols = [];
            foreach($price_ids as $key2 => $price_id)
            {
                $price_value = collect($prices_values[$pro->pro_id])->groupBy("price_id")->all();
                if(isset($price_value[$price_id]) && isset($price_value[$price_id][0]))
                {
                    $price_cols[] = ($price_value[$price_id][0]->ppv_value*$pro->b_p_quantity);
                }
            }

            $all_pro[] = array_merge([
                "",
                $pro->cat_name,
                $pro->pro_name,
                $pro->pro_barcode,
                $pro->b_p_quantity,
                $pro->b_p_date
            ],$price_cols);

        }


        return $all_pro;
    }


    //  تقرير مبيعات الكوبونات
    public function coupons_report(Request $request)
    {
        if (!check_permission($this->user_permissions,"factory/reports","coupons_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_post_data"]=(object)$request->all();
        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();
        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;

        $this->data["meta_title"]="تقرير الكوبونات ";

        // price lists
        $this->data["price_ids"] = [""];
        $prices_list = price_list_m::get()->all();
        $this->data["prices_list"] = $prices_list;
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();

        $this->data["all_post_data"]=(object)$request->all();

        $this->data["all_coupons_cats"]=category_m::where("cat_type","coupon")->get()->groupBy("cat_id")->all();

        $output_data = [];

        if($request->method()=="GET")
        {

            $client_bill_used_coupon=
                client_bill_used_coupon_m::
                join("clients_bills","clients_bills.client_bill_id","=","client_bill_used_coupon.client_bill_id")->
                where("cbuc_id",">","0");


            if(($branch_id=$request->get("branch_id",""))!=""){
                $this->data["meta_title"].=" - "." فرع ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
                $client_bill_used_coupon=$client_bill_used_coupon->where("clients_bills.branch_id",$branch_id);
            }


            if(($start_date=$request->get("start_date",""))!=""){
                $this->data["meta_title"]." - "." من التاريخ ".date("Y-m-d",strtotime($start_date));
                $client_bill_used_coupon=$client_bill_used_coupon->where("used_at",">=",date("Y-m-d",strtotime($start_date)));
            }

            if(($end_date=$request->get("end_date",""))!=""){
                $this->data["meta_title"]." - "." الي التاريخ ".date("Y-m-d",strtotime($end_date));
                $client_bill_used_coupon=$client_bill_used_coupon->where("used_at","<=",date("Y-m-d",strtotime($end_date)));
            }

            $client_bill_used_coupon=$client_bill_used_coupon->get()->groupBy("coupon_cat_id")->all();
            $all_coupons_cats = $this->data["all_coupons_cats"];


            foreach($client_bill_used_coupon as $key => $coupon_cat)
            {
                $coupon_branches=$coupon_cat->groupBy("branch_id")->all();

                foreach($coupon_branches as $key2 => $coupon_branch)
                {


                    if($coupon_branch->count()==0)continue;
                    if(!isset($all_branches_grouped_by_id[$coupon_branch[0]->branch_id]))continue;
                    if(!isset($all_coupons_cats[$coupon_branch[0]->coupon_cat_id]))continue;

                    $branch_full_name=$all_branches_grouped_by_id[$coupon_branch[0]->branch_id][0]->full_name;
                    $cat_name=$all_coupons_cats[$coupon_branch[0]->coupon_cat_id][0]->cat_name;
                    

                    $get_bills_ids = [];
                    $total_after_discount = 0;
                    $total_before_discount = 0;
                    $total_discount = 0;
                    $total_gain = 0;

                    $total_after_amount = [];
                    $total_before_amount = [];
                    $total_discount_amount = [];

                    foreach($coupon_branch as $key3 => $bill)
                    {
                        $get_bills_ids[] = $bill->client_bill_id;
                        $total_after_amount[$bill->client_bill_id] = ($bill->client_bill_total_amount - $bill->client_bill_total_return_amount);
                        $total_before_amount[$bill->client_bill_id] = (($bill->client_bill_total_amount + $bill->bill_coupon_amount) - $bill->client_bill_total_return_amount);
                        $total_discount_amount[$bill->client_bill_id] = $bill->bill_coupon_amount;
                    }

                    $total_after_discount = array_sum(array_values($total_after_amount));
                    $total_before_discount = array_sum(array_values($total_before_amount));
                    $total_discount = array_sum(array_values($total_discount_amount));

                    $get_bills_ids = array_unique($get_bills_ids);

                    $get_orders = clients_orders_m::whereIn("client_bill_id",$get_bills_ids)->get()->all();
                    $total_products_sells = 0;
                    $total_products_returns = 0;
                    $total_price_value = 0;
                    foreach ($get_orders as $key3 => $order)
                    {
                        if (isset($_GET['price_id']) && isset($prices_values[$order->pro_id])
                            && count($prices_values[$order->pro_id]) )
                        {
                            $prices_values[$order->pro_id] = $prices_values[$order->pro_id];
                            $get_pro_prices = collect($prices_values[$order->pro_id])->groupBy('price_id')->all();
                            if(isset($get_pro_prices[$_GET['price_id']]))
                            {
                                $total_price_value += ($get_pro_prices[$_GET['price_id']][0]->ppv_value * $order->order_quantity);
                            }
                        }

                        if ($order->order_return == 0)
                        {
                            $total_products_sells += ($order->pro_sell_price * $order->order_quantity);
                        }
                        else{
                            $total_products_returns += ($order->pro_sell_price * $order->order_quantity);
                        }

                    }

                    $total_gain = (($total_products_sells - $total_products_returns) - $total_price_value);
                    if(!isset($_GET['price_id']))
                    {
                        $total_gain = 'إختار سعر';
                    }

                    $output_data[] = [
                        "#" => "",
                        "branch_name" =>$branch_full_name,
                        "cat_name" =>$cat_name,
                        "coupons_count" =>$coupon_branch->count(),
                        "bills_count" =>count($get_bills_ids),
                        "total_bills_before_discount" =>$total_before_discount,
                        "total_bills_after_discount" =>$total_after_discount,
                        "total_bills_discount" =>$total_discount,
                        "total_gain" => $total_gain,
                    ];


                }

            }
            $this->data['output_data'] = $output_data;

        }

//        dump($this->data["output_data"]);

        return view("admin.subviews.reports.coupons_report",$this->data);
    }

    //  تقرير مبيعات العروض
    public function packages_report(Request $request)
    {
      if (!check_permission($this->user_permissions,"factory/reports","packages_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_post_data"]=(object)$request->all();
        $all_branches_grouped_by_id=collect($this->data["all_branches"])->groupBy("user_id")->all();
        $this->data["all_branches_grouped_by_id"]=$all_branches_grouped_by_id;

        $this->data["meta_title"]="تقرير العروض ";

        // price lists
        $this->data["price_ids"] = [""];
        $prices_list = price_list_m::get()->all();
        $this->data["prices_list"] = $prices_list;
        $prices_values = product_prices_values_m::get()->all();
        $prices_values = collect($prices_values)->groupBy("pro_id")->all();

        $output_data = [];
        
        $this->data["all_post_data"]=(object)$request->all();

        $this->data["all_packages"]=packages_m::all()->groupBy("package_id")->all();
        $all_packages_cats = $this->data["all_packages"];

        if($request->method()=="GET"){
            $client_bill_used_package=
                client_bill_used_package_m::
                join("clients_bills","clients_bills.client_bill_id","=","client_bill_used_package.client_bill_id")->
                where("cbup_id",">","0");


            if(($branch_id=$request->get("branch_id",""))!=""){
                $this->data["meta_title"].=" - "." فرع ".$all_branches_grouped_by_id[$branch_id][0]->full_name;
                $client_bill_used_package=$client_bill_used_package->where("clients_bills.branch_id",$branch_id);
            }


            if(($start_date=$request->get("start_date",""))!=""){
                $this->data["meta_title"]." - "." من التاريخ ".date("Y-m-d",strtotime($start_date));
                $client_bill_used_package=$client_bill_used_package->where("used_at",">=",date("Y-m-d",strtotime($start_date)));
            }

            if(($end_date=$request->get("end_date",""))!=""){
                $this->data["meta_title"]." - "." الي التاريخ ".date("Y-m-d",strtotime($end_date));
                $client_bill_used_package=$client_bill_used_package->where("used_at","<=",date("Y-m-d",strtotime($end_date)));
            }

            $client_bill_used_package=$client_bill_used_package->get()->groupBy("package_id")->all();
//            dump($client_bill_used_package[11]);
            
            foreach($client_bill_used_package as $key => $package_cat)
            {
                $package_branches=$package_cat->groupBy("branch_id")->all();

                foreach($package_branches as $key2 => $package_branch)
                {


                    if($package_branch->count()==0)continue;
                    if(!isset($all_branches_grouped_by_id[$package_branch[0]->branch_id]))continue;
                    if(!isset($all_packages_cats[$package_branch[0]->package_id]))continue;

                    $branch_full_name=$all_branches_grouped_by_id[$package_branch[0]->branch_id][0]->full_name;
                    $cat_name=$all_packages_cats[$package_branch[0]->package_id][0]->package_name;


                    $get_bills_ids = [];
                    $total_after_discount = 0;
                    $total_gain = 0;
                    $get_bills_amount = [];

                    foreach($package_branch as $key3 => $bill)
                    {
                        $get_bills_ids[] = $bill->client_bill_id;
                        $get_bills_amount[$bill->client_bill_id] = ($bill->client_bill_total_amount - $bill->client_bill_total_return_amount);
                    }

                    $total_after_discount = array_sum(array_values($get_bills_amount));
                    $get_bills_ids = array_unique($get_bills_ids);

                    $get_orders = clients_orders_m::whereIn("client_bill_id",$get_bills_ids)->get()->all();
                    $total_products_sells = 0;
                    $total_products_returns = 0;
                    $total_price_value = 0;
                    foreach ($get_orders as $key3 => $order)
                    {
                        if (isset($_GET['price_id']) && isset($prices_values[$order->pro_id])
                            && count($prices_values[$order->pro_id]) )
                        {

                            $get_pro_prices = collect($prices_values[$order->pro_id])->groupBy('price_id')->all();
                            if(isset($get_pro_prices[$_GET['price_id']]))
                            {
                                $total_price_value += ($get_pro_prices[$_GET['price_id']][0]->ppv_value * $order->order_quantity);
                            }
                        }

                        if ($order->order_return == 0)
                        {
                            $total_products_sells += ($order->pro_sell_price * $order->order_quantity);
                        }
                        else{
                            $total_products_returns += ($order->pro_sell_price * $order->order_quantity);
                        }

                    }

                    $total_gain = (($total_products_sells - $total_products_returns) - $total_price_value);
                    if(!isset($_GET['price_id']))
                    {
                        $total_gain = 'إختار سعر';
                    }

                    $output_data[] = [
                        "#" => "",
                        "branch_name" =>$branch_full_name,
                        "cat_name" =>$cat_name,
                        "total_packages_used" => count($package_branch),
                        "bills_count" =>count($get_bills_ids),
                        "total_bills_after_discount" =>$total_after_discount,
                        "total_gain" => $total_gain,
                    ];


                }

            }
            $this->data['output_data'] = $output_data;

        }


        return view("admin.subviews.reports.packages_report",$this->data);
    }

    //تقرير بيانات الفروع
    public function branches_report()
    {
        if (!check_permission($this->user_permissions,"factory/reports","branches_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = "تقرير بيانات الفروع";

        if($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = \App\User::get_all_users(" AND user.user_type in ('branch')");
        }
        else{
            $this->data["all_branches"] = \App\User::get_all_users(" AND user.user_type in ('branch') AND user.user_id = $this->related_user_id ");
        }

        return view("admin.subviews.reports.branches_report",$this->data);
    }

    //تقرير بيانات الموظفين
    public function cashiers_report()
    {
        if (!check_permission($this->user_permissions,"factory/reports","cashiers_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = "تقرير بيانات الموظفين";

        if($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_cashiers"] = \App\User::get_all_users(" AND user.user_type in ('branch_admin')");
        }
        else{
            $this->data["meta_title"] .= ' | '.$this->branch_data->full_name;

            $this->data["all_cashiers"] = \App\User::get_all_users(" 
            AND user.user_type in ('branch_admin') AND user.related_id = $this->related_user_id ");
        }

        return view("admin.subviews.reports.cashiers_report",$this->data);
    }

    //تقرير المبيعات لأفضل وقت
    public function best_sell_bills_report()
    {
        if (!check_permission($this->user_permissions,"factory/reports","best_sell_bills_report",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["meta_title"] = "تقرير المبيعات لأفضل وقت";

        $this->data['all_bills'] = [];
        $cond = "";

        if(count($_GET))
        {
            if(isset($_GET['start_date']) && !empty($_GET['start_date']) &&
                isset($_GET['end_date']) && !empty($_GET['end_date']))
            {
                $date_from = $_GET['start_date'];
                $date_to = $_GET['end_date'];


                $date_from=Carbon::createFromTimestamp(strtotime($date_from));
                $date_to=Carbon::createFromTimestamp(strtotime($date_to));

                $date_from=Carbon::create($date_from->year,$date_from->month,$date_from->day,5)->toDateTimeString();
                $date_to=Carbon::create($date_to->year,$date_to->month,$date_to->day,4)->toDateTimeString();

                $this->data["meta_title"] .= ' | from '.$date_from." To $date_to ";

                $cond .= " 
                    AND (bill.client_bill_date) between '$date_from' AND '$date_to' 
                ";

                if($this->data["current_user"]->user_type != "admin")
                {
                    $cond .= " AND bill.branch_id = $this->branch_id ";
                }

                $all_bills = clients_bills_m::get_clients_bills($cond);
                if(count($all_bills))
                {
                    $all_bills_branches = collect($all_bills)->groupBy('branch_full_name')->all();


                    foreach($all_bills_branches as $branch_name => $branch_bills)
                    {

                        $all_branch_bills = collect($branch_bills)->groupBy('client_bill_hour_only')->all();
                        foreach($all_branch_bills as $key => $hour_bills)
                        {
                            $bills_count = count($hour_bills->all());
                            $total_bills_amount = array_sum(convert_inside_obj_to_arr($hour_bills->all(),'client_bill_total_amount'));
                            $this->data['all_bills'][$branch_name][$key] = [
                                "bills_count" => $bills_count,
                                "total_bills_amount" => $total_bills_amount,
                            ];
                        }

                    }
//                    dump($this->data['all_bills']);

                }

            }
        }

        return view("admin.subviews.reports.best_sell_bills_report",$this->data);
    }

}
