<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\payment_methods_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class payment_methods extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if (!check_permission($this->user_permissions,"factory/payment_methods","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["all_payments"] = payment_methods_m::get()->all();

        return view("admin.subviews.payment_methods.show",$this->data);
    }

    public function save(Request $request , $payment_method_id = null)
    {

        $default_action = "add_action";
        if ($payment_method_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"factory/payment_methods","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["payment"] = "";

        if ($payment_method_id != null)
        {

            $get_payment = payment_methods_m::findOrFail($payment_method_id);
            $this->data["payment"] = $get_payment;

        }

        if ($request->method()=="POST")
        {

            $this->validate($request,
                [
                    "payment_method_name"=>"required|unique:payment_methods,payment_method_name,".$payment_method_id.",payment_method_id,deleted_at,NULL",
                    "payment_method_name_en"=>"required|unique:payment_methods,payment_method_name_en,".$payment_method_id.",payment_method_id,deleted_at,NULL",
                ],
                [
                    "payment_method_name.required" => "نوع السداد بالعربي مطلوب إدخالة",
                    "payment_method_name.unique" => "نوع السداد بالعربي موجود مسبقا",
                    "payment_method_name_en.required" => "نوع السداد بالإنجليزي مطلوب إدخالة",
                    "payment_method_name_en.unique" => "نوع السداد بالإنجليزي موجود مسبقا",
                ]
            );


            $request = clean($request->all());

            if ($payment_method_id == null)
            {
                $get_payment = payment_methods_m::create($request);
                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم تسجيل طريقة سداد جديدة"
                );
                #endregion
            }
            else{
                $get_payment->update($request);
                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تم تعديل بيانات طريقة سداد "
                );
                #endregion
            }


            $msg = "<div class='alert alert-success'> تم الحفظ بنجاح ..</div>";

            return Redirect::to('admin/payment_methods/save')->with([
                "msg"=> $msg
            ])->send();
        }


        return view("admin.subviews.payment_methods.save")->with($this->data);
    }


}
