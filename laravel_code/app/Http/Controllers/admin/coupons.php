<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\is_admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\admin\category_m;
use App\models\langs_m;
use App\models\coupons_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class coupons extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    #region coupon category

    public function show_all_coupon_cats(){
        if (!check_permission($this->user_permissions,"admin/coupons","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["all_coupon_cats"] = category_m::get_coupons_cats("");


        return view("admin.subviews.coupons.cats.show")->with($this->data);
    }

    public function save_coupon_cat(Request $request,$cat_id=null){
        if($cat_id==null){
            if (!check_permission($this->user_permissions,"admin/coupons","add_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"admin/coupons","edit_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }



        $this->data["cat_data"] = "";


        if ($cat_id != null){

            $this->data["cat_data"]=category_m::find($cat_id);

            if(!is_object($this->data["cat_data"])){
                return Redirect::to('admin/coupons/cat/save_cat')->with(["msg"=>"<div class='alert alert-danger'>التصنيف غير موجود اضف واحد جديد من هنا</div>"])->send();
            }

        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "cat_name" => "required",
            ]);





            $request = clean($request->all());
            $request["cat_type"]="coupon";

            if (isset($coupon_id))
            {
                // update
                category_m::find($cat_id)->update($request);
                $msg = "لقد تم التعديل بنجاح";
            }
            else{
                // insert
                $new_obj = category_m::create($request);
                $cat_id = $new_obj->cat_id;
                $msg = "لقد تم الاضافة بنجاح";
            }


            return Redirect::to('admin/coupons/cats/save_cat/'.$cat_id)->with(["msg"=>"<div class='alert alert-success'>$msg</div>"])->send();

        }//end submit


        return view("admin.subviews.coupons.cats.save")->with($this->data);
    }



    #endregion

    public function index(Request $request)
    {
        if (!check_permission($this->user_permissions,"admin/coupons","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $selected_cat_id=$request->get("cat_id","");
        $selected_coupon_text=$request->get("coupon_text","");

        $this->data["all_coupon_cats"]=category_m::where("cat_type","coupon")->get();

        $this->data["selected_cat_id"]=$selected_cat_id;
        $this->data["selected_coupon_text"]=$selected_coupon_text;

        $all_coupons=coupons_m::whereNull("deleted_at");
        if($selected_cat_id!=""){
            $all_coupons= $all_coupons->where("cat_id",$selected_cat_id);
        }

        if($selected_coupon_text!=""){
            $all_coupons= $all_coupons->where("coupon_code","like","%$selected_coupon_text%");
        }

        $all_coupons=$all_coupons->paginate(50);
        $all_coupons=$all_coupons->appends(Input::except("page"));

        $this->data["all_coupons"]=$all_coupons;


//        $get_coupons = coupons_m::paginate(10);
//        $this->data["coupons_pagination"] = $get_coupons;
//        $get_coupons_ids = convert_inside_obj_to_arr($get_coupons,'coupon_id');
//        $this->data["all_coupons"] = coupons_m::whereIn("coupon_id",$get_coupons_ids)->get()->all();

        return view("admin.subviews.coupons.show")->with($this->data);
    }

    public function save_coupon(Request $request , $coupon_id = null)
    {

        if($coupon_id==null){
            if (!check_permission($this->user_permissions,"admin/coupons","add_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"admin/coupons","edit_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }

        $this->data["all_coupon_cats"]=category_m::where("cat_type","coupon")->get();


        $this->data["coupon_data"] = "";


        if ($coupon_id != null){

            $this->data["coupon_data"]=coupons_m::where("coupon_id",$coupon_id)->get()->first();

            if(!is_object($this->data["coupon_data"]) || empty($this->data["coupon_data"])){
                return Redirect::to('admin/coupons/save_coupon')->with(["msg"=>"<div class='alert alert-danger'>الكوبون غير موجود اضف واحد جديد من هنا</div>"])->send();
            }

        }

        if ($request->method() == "POST")
        {
            $this->validate($request,[
                "coupon_code" => "required|unique:coupons,coupon_code,".$coupon_id.",coupon_id,deleted_at,NULL",
                "coupon_end_date" => "required|date",
                "coupon_used_times" => "required|numeric|min:1",
                "is_rate" => "required|numeric|min:0",
                "coupon_value" => "required|numeric|min:0",
                "cat_id" => "required",
            ]);


            $request = clean($request->all());


            if (isset($coupon_id))
            {
                // update
                coupons_m::find($coupon_id)->update($request);
                $msg = "لقد تم التعديل بنجاح";
                $action_desc = " تعديل بيانات الكوبون رقم ".$coupon_id;
            }
            else{
                // insert
                $new_obj = coupons_m::create($request);
                $coupon_id = $new_obj->coupon_id;
                $msg = "لقد تم الاضافة بنجاح";
                $action_desc = "إضافة كوبون جديد برقم ".$coupon_id;
            }

            #region save in site_tracking
            $this->track_my_action(
                $action_desc
            );
            #endregion

            return Redirect::to('admin/coupons/save_coupon/'.$coupon_id)->with(["msg"=>"<div class='alert alert-success'>$msg</div>"])->send();

        }//end submit


        return view("admin.subviews.coupons.save")->with($this->data);
    }

    public function import_coupons(Request $request)
    {

        if ($request->method() == "POST")
        {

            if (!check_permission($this->user_permissions,"admin/coupons","add_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }

            $name = $_FILES['excel_file']['name'];
            $ext = (explode(".", $name));

            if (empty($_FILES) || !is_array($ext) || !count($ext) || !isset($ext[1]) || !in_array($ext[1],["xls","XLS","xlsx","XLSX"]))
            {
                return Redirect::to('admin/coupons/save_coupon')->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك ارفع ملف اكسيل فقط  </div>"
                ])->send();
            }

            $this->data['input_arr'] = [];
            $this->data["cat_id"]=$request->get("cat_id");
            $this->data["coupon_used_times"]=$request->get("coupon_used_times");

            if(!($this->data["cat_id"]>0)){
                return Redirect::to('admin/coupons/save_coupon')->with([
                    "msg" => "<div class='alert alert-danger'> يجب ان تختار تصنيف  </div>"
                ])->send();
            }

            if($this->data["coupon_used_times"] <= 0)
            {
                return Redirect::to('admin/coupons/save_coupon')->with([
                    "msg" => "<div class='alert alert-danger'> يجب ان يكون عدد مرات الاستخدام لكل كوبون اكبر من او يساوي واحد  </div>"
                ])->send();
            }


            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

            Excel::load($_FILES['excel_file']['tmp_name'],function($reader)
            {

                // Getting all results
                $results = $reader->toArray();
                if (is_array($results) && count($results) > 0)
                {

                    foreach ($results as $key => $value)
                    {

                        $value['coupon_code'] = clean($value['coupon_code']);
                        $value['coupon_end_date'] = clean($value['coupon_end_date']);
                        $value['coupon_value'] = clean($value['coupon_value']);
                        $value['is_rate'] = clean($value['is_rate']);
                        $value['cat_id'] = $this->data["cat_id"];
                        $value['coupon_used_times'] = $this->data["coupon_used_times"];

                        $check_unique_coupon = coupons_m::where("coupon_code",$value['coupon_code'])
                            ->get()->all();

                        if (is_array($check_unique_coupon) && count($check_unique_coupon))
                        {
                            continue;
                        }

                        if (isset($this->data['input_arr'][$value['coupon_code']]) || empty($value['coupon_code']))
                        {
                            continue;
                        }

                        $this->data['input_arr'][$value['coupon_code']] = [
                            "coupon_code" => $value['coupon_code'],
                            "coupon_end_date" => date('Y-m-d',strtotime($value['coupon_end_date'])),
                            "is_rate" => ($value['is_rate'] == '%')?1:0,
                            "coupon_value" => $value['coupon_value'],
                            "cat_id" => $value['cat_id'],
                            "coupon_used_times" => $value['coupon_used_times']
                        ];

                    }

                }

            });

            if (!count($this->data['input_arr']))
            {
                return Redirect::to('admin/coupons/save_coupon')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحه للإدخال برجاء الإلتزام بالملف المرفق وعدم تغيير اسماء اول صف  </div>"
                ])->send();
            }

            $split_arr = array_chunk($this->data['input_arr'],500);


            foreach ($split_arr as $key => $arr)
            {
                coupons_m::insert($arr);
            }


            $desc_msg = ' لقد تم تسجيل الكوبونات بنجاح ';

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

            return Redirect::to('admin/coupons/save_coupon')->with([
                "msg" => "<div class='alert alert-success'>$desc_msg  </div>"
            ])->send();


        }


    }

    public function remove_coupon(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/coupons","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        #region save in site_tracking
            $this->track_my_action(
                $action_desc = "تم مسح كوبون"
            );
        #endregion

        $this->general_remove_item($request,'App\models\coupons_m');
    }

    public function remove_coupons_cat(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/coupons","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $output = array();
        $item_id = (int)$request->get("item_id");

        $model_name = 'App\models\coupons_m';
        if($model_name==""){
            $model_name = $request->get("table_name"); // App\User
        }

        if ($item_id > 0)
        {

            coupons_m::where("cat_id",$item_id)->delete();
            category_m::find($item_id)->delete();

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = "تم مسح كوبونات القسم"
            );
            #endregion

            $output["deleted"] = "yes";

        }
        else{
            $output["deleted"] = "no";
        }

        echo json_encode($output);
        return;

    }

}
