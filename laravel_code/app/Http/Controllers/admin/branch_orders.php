<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\bills\branch_bills_m;
use App\models\bills\permission_bill_orders_m;
use App\models\bills\permission_bills_m;
use App\models\orders\branches_orders_m;
use App\models\product\branches_products_m;
use App\models\product\products_m;
use App\models\status_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class branch_orders extends is_admin_controller
{

    public function __construct(){
        parent::__construct();

        //pending admin 1
        //waiting admin 2
        //sending admin 3
        //received branch 4
        //cancelled branch 5
        //refused admin 6
        $this->data["all_status"]=status_m::where("user_type","branch")->get()->all();

    }

    public function show_all_branch_bills()
    {
        if (!check_permission($this->user_permissions,"branch_orders","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->send();
        }


        //get all branch_bills

        $cond = "";

        if (isset($_GET["start_date"]) && !empty($_GET["start_date"]))
        {
            $start_date = $_GET["start_date"];
            $cond .= " AND bill.branch_bill_date >= '$start_date' ";
        }

        if (isset($_GET["end_date"]) && !empty($_GET["end_date"]))
        {
            $end_date = $_GET["end_date"];
            $cond .= " AND bill.branch_bill_date <= '$end_date' ";
        }

        $this->data["all_branches"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");

            if (isset($_GET["branch_id"]) && $_GET["branch_id"] > 0)
            {
                $branch_id = $_GET["branch_id"];
                $cond .= " AND bill.branch_id = $branch_id ";
            }

            $this->data["all_bills"]=branch_bills_m::get_branch_bills(" $cond order by bill.branch_bill_date desc ");

        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $cond .= " AND bill.branch_id = $this->branch_id ";
            $this->data["all_bills"]=branch_bills_m::get_branch_bills(" $cond order by bill.branch_bill_date desc  ");
        }

        $this->data["all_branches"] = $all_branches;

        return view("admin.subviews.branch_bills.with_factory.show_bills",$this->data);
    }

    public function change_bill_status(Request $request) {

        if (!check_permission($this->user_permissions,"branch_orders","change_bill_status",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->send();
        }

        $output = array();
        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;

            if(!in_array($accept,[4,5])){
                $output["msg"].="ليس لديك الصلاحية للدخول لهذة الصفحة !!"."<br>";
                $make_update=false;
            }

            if(!in_array($obj->status_id,[1,2])&&$accept==5){
                $output["msg"].="لا يمكن الغاء الفاتورة اذا كانت حالتها قيد الانتظار"."<br>";
                $make_update=false;

            }

            if($obj->status_id==4||$obj->status_id==5||$obj->status_id==6){
                $output["msg"].="لا يمكن تغيير الحاله اذا كانت الغيت او رفضت او تم الاستلام"."<br>";
                $make_update=false;

            }


            if($accept==4&&$obj->status_id!=3){
                $output["msg"].="لا يمكن استلام الطلبات حاليا لان الادمن لم يرسلها بعد"."<br>";
                $make_update=false;
            }

            if($make_update){
                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                if ($accept==4&&$return_statues){
                    //move bills order to branches products
                    $output["msg"].="نقل الطلبات الي المخزن";

                    if ($this->data["current_user"]->user_type == "admin")
                    {
                        $branch_id = $obj->branch_id;
                    }
                    else{
                        $branch_id = $this->branch_id;
                    }

                    //get bill's orders
                    $all_bill_orders=branches_orders_m::get_bill_orders(" 
                        AND branch_bill.branch_id= $branch_id
                        AND branch_bill.branch_bill_id=$item_id"
                    );

                    //add to branches_products

                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = $output["msg"]
                    );
                    #endregion

                    foreach($all_bill_orders as $order_key=>$order){

                        //check if there is a product in branches_products
                        //if true add quantity to this  row

                        $old_pro=branches_products_m::
                        where("branch_id",$obj->branch_id)->
                        where("pro_id",$order->pro_id)->
                        get()->first();


                        if(is_object($old_pro)){
                            //update quantity
                            $old_pro->update(
                                [
                                    "b_p_quantity"=>$old_pro->b_p_quantity+$order->b_o_quantity,
                                    "b_p_first_time" => 1
                                ]
                            );
                        }
                        else{
                            //add new row to branches_products
                            branches_products_m::create([
                                "branch_id"=>$obj->branch_id,
                                "pro_id"=>$order->pro_id,
                                "b_p_quantity"=>$order->b_o_quantity,
                                "b_p_date"=>Carbon::now(),
                                "b_p_first_time" => 1

                            ]);
                        }


                    }


                }

            }


            $output["msg"].=generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
        }


        echo json_encode($output);
    }

    public function show_bill_orders($bill_id){

        if (!check_permission($this->user_permissions,"branch_orders","show_bill_orders",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->send();
        }

        $bill_obj=branch_bills_m::get_branch_user_bills(" AND bill.branch_bill_id=$bill_id");

        if(!isset_and_array($bill_obj)){
            return abort(404);
        }
        $bill_obj=$bill_obj[0];


        $this->data["meta_title"]="منتجات طلب فرع- ".$bill_obj->full_name." رقم الفاتورة ".$bill_id;

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_bill_orders"]=branches_orders_m::get_bill_orders(" 
            AND branch_bill.branch_bill_id=$bill_id"
            );

        }
        else{
            $this->data["all_bill_orders"]=branches_orders_m::get_bill_orders(" 
            AND branch_bill.branch_id=$this->branch_id
            AND branch_bill.branch_bill_id=$bill_id"
            );
        }

        return view("admin.subviews.branch_bills.with_factory.show_bill_orders",$this->data);

    }

    public function make_bill(Request $request,$branch_bill_id = null)
    {

        $default_action = "add_action";

        if ($branch_bill_id != null && $branch_bill_id > 0)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"branch_orders","$default_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();

        }

        $this->get_cat_child_cats_and_products($request,"0");


        $this->data["bill_data"] = "";
        $this->data["bill_orders"] = [];

        if ($branch_bill_id != null && $branch_bill_id > 0)
        {
            $bill_data = branch_bills_m::findOrFail($branch_bill_id);
            if (!is_object($bill_data) || $bill_data->status_id >=3)
            {
                return  Redirect::to('admin/branch_bills/add_bill')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح لتعديل هذة الفاتورة</div>"])->send();
            }

            $this->data["bill_data"] = $bill_data;
            $this->data["bill_orders"] = branches_orders_m::where("branch_bill_id",$branch_bill_id)
            ->get()->all();
        }

        //get all products
        $this->data["all_products"]=products_m::get_pros(" ");

        $all_products=collect($this->data["all_products"])->groupBy("pro_id")->all();



        $this->data["all_branches"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $get_all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $get_all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        $this->data["all_branches"] = $get_all_branches;


        if($request->method()=="POST")
        {

            $this->validate($request, [
                "pro_id.*" => "required",
                "b_o_quantity.0" => "required|min:1|numeric",
                "bill_date" => "required",
                "branch_id" => "required",
            ]);

            if ($branch_bill_id != null && $branch_bill_id > 0)
            {
                $bill_data = branch_bills_m::findOrFail($branch_bill_id);
                if (!is_object($bill_data) || $bill_data->status_id >=3)
                {
                    return  Redirect::to('admin/branch_bills/add_bill')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح لتعديل هذة الفاتورة</div>"])->send();
                }

            }

            $branch_data = User::find($request["branch_id"]);




            $request_data = $this->cleaning_input($request->all(), $except = array());

            $request_data["b_o_quantity"]=array_map("intval",$request_data["b_o_quantity"]);


            //create orders
            if(count($request_data["pro_id"])==count($request_data["b_o_quantity"])){

                if ($branch_bill_id != null && $branch_bill_id > 0)
                {
                    // edit
                    $bill_data->update([
                        "branch_bill_date" => $request_data['bill_date'],
                        "branch_id" => $request_data['branch_id']
                    ]);

                    // remove previous orders
                    branches_orders_m::where("branch_bill_id",$branch_bill_id)->forceDelete();
                    $bill_obj = $bill_data;

                }
                else{

                    // add
                    $bill_obj=branch_bills_m::create([
                        "branch_id"=>$request["branch_id"],
                        "status_id"=>"1",
                        "branch_bill_date"=>$request_data["bill_date"]
                    ]);

                }

                $bill_orders=[];

                foreach($request_data["pro_id"] as $pro_key=>$pro_id){

                    if (!isset($all_products[$pro_id])||!($request_data["b_o_quantity"][$pro_key]>0))
                    {
                        continue;
                    }

                    if(isset($bill_orders[$pro_id]["b_o_quantity"])){
                        $bill_orders[$pro_id]["b_o_quantity"]=$bill_orders[$pro_id]["b_o_quantity"]+$request_data["b_o_quantity"][$pro_key];
                    }else{
                        $bill_orders[$pro_id]["b_o_quantity"]=$request_data["b_o_quantity"][$pro_key];
                    }

                    //check if b_o_quantity > pro_quantity
//                    if($bill_orders[$pro_id]["b_o_quantity"]>$all_products[$pro_id][0]->pro_quantity){
//                        $bill_orders[$pro_id]["b_o_quantity"]=$all_products[$pro_id][0]->pro_quantity;
//                    }


                    $bill_orders[$pro_id]["branch_bill_id"]=$bill_obj->branch_bill_id;
                    $bill_orders[$pro_id]["pro_id"]=$pro_id;
                    $bill_orders[$pro_id]["branch_id"]=$request["branch_id"];
                    $bill_orders[$pro_id]["b_o_return"]=0;
                    $bill_orders[$pro_id]["b_o_date"]=$request_data["bill_date"];
                    $bill_orders[$pro_id]["created_at"]=Carbon::now();
                    $bill_orders[$pro_id]["updated_at"]=Carbon::now();
                }

                if (!count($bill_orders))
                {
                    branch_bills_m::find($bill_obj->branch_bill_id)->forceDelete();

                    return  Redirect::to('admin/branch_bills/add_bill')->with(
                        [
                            "msg" => "<div class='alert alert-danger'>لم يتم إضافة منتجات لعدم وجود كمية </div>"
                        ])->send();
                }

                branches_orders_m::insert($bill_orders);

                //notify admin
                $bill_notification="تم عمل/تعديل طلبيه منتجات من الفرع ";
                $bill_notification .= $branch_data->full_name." ";

                $bill_notification .="بواسطة";
                $bill_notification .=" ".$this->current_user_data->full_name." ";
                $bill_notification .="يمكنك مشاهدة تفاصيل الفاتورة من خلال ";
                $bill_notification .="<a href='".url("admin/order/show_branch_bill_orders/$bill_obj->branch_bill_id")."'>اللينك</a>";

                $this->track_my_action($bill_notification);
                $this->_send_email_to_all_users_type(
                    $user_type = "admin" ,
                    $data = $bill_notification ,
                    $subject = "اشعار من النظام بتاريخ ".Carbon::now()
                );


                return Redirect::to('admin/branch_bills/show_bill_orders/'.$bill_obj->branch_bill_id)->send();
            }

        }

        return view("admin.subviews.branch_bills.with_factory.add_bill",$this->data);
    }


    public function show_permission_to_stock_bills($permission_type)
    {

        if ($permission_type != "add" && $permission_type != "get")
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $permission_page = "add_permission_to_stock";
        if($permission_type == "get")
        {
            $permission_page = "get_permission_from_stock";
        }

        if (!check_permission($this->user_permissions,"$permission_page","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["all_branches"] = [];
        $this->data["permission_type"] = $permission_type;

        if ($this->data["current_user"]->user_type == "admin")
        {
            $get_all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $get_all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        $this->data["all_branches"] = $get_all_branches;

        $this->data["bills"] = [];
        if (is_array($_GET) && count($_GET) && isset($_GET['branch_id']))
        {
            $branch_id = clean(intval($_GET['branch_id']));
            $cond = "";
            if (isset($_GET['from_date']) && !empty($_GET['from_date']))
            {
                $from_date = clean($_GET['from_date']);
                $cond = " AND bill.permission_date >= '$from_date' ";
            }

            if (isset($_GET['to_date']) && !empty($_GET['to_date']))
            {
                $to_date = clean($_GET['to_date']);
                $cond = " AND bill.permission_date <= '$to_date' ";
            }

            $bills = permission_bills_m::get_data(" 
                AND bill.branch_id = $branch_id 
                AND bill.permission_type = '$permission_type' 
                $cond 
                order by bill.permission_date desc
             ");
            $this->data["bills"] = $bills;
        }

        return view("admin.subviews.branch_bills.show_permission_to_stock_bills",$this->data);
    }


    public function show_permission_to_stock_bill_orders($permission_type, $permission_id)
    {

        if ($permission_type != "add" && $permission_type != "get")
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $permission_page = "add_permission_to_stock";
        if($permission_type == "get")
        {
            $permission_page = "get_permission_from_stock";
        }

        if (!check_permission($this->user_permissions,"$permission_page","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["bill"] = permission_bills_m::findOrFail($permission_id);
        $this->data["orders"] = permission_bill_orders_m::get_data(" 
            AND o.permission_id = $permission_id
         ");

        if($this->data["bill"]->branch_id=="0"){
            $this->data["meta_title"]="منتجات اذن اضافة "." الفرع الرئيسي";
        }
        else{
            $branch_data=User::find($this->data["bill"]->branch_id);
            $this->data["meta_title"]="منتجات اذن اضافة ".$branch_data->full_name;
        }

        return view("admin.subviews.branch_bills.show_permission_to_stock_bill_orders",$this->data);
    }

    public function add_permission_to_stock(Request $request,$permission_type)
    {

        if ($permission_type == "add")
        {
            if (!check_permission($this->user_permissions,"add_permission_to_stock","add_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }
        elseif($permission_type == "get"){
            if (!check_permission($this->user_permissions,"get_permission_from_stock","add_action",$this->data["current_user"]))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
            }
        }
        else{
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["permission_type"] = $permission_type;

        $this->get_cat_child_cats_and_products($request,"0");

        //get all products
        $this->data["all_products"]=products_m::get_pros(" ");

        $all_products=collect($this->data["all_products"])->groupBy("pro_id")->all();

        $this->data["all_branches"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $get_all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $get_all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
        }

        $this->data["all_branches"] = $get_all_branches;


        if($request->method()=="POST")
        {

            $this->validate($request,
                [
                    "pro_id.0" => "required",
                    "pro_quantity.0" => "required|min:1|numeric",
                    "branch_id" => "required",
                    "permission_date" => "required|date"
                ],
                [
                    "branch_id.required" => "من فضلك إختار الفرع",
                    "permission_date.required" => "من فضلك إختار تاريخ الإذن",
                    "permission_date.date" => "تاريخ الإذن غير صحيح"
                ]
            );


            $branch_data = User::find($request["branch_id"]);




            $request_data = $this->cleaning_input($request->all(), $except = array());

            $request_data["pro_quantity"]=array_map("intval",$request_data["pro_quantity"]);
            $branch_id = $request_data["branch_id"];

            //create orders
            if(count($request_data["pro_id"])==count($request_data["pro_quantity"]))
            {

                $bill_orders=[];

                if($permission_type == "add")
                {

                    // إذن إضافة

                    foreach($request_data["pro_id"] as $pro_key=>$pro_id)
                    {

                        if (isset($all_products[$pro_id]) && count($all_products[$pro_id]->all()))
                        {

                            if(isset($bill_orders[$pro_id]["pro_quantity"])){
                                $bill_orders[$pro_id]["pro_quantity"]=$bill_orders[$pro_id]["pro_quantity"]+$request_data["pro_quantity"][$pro_key];
                            }
                            else{
                                $bill_orders[$pro_id]["pro_quantity"]=$request_data["pro_quantity"][$pro_key];
                            }

                            $bill_orders[$pro_id]["pro_id"]=$pro_id;

                        }
                        else{
                            continue;
                        }

                    }

                }
                else{

                    // إذن صرف
                    if($branch_id == 0)
                    {

                        // from main stock
                        foreach($request_data["pro_id"] as $pro_key=>$pro_id)
                        {

                            if (isset($all_products[$pro_id]) && count($all_products[$pro_id]->all()))
                            {

                                if ($all_products[$pro_id][0]->pro_quantity == 0)
                                {
                                    continue;
                                }

                                if(isset($bill_orders[$pro_id]["pro_quantity"])){
                                    $bill_orders[$pro_id]["pro_quantity"]=$bill_orders[$pro_id]["pro_quantity"]+$request_data["pro_quantity"][$pro_key];
                                }
                                else{
                                    $bill_orders[$pro_id]["pro_quantity"]=$request_data["pro_quantity"][$pro_key];
                                }

                                //check if pro_quantity > pro_quantity
                                if($bill_orders[$pro_id]["pro_quantity"]>$all_products[$pro_id][0]->pro_quantity)
                                {
                                    $bill_orders[$pro_id]["pro_quantity"]=$all_products[$pro_id][0]->pro_quantity;
                                }

                                $bill_orders[$pro_id]["pro_id"]=$pro_id;

                            }
                            else{
                                continue;
                            }


                        }

                    }
                    else{

                        $this->data["all_products"]=branches_products_m::get_branches_pros(" 
                            AND branch_pro.branch_id = $branch_id
                         ");
                        $all_products=collect($this->data["all_products"])->groupBy("pro_id")->all();


                        // from specific branch
                        foreach($request_data["pro_id"] as $pro_key=>$pro_id)
                        {

                            if (isset($all_products[$pro_id]) && count($all_products[$pro_id]->all()))
                            {

                                if ($all_products[$pro_id][0]->b_p_quantity == 0)
                                {
                                    continue;
                                }

                                if(isset($bill_orders[$pro_id]["pro_quantity"])){
                                    $bill_orders[$pro_id]["pro_quantity"]=$bill_orders[$pro_id]["pro_quantity"]+$request_data["pro_quantity"][$pro_key];
                                }
                                else{
                                    $bill_orders[$pro_id]["pro_quantity"]=$request_data["pro_quantity"][$pro_key];
                                }

                                //check if pro_quantity > pro_quantity
                                if($bill_orders[$pro_id]["pro_quantity"]>$all_products[$pro_id][0]->b_p_quantity){
                                    $bill_orders[$pro_id]["pro_quantity"]=$all_products[$pro_id][0]->b_p_quantity;
                                }

                                $bill_orders[$pro_id]["pro_id"]=$pro_id;


                            }
                            else{
                                continue;
                            }


                        }
                    }

                }

                if (!count($bill_orders))
                {
                    return Redirect::to("admin/branch_bills/add_permission_to_stock/$permission_type")->with(
                        [
                            "msg"=>"<div class='alert alert-danger'>برجاء التأكد ان المنتجات موجود في هذا الفرع او الكميات لا تسمح لعمل إذن </div>"
                        ]
                    )->send();
                }
                else{

                    // add permission bill
                    $permission = permission_bills_m::create([
                        "user_id" => $this->user_id,
                        "branch_id" => $branch_id,
                        "permission_type" => $permission_type,
                        "permission_date" => $request_data["permission_date"],
                    ]);

                    $permission_id = $permission->permission_id;

                    $permission_bill_orders = [];
                    foreach($bill_orders as $key => $bill_order)
                    {
                        $permission_bill_orders[] = [
                            "permission_id" => $permission->permission_id,
                            "pro_id" => $bill_order['pro_id'],
                            "pro_quantity" => $bill_order['pro_quantity'],
                        ];

                        if ($permission_type == "add")
                        {
                            // add permission
                            if ($branch_id == 0)
                            {
                                $get_pro_obj = products_m::find($bill_order['pro_id']);
                                if (is_object($get_pro_obj))
                                {
                                    $get_pro_obj->update([
                                        "pro_quantity" => ($get_pro_obj->pro_quantity + $bill_order['pro_quantity'])
                                    ]);
                                }

                            }
                            else{
                                $get_pro_obj = branches_products_m::where("branch_id",$branch_id)
                                    ->where("pro_id",$bill_order['pro_id'])->get()->first();
                                if (is_object($get_pro_obj))
                                {
                                    $get_pro_obj->update([
                                        "b_p_quantity" => ($get_pro_obj->b_p_quantity + $bill_order['pro_quantity'])
                                    ]);
                                }
                                else{
                                    // add new product
                                    branches_products_m::create([
                                        "branch_id" => $branch_id,
                                        "pro_id" => $bill_order['pro_id'],
                                        "b_p_quantity" => $bill_order['pro_quantity'],
                                        "b_p_date" => date("Y-m-d H:i:s")
                                    ]);
                                }

                            }
                        }
                        else{
                            // get permission
                            if ($branch_id == 0)
                            {
                                $get_pro_obj = products_m::find($bill_order['pro_id']);
                                if (is_object($get_pro_obj))
                                {
                                    $get_pro_obj->update([
                                        "pro_quantity" => ($get_pro_obj->pro_quantity - $bill_order['pro_quantity'])
                                    ]);
                                }

                            }
                            else{
                                $get_pro_obj = branches_products_m::where("branch_id",$branch_id)
                                    ->where("pro_id",$bill_order['pro_id'])->get()->first();
                                if (is_object($get_pro_obj))
                                {
                                    $get_pro_obj->update([
                                        "b_p_quantity" => ($get_pro_obj->b_p_quantity - $bill_order['pro_quantity'])
                                    ]);
                                }

                            }
                        }

                    }

                    permission_bill_orders_m::insert($permission_bill_orders);
                    //notify admin
                    if ($permission_type == "add")
                    {
                        $bill_notification="تم عمل إذن إضافة ";
                    }
                    else{
                        $bill_notification="تم عمل إذن صرف ";
                    }

                    if ($branch_id==0){
                        $branch_full_name="الفرع الرئيسي";
                    }
                    else{
                        $branch_full_name=User::find($branch_id)->full_name;
                    }

                    $bill_notification.=" بواسطة ".$this->current_user_data->full_name." - ".$branch_full_name." ";
                    $bill_notification.="يمكنك ان تري تفاصيل الاذن من خلال  ";
                    $bill_notification.="<a href='".url("/admin/branch_bills/show_permission_to_stock_bill_orders/$permission_type/$permission->permission_id")."'>
اللنك
</a>";

                    $this->track_my_action($bill_notification);
                    $this->_send_email_to_all_users_type(
                        $user_type = "admin" ,
                        $data = $bill_notification ,
                        $subject = "اشعار من النظام بتاريخ ".Carbon::now()
                    );
                }

                return Redirect::to('admin/branch_bills/show_permission_to_stock_bill_orders/'.$permission_type."/$permission_id")->with(
                    [
                        "msg"=>"<div class='alert alert-success'>$bill_notification</div>"
                    ]
                )->send();
            }

        }

        return view("admin.subviews.branch_bills.add_permission_to_stock",$this->data);
    }

    public function make_return_bill(Request $request){

        if (!check_permission($this->user_permissions,"branch_orders","add_return_bill",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->send();
        }


        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "bill_id" => "required",
                "returned_quantity.*" => "required|min:0|numeric",
                "order_id*" => "required",
            ]);

            $request_data = $this->cleaning_input($request->all());

            $request_data["returned_quantity"]=array_map("intval",$request_data["returned_quantity"]);

            if ((count($request_data["returned_quantity"]) != count($request_data["order_id"])) ||
                empty($request_data["bill_id"]))
            {
                return Redirect::to('branch/branch_bills/add_return_bill')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحة !!  </div>"
                ])->send();
            }

            $bill_id = $request_data["bill_id"];
            $returned_quantity = $request_data["returned_quantity"];
            $order_id = $request_data["order_id"];


            // check if bill is exist

            $bill_obj = branch_bills_m::get_branch_bills(" AND bill.branch_bill_id = $bill_id ");
            if (count($bill_obj) == 0)
            {
                return Redirect::to('branch/branch_bills/add_return_bill')->with([
                    "msg" => "<div class='alert alert-danger'> الفاتورة غير موجودة  </div>"
                ])->send();
            }

            $bill_obj = $bill_obj[0];
            $returns_input_arr = array();

            // get all bill returns orders if exist
            $bill_return_orders = branches_orders_m::get_bill_orders(" 
                        AND branch_bill.branch_bill_id = $bill_id 
                        AND branches_order.b_o_return = 1 
                        AND branches_order.factory_accept_return=1
                        
                    ");
            $bill_return_orders = collect($bill_return_orders);
            $bill_return_orders = $bill_return_orders->groupBy("pro_id");

            foreach($order_id as $key => $id)
            {

                // check if this order item is exist to this bill_id and this quantity is larger than return
                $order_check_obj = branches_orders_m::where("b_o_id",$id)->get()->first();

                if(
                    is_object($order_check_obj) &&
                    $order_check_obj->branch_bill_id == $bill_id &&
                    $order_check_obj->b_o_quantity >= $returned_quantity[$key] &&
                    $returned_quantity[$key] > 0 )
                {

                    // check if total returns quantity to this item in bill less than purchase orders to this item

                    if  (
                            empty($bill_return_orders) ||
                            !isset($bill_return_orders[$order_check_obj->pro_id]) ||
                            (
                                isset($bill_return_orders[$order_check_obj->pro_id]) &&
                                ($order_check_obj->b_o_quantity - array_sum(convert_inside_obj_to_arr($bill_return_orders[$order_check_obj->pro_id],"b_o_quantity"))) >= $returned_quantity[$key]
                            )
                        )
                    {

                        // check if quantity in stock larger than or equal returned and update it
                        //$product_mat_obj
                        $branch_product_obj = branches_products_m::where("pro_id",$order_check_obj->pro_id)->get()->first();
                        $product_obj=products_m::find($order_check_obj->pro_id);
                        if (is_object($branch_product_obj) && $branch_product_obj->b_p_quantity >= $returned_quantity[$key])
                        {

//                            branches_products_m::where("pro_id",$order_check_obj->pro_id)->update([
//                                "b_p_quantity" => ($branch_product_obj->b_p_quantity - $returned_quantity[$key])
//                            ]);


                            $branch_data = User::find($bill_obj->branch_id);

                            $desc_msg = " تعديل الفرع  ".$branch_data->full_name." للمنتجات (".$product_obj->pro_name.")  لترجيع كمية $returned_quantity[$key] ";

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                            #endregion

                            #region save in notification
                            $this->send_all_user_type_notifications(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_type = "admin"
                            );
                            #endregion


                            #region send email
                            $this->_send_email_to_all_users_type(
                                $user_type = "admin" ,
                                $data = $desc_msg,
                                $subject = "مرتجع علي الفاتورة من الفرع (".$branch_data->full_name.")"
                            );
                            #endregion

                            unset($order_check_obj["sup_mat_id"], $order_check_obj["created_at"], $order_check_obj["updated_at"]);
                            $returns_row=[];
                            $returns_row["branch_bill_id"] = $bill_id;
                            $returns_row["pro_id"] = $order_check_obj->pro_id;
                            $returns_row["branch_id"] = $bill_obj->branch_id;
                            $returns_row["b_o_quantity"] = $returned_quantity[$key];
                            $returns_row["b_o_date"] = Carbon::now();
                            $returns_row["b_o_return"] = 1;
                            $returns_row["created_at"] = Carbon::now();
                            $returns_row["updated_at"] = Carbon::now();


                            $returns_input_arr[]=$returns_row;

                        }

                    }
                }

            }


            if(count($returns_input_arr))
            {
                branches_orders_m::insert($returns_input_arr);


                return Redirect::to('admin/branch_bills/add_return_bill')->with([
                    "msg" => "<div class='alert alert-success'> تم الحفظ بنجاح  </div>"
                ])->send();

            }
            else{
                return Redirect::to('admin/branch_bills/add_return_bill')->with([
                    "msg" => "<div class='alert alert-danger'> لم يتم حفظ الطلبات بسبب بيانات غير صالحه او كمية غير كافية </div>"
                ])->send();
            }


        }


        return view("admin.subviews.branch_bills.with_factory.add_return_bill",$this->data);
    }

    public function get_bill_orders(Request $request)
    {

        $output = array();
        $output["success"] = "error";
        $output["msg"] = "";

        if ($request->method() == "POST")
        {
            $bill_id = clean($request->get("bill_id"));
            if (!isset($bill_id) && empty($bill_id))
            {
                $output["msg"] = "<div class='alert alert-danger'> من فضلك ادخل رقم الفاتورة اولا </div>";
                echo json_encode($output);
                return;
            }



            if ($this->data["current_user"]->user_type == "admin")
            {
                $bill_obj = branch_bills_m::get_branch_bills(" AND bill.branch_bill_id = $bill_id  AND bill.status_id=4");
            }
            else{

                $bill_obj = branch_bills_m::get_branch_bills(" 
                        AND bill.branch_id = $this->branch_id
                        AND bill.branch_bill_id = $bill_id
                        AND bill.status_id = 4 ");

            }



            if (count($bill_obj) == 0)
            {
                $output["msg"] = "<div class='alert alert-danger'> الفاتورة غير موجوده !!!</div>";
                echo json_encode($output);
                return;
            }

            $bill_obj = $bill_obj[0];
            $output["success"] = "success";

            // get all bill purchases orders
            $bill_orders = branches_orders_m::get_bill_orders(" 
                AND branches_order.branch_bill_id = $bill_obj->branch_bill_id 
                AND branches_order.b_o_return = 0
            ");

            // in some way bill not have orders
            if(count($bill_orders) == 0)
            {
                $output["success"] = "error";
                $output["msg"] = "<div class='alert alert-danger'>الفاتورة لا تحتوي علي طلبات</div>";
                echo json_encode($output);
                return;
            }

            // get all bill returns orders
            $bill_return_orders = branches_orders_m::get_bill_orders(" 
                AND branches_order.branch_bill_id = $bill_obj->branch_bill_id 
                AND branches_order.b_o_return = 1
                AND branches_order.factory_accept_return = 1
                order by branches_order.branch_bill_id desc
            ");

            $output["return_orders_table"] = "";
            if (count($bill_return_orders))
            {
                $output["return_orders_table"] = \View::make("admin.subviews.branch_bills.with_factory.bill_returns_orders_table",
                    ["orders"=>$bill_return_orders])->render();
            }

            $output["bill_data"] = "<h3> فاتورة رقم.$bill_obj->branch_bill_id , لديها (".count($bill_orders).") عنصر</h3>";
            $output["bill_data"] .= "<h5> <i class='fa fa-clock-o'></i> بتاريخ ".Carbon::createFromTimestamp(strtotime($bill_obj->created_at))->diffForHumans()." - ($bill_obj->created_at) </h5>";

            $output["table_data"] = "<input type='hidden' value='".$bill_obj->branch_bill_id."' class='form-control' name='bill_id' >";


            $get_pro_arr = branches_products_m::all();
            $get_pro_arr = $get_pro_arr->groupBy("pro_id");


            $output["table_data"] .= \View::make("admin.subviews.branch_bills.with_factory.branch_products_table",
                [
                    "orders" => $bill_orders,
                    "return_orders" => $bill_return_orders,
                    "branch_product" => $get_pro_arr
                ])->render();


            echo json_encode($output);
            return;

        }

    }

    public function save_product_first_time(Request $request)
    {

        if (!check_permission($this->user_permissions,"branch/products_first_time","add_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["all_branches"] = [];
        $get_pro_cond = "";

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $get_pro_cond = "AND branch_pro.branch_id = $this->branch_id";
        }

        if(!count($all_branches)){
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يوجد فروع حاليا أضف فرع اولا</div>"
            )->send();
        }

        $this->data["all_branches"] = $all_branches;

        $get_all_products = products_m::get_pros();

        if (count($get_all_products) == 0)
        {
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا توجد منتجات حالية !!</div>"
            )->send();
        }


        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "pro_id"=>"required",
                    "b_p_quantity"=>"required|numeric|min:0",
                    "b_p_quantity_limit"=>"numeric|min:0",
                    "b_p_date"=>"required|date",
                    "branch_id"=>"required"
                ],
                [
                    "pro_id.required" => "المنتج مطلوب إدخالة",
                    "b_p_quantity.required" => "الكمية مطلوب إدخالة",
                    "b_p_date.required" => "التاريخ مطلوب إدخالة",
                    "branch_id.required" => "الفرع مطلوب إدخالة",
                ]
            );

            $request = clean($request->all());

            if ($this->data["current_user"]->user_type == "admin")
            {
                $branch_id = $request['branch_id'];
            }
            else{
                $branch_id = $this->branch_id;
            }

            $request["b_p_first_time"] = 1;

            $request["b_p_date"] = date("Y-m-d H:i:s",strtotime($request["b_p_date"]));

            $pro_id = $request["pro_id"];

            // check if there is first time to this product
            $get_check = branches_products_m::where("branch_id",$branch_id)
                ->where("pro_id",$pro_id)->get();
            $get_check = $get_check->all();
            if (count($get_check))
            {
                return Redirect::to('admin/branch_bills/save_product_first_time')->with(
                    "msg","<div class='alert alert-danger'>هذا المنتج موجود مسبقا برجاء عمل طلبيه من الفرع الرئيسي</div>"
                )->send();
            }

            branches_products_m::create($request);

            $msg = "<div class='alert alert-success'>  تم تسجيل بضاعه اول مدة بنجاح اضف جديد ..  </div>";

            #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " إدخال بضاعه اول مدة "
                );
            #endregion

            return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                "msg"=>$msg
            ])->send();

        }

        return view("admin.subviews.branch_bills.save_product_first_time",$this->data);
    }


    public function import_products_first_time_to_stock(Request $request)
    {

        $this->data["all_branches"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $get_pro_cond = "AND branch_pro.branch_id = $this->branch_id";
        }

        if(!count($all_branches)){
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يوجد فروع حاليا أضف فرع اولا</div>"
            )->send();
        }

        $this->data["all_branches"] = $all_branches;

        if ($request->method() == "POST")
        {

            if (!check_permission($this->user_permissions,"branch/products_first_time","add_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }

            $name = $_FILES['excel_file']['name'];
            $ext = (explode(".", $name));
            $branch_id = clean($request->get('branch_id'));
            $this->data['branch_id'] = $branch_id;

            if (!isset($branch_id) || !($branch_id > 0))
            {
                return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك إختار الفرع  </div>"
                ])->send();
            }

            if (empty($_FILES) || !is_array($ext) || !count($ext) || !isset($ext[1]) || !in_array($ext[1],["xls","XLS","xlsx","XLSX"]))
            {
                return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك ارفع ملف اكسيل فقط  </div>"
                ])->send();
            }

            $this->data['input_arr'] = [];
            $this->data['inserted_before_pros_barcode_arr'] = [];// دخل ليها قبل كده بضاعه اول مده
            $this->data['not_found_pros_barcode_arr'] = [];
            $this->data['duplicate_pros_barcode_arr'] = [];

            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

            Excel::load($_FILES['excel_file']['tmp_name'],function($reader)
            {

                // Getting all results
                $results = $reader->toArray();
                if (is_array($results) && count($results) > 0)
                {

                    foreach ($results as $key => $value)
                    {

                        $value['product_barcode'] = clean($value['product_barcode']);
                        $value['quantity'] = clean($value['quantity']);
                        $value['limit'] = clean($value['limit']);

                        if (empty($value['product_barcode']) || empty($value['quantity']))
                        {
                            continue;
                        }

                        // check if this product is exist or not
                        $check_exist_barcode = products_m::where("pro_barcode",$value['product_barcode'])
                            ->get()->first();

                        if (!is_object($check_exist_barcode))
                        {
                            $this->data['not_found_pros_barcode_arr'][] = $value['product_barcode'];
                            continue;
                        }

                        // check if this product is first time to add بضاعه اول مده
                        $get_check = branches_products_m::where("branch_id",$this->data['branch_id'])
                            ->where("pro_id",$check_exist_barcode->pro_id)->get();
                        $get_check = $get_check->all();
                        if (count($get_check))
                        {
                            $this->data['inserted_before_pros_barcode_arr'][] = $value['product_barcode'];
                            continue;
                        }


                        if (isset($this->data['input_arr'][$value['product_barcode']]))
                        {
                            $this->data['duplicate_pros_barcode_arr'][] = $value['product_barcode'];
                        }
                        else{
                            $this->data['input_arr'][$value['product_barcode']] = [
                                "branch_id" => $this->data['branch_id'],
                                "pro_id" => $check_exist_barcode->pro_id,
                                "pro_name" => $check_exist_barcode->pro_name,
                                "pro_barcode" => $check_exist_barcode->pro_barcode,
                                "b_p_quantity" => intval($value['quantity']),
                                "b_p_quantity_limit" => intval($value['limit']),
                                "b_p_date" => date('Y-m-d'),
                                "b_p_first_time" => 1
                            ];
                        }

                    }


                }

            });


        }

        return view("admin.subviews.branch_bills.save_product_first_time",$this->data);

    }


    public function approve_import_products_first_time_to_stock(Request $request)
    {
        if ($request->method() == "POST") {

            if (!check_permission($this->user_permissions, "branch/products_first_time", "add_action", $this->data["current_user"])) {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }

            $branch_ids = clean($request->get('branch_id'));
            $pro_ids = clean($request->get('pro_id'));
            $quantitys = clean($request->get('quantity'));
            $limits = clean($request->get('limit'));
            $input_arr = [];

            if (count($pro_ids) == count($branch_ids) &&  count($quantitys) == count($pro_ids)
             && count($limits) == count($pro_ids))
            {

                foreach($pro_ids as $key => $pro_id)
                {

                    $input_arr[$pro_id] = [
                        "branch_id" => $branch_ids[$key],
                        "pro_id" => $pro_id,
                        "b_p_quantity" => $quantitys[$key],
                        "b_p_quantity_limit" => $limits[$key],
                        "b_p_date" => date('Y-m-d'),
                        "b_p_first_time" => 1,
                        "created_at" => date('Y-m-d H:i:s'),
                        "updated_at" => date('Y-m-d H:i:s')
                    ];

                }

            }
            else{
                return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات المنتجات فارغه او غير صحيحة  </div>"
                ])->send();

            }

            if (count($input_arr))
            {

                $split_arr = array_chunk($input_arr,500);

                foreach ($split_arr as $key => $arr)
                {
                    branches_products_m::insert($arr);
                }


                $desc_msg = ' لقد تم تسجيل المنتجات اول مده بنجاح ';

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                    "msg" => "<div class='alert alert-success'> $desc_msg  </div>"
                ])->send();


            }
            else{
                return Redirect::to('admin/branch_bills/save_product_first_time')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات المنتجات فارغه او غير صحيحة  </div>"
                ])->send();
            }


        }
    }

    public function get_products_for_save_product_first_time(Request $request)
    {

        $output = [];
        $output["success"] = "";
        $output["msg"] = "";
        $output["options"] = "";

        $branch_id = clean($request->get("branch_id"));

        $get_all_products = products_m::get_pros();


        $get_all_products_ids = convert_inside_obj_to_arr($get_all_products,"pro_id");

        $get_all_branch_products = branches_products_m::get_branches_pros("
            AND branch_pro.branch_id = $branch_id
            AND branch_pro.b_p_first_time = 1
        ");


        $get_all_branch_products_pro_ids = [];

        if (count($get_all_branch_products))
        {
            $get_all_branch_products_pro_ids = convert_inside_obj_to_arr($get_all_branch_products,"pro_id");

        }

        $get_remaining_pros_ids = array_diff($get_all_products_ids,$get_all_branch_products_pro_ids);
        if (count($get_remaining_pros_ids) == 0)
        {
            $output["success"] = "error";
            $output["msg"] = "لا توجد منتجات لإدخال بضاعه اول مده قد تكون ادخلت مسبقا ";
            echo json_encode($output);
            return;
        }

        $get_remaining_pros_ids = implode(',',$get_remaining_pros_ids);
        $get_remaining_pros = products_m::get_pros("
            AND pro.pro_id in ($get_remaining_pros_ids)
        ");

        foreach($get_remaining_pros as $key => $pro)
        {
            $output["options"] .= "<option value='".$pro->pro_id."'>$pro->pro_name</option>";
        }
        $output["success"] = "success";

        echo json_encode($output);
        return;
    }

    public function get_cat_child_cats_and_products(Request $request,$cat_id=null,$return_this_data=false){


        $echo_json=false;
        if($cat_id==null){

            $cat_id=$request->get("cat_id");
            $cat_id=intval($cat_id);

            if(!($cat_id>0)){
                return;
            }

            $echo_json=true;
        }



        //get product parent categories
        $all_parent_cats=category_m::
        where("cat_type","product")->
        where("parent_id",$cat_id)->
        get();

        //get products in $all_parent_cats
        $all_parent_cats_products=products_m::get_pros("
                AND pro.cat_id=$cat_id
            ");


        $all_parent_cats_products=collect($all_parent_cats_products)->groupBy("cat_id");

        $cats_ids=[];
        if(count($all_parent_cats)){
            $cats_ids=$all_parent_cats->lists("cat_id")->all();
        }


        $this->data["all_parent_cats"]=$all_parent_cats->all();
        $this->data["all_parent_cats_products"]=$all_parent_cats_products->all();
        $this->data["loaded_cat_id"]=$cat_id;

        $this->data["cats_view"]=\View::make("admin.subviews.branch_bills.with_factory.blocks.load_cats_block",$this->data)->render();
        $this->data["products_view"]=\View::make("admin.subviews.branch_bills.with_factory.blocks.load_products_block",$this->data)->render();

        if($return_this_data){
            return $this->data;
        }

        if($echo_json){
            $output=[];

            $output["cats_ids"]=$cats_ids;
            $output["cats_view"]=$this->data["cats_view"];
            $output["products_view"]=$this->data["products_view"];

            echo json_encode($output);
        }


    }

    public function search_for_product(Request $request){


        $search_keyword=clean($request->get("search_keyword"));


        $products=products_m::get_pros("
                AND (
                    pro.pro_name like '%$search_keyword%' OR
                    pro.pro_barcode ='$search_keyword' 
                )
            ");


        $output["products_view"]=\View::make("admin.subviews.branch_bills.with_factory.blocks.search_products_block",["products"=>$products])->render();

        echo json_encode($output);
    }


}
