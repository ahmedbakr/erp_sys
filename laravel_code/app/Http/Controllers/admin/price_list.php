<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\admin\price_list_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class price_list extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if (!check_permission($this->user_permissions,"admin/price_list","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["all_price_list"] = price_list_m::all();


        return view("admin.subviews.price_list.show")->with($this->data);
    }


    public function save_price(Request $request, $price_id = null)
    {

        $default_action = "add_action";
        if ($price_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/price_list","$default_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $price_data= "";

        if ($price_id != null)
        {
            $price_data = price_list_m::findOrFail($price_id);
        }

        $this->data["price_data"] = $price_data;

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "price_title" => "required",
                "default_price" => "required"
            ]);

            $request = clean($request->all());

            if ($price_id == null)
            {
                // insert
                $price_obj = price_list_m::create($request);
                $price_id = $price_obj->price_id;
                $action_desc = "تم إضافة سعر جديد بعنوان ".$price_obj->price_title;

            }
            else{
                $price_data->update($request);
                $action_desc = "تم تعديل بيانات سعر ".$price_data->price_title;
            }

            #region save in site_tracking
                $this->track_my_action(
                    $action_desc
                );
            #endregion

            return  Redirect::to('admin/prices/save_price/'.$price_id)->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.price_list.save")->with($this->data);
    }


    public function remove_price(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/price_list","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تم مسح سعر"
        );
        #endregion


        $this->general_remove_item($request,'App\models\admin\price_list_m');
    }

}
