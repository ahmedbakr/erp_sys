<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\dashbaord_controller;
use App\Http\Controllers\is_admin_controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class uploader extends is_admin_controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->data['user_permissions']['uploader'][0]->show_action)
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> You Don't Have Permission to access this page !!  </div>"
            ])->send();
        }

    }


    //uploader
    public function index()
    {
        return view("admin.subviews.uploader.upload")->with($this->data);
    }


    public function load_files(Request $request)
    {
        // for multiple files
        $uploaded = $this->cms_upload(
            $request,
            $user_id = $this->user_id,
            $file_name = "file",
            $folder = "/general_uploader",
            $width = 0, $height = 0);
        echo $uploaded[0];
    }


}
