<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\bills\branch_bills_m;
use App\models\bills\supplier_bills_m;
use App\models\expenses\expenses_m;
use App\models\money_transfers_m;
use App\models\orders\branches_orders_m;
use App\models\orders\clients_orders_m;
use App\models\orders\suppliers_materials_m;
use App\models\product\branches_products_m;
use App\models\product\materials_m;
use App\models\product\product_materials_m;
use App\models\product\products_m;
use App\models\status_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class orders extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();


    }


    //not used method
    public function index()
    {

        // get suppliers , branch , clients count orders
        $this->data["supplier_orders"] = suppliers_materials_m::get_count();
        $this->data["branch_orders"] = branches_orders_m::get_count();
        $this->data["branch_clients_orders"] = clients_orders_m::get_count();

        return view("admin.subviews.orders.index",$this->data);
    }


    #region Supplier

    public function supplier_bills($supplier_id="",$from_date="",$to_date="",$group_by=""){

        if (!check_permission($this->user_permissions,"orders/suppliers","show_action"))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_suppliers"]=User::where("user_type","supplier")->get()->all();

        $this->data["group_by"] = $group_by;

        $cond_arr=[""];

        $this->data["selected_supplier_id"]=0;
        if(!empty($supplier_id)){
            $cond_arr[]=" AND supplier_bill.supplier_id=$supplier_id";
            $this->data["selected_supplier_id"]=$supplier_id;
        }

        $this->data["bill_date_from"]="";
        if(!empty($from_date)){
            $cond_arr[]=" AND supplier_bill.supplier_bill_date>='$from_date'";
            $this->data["bill_date_from"]=$from_date;
        }


        $this->data["bill_date_to"]="";
        if(!empty($to_date)){
            $cond_arr[]=" AND supplier_bill.supplier_bill_date<'$to_date'";
            $this->data["bill_date_to"]="$to_date";
        }


        $this->data["all_supplier_bills"]=supplier_bills_m::get_bill_data(implode(" ",$cond_arr));



        return view("admin.subviews.orders.supplier.show.all_bills",$this->data);
    }

    public function supplier_materials($bill_id)
    {

        if (!check_permission($this->user_permissions,"orders/suppliers","show_action"))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $bill_data=supplier_bills_m::get_bill_data(" AND supplier_bill_id=$bill_id");

        if(!isset_and_array($bill_data)){
            abort(404);
        }
        $bill_data=$bill_data[0];
        $this->data["bill_data"]=$bill_data;


        $cond = " AND sup_mat.supplier_bill_id = $bill_id";

        $this->data["all_orders"] = suppliers_materials_m::get_orders_data(" $cond order by sup_mat.created_at desc ");

        return view("admin.subviews.orders.supplier.show.bill_orders",$this->data);
    }

    public function supplier_materials_save_purchase_bill_orders(Request $request)
    {

        $this->data["item_data"] = "";

        $get_permission = $this->data['user_permissions']['orders/suppliers'][0]->additional_permissions;
        if (!empty($get_permission))
        {
            $get_permission = json_decode($get_permission);

            if ( !is_array($get_permission) || !in_array("add_purchase_bill",$get_permission))
            {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }
        }


        $this->data["materials"] = materials_m::all();

        // get all suppliers
        $this->data['suppliers'] = User::get_users(" AND u.user_type = 'supplier' AND u.user_active = 1 AND u.user_can_login = 1 ");


        if($request->method() == "POST") {

            $this->validate($request, [

                "mat_id.*" => "required",
                "sup_mat_user_id" => "required",
                "sup_mat_amount.*" => "required|min:1|numeric",
                "sup_mat_price.*" => "required|min:0|numeric",
                "supplier_bill_total_paid_in_cash" => "min:0|numeric",
                "supplier_bill_total_paid_in_atm" => "min:0|numeric",
                "supplier_bill_date" => "required",

            ]);

            if(!check_permission($this->user_permissions,"factory/atm_permission","show_action")){
                $request["supplier_bill_total_paid_in_atm"]=0;
            }

            $request["supplier_bill_total_paid"]=
                $request["supplier_bill_total_paid_in_cash"]+$request["supplier_bill_total_paid_in_atm"];



            $request_data = $this->cleaning_input($request->all(), $except = array());
            $bill_orders_arr = array();
            $bill_input_arr = array();
            $expenses_arr = array();
            $product_materials_arr = array();

            // bill arr data
            $bill_input_arr["supplier_bill_total_amount"] = 0;
            $bill_input_arr["supplier_id"] = $request_data["sup_mat_user_id"];
            $bill_input_arr["supplier_bill_extra_cost"] = $request_data["expense_amount"];
            $bill_input_arr["supplier_bill_total_paid"] = $request_data["supplier_bill_total_paid"];
            $bill_input_arr["supplier_bill_date"] = $request_data["supplier_bill_date"];
            $bill_input_arr["supplier_bill_total_paid_in_cash"] = $request_data["supplier_bill_total_paid_in_cash"];
            $bill_input_arr["supplier_bill_total_paid_in_atm"] = $request_data["supplier_bill_total_paid_in_atm"];

            // expenses arr data
            $expenses_arr["user_id"] = $this->factory_id;
            $expenses_arr["expense_type_id"] = 1;
            $expenses_arr["expense_name"] = $request_data["expense_name"];
            $expenses_arr["expense_desc"] = $request_data["expense_desc"];
            $expenses_arr["expense_amount"] = $request_data["expense_amount"];
            $expenses_arr["expense_date"] = Carbon::now();


            $total_bill_price = [];
            foreach($request_data["sup_mat_amount"] as $key => $value)
            {
                $index_mat_id = $request_data["mat_id"][$key];

                if (isset($bill_orders_arr[$index_mat_id]["mat_id"]))
                {
                    // count price avg
                    $new_price = (
                        (
                            ($bill_orders_arr[$index_mat_id]["sup_mat_amount"] * $bill_orders_arr[$index_mat_id]["sup_mat_price"])
                            +
                            ($request_data["sup_mat_amount"][$key] * $request_data["sup_mat_price"][$key])
                        ) /
                        ($bill_orders_arr[$index_mat_id]["sup_mat_amount"] + $request_data["sup_mat_amount"][$key])
                    );
                    $bill_orders_arr[$index_mat_id]["sup_mat_amount"] += $request_data["sup_mat_amount"][$key];
                    $bill_orders_arr[$index_mat_id]["sup_mat_price"] = $new_price;
                }
                else{
                    $bill_orders_arr[$index_mat_id]["sup_mat_user_id"] = $request_data["sup_mat_user_id"];
                    $bill_orders_arr[$index_mat_id]["mat_id"] = $request_data["mat_id"][$key];
                    $bill_orders_arr[$index_mat_id]["sup_mat_amount"] = $request_data["sup_mat_amount"][$key];
                    $bill_orders_arr[$index_mat_id]["sup_mat_price"] = $request_data["sup_mat_price"][$key];
                    $bill_orders_arr[$index_mat_id]["sup_mat_barcode"] = $request_data["sup_mat_barcode"][$key];
                    $bill_orders_arr[$index_mat_id]["sup_mat_desc"] = $request_data["sup_mat_desc"][$key];
                    $bill_orders_arr[$index_mat_id]["created_at"] = Carbon::now();
                    $bill_orders_arr[$index_mat_id]["updated_at"] = Carbon::now();

                }

                $total_bill_price[$index_mat_id] = ($bill_orders_arr[$index_mat_id]["sup_mat_price"] * $bill_orders_arr[$index_mat_id]["sup_mat_amount"]);
            }

            $bill_input_arr["supplier_bill_total_amount"] = array_sum($total_bill_price);

            if($request["supplier_bill_total_paid"]>$bill_input_arr["supplier_bill_total_amount"]){
                return Redirect::to('admin/order/supplier_materials/save_purchase_bill_orders')->with([
                    "msg" => "<div class='alert alert-danger'> لا يمكنك ان تدفع للفاتورة اكتر من تكلفتها   </div>"
                ])->send();
            }


            #region add new bill

            $bill_obj = supplier_bills_m::create($bill_input_arr);
            if (!is_object($bill_obj))
            {
                return Redirect::back()->withErrors([
                    "msg" => "<div class='alert alert-danger'> لم يتم حفظ الفاتورة برجاء المحاولة مرة اخري  </div>"
                ])->send();
            }

            // add bill orders
            foreach($bill_orders_arr as $key => $value)
            {
                $bill_orders_arr[$key]["supplier_bill_id"] = $bill_obj->supplier_bill_id;
            }

            suppliers_materials_m::insert($bill_orders_arr);

            $desc_msg = " إضافة فاتورة برقم.".$bill_obj->supplier_bill_id;

            #region save in money_transfers
            //save cash,atm paid and remain money

            $factory_obj=User::find($this->factory_id);

            //paid cash
            if($request["supplier_bill_total_paid_in_cash"]>0){
                money_transfers_m::create([
                    "from_user_id"=>$this->factory_id,
                    "to_user_id"=>$request_data["sup_mat_user_id"],
                    "m_t_bill_id"=>$bill_obj->supplier_bill_id,
                    "m_t_bill_type"=>"supplier_bill",
                    "m_t_amount"=>$request["supplier_bill_total_paid_in_cash"],
                    "m_t_amount_type"=>"cash",
                    "m_t_status"=>"1",
                    "m_t_desc"=>"$desc_msg",
                ]);

                $factory_obj->update([
                    "user_balance"=>($factory_obj->user_balance-$request["supplier_bill_total_paid_in_cash"])
                ]);
            }

            //paid atm
            if($request["supplier_bill_total_paid_in_atm"]>0){
                money_transfers_m::create([
                    "from_user_id"=>$this->factory_id,
                    "to_user_id"=>$request_data["sup_mat_user_id"],
                    "m_t_bill_id"=>$bill_obj->supplier_bill_id,
                    "m_t_bill_type"=>"supplier_bill",
                    "m_t_amount"=>$request["supplier_bill_total_paid_in_atm"],
                    "m_t_amount_type"=>"atm",
                    "m_t_status"=>"1",
                    "m_t_desc"=>"$desc_msg",
                ]);

                $factory_obj->update([
                    "user_atm_balance"=>($factory_obj->user_atm_balance-$request["supplier_bill_total_paid_in_atm"])
                ]);
            }

            //remain
            if($bill_obj->supplier_bill_total_amount>$request["supplier_bill_total_paid"]){
                money_transfers_m::create([
                    "from_user_id"=>$this->factory_id,
                    "to_user_id"=>$request_data["sup_mat_user_id"],
                    "m_t_bill_id"=>$bill_obj->supplier_bill_id,
                    "m_t_bill_type"=>"supplier_bill",
                    "m_t_amount"=>($bill_obj->supplier_bill_total_amount-$request["supplier_bill_total_paid"]),
                    "m_t_amount_type"=>"",
                    "m_t_status"=>"0",
                    "m_t_desc"=>"$desc_msg",
                ]);
            }

            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion


            #region send email
            $this->_send_email_to_all_users_type(
                $user_type = "admin" ,
                $data = $desc_msg,
                $subject = $desc_msg
            );
            #endregion


            #endregion


            #region expenses

                if(!empty($request_data["expense_amount"]))
                {
                    $expense_obj = expenses_m::create($expenses_arr);

                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " إضافة مصروف برقم.$expense_obj->expense_id من الفاتورة رقم.".$bill_obj->supplier_bill_id
                    );
                    #endregion

                }

            #endregion


            #region add to product materials

            foreach($request_data["sup_mat_amount"] as $key => $value)
            {
                $current_mat_id = $request_data["mat_id"][$key];
                $current_mat_price = $request_data["sup_mat_price"][$key];
                $current_mat_quantity = $request_data["sup_mat_amount"][$key];

                // check if exist
                $mat_old_obj = product_materials_m::where("mat_id",$current_mat_id)->get()->first();
                if (is_object($mat_old_obj))
                {

                    // update price and quantity
                    $new_quantity = ($current_mat_quantity + $mat_old_obj->mat_amount);
                    $new_price = (
                        (
                            ($current_mat_quantity * $current_mat_price)+($mat_old_obj->mat_amount * $mat_old_obj->mat_price)
                        )/$new_quantity
                    );

                    product_materials_m::where("mat_id",$current_mat_id)->update([
                        "mat_amount" => $new_quantity,
                        "mat_price" => $new_price
                    ]);

                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = "تعديل الماده الخام رقم.$mat_old_obj->pro_mat_id في الكمية الخاص بها والسعر"
                        );
                    #endregion

                }
                else{

                    $product_materials_arr["mat_id"] = $current_mat_id;
                    $product_materials_arr["mat_amount"] = $current_mat_quantity;
                    $product_materials_arr["mat_price"] = $current_mat_price;

                    $new_mat = product_materials_m::create($product_materials_arr);
                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = "إضافة ماده خام جديدة برقم.".$new_mat->pro_mat_id
                    );
                    #endregion

                }


            }


            return Redirect::to('admin/order/supplier_materials/save_purchase_bill_orders')->with([
                "msg" => "<div class='alert alert-success'> لقد تم حفظ الفاتورة بنجاح  </div>"
            ])->send();

            #endregion


        }


        return view("admin.subviews.orders.supplier.save_purchase_bill_orders",$this->data);
    }

    public function supplier_materials_save_return_bill_orders(Request $request,$bill_id="")
    {
        if (!check_permission($this->user_permissions,"orders/suppliers","add_return_bill"))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["bill_id"]=$bill_id;


        $this->data["count_bills"] = supplier_bills_m::get_count();


        return view("admin.subviews.orders.supplier.save_return_bill_orders",$this->data);
    }

    public function get_bill_orders(Request $request)
    {

        $output = array();
        $output["success"] = "error";
        $output["msg"] = "";

        $get_permission = $this->data['user_permissions']['orders/suppliers'][0]->additional_permissions;
        if (!empty($get_permission))
        {
            $get_permission = json_decode($get_permission);

            if (!in_array("add_return_bill",$get_permission))
            {

                $output["msg"] = "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>";
                echo json_encode($output);
                return;
            }
        }

        if ($request->method() == "POST")
        {
            $bill_id = clean($request->get("bill_id"));
            if (!isset($bill_id) && empty($bill_id))
            {
                $output["msg"] = "<div class='alert alert-danger'> برجاء إدخال رقم الفاتورة اولا </div>";
                echo json_encode($output);
                return;
            }

            $bill_obj = supplier_bills_m::get_bill_data(" AND supplier_bill.supplier_bill_id = $bill_id ");
            if (count($bill_obj) == 0)
            {
                $output["msg"] = "<div class='alert alert-danger'> الفاتورة غير موجودة</div>";
                echo json_encode($output);
                return;
            }

            $bill_obj = $bill_obj[0];
            $output["success"] = "success";

            // get all bill purchases orders
            $bill_orders = suppliers_materials_m::get_orders_data(" 
                AND sup_mat.supplier_bill_id = $bill_obj->supplier_bill_id 
                AND sup_mat.sup_mat_return = 0
            ");

            // in some way bill not have orders
            if(count($bill_orders) == 0)
            {
                $output["success"] = "error";
                $output["msg"] = "<div class='alert alert-danger'> لا توجد طلبات لهذة الفاتورة</div>";
                echo json_encode($output);
                return;
            }

            // get all bill returns orders
            $bill_return_orders = suppliers_materials_m::get_orders_data(" 
                AND sup_mat.supplier_bill_id = $bill_obj->supplier_bill_id 
                AND sup_mat.sup_mat_return = 1
                order by sup_mat.created_at desc
            ");

            $output["return_orders_table"] = "";
            if (count($bill_return_orders))
            {
                $output["return_orders_table"] = \View::make("admin.subviews.orders.supplier.bill_returns_orders_table",
                    ["orders"=>$bill_return_orders])->render();
            }

            $output["bill_data"] = "<h3> الفاتورة رقم.$bill_obj->supplier_bill_id , لديها (".count($bill_orders).") عنصر</h3>";
            $output["bill_data"] .= "<h5> <i class='fa fa-clock-o'></i> أنشأت من ".Carbon::createFromTimestamp(strtotime($bill_obj->supplier_bill_created))->diffForHumans()." </h5>";
            $output["bill_data"] .= "<h3> <i class='fa fa-user'></i> $bill_obj->full_name </h3>";
            $output["bill_data"] .= "<h3> <small>";
            $output["bill_data"] .= "<b>التكاليف الكليه : </b> $bill_obj->supplier_bill_total_amount - ";
            $output["bill_data"] .= "<b>المدفوع : </b> $bill_obj->supplier_bill_total_paid - ";
            $output["bill_data"] .= "<b>مصاريف اخري : </b> $bill_obj->supplier_bill_extra_cost";
            $output["bill_data"] .= "</small> </h3>";

            $output["table_data"] = "<input type='hidden' value='".$bill_obj->supplier_bill_id."' class='form-control' name='bill_id' >";


            $get_mat_arr = product_materials_m::all();
            $get_mat_arr = $get_mat_arr->groupBy("mat_id");

            $output["table_data"] .= \View::make("admin.subviews.orders.supplier.bill_purchases_orders_table",
                [
                    "orders" => $bill_orders,
                    "return_orders" => $bill_return_orders,
                    "product_materials" => $get_mat_arr
                ])->render();

            echo json_encode($output);
            return;

        }


    }

    public function make_return_bill(Request $request)
    {

        $get_permission = $this->data['user_permissions']['orders/suppliers'][0]->additional_permissions;
        if (!empty($get_permission))
        {
            $get_permission = json_decode($get_permission);

            if (!in_array("add_return_bill",$get_permission))
            {

                $output["msg"] = "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>";
                echo json_encode($output);
                return;
            }
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "bill_id" => "required",
                "returned_quantity.*" => "required|numeric|min:0",
                "order_id.*" => "required",
            ]);

            $request_data = $this->cleaning_input($request->all());

//            dump($request_data);
//            die();

            if ((count($request_data["returned_quantity"]) != count($request_data["order_id"])) ||
                empty($request_data["bill_id"]))
            {
                return Redirect::to('admin/order/supplier_materials/get_bill_orders')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحة  </div>"
                ])->send();
            }

            $bill_id = $request_data["bill_id"];
            $returned_quantity = $request_data["returned_quantity"];
            $order_id = $request_data["order_id"];


            // check if bill is exist

            $bill_obj = supplier_bills_m::get_bill_data(" AND supplier_bill.supplier_bill_id = $bill_id ");
            if (count($bill_obj) == 0)
            {
                return Redirect::to('admin/order/supplier_materials/get_bill_orders')->with([
                    "msg" => "<div class='alert alert-danger'> الفاتورة غير موجودة  </div>"
                ])->send();
            }

            $bill_obj = $bill_obj[0];
            $returns_input_arr = array();
            $total_price = 0;
            $supplier_id = $bill_obj->supplier_id;
            $index_counter = 0;

            // get all bill returns orders if exist
            $bill_return_orders = suppliers_materials_m::get_orders_data(" 
                        AND sup_mat.supplier_bill_id = $bill_obj->supplier_bill_id 
                        AND sup_mat.sup_mat_return = 1
                    ");
            $bill_return_orders = collect($bill_return_orders);
            $bill_return_orders = $bill_return_orders->groupBy("mat_id");

            foreach($order_id as $key => $id)
            {

                // check if this order item is exist to this bill_id and this quantity is larger than return
                $order_check_obj = suppliers_materials_m::where("sup_mat_id",$id)->get()->first();
                if(
                    is_object($order_check_obj) && !empty($order_check_obj) &&
                    $order_check_obj->supplier_bill_id == $bill_id &&
                    $order_check_obj->sup_mat_amount >= $returned_quantity[$key] &&
                    $returned_quantity[$key] > 0 )
                {

                    // check if total returns quantity to this item in bill less than purchase orders to this item

                    if (empty($bill_return_orders) || !isset($bill_return_orders[$order_check_obj->mat_id]) ||
                        ( isset($bill_return_orders[$order_check_obj->mat_id]) &&
                            ($order_check_obj->sup_mat_amount - array_sum(convert_inside_obj_to_arr($bill_return_orders[$order_check_obj->mat_id],"sup_mat_amount"))) >= $returned_quantity[$key] ))
                    {

                        // check if quantity in stock larger than or equal returned and update it
                        $product_mat_obj = product_materials_m::where("mat_id",$order_check_obj->mat_id)->get()->first();
                        if (is_object($product_mat_obj) && $product_mat_obj->mat_amount >= $returned_quantity[$key])
                        {

                            $final_stock_quantity = ($product_mat_obj->mat_amount - $returned_quantity[$key]);

                            product_materials_m::where("mat_id",$order_check_obj->mat_id)->update([
                                "mat_amount" => $final_stock_quantity
                            ]);

                            $get_material_obj = materials_m::where("mat_id",$order_check_obj->mat_id)->get()->first();

                            // check if return quantity reach to material limit or less than it
                            if ($final_stock_quantity <= $product_mat_obj->mat_pro_limit)
                            {
                                #region save in notification
                                    $this->send_all_user_type_notifications(
                                        $not_title = " الماده الخام  $get_material_obj->mat_name تجاوزت حد الطلب " ,
                                        $not_type = "danger" ,
                                        $user_type = "admin"
                                    );
                                #endregion


                                #region send email
                                    $this->_send_email_to_all_users_type(
                                        $user_type = "admin" ,
                                        $data = " الماده الخام  $get_material_obj->mat_name  تجاوزت حد الطلب " ,
                                        $subject = "تنبيه لوصول الماده الخام لحد الطلب"
                                    );
                                #endregion
                            }

                            $desc_msg = " تعديل كمية الماده الخام $get_material_obj->mat_name من ".$product_mat_obj->mat_amount." الي ".($product_mat_obj->mat_amount - $returned_quantity[$key]);

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                            #endregion

                            #region save in notification
                            $this->send_all_user_type_notifications(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_type = "admin"
                            );
                            #endregion


                            #region send email
                            $this->_send_email_to_all_users_type(
                                $user_type = "admin" ,
                                $data = $desc_msg,
                                $subject = "تعديل كمية الماده الخام"
                            );
                            #endregion

                            unset($order_check_obj["sup_mat_id"], $order_check_obj["created_at"], $order_check_obj["updated_at"]);
                            $returns_input_arr[$index_counter]["supplier_bill_id"] = $bill_id;
                            $returns_input_arr[$index_counter]["sup_mat_user_id"] = $order_check_obj["sup_mat_user_id"];
                            $returns_input_arr[$index_counter]["mat_id"] = $order_check_obj["mat_id"];
                            $returns_input_arr[$index_counter]["sup_mat_desc"] = $order_check_obj["sup_mat_desc"];
                            $returns_input_arr[$index_counter]["sup_mat_barcode"] = $order_check_obj["sup_mat_barcode"];
                            $returns_input_arr[$index_counter]["sup_mat_amount"] = $returned_quantity[$key];
                            $returns_input_arr[$index_counter]["sup_mat_price"] = ($order_check_obj->sup_mat_price);
                            $returns_input_arr[$index_counter]["sup_mat_return"] = 1;
                            $returns_input_arr[$index_counter]["created_at"] = Carbon::now();
                            $returns_input_arr[$index_counter]["updated_at"] = Carbon::now();
                            $index_counter++;
                            $total_price += ($returned_quantity[$key] * $order_check_obj->sup_mat_price);

                        }

                    }
                }

            }

            if(count($returns_input_arr))
            {
                suppliers_materials_m::insert($returns_input_arr);
                
                $desc_msg = " إضافة فاتورة مرتجع برقم.$bill_id";

                #region save in money transfer
                money_transfers_m::create([
                    "from_user_id" =>$supplier_id,
                    "to_user_id" =>$this->factory_id,
                    "m_t_bill_id" =>$bill_id,
                    "m_t_bill_type"=>"supplier_bill",
                    "m_t_amount" =>$total_price,
                    "m_t_status" =>0,
                    "m_t_desc" => "مرتجع من فاتورة رقم.$bill_id",
                ]);
                #endregion

                //update supplier_bill_total_return_money col in supplier bill
                $supplier_bill_obj=supplier_bills_m::find($bill_id);
                $supplier_bill_obj->update([
                    "supplier_bill_total_return_money"=>($supplier_bill_obj->supplier_bill_total_return_money+$total_price)
                ]);


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region send email
                $this->_send_email_to_all_users_type(
                    $user_type = "admin" ,
                    $data = $desc_msg,
                    $subject = "مرتجع علي الفاتورة"
                );
                #endregion

                return Redirect::to('admin/order/supplier_materials/save_return_bill_orders/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-success'> لقد تم حفظ الطلبات بنجاح  </div>"
                ])->send();

            }
            else{
                return Redirect::to('admin/order/supplier_materials/save_return_bill_orders/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-danger'> لم يتم حفظ الطلبات بسبب بيانات غير صالحه او نقص فالكمية </div>"
                ])->send();
            }


        }

    }

    #endregion



    #region Branch

    public function show_branch_orders()
    {

        if (!check_permission($this->user_permissions,"branch_orders","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $cond = "";

        if (isset($_GET["start_date"]) && !empty($_GET["start_date"]))
        {
            $start_date = $_GET["start_date"];
            $cond .= " AND bill.branch_bill_date >= '$start_date' ";
        }

        if (isset($_GET["end_date"]) && !empty($_GET["end_date"]))
        {
            $end_date = $_GET["end_date"];
            $cond .= " AND bill.branch_bill_date <= '$end_date' ";
        }

        $this->data["all_branches"] = [];

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");

            if (isset($_GET["branch_id"]) && $_GET["branch_id"] > 0)
            {
                $branch_id = $_GET["branch_id"];
                $cond .= " AND bill.branch_id = $branch_id ";
            }

            $this->data["branch_bills"]=branch_bills_m::get_branch_user_bills(" $cond ");

        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $cond .= " AND bill.branch_id = $this->branch_id ";
            $this->data["branch_bills"]=branch_bills_m::get_branch_user_bills(" $cond ");
        }

        $this->data["all_branches"] = $all_branches;

        return view("admin.subviews.orders.branch.show_branch_orders",$this->data);
    }

    public function show_branch_bill_orders($bill_id)
    {

        if (!check_permission($this->user_permissions,"branch_orders","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

//        $get_permission = $this->data['user_permissions']['branch_orders'][0]->additional_permissions;
//        $this->data["additional_permission"] = json_decode($get_permission);

        $this->data["all_status"]=status_m::where("user_type","admin")->get()->all();

        if ($this->data["current_user"]->user_type == "admin")
        {
            $branch_bill_data = branch_bills_m::get_branch_user_bills(" AND bill.branch_bill_id = $bill_id ");

        }
        else{
            $branch_bill_data = branch_bills_m::get_branch_user_bills(" 
                AND bill.branch_id = $this->branch_id
                AND bill.branch_bill_id = $bill_id
             ");
        }


        if (is_array($branch_bill_data) && count($branch_bill_data) && isset($branch_bill_data[0]))
        {
            $this->data["branch_bill_data"] = $branch_bill_data[0];
            $this->data["branch_bill_orders"] = branches_orders_m::get_bill_orders(" AND branch_bill.branch_bill_id = $bill_id AND branches_order.b_o_return = 0 ");
        }
        else{
            return Redirect::to('admin/order/branch_orders')->with([
                "msg" => "<div class='alert alert-danger'> فاتورة الفرع غير موجودة </div>"
            ])->send();
        }

        return view("admin.subviews.orders.branch.show_branch_bill_orders",$this->data);
    }

    public function change_bill_status(Request $request)
    {
//
//        $get_permission = $this->data['user_permissions']['branch_orders'][0]->additional_permissions;
//        $this->data["additional_permission"] = json_decode($get_permission);

        $output = array();

        if (!check_permission($this->user_permissions,"branch_orders","manage_bill_orders",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $branch_user = User::find($obj->branch_id);

            $make_update=true;

            if(!in_array($accept,[2,3,6])){
                $output["msg"] ="ليس لديك الصلاحية للدخول لهذة الصفحة !!"."<br>";
                $make_update=false;
            }

            if(!in_array($obj->status_id,[1,2])&&$accept==6){
                $output["msg"] ="لا يمكن الغاء الفاتورة اذا كانت في حاله الانتظار"."<br>";
                $make_update=false;

            }

            if(in_array($obj->status_id,[3,4,5,6])){
                $output["msg"] ="لا يمكن تغيير حالة الفاتورة اذا كانت حالتها ارسلت او رفضت"."<br>";
                $make_update=false;

            }


            if($make_update)
            {


                if ($accept==3)
                {

                    //get bill orders and check if you have these quantity on stock

                    //get bill's orders
                    $all_bill_orders=branches_orders_m::get_bill_orders(" 
                        AND branch_bill.branch_bill_id=$item_id
                        AND branches_order.b_o_return = 0
                    ");

                    //minus quantity form factory stock if allowed

                    $input_edit_arr = [];
                    foreach($all_bill_orders as $order_key => $order)
                    {

                        //check if there is a product in branches_products
                        //if true add quantity to this  row

                        $product_on_stock = products_m::get_pros(" AND pro.pro_id = $order->pro_id ");
                        $product_on_stock = $product_on_stock[0];


                        if ($product_on_stock->pro_quantity >= $order->b_o_quantity)
                        {
                            $input_edit_arr[$product_on_stock->pro_id] = ($product_on_stock->pro_quantity - $order->b_o_quantity);
                        }
                        else{
                            $output["msg"] = "لا توجد كمية كافية من المنتج $product_on_stock->pro_name";
                            echo json_encode($output);
                            return;
                        }

                    }

                    if (count($input_edit_arr))
                    {
                        // minus product quantity on stock
                        foreach ($input_edit_arr as $pro_id => $quantity)
                        {
                            $old_pro_obj = products_m::find($pro_id);

                            products_m::find($pro_id)->update([
                                "pro_quantity" => $quantity
                            ]);

                            #region save in site_tracking
                                $this->track_my_action(
                                    $action_desc = " تعديل كمية المنتج رقم.$pro_id من $old_pro_obj->pro_quantity الي $quantity "
                                );
                            #endregion

                            // check if exceed pro_limit
                            if ($quantity <= $old_pro_obj->pro_limit)
                            {
                                $desc_msg = "المنتج '".$old_pro_obj->pro_name."' وصلت كميته لحد الطلب ";
                                #region save in notification
                                    $this->send_all_user_type_notifications(
                                        $not_title = $desc_msg ,
                                        $not_type = "info" ,
                                        $user_type = "admin"
                                    );
                                #endregion

                                #region send email
                                    $this->_send_email_to_all_users_type(
                                        $user_type = "admin" ,
                                        $data = $desc_msg,
                                        $subject = "وصول كمية المنتج لحد الطلب"
                                    );
                                #endregion
                            }

                        }
                    }

                }

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                if (in_array($accept,[2,3,6]) && $return_statues)
                {
                    $get_status_obj = status_m::find($accept);

                    $track_desc_msg = "تعديل حاله الفاتورة رقم.$item_id لتكون $get_status_obj->status_label";
                    $desc_msg = "حاله الفاتورة رقم.$item_id تغيرت لتكون $get_status_obj->status_label";

                    #region save in notification
                        $this->send_user_notification(
                            $not_title = $desc_msg ,
                            $not_type = "info" ,
                            $user_id = $obj->branch_id
                        );
                    #endregion

                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = $track_desc_msg
                        );
                    #endregion

                    #region send email
                        $this->_send_email_to_custom(
                            $emails = array($branch_user->email) ,
                            $data = $desc_msg,
                            $subject = "تغيير حالة الفاتورة رقم.$item_id"
                        );
                    #endregion
                }

            }
            
            if ($accept == 6)
            {
                $output["msg"] = " لقد تم الغاء طلبات الفاتورة ";
            }
            elseif($accept == 3)
            {
                $output["msg"] = " لقد تم ارسال طلبات الفاتورة للفرع ";
            }
            else{
                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
            }
            
        }


        echo json_encode($output);
    }

    public function show_branch_bill_returns($bill_id)
    {

        if (!check_permission($this->user_permissions,"branch_orders","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

//        $get_permission = $this->data['user_permissions']['branch_orders'][0]->additional_permissions;
//        $this->data["additional_permission"] = json_decode($get_permission);

        if ($this->data["current_user"]->user_type == "admin")
        {
            $branch_bill_data = branch_bills_m::get_branch_user_bills(" AND bill.branch_bill_id = $bill_id ");
        }
        else{

            $branch_bill_data = branch_bills_m::get_branch_user_bills(" 
                AND bill.branch_id = $this->branch_id
                AND bill.branch_bill_id = $bill_id
             ");
        }

        if (is_array($branch_bill_data) && count($branch_bill_data) && isset($branch_bill_data[0]))
        {
            $this->data["branch_bill_data"] = $branch_bill_data[0];
            $this->data["branch_bill_orders"] = branches_orders_m::get_bill_orders(" AND branch_bill.branch_bill_id = $bill_id AND branches_order.b_o_return = 1 ");
        }
        else{
            return Redirect::to('admin/order/branch_orders')->with([
                "msg" => "<div class='alert alert-danger'> الفاتورة غير موجودة </div>"
            ])->send();
        }

        return view("admin.subviews.orders.branch.show_branch_bill_returns",$this->data);
    }

    public function change_bill_return_status(Request $request)
    {

//        $get_permission = $this->data['user_permissions']['branch_orders'][0]->additional_permissions;
//        $this->data["additional_permission"] = json_decode($get_permission);

        $output = array();

        if (!check_permission($this->user_permissions,"branch_orders","manage_bill_return_orders",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $branch_user = User::find($obj->branch_id);
            $make_update=true;


            if($obj->factory_accept_return == 1 && $accept == 0){
                $output["msg"] = "لا يمكن تغيير الحالة الي الرفض"."<br>";
                $make_update=false;

            }


            if($make_update)
            {

                if ($accept == 1)
                {

                    //get product from branch stock
                    $branch_product_on_stock = branches_products_m::where("branch_id",$obj->branch_id)->where("pro_id",$obj->pro_id)->get()->first();

                    if ($obj->b_o_quantity > $branch_product_on_stock->b_p_quantity)
                    {
                        $output["msg"]="<div class='alert alert-danger'> كمية المنتج في مخزن الفرع غير كافية </div>";
                        echo json_encode($output);
                        return;
                    }

                    // update branch product
                    $new_quantity = ($branch_product_on_stock->b_p_quantity - $obj->b_o_quantity);
                    branches_products_m::where("branch_id",$obj->branch_id)->where("pro_id",$obj->pro_id)->get()->first()->update([
                        "b_p_quantity" =>$new_quantity
                    ]);

                    $desc_msg = " تعديل كمية المنتج.$obj->pro_id للفرع رقم.$obj->branch_id ' من $branch_product_on_stock->b_p_quantity الي $new_quantity ";
                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = $desc_msg
                        );
                    #endregion

                    // update branches_orders
                    branches_orders_m::find($item_id)->update([
                        "factory_accept_return" => 1
                    ]);

                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = " الموافقه علي طلبات المرتجع من الفرع رقم.$obj->branch_id من الفاتورة رقم.$obj->branch_bill_id "
                        );
                    #endregion

                    #region save in notification
                        $this->send_user_notification(
                            $not_title = $desc_msg ,
                            $not_type = "info" ,
                            $user_id = $obj->branch_id
                        );
                    #endregion

                    #region send email
                        $this->_send_email_to_custom(
                            $emails = array($branch_user->email) ,
                            $data = $desc_msg,
                            $subject = "منتجات الفروع"
                        );
                    #endregion


                    // check to branch product limit
                    if($branch_product_on_stock->b_p_quantity_limit >= $new_quantity)
                    {
                        $desc_msg = " المنتج رقم.$branch_product_on_stock->pro_id تجاوزت كميته حد الطلب ";
                        #region save in notification
                            $this->send_user_notification(
                                $not_title = $desc_msg ,
                                $not_type = "info" ,
                                $user_id = $obj->branch_id
                            );
                        #endregion


                        #region send email
                            $this->_send_email_to_custom(
                                $emails = array($branch_user->email) ,
                                $data = $desc_msg,
                                $subject = "حد الطلب لمنتجات الفرع"
                            );
                        #endregion

                    }


                    // add the returned quantity to factory stock
                    $factory_product = products_m::find($obj->pro_id);
                    $new_product_quantity = ($factory_product->pro_quantity + $obj->b_o_quantity);
                    products_m::find($obj->pro_id)->update([
                        "pro_quantity" => $new_product_quantity
                    ]);

                    $desc_msg = " ارجاع كمية $obj->b_o_quantity من الفاتورة رقم.$obj->branch_bill_id للفرع رقم.$obj->branch_id ";

                    #region save in site_tracking
                        $this->track_my_action(
                            $action_desc = $desc_msg
                        );
                    #endregion

                    #region save in notification
                        $this->send_all_user_type_notifications(
                            $not_title = $desc_msg ,
                            $not_type = "info" ,
                            $user_type = "admin"
                        );
                    #endregion

                    #region send email
                    $this->_send_email_to_all_users_type(
                        $user_type = "admin" ,
                        $data = $desc_msg,
                        $subject = "ارجاع كمية للمنتج في المخزن"
                    );
                    #endregion

                }

                $output["msg"] = " لقد تم الارجاع بنجاح ";

            }


        }


        echo json_encode($output);
    }


    public function remove_branch_bill(Request $request)
    {

        if (!check_permission($this->user_permissions,"branch_orders","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $item_id = (int)$request->get("item_id");
        if ($item_id > 0)
        {

            $obj = branch_bills_m::find($item_id);
            if (is_object($obj))
            {

                if ($obj->status_id < 4)
                {
                    #region save in site_tracking
                    $this->track_my_action(
                        $action_desc = " تم مسح فاتورة طلبات "
                    );
                    #endregion

                    $this->general_remove_item($request,'App\models\bills\branch_bills_m');

                    // delete bill orders
                    branches_orders_m::where("branch_bill_id",$obj->branch_bill_id)->delete();
                    return;
                }
                else{
                    echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح بمسح هذة الفاتورة</div>"]);
                    return;
                }


            }
            else{
                echo json_encode(["msg"=>"<div class='alert alert-danger'>الفاتورة غير موجودة</div>"]);
                return;
            }


        }
        else{
            echo json_encode(["msg"=>"<div class='alert alert-danger'>الفاتورة غير موجودة</div>"]);
            return;
        }


    }

    #endregion


}
