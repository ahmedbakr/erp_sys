<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\is_admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\admin\category_m;
use App\models\admin\client_bill_payment_details_m;
use App\models\admin\payment_methods_m;
use App\models\admin\points_of_sale_m;
use App\models\admin\price_list_m;
use App\models\banks_m;
use App\models\bills\branch_bills_m;
use App\models\bills\clients_bills_m;
use App\models\branch_target_m;
use App\models\client_bill_used\client_bill_used_coupon_m;
use App\models\client_bill_used\client_bill_used_package_m;
use App\models\coupons_m;
use App\models\money_transfers_m;
use App\models\orders\clients_orders_m;
use App\models\packages\branch_packages_m;
use App\models\packages\package_products_m;
use App\models\packages\packages_m;
use App\models\product\branches_products_m;
use App\models\product\product_prices_values_m;
use App\models\product\products_m;
use App\models\user_as_sponsor_m;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class client_bills extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    #region admin_panel_code

    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/review_client_bills","show_action",$this->current_user_data))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }



        $this->data["all_bills"] = [];
        $this->data["start_date"] = date("Y-m-d");
        $this->data["end_date"] = date("Y-m-d");
        $this->data["selected_branch"] = "";

        $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");

        return view("admin.subviews.client_bills.show")->with($this->data);
    }

    public function filter_results($branch_id , $from_date , $to_date)
    {
        if (!check_permission($this->user_permissions,"admin/review_client_bills","show_action",$this->current_user_data))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->data["start_date"] = clean($from_date);
        $this->data["end_date"] = clean($to_date);
        $this->data["selected_branch"] = clean($branch_id);


        $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");

        $this->data["all_bills"] = clients_bills_m::get_clients_bills("
            AND bill.is_reviewed = 0
            AND bill.branch_id = $branch_id
            AND date(bill.client_bill_date) >=  '$from_date'
            AND date(bill.client_bill_date) <=  '$to_date'
            order by bill.client_bill_date desc
        ");

        return view("admin.subviews.client_bills.show")->with($this->data);
    }

    public function bill_is_reviewed(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/review_client_bills","edit_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                if ($accept == 0)
                {
                    $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
                    echo json_encode($output);
                    return;
                }

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $desc_msg = "تم مراجعه الفاتورة رقم $item_id";

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                $output["msg"] = "<span class='label label-success'>";
                $output["msg"] .= "تم الموافقة بنجاح";
                $output["msg"] .= "</span>";

            }

        }

        echo json_encode($output);
    }


    public function set_atm_ratio(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/review_client_bills","edit_atm_ratio",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية !! </div>";
            echo json_encode($output);
            return;
        }

        $output = array();
        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $value = $request->get("value");
        $input_type= $request->get("input_type");
        $row_primary_col= $request->get("row_primary_col");

        $output["success"] = "";
        $output["status"] = "";

        if ($item_id > 0) {

            $item_obj=$model_name::find($item_id);
            $return_statues=$item_obj->update(["$field_name"=>$value]);
            if($return_statues){
                $output["success"] = "success";
            }

            $output["msg"]=generate_self_edit_input(
                $url="",
                $item_obj,
                $item_primary_col=$row_primary_col,
                $item_edit_col=$field_name,
                $table=$model_name,
                $input_type=$input_type
            );

            $desc_msg = "تم تعديل نسبه الشبكه في الفاتورة رقم $item_id";

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

        }
        else{
            $output["success"] = "error";
        }



        echo json_encode($output);
    }

    #endregion

    #region show_bill

    public function show_all_clients_bills(Request $request)
    {
        if(!check_permission($this->user_permissions,"admin/client_orders","show_action",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->send();
        }


        $this->get_branches_and_other_data();

        $bill_id=$request->get("bill_id","");
        $branch_id=$request->get("select_branch_id","");
        $customer_id=$request->get("customer_id","");
        $branch_admin_id=$request->get("select_branch_admin_id","");
        $from_date=$request->get("from_date","");
        $to_date=$request->get("to_date","");
        $bill_pagination_number=$request->get("bill_pagination_number","50");

        $this->data["bill_pagination_number"]=$bill_pagination_number;

        $this->data["request_data"]=(object)$request->all();
        

        $bill_objs=clients_bills_m::whereNull("deleted_at");

        $cond_arr=[""];

        if(!empty($branch_id)){
            $bill_objs=$bill_objs->where("branch_id",$branch_id);
        }
        elseif($this->data["current_user"]->user_type == "branch_admin")
        {
            $current_branch_id = $this->data["current_user"]->related_id;
            $bill_objs=$bill_objs->where("branch_id",$current_branch_id);
        }

        if(!empty($branch_admin_id)){
            $bill_objs=$bill_objs->where("cashier_id",$branch_admin_id);
        }

        if(!empty($customer_id)){
            $bill_objs=$bill_objs->where("customer_id",$customer_id);
        }

        $this->data["bill_date_from"]=Carbon::now();
        if(!empty($from_date)){
            $from_date=date('Y-m-d',strtotime($from_date));
            $this->data["bill_date_from"]="$from_date";
            $bill_objs=$bill_objs->where("client_bill_date",">=",$from_date);
        }


        $this->data["bill_date_to"]=Carbon::now();
        if(!empty($to_date)){
            $to_date=date('Y-m-d',strtotime($to_date));
            $this->data["bill_date_to"]="$to_date";
            $bill_objs=$bill_objs->where("client_bill_date","<=",$to_date);
        }

        if(!empty($bill_id))
        {
            $bill_objs=$bill_objs->where("client_bill_id","=",$bill_id);
            if($this->current_user_data->user_type!="admin"){
                $bill_objs=$bill_objs->where("branch_id","=",$this->current_user_data->related_id);
            }
        }


        $bill_objs=$bill_objs->paginate($bill_pagination_number);

        $bill_objs->appends(Input::except("page"));

        $this->data["bill_objs_pagination"]=$bill_objs;

        //get all clients_bills
//        $this->data["all_bills"]=clients_bills_m::get_clients_bills(implode(" ",$cond_arr));
        $this->data["all_bills"]=[];
        if (count($bill_objs->all())>0){
            $this->data["all_bills"]=clients_bills_m::get_clients_bills(" AND bill.client_bill_id in (".implode(",",$bill_objs->pluck("client_bill_id")->all()).")");
        }



        return view("admin.subviews.branch_bills.with_clients.show_bills",$this->data);
    }

    public function show_bill_orders($bill_id){


        if(!check_permission($this->user_permissions,"admin/client_orders","show_bill_orders",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس لديك الصلاحية للدخول هنا</div>"])->send();
        }

        $this->data["bill_data"]=clients_bills_m::get_clients_bills(" AND bill.client_bill_id=$bill_id");
        if (!isset_and_array($this->data["bill_data"])){
            return Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>حدث خطأ</div>"]
            )->send();
        }
        $this->data["bill_data"]=$this->data["bill_data"][0];




        $this->data["all_bill_orders"]=clients_orders_m::get_bill_orders(" 
            AND client_bill.client_bill_id=$bill_id
            "
        );

        //get client_bill_payment_details
        $this->data["client_bill_payment_details"]=client_bill_payment_details_m::get_bill_payments_methods(" AND pay_datails.bill_id=$bill_id");


        return view("admin.subviews.branch_bills.with_clients.show_bill_orders",$this->data);
    }

    #endregion

    #region add_bill

    public function search_for_user(Request $request){

        $search_keyword=clean($request->get("q"));


        $users=User::get_all_users(" 
            AND user.user_type='customer'
            AND (user.full_name like '%$search_keyword%' or user.user_tel like '%$search_keyword%') 
        ");


        $output=[];

        $output["incomplete_results"]=false;
        $output["items"]=$users;
        $output["total_count"]=count($users);

        return $output;

    }

    public function get_product_by_barcode(Request $request){
        $output=[];

        $product_barcode=clean($request->get("pro_barcode"));
        $selected_price_id=clean($request->get("selected_price_id"));
        $branch_id=clean($request->get("branch_id"));

        if(empty($product_barcode)||empty($selected_price_id)||empty($branch_id)){
            echo json_encode($output);
        }

        $pro_data=branches_products_m::get_branches_pros(" 
            AND pro.pro_barcode='$product_barcode' 
            AND branch_pro.branch_id=$branch_id
        ");

        if(!isset_and_array($pro_data)){
            $output["msg"]="<div class='alert alert-danger'>لا يوجد منتج بهذا الباركود</div>";
            echo json_encode($output);
            return;
        }
        $pro_data=$pro_data[0];

        //overwrite $pro_data->current_pro_price
        $pro_data->current_pro_price=product_prices_values_m::
        where("price_id",$selected_price_id)->
        where("pro_id",$pro_data->pro_id)->
        get()->first();

        if(is_object($pro_data->current_pro_price)){
            $pro_data->current_pro_price=$pro_data->current_pro_price->ppv_value;
        }
        else{
            //get default price
            $default_price_id=price_list_m::where("default_price",1)->get()->first()->price_id;

            if(!is_object($default_price_id)){
                $output["msg"]="<div class='alert alert-danger'>  تواصل مع الادارة وابلغهم باهمية ان يختاروا السعر الافتراضي </div>";
                echo json_encode($output);
                return;
            }

            $pro_data->current_pro_price=product_prices_values_m::
            where("price_id",$default_price_id)->
            where("pro_id",$pro_data->pro_id)->
            get()->first();
        }


        $output["msg"]="
                <div class='alert alert-success'> 
                                تم اضافة المنتج
                        $pro_data->pro_name          
                                  للفاتورة
                </div>";

        $output["pro_data"]=[
            "pro_id"=>$pro_data->pro_id,
            "pro_name"=>$pro_data->pro_name,
            "pro_price"=>$pro_data->current_pro_price,
            "branch_quantity"=>$pro_data->b_p_quantity,
        ];

        $output["table_tr"]=\View::make("admin.subviews.branch_bills.with_clients.order_tr",["pro_obj"=>$pro_data])->render();


        echo json_encode($output);
    }

    public function check_coupon(Request $request){
        $output=[];
        $coupon_text=clean($request->get("coupon_text"));

        if(!empty($coupon_text)&&!is_array($coupon_text)){
            $coupon_text=[$coupon_text];
        }

        if(!isset_and_array($coupon_text)){
            $output["msg"]="<div class='alert alert-danger'>لا يوجد كوبونات</div>";
            echo json_encode($output);
            return;
        }


        $coupon_text=array_unique($coupon_text);

        $coupons=coupons_m::
        whereIn("coupon_code",$coupon_text)->
        where("coupon_end_date",">=",Carbon::now())->
        whereRaw("coupon_used_times>coupon_current_used")->
        get()->groupBy("coupon_code");

        if(count($coupons)){
            $output["msg"]="<div class='alert alert-success'>تم ايجاد الكوبون</div>";
        }
        else{
            $output["msg"]="<div class='alert alert-danger'>لا وجود لكوبونات غير مستعملة مطابقة للاكواد المدخلة</div>";
        }

        $output["coupons"]=$coupons;

        echo json_encode($output);
    }

    public function get_client_data(Request $request){
        $output=[];
        $user_tel=clean($request->get("user_tel"));
//        $user_name=clean($request->get("user_name"));
//        $user_address=clean($request->get("user_address"));
//        $user_email=clean($request->get("user_email"));
//        $user_gender=clean($request->get("user_gender"));
//        $age_range=clean($request->get("age_range"));
        $this->branch_id=clean($request->get("branch_id"));

        if(empty($user_tel)){
            $output["msg"]="<div class='alert alert-danger'>يجب ان تكتب تليفون العميل </div>";
            echo json_encode($output);
            return;
        }

        //search for user tel
        $user_obj=User::
        where("user_tel",$user_tel)->
        where("user_type",'customer')->
        get()->first();


        if(!is_object($user_obj)){

            $start_join_date=date("Y-m-d",time());

            //insert new client
            $user_obj=User::create([
                "user_tel"=>$user_tel,
                "full_name"=>"عميل جديد يحتاج الي تحديث البيانات",
                "user_type"=>"customer",
                "related_id"=>$this->branch_id,
                "start_join_date"=>$start_join_date
            ]);


//            $output["msg"]="
//            <div class='row'>
//            
//                <div class='col-md-6'>
//                    <div class='alert alert-danger'>لا يوجد عميل بهذا الرقم
//                    </div>
//                </div>
//                <div class='col-md-6'>
//                    <div class='alert alert-info'>
//                        <a target='_blank' href='".url("/admin/users/save/customer")."'>ادخل عميل جديد</a>
//                    </div>
//                </div>
//            </div>
//
//            ";
//
//            echo json_encode($output);
//            return;
        }
        elseif(is_object($user_obj)&&$user_obj->related_id=="0"){
            $user_obj->update([
                "related_id"=>$this->branch_id
            ]);
        }






        //get clients that registered by him
        $get_user_as_sponsor_count=user_as_sponsor_m::where("sponsor_id",$user_obj->user_id)->where("last_earn_gift_date",">",Carbon::now())->get()->count();

        $get_user_as_sponsor_tr_label="";
        $get_user_as_sponsor_tr_val="";

        if($get_user_as_sponsor_count>0){
            $get_user_as_sponsor_tr_label="هذا العميل جاء من طرفة عدد";
            $get_user_as_sponsor_tr_val=" $get_user_as_sponsor_count              
                  عملاء";

        }


        $selected_price=price_list_m::find($user_obj->selected_price_id);
        if(!is_object($selected_price)){
            //get default price
            $selected_price=price_list_m::where("default_price","1")->get()->first();
        }

        //get selected_price_id for this
        $output["selected_price_id"]=$selected_price->price_id;



        //get total client_bill_total_amount-client_bill_total_return_amount
        $all_client_bills=clients_bills_m::where("customer_id",$user_obj->user_id)->get();
        $all_client_bills_client_bill_total_amount=convert_inside_obj_to_arr($all_client_bills,"client_bill_total_amount");
        $all_client_bills_client_bill_total_return_amount=convert_inside_obj_to_arr($all_client_bills,"client_bill_total_return_amount");
        $bill_total_amount=array_sum($all_client_bills_client_bill_total_amount)-array_sum($all_client_bills_client_bill_total_return_amount);



        //get last bill and from any branch
        $last_client_bill=
            clients_bills_m::where("customer_id",$user_obj->user_id)
                ->limit(1)->orderBy("client_bill_id","desc")
                ->get()->first();

        $last_client_bill_tr_label="";
        $last_client_bill_tr_val="";

        if(is_object($last_client_bill)){
            $last_client_bill_branch=User::find($last_client_bill->branch_id);
            $last_client_bill_cashire=User::find($last_client_bill->cashier_id);


            $last_client_bill_tr_label="اخر فاتورة للعميل كانت بتاريخ";

            $last_client_bill_tr_val=
            "<div class='alert alert-info'>
                    $last_client_bill->client_bill_date
                <br>
                تكلفتها:                 
                $last_client_bill->client_bill_total_amount
                <br>
                الفرع:              
                $last_client_bill_branch->full_name
                <br>
                        الكاشير:            
                $last_client_bill_cashire->full_name
                <br>
            
    
            </div>";

        }


        $output["msg"]="
            <div class='row'>
                <div class='col-md-12'>
                        <table class='table table-bordered table-striped'>
                            <tr>
                                <td>نوع العميل</td>
                                <td>".$user_obj->user_gender."</td>
                            </tr>  
                            <tr>
                                <td>رقم التليفون</td>
                                <td>".$user_obj->user_tel."</td>
                            </tr> 
                            <tr>
                                <td>اسم العميل</td>
                                <td>
                                    ".$user_obj->full_name ."-". $user_obj->full_name_en."
                                    <a target='_blank' href='".url("/admin/users/save/customer/$user_obj->user_id")."'>
                            تعديل
                                    </a>
                                    
                                    -
                                    
                                    <a target='_blank' href='".url("/admin/users/clients/client_profile/$user_obj->user_id?branch_id=$this->branch_id")."'>
                                    البروفايل
                                    </a>
                                </td>
                            </tr> 
                            
                            <tr>
                                <td>
              $get_user_as_sponsor_tr_label
                                </td>
                                
                                <td>
                                $get_user_as_sponsor_tr_val
                                </td>   
                            </tr>
                            <tr>
                                <td>
                          سوف تتعامل مع سعر:
                                </td>
                                
                                <td>
                                $selected_price->price_title
                                </td>   
                            </tr>
                            <tr>
                                <td>
                                مقدار ما تم شراءة:
                                </td>
                                
                                <td>
                                $bill_total_amount
                                </td>   
                            </tr>
                            
                            <tr>
                                <td>
                                $last_client_bill_tr_label
                                </td>
                                
                                <td>
                                $last_client_bill_tr_val
                                </td>   
                            </tr>
                        
                        </table>

                </div>
            </div>

            ";


        echo json_encode($output);
    }

    public function make_bill(Request $request){

        if(!check_permission($this->user_permissions,"admin/client_orders","add_action",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with([
                "msg"=>"<div class='alert alert-danger'>ليس لديك الصلاحية للدخول في هذه الصفحة</div>"
            ])->send();
        }

        $this->branch_id =$request->get("branch_id");
        $branch_admin_id =$request->get("branch_admin_id");

        $this->data["branch_id"]=$this->branch_id;
        $this->data["branch_admin_id"]=$branch_admin_id;

        $this->branch_data=User::get_users(" AND u.user_id=$this->branch_id");
        $this->branch_data=$this->branch_data[0];
        $this->data["branch_data"]=$this->branch_data;


        $branch_admin_data=User::get_users(" AND u.user_id = ".$branch_admin_id." ");
        if(!isset_and_array($branch_admin_data)){
            return Redirect::to('admin/dashboard')->with([
                "msg"=>"<div class='alert alert-danger'>خطأ في البيانات</div>"
            ])->send();
        }
        $branch_admin_data=$branch_admin_data[0];


        //check if  $this->branch_id is equal to related id with current_user if current_user!=admin
        if($this->current_user_data->user_type!="admin"&&$this->current_user_data->related_id!=$this->branch_id){
            return Redirect::to('admin/dashboard')->with([
                "msg"=>"<div class='alert alert-danger'>ليس لديك الصلاحية للدخول في هذه الصفحة بهذه البيانات </div>"
            ])->send();
        }

        $this->data["branch_id"]=$this->branch_id;

        //get all branch packages
        $all_branch_packages=branch_packages_m::get_branch_packages("
            where branch_pac.branch_id=$this->branch_id 
            AND pac.package_start_date<=CURDATE()
            AND pac.package_end_date>=CURDATE()
            order by pac.min_products_to_get_offer
        ");


        $all_branch_packages_products=[];
        if(isset_and_array($all_branch_packages)){
            $all_branch_packages_ids=convert_inside_obj_to_arr($all_branch_packages,"package_id");
            $all_branch_packages_products=package_products_m::whereIn("package_id",$all_branch_packages_ids)->get()->groupBy("package_id")->all();
        }

        foreach($all_branch_packages as $branch_pac_key=>$branch_pac_data){
            if(!isset($all_branch_packages_products[$branch_pac_data->package_id])){
                continue;
            }
            $pac_pro_ids=$all_branch_packages_products[$branch_pac_data->package_id]->all();
            $pac_pro_ids=convert_inside_obj_to_arr($pac_pro_ids,"pro_id");

            $branch_pac_data->proids=$pac_pro_ids;
        }



        $this->data["all_branch_packages_data"]=$all_branch_packages;


        //get point of sale of this user
        $point_of_sale=points_of_sale_m::find($branch_admin_data->point_of_sale_id);

        if(!is_object($point_of_sale)){
            return Redirect::to('admin/dashboard')->with([
                "msg"=>"<div class='alert alert-danger'>تواصل مع مدير الفرع لكي يجعلك مسئول عن نقطة بيع قبل ان تدخل علي هذه الصفحة</div>"
            ])->send();
        }


        //get all payment methods
        $all_payment_methods=payment_methods_m::all();
        $this->data["all_payment_methods"]=$all_payment_methods;

        //get cats with parent_id=0
        $this->get_cat_child_cats_and_products($request,"0");


        if($request->method()=="POST"){

            $this->validate($request, [
                "branch_id"=>"required",
                "branch_admin_id"=>"required",
                "pro_id*" => "required",
                "order_quantity*" => "required|min:1|numeric",
                "client_bill_date" => "required",
            ]);

            //notify admin
            $bill_notification=$branch_admin_data->full_name." من الفرع ".$this->branch_data->full_name ." تم عمل فاتورة بيع+";
            $this->track_my_action($bill_notification);


            $request["client_bill_date"]=date('Y-m-d H:i:s',strtotime($request->get("client_bill_date")));


            $request_data = $this->cleaning_input($request->all(), $except = array());


            //check user permission of allow_cashier_atm,allow_cashier_to_remain_money
            if(!check_permission($this->user_permissions,"admin/client_orders","allow_cashier_atm",$this->current_user_data)){
                $request_data["atm_money"]=0;
            }

            if(!check_permission($this->user_permissions,"admin/client_orders","allow_cashier_to_remain_money",$this->current_user_data)){
                if($request_data["client_bill_total_amount"]!=($request_data["atm_money"]+$request_data["cash_money"])){
                    return Redirect::to('admin/dashboard')->with([
                        "msg"=>"<div class='alert alert-danger'>ليس لديك الصلاحية ان تبقي علي العميل اي اموال</div>"
                    ])->send();
                }
            }

            $request_data["order_quantity"]=array_map("intval",$request_data["order_quantity"]);


            //create orders
            if(count($request_data["pro_id"])==count($request_data["order_quantity"])){

                //check customer
                $user_obj=User::where("user_tel",$request["client_tel_number"])->get()->first();

                if(!is_object($user_obj)){

                    return Redirect::to('admin/dashboard')->with([
                        "msg"=>"<div class='alert alert-danger'>رقم تليفون العميل غير صحيح</div>"
                    ])->send();

//                    $user_obj=User::create([
//                        "email"=>$request["client_email"],
//                        "user_tel"=>$request["client_tel_number"],
//                        "full_name"=>$request["client_name"],
//                        "username"=>$request["client_name"],
//                        "user_type"=>"customer",
//                        "role"=>"customer",
//                        "user_address"=>$request["client_address"],
//                        "related_id"=>$this->branch_id
//                    ]);
                }


                //what client paid
                $request_data["client_total_paid_amount_in_cash"]=$request_data["cash_money"];
                $request_data["client_total_paid_amount_in_atm"]=$request_data["atm_money"];


                //calc client_bill_total_remain_amount
                $client_bill_total_remain_amount=0;
                if(($request["client_bill_total_amount"]-($request_data["cash_money"]+$request_data["atm_money"])>0)){
                    $client_bill_total_remain_amount=($request["client_bill_total_amount"]-($request_data["cash_money"]+$request_data["atm_money"]));
                }

                $total_bill_tax_value = $request_data['total_bill_tax_value'];

                //recheck client_bill_total_amount again after calc all orders items
                $bill_obj=clients_bills_m::create([
                    "cashier_id"=>$branch_admin_id,
                    "branch_id"=>$this->branch_id,
                    "customer_id"=>$user_obj->user_id,
                    "client_bill_total_amount"=>$request_data["client_bill_total_amount"],
                    "client_total_paid_amount_in_cash"=>$request_data["client_total_paid_amount_in_cash"],
                    "client_total_paid_amount_in_atm"=>$request_data["client_total_paid_amount_in_atm"],
                    "client_bill_total_remain_amount"=>$client_bill_total_remain_amount,
                    "client_bill_date"=>$request_data["client_bill_date"],
                    "bill_tax_percent"=>$this->tax_percent,
                    "bill_tax_value"=>$total_bill_tax_value,
                    "bill_total_amount_before_tax"=>($request_data["client_bill_total_amount"] - $total_bill_tax_value),
                    "is_reviewed"=>"1"
                ]);

                $bill_update_data=[];
                $bill_update_data["bank_atm_ratio_value"]=0;

                #region add client_bill_payment_details rows
                $client_bill_payment_details_rows=[];
                foreach($all_payment_methods as $pay_id=>$pay){

                    $pay_amount=floatval($request->get("payment_method_".$pay->payment_method_id));
                    $atm_ratio_amount=0;


                    if($pay_amount>0){

                        if($pay->is_payment_method_atm=="1"){
                            $atm_ratio_amount=($pay_amount*$pay->atm_ratio)/100;
                            $bill_update_data["bank_atm_ratio_value"]=$bill_update_data["bank_atm_ratio_value"]+$atm_ratio_amount;
                        }

                        $client_bill_payment_details_rows[]=[
                            'bill_id'=>$bill_obj->client_bill_id,
                            'payment_method_id'=>$pay->payment_method_id,
                            'payment_amount'=>$pay_amount,
                            'atm_ratio_amount'=>$atm_ratio_amount
                        ];
                    }

                }

                client_bill_payment_details_m::insert($client_bill_payment_details_rows);

                #endergion



                #region money_transfers

                if($request_data["client_total_paid_amount_in_cash"]>0){
                    money_transfers_m::create([
                        "from_user_id"=>$user_obj->user_id,
                        "to_user_id"=>$this->branch_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$request_data["client_total_paid_amount_in_cash"],
                        "m_t_amount_type"=>"cash",
                        "m_t_status"=>"1",
                        "m_t_desc"=>"فلوس كاش من قيمة فاتورة مبيعات",
                    ]);
                }

                if($request_data["client_total_paid_amount_in_atm"]>0){
                    money_transfers_m::create([
                        "from_user_id"=>$user_obj->user_id,
                        "to_user_id"=>$this->branch_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$request_data["client_total_paid_amount_in_atm"],
                        "m_t_amount_type"=>"atm",
                        "m_t_status"=>"1",
                        "m_t_desc"=>"فلوس شبكة من قيمة فاتورة مبيعات",
                    ]);
                }

                if($client_bill_total_remain_amount>0){
                    money_transfers_m::create([
                        "from_user_id"=>$user_obj->user_id,
                        "to_user_id"=>$this->branch_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$client_bill_total_remain_amount,
                        "m_t_amount_type"=>"",
                        "m_t_status"=>"0",
                        "m_t_desc"=>"متبقي من قيمة فاتورة مبيعات",
                    ]);
                }


                #endregion


                $bill_orders=[];
                $bill_orders_gifts=[];
                $client_bill_total_amount=[];

                foreach($request_data["pro_id"] as $pro_key=>$pro_id){

                    $pro_obj=branches_products_m::get_branches_pros(" AND branch_pro.branch_id=$this->branch_id AND branch_pro.pro_id=$pro_id");
                    if(!isset_and_array($pro_obj)){
                        continue;
                    }
                    $pro_obj=$pro_obj[0];

                    //overwrite $pro_data->current_pro_price
                    $pro_obj->current_pro_price=product_prices_values_m::
                    where("price_id",$request_data["selected_price_id"])->
                    where("pro_id",$pro_id)->
                    get()->first();

                    if(is_object($pro_obj->current_pro_price)){
                        $pro_obj->current_pro_price=$pro_obj->current_pro_price->ppv_value;
                    }
                    else{
                        //get default price
                        $default_price_id=price_list_m::where("default_price",1)->get()->first()->price_id;

                        if(!is_object($default_price_id)){
                            $output["msg"]="<div class='alert alert-danger'>  تواصل مع الادارة وابلغهم باهمية ان يختاروا السعر الافتراضي </div>";
                            echo json_encode($output);
                            return;
                        }

                        $pro_obj->current_pro_price=product_prices_values_m::
                        where("price_id",$default_price_id)->
                        where("pro_id",$pro_obj->pro_id)->
                        get()->first();
                    }
                    $pro_sell_price=$pro_obj->current_pro_price;



                    if(!($pro_sell_price>0)){
                        continue;
                    }

                    if(isset($bill_orders[$pro_id]["order_quantity"])){
                        $bill_orders[$pro_id]["order_quantity"]=$bill_orders[$pro_id]["order_quantity"]+$request_data["order_quantity"][$pro_key];
                    }else{
                        $bill_orders[$pro_id]["order_quantity"]=$request_data["order_quantity"][$pro_key];
                    }

                    //check if b_o_quantity > pro_quantity
                    if($bill_orders[$pro_id]["order_quantity"]>$pro_obj->b_p_quantity){
                        $bill_orders[$pro_id]["order_quantity"]=$pro_obj->b_p_quantity;
                    }

                    $bill_orders[$pro_id]["pro_id"]=$pro_id;
                    $bill_orders[$pro_id]["client_bill_id"]=$bill_obj->client_bill_id;
                    $bill_orders[$pro_id]["pro_sell_price"]=$pro_sell_price;
                    $bill_orders[$pro_id]["pro_original_price"]=$pro_sell_price;
                    $client_bill_total_amount[$pro_id]=
                        ($bill_orders[$pro_id]["pro_sell_price"]*$bill_orders[$pro_id]["order_quantity"]);

                    $bill_orders[$pro_id]["order_return"]=0;
                    $bill_orders[$pro_id]["created_at"]=Carbon::now();
                    $bill_orders[$pro_id]["updated_at"]=Carbon::now();
                }

                //before insert rows into clients_orders table
                //check of packages first

                //store prices in variable before modifying
                //ex if product in two packages and min_pro in these is 2,4
                //then this product price will price twice once at first package and other at second package
                //so we must store the original price before calc package discount
                $orignal_bill_orders=$bill_orders;




                //check orders in package ex: $pac->pac_id=1 $pac->count_pros_in_pac=5
                foreach($all_branch_packages as $pac_key=>$pac){
                    if(!isset($pac->proids)){
                        continue;
                    }

                    $pac->count_pros_in_pac=0;
                    foreach($bill_orders as $order_key=>$order){
                        //$order_key is pro_id
                        if(in_array($order_key,$pac->proids)){
                            $pac->count_pros_in_pac=$pac->count_pros_in_pac+$order["order_quantity"];
                        }
                    }



                    //check if $pac->min_products_to_get_offer>=$pac->count_pros_in_pac

                    if($pac->count_pros_in_pac>=$pac->min_products_to_get_offer){

                        $item_discount_val=$pac->discount_percentage_for_product/$pac->count_pros_in_pac;

                        foreach($bill_orders as $order_key=>$order){
                            //$order_key is pro_id
                            if(in_array($order_key,$pac->proids)){
                                //this $order is inside package and package offer is activate

                                if($pac->package_type=="1"){

                                    if($item_discount_val>$orignal_bill_orders[$order_key]["pro_sell_price"]){
                                        $bill_orders[$order_key]["pro_sell_price"]=0;
                                    }
                                    else{
                                        $bill_orders[$order_key]["pro_sell_price"]=
                                            $orignal_bill_orders[$order_key]["pro_sell_price"]
                                            -
                                            $item_discount_val;
                                    }

                                }
                                else{
                                    $bill_orders[$order_key]["pro_sell_price"]=
                                        $orignal_bill_orders[$order_key]["pro_sell_price"]
                                        -
                                        (
                                            ($pac->discount_percentage_for_product*$orignal_bill_orders[$order_key]["pro_sell_price"])/100
                                        );
                                }


//                                $bill_orders[$order_key]["pro_sell_price"]=round($bill_orders[$order_key]["pro_sell_price"]);


                                $client_bill_total_amount[$order_key]=
                                    ($bill_orders[$order_key]["pro_sell_price"]*$bill_orders[$order_key]["order_quantity"]);

//                                $client_bill_total_amount[$order_key]=round($client_bill_total_amount[$order_key]);

                            }
                        }
                    }



                    //check if $pac->min_products_to_get_gift>=$pac->count_pros_in_pac
                    if(false&&$pac->count_pros_in_pac>=$pac->min_products_to_get_gift){
                        //adding gift to orders

                        $bill_orders_gifts[$pac->product_gift_id]["pro_id"]=$pac->product_gift_id;
                        $bill_orders_gifts[$pac->product_gift_id]["client_bill_id"]=$bill_obj->client_bill_id;
                        $bill_orders_gifts[$pac->product_gift_id]["pro_sell_price"]=0;
                        $bill_orders_gifts[$pac->product_gift_id]["order_quantity"]=(int)($pac->count_pros_in_pac/$pac->min_products_to_get_gift);
                        $bill_orders_gifts[$pac->product_gift_id]["order_return"]=0;
                        $bill_orders_gifts[$pac->product_gift_id]["is_gift"]=1;
                        $bill_orders_gifts[$pac->product_gift_id]["created_at"]=Carbon::now();
                        $bill_orders_gifts[$pac->product_gift_id]["updated_at"]=Carbon::now();
                    }

                }


                #region calc package total amount

                //update bill_packages,bill_packages_amount
                $bill_packages=$request->get("selected_pac_id");
                $bill_packages_amount=0;

                $client_bill_used_package=[];
                if(is_array($bill_packages)){
                    $bill_packages=array_unique($bill_packages);
                    $bill_packages_objs=packages_m::whereIn("package_id",$bill_packages)->get();
                    $bill_packages=[];
                    foreach ($bill_packages_objs as $pac_obj){
                        $bill_packages[]=
                            $pac_obj->package_name." - ".
                            $pac_obj->discount_percentage_for_product.
                            (($pac_obj->package_type=="0")?"%":"");

                        $client_bill_used_package[]=[
                            'client_bill_id'=>$bill_obj->client_bill_id,
                            'package_id'=>$pac_obj->package_id,
                            'used_at'=>Carbon::now()
                        ];
                    }

                    foreach($bill_orders as $order){
                        $bill_packages_amount+=$order["order_quantity"]*($order["pro_original_price"]-$order["pro_sell_price"]);
                    }


                }

                #endregion


                //check of coupons second
                $coupon_text=$request_data["coupon_text"];
                $used_coupon=[];
                $bill_coupon_val=0;

                $client_bill_used_coupon=[];

                if(isset_and_array($coupon_text)){
                    $client_bill_total_amount_sum=array_sum($client_bill_total_amount);

                    $coupons=coupons_m::
                    leftJoin("category as cat","cat.cat_id","=","coupons.cat_id")->
                    whereIn("coupon_code",$coupon_text)->
                    where("coupon_end_date",">=",Carbon::now())->
                    whereRaw("coupon_used_times>coupon_current_used")->
                    get();

                    if(count($coupons)){
                        //get all orders quantity sum
                        $all_orders_count=convert_inside_obj_to_arr($bill_orders,"order_quantity","array");
                        $all_orders_count=array_sum($all_orders_count);

                        $coupon_value=0;
                        $total_coupons_val=0;


                        foreach($coupons as $coupon_obj){
                            $used_coupon[]=$coupon_obj->coupon_code." - ".$coupon_obj->cat_name;

                            $coupon_type=($coupon_obj->is_rate)?"percentage":"value";

                            if($coupon_type=="value"){
                                $total_coupons_val+=$coupon_obj->coupon_value;
                            }
                            elseif($coupon_type=="percentage"){
                                $total_coupons_val+=($client_bill_total_amount_sum*$coupon_obj->coupon_value/100);
                                $total_coupons_val=round($total_coupons_val,0);
                            }


                            $client_bill_used_coupon[]=[
                                'client_bill_id'=>$bill_obj->client_bill_id,
                                'coupon_id'=>$coupon_obj->coupon_id,
                                'coupon_cat_id'=>$coupon_obj->cat_id,
                                'used_at'=>Carbon::now()
                            ];
                        }


                        if($total_coupons_val>$client_bill_total_amount_sum){
                            $total_coupons_val=$client_bill_total_amount_sum;
                        }

                        $bill_coupon_val=$total_coupons_val;


                        $coupon_value_in_order_item=$total_coupons_val/$all_orders_count;

                        //make coupon used
                        foreach($coupon_text as $singel){
                            $used_coupon=coupons_m::where("coupon_code",$singel)->get()->first();
                            if(is_object($used_coupon)){
                                $used_coupon->update([
                                    "coupon_current_used"=>$used_coupon->coupon_current_used+1
                                ]);
                            }
                        }


                        //distribute coupon value to all order items
                        foreach($bill_orders as $order_key=>$order){
                            //$order_key is pro_id

                            $bill_orders[$order_key]["pro_sell_price"]=
                                $bill_orders[$order_key]["pro_sell_price"]-$coupon_value_in_order_item;



                            $client_bill_total_amount[$order_key]=
                                ($bill_orders[$order_key]["pro_sell_price"]*$bill_orders[$order_key]["order_quantity"]);
                        }

                    }



                    //calc total bill number
                    $client_bill_total_amount=round(array_sum($client_bill_total_amount));
                }


//                $bill_coupon_val=round($bill_coupon_val);
                //update client bill with bill_coupon col


                $bill_obj->update([
                    "bill_coupon"=>json_encode($used_coupon),
                    "bill_coupon_amount"=>$bill_coupon_val,
                    "bank_atm_ratio_value"=>$bill_update_data["bank_atm_ratio_value"],
                    "bill_packages"=>json_encode($bill_packages),
                    "bill_packages_amount"=>"$bill_packages_amount"
                ]);


                if($this->tax_percent > 0 && $request["client_bill_total_amount"] > 0)
                {
                    $client_bill_total_amount = ($client_bill_total_amount+$total_bill_tax_value);
                }



                //i want to change client side code to make calculation like here
                //but i think the difference between them will be very small
                //so i compare between integers numbers

                if((int)($request["client_bill_total_amount"])!=(int)($client_bill_total_amount)){

                    //there is a playing in the inspect element happen or any thing change real value
                    //so i'll remove this bill

                    clients_bills_m::find($bill_obj->client_bill_id)->forceDelete();

                    money_transfers_m::where("m_t_bill_id",$bill_obj->client_bill_id)->forceDelete();

                    return Redirect::to('admin/dashboard')->with([
                        "msg"=>"<div class='alert alert-danger'>يوجد خطا في البيانات المرسلة الي السيرفر.يمكنك الاتصال بالشركة المسؤلة عن برمجة الموقع للسؤال عن هذا الخطأ seoera.net</div>"
                    ])->send();
                }


                #region set tax value to order items

                $order_item_tax_value = 0;
//                if($this->tax_percent > 0)
//                {
//                    $get_all_orders_quanatity = convert_inside_obj_to_arr(array_values($bill_orders),"order_quantity","array");
//                    $order_item_tax_value = $bill_obj->bill_tax_value/array_sum($get_all_orders_quanatity);
//                    $order_item_tax_value = round($order_item_tax_value,3);
//                }

                foreach($bill_orders as $bill_order_key => $bill_order_item)
                {
                    $bill_orders[$bill_order_key]["pro_sell_price_before_tax"] = $bill_orders[$bill_order_key]["pro_sell_price"];

                    if($this->tax_percent > 0)
                    {
                        $order_item_tax_value = ($bill_orders[$bill_order_key]["pro_sell_price_before_tax"]*$this->tax_percent)/100;
                        $order_item_tax_value = round($order_item_tax_value,2);
                    }

                    $bill_orders[$bill_order_key]["pro_sell_price"] = (
                        $bill_orders[$bill_order_key]["pro_sell_price"] +
                        $order_item_tax_value
                    );

                    $bill_orders[$bill_order_key]["pro_sell_tax_value"] = (
                        $bill_orders[$bill_order_key]["pro_sell_price"] -
                        $bill_orders[$bill_order_key]["pro_sell_price_before_tax"]
                    );
                }

                #endregion


                clients_orders_m::insert($bill_orders);
                clients_orders_m::insert($bill_orders_gifts);

                client_bill_used_package_m::insert($client_bill_used_package);
                client_bill_used_coupon_m::insert($client_bill_used_coupon);

                //reduce branch stock by $bill_orders
                foreach($bill_orders as $order_key=>$order){
                    $pro_obj=branches_products_m::
                    where("pro_id",$order["pro_id"])->
                    where("branch_id",$this->branch_id)->
                    get()->first();

                    if(!is_object($pro_obj)){
                        dump("error");
                        continue;
                    }

                    $new_quan=$pro_obj->b_p_quantity-$order["order_quantity"];

                    if($new_quan<0){
                        continue;
                    }

                    if($pro_obj->b_p_quantity_limit>$new_quan){
                        $this->_send_email_to_custom(
                            $emails = [$this->branch_data->email] ,
                            $data =" The quantity of $pro_obj->pro_name is now $new_quan" ,
                            $subject = "اشعار من النظام بتاريخ".Carbon::now(),
                            $sender = "seoera@seoera.net" ,
                            $path_to_file = ""
                        );
                    }

                    $pro_obj->update([
                        "b_p_quantity"=>$new_quan
                    ]);
                }

                //reduce branch stock by $bill_orders_gifts
                foreach($bill_orders_gifts as $order_key=>$order){

                    $pro_obj=branches_products_m::
                    where("pro_id",$order["pro_id"])->
                    where("branch_id",$this->branch_id)->
                    get()->first();


                    if(!is_object($pro_obj)){
                        dump("لا يوجد المنتج الهدية في هذا الفرع");
                        continue;
                    }

                    $new_quan=$pro_obj->b_p_quantity-$order["order_quantity"];

                    if($new_quan<0){
                        continue;
                    }

                    if($pro_obj->b_p_quantity_limit>$new_quan){
                        $this->_send_email_to_custom(
                            $emails = [$this->branch_data->email] ,
                            $data =" The quantity of $pro_obj->pro_name is now $new_quan" ,
                            $subject = "اشعار من النظام بتاريخ".Carbon::now(),
                            $sender = "seoera@seoera.net" ,
                            $path_to_file = ""
                        );
                    }

                    $pro_obj->update([
                        "b_p_quantity"=>$new_quan
                    ]);
                }


                //add cash value to $point_of_sale or branch_balance
                if($point_of_sale->set_cash_on_my_balance=="1"){
                    $point_of_sale->update([
                        "point_of_sale_balance"=>$point_of_sale->point_of_sale_balance+$request_data["cash_money"]
                    ]);
                }
                else{
                    $branch_obj=User::find($this->branch_id);
                    $branch_obj->update([
                        "user_balance"=>$branch_obj->user_balance+$request_data["cash_money"]
                    ]);
                }



                return Redirect::to("print_client_bill?bill_id=$bill_obj->client_bill_id&bill_type=0&branch_id=$bill_obj->branch_id&branch_admin_id=$bill_obj->cashier_id")->with([
                    "msg"=>"<div class='alert alert-success'>بيانات الفاتورة للطباعه بالأسفل برجاء ضغط CTRL+P او الضغط علي زر الطباعة</div>"
                ])->send();

            }
            else{
                return Redirect::to('admin/dashboard')->with([
                    "msg"=>"<div class='alert alert-danger'>حدث خطا في العملية</div>"
                ])->send();
            }



        }


        return view("admin.subviews.branch_bills.with_clients.add_bill",$this->data);
    }

    public function get_cat_child_cats_and_products(Request $request,$cat_id=null){


        $echo_json=false;
        if($cat_id==null){

            $cat_id=$request->get("cat_id");
            $cat_id=intval($cat_id);

            if(!($cat_id>0)){
                return;
            }

            $this->branch_id=$request->get("branch_id");
            $echo_json=true;
        }



        //get product parent categories
        $all_parent_cats=category_m::
        where("cat_type","product")->
        where("parent_id",$cat_id)->
        get();

        //get products in $all_parent_cats
        $all_parent_cats_products=branches_products_m::get_branches_pros("
                AND pro.cat_id=$cat_id
                AND branch_pro.branch_id=$this->branch_id
            ");

        $all_parent_cats_products=collect($all_parent_cats_products)->groupBy("cat_id");

        $cats_ids=[];
        if(count($all_parent_cats)){
            $cats_ids=$all_parent_cats->lists("cat_id")->all();
        }


        $this->data["all_parent_cats"]=$all_parent_cats->all();
        $this->data["all_parent_cats_products"]=$all_parent_cats_products->all();
        $this->data["loaded_cat_id"]=$cat_id;

        $this->data["cats_view"]=\View::make("admin.subviews.branch_bills.with_clients.blocks.load_cats_block",$this->data)->render();
        $this->data["products_view"]=\View::make("admin.subviews.branch_bills.with_clients.blocks.load_products_block",$this->data)->render();

        if($echo_json){
            $output=[];

            $output["cats_ids"]=$cats_ids;
            $output["cats_view"]=$this->data["cats_view"];
            $output["products_view"]=$this->data["products_view"];

            echo json_encode($output);
        }


    }

    public function search_for_product(Request $request){

        $branch_id=intval($request->get("branch_id"));

        if(empty($branch_id)){
            return;
        }

        $search_keyword=clean($request->get("search_keyword"));


        $products=branches_products_m::get_branches_pros("
                AND (
                    pro.pro_name like '%$search_keyword%' OR
                    pro.pro_barcode = '$search_keyword' 
                )
                AND branch_pro.branch_id=$branch_id
            ");

        $output["products_view"]=\View::make("admin.subviews.branch_bills.with_clients.blocks.search_products_block",["products"=>$products])->render();

        echo json_encode($output);
    }


    #endregion

    #region return bill
    public function make_return_bill(Request $request,$bill_id=null){

        if(!check_permission($this->user_permissions,"admin/client_orders","add_return_bill",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->send();
        }

        $this->data["bill_id"] = $bill_id;

        $client_bill=clients_bills_m::findOrFail($bill_id);
        $this->branch_id=$client_bill->branch_id;
        $this->branch_data=User::get_users(" AND u.user_id=$this->branch_id");
        $this->branch_data=$this->branch_data[0];
        $this->data["branch_data"]=$this->branch_data;

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "bill_id" => "required",
                "returned_quantity*" => "required|min:0|numeric",
                "order_id*" => "required",
            ]);

            $from_form_return_money_to_client=floatval($request->get("return_money_to_client"));
            $return_cash_money_to_client=floatval($request->get("return_cash_money_to_client"));
            $return_atm_money_to_client=floatval($request->get("return_atm_money_to_client"));

            //check if $return_cash_money_to_client>user_balance
            $branch_obj=User::find($this->branch_id);

            if($return_cash_money_to_client>$branch_obj->user_balance){
                return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-danger'> لا يمكنك دفع كاش بالقيمة المكتوبة لانك لا تمتلكها   </div>"
                ])->send();
            }

            //check atm permission for this cashier
            if(!check_permission($this->user_permissions,"admin/client_orders","return_bill_allow_use_atm",$this->current_user_data)){
                $return_atm_money_to_client=0;
            }


            if($from_form_return_money_to_client<($return_cash_money_to_client+$return_atm_money_to_client)){
                return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-danger'> لا يمكنك ان تدفع للعميل اكثر مما يستحقه   </div>"
                ])->send();
            }

            if($from_form_return_money_to_client>($return_cash_money_to_client+$return_atm_money_to_client)){

                //check if this cashier has permission to remain money for user
                if(!check_permission($this->user_permissions,"admin/client_orders","return_bill_allow_remain_money",$this->current_user_data)) {
                    return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                        "msg" => "<div class='alert alert-danger'> لا يمكنك ان تبقي للعميل مما يستحقة اي اموال . حدث خلل في النظام ويجب ان ترجعه بنفسك في كمية المنتجات الموجوده في مخزن الفرع بسبب هذا الخطا </div>"
                    ])->send();
                }

            }


            $request_data = clean($request->all());

            $request_data["returned_quantity"]=array_map("intval",$request_data["returned_quantity"]);

            if ((count($request_data["returned_quantity"]) != count($request_data["order_id"])) ||
                empty($request_data["bill_id"]))
            {
                return Redirect::to('admin/client_bills/add_return_bill')->with([
                    "msg" => "<div class='alert alert-danger'> خطأ في البيانات !!!  </div>"
                ])->send();
            }

            $bill_id = $request_data["bill_id"];
            $returned_quantity = $request_data["returned_quantity"];
            $order_id = $request_data["order_id"];


            // check if bill is exist
            $bill_obj = clients_bills_m::findOrFail($bill_id );

            $returns_input_arr = array();

            // get all bill returns orders if exist
            $bill_return_orders = clients_orders_m::get_bill_orders(" 
                AND client_bill.client_bill_id=$bill_id
                AND client_bill.branch_id=$this->branch_id
                AND clients_order.order_return = 1
                order by clients_order.order_id desc
            ");
            $bill_return_orders = collect($bill_return_orders);
            $bill_return_orders = $bill_return_orders->groupBy("pro_id");


            $total_return_money_to_user=0;
            $total_return_tax=0;

            foreach($order_id as $key => $id)
            {
                // check if this order item is exist to this bill_id and this quantity is larger than return
                $order_check_obj = clients_orders_m::where("order_id",$id)->get()->first();

                if(
                    is_object($order_check_obj) &&
                    $order_check_obj->order_quantity >= $returned_quantity[$key] &&
                    $returned_quantity[$key] > 0 )
                {
                    // check if total returns quantity to this item in bill less than purchase orders to this item

                    if  (
                        empty($bill_return_orders) ||
                        !isset($bill_return_orders[$order_check_obj->pro_id]) ||
                        (
                            isset($bill_return_orders[$order_check_obj->pro_id]) &&
                            ($order_check_obj->order_quantity - array_sum(convert_inside_obj_to_arr($bill_return_orders[$order_check_obj->pro_id],"order_quantity"))) >= $returned_quantity[$key]
                        )
                    )
                    {

                        // check if quantity in stock larger than or equal returned and update it
                        //$product_mat_obj
                        $branch_product_obj = branches_products_m::
                            where("pro_id",$order_check_obj->pro_id)
                            ->where("branch_id",$bill_obj->branch_id)
                            ->get()->first();
                        $product_obj=products_m::find($order_check_obj->pro_id);

                        if (is_object($branch_product_obj))
                        {

                            $branch_product_obj->update([
                                "b_p_quantity" => ($branch_product_obj->b_p_quantity + $returned_quantity[$key])
                            ]);


                            $desc_msg = " تعديل منتجات الفرع ".
                                $this->branch_data->full_name." للمنتج (".
                                $product_obj->pro_name.
                                ") .  مرتجع من العميل بكمية $returned_quantity[$key] لهذا الفرع";

                            #region save in site_tracking
                            $this->track_my_action(
                                $action_desc = $desc_msg
                            );
                            #endregion


                            $total_return_money_to_user=$total_return_money_to_user+($order_check_obj->pro_sell_price*$returned_quantity[$key]);

                            $returns_row=[];
                            $returns_row["pro_id"] = $order_check_obj->pro_id;
                            $returns_row["client_bill_id"] = $bill_id;
                            $returns_row["pro_sell_price"] = $order_check_obj->pro_sell_price;
                            $returns_row["pro_sell_tax_value"] = $order_check_obj->pro_sell_tax_value;
                            $returns_row["pro_original_price"] = $order_check_obj->pro_original_price;
                            $returns_row["pro_sell_price_before_tax"] = $order_check_obj->pro_sell_price_before_tax;
                            $returns_row["order_quantity"] = $returned_quantity[$key];
                            $returns_row["order_return"] = 1;

                            $returns_row["created_at"] = Carbon::now();
                            $returns_row["updated_at"] = Carbon::now();

                            $total_return_tax += ($returned_quantity[$key] * $returns_row["pro_sell_tax_value"]);

                            $returns_input_arr[]=$returns_row;

                        }

                    }
                }

            }//end foreach


            if(count($returns_input_arr))
            {
                $total_return_money_to_user=round($total_return_money_to_user);

                //$total_return_money_to_user

                if($total_return_money_to_user<($return_cash_money_to_client+$return_atm_money_to_client)){
                    return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                        "msg" => "<div class='alert alert-danger'> لا يمكنك ان تدفع للعميل اكثر مما يستحقه . حدث خلل في النظام ويجب ان ترجعه بنفسك في كمية المنتجات الموجوده في مخزن الفرع بسبب هذا الخطا </div>"
                    ])->send();
                }

                if($total_return_money_to_user>($return_cash_money_to_client+$return_atm_money_to_client)){
                    //check if this cashier has permission to remain money for user
                    if(!check_permission($this->user_permissions,"admin/client_orders","return_bill_allow_remain_money",$this->current_user_data)) {
                        return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                            "msg" => "<div class='alert alert-danger'> لا يمكنك ان تبقي للعميل مما يستحقة اي اموال . حدث خلل في النظام ويجب ان ترجعه بنفسك في كمية المنتجات الموجوده في مخزن الفرع بسبب هذا الخطا </div>"
                        ])->send();
                    }
                }

                $client_retrn_bill_total_remain_amount=$total_return_money_to_user-($return_cash_money_to_client+$return_atm_money_to_client);

                #region money_transfers

                if($return_cash_money_to_client>0){
                    money_transfers_m::create([
                        "from_user_id"=>$this->branch_id,
                        "to_user_id"=>$bill_obj->customer_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$return_cash_money_to_client,
                        "m_t_amount_type"=>"cash",
                        "m_t_status"=>"1",
                        "m_t_desc"=>"فلوس كاش من قيمة مرجعات فاتورة مبيعات",
                    ]);
                }

                if($return_atm_money_to_client>0){
                    money_transfers_m::create([
                        "from_user_id"=>$this->branch_id,
                        "to_user_id"=>$bill_obj->customer_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$return_atm_money_to_client,
                        "m_t_amount_type"=>"atm",
                        "m_t_status"=>"1",
                        "m_t_desc"=>"فلوس شبكة من قيمة مرجعات فاتورة مبيعات",
                    ]);
                }



                if($client_retrn_bill_total_remain_amount>0){
                    money_transfers_m::create([
                        "from_user_id"=>$this->branch_id,
                        "to_user_id"=>$this->branch_id,
                        "m_t_bill_id"=>$bill_obj->client_bill_id,
                        "m_t_bill_type"=>"client_bill",
                        "m_t_amount"=>$client_retrn_bill_total_remain_amount,
                        "m_t_amount_type"=>"",
                        "m_t_status"=>"0",
                        "m_t_desc"=>"متبقي من قيمة مرجعات فاتورة مبيعات",
                    ]);
                }


                #endregion

                //update client bill by client_bill_total_return_amount
                $bill_obj->update([
                    "client_bill_total_return_amount"=>($bill_obj->client_bill_total_return_amount+$total_return_money_to_user),
                    'client_bill_return_cash_amount'=>($bill_obj->client_bill_return_cash_amount+$return_cash_money_to_client),
                    'client_bill_return_atm_amount'=>($bill_obj->client_bill_return_atm_amount+$return_atm_money_to_client),
                    'bill_tax_return_value'=>($bill_obj->bill_tax_return_value+$total_return_tax),
                ]);

                clients_orders_m::insert($returns_input_arr);


                $branch_obj=User::find($this->branch_id);
                $branch_obj->update([
                    "user_balance"=>$branch_obj->user_balance-$return_cash_money_to_client
                ]);

                return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-success'> لقد تم الحقظ بنجاح  </div>"
                ])->send();

            }
            else{
                return Redirect::to('admin/client_bills/add_return_bill/'.$bill_id)->with([
                    "msg" => "<div class='alert alert-danger'> لم يتم الحفظ بنجاح بسبب بيانات غير صالحه او كمية غير كافية </div>"
                ])->send();
            }


        }


        return view("admin.subviews.branch_bills.with_clients.return.add_return_bill",$this->data);
    }

    public function replace_bill_order(Request $request){

        if(!check_permission($this->user_permissions,"admin/client_orders","add_return_bill",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->send();
        }

        $this->show_category_tree("true","product");

        if ($request->method()=="GET"){

            $order_id=$request->get("order_id","");
            $order_obj=clients_orders_m::findOrFail($order_id);

            $pro_obj=products_m::findOrFail($order_obj->pro_id);
            $cat_obj=category_m::findOrFail($pro_obj->cat_id);

            $this->data["order_id"]=$order_obj->order_id;
            $this->data["cat_obj"]=$cat_obj;

        }

        if ($request->method()=="POST"){

            $order_id=$request->get("order_id","");
            $order_obj=clients_orders_m::findOrFail($order_id);
            $bill_obj=clients_bills_m::findOrFail($order_obj->client_bill_id);
            $pro_obj=products_m::findOrFail($order_obj->pro_id);

            $replace_with_pro_id=$request->get("replace_with_pro_id");
            $replace_obj=products_m::findOrFail($replace_with_pro_id);

            //check if $pro_obj has the same cat_id
            if($pro_obj->cat_id!=$replace_obj->cat_id){
                return Redirect::to('admin/client_bills/replace_bill_order?order_id='.$order_id)->with(
                    "msg","<div class='alert alert-danger'>يجب ان تختار البديل من نفس التصنيف</div>"
                )->send();
            }

            //get $replace_with_pro_id quantity in this branch
            $replace_product_in_branch=branches_products_m::
            where("branch_id",$bill_obj->branch_id)->
            where("pro_id",$replace_with_pro_id)->
            get()->first();

            $pro_obj_in_branch=branches_products_m::
            where("branch_id",$bill_obj->branch_id)->
            where("pro_id",$order_obj->pro_id)->
            get()->first();

            if(!is_object($replace_product_in_branch)){
                return Redirect::to('admin/client_bills/replace_bill_order?order_id='.$order_id)->with(
                    "msg","<div class='alert alert-danger'>لا يمكنك استخدام هذا الصنف لعدم وجوده لديك في الفرع</div>"
                )->send();
            }

            if(!is_object($pro_obj_in_branch)){
                return Redirect::to('admin/client_bills/replace_bill_order?order_id='.$order_id)->with(
                    "msg","<div class='alert alert-danger'>الصنف المراد تبديله لا يوجد لديك في المخزن</div>"
                )->send();
            }

            if($replace_product_in_branch->b_p_quantity<($order_obj->order_quantity-$order_obj->order_return)){
                return Redirect::to('admin/client_bills/replace_bill_order?order_id='.$order_id)->with(
                    "msg","<div class='alert alert-danger'>لا يمكنك استخدام هذا الصنف لعدم وجود عدد كافي منه للتبديل</div>"
                )->send();
            }


            //update store increase $pro_obj and decrease $replace_obj
            $replace_product_in_branch->update([
                "b_p_quantity"=>$replace_product_in_branch->b_p_quantity-($order_obj->order_quantity-$order_obj->order_return)
            ]);

            $pro_obj_in_branch->update([
                "b_p_quantity"=>$pro_obj_in_branch->b_p_quantity+($order_obj->order_quantity-$order_obj->order_return)
            ]);


            $order_obj->update(["pro_id"=>$replace_obj->pro_id]);



            return Redirect::to('admin/client_bills/replace_bill_order?order_id='.$order_id)->with(
                "msg","<div class='alert alert-success'>تم التبديل بنجاح</div>"
            )->send();
        }



        return view("admin.subviews.branch_bills.with_clients.return.replace_order",$this->data);
    }

    public function get_bill_orders(Request $request)
    {

        $output = array();
        $output["success"] = "error";
        $output["msg"] = "";

        if ($request->method() == "POST")
        {
            $bill_id = clean($request->get("bill_id"));
            if (!isset($bill_id) && empty($bill_id))
            {
                $output["msg"] = "<div class='alert alert-danger'> من فضلك ادخل رقم الفاتورة اولا </div>";
                echo json_encode($output);
                return;
            }

            $bill_obj = clients_bills_m::get_clients_bills(" AND bill.client_bill_id = $bill_id ");
            if (count($bill_obj) == 0)
            {
                $output["msg"] = "<div class='alert alert-danger'> الفاتورة غير موجودة</div>";
                echo json_encode($output);
                return;
            }

            $bill_obj = $bill_obj[0];
            $output["success"] = "success";
            $this->branch_id=$bill_obj->branch_id;

            // get all bill purchases orders
            $bill_orders = clients_orders_m::get_bill_orders("
                AND client_bill.client_bill_id=$bill_id
                AND client_bill.branch_id=$this->branch_id
                AND clients_order.order_return = 0 
                AND clients_order.is_gift=0
            ");

            // in some way bill not have orders
            if(count($bill_orders) == 0)
            {
                $output["success"] = "error";
                $output["msg"] = "<div class='alert alert-danger'> الفاتورة لا تحتوي علي طلبات</div>";
                echo json_encode($output);
                return;
            }

            // get all bill returns orders
            $bill_return_orders = clients_orders_m::get_bill_orders(" 
                AND client_bill.client_bill_id=$bill_id
                AND client_bill.branch_id=$this->branch_id
                AND clients_order.order_return = 1
                order by clients_order.order_id desc
            ");

            $output["return_orders_table"] = "";
            if (count($bill_return_orders))
            {
                $output["return_orders_table"] = \View::make("admin.subviews.branch_bills.with_clients.return.bill_returns_orders_table",
                    [
                        "orders"=>$bill_return_orders
                    ]
                )->render();
            }

            $output["bill_data"] = "<h3> الفاتورة رقم.$bill_obj->client_bill_id , لديها (".count($bill_orders).") عنصر</h3>";
            $output["bill_data"] .= "<h5> <i class='fa fa-clock-o'></i> أنشأت من ".Carbon::createFromTimestamp(strtotime($bill_obj->client_bill_date))->diffForHumans()." - ($bill_obj->client_bill_date) </h5>";

            $output["table_data"] = "<input type='hidden' value='".$bill_obj->client_bill_id."' class='form-control' name='bill_id' >";


            $get_pro_arr = branches_products_m::all();
            $get_pro_arr = $get_pro_arr->groupBy("pro_id");


            $output["table_data"] .= \View::make("admin.subviews.branch_bills.with_clients.return.branch_products_table",
                [
                    "orders" => $bill_orders,
                    "return_orders" => $bill_return_orders,
                    "branch_product" => $get_pro_arr,
                    "user_permissions"=>$this->user_permissions,
                    "current_user"=>$this->current_user_data
                ])->render();


            echo json_encode($output);
            return;

        }

    }

    #endregion

    #region branch target
    public function show_client_bills_statistics()
    {

        if(!check_permission($this->user_permissions,"admin/client_orders","show_client_bills_statistics",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس من المسسموح لك ان  تدخل هذه الصفحة</div>"])->send();
        }

        $branch_id = "";

        if ($this->data["current_user"]->user_type == "admin")
        {
            $all_branches = User::get_users(" AND u.user_type = 'branch' ");

            if (isset($_GET["branch_id"]) && $_GET["branch_id"] > 0)
            {
                $branch_id = $_GET["branch_id"];
            }

        }
        else{
            $all_branches = User::get_users(" AND u.user_id = $this->branch_id ");
            $branch_id = $this->branch_id;
        }

        $this->data['all_branches'] = $all_branches;

        $current_data=Carbon::now();

        #region total clients orders,returns this day
        $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 5);
        $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day+1, $hour = 4);
        $this->data["start_day_date"]=$start_day->toDateTimeString();
        $this->data["end_day_date"]=$end_day->toDateTimeString();


        $order_current_day=$this->get_bills(
            $all="",$day=0,$month=0,$year=0,$date_range=[$start_day,$end_day] , $branch_id
        );

        $this->data["this_day_buy_amount"]=$order_current_day[0];
        $this->data["this_day_return_amount"]=$order_current_day[1];
        #endregion

        #region total clients orders,returns day before
        $start_before_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5);
        $end_before_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4);
        $this->data["start_before_date"]=$start_before_day->toDateTimeString();
        $this->data["end_before_date"]=$end_before_day->toDateTimeString();

        $order_before_day=$this->get_bills(
            $all="",$day=0,$month=0,$year=0,$date_range=[$start_before_day,$end_before_day]
        );

        $this->data["before_day_buy_amount"]=$order_before_day[0];
        $this->data["before_day_return_amount"]=$order_before_day[1];
        #endregion

        #region clients orders this month in this year
        $this_month_orders=$this->get_bills($all="",$day=0,$month=Carbon::now()->month,$year=Carbon::now()->year);
        $this->data["buy_amount_this_month"]=$this_month_orders[0];
        $this->data["return_amount_this_month"]=$this_month_orders[1];
        #endregion

        #region clients orders  this year
        $this_year_orders=$this->get_bills($all="",$day=0,$month=0,$year=Carbon::now()->year);
        $this->data["buy_amount_this_year"]=$this_year_orders[0];
        $this->data["return_amount_this_year"]=$this_year_orders[1];
        #endregion


        return view("admin.subviews.branch_bills.with_clients.statistics.index",$this->data);
    }

    private function get_bills($all="",$day=0,$month=0,$year=0,$date_range="",$return_before_group=false){
        if($all!=""){
            $clients_bills=clients_bills_m::
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0&&$month!=0&&$day!=0){
            $clients_bills=clients_bills_m::
            whereDay("client_bill_date",'=',$day)->
            whereMonth("client_bill_date",'=',$month)->
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0&&$month!=0){
            $clients_bills=clients_bills_m::
            whereMonth("client_bill_date",'=',$month)->
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0){
            $clients_bills=clients_bills_m::
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }


        elseif ($date_range!=""){
            $clients_bills=clients_bills_m::
            whereBetween('client_bill_date', [$date_range[0], $date_range[1]])->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        else{
            return[0,0];
        }

        if($return_before_group){
            return $clients_bills;
        }

        $buy_amount=convert_inside_obj_to_arr($clients_bills,"client_bill_total_amount");
        $return_amount=convert_inside_obj_to_arr($clients_bills,"client_bill_total_return_amount");

        $buy_amount=array_sum($buy_amount);
        $return_amount=array_sum($return_amount);

        return [$buy_amount,$return_amount];
    }

    public function branch_target($target_type='year',$year=0,$month=0){

        if(!in_array($target_type,["day","month","year"])){
            $target_type="year";
        }

        if(!check_permission($this->user_permissions,"admin/client_orders","check_branch_target",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس من المسسموح لك ان  تدخل هذه الصفحة</div>"])->send();
        }

        $this->data["target_type"]=$target_type;
        $this->data["target_year"]=$year;
        $this->data["target_month"]=$month;


        if($target_type=="day"){

            //get target of branch of this month
            $branch_target_this_month=branch_target_m::
            where("branch_id",$this->branch_id)->
            where("year",$year)->
            where("month",$month)->
            get()->first();

            $this->data["branch_target_this_month"]=$branch_target_this_month;

            //get days number of this month
            $month_days_number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $this->data["month_days_number"]=$month_days_number;

            $all_bills=[];
            if($month>0&&$year>0){
                $all_bills=$this->get_bills(
                    $all="",
                    $day=0,
                    $month,
                    $year,
                    $date_range="",
                    $return_before_group=true
                );
            }
            $all_bills=collect($all_bills)->groupBy("created_day")->all();

            $this->data["all_bills"]=$all_bills;
        }
        elseif($target_type=="month"){

            //get target of branch of this year
            $branch_target_this_year=branch_target_m::
            where("branch_id",$this->branch_id)->
            where("year",$year)->
            get()->groupBy("month")->all();

            $this->data["branch_target_this_year"]=$branch_target_this_year;

            $all_bills=[];
            if($year>0){
                $all_bills=$this->get_bills(
                    $all="",
                    $day=0,
                    $month=0,
                    $year,
                    $date_range="",
                    $return_before_group=true
                );
            }
            $all_bills=collect($all_bills)->groupBy("created_month")->all();


            $this->data["all_bills"]=$all_bills;
        }
        elseif($target_type=="year"){
            $branch_target_all_years=branch_target_m::
            where("branch_id",$this->branch_id)->
            get()->groupBy("year")->all();


            $this->data["branch_target_all_years"]=$branch_target_all_years;

            $all_bills=$this->get_bills(
                $all="all",
                $day=0,
                $month=0,
                $year=0,
                $date_range="",
                $return_before_group=true
            );
            $all_bills=collect($all_bills)->groupBy("created_year")->all();


            $this->data["all_bills"]=$all_bills;
        }


        return view("admin.subviews.branch_bills.with_clients.statistics.branch_target",$this->data);

    }


    #endregion


}
