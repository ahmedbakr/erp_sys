<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\dashbaord_controller;
use App\Http\Controllers\is_admin_controller;
use App\models\admin\points_of_sale_m;
use App\models\admin\price_list_m;
use App\models\admin\users_transits_m;
use App\models\attachments_m;
use App\models\balance_settings_m;
use App\models\bills\clients_bills_m;
use App\models\bills\supplier_bills_m;
use App\models\money_transfers_m;
use App\models\orders\clients_orders_m;
use App\models\orders\suppliers_materials_m;
use App\models\permissions\permission_pages_m;
use App\models\permissions\permissions_m;
use App\models\user_as_sponsor_m;
use App\models\user_messages_m;
use App\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class users extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    #region users

    public function get_all(Request $request,$user_type)
    {
        if(!check_permission($this->user_permissions,"factory/users","show_action",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with("msg","
                <div class='alert alert-danger'>
                 ليس لديك الصلاحية للدخول هنا
                </div>
            ")->send();
        }

        $this->data["user_type"] = $user_type;
        $filter_branch_cond = "";
        if ($this->data["current_user"]->user_type == "branch_admin" &&
            ($user_type == 'admin' || $user_type == 'branch' || $user_type == 'branch_admin'))
        {
            return Redirect::to('admin/dashboard')->with("msg","
                    <div class='alert alert-danger'>غير مسموح بالدخول هنا</div>
                ")->send();
        }

        if ($this->data["current_user"]->user_type == "branch_admin")
        {
            $branch_id = $this->data["current_user"]->related_id;
            $filter_branch_cond = " AND client.related_id = $branch_id ";
        }


        if ($user_type == 'admin')
        {
            $this->data["users"]=User::get_all_users(" AND user.user_type in ('admin')");
            return view("admin.subviews.users.users.show.show_admins",$this->data);
        }
        elseif ($user_type == 'branch')
        {
            $this->data["users"]=User::get_all_users(" AND user.user_type in ('branch')");
            return view("admin.subviews.users.users.show.show_branches",$this->data);
        }
        elseif ($user_type == 'branch_admin')
        {
            $this->data["users"]=User::get_all_users(" AND user.user_type in ('branch_admin')");
            return view("admin.subviews.users.users.show.show_branches_admins",$this->data);
        }
        elseif ($user_type == 'customer')
        {
            if ($this->data["current_user"]->user_type == "branch_admin")
            {
                $users_pagination=User::where("user_type","customer")->where("related_id",$branch_id);
            }
            else{
                $users_pagination=User::where("user_type","customer");
            }

            $search_for_user=$request->get("search_for_user","");

            if($search_for_user!=""){
                $users_pagination->where("full_name","like","%$search_for_user%");
                $users_pagination->orWhere("full_name_en","like","%$search_for_user%");
                $users_pagination->orWhere("user_tel","like","%$search_for_user%");
            }

            $this->data["search_for_user"]=$search_for_user;

            $users_pagination=$users_pagination->paginate(50);
            $users_pagination->appends(Input::except("page"));

            $this->data["users_pagination"]=$users_pagination;

            $this->data["users"]=[];
            if (count($users_pagination->pluck("user_id")->all())){
                $this->data["users"]=User::get_user_money_transfer_statistics("
                    AND client.user_type in ('customer')
                    $filter_branch_cond
                    AND client.user_id in (".implode(",",$users_pagination->pluck("user_id")->all()).")
                ");
            }

            return view("admin.subviews.users.users.show.show_customers",$this->data);
        }
        elseif ($user_type == 'supplier')
        {
            $this->data["users"]=User::get_user_money_transfer_statistics(" AND client.user_type in ('supplier')");
            return view("admin.subviews.users.users.show.show_suppliers",$this->data);
        }

    }

    public function client_profile(Request $request,$user_id){

        if(!check_permission($this->user_permissions,"admin/users","show_client_profile",$this->current_user_data)){
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس مسموح لك ان تري هذه الصفحة</div>"])->send();
        }

        $current_branch_id=$request->get("branch_id",0);

        if($current_branch_id==0){
            return Redirect::to('admin/dashboard')->
            with(["msg"=>"<div class='alert alert-danger'>branch id is required</div>"])->send();
        }

        //check user
        $user_obj=User::get_all_branch_customers(" 
            AND client.user_type in ('customer') 
            AND (client.related_id=$current_branch_id)
            AND client.user_id=$user_id
            "
        );

        if(!isset_and_array($user_obj)){
            abort(404);
        }

        $user_obj=$user_obj[0];
        $this->data["user_obj"]=$user_obj;


        //get user bills
        $user_bills=clients_bills_m::get_clients_bills(" AND bill.customer_id=$user_id");
        $this->data["user_bills"] = $user_bills;

        //get products that buy from branch
        $this->data["buyed_pros"]=[];
        if (isset_and_array($user_bills)){
            $user_bill_ids=convert_inside_obj_to_arr($user_bills,"client_bill_id");
            $bill_orders=clients_orders_m::get_bill_orders("AND clients_order.order_id in (".implode(",",$user_bill_ids).")");
            $bill_orders=collect($bill_orders)->groupBy("pro_id")->all();

            $buyed_pros=[];
            foreach($bill_orders as $pro_id=>$pro_orders){
                $buyed_pros[$pro_id]=[];

                $pro_orders=collect($pro_orders)->groupBy("order_return")->all();
                //$pro_orders[0] is selled products
                if(isset($pro_orders[0])&&count($pro_orders[0])){
                    $pro_orders[0]=$pro_orders[0]->all();

                    $buyed_pros[$pro_id]["pro_name"]=$pro_orders[0][0]->pro_name;
                    $buyed_pros[$pro_id]["pro_barcode"]=$pro_orders[0][0]->pro_barcode;

                    $buyed_qunatity=convert_inside_obj_to_arr($pro_orders[0],"order_quantity");
                    $buyed_pros[$pro_id]["buyed_quantity"]=array_sum($buyed_qunatity);
                }

                if(isset($pro_orders[1])&&count($pro_orders[1])){
                    $pro_orders[1]=$pro_orders[1]->all();

                    $buyed_qunatity=convert_inside_obj_to_arr($pro_orders[1],"order_quantity");
                    $buyed_pros[$pro_id]["buyed_quantity"]=$buyed_pros[$pro_id]["buyed_quantity"]-array_sum($buyed_qunatity);
                }


            }

            $this->data["buyed_pros"]=$buyed_pros;
        }


        //get user all transfer money

        $money_transfers_to_user=money_transfers_m::where("to_user_id",$user_id)->get()->all();
        $money_transfers_from_user=money_transfers_m::where("from_user_id",$user_id)->get()->all();

        $this->data["money_transfers_to_user"]=$money_transfers_to_user;
        $this->data["money_transfers_from_user"]=$money_transfers_from_user;



        return view("admin.subviews.users.clients.client_profile",$this->data);
    }

    public function money_transfers(Request $request,$money_transfer_id){

        if(!check_permission($this->user_permissions,"branch/users","finish_money_transfer",$this->current_user_data)){
            return Redirect::to('branch/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس مسموح لك ان تري هذه الصفحة</div>"])->send();
        }

        $money_transfer_data=money_transfers_m::findOrFail($money_transfer_id);
        $this->data["money_transfer_data"]=$money_transfer_data;

        $from_user_obj=User::find($money_transfer_data->from_user_id);
        $to_user_obj=User::find($money_transfer_data->to_user_id);

        $branch_id=0;

        $this->data["from_branch"]="";
        if($from_user_obj->user_type=="branch"){
            //money from branch to user
            $this->data["from_branch"]="yes";
            $branch_id=$from_user_obj->user_id;
        }
        else{
            $this->data["from_branch"]="";
            $branch_id=$to_user_obj->user_id;
        }


        if ($request->method()=="POST"){

            $money_transfer_cash_money=$request->get("money_transfer_cash_money");
            $money_transfer_atm_money=$request->get("money_transfer_atm_money");


            if(!check_permission($this->user_permissions,"admin/users","finish_money_transfer_with_atm",$this->current_user_data)){
                $money_transfer_atm_money=0;
            }


            if($money_transfer_data->m_t_amount<($money_transfer_cash_money+$money_transfer_atm_money)){
                return Redirect::to('branch/users/clients/money_transfers/'.$money_transfer_id)->with(
                    ["msg"=>"<div class='alert alert-danger'>لا يمكنك ان تدفع اكثر من القيمة المسموح بها</div>"]
                )->send();
            }

            if(!check_permission($this->user_permissions,"admin/users","finish_money_transfer_partially",$this->current_user_data)){
                if($money_transfer_data->m_t_amount!=($money_transfer_cash_money+$money_transfer_atm_money)){
                    return Redirect::to('branch/users/clients/money_transfers/'.$money_transfer_id)->with(
                        ["msg"=>"<div class='alert alert-danger'>يجب عليك ان تدفع المبلغ بالكامل</div>"]
                    )->send();
                }
            }



            $remain_money_after_new_transfer=$money_transfer_data->m_t_amount-($money_transfer_cash_money+$money_transfer_atm_money);


            $client_id=$money_transfer_data->to_user_id;
            if($from_user_obj->user_type!="branch") {
                $client_id=$money_transfer_data->from_user_id;
            }


            //update cash and atm money for bill
            $bill_obj=clients_bills_m::find($money_transfer_data->m_t_bill_id);

            if(is_object($bill_obj)){

                if($money_transfer_cash_money>0){
                    $bill_obj->update([
                        "client_total_paid_amount_in_cash"=>($bill_obj->client_total_paid_amount_in_cash+$money_transfer_cash_money)
                    ]);
                }

                if($money_transfer_atm_money>0){
                    $bill_obj->update([
                        "client_total_paid_amount_in_atm"=>($bill_obj->client_total_paid_amount_in_atm+$money_transfer_atm_money)
                    ]);
                }

                if(($money_transfer_cash_money+$money_transfer_atm_money)>0){
                    $bill_obj->update([
                        "client_bill_total_remain_amount"=>($bill_obj->client_bill_total_remain_amount-($money_transfer_cash_money+$money_transfer_atm_money))
                    ]);
                }

            }

            if($money_transfer_cash_money>0){
                //add new row with this value
                money_transfers_m::create([
                    "from_user_id"=>$money_transfer_data->from_user_id,
                    "to_user_id"=>$money_transfer_data->to_user_id,
                    "m_t_bill_id"=>$money_transfer_data->m_t_bill_id,
                    "m_t_bill_type"=>$money_transfer_data->m_t_bill_type,
                    "m_t_amount"=>$money_transfer_cash_money,
                    "m_t_amount_type"=>"cash",
                    "m_t_status"=>"1",
                    "m_t_desc"=>"دفع فلوس متاخرة"
                ]);
            }

            if($money_transfer_atm_money>0){
                //add new row with this value
                money_transfers_m::create([
                    "from_user_id"=>$money_transfer_data->from_user_id,
                    "to_user_id"=>$money_transfer_data->to_user_id,
                    "m_t_bill_id"=>$money_transfer_data->m_t_bill_id,
                    "m_t_bill_type"=>$money_transfer_data->m_t_bill_type,
                    "m_t_amount"=>$money_transfer_atm_money,
                    "m_t_amount_type"=>"atm",
                    "m_t_status"=>"1",
                    "m_t_desc"=>"دفع فلوس متاخرة"
                ]);
            }


            if($remain_money_after_new_transfer==0){
                //delete this money transfer row
                money_transfers_m::destroy($money_transfer_id);
            }
            else{
                $money_transfer_data->update([
                    "m_t_amount"=>$remain_money_after_new_transfer
                ]);
            }



            return Redirect::to('admin/users/clients/client_profile/'.$client_id."?branch_id=".$branch_id)->with(
                ["msg"=>"<div class='alert alert-success'>تم بحمد الله</div>"]
            )->send();

        }

        return view("admin.subviews.users.clients.money_transfer",$this->data);
    }

    public function save_user(Request $request,$user_type="admin",$user_id = null)
    {
        //create factory_admin,branch

        if($user_id==null){
            if(!check_permission($this->user_permissions,"factory/users","add_action",$this->current_user_data)){
                return Redirect::to('admin/dashboard')->with("msg","
                    <div class='alert alert-danger'>ليس لديك الصلاحية للدخول هنا</div>
                ")->send();
            }
        }
        else{
            if(!check_permission($this->user_permissions,"factory/users","edit_action",$this->current_user_data)){
                return Redirect::to('admin/dashboard')->with("msg","
                    <div class='alert alert-danger'>ليس لديك الصلاحية للدخول هنا</div>
                ")->send();
            }
        }


        $this->data["user_data"] = "";
        $password_required = "required";
        $logo_id=0;

        $this->data["user_type"]=$user_type;

        $this->data["disabled_select_price_list"] = "";
        $this->data["all_branches"] = [];
        $this->data["all_point_of_sales"] = [];
        $this->data["prices_list"] = [];

        if($user_type == "customer"){
            if ($this->data["current_user"]->user_type == "branch_admin")
            {
                if ($user_id == null)
                {
                    $this->data["prices_list"]=price_list_m::where("default_price",1)->get()->all();
                }
                else{
                    $get_user_obj = User::find($user_id);
                    $this->data["prices_list"]=price_list_m::where("price_id",$get_user_obj->selected_price_id)->get()->all();
                    if (!count($this->data["prices_list"]))
                    {
                        $this->data["prices_list"]=price_list_m::where("default_price",1)->get()->all();
                    }
                    $this->data["disabled_select_price_list"] = "disabled";
                }
            }
            else
            {
                $this->data["prices_list"]=price_list_m::all()->all();
            }

            if (!count($this->data["prices_list"]))
            {
                return Redirect::to('admin/dashboard')->with("msg","
                    <div class='alert alert-danger'>لا توجد قوائم أسعار حتي الان</div>
                ")->send();
            }
        }



        $this->get_branches_and_other_data();
        $get_all_branches = $this->data["all_branches"];
        if (!count($this->data["all_branches"]) && $user_type == "branch_admin")
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'>  لا يوجد فروع !!!   </div>"
            ])->send();
        }

        if ($user_type == "branch_admin")
        {
            $this->data["all_point_of_sales"] = points_of_sale_m::whereIn("branch_id",convert_inside_obj_to_arr($get_all_branches,"user_id"))->get()->all();
        }


        if ($user_id != null)
        {
            $password_required = "";
            $this->data["user_data"] = User::get_users(" AND u.user_id=$user_id");

            if(!isset_and_array($this->data["user_data"])){
                return Redirect::to('admin/dashboard')->send();
            }
            $this->data["user_data"]=$this->data["user_data"][0];
            $logo_id=$this->data["user_data"]->logo_id;
            $this->data["user_data"]->user_img_file=attachments_m::find($logo_id);


        }

        if ($request->method()=="POST")
        {


            if($user_type=='admin' || $user_type=='branch_admin'){
                $this->validate($request,
                    [
                        "full_name"=>"required",
                        "email"=> "required|unique:users,email,".$user_id.",user_id,deleted_at,NULL",
                    ],
                    [
                        "full_name.required" => "الاسم العربي مطلوب إدخالة",
                        "email.required" => "الإيميل مطلوب إدخالة",
                        "email.unique" => "الإيميل موجود مسبقا برجاء تغييرة",
                    ]
                );
            }

            if($user_type=='customer'){
                $this->validate($request,
                    [
                        "user_tel"=> "required|unique:users,user_tel,".$user_id.",user_id,deleted_at,NULL,user_type,customer",
                    ],
                    [
                        "user_tel.required" => "رقم التليفون مطلوب إدخالة",
                        "user_tel.unique" => "رقم التليفون موجود مسبقا برجاء تغييرة",
                    ]
                );
            }


//            if ($user_id != null)
//            {
//                unset($request["related_id"]);
//            }

            $request["user_type"]=$user_type;

            if($request["user_type"]=="factory_admin"){
                $request["related_id"] = $this->factory_id;
            }

            $request["logo_id"] = $this->general_save_img(
                $request ,
                $item_id=$user_id,
                "user_img_file",
                $new_title = "",
                $new_alt = "",
                $upload_new_img_check = $request["user_img_checkbox"],
                $upload_file_path = "/admins",
                $width = 0,
                $height = 0,
                $photo_id_for_edit = $logo_id
            );


            if (isset($request["password"]) && !empty($request["password"]))
            {
                $request["password"] = bcrypt($request["password"]);
            }
            else{
                if ($user_id != null){
                    $request["password"] = $this->data["user_data"]->password;
                }
            }

//            dump($request->all());
//            die();

            // update
            $user_obj="";

            $add_or_edit=($user_id == null)?"add":"edit";

            if ($user_id != null)
            {

                if ($this->data["current_user"]->user_type == "branch_admin")
                {
                    unset(
                        $request['related_id'],
                        $request['point_of_sale_id'],
                        $request['user_code'],
                        $request['start_join_date']
                    );
                }

                $user_obj=User::find($user_id);

                $check = $user_obj->update($request->all());

                if ($check == true)
                {
                    $this->data["msg"] = "<div class='alert alert-success'> لقد تم التعديل بنجاح </div>";
                }
                else{
                    $this->data["msg"] = "<div class='alert alert-danger'> حدث خطأ برجاء المحاولة لاحقا !!!</div>";
                }


                $target_user = "ادمن";
                if ($request["user_type"] == "admin")
                {
                    $target_user = " ادمن ";
                }
                else if ($request["user_type"] == "branch")
                {
                    $target_user = " فرع ";
                }
                else if ($request["user_type"] == "branch_admin")
                {
                    $target_user = " مستخدم ";
                }
                else if ($request["user_type"] == "customer")
                {
                    $target_user = " عميل ";
                }
                else if ($request["user_type"] == "supplier")
                {
                    $target_user = " مورد ";
                }

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " تسجيل $target_user جديد "
                );
                #endregion

            }
            else{

                $request["user_active"] = 1;
                $request["user_can_login"] = 1;

                $user_obj= User::create($request->all());

                if (is_object($user_obj))
                {
                    $this->data["msg"] = "<div class='alert alert-success'> لقد تم الحفظ بنجاح </div>";
                    if ($user_type == "branch_admin")
                    {

                        users_transits_m::create([
                            "user_id" => $user_obj->user_id,
                            "from_branch" => $request["related_id"],
                            "to_branch" => $request["related_id"],
                            "user_transit_date" => $request["start_join_date"]
                        ]);

                    }

                    if ($user_type == "branch_admin")
                    {
                        $permission_arr = [
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 1, // مستخدكي النظام
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 1,
                                "additional_permissions" => "[]"
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 8, // فواتير العملاء
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 1,
                                "additional_permissions" => '["change_bill_status","show_bill_orders","add_return_bill","check_branch_target","allow_cashier_atm","return_bill_allow_use_atm"]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 35, // تارجت الفرع
                                "show_action" => 1,
                                "add_action" => 0,
                                "edit_action" => 0,
                                "delete_action" => 0,
                                "additional_permissions" => '["show_branch_target"]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 4, // طلبات الفرع
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 1,
                                "additional_permissions" => '["change_bill_status","show_bill_orders","add_return_bill"]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 38, // العمولات
                                "show_action" => 1,
                                "add_action" => 0,
                                "edit_action" => 0,
                                "delete_action" => 0,
                                "additional_permissions" => '[]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 39, // قواعد العمولات
                                "show_action" => 1,
                                "add_action" => 0,
                                "edit_action" => 0,
                                "delete_action" => 0,
                                "additional_permissions" => '[]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 36, // إيداعات الفروع
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 0,
                                "additional_permissions" => '[]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 41, // طباعة الفاتورة
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 0,
                                "additional_permissions" => '[]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 69, // سندات العهد
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 0,
                                "additional_permissions" => '["change_duty_document_received"]'
                            ],
                            [
                                "user_id" => $user_obj->user_id,
                                "per_page_id" => 10, // سندات العهد
                                "show_action" => 1,
                                "add_action" => 1,
                                "edit_action" => 1,
                                "delete_action" => 1,
                                "additional_permissions" => '[]'
                            ]
                        ];
                        permissions_m::insert($permission_arr);
                    }

                }
                else{
                    $this->data["msg"] = "<div class='alert alert-danger'> حدث خطأ برجاء المحاولة لاحقا !!!</div>";
                }

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " edit user type ".$request["user_type"]
                );
                #endregion
                $user_id=$user_obj->user_id;
            }




            if($add_or_edit=="add"&&$user_type=="customer"){
                $original_client_tel_number=clean($request->get("original_client_tel_number"));
                $user_tel=clean($request->get("user_tel"));

                //make sure that he is not the same customer
                if($original_client_tel_number!=$user_tel){
                    //check of $original_client_tel_number number

                    $original_client=User::where("user_tel",$original_client_tel_number)->get()->first();


                    if(is_object($original_client)){

                        //check if it is not added before
                        $added_before=user_as_sponsor_m::
                        where("user_id",$user_obj->user_id)->
                        where("sponsor_id",$original_client->user_id)->get()->count();


                        if($added_before==0){
                            user_as_sponsor_m::create([
                                "user_id"=>$user_obj->user_id,
                                "sponsor_id"=>$original_client->user_id,
                                "last_earn_gift_date"=>Carbon::now()->addDays(30)
                            ]);
                        }


                    }
                }

            }


            return Redirect::to('admin/users/save/'.$user_type."/".$user_id)->with([
                "msg"=>$this->data["msg"]
            ])->send();
        }


        return view("admin.subviews.users.users.save")->with($this->data);
    }


    public function import_customers(Request $request)
    {

        if ($request->method() == "POST")
        {

            if (!check_permission($this->user_permissions,"factory/users","add_action",$this->data["current_user"]))
            {
                return Redirect::to('admin/dashboard')->with([
                    "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
                ])->send();
            }

            $name = $_FILES['excel_file']['name'];
            $ext = (explode(".", $name));

            $related_id = clean($request->get('related_id'));
            $selected_price_id = clean($request->get('selected_price_id'));

            if (!($related_id > 0) || !($selected_price_id > 0))
            {
                return Redirect::to('admin/users/save/customer')->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك تأكد من إختيار الفرع والسعر !!  </div>"
                ])->send();
            }

            if (empty($_FILES) || !is_array($ext) || !count($ext) || !isset($ext[1]) || !in_array($ext[1],["xls","XLS","xlsx","XLSX"]))
            {
                return Redirect::to('admin/users/save/customer')->with([
                    "msg" => "<div class='alert alert-danger'> من فضلك ارفع ملف اكسيل فقط  </div>"
                ])->send();
            }


            $this->data['related_id'] = $related_id;
            $this->data['selected_price_id'] = $selected_price_id;
            $this->data['input_arr'] = [];

            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

            Excel::load($_FILES['excel_file']['tmp_name'],function($reader)
            {

                // Getting all results
                $results = $reader->toArray();
                if (is_array($results) && count($results) > 0)
                {

                    foreach ($results as $key => $value)
                    {

                        $value['full_name'] = clean($value['full_name']);
                        $value['full_name_en'] = clean($value['full_name_en']);
                        $value['email'] = clean($value['email']);
                        $value['user_tel'] = clean($value['user_tel']);
                        $value['user_mobile'] = clean($value['user_mobile']);
                        $value['user_fax'] = clean($value['user_fax']);
                        $value['user_address'] = clean($value['user_address']);
                        $value['start_join_date'] = clean($value['start_join_date']);
                        $value['age_range'] = clean($value['age_range']);
                        $value['user_gender'] = clean($value['user_gender']);


                        $check_unique_customer = User::where("user_mobile",$value['user_mobile'])
                            ->orWhere("email",$value['email'])
                            ->get()->all();

                        if (is_array($check_unique_customer) && count($check_unique_customer))
                        {
                            continue;
                        }

                        if (isset($this->data['input_arr'][$value['user_mobile']]) || empty($value['user_mobile'])
                            || empty($value['full_name']))
                        {
                            continue;
                        }

                        $this->data['input_arr'][$value['user_mobile']] = [
                            "full_name" => $value['full_name'],
                            "full_name_en" => $value['full_name_en'],
                            "email" => $value['email'],
                            "user_tel" => $value['user_tel'],
                            "user_mobile" => $value['user_mobile'],
                            "user_fax" => $value['user_fax'],
                            "user_address" => $value['user_address'],
                            "start_join_date" => (!empty($value['start_join_date']))?date('Y-m-d',strtotime($value['start_join_date'])):date('Y-m-d'),
                            "user_type" => 'customer',
                            "related_id" => $this->data['related_id'],
                            "selected_price_id" => $this->data['selected_price_id'],
                            "age_range" => $value['age_range'],
                            "user_gender" => ($value['user_gender'] == 'ذكر')?'male':'female',
                            "created_at" => date('Y-m-d H:i:s')
                        ];

                    }


                }

            });

            if (!count($this->data['input_arr']))
            {
                return Redirect::to('admin/users/save/customer')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحه للإدخال برجاء الإلتزام بالملف المرفق وعدم تغيير اسماء اول صف  </div>"
                ])->send();
            }

            $split_arr = array_chunk($this->data['input_arr'],500);

            foreach ($split_arr as $key => $arr)
            {
                User::insert($arr);
            }


            $desc_msg = ' لقد تم تسجيل العملاء بنجاح ';

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion

            return Redirect::to('admin/users/save/customer')->with([
                "msg" => "<div class='alert alert-success'> $desc_msg  </div>"
            ])->send();


        }


    }

    public function assign_permission(Request $request,$user_id){

        if(!check_permission($this->user_permissions,"factory/users","manage_permissions",$this->data["current_user"])){
            return Redirect::to('admin/dashboard')->send();
        }

        $user_obj=User::where("user_id",$user_id)->get()->first();

        if(!is_object($user_obj)){
            return Redirect::to('admin/dashboard')->send();
        }

        $this->data["user_obj"]=$user_obj;

//        $permissions_type="factory";
//        if($user_obj->user_type=="branch"){
//            $permissions_type="branch";
//        }


        //get all permission pages
        $all_permission_pages=permission_pages_m::where("sub_sys","!=","accountant")->get()->all();
        $all_permission_pages=array_combine(convert_inside_obj_to_arr($all_permission_pages,"per_page_id"),$all_permission_pages);

        //get all user permissions
//        $all_user_permissions=permissions_m::where("user_id",$user_id)->get()->all();
        $all_user_permissions=permissions_m::get_permissions(" AND per.user_id = $user_id ");
        $all_user_permissions=array_combine(convert_inside_obj_to_arr($all_user_permissions,"per_page_id"),$all_user_permissions);

        $this->data["all_permission_pages"]=$all_permission_pages;
        $this->data["all_user_permissions"]=$all_user_permissions;


        foreach($all_user_permissions as $user_per_key=>$user_per_val){
            unset($all_permission_pages[$user_per_key]);
        }



        $all_user_permissions_groups = collect($all_user_permissions)->groupBy('sub_group_ar');
        $this->data["all_user_permissions_groups"]=$all_user_permissions_groups;


        if(isset_and_array($all_permission_pages)){
            foreach($all_permission_pages as $page_key=>$page_val){
                permissions_m::create([
                    "user_id"=>"$user_id",
                    "per_page_id"=>"$page_key"
                ]);
            }

            return Redirect::to('admin/users/assign_permission/'.$user_id)->send();
        }


        if($request->method()=="POST"){

            foreach($all_user_permissions as $user_per_key=>$user_per_val){
                $new_perms=$request->get("additional_perms_new".$user_per_val->per_id);
                permissions_m::where("per_id",$user_per_val->per_id)->update([
                    "additional_permissions"=>json_encode($new_perms)
                ]);
            }


            return Redirect::to('admin/users/assign_permission/'.$user_id)->with([
                "msg"=>"<div class='alert alert-success'>تعديل اذونات المستخدم للصفحات بنجاح</div>"
            ])->send();

        }



        return view("admin.subviews.users.users.user_permissions",$this->data);
    }

    public function change_password(Request $request){


        if($request->method()=="POST"){
            $old_password = ($request->get("old_password"));

            $user_obj=User::find($this->user_id);

            if(Hash::check($old_password, $user_obj->password)&&!empty($request->get("new_password"))){
                $new_password = bcrypt($request->get("new_password"));
                $user_obj->update([
                    "password"=>$new_password
                ]);

                return Redirect::to('/admin/dashboard')->with([
                    "msg"=>"<div class='alert alert-success'>تم تغيير الباسورد</div>"
                ])->send();
            }


        }


        return view("admin.subviews.users.users.change_password")->with($this->data);
    }

    public function export_excel()
    {
        if ($this->data["current_user"]->user_type == "branch_admin")
        {
            $branch_id = $this->data["current_user"]->related_id;
            $users = User::where("user_type","customer")->where("related_id",$branch_id);
        }
        else{
            $users = User::where("user_type","customer");
        }

        $all_users = [];
        if (count($users->pluck("user_id")->all())){
            $all_users = User::get_user_money_transfer_statistics("
                    AND client.user_type in ('customer')
                    AND client.user_id in (".implode(",",$users->pluck("user_id")->all()).")
                ");
        }

        $export_arr = [];
        if (count($all_users))
        {

            $users_total_payed_cash=0;
            $users_total_payed_atm=0;

            $total_user_received_cash=0;
            $total_user_received_atm=0;

            $total_user_remain_for_him=0;
            $total_user_remain_for_branch=0;

            foreach($all_users as $key => $user)
            {

                $users_total_payed_cash=$users_total_payed_cash+$user->total_payed_cash;
                $users_total_payed_atm=$users_total_payed_atm+$user->total_payed_atm;

                $total_user_received_cash=$total_user_received_cash+$user->total_received_cash;
                $total_user_received_atm=$total_user_received_atm+$user->total_received_atm;

                $total_user_remain_for_him=$total_user_remain_for_him+$user->total_remain_for_him;
                $total_user_remain_for_branch=$total_user_remain_for_branch+$user->total_remain_for_branch;

                $export_arr[] = [
                    "#" => ($key + 1),
                    "كود العميل" => $user->user_id,
                    "اسم العميل" => $user->full_name,
                    "ايميل العميل" => $user->email,
                    "تليفون العميل" => $user->user_tel,
                    "موبايل العميل" => $user->user_mobile,
                    "فاكس العميل" => $user->user_fax,
                    "تاريخ بدء التعامل العميل" => $user->start_join_date,
                    "عنوان العميل" => $user->user_address,
                    "سعر العميل" => $user->client_price_title,
                    "الفئة العمرية للعميل" => $user->age_range,
                    "نوع العميل" => $user->user_gender,
                    "عدد العملاء الحاليين من طرفه" => $user->user_as_sponsor_count." عميل ",
                    "مقدار ما دفعه" => " (كاش : $user->total_payed_cash) || ( شبكة : $user->total_payed_atm ) ",
                    "مقدار ما استلمه" => " (كاش : $user->total_received_cash) || ( شبكة : $user->total_received_atm ) ",
                    "المتبقي له" => $user->total_remain_for_him,
                    "المتبقي عليه" => $user->total_remain_for_branch,
                ];
            }
        }

        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);


        Excel::create("Customers", function($excel)use($export_arr) {

            $excel->sheet('Sheet1', function($sheet)use($export_arr) {
                $sheet->fromArray($export_arr);
            });

        })->export('xls');


    }

    public function remove_customer(Request $request)
    {

        if (!check_permission($this->user_permissions,"factory/users","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        // check if not have bills
        $item_id = (int)$request->get("item_id");
        $get_bills = clients_bills_m::where("customer_id",$item_id)->get()->all();
        if (count($get_bills))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح بمسح هذا العميل لوجود فواتير له</div>"]);
            return;
        }
        
        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تم مسح بيانات عميل"
        );
        #endregion

        $this->general_remove_item($request,'App\User');
    }

    #endregion




    #region Supplier Profile

    public function supplier_profile($supplier_id)
    {

        if (!$this->check_user_permission("suppliers/profile","show_action"))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $supplier_data = User::get_users(" AND u.user_id = $supplier_id ");

        if (count($supplier_data) == 0)
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> المورد غير موجود  </div>"
            ])->send();
        }

        $this->data["supplier_data"] = $supplier_data[0];
        if(!empty($supplier_data[0]->contacts))
        {
            $this->data["supplier_data"]->contacts = json_decode($supplier_data[0]->contacts);
        }
        
        $this->data["bills_data"] = "";
        $bills_data = supplier_bills_m::get_bill_orders_with_money_transfers(" AND supplier_bill.supplier_id = $supplier_id order by supplier_bill.created_at desc ");

        $this->data["bills_data"] = $bills_data;

        $this->data["total_paid"] = 0;
        $this->data["total_remained"] = 0;
        $this->data["total_purchases_orders"] = 0;
        $this->data["total_returns_orders"] = 0;
        $this->data["total_returns_received"] = 0;
        $this->data["total_returns_remained"] = 0;


        if (is_array($bills_data) && count($bills_data) && isset($bills_data[0]->supplier_bill_id))
        {
            $this->data["total_paid"] = array_sum(convert_inside_obj_to_arr($bills_data,"total_paid_money"));
            $this->data["total_remained"] = array_sum(convert_inside_obj_to_arr($bills_data,"total_remain_money"));
            $this->data["total_purchases_orders"] = array_sum(convert_inside_obj_to_arr($bills_data,"total_purchases_amount"));
            $this->data["total_returns_orders"] = array_sum(convert_inside_obj_to_arr($bills_data,"total_returns_amount"));
            $this->data["total_returns_received"] = array_sum(convert_inside_obj_to_arr($bills_data,"return_received_money"));
            $this->data["total_returns_remained"] = array_sum(convert_inside_obj_to_arr($bills_data,"return_remained_money"));
        }

        $this->data["material_purchases_orders_names"] = [];
        $this->data["material_returns_orders_names"] = [];

        $supplier_orders = suppliers_materials_m::get_orders_data(" AND sup_mat.sup_mat_user_id = $supplier_id ");
        if (is_array($supplier_orders) && count($supplier_orders))
        {
            $supplier_orders = collect($supplier_orders);
            $supplier_orders = $supplier_orders->groupBy("sup_mat_return");

            // purchases orders
            if (isset($supplier_orders[0]) && count($supplier_orders[0]))
            {
                $get_purchases_material_arr = $supplier_orders[0];
                $get_purchases_material_arr = collect($get_purchases_material_arr);
                $get_purchases_material_arr = $get_purchases_material_arr->groupBy('mat_name');

                foreach($get_purchases_material_arr as $key => $value)
                {

                    $temp = collect($value);
                    $temp = $temp->sum("sup_mat_amount");

                    $this->data["material_purchases_orders_names"][$key] = $temp;
                }

            }

            // returns orders
            if (isset($supplier_orders[1]) && count($supplier_orders[1]))
            {
                $get_returns_material_arr = $supplier_orders[1];
                $get_returns_material_arr = collect($get_returns_material_arr);
                $get_returns_material_arr = $get_returns_material_arr->groupBy('mat_name');

                foreach($get_returns_material_arr as $key => $value)
                {

                    $temp = collect($value);
                    $temp = $temp->sum("sup_mat_amount");

                    $this->data["material_returns_orders_names"][$key] = $temp;
                }

            }
        }


//        dump($bills_data);

        return view("admin.subviews.users.supplier_profile",$this->data);
    }

    #endregion

    public function make_sponsor_zero(Request $request){

        $output=[];

        $user_id=$request->get("user_id");

        $client_obj=User::findOrFail($user_id);


        user_as_sponsor_m::where("sponsor_id",$client_obj->user_id)->update([
            "last_earn_gift_date"=>Carbon::now()
        ]);


        $output["msg"]="<div class='alert alert-success'>تم التصفير لهذا العميل الرجاء عدم نسي ان تكافئة علي عدد العملاء اللذين اتوا بواسطنه يمكنك ان تعطيه كوبون بقيمة خصم ايا تكن</div>";
        echo json_encode($output);
    }


}
