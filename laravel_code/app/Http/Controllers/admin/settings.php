<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\product\materials_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class settings extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function show_materials()
    {


        if(!$this->check_user_permission("factory/materials","show_action")){
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["materials"] = materials_m::get()->all();

        return view("admin.subviews.materials.index",$this->data);
    }

    public function save_material(Request $request , $mat_id = null)
    {

        $default_action = "add_action";
        if (isset($mat_id) && $mat_id > 0)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("factory/materials","$default_action")){
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["material_data"] = "";
        if (isset($mat_id))
        {
            $this->data["material_data"] = materials_m::where("mat_id",$mat_id)->get()->first();

            if (!is_object($this->data["material_data"]))
            {
                return Redirect::to('admin/materials/save')->with([
                    "msg" => "<div class='alert alert-danger'> الماده الخام غير موجودة , اضف جديد من هنا </div>"
                ])->send();
            }
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "mat_name" => "required|unique:materials,mat_name,".$mat_id.",mat_id,deleted_at,NULL",
                "mat_type" => "required"
            ]);

            $request_data = $this->cleaning_input($request->all(),$except = array("_token"));
            $desc_msg = " تعديل الماده الخام رقم.$mat_id";

            if (!isset($mat_id))
            {
                // insert
                $mat_obj = materials_m::create($request_data);
                $mat_id = $mat_obj->mat_id;
                $desc_msg = " اضافة ماده خام جديدة برقم.$mat_id";

            }
            else{
                unset($request_data["_token"]);
                $mat_check = materials_m::where("mat_id",$mat_id)->update($request_data);
            }

            #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
            #endregion


            #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
            #endregion

            return Redirect::to('admin/materials/save/'.$mat_id)->with([
                "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح </div>"
            ])->send();

        }

        return view("admin.subviews.materials.save",$this->data);
    }
}
