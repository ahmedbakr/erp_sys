<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\product\product_codes_m;
use App\models\product\product_select_codes_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class product_codes extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/product_codes","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["codes"] = product_codes_m::all();

        return view("admin.subviews.product_codes.show")->with($this->data);
    }


    public function save_code(Request $request, $pro_code_id = null)
    {

        $default_action = "add_action";
        if ($pro_code_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/product_codes","$default_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"])->send();
        }

        $this->data["code_data"] = "";

        if ($pro_code_id != null)
        {
            $code_data = product_codes_m::findOrFail($pro_code_id);
            $this->data["code_data"] = $code_data;
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "pro_code_label" => "required|unique:product_codes,pro_code_label,".$pro_code_id.",pro_code_id,deleted_at,NULL",
                    "pro_code_label_en" => "required|unique:product_codes,pro_code_label_en,".$pro_code_id.",pro_code_id,deleted_at,NULL",
                ],
                [
                    "pro_code_label.required" => "الكود بالعربي مطلوب إدخاله",
                    "pro_code_label.unique" => "الكود بالعربي موجود مسبقا",
                    "pro_code_label_en.required" => "الكود بالإنجليزي مطلوب إدخاله",
                    "pro_code_label_en.unique" => "الكود بالإنجليزي موجود مسبقا",
                ]
            );

            $request = clean($request->all());

            if ($pro_code_id == null)
            {
                // insert
                $pro_code_obj = product_codes_m::create($request);
                $pro_code_id = $pro_code_obj->pro_code_id;
                $action_desc = "تم إضافة كود جديد باسم ".$pro_code_obj->pro_code_label;

            }
            else{
                product_codes_m::find($pro_code_id)->update($request);
                $action_desc = "تم تعديل بيانات كود ".$code_data->pro_code_label;
            }

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $action_desc
            );
            #endregion

            return  Redirect::to('admin/product_codes/save_code/'.$pro_code_id)->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.product_codes.save")->with($this->data);
    }


    public function remove_code(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/product_codes","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        #region save in site_tracking
            $this->track_my_action(
                $action_desc = " تم مسح بيانات كود "
            );
        #endregion

        $item_id = (int)$request->get("item_id");

        // deleted all product_select_codes
        product_select_codes_m::where("pro_code_id",$item_id)->delete();


        $this->general_remove_item($request,'App\models\product\product_codes_m');
    }


}
