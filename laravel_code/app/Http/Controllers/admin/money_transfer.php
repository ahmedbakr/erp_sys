<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\bills\supplier_bills_m;
use App\models\money_transfers_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class money_transfer extends is_admin_controller
{
    
    public function __construct(){
        parent::__construct();
    }


    public function index($supplier_id="",$bill_id="")
    {

        if (!$this->data['user_permissions']['admin/money_transfer'][0]->show_action)
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["supplier_id"]=$supplier_id;
        $this->data["bill_id"]=$bill_id;

        $this->data["suppliers"] = User::get_users(" AND u.user_active = 1 AND u.user_can_login = 1 AND u.user_type = 'supplier' ");

        return view("admin.subviews.money_transfer.index",$this->data);
    }

    public function get_supplier_data(Request $request)
    {

        $output = array();
        $output["msg"]="";

        if(!$this->data['user_permissions']['admin/money_transfer'][0]->show_action)
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }

        $user_id = $request->get("user_id");
        $bill_id = $request->get("bill_id");
        $get_user_data = User::get_users(" AND u.user_active = 1 AND u.user_can_login = 1 AND u.user_type = 'supplier' AND user_id = $user_id ");

        if (is_array($get_user_data) && count($get_user_data) && isset($get_user_data[0]))
        {
            $get_user_data = $get_user_data[0];

            $get_orders_supplier_transfers = money_transfers_m::get_data(" AND money.m_t_bill_id = $bill_id AND money.to_user_id = $user_id ");
            $get_returns_supplier_transfers = money_transfers_m::get_data(" AND money.m_t_bill_id = $bill_id AND money.from_user_id = $user_id ");

            if (is_array($get_orders_supplier_transfers) && count($get_orders_supplier_transfers))
            {

                $temp_output = "";
                // for bill orders
                $temp_output.= \View::make("admin.subviews.money_transfer.bill_orders",
                    [
                        "orders" => $get_orders_supplier_transfers,
                        "header" => "طلبات المشتريات للفاتورة رقم.$bill_id",
                        "status_label" => "إدفع",
                        "allow_atm"=>check_permission($this->user_permissions,"factory/atm_permission","show_action")
                    ])->render();


                // for bill returns
                $temp_output.= \View::make("admin.subviews.money_transfer.bill_orders",
                    [
                        "orders" => $get_returns_supplier_transfers,
                        "header" => "طلبات المرتجع للفاتورة رقم.$bill_id",
                        "status_label" => "إستلم",
                        "allow_atm"=>check_permission($this->user_permissions,"factory/atm_permission","show_action")
                    ])->render();

                if (!empty($temp_output))
                {
                    $output["msg"] = $temp_output;
                    echo json_encode($output);
                    return;
                }
                else{
                    $output["msg"]="<div class='alert alert-danger'> لا توجد تحويلات مالية لهذة الفاتورة </div>";
                    echo json_encode($output);
                    return;
                }


            }
            else{
                $output["msg"]="<div class='alert alert-danger'> المورد $get_user_data->full_name لا توجد له تحويلات مالية للفاتورة رقم $bill_id !! </div>";
                echo json_encode($output);
                return;
            }

        }
        else{
            $output["msg"]="<div class='alert alert-danger'> المورد غير موجود </div>";
            echo json_encode($output);
            return;
        }


    }

    public function change_bill_orders_money_status(Request $request)
    {

        $output = array();

        if(!check_permission($this->user_permissions,"admin/money_transfer","add_action"))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }

        $money_transfer_id=clean($request->get("money_transfer_id"));
        $cash=clean(floatval($request->get("cash")));
        $atm=clean(floatval($request->get("atm")));

        //check if this user has permission to deal by atm or not
        if(!check_permission($this->user_permissions,"factory/atm_permission","show_action")){
            $atm=0;
        }

        $money_transfer=money_transfers_m::find($money_transfer_id);

        if(!is_object($money_transfer)){
            $output["msg"]="<div class='alert alert-danger'> حدث خطأ</div>";
            echo json_encode($output);
            return;
        }

        if(!($cash>0||$atm>0)){
            $output["msg"]="<div class='alert alert-danger'> ادخل قيمة اكبر من الصفر </div>";
            echo json_encode($output);
            return;
        }

        if($money_transfer->m_t_amount<($cash+$atm)){
            $output["msg"]="<div class='alert alert-danger'> لا يمكنك ان تدفع اكثر من $money_transfer->m_t_amount  </div>";
            echo json_encode($output);
            return;
        }
        else if($money_transfer->m_t_amount>=($cash+$atm)){

            $factory_obj_from="";
            $factory_obj_to="";

            if($money_transfer->from_user_id==$this->factory_id){
                $factory_obj_from=User::find($this->factory_id);
            }
            if($money_transfer->to_user_id==$this->factory_id){
                $factory_obj_to=User::find($this->factory_id);
            }




            if($cash>0){
                //add cash row to money_transfers_m to this user

                money_transfers_m::create([
                    "from_user_id"=>$money_transfer->from_user_id,
                    "to_user_id"=>"$money_transfer->to_user_id",
                    "m_t_bill_id"=>"$money_transfer->m_t_bill_id",
                    "m_t_amount"=>"$cash",
                    "m_t_amount_type"=>"cash",
                    "m_t_status"=>"1",
                    "m_t_desc"=>" ملحق لفاتورة رقم $money_transfer->m_t_bill_id "
                ]);

                if(is_object($factory_obj_from)){
                    //decrease it from this user balance
                    $factory_obj_from->update([
                        "user_balance"=>($factory_obj_from->user_balance-$cash)
                    ]);
                }

                if(is_object($factory_obj_to)){
                    //decrease it from this user balance
                    $factory_obj_to->update([
                        "user_balance"=>($factory_obj_to->user_balance+$cash)
                    ]);
                }

            }

            if($atm>0){
                //add atm row to money_transfers_m to this user


                money_transfers_m::create([
                    "from_user_id"=>$money_transfer->from_user_id,
                    "to_user_id"=>"$money_transfer->to_user_id",
                    "m_t_bill_id"=>"$money_transfer->m_t_bill_id",
                    "m_t_amount"=>"$atm",
                    "m_t_amount_type"=>"atm",
                    "m_t_status"=>"1",
                    "m_t_desc"=>" ملحق دفع لفاتورة رقم $money_transfer->m_t_bill_id "
                ]);

                if (is_object($factory_obj_from)){
                    //decrease it from this user balance
                    $factory_obj_from->update([
                        "user_atm_balance"=>($factory_obj_from->user_atm_balance-$atm)
                    ]);
                }

                if (is_object($factory_obj_to)){
                    //decrease it from this user balance
                    $factory_obj_to->update([
                        "user_atm_balance"=>($factory_obj_to->user_atm_balance+$atm)
                    ]);
                }

            }

            //remove this row because it is paid completed
            if($money_transfer->m_t_amount==($cash+$atm)){
                money_transfers_m::destroy($money_transfer->m_t_id);
            }
            else{
                $money_transfer->update([
                    "m_t_amount"=>($money_transfer->m_t_amount)-($cash+$atm)
                ]);
            }

            //change supplier_bill_total_paid,supplier_bill_total_paid_in_cash,supplier_bill_total_paid_in_atm
            //this check mean that this $money_transfer row is for buy order not return order
            if(is_object($factory_obj_from)){
                $bill_obj=supplier_bills_m::find($money_transfer->m_t_bill_id);

                $bill_obj->update([
                    "supplier_bill_total_paid"=>$bill_obj->supplier_bill_total_paid+($cash+$atm),
                    "supplier_bill_total_paid_in_cash"=>$bill_obj->supplier_bill_total_paid_in_cash+$cash,
                    "supplier_bill_total_paid_in_atm"=>$bill_obj->supplier_bill_total_paid_in_atm+$atm
                ]);
            }

            if(is_object($factory_obj_from)){
                $output["msg"]="<div class='alert alert-success'>
            تم دفع ($cash+$atm)
             يرجي ان تحدث الصفحة لكي تري البيانات بعد التعديل
             
</div>";
            }

            if(is_object($factory_obj_to)){
                $output["msg"]="<div class='alert alert-success'>
            تم استلام ($cash+$atm)
             يرجي ان تحدث الصفحة لكي تري البيانات بعد التعديل
             
</div>";
            }




        }



        echo json_encode($output);
    }

}
