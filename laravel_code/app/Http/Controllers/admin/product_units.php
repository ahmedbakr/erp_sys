<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\product\product_units_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class product_units extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if (!check_permission($this->user_permissions,"admin/product_units","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["product_units"] = product_units_m::get()->all();

        return view("admin.subviews.product_units.index",$this->data);
    }

    public function save(Request $request , $pro_unit_id = null)
    {

        $default_action = "add_action";
        if ($pro_unit_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/product_units","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["product_unit_data"] = "";
        if ($pro_unit_id != null)
        {
            $this->data["product_unit_data"] = product_units_m::where("pro_unit_id",$pro_unit_id)->get()->first();

            if (!is_object($this->data["product_unit_data"]))
            {
                return Redirect::to('admin/product_units/save')->with([
                    "msg" => "<div class='alert alert-danger'> وحدة القياس غير موجودة , اضف جديد من هنا </div>"
                ])->send();
            }
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "pro_unit_name" => "required|unique:product_units,pro_unit_name,".$pro_unit_id.",pro_unit_id,deleted_at,NULL",
                ],
                [
                    "pro_unit_name.required" => " الاسم موجود مسبقا برجاء تغييرة "
                ]
            );

            $request_data = $this->cleaning_input($request->all(),$except = array("_token"));

            if ($pro_unit_id == null)
            {
                // insert
                $obj = product_units_m::create($request_data);
                $pro_unit_id = $obj->pro_unit_id;
                $desc_msg = " اضافة وحدة قياس جديدة";

            }
            else{
                unset($request_data["_token"]);
                $mat_check = product_units_m::where("pro_unit_id",$pro_unit_id)->update($request_data);
                $desc_msg = " تم تعديل وحدة قياس ";
            }

            #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
            #endregion


            #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
            #endregion

            return Redirect::to('admin/product_units/save/'.$pro_unit_id)->with([
                "msg" => "<div class='alert alert-success'> لقد تم الحفظ بنجاح </div>"
            ])->send();

        }

        return view("admin.subviews.product_units.save",$this->data);
    }


    public function remove_product_unit(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/product_units","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تم مسح وحدة قياس"
        );
        #endregion

        $this->general_remove_item($request,'App\models\product\product_units_m');
    }

}
