<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\bills\clients_bills_m;
use App\models\branch_target_m;
use App\User;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class branch_target extends is_admin_controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/branch_target","show_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح لك للدخول لهذة الصفحه</div>"]
            )->send();
        }


        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["branch_target"] = branch_target_m::get_data();
        }
        else{
            $this->data["branch_target"] = branch_target_m::get_data(" AND b_target.branch_id = $this->branch_id ");
        }


        return view("admin.subviews.branch_target.show")->with($this->data);
    }


    public function save_target(Request $request ,$branch_target_id = null)
    {

        $default_action = "add_action";
        if ($branch_target_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/branch_target","$default_action",$this->data["current_user"]))
        {
            return  Redirect::to('admin/dashboard')->with(
                ["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]
            )->send();
        }

        $this->data["branch_target_data"] = "";
        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $this->branch_id ");
        }


        if ($branch_target_id != null)
        {
            $branch_target_data = branch_target_m::find($branch_target_id);
            if (!is_object($branch_target_data))
            {
                return  Redirect::to('admin/branch_target/save_target')->with(
                    ["msg"=>"<div class='alert alert-danger'>لا توجد بيانات لهذا التارجت</div>"]
                )->send();
            }

            $this->data["branch_target_data"] = $branch_target_data;
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "branch_id" => "required",
                "year" => "required|numeric|min:0",
                "month" => "required|numeric|min:0",
                "target_amount" => "required|numeric|min:0",
            ]);

            $request = clean($request->all());

            $branch_id = $request["branch_id"];
            $year = $request["year"];
            $month = $request["month"];

            $get_same_obj = branch_target_m::get_data(" 
                    where
                    b_target.branch_id = $branch_id 
                    AND b_target.year = $year 
                    AND b_target.month = $month 
                ");

            if (is_array($get_same_obj) && count($get_same_obj) && $branch_target_id == null)
            {
                return  Redirect::to('admin/branch_target/save_target')->with(
                    ["msg"=>"<div class='alert alert-danger'>بيانات التارجت لهذا الفرع موجودة مسبقا !!!</div>"]
                )->send();
            }

            if ($branch_target_id == null)
            {
                // insert
                $branch_target_obj = branch_target_m::create($request);
                $branch_target_id = $branch_target_obj->branch_target_id;
                $action_desc = "تم إضافة تارجت جديد للفرع ".$branch_target_obj->full_name;

            }
            else{
                branch_target_m::find($branch_target_id)->update($request);
                $action_desc = "تم تعديل تارجت للفرع ".$branch_target_data->full_name;
            }

            #region save in site_tracking
                $this->track_my_action(
                    $action_desc
                );
            #endregion

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion


            #region send user notification

            $this->send_user_notification(
                $not_title = $action_desc ,
                $not_type = "info" ,
                $user_id = $branch_id);

            #endregion

            return  Redirect::to('admin/branch_target/save_target/'.$branch_target_id)->with(
                ["msg"=>"<div class='alert alert-success'>$action_desc</div>"]
            )->send();

        }

        return view("admin.subviews.branch_target.save")->with($this->data);


    }

    public function remove_branch_target(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/branch_target","delete_action",$this->data["current_user"]))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تم مسح تارجت فرع"
        );
        #endregion

        $this->general_remove_item($request,'App\models\branch_target_m');
    }

    #region branch target

    private function get_bills($all="",$day=0,$month=0,$year=0,$date_range="",$return_before_group=false,$branch_id=""){
        if($all!=""){
            $clients_bills=clients_bills_m::
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0&&$month!=0&&$day!=0){
            $clients_bills=clients_bills_m::
            whereDay("client_bill_date",'=',$day)->
            whereMonth("client_bill_date",'=',$month)->
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0&&$month!=0){
            $clients_bills=clients_bills_m::
            whereMonth("client_bill_date",'=',$month)->
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        elseif($year!=0){
            $clients_bills=clients_bills_m::
            whereYear("client_bill_date",'=',$year)->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }


        elseif ($date_range!=""){
            $clients_bills=clients_bills_m::
            whereBetween('client_bill_date', [$date_range[0], $date_range[1]])->
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
        }
        else{
            return[0,0];
        }

        if($branch_id!=""){
            $clients_bills=collect($clients_bills)->groupBy("branch_id")->all();
            if(isset($clients_bills[$branch_id])){
                $clients_bills=$clients_bills[$branch_id]->all();
            }
            else{
                $clients_bills=[];
            }
        }

        if($return_before_group){
            return $clients_bills;
        }

        if(count($clients_bills)==0){
            return [0,0];
        }
        $buy_amount=convert_inside_obj_to_arr($clients_bills,"client_bill_total_amount");
        $return_amount=convert_inside_obj_to_arr($clients_bills,"client_bill_total_return_amount");

        $buy_amount=array_sum($buy_amount);
        $return_amount=array_sum($return_amount);

        return [$buy_amount,$return_amount];
    }

    public function branch_target(Request $request){

        $this->data["all_post_data"]=(object)$request->all();

        $branch_id=$request->get("branch_id",0);
        $target_type=$request->get("target_type",0);
        $year=$request->get("year",0);
        $month=$request->get("month",0);


        if(!in_array($target_type,["day","month","year"])){
            $target_type="year";
        }

        if(!check_permission($this->user_permissions,"admin/branch_target","show_branch_target",$this->data["current_user"])){
            return Redirect::to('branch/dashboard')->with(["msg"=>"<div class='alert alert-danger'>ليس من المسسموح لك ان  تدخل هذه الصفحة</div>"])->send();
        }

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch'");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $branch_id ");
        }

        $this->data["selected_branch_id"]=$branch_id;
        $this->data["target_type"]=$target_type;
        $this->data["target_year"]=$year;
        $this->data["target_month"]=$month;

        $this->data["all_bills"]=[];
        if ($branch_id>0){
            if($target_type=="day"){

                //get target of branch of this month
                $branch_target_this_month=branch_target_m::
                where("branch_id",$branch_id)->
                where("year",$year)->
                where("month",$month)->
                get()->first();

                $this->data["branch_target_this_month"]=$branch_target_this_month;

                //get days number of this month
                $month_days_number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $this->data["month_days_number"]=$month_days_number;

                $all_bills=[];
                if($month>0&&$year>0){
                    $all_bills=$this->get_bills(
                        $all="",
                        $day=0,
                        $month,
                        $year,
                        $date_range="",
                        $return_before_group=true,
                        $branch_id
                    );
                }
                $all_bills=collect($all_bills)->groupBy("created_day")->all();

                $this->data["all_bills"]=$all_bills;
            }
            elseif($target_type=="month"){

                //get target of branch of this year
                $branch_target_this_year=branch_target_m::
                where("branch_id",$branch_id)->
                where("year",$year)->
                get()->groupBy("month")->all();

                $this->data["branch_target_this_year"]=$branch_target_this_year;

                $all_bills=[];
                if($year>0){
                    $all_bills=$this->get_bills(
                        $all="",
                        $day=0,
                        $month=0,
                        $year,
                        $date_range="",
                        $return_before_group=true,
                        $branch_id
                    );
                }
                $all_bills=collect($all_bills)->groupBy("created_month")->all();


                $this->data["all_bills"]=$all_bills;
            }
            elseif($target_type=="year"){
                $branch_target_all_years=branch_target_m::
                where("branch_id",$branch_id)->
                get()->groupBy("year")->all();


                $this->data["branch_target_all_years"]=$branch_target_all_years;

                $all_bills=$this->get_bills(
                    $all="all",
                    $day=0,
                    $month=0,
                    $year=0,
                    $date_range="",
                    $return_before_group=true,
                    $branch_id
                );
                $all_bills=collect($all_bills)->groupBy("created_year")->all();


                $this->data["all_bills"]=$all_bills;
            }
        }
        else{
            $branch_target_all_years=branch_target_m::
            get()->groupBy("branch_id")->all();

            $this->data["branch_target_all_years"]=$branch_target_all_years;

            $all_bills=clients_bills_m::
            select(DB::raw('*,day(client_bill_date) as "created_day",month(client_bill_date) as "created_month",year(client_bill_date) as "created_year" '))->
            get()->all();
            $all_bills=collect($all_bills)->groupBy("branch_id")->all();

            $this->data["all_bills"]=$all_bills;
        }


        return view("admin.subviews.branch_target.statistics.branch_target",$this->data);

    }
    #endregion

}
