<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\is_admin_controller;
use App\models\cheques\cheque_template_items_m;
use App\models\cheques\cheque_templates_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class cheques extends is_admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/cheque_templates","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_cheques"] = cheque_templates_m::get_data();

        return view("admin.subviews.cheque_templates.show",$this->data);
    }

    public function show_cheque_items($cheque_template_id)
    {

        if (!check_permission($this->user_permissions,"admin/cheque_templates","show_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $get_cheque_data = cheque_templates_m::find($cheque_template_id);
        if (is_object($get_cheque_data))
        {
            $this->data["cheque_data"] = $get_cheque_data;
        }
        else{
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> القالب غير موجود  </div>"
            ])->send();
        }

        $get_all_items = cheque_template_items_m::where("cheque_template_id",$cheque_template_id)->get();
        $get_all_items = $get_all_items->all();
        $this->data["cheque_items"] = $get_all_items;
//        dump($get_all_items);

        return view("admin.subviews.cheque_templates.show_items",$this->data);

    }


    public function save_template(Request $request , $cheque_template_id = null)
    {

        $default_action = "add_action";

        if ($cheque_template_id != null)
        {
            $default_action = "edit_action";
        }

        if (!check_permission($this->user_permissions,"admin/cheque_templates","$default_action",$this->data["current_user"]))
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["template"] = "";
        if ($cheque_template_id != null)
        {
            $template = cheque_templates_m::find($cheque_template_id);
            if (is_object($template))
            {
                $this->data["template"] = $template;
            }
            else{
                return Redirect::to('admin/cheques/save_template')->with([
                    "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود يمكنك إضافة جديد من هنا  </div>"
                ])->send();
            }
        }


        $this->data["all_branches"] = "";
        $get_all_branches = User::where("user_type","branch")->get();
        $this->data["all_branches"] = $get_all_branches->all();

        if ($request->method() == "POST")
        {

            $this->validate($request,[
                "cheque_template_name" => "required|unique:cheque_templates,cheque_template_name,".$cheque_template_id.",cheque_template_id,deleted_at,NULL",
                "cheque_width" => "required|numeric|min:0",
                "cheque_height" => "required|numeric|min:0",
                "branch_id" => "required",
            ]);

            $request = clean($request->all());

            if ($cheque_template_id == null)
            {


                // insert
                $new_obj = cheque_templates_m::create($request);
                $cheque_template_id = $new_obj->cheque_template_id;
                $desc_msg = "تم إضافة قالب شيك جديد برقم $cheque_template_id ";

                #region save default items

                $items_arr = [
                    [
                        "cheque_template_id" => $cheque_template_id,
                        "item_name" => "cheque_date"
                    ],
                    [
                        "cheque_template_id" => $cheque_template_id,
                        "item_name" => "receiver_name"
                    ],
                    [
                        "cheque_template_id" => $cheque_template_id,
                        "item_name" => "money_in_numbers"
                    ],
                    [
                        "cheque_template_id" => $cheque_template_id,
                        "item_name" => "money_in_text"
                    ],
                ];
                cheque_template_items_m::insert($items_arr);

                #endregion

            }
            else{

                // update
                $check = cheque_templates_m::find($cheque_template_id)->update($request);
                $desc_msg = "تم تعديل قالب شيك رقم $cheque_template_id";
            }

            $desc_msg .= "يمكنك تعديل العناصر للقالب من هنا ";

            #region save in notification
            $this->send_all_user_type_notifications(
                $not_title = $desc_msg ,
                $not_type = "info" ,
                $user_type = "admin"
            );
            #endregion

            #region save in site_tracking
            $this->track_my_action(
                $action_desc = $desc_msg
            );
            #endregion


            return Redirect::to('admin/cheques/show_cheque_items/'.$cheque_template_id)->with([
                "msg" => "<div class='alert alert-success'> $desc_msg </div>"
            ])->send();

        }

        return view("admin.subviews.cheque_templates.save_template",$this->data);
    }


    public function change_template_default(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/cheque_templates","edit_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $desc_msg = "تم تغيير حالة القالب رقم $item_id";

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

            }

        }

        echo json_encode($output);
    }

    public function change_template_item_status(Request $request)
    {

        $output = array();

        if (!check_permission($this->user_permissions,"admin/cheque_templates","edit_action",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !! </div>";
            echo json_encode($output);
            return;
        }


        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                $return_statues=$obj->update(["$field_name"=>"$accept"]);

                $desc_msg = "تم تغيير حالة ظهور العنصر رقم $item_id";

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

                $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));

            }

        }

        echo json_encode($output);
    }

    public function remove_template(Request $request)
    {

        $output = array();
        $item_id = (int)$request->get("item_id");
        $model_name = $request->get("table_name"); // App\User

        if (!check_permission($this->user_permissions,"admin/cheque_templates","delete_action",$this->data["current_user"]))
        {
            $output["deleted"] = "ليس لديك الصلاحية لفعل هذا الامر";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            $model_name::destroy($item_id);

            $output = array();
            $removed_item = $model_name::find($item_id);
            if (!isset($removed_item)) {
                $output["deleted"] = "yes";

                $desc_msg = "لقد تم مسح قالب شيك رقم #$item_id";

                #region save in notification
                $this->send_all_user_type_notifications(
                    $not_title = $desc_msg ,
                    $not_type = "info" ,
                    $user_type = "admin"
                );
                #endregion


                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = $desc_msg
                );
                #endregion

            }

        }

        echo json_encode($output);

    }


    #region template print


    public function preview_template_items($cheque_template_id)
    {

        if (!check_permission($this->user_permissions,"admin/cheque_templates","save_items_position",$this->data["current_user"]))
        {
            return Redirect::to('admin/cheques')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["template_data"] = "";
        $this->data["template_items"] = [];

        $template_data = cheque_templates_m::find($cheque_template_id);

        if (is_object($template_data))
        {

            $this->data["template_data"] = $template_data;

            // get template items
            $template_items = cheque_template_items_m::where("cheque_template_id",$cheque_template_id)->get();
            $template_items = collect($template_items)->groupBy("item_name");
            $template_items = $template_items->all();
            $this->data["template_items"] = $template_items;

        }
        else{
            return Redirect::to('admin/cheques')->with([
                "msg" => "<div class='alert alert-danger'> هذا القالب غير موجود او لا يحتوي علي عناصر !!! </div>"
            ])->send();
        }


        return view("admin.subviews.cheque_templates.print.save_items_position",$this->data);
    }


    public function save_template_items_position(Request $request)
    {
        $output = [];
        $output["success"] = "";
        $output["error"] = "";

        if (!check_permission($this->user_permissions,"admin/cheque_templates","save_items_position",$this->data["current_user"]))
        {
            $output["error"] = "ليس لديك الصلاحية للدخول لهذة الصفحة !!";
            echo json_encode($output);
            return;
        }

        $items = clean($request->all());

        if (isset($items["cheque_template_id"]))
        {
            $cheque_template_id = intval($items["cheque_template_id"]);
            $template_obj = cheque_templates_m::find($cheque_template_id);
            unset($items["cheque_template_id"]);
        }
        else{
            $output["error"] = "بيانات القالب غير موجودة !!";
            echo json_encode($output);
            return;
        }

        foreach($items as $key => $item)
        {

            $item_obj = cheque_template_items_m::find($key);
            if (is_object($item_obj))
            {
                $item_obj->update([
                    "item_position_left" => $item["left"],
                    "item_position_top" => $item["top"],
                    "item_width" => $item["width"],
                    "item_height" => $item["height"],
                ]);
            }

        }

        #region save in site_tracking
        $this->track_my_action(
            $action_desc = "تعديل قالب شيك رقم $cheque_template_id"
        );
        #endregion

        $output["success"] = "success";
        echo json_encode($output);
        return;
    }


    #endregion

}
