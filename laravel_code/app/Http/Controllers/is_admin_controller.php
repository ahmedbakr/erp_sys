<?php

namespace App\Http\Controllers;

use App\models\admin\category_m;
use App\models\notification_m;
use App\models\product\products_m;
use App\models\site_tracking_m;
use App\models\user_messages_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class is_admin_controller extends dashbaord_controller
{

    public $factory_id;
    public $factory_data;

    public $branch_id;
    public $branch_data;

    public function __construct()
    {
        parent::__construct();
        $this->middleware("check_admin");

        if($this->data["current_user"]->user_type=="admin"){
            $this->factory_id=$this->data["current_user"]->user_id;
        }
        else if($this->data["current_user"]->user_type=="factory_admin") {
            $this->factory_id=$this->data["current_user"]->related_id;
        }
        else if($this->data["current_user"]->user_type=="branch"){
            $this->branch_id=$this->data["current_user"]->user_id;
            $this->branch_data=User::get_users(" AND u.user_id=$this->branch_id");
            $this->branch_data=$this->branch_data[0];

            $this->data["branch_data"]=$this->branch_data;
        }
        else if($this->data["current_user"]->user_type=="branch_admin") {
            $this->branch_id=$this->data["current_user"]->related_id;
            $this->branch_data=User::get_users(" AND u.user_id=$this->branch_id");
            $this->branch_data=$this->branch_data[0];

            $this->data["branch_data"]=$this->branch_data;
        }

        if (isset($this->factory_id) && !empty($this->factory_id) && $this->factory_id > 0)
        {
            $this->factory_data=User::get_users(" AND u.user_id=$this->factory_id");
            $this->factory_data=$this->factory_data[0];
        }

        $this->data["current_notifications"] = notification_m::get_notifications(" where not_to_userid = $this->user_id order by created_at desc limit 10 ");

        $this->data["main_layout"]="admin";
    }


}
