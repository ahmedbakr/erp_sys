<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class logout extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        User::findOrFail($this->user_id)->update([
            "last_login_date" => date("Y-m-d H:i:s")
        ]);

        \Auth::logout();
        return Redirect::to('/logout_page')->with([
            "msg"=>"true"
        ])->send();
    }

    public function logout_page()
    {

//        $make_logout = \Session::get("msg");
//
//        if (!isset($make_logout))
//        {
//            return Redirect::to('/')->with(
//                ["msg"=>"<div class='alert alert-danger'>يمكنك تسجيل الدخول من هنا</div>"]
//            )->send();
//        }

        return view("front.subviews.logout",$this->data);
    }

}
