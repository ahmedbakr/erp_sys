<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;

use App\Http\Requests;
use App\User;

class dashboard extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {


        return view("accountant.subviews.dashboard",$this->data);
    }


}
