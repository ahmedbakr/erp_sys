<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;

use App\models\accountant\entries\entries_m;
use App\models\accountant\entries\entry_consuming_centers_m;
use App\models\accountant\entries\entry_items_m;
use App\models\accountant\entries\entry_template_items_m;
use App\models\accountant\entries\entry_templates_m;
use App\models\accountant\financial_years\financial_year_accounts_m;
use App\models\accountant\financial_years\financial_year_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class entry_templates extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function show_all_templates(Request $request){

        if (!check_permission($this->user_permissions,"accountant/entry_templates","show_action")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["entry_templates"]=entry_templates_m::all();

        return view("accountant.subviews.entry_templates.show",$this->data);
    }

    public function show_template_items($template_id){

        $this->data["template_obj"]=entry_templates_m::findOrFail($template_id);

        $template_items=
            entry_template_items_m::get_template_items(" AND items.entry_template_id=$template_id");


        $this->data["template_items"]=$template_items;


        return view("accountant.subviews.entry_templates.show_entry_template_items",$this->data);
    }


    public function add_template(Request $request){

        if(!check_permission($this->user_permissions,"accountant/entry_templates","edit_action")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        //load add entry components (accounts_tree,consuming_accounts_tree,all_entry_templates)
        $this->show_tree(true,"general","gen_acc_");

        //when i'll call accounts modal before calling assign $categories,$accounts variables by new variables name
        //they will be  $gen_acc_categories,$gen_acc_accounts , $cons_acc_categories,$cons_acc_accounts

        //entry_template
        $this->data["entry_templates"]=entry_templates_m::all();

        if ($request->method()=="POST"){

            $template_obj=entry_templates_m::create([
                "template_name"=>$request->get("template_name")
            ]);

            $entry_template_items=[];

            //get entry_template_items
            //$selected_accounts is refered to number of rows of entry
            $selected_accounts=$request->get("selected_account");
            $credit_cell=$request->get("credit_cell");
            $debit_cell=$request->get("debit_cell");

            foreach($selected_accounts as $acc_key=>$acc_id){

                //check if this row is debit ot credit row
                $row_acc_type="1";
                $row_acc_val=floatval($credit_cell[$acc_key]);

                if(floatval($debit_cell[$acc_key])>0){
                    $row_acc_type="0";
                    $row_acc_val=floatval($debit_cell[$acc_key]);
                }



                $entry_template_items[]=[
                    "entry_template_id"=>"$template_obj->template_id",
                    "t_item_acc_id"=>"$acc_id",
                    "t_item_acc_debit_or_credit"=>"$row_acc_type",
                    "t_item_value_type"=>"0",
                    "t_item_value"=>"$row_acc_val",
                    "created_at"=>Carbon::now(),
                    "updated_at"=>Carbon::now()
                ];
            }


            entry_template_items_m::insert($entry_template_items);

            return Redirect::to('accountant/entry/show_all_templates')->with(
                "msg","<div class='alert alert-success'>تم حفظ النموذج .</div>"
            )->send();



        }


        return view("accountant.subviews.entry_templates.add_template",$this->data);
    }



    public function get_template_items_for_template(Request $request){

        $output=[];

        $template_id=$request->get("template_id");

        $template_items=
            entry_template_items_m::get_template_items(" AND items.entry_template_id=$template_id");
        $data["template_items"]=$template_items;
        $data["en"]=$this->data["en"];


        $output["template_items_html"]=\View::make("accountant.subviews.entry_templates.save_entry_components.entry_item_tr",$data)->render();


        echo json_encode($output);
    }


}
