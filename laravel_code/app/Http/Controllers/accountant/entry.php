<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;

use App\models\accountant\entries\entries_m;
use App\models\accountant\entries\entry_consuming_centers_m;
use App\models\accountant\entries\entry_items_m;
use App\models\accountant\entries\entry_template_items_m;
use App\models\accountant\entries\entry_templates_m;
use App\models\accountant\financial_years\financial_year_accounts_m;
use App\models\accountant\financial_years\financial_year_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class entry extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }


    public function show_entries(Request $request){

        if (!check_permission($this->user_permissions,"accountant/entry","show_action")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $currnt_time=Carbon::now();

        $current_financial_year=financial_year_m::
        where("fin_start_range","<=",date("Y-m-d",strtotime($currnt_time)))->
        where("fin_end_range",">=",date("Y-m-d",strtotime($currnt_time)))->
        get()->first();


        $financial_year_id=$request->get("financial_year",0);

        if($financial_year_id!=0){
            $current_financial_year=financial_year_m::findOrFail($financial_year_id);
        }


        $un_approved_entry=$request->get("un_approved_entry");



        $this->data["all_year_entries"]=
            entries_m::
            where("entry_created_at",">=",date("Y-m-d",strtotime($current_financial_year->fin_start_range)))->
            where("entry_created_at","<=",date("Y-m-d",strtotime($current_financial_year->fin_end_range)));

        if($un_approved_entry=="on"){
            $un_approved_entry=0;
            $this->data["all_year_entries"]=$this->data["all_year_entries"]->where("entry_approved","=",$un_approved_entry);
        }



        $this->data["all_year_entries"]=$this->data["all_year_entries"]->orderBy("entry_created_at")->paginate(100);
        $this->data["all_year_entries"]->appends(Input::except('page'));


        return view("accountant.subviews.entries.show",$this->data);
    }

    public function show_entry_items($entry_id){

        //get entry
        $this->data["entry_obj"]=entries_m::findOrFail($entry_id);

        //get entry_items
        $this->data["entry_items"]=
            entry_items_m::
            join("general_accounts","general_accounts.acc_id","=","entry_items.acc_id")->
            where("entry_id",$entry_id)->get()->all();


        //get entry_consuming_centers
        $this->data["entry_consuming_items"]=
            entry_consuming_centers_m::
            join("general_accounts","general_accounts.acc_id","=","entry_consuming_centers.consuming_acc_id")->
            whereIn("entry_item_id",convert_inside_obj_to_arr($this->data["entry_items"],"item_id"))->get()->all();
        $this->data["entry_consuming_items"]=collect($this->data["entry_consuming_items"])->groupBy("entry_item_id")->all();


        return view("accountant.subviews.entries.show_entry_items",$this->data);
    }

    public function approve_entry(Request $request){

        if (!check_permission($this->user_permissions,"accountant/entry","entry_approve")){
            $output["msg"]="<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>";
            echo json_encode($output);
            return;
        }

        $entry_id=$request->get("entry_id");

        if(!is_array($entry_id)){
            $entry_id=[$entry_id];
//            $output["msg"]="<div class='alert alert-danger'>خطأ في البيانات المرسلة</div>";
//            echo json_encode($output);
//            return;
        }

        $output=[];

        entries_m::whereIn("entry_id",$entry_id)->update([
            "entry_approved"=>"1"
        ]);


        $output["msg"]="<div class='alert alert-success'>تمت الموافقة علي القيد</div>";

        echo json_encode($output);
    }

    public function add_entry_view(Request $request){
        $this->add_entry($request);

        return view("accountant.subviews.entries.add_entry",$this->data);
    }

    public static function static_add_entry(Request $request){
        $obj=new entry();
        return $obj->add_entry($request);
    }

    public function get_template_items(Request $request){

        $output=[];

        $template_id=$request->get("template_id");

        $template_items=
            entry_template_items_m::get_template_items(" AND items.entry_template_id=$template_id");
        $data["template_items"]=$template_items;
        $data["en"]=$this->data["en"];


        $output["template_items_html"]=\View::make("accountant.subviews.entries.save_entry_components.entry_item_tr",$data)->render();


        echo json_encode($output);
    }

    public function add_entry(Request $request){

        if(!check_permission($this->user_permissions,"accountant/entry","add_action")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        //load add entry components (accounts_tree,consuming_accounts_tree,all_entry_templates)
        $this->show_tree(true,"general","gen_acc_");
        $this->show_tree(true,"consuming","cons_acc_");

        //when i'll call accounts modal before calling assign $categories,$accounts variables by new variables name
        //they will be  $gen_acc_categories,$gen_acc_accounts , $cons_acc_categories,$cons_acc_accounts


        //entry_template
        $this->data["entry_templates"]=entry_templates_m::all();


        if ($request->method()=="POST"){

            $entry_date=date("Y-m-d",strtotime($request->get("entry_created_at")));


            //check if this entry_date is in a financial year or not
            $financial_obj=financial_year_m::
            where("fin_start_range","<=",$entry_date)->
            where("fin_end_range",">=",$entry_date)->
            where("fin_closed","0")->
            get()->first();

            if(!is_object($financial_obj)){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا يمكنك ان تختار تاريخ خارج اي سنة مالية مفتوحة عندك .</div>"
                )->send();
            }


            //TODO (forget it now) consuming centers in saving templates

            //check save_template_only_class
            //check save_this_entries_as_new_template

            if($request->get("save_template_only")=="on"||$request->get("save_this_entries_as_new_template")=="on"){

                $template_obj=entry_templates_m::create([
                    "template_name"=>$request->get("template_name")
                ]);

                $entry_template_items=[];

                //get entry_template_items
                //$selected_accounts is refered to number of rows of entry
                $selected_accounts=$request->get("selected_account");
                $credit_cell=$request->get("credit_cell");
                $debit_cell=$request->get("debit_cell");

                foreach($selected_accounts as $acc_key=>$acc_id){

                    //check if this row is debit ot credit row
                    $row_acc_type="1";
                    $row_acc_val=floatval($credit_cell[$acc_key]);

                    if(floatval($debit_cell[$acc_key])>0){
                        $row_acc_type="0";
                        $row_acc_val=floatval($debit_cell[$acc_key]);
                    }



                    $entry_template_items[]=[
                        "entry_template_id"=>"$template_obj->template_id",
                        "t_item_acc_id"=>"$acc_id",
                        "t_item_acc_debit_or_credit"=>"$row_acc_type",
                        "t_item_value_type"=>"0",
                        "t_item_value"=>"$row_acc_val",
                        "created_at"=>Carbon::now(),
                        "updated_at"=>Carbon::now()
                    ];
                }


                entry_template_items_m::insert($entry_template_items);

                if ($request->get("save_template_only")=="on"){
                    return Redirect::to('accountant/dashboard')->with(
                        "msg","<div class='alert alert-success'>تم حفظ النموذج .</div>"
                    )->send();
                }
            }





            //save entry
            //make entry approved

            //TODO (forget it now) doc_id
            $entry_obj=entries_m::create([
                'entry_day_help'=>$request->get("entry_day_help"),
                'entry_type'=>$request->get("entry_type"),
                'entry_desc'=>$request->get("entry_desc"),
                'entry_approved'=>1,
                'doc_id'=>0,
                'entry_created_at'=>date("Y-m-d",strtotime($request->get("entry_created_at")))
            ]);

            //save entry_items
            $entry_items=[];

            //get entry_template_items
            //$selected_accounts is refered to number of rows of entry
            $selected_accounts=$request->get("selected_account");
            $credit_cell=$request->get("credit_cell");
            $debit_cell=$request->get("debit_cell");

            foreach($selected_accounts as $acc_key=>$acc_id){

                //check if this row is debit ot credit row
                $row_acc_type="1";
                $row_acc_val=floatval($credit_cell[$acc_key]);

                if(floatval($debit_cell[$acc_key])>0){
                    $row_acc_type="0";
                    $row_acc_val=floatval($debit_cell[$acc_key]);
                }

                $entry_items[]=entry_items_m::create([
                    "entry_id"=>"$entry_obj->entry_id",
                    "acc_id"=>"$acc_id",
                    "credit_or_debit"=>"$row_acc_type",
                    "item_amount"=>"$row_acc_val",
                    "item_created_at"=>date("Y-m-d",strtotime($request->get("entry_created_at")))
                ]);

            }


            //update accounts values
            foreach($entry_items as $item_key=>$item){
                $this->update_acc_money(
                    $item->acc_id,
                    $acc_operation=(($item->credit_or_debit==0)?"debit":"credit"),
                    $money_amount=$item->item_amount,
                    $financial_obj
                );

                //save entry consuming centers
                $selected_consuming_accounts=$request->get("selected_consuming_accounts_$item_key");
                $value_of_selected_consuming_accounts=$request->get("value_of_selected_consuming_accounts_$item_key");

                if(is_array($selected_consuming_accounts)){
                    foreach($selected_consuming_accounts as $cons_key=>$cons_acc){
                        $this->update_acc_money(
                            $cons_acc,
                            $acc_operation=(($item->credit_or_debit==0)?"debit":"credit"),
                            $money_amount=$value_of_selected_consuming_accounts[$cons_key],
                            $financial_obj
                        );

                        //save row at entry_consuming_center
                        entry_consuming_centers_m::create([
                            'entry_item_id'=>$item->item_id,
                            'consuming_acc_id'=>$cons_acc,
                            'acc_value'=>$value_of_selected_consuming_accounts[$cons_key]
                        ]);

                    }
                }

            }





            //TODO (forget it now) check if this is the first entry for account save this entry as initial entry


        }

    }

    public function update_acc_money($acc_id,$acc_operation,$money_amount,$financial_obj)
    {
        $acc_obj=general_accounts_m::findOrFail($acc_id);

        //get current row in financial year accounts
        $fin_acc_obj=financial_year_accounts_m::
        where("fya_acc_id",$acc_id)->
        where("fya_fin_year_id",$financial_obj->fin_id)->
        get()->first();


        if(!is_object($fin_acc_obj)){

            $fin_acc_obj=financial_year_accounts_m::create([
                "fya_acc_id"=>$acc_id,
                "fya_fin_year_id"=>$financial_obj->fin_id
            ]);
        }


        if($acc_obj->acc_money_type=="debit"){
            if($acc_operation=="debit"){
                //increase
                $fin_acc_obj->update([
                    "fya_acc_value"=>($fin_acc_obj->fya_acc_value+$money_amount)
                ]);
            }
            elseif($acc_operation=="credit"){
                //decrease
                $fin_acc_obj->update([
                    "fya_acc_value"=>($fin_acc_obj->fya_acc_value-$money_amount)
                ]);
            }
        }
        elseif($acc_obj->acc_money_type=="credit"){
            if($acc_operation=="debit"){
                //decrease
                $fin_acc_obj->update([
                    "fya_acc_value"=>($fin_acc_obj->fya_acc_value-$money_amount)
                ]);
            }
            elseif($acc_operation=="credit"){
                //increase
                $fin_acc_obj->update([
                    "fya_acc_value"=>($fin_acc_obj->fya_acc_value+$money_amount)
                ]);
            }
        }

    }


}
