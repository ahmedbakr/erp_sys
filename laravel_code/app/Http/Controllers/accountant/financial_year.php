<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;

use App\models\accountant\entries\entries_m;
use App\models\accountant\entries\entry_items_m;
use App\models\accountant\financial_years\financial_year_accounts_m;
use App\models\accountant\financial_years\financial_year_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class financial_year extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }


    //TODO when i close financial year i must check before copying accounts if account is exsit or not


    public function show_all_financial_years(){

        if (!check_permission($this->user_permissions,"accountant/financial_year","show_action")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["all_financial_years"]=financial_year_m::all();

        return view("accountant.subviews.financial_year.show",$this->data);
    }

    public function save_financial_year(Request $request,$fin_id=null){

        if ($fin_id==null){
            if(!check_permission($this->user_permissions,"accountant/financial_year","add_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }
        else{
            if(!check_permission($this->user_permissions,"accountant/financial_year","edit_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }


        $fin_obj="";
        if ($fin_id!=null){
            $fin_obj=financial_year_m::findOrFail($fin_id);
        }
        $this->data["fin_obj"]=$fin_obj;



        if ($request->method()=="POST"){


            $rules_arr=[
                "fin_label"=>"required",
                "fin_start_range"=>"required",
                "fin_end_range"=>"required",
            ];

            if ($fin_id!=null){
                unset(
                    $rules_arr["fin_start_range"],
                    $rules_arr["fin_end_range"]
                );
            }

            $this->validate($request,$rules_arr);

            if($fin_id==null){

                //check if there is more that 2 open financial years open if true then prevent admin to create new one until he close first one
                $opened_financial_years=financial_year_m::where("fin_closed","0")->get()->count();

                if($opened_financial_years>=2){
                    return Redirect::to('accountant/dashboard')->with(
                        "msg","<div class='alert alert-danger'>لا يمكنك ان تجعل اكثر من 2 سنة مالية مفتوحة . يجب ان تغلق احداهما لكي تتمكن من ان تفتح سنة مالية جديدة</div>"
                    )->send();
                }

                //check if new financial year date range is overlaps with other financial years
                $date_check=(DB::select("
                    select 
                    * from financial_year 
                    where 
                    ('".$request["fin_start_range"]."' between fin_start_range AND fin_end_range) OR
                    ('".$request["fin_end_range"]."' between fin_start_range AND fin_end_range)
                    
                "));

                if(count($date_check)>0){
                    return Redirect::to('accountant/dashboard')->with(
                        "msg","<div class='alert alert-danger'>لايمكنك ان تحدد فترة مالية بتاريخ بداية او نهاية متداخل مع فترة مالية اخري</div>"
                    )->send();
                }

                //add
                $fin_obj=financial_year_m::create($request->all());


                //check if previous year is closed or not to get values of accounts
                $prev_financial_year=financial_year_m::orderBy("fin_id","desc")->get()->all();
                if(isset($prev_financial_year[1])){
                    $prev_financial_year=$prev_financial_year[1];
                }
                else{
                    $prev_financial_year="";
                }

                //because if it is closed then we must get values of transportable accounts
                $accounts_values=[];

                if(is_object($prev_financial_year)&&$prev_financial_year->fin_closed==1){
                    $accounts_values=general_accounts_m::get_general_accounts_of_financial_year(" AND acc.acc_go_away=1 AND fin_year.fin_id=$prev_financial_year->fin_id");
                    $accounts_values=collect($accounts_values)->groupBy("acc_id")->all();
                }


                //make relation between all general_accounts to this new $fin_obj in financial_year_accounts table
                //get all accounts
                $all_accounts=general_accounts_m::all();


                $financial_year_accounts=[];
                foreach ($all_accounts as $acc_key=>$acc_obj){
                    $financial_year_accounts[]=[
                        "fya_fin_year_id"=>$fin_obj->fin_id,
                        "fya_acc_id"=>$acc_obj->acc_id,
                        "fya_acc_value"=>(isset($accounts_values[$acc_obj->acc_id])?$accounts_values[$acc_obj->acc_id]->fya_acc_value:0)
                    ];
                }

                financial_year_accounts_m::insert($financial_year_accounts);

            }
            else{
                //edit
                $fin_obj->update($request->all());
            }



            return Redirect::to('accountant/financial_year/show_all_financial_years')->with(
                "msg","<div class='alert alert-success'>تم اجراء العملية بنجاح</div>"
            )->send();

        }



        return view("accountant.subviews.financial_year.save",$this->data);
    }

    public function close_financial_year(Request $request,$fin_id){

        if(!check_permission($this->user_permissions,"accountant/financial_year","close_financial_year")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $fin_obj=financial_year_m::where("fin_id",$fin_id)->where("fin_closed","0")->get()->first();

        if(!is_object($fin_obj)){
            abort(404);
        }


        $this->data["fin_obj"]=$fin_obj;

        if($request->method()=="POST"){


            //check if there is entries not entry_approved because he can no continue
            $un_approved_entries=entries_m::
                where("entry_approved","0")->
                whereBetween("entry_created_at",[$fin_obj->fin_start_range,$fin_obj->fin_end_range])->
                get();


            if(count($un_approved_entries)>0){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>
    
    لا يمكنك ان تغلق هذه السنة المالية ويوجد عدد
    ".(count($un_approved_entries))."
     قيود غير مرحلة
     يمكنك ان ترحل هذه القيود من شاشة القيود
     ثم يمكنك ان تغلق هذه السنة المالية
    
                        </div>"
                )->send();
            }


            //if there is two open financial years admin can not close second one before first one of course
            $open_years=
                financial_year_m::
                where("fin_closed",0)->
                orderBy("fin_start_range")->
                get();

            if(count($open_years)>=2){
                if($open_years[1]->fin_id==$fin_id){
                    return Redirect::to('accountant/dashboard')->with(
                        "msg","<div class='alert alert-danger'>يجب ان تغلق الفترة المالية الاقدم اولا . قبل ان تغلق الفترة المالية الاحدث</div>"
                    )->send();
                }
            }



            //check if there is a next financial year or not because we can not close this financial year except there is one after
            $next_year=financial_year_m::
            where("fin_start_range",">","$fin_obj->fin_end_range")->
            orderBy("fin_start_range")->
            get()->first();

            if(!is_object($next_year)){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا يمكنك ان تغلق هذه الفترة المالية لانه لا يوجد فترة مالية جديدة بعدها</div>"
                )->send();
            }


            //قيد النهاية او البداية بجيب كل الحسابات بتاعت السنة واروح اجيب مجموع الدائن والمدين بتاعها واحطهم في قيد واحد هو ده قيد نهاية المدة واخد ده كوبس واحطه قيد بداية المدة لبداية العام الجديد

            //make قيد نهاية المدة
            $end_year_entry_obj=entries_m::create([
                "entry_desc"=>"$fin_obj->fin_label"."قيد نهاية المدة للسنة المالية ",
                "entry_approved"=>1,
                "entry_created_at"=>$fin_obj->fin_end_range
            ]);

            $start_year_entry_obj=entries_m::create([
                "entry_desc"=>"$next_year->fin_label"."قيد بداية المدة للسنة المالية ",
                "entry_approved"=>1,
                "entry_created_at"=>$next_year->fin_start_range
            ]);

            $end_year_entry_items=[];
            $start_year_entry_items=[];

            //for $fin_obj
            //get all accounts for this $fin_obj and get its accounts debit,credit values from get_accounts_debit_and_credit_values method

            $all_fin_accounts=financial_year_accounts_m::where("fya_fin_year_id",$fin_obj->fin_id)->get()->all();

            if(isset_and_array($all_fin_accounts)){
                $all_fin_accounts_ids=convert_inside_obj_to_arr($all_fin_accounts,"fya_acc_id");

                $all_accounts=entry_items_m::get_accounts_debit_and_credit_values(
                    $all_fin_accounts_ids,
                    "
                     AND entry.entry_approved=1
                     AND (entry_item.item_created_at>='$fin_obj->fin_start_range' AND  entry_item.item_created_at<='$fin_obj->fin_end_range')"
                );

                $all_accounts=collect($all_accounts)->groupBy("acc_id");


                foreach($all_accounts as $acc_id=>$acc){

                    $acc=$acc->groupBy("credit_or_debit");


                    if(isset($acc[0])&&isset_and_array($acc[0]->all())){
                        $acc[0]=$acc[0]->all()[0];
                        //debit
                        $end_year_entry_items[]=[
                            "entry_id"=>$end_year_entry_obj->entry_id,
                            "acc_id"=>$acc_id,
                            "credit_or_debit"=>"0",
                            "item_amount"=>$acc[0]->item_sum_amount,
                            "item_created_at"=>$end_year_entry_obj->entry_created_at,
                            "created_at"=>Carbon::now()
                        ];
                        $start_year_entry_items[]=[
                            "entry_id"=>$start_year_entry_obj->entry_id,
                            "acc_id"=>$acc_id,
                            "credit_or_debit"=>"0",
                            "item_amount"=>$acc[0]->item_sum_amount,
                            "item_created_at"=>$start_year_entry_obj->entry_created_at,
                            "created_at"=>Carbon::now()
                        ];
                    }
                    else if(isset($acc[1])&&isset_and_array($acc[1]->all())){
                        //credit
                        $acc[1]=$acc[1]->all()[0];

                        $end_year_entry_items[]=[
                            "entry_id"=>$end_year_entry_obj->entry_id,
                            "acc_id"=>$acc_id,
                            "credit_or_debit"=>"1",
                            "item_amount"=>$acc[1]->item_sum_amount,
                            "item_created_at"=>$end_year_entry_obj->entry_created_at,
                            "created_at"=>Carbon::now()
                        ];
                        $start_year_entry_items[]=[
                            "entry_id"=>$start_year_entry_obj->entry_id,
                            "acc_id"=>$acc_id,
                            "credit_or_debit"=>"1",
                            "item_amount"=>$acc[1]->item_sum_amount,
                            "item_created_at"=>$start_year_entry_obj->entry_created_at,
                            "created_at"=>Carbon::now()
                        ];
                    }


                }


                //add these fields as end_year_entry and start_year_entry

                if(isset_and_array($end_year_entry_items)){
                    //make قيد نهاية المدة
                    entry_items_m::insert($end_year_entry_items);
                }

                if(isset_and_array($start_year_entry_items)){
                    //make قيد بداية المدة //for $next_year
                    entry_items_m::insert($start_year_entry_items);
                }



                //copy all account in $fin_obj to $next_year
                $accounts_values=general_accounts_m::get_general_accounts_of_financial_year(
                    " AND acc.acc_go_away=1 AND fin_year.fin_id=$fin_obj->fin_id"
                );
                $accounts_values=collect($accounts_values)->groupBy("acc_id")->all();


                //make relation between all general_accounts to this new $fin_obj in financial_year_accounts table
                //get all accounts
                $all_accounts=general_accounts_m::all();


                $financial_year_accounts=[];

                foreach ($all_accounts as $acc_key=>$acc_obj){

                    $fya_acc_value=0;
                    if(isset($accounts_values[$acc_obj->acc_id])&&isset_and_array($accounts_values[$acc_obj->acc_id]->all())){
                        $fya_acc_value=$accounts_values[$acc_obj->acc_id]->all()[0]->fya_acc_value;
                    }

                    $financial_year_accounts[]=[
                        "fya_fin_year_id"=>$next_year->fin_id,
                        "fya_acc_id"=>$acc_obj->acc_id,
                        "fya_acc_value"=>$fya_acc_value
                    ];
                }

                financial_year_accounts_m::insert($financial_year_accounts);


            }

            //update fin_obj
            $fin_obj->update([
               "fin_closed"=>"1"
            ]);

            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-success'>تم اغلاق السنة المالية بنجاح</div>"
            )->send();

        }


        return view("accountant.subviews.financial_year.close_financial_year",$this->data);
    }


}
