<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\general_accounts\general_account_category_m;
use App\models\accountant\general_accounts\general_accounts_m;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class general_account_category extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }


    public function save_category(Request $request,$cat_type="general",$parent_id,$cat_id=null){

        if ($cat_id==null){
            if(!check_permission($this->user_permissions,"accountant/general_account_category","add_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }
        else{
            if(!check_permission($this->user_permissions,"accountant/general_account_category","edit_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }


        $this->data["selected_cat_type"]=$cat_type;

        $this->data["parent_id"]=$parent_id;

        $cat_obj="";
        if ($cat_id!=null){

            $cat_obj=general_account_category_m::findOrFail($cat_id);
        }
        $this->data["cat_obj"]=$cat_obj;


        if ($request->method()=="POST"){

            $this->validate($request,[
                "cat_name"=>"required",
                "cat_name_en"=>"required",
            ]);


            $request["parent_id"]=$parent_id;
            $request["cat_type"]=$cat_type;


            if($cat_id==null){
                //add
                $cat_obj=general_account_category_m::create($request->all());
            }
            else{
                //edit
                $cat_obj->update($request->all());
            }



            //check if cat_code is empty if true then update cat_code to this obj cat_code
            if(empty($request["cat_code"])){
                $cat_obj->update([
                    "cat_code"=>$cat_obj->cat_id
                ]);
            }


            return Redirect::to('accountant/general_account_category/show_tree/false/'.$cat_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء العملية بنجاح</div>"
            )->send();

        }


        return view("accountant.subviews.general_account_category.save",$this->data);
    }

    public function move_category_to_another_parent(Request $request,$cat_type,$cat_id){

        if(!check_permission($this->user_permissions,"accountant/general_account_category","move_category_to_another_parent")){
            return Redirect::to('admin/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->show_tree(true,$cat_type);

        $this->data["selected_cat_type"]=$cat_type;

        //load all categories from tree

        $cat_data=general_account_category_m::get_cats(" AND cat.cat_id=$cat_id");

        if(isset_and_array($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            abort(404);
        }
        $this->data["cat_data"]=$cat_data;

        if ($request->method()=="POST"){
            $cat_obj=general_account_category_m::findOrFail($cat_id);

            $parent_id=Input::get("parent_id",0);

            //of course he can not make category itself the parent_id
            if($cat_id==$parent_id){
                return Redirect::to('accountant/general_account_category/show_tree')->with(
                    "msg","<div class='alert alert-danger'>
                            <img src='".url("/public_html/img/alert.jpg")."' >
                        </div>"
                )->send();
            }

            //nodes in same tree branch can not move to down
            //example
            //a>b>c>d
            //you can not make d parent of a

            //get all childs of current node and check if $parent_id is one of them
            //if true then this operation must cancelled
            $move_to_child=general_account_category_m::check_in_parent_node_contain_node([$cat_obj->cat_id],$parent_id);

            if ($move_to_child){
                return Redirect::to('accountant/general_account_category/show_tree')->with(
                    "msg","<div class='alert alert-danger'>
                            لا يمكن ان تتم هذه العملية لاسباب منطقية
                        </div>"
                )->send();
            }


            $cat_obj->update([
                "parent_id"=>$parent_id
            ]);


            return Redirect::to('accountant/general_account_category/show_tree/false/'.$cat_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء عملية النقل بنجاح</div>"
            )->send();
        }



        return view("accountant.subviews.general_account_category.move_category_to_another_parent",$this->data);
    }

    public function remove_general_account_category(Request $request){


        if(!check_permission($this->user_permissions,"accountant/general_account_category","delete_action")){
            $output["msg"]="<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>";
            echo json_encode($output);
            return;
        }
        $cat_id = (int)$request->get("item_id");


        //no one can remove category has accounts
        $accounts=general_account_category_m::get_all_accounts_in_branch([$cat_id]);

        if(isset_and_array($accounts)){
            $output["msg"]="<div class='alert alert-warning'>
لا يمكنك ان تمسح تصنيف يحتوي علي حسابات عامة.اذا اردت ان تمسحه يمكنك ان تمسح جميع
 الحسابات الفرعية بداخله اولا او انقل الحسابات الفرعية الي تصنيف اخر .حينئذ يمكنك ان تسمح 
            </div>";
            echo json_encode($output);
            return;
        }


        $this->general_remove_item($request,'App\models\accountant\general_accounts\general_account_category_m');
    }







}
