<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\is_accountant_controller;

use App\models\accountant\general_accounts\general_account_category_m;
use App\models\accountant\general_accounts\general_accounts_m;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class general_accounts extends is_accountant_controller
{

    public function __construct(){
        parent::__construct();
    }


    public function move_account_to_another_category(Request $request,$acc_type="general",$acc_id){

        if(!check_permission($this->user_permissions,"accountant/general_account","move_account_to_another_category")){
            return Redirect::to('accountant/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->data["selected_acc_type"]=$acc_type;

        $this->show_tree(true,$acc_type);

        $acc_data=general_accounts_m::findOrFail($acc_id);

        $this->data["acc_data"]=$acc_data;

        if ($request->method()=="POST"){
            $cat_id=Input::get("cat_id",0);


            $acc_data->update([
                "cat_id"=>$cat_id
            ]);


            return Redirect::to('accountant/general_account_category/show_tree/false/'.$acc_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء عملية النقل بنجاح</div>"
            )->send();
        }


        return view("accountant.subviews.general_account.move_account_to_another_parent",$this->data);
    }

    public function save_general_account(Request $request,$acc_type="general",$cat_id,$acc_id=null){

        if ($cat_id==null){
            if(!check_permission($this->user_permissions,"accountant/general_account","add_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }
        else{
            if(!check_permission($this->user_permissions,"accountant/general_account","edit_action")){
                return Redirect::to('accountant/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }

        $this->data["selected_cat_type"]=$acc_type;



        $this->data["cat_id"]=$cat_id;

        $acc_obj="";
        if ($acc_id!=null){

            $acc_obj=general_accounts_m::findOrFail($acc_id);
        }
        $this->data["acc_obj"]=$acc_obj;



        if ($request->method()=="POST"){

            $request["cat_id"]=$cat_id;
            $request["acc_type"]=$acc_type;


            $rules_arr=[
                "acc_name"=>"required",
                "acc_name_en"=>"required",
                "acc_code"=>"required",
                "acc_money_type"=>"required",
                "acc_go_away"=>"required",
            ];

            if ($acc_id!=null){

                unset(
                    $rules_arr["acc_money_type"],
                    $request["acc_money_type"]
                );
            }

            $this->validate($request,$rules_arr);




            if($acc_id==null){
                //add
                $acc_obj=general_accounts_m::create($request->all());



            }
            else{
                //edit
                $acc_obj->update($request->all());
            }



            //check if cat_code is empty if true then update cat_code to this obj cat_code
            if(empty($request["acc_code"])){
                $acc_obj->update([
                    "acc_code"=>$acc_obj->acc_id
                ]);
            }


            return Redirect::to('accountant/general_account_category/show_tree/false/'.$acc_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء العملية بنجاح</div>"
            )->send();

        }



        return view("accountant.subviews.general_account.save",$this->data);
    }

    public function remove_general_account(){
        //waiting for discussion of logical cases


    }

    public function add_initial_balance_to_account(Request $request,$cat_type,$account_id){

        //TODO get current financial year and add them to financial_year_accounts

        //TODO make it tomorrow

        $this->show_tree(true,$cat_type);


        if ($request->method()=="POST"){



        }


        return view("accountant.subviews.general_account.add_initial_balance_to_account",$this->data);
    }





}
