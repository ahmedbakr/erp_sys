<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\assets\assets_transports_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_transports extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/assets_transports","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_transports_m::get_data(" $cond " , false);
            }
        }

        #endregion

        return view("accountant.subviews.assets.assets_transports.index",$this->data);
    }


    public function save(Request $request , $asset_transport_id = null)
    {

        $default_action = "add_action";
        if ($asset_transport_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/assets_transports","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["transports"] = "";
        $asset_id_required = "required|";

        if ($asset_transport_id != null)
        {
            $get_transports = assets_transports_m::get_data(" 
                AND transport.asset_transport_id = $asset_transport_id
            ", true);
            if (!is_object($get_transports))
            {
                return Redirect::to('accountant/assets/assets_transports')->with([
                    "msg" => "<div class='alert alert-danger'> لا يمكنك التعديل لعدم وجود البيانات الخاصة به !!  </div>"
                ])->send();
            }
            $this->data["transports"] = $get_transports;
            $asset_id_required = "";


        }

        $this->data["all_locations"] = asset_locations_m::all();

        if (count($this->data["all_locations"]) == 0)
        {
            return Redirect::to('accountant/assets/locations/save/')->with([
                "msg" => "<div class='alert alert-danger'>  لا توجد بيانات مواقع للأصول اضف من هنا  </div>"
            ])->send();
        }

        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/assets_transports')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لعمل تنقل لها الأن !!  </div>"
            ])->send();
        }


        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => $asset_id_required."numeric|min:0",
                    "asset_transport_location_to" => "required|numeric|min:1",
                    "asset_transport_type" => "required",
                    "asset_transport_date" => "required|date",
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "asset_transport_location_to.required" => "مدة الصيانة مطلوب إدخالة",
                    "asset_transport_type.required" => "نوع النقل مطلوب إدخالة",
                    "asset_transport_date.required" => "تاريخ النقل مطلوب إدخالة",
                ]

            );

            $request = clean($request->all());
            if (isset($request["asset_id"]) && $asset_transport_id == null)
            {
                $asset_id = $request["asset_id"];
            }
            else{
                $asset_id = $get_transports->asset_id;
            }

            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);
            $request["asset_transport_location_from"] = $check_asset->asset_location_id;


            if ($asset_transport_id == null)
            {
                // insert

                if ($request["asset_transport_location_from"] == $request["asset_transport_location_to"])
                {
                    return Redirect::to('accountant/assets/assets_transports/save')->with([
                        "msg" => "<div class='alert alert-danger'> لا يمكن نقل الأصل لنفس الموقع !! </div>"
                    ])->send();
                }

                // check if asset not sold

                if (is_object($check_asset) && $check_asset->asset_is_sold == 0)
                {

                    assets_transports_m::create($request);

                    assets_m::findOrFail($asset_id)->update([
                        "asset_location_id" => $request["asset_transport_location_to"]
                    ]);

                    $msg = "لقد تم نقل الأصل بنجاح...";

                }
                else{
                    return Redirect::to('accountant/assets/assets_transports/save')->with([
                        "msg" => "<div class='alert alert-danger'> الأصل غير صالح لادخال عملية نقل ليه الان قد يكون تم بيعه !! </div>"
                    ])->send();
                }
            }
            else{
                // edit

                $request["asset_id"] = $asset_id;


                // check if there is not from related to new to you edited
                if ($request["asset_transport_location_to"] != $get_transports->asset_transport_location_to)
                {
                    $check_data = assets_transports_m::
                    where("asset_transport_location_from",$get_transports->asset_transport_location_to)
                        ->where("asset_id",$asset_id)->get();

                    $check_data = $check_data->all();
                    if (is_array($check_data) && count($check_data))
                    {
                        return Redirect::to('accountant/assets/assets_transports?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                            "msg" => "<div class='alert alert-danger'> لا يمكن تعديل عملية النقل نظرا لوجود تنقلات اخري تبدأ بها </div>"
                        ])->send();

                    }

                    unset(
                        $request["asset_transport_location_from"]
                    );

                    assets_m::findOrFail($asset_id)->update([
                        "asset_location_id" => $request["asset_transport_location_to"]
                    ]);

                }
                else{

                    unset(
                        $request["asset_transport_location_from"],
                        $request["asset_transport_location_to"]
                    );

                }


                assets_transports_m::findOrFail($asset_transport_id)->update($request);

                $msg = "لقد تم تعديل بيانات نقل الأصل بنجاح...";

            }

            return Redirect::to('accountant/assets/assets_transports?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> $msg </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.assets_transports.save",$this->data);
    }

}
