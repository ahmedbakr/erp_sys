<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class locations extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/locations","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_locations"] = asset_locations_m::all();

        return view("accountant.subviews.assets.locations.index",$this->data);
    }

    public function save(Request $request, $asset_location_id = null)
    {

        $default_action = "add_action";
        if ($asset_location_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/locations","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["location_data"] = "";
        if ($asset_location_id != null)
        {
            $this->data["location_data"] = asset_locations_m::findOrFail($asset_location_id);
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[

                "asset_location_code" => "required|unique:asset_locations,asset_location_code,".$asset_location_id.",asset_location_id,deleted_at,NULL",
                "asset_location_name" => "required"

            ],
                [
                    "asset_location_code.required" => "الكود مطلوب إدخالة",
                    "asset_location_name.required" => "الاسم العربي مطلوب إدخالة",
                    "asset_location_code.unique" => "الكود موجود مسبقا برجاء تغييرة",
                ]
            );

            $request = clean($request->all());

            if ($asset_location_id == null)
            {
                // insert
                $new_obj = asset_locations_m::create($request);
                $msg = "تم التسجيل بنجاح";

                $asset_location_id = $new_obj->asset_location_id;

            }
            else{
                // edit
                asset_locations_m::findOrFail($asset_location_id)->update($request);
                $msg = "تم التعديل بنجاح";
            }

            return Redirect::to('accountant/assets/locations/save/'.$asset_location_id)->with([
                "msg"=>"<div class='alert alert-success'>$msg</div>"
            ])->send();

        }

        return view("accountant.subviews.assets.locations.save",$this->data);
    }

    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/locations","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this cat not own to any assets
        $item_id = (int)$request->get("item_id");

        $get_assets = assets_m::
                get_assets(" and asset.asset_location_id = $item_id ",false);

        if (is_array($get_assets) && count($get_assets))
        {
            echo json_encode(
                [
                    "msg"=>"<div class='alert alert-danger'>لا يمكن المسح لانه توجد أصول تابعه له</div>"
                ]
            );
            return;
        }

        $this->general_remove_item($request,'App\models\accountant\assets\asset_locations_m');

    }
}
