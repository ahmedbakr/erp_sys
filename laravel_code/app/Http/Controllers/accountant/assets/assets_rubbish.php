<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\assets\assets_rubbish_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_rubbish extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/assets_rubbish","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_rubbish_m::get_data(" $cond " , false);
            }
        }

        #endregion


        return view("accountant.subviews.assets.assets_rubbish.index",$this->data);
    }


    public function save(Request $request)
    {

        if(!$this->check_user_permission("assets/assets_rubbish","add_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
                AND asset.asset_is_rubbish = 0 
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/assets_sold')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لتكهينها الأن !!  </div>"
            ])->send();
        }

        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "asset_rubbish_date" => "required|date",
                    "asset_rubbish_amount" => "required|numeric|min:0"
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "asset_rubbish_date.required" => "تاريخ البيع مطلوب إدخالة",
                    "asset_rubbish_amount.required" => "المبلغ مطلوب إدخالة",
                ]

            );

            $request = clean($request->all());
            $asset_id = $request["asset_id"];

            // check if asset not rubbish
            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);
            if (is_object($check_asset) && $check_asset->asset_is_rubbish == 0 && $check_asset->asset_is_sold == 0)
            {

                assets_rubbish_m::create($request);

                assets_m::findOrFail($asset_id)->update([
                    "asset_is_rubbish" => 1
                ]);

                return Redirect::to('accountant/assets/assets_rubbish?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                    "msg" => "<div class='alert alert-success'> لقد تم تكهين الأصل بنجاح... </div>"
                ])->send();

            }
            else{
                return Redirect::to('accountant/assets/assets_rubbish/save')->with([
                    "msg" => "<div class='alert alert-danger'> الأصل قد يكون مكهن مسبقا او غير موجود !! </div>"
                ])->send();
            }

        }

        return view("accountant.subviews.assets.assets_rubbish.save",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/assets_rubbish","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        $model_name = $request->get("table_name"); // App\User

        $obj = $model_name::find($item_id);


        assets_m::findOrFail($obj->asset_id)->update([
            "asset_is_rubbish" => 0
        ]);


        $this->general_remove_item($request,'App\models\accountant\assets\assets_rubbish_m');

    }

}
