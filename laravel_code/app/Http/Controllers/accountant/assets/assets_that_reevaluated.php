<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\assets\assets_that_reevaluated_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_that_reevaluated extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/assets_that_reevaluated","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_that_reevaluated_m::get_data(" $cond " , false);
            }
        }

        #endregion

        return view("accountant.subviews.assets.assets_that_reevaluated.index",$this->data);

    }


    public function save(Request $request)
    {


        if(!$this->check_user_permission("assets/assets_that_reevaluated","add_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
                AND asset.asset_deprecation_type = 'reevaluation'
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/assets_that_reevaluated')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول الأن !!  </div>"
            ])->send();
        }


        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "value_before_change" => "required|numeric|min:0",
                    "change_value" => "required|numeric|min:0",
                    "change_date" => "required|date",
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "value_before_change.required" => "القيمة قبل التغيير مطلوب إدخالة",
                    "change_value.required" => "قيمة التغيير مطلوب إدخالة",
                    "change_date.required" => "التاريخ مطلوب إدخالة",
                ]

            );

            $request = clean($request->all());
            $asset_id = $request["asset_id"];

            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);

            // check if asset not sold
            if (is_object($check_asset) && $check_asset->asset_is_sold == 0 && $check_asset->asset_deprecation_type == 'reevaluation')
            {


                // check if there is another reevaluate with date after the comming date not accept it
                $check_data = assets_that_reevaluated_m::where("asset_id",$asset_id)
                    ->where("change_date",">",$request["change_date"])
                    ->orWhere("change_date","=",$request["change_date"])->get();
                $check_data = $check_data->all();
                if(count($check_data))
                {
                    return Redirect::to('accountant/assets/assets_that_reevaluated/save')->with([
                        "msg" => "<div class='alert alert-danger'> لا يمكن اتمام العملية لوجود عملية تقييم بعد هذا التاريخ مسجله مسبقا </div>"
                    ])->send();
                }

                $request["value_after_change"] = ($request["value_before_change"] - $request["change_value"]);

                assets_that_reevaluated_m::create($request);

                $msg = "لقد تم الإدخال بنجاح...";

                // TODO make entry here or ask for this before do it

            }
            else{
                return Redirect::to('accountant/assets/assets_that_reevaluated/save')->with([
                    "msg" => "<div class='alert alert-danger'> الأصل غير صالح الان قد يكون تم بيعه !! </div>"
                ])->send();
            }

            return Redirect::to('accountant/assets/assets_that_reevaluated?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> $msg </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.assets_that_reevaluated.save",$this->data);
    }

}
