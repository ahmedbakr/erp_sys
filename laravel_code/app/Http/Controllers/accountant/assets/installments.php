<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_installment_m;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class installments extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/installment","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["installments"] = [];

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["installments"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");


            if (!empty($cond))
            {
                $this->data["installments"] = asset_installment_m::get_data(" $cond " , false);
            }
        }

        #endregion


        return view("accountant.subviews.assets.installments.index",$this->data);
    }




    // حفظ الاقساط المحددة
    public function save(Request $request , $asset_install_id = null)
    {

        $default_action = "add_action";
        if ($asset_install_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/installment","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["installment_data"] = "";
        $this->data["all_assets"] = "";


        if ($asset_install_id != null)
        {

            $installment_data = asset_installment_m::get_data(" and installment.asset_install_id = $asset_install_id ",true);
            if (is_object($installment_data))
            {
                if ($installment_data->asset_is_sold)
                {
                    return Redirect::to('accountant/assets/installments')->with([
                        "msg" => "<div class='alert alert-danger'>  لقد تم بيع هذا الأصل  </div>"
                    ])->send();
                }
                if ($installment_data->asset_install_payed_value > 0)
                {
                    return Redirect::to('accountant/assets/installments')->with([
                        "msg" => "<div class='alert alert-danger'>  لا يمكن تعديل هذا القسط !!  </div>"
                    ])->send();
                }
                $this->data["installment_data"] = $installment_data;
            }
            else{
                return Redirect::to('accountant/assets/installments')->with([
                    "msg" => "<div class='alert alert-danger'>  بيانات القسط غير موجوده !!  </div>"
                ])->send();
            }

        }


        $all_assets = assets_m::get_assets(" AND asset.asset_is_sold = 0 ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/installments')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لادخال او تعديل أقساط لها !!  </div>"
            ])->send();
        }



        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "asset_install_type" => "required",
                    "asset_install_date" => "required|date",
                    "asset_install_value" => "required|numeric|min:0"
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "asset_install_type.required" => "نوع القسط مطلوب إدخالة",
                    "asset_install_date.required" => "تاريخ الاستحقاق مطلوب إدخالة",
                    "asset_install_value.required" => "قيمة القسط مطلوب إدخالة",
                ]

            );


            $request = clean($request->all());

            $request["asset_install_total_value"] = (
                ($request["asset_install_value"] + $request["asset_install_additions"])
                -
                $request["asset_install_discount"]
            );

            $request["asset_install_remained"] = $request["asset_install_total_value"];

            $asset_id = $request["asset_id"];
            $asset_install_date = $request["asset_install_date"];
            $asset_install_type = $request["asset_install_type"];

            if ($asset_install_id == null)
            {

                // check if installment with same asset , type and date not accept it
                $check = asset_installment_m::get_data(
                    " AND installment.asset_id = $asset_id 
                                    AND installment.asset_install_date = '$asset_install_date'
                                    AND installment.asset_install_type = '$asset_install_type'  "
                ,true);

                if (is_object($check))
                {
                    return Redirect::to('accountant/assets/installments/save')->with([
                        "msg"=>"<div class='alert alert-danger'>لا يمكن تكرار القسط لنفس الأصل بنفس التاريخ !!</div>"
                    ])->send();
                }

                // save
                $new_obj = asset_installment_m::create($request);
                $asset_install_id = $new_obj->asset_install_id;
                $msg = "تم إضافة القسط بنجاح";

            }
            else{

                // check if installment with same asset , type and date not accept it
                $check = asset_installment_m::get_data(
                    " AND installment.asset_id = $asset_id 
                                    AND installment.asset_install_date = '$asset_install_date'
                                    AND installment.asset_install_type = '$asset_install_type'
                                    AND installment.asset_install_id <> $asset_install_id "
                    ,true);

                if (is_object($check))
                {
                    return Redirect::to('accountant/assets/installments/save')->with([
                        "msg"=>"<div class='alert alert-danger'>لا يمكن تكرار القسط لنفس الأصل بنفس التاريخ !!</div>"
                    ])->send();
                }

                if ($installment_data->asset_install_payed_value == 0)
                {
                    // update
                    asset_installment_m::findOrFail($asset_install_id)->update($request);
                    $msg = "تم تعديل بيانات القسط بنجاح";
                }

            }

            return Redirect::to('accountant/assets/installments/save/'.$asset_install_id)->with([
                "msg"=>"<div class='alert alert-success'>$msg</div>"
            ])->send();

        }

        return view("accountant.subviews.assets.installments.save",$this->data);
    }



    // حفظ الاقساط الدورية
    public function save_generated_installments(Request $request)
    {

        if(!$this->check_user_permission("assets/installment","add_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_assets"] = "";

        $all_assets = assets_m::get_assets(" AND asset.asset_is_sold = 0 ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/installments')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لادخال او تعديل أقساط لها !!  </div>"
            ])->send();
        }


        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "asset_install_type" => "required",
                    "custom_installment_value" => "required|numeric|min:0",
                    "custom_installment_numbers" => "required|numeric|min:1",
                    "custom_installment_first_date" => "required|date",
                    "custom_next_installment_days" => "required|numeric|min:1",
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "asset_install_type.required" => "نوع القسط مطلوب إدخالة",
                    "custom_installment_value.required" => "قيمة القسط مطلوب إدخالة",
                    "custom_installment_numbers.required" => "عدد الأقساط مطلوب إدخالة",
                    "custom_installment_first_date.required" => "تاريخ أول قسط مطلوب إدخالة",
                    "custom_next_installment_days.required" => "عدد أيام القسط التالي مطلوب إدخالة",
                ]
            );

            $request = clean($request->all());

            // generate installments
            $inputs = [];
            $target_date = $request["custom_installment_first_date"];
            $asset_id = $request["asset_id"];
            $asset_install_type = $request["asset_install_type"];

            for($ind = 0;$ind<intval($request["custom_installment_numbers"]);$ind++)
            {

                if ($ind > 0)
                {
                    $target_date = Carbon::createFromTimestamp(strtotime($target_date))->addDays(intval($request["custom_next_installment_days"]));
                    $target_date = $target_date->toDateString();
                }


                // check if installment with same asset , type and date not accept it
                $check = asset_installment_m::get_data(
                    " AND installment.asset_id = $asset_id 
                                    AND installment.asset_install_date = '$target_date'
                                    AND installment.asset_install_type = '$asset_install_type'  "
                    ,true);

                if (is_object($check))
                {
                    continue;
                }

                $request["asset_install_total_value"] = (
                    ($request["custom_installment_value"] + $request["asset_install_additions"])
                    -
                    $request["asset_install_discount"]
                );

                $inputs[$ind] = [
                    "asset_id" => $asset_id,
                    "asset_install_type" => $asset_install_type,
                    "asset_install_desc" => $request["asset_install_desc"],
                    "asset_install_date" => $target_date,
                    "asset_install_value" => $request["custom_installment_value"],
                    "asset_install_additions" => $request["asset_install_additions"],
                    "asset_install_discount" => $request["asset_install_discount"],
                    "asset_install_allowed_days" => $request["asset_install_allowed_days"],
                    "asset_install_delay_fine" => $request["asset_install_delay_fine"],
                    "asset_install_total_value" => $request["asset_install_total_value"],
                    "asset_install_remained" => $request["asset_install_total_value"],
                ];

            }

            if (is_array($inputs) && count($inputs))
            {

                asset_installment_m::insert($inputs);
                $get_asset = assets_m::findOrFail($asset_id);
                return Redirect::to('accountant/assets/installments?asset_code='.$get_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                    "msg" => "<div class='alert alert-success'> لقد تم إنشاء الأقساط الدورية لهذا الأصل بنجاح  </div>"
                ])->send();

            }
            else{
                return Redirect::to('accountant/assets/installments/save_generated_installments')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات غير صالحه او مكررة للأصل !!  </div>"
                ])->send();
            }

        }


        return view("accountant.subviews.assets.installments.save_generated_installments",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/installment","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        $model_name = $request->get("table_name"); // App\User

        $obj = $model_name::find($item_id);

        if ($obj->asset_install_payed_value == 0)
        {
            $this->general_remove_item($request,'App\models\accountant\assets\asset_installment_m');
        }
        else{
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح بمسح هذا القسط نظرا لوجود قيود خاص به</div>"]);
            return;
        }


    }


    public function pay_get_installment(Request $request , $asset_install_id)
    {

        if(!$this->check_user_permission("assets/installment","pay_get_installment")){
            return Redirect::to('accountant/assets/installments')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $get_installment = asset_installment_m::
        get_data(" AND installment.asset_install_id = $asset_install_id " , true);
        if (!is_object($get_installment))
        {
            return Redirect::to('accountant/assets/installments')->with([
                "msg" => "<div class='alert alert-danger'> بيانات القسط غير موجودة  </div>"
            ])->send();
        }

        if ($get_installment->asset_install_remained == 0)
        {
            return Redirect::to('accountant/assets/installments')->with([
                "msg" => "<div class='alert alert-danger'> لقد تم سداد هذا القسط مسبقا !!  </div>"
            ])->send();
        }

        $this->data["asset_install_id"] = $asset_install_id;

        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "target_date" => "required|date",
                    "target_amount" => "required|numeric|min:0",
                ],
                [
                    "target_date.required" => "تاريخ الاستحقاق مطلوب إدخالة",
                    "target_amount.required" => "قيمة القسط مطلوب إدخالة",
                ]
            );

            $request = clean($request->all());

            $target_date = $request["target_date"];
            $target_amount = $request["target_amount"];

            if ($get_installment->asset_install_allowed_days > 0)
            {
                $target_date_adding_allowed_days = Carbon::
                createFromTimestamp(strtotime($get_installment->asset_install_date))->addDays($get_installment->asset_install_allowed_days)->toDateString();
            }
            else{
                $target_date_adding_allowed_days = $get_installment->asset_install_date;
            }


            if ($target_date > $target_date_adding_allowed_days && $get_installment->asset_install_delay_fine > 0)
            {

                // اول فلوس تدفع للقسط ده
                if ($get_installment->asset_install_payed_value==0)
                {
                    $new_delay_fine_value = ($get_installment->asset_install_total_value * $get_installment->asset_install_delay_fine)/100;

                    asset_installment_m::findOrFail($get_installment->asset_install_id)->update([
                        "asset_install_additions" => ($get_installment->asset_install_additions + $new_delay_fine_value),
                        "asset_install_total_value" => ($get_installment->asset_install_total_value + $new_delay_fine_value),
                        "asset_install_remained" => ($get_installment->asset_install_remained + $new_delay_fine_value),
                    ]);
                    $get_installment = asset_installment_m::
                    get_data(" AND installment.asset_install_id = $asset_install_id " , true);
                }

            }


            if ($target_amount > $get_installment->asset_install_remained)
            {
                $target_amount = $get_installment->asset_install_remained;
            }

            asset_installment_m::findOrFail($get_installment->asset_install_id)->update([
                "asset_install_payed_value" => ($get_installment->asset_install_payed_value + $target_amount),
                "asset_install_payed_date" => ($target_date),
                "asset_install_remained" => ($get_installment->asset_install_remained - $target_amount)
            ]);

            // TODO make entry here

            return Redirect::to('accountant/assets/installments?asset_code='.$get_installment->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> تم اتمام العملية بنجاح  </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.installments.pay_get_installment",$this->data);
    }

}
