<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_category_m;
use App\models\accountant\assets\asset_expenses_and_incomes_m;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\asset_status_m;
use App\models\accountant\assets\assets_deprecation_rules_m;
use App\models\accountant\assets\assets_m;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets extends is_accountant_controller
{

    public function __construct()
    {
        parent::__construct();

        $this->data["deprecation_types"] = [
            "deprecation_rate" => "معدل الإهلاك",
            "fixed_value" => "القسط الثابت",
            "decreased_value" => "القسط المتناقص",
            "syd_function" => "مجموع ارقام السنوات",
            "depend_on_production" => "حسب الإنتاجية / وحدات الانتاج",
            "reevaluation" => "إعادة التقدير",
        ];

    }

    public function index()
    {

        if(!$this->check_user_permission("assets","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");


            if (!empty($cond))
            {
                $this->data["assets"] = assets_m::get_assets(" $cond " , false);
            }
        }

        #endregion



        return view("accountant.subviews.assets.assets.show_assets",$this->data);
    }

    public function save(Request $request , $asset_id = null)
    {

        $default_action = "add_action";
        if ($asset_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["parent_cats"] = asset_category_m::
            get_cats(" and cat.parent_id = 0 ",false);

        $this->data["child_cats"] = asset_category_m::
            get_cats(" and cat.parent_id > 0 ",false);

        if (count($this->data["child_cats"]) == 0 || count($this->data["child_cats"]) == 0)
        {
            return Redirect::to('accountant/assets/category/save/')->with([
                "msg" => "<div class='alert alert-danger'>  لا توجد فئات او مجموعات اضف من هنا  </div>"
            ])->send();
        }

        $this->data["all_status"] = asset_status_m::all();
        $this->data["asset_expenses"] = [];
        $this->data["asset_deprecation_rules"] = [];

        $this->show_tree();

        if (count($this->data["all_status"]) == 0)
        {
            return Redirect::to('accountant/assets/status/save/')->with([
                "msg" => "<div class='alert alert-danger'>  لا توجد حالات للأصول اضف من هنا  </div>"
            ])->send();
        }

        $this->data["all_locations"] = asset_locations_m::all();

        if (count($this->data["all_locations"]) == 0)
        {
            return Redirect::to('accountant/assets/locations/save/')->with([
                "msg" => "<div class='alert alert-danger'>  لا توجد بيانات مواقع للأصول اضف من هنا  </div>"
            ])->send();
        }

        $this->data["disable_fields"] = "required";
        $validation_required = "required|";
        $this->data["asset_data"] = "";
        $asset_img_id = 0;
        if ($asset_id != null)
        {
            $asset_data = assets_m::get_assets("
                AND asset.asset_id = $asset_id
            ",true);

            if (!is_object($asset_data))
            {
                return Redirect::to('accountant/assets/save')->with([
                    "msg" => "<div class='alert alert-danger'> بيانات الاصل غير موجودة  </div>"
                ])->send();
            }

            $asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset_id)->get();
            $this->data["asset_deprecation_rules"] = $asset_deprecation_rules->all();

            $asset_expenses = asset_expenses_and_incomes_m::where("asset_id",$asset_id)->get();
            $this->data["asset_expenses"] = $asset_expenses->all();

            $validation_required = "";
            $this->data["disable_fields"] = "disabled";
            $this->data["asset_data"] = $asset_data;
            $asset_img_id = $asset_data->asset_img_id;
        }

        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "asset_code" => "required|unique:assets,asset_code,".$asset_id.",asset_id,deleted_at,NULL",
                    "asset_cat_id" => "required",
                    "asset_location_id" => "required",
                    "asset_status_id" => "required",
                    "asset_title" => "required",
                    "asset_working_units" => $validation_required."numeric|min:0",
                    "asset_age" => $validation_required."numeric|min:0",
                    "asset_buy_date" => $validation_required."date",
                    "asset_buy_amount" => $validation_required."numeric|min:0",
                    "asset_expected_sell_amount" => $validation_required."numeric|min:0",
                    "asset_deprecation_type" => "$validation_required",
//                    "asset_acc" => "$validation_required",
//                    "asset_deprecation_acc" => "$validation_required",

                ],
                [
                    "asset_code.required" => "الكود مطلوب إدخالة",
                    "asset_title.required" => "الاسم العربي مطلوب إدخالة",
                    "asset_code.unique" => "الكود موجود مسبقا برجاء تغييرة",
                    "asset_cat_id.required" => "إختار مجموعه للأصل",
                    "asset_location_id.required" => "إختار الموقع للأصل",
                    "asset_status_id.required" => "إختار الحالة للأصل",
                    "asset_working_units.required" => "أدخل وحدات/ساعات تشغيل للأصل",
                    "asset_age.required" => "أدخل عمر الأصل",
                    "asset_buy_date.required" => "أدخل تاريخ شراء الأصل",
                    "asset_buy_amount.required" => "أدخل قيمة شراء الأصل",
                    "asset_expected_sell_amount.required" => "أدخل القيمة التخريدية للأصل",
                    "asset_deprecation_type.required" => "إختار طريقة الاهلاك للأصل",
//                    "asset_acc.required" => "أدخل حساب الأصل",
//                    "asset_deprecation_acc.required" => "إختار حساب مجمع الإهلاك للأصل",
                ]
            );



//            dump($request);
            $asset_img_id = $this->general_save_img($request , $item_id=$asset_id,
                "asset_img_file",
                $new_title = "", $new_alt = "",
                $upload_new_img_check = $request->get("asset_img_checkbox"), $upload_file_path = "/assets/",
                $width = 0, $height = 0, $photo_id_for_edit = $asset_img_id);

            $request = clean($request->all());
            $request["asset_img_id"] = $asset_img_id;

            unset($request["asset_total_expenses"]);

            if ($asset_id == null)
            {

                // make insert
                $new_asset = assets_m::create($request);

                $asset_id = $new_asset->asset_id;

                $msg = "تم إضافة الاصل بنجاح";


                // if deprecation_types >> depend_on_production
//                if ($request["asset_deprecation_type"] == "depend_on_production")
//                {

                    // generate years on all types of asset_deprecation_type

                    $arr_dates = get_years_in_range(date($request["asset_buy_date"]),intval($request["asset_age"]));
                    if (count($arr_dates))
                    {

                        $assets_deprecation_rules = [];
                        foreach($arr_dates as $key => $arr_date)
                        {

                            $num_of_months = Carbon::createFromTimestamp(strtotime($arr_date["to"]))->diffInMonths(Carbon::createFromTimestamp(strtotime($arr_date["from"])));

                            $assets_deprecation_rules[$key] = [
                                "asset_id" => $asset_id,
                                "asset_rule_from_date" => $arr_date["from"],
                                "asset_rule_to_date" => $arr_date["to"],
                                "asset_deprecation_months" => ($num_of_months+1),
                                "real_working_units" => isset($request["real_working_units"][$key-1])?$request["real_working_units"][$key-1]:0,
                            ];

                        }

                        assets_deprecation_rules_m::insert($assets_deprecation_rules);

                    }

//                }


                // add expenses if exist
                if (is_array($request["asset_exp_value"]) && count($request["asset_exp_value"]))
                {

                    $expenses_values = $request["asset_exp_value"];
                    $expenses_inputs = [];
                    $total_expenses = 0;
                    foreach($expenses_values as $key => $expenses_value)
                    {

                        if (isset($request["asset_exp_desc"][$key]) &&
                            isset($request["receipt_number"][$key]) &&
                            isset($request["asset_exp_date"][$key]) &&
                            isset($request["payment_method"][$key])
                        )
                        {
                            $total_expenses += $expenses_value;
                            $expenses_inputs[$key] = [
                                "asset_id" => $asset_id,
                                "asset_exp_value" => $expenses_value,
                                "asset_exp_desc" => $request["asset_exp_desc"][$key],
                                "receipt_number" => $request["receipt_number"][$key],
                                "asset_exp_date" => $request["asset_exp_date"][$key],
                                "expense_or_income" => "expense",
                                "payment_method" => $request["payment_method"][$key],
                            ];
                        }

                    }


                    if (count($expenses_inputs))
                    {
                        asset_expenses_and_incomes_m::insert($expenses_inputs);
                        assets_m::findOrFail($asset_id)->update([
                            "asset_total_expenses" => $total_expenses
                        ]);
                    }


                }


                // TODO redirect to page to enter entry data

            }
            else{

                // make update
                unset(
                    $request["asset_buy_date"],$request["asset_buy_amount"],
                    $request["asset_expected_sell_amount"],$request["asset_all_deprecation_values"],
                    $request["asset_working_units"],$request["asset_deprecation_type"],
                    $request["asset_acc"],$request["asset_deprecation_acc"]
                );

                assets_m::findOrFail($asset_id)->update($request);
                $msg = "تم تعديل بيانات الاصل بنجاح";


                // edit asset_deprecation_rules
                if($asset_data->asset_deprecation_type == "depend_on_production")
                {

                    $get_old_rules = assets_deprecation_rules_m::where("asset_id",$asset_id)->get();
                    $get_old_rules = $get_old_rules->all();

                    if (count($get_old_rules) && count($request["real_working_units"]))
                    {

                        foreach($get_old_rules as $key => $rule)
                        {

                            if ($rule->is_closed_to_edit == 1)
                            {
                                continue;
                            }

                            if (isset($request["real_working_units"][$key]))
                            {
                                assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update([
                                        "real_working_units" =>$request["real_working_units"][$key]
                                ]);
                            }

                        }

                    }

                }


            }

            return Redirect::to('accountant/assets/save/'.$asset_id)->with([
                "msg"=>"<div class='alert alert-success'>$msg</div>"
            ])->send();

        }


        return view("accountant.subviews.assets.assets.save_asset",$this->data);
    }


    public function get_years_in_range(Request $request)
    {

        $output = [];
        $output["data"] = "";

        $asset_age = clean($request->get("asset_age"));
        $asset_buy_date = clean($request->get("asset_buy_date"));

        $arr_dates = get_years_in_range(date("$asset_buy_date"),intval($asset_age));

        $output["data"] = $arr_dates;

        echo json_encode($output);
        return;
    }



    // TODO on counting deprecation if type of asset == depend_on_production dont forget to close year of counting deprecation

    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        echo json_encode(
            [
                "msg"=>"<div class='alert alert-danger'>لا يمكن مسح الأصول الان</div>"
            ]
        );
        return;


        // check if this asset not own any old deprecation or not sold
        $item_id = (int)$request->get("item_id");

        $get_asset = assets_m::
        get_assets(" and asset.asset_id = $item_id ",true);

        if (!is_object($get_asset))
        {
            echo json_encode(
                [
                    "msg"=>"<div class='alert alert-danger'>الأصل غير موجود</div>"
                ]
            );
            return;
        }


        // TODO if this asset has entries or previous deprecation can not deleted

        // TODO if asset allowed to delete >> delete all documents,asset_documents

        $this->general_remove_item($request,'App\models\accountant\assets\assets_m');

    }

}
