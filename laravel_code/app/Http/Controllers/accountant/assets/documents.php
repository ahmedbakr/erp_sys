<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_documents_m;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\documents_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class documents extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
        //test
    }


    public function index($asset_id)
    {

        if(!$this->check_user_permission("assets/documents","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["documents"] = [];

        $this->data["asset_data"] = assets_m::get_assets(" And asset.asset_id = $asset_id " ,
                        true);

        if (!is_object($this->data["asset_data"]))
        {
            return Redirect::to('accountant/assets')->with([
                "msg" => "<div class='alert alert-danger'> الأصل غير موجود !!  </div>"
            ])->send();
        }

        $get_asset_documents = asset_documents_m::where("asset_id",$asset_id)->get();
        $get_asset_documents = $get_asset_documents->all();
        if (is_array($get_asset_documents) && count($get_asset_documents))
        {
            $get_asset_documents_ids = convert_inside_obj_to_arr($get_asset_documents,"doc_id");
            $get_asset_documents_ids = implode(',',$get_asset_documents_ids);
            $get_documents = documents_m::get_docs(" and doc.doc_id in ($get_asset_documents_ids)  ");
            $this->data["documents"] = $get_documents;
        }


        return view("accountant.subviews.assets.documents.index",$this->data);

    }


    public function save(Request $request , $doc_id = null)
    {

        $default_action = "add_action";
        if ($doc_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/documents","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $this->data["asset_id"] = "";
        $this->data["document_data"] = "";
        $this->data["all_assets"] = "";

        $all_assets = assets_m::get_assets();
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لادخال مستندات لها !!  </div>"
            ])->send();
        }

        if ($doc_id != null)
        {

            $document_data = documents_m::get_docs(" and doc.doc_id = $doc_id ");
            if (is_array($document_data) && count($document_data))
            {
                $document_data = $document_data[0];
                $this->data["document_data"] = $document_data;
            }
            else{
                return Redirect::to('accountant/documents/save')->with([
                    "msg" => "<div class='alert alert-danger'>  المستند غير موجود أضف جديد من هنا  </div>"
                ])->send();
            }


            $get_asset_document = asset_documents_m::where("doc_id",$doc_id)->get()->first();
            if (!is_object($get_asset_document))
            {
                return Redirect::to('accountant/documents')->with([
                    "msg" => "<div class='alert alert-danger'>  المستند غير مرتبط بالأصل </div>"
                ])->send();
            }
            $asset_id = $get_asset_document->asset_id;
            $this->data["asset_id"] = $asset_id;
        }


        if ($request->method() == "POST")
        {

            $this->validate($request,
                [
                    "doc_code" => "required|unique:documents,doc_code,".$doc_id.",doc_id,deleted_at,NULL",
                    "doc_start_date" => "required|date",
                    "doc_end_date" => "required|date"
                ],
                [
                    "doc_code.required" => "الكود مطلوب إدخالة",
                    "doc_code.unique" => "الكود موجود مسبقا برجاء تغييرة",
                    "doc_start_date.required" => "تاريخ الإصدار مطلوب إدخالة",
                    "doc_end_date.required" => "تاريخ الإنتهاء مطلوب إدخالة",
                ]

            );



            $request["json_values_of_sliderdoc_files"] = json_decode($request->get("json_values_of_sliderdoc_files"));



            $doc_files = $this->general_save_slider(
                $request,
                $field_name="doc_files",
                $width=0,
                $height=0,
                $new_title_arr = "",
                $new_alt_arr = "",
                $json_values_of_slider=$request["json_values_of_sliderdoc_files"],
                $old_title_arr = "",
                $old_alt_arr = [],
                $path="/documents/files"
            );

            $request = clean($request->all());

            $request["doc_files"] = json_encode($doc_files);


            if ($doc_id == null)
            {

                // save
                $new_doc = documents_m::create($request);
                $new_asset_doc = asset_documents_m::create([
                    "asset_id" => $request["asset_id"],
                    "doc_id" => $new_doc->doc_id
                ]);

                $doc_id = $new_doc->doc_id;
                $asset_id = $new_asset_doc->asset_id;
                $msg = "تم إضافة المستند بنجاح";

            }
            else{

                // update
                documents_m::findOrFail($doc_id)->update($request);
                $msg = "تم تعديل بيانات المستند بنجاح";
            }

            return Redirect::to('accountant/assets/documents/save/'.$doc_id)->with([
                "msg"=>"<div class='alert alert-success'>$msg</div>"
            ])->send();

        }

        return view("accountant.subviews.assets.documents.save",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/documents","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        asset_documents_m::where("doc_id",$item_id)->delete();

        $this->general_remove_item($request,'App\models\accountant\documents_m');

    }

}
