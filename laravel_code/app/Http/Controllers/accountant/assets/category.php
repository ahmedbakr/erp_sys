<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_category_m;
use App\models\accountant\assets\assets_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class category extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index($parent_id = 0)
    {

        if(!$this->check_user_permission("assets/category","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["all_cats"] = asset_category_m::
                get_cats(" and cat.parent_id = $parent_id ",false);

        return view("accountant.subviews.assets.category.index",$this->data);
    }

    public function save(Request $request, $asset_cat_id = null)
    {

        $default_action = "add_action";
        if ($asset_cat_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/category","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["cat_data"] = "";
        if ($asset_cat_id != null)
        {
            $this->data["cat_data"] = asset_category_m::
                    get_cats(" and cat.asset_cat_id = $asset_cat_id ",true);
        }

        $this->data["parent_cats"] = asset_category_m::
                    get_cats(" and cat.parent_id = 0 ",false);

        if ($request->method() == "POST")
        {

            $this->validate($request,[

                "asset_cat_code" => "required|unique:asset_category,asset_cat_code,".$asset_cat_id.",asset_cat_id,deleted_at,NULL",
                "asset_cat_name" => "required"

            ],
            [
                "asset_cat_code.required" => "الكود مطلوب إدخالة",
                "asset_cat_name.required" => "الاسم العربي مطلوب إدخالة",
                "asset_cat_code.unique" => "الكود موجود مسبقا برجاء تغييرة",
            ]
            );

            $request = clean($request->all());

            if ($asset_cat_id == null)
            {
                // insert
                asset_category_m::create($request);
                $msg = "تم التسجيل بنجاح";

            }
            else{
                // edit
                $new_obj = asset_category_m::findOrFail($asset_cat_id)->update($request);
                $msg = "تم التعديل بنجاح";
                $asset_cat_id = $new_obj->asset_cat_id;
            }

            return Redirect::to('accountant/assets/category/save/'.$asset_cat_id)->with([
                "msg"=>"<div class='alert alert-success'>$msg</div>"
            ])->send();

        }

        return view("accountant.subviews.assets.category.save",$this->data);
    }

    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/category","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this cat not own to any assets
        $item_id = (int)$request->get("item_id");

        $get_obj = asset_category_m::get_cats(" and cat.asset_cat_id = $item_id " , true);
        if ($get_obj->parent_id > 0)
        {
            // is child cat
            $get_assets = assets_m::
                    get_assets(" and asset.asset_cat_id = $item_id ",false);

            if (is_array($get_assets) && count($get_assets))
            {
                echo json_encode(
                    [
                        "msg"=>"<div class='alert alert-danger'>لا يمكن المسح لانه توجد أصول تابعه له</div>"
                    ]
                );
                return;
            }
        }
        else{

            // is parent cat
            $get_childs = asset_category_m::
                    get_cats(" and cat.parent_id = $item_id ",false);

            if (count($get_childs))
            {
                $get_childs_ids = convert_inside_obj_to_arr($get_childs,"asset_cat_id");
                $get_assets = assets_m::whereIn("asset_cat_id",$get_childs_ids)->get();
                $get_assets = $get_assets->all();

                if (is_array($get_assets) && count($get_assets))
                {
                    echo json_encode(
                        [
                            "msg"=>"<div class='alert alert-danger'>لا يمكن المسح لانه توجد أصول تابعه له</div>"
                        ]
                    );
                    return;
                }
            }

        }


        $this->general_remove_item($request,'App\models\accountant\assets\asset_category_m');

    }
}
