<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_expenses_and_incomes_m;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class asset_expenses_and_incomes extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($expense_or_income)
    {

        $this->data["expense_or_income"] = "مصروفات";
        if ($expense_or_income != "expense")
        {
            $this->data["expense_or_income"] = "إيرادات";
        }

        $this->data["expense_or_income_url"] = "$expense_or_income";

        if ($expense_or_income != "expense" && $expense_or_income != "income")
        {
            return Redirect::to('accountant/assets/asset_expenses_and_incomes/expense')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        if(!$this->check_user_permission("assets/asset_expenses_and_incomes","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = " AND exp_in.expense_or_income = '$expense_or_income' ";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = asset_expenses_and_incomes_m::get_data(" $cond " , false);
            }
        }

        #endregion

        return view("accountant.subviews.assets.asset_expenses_and_incomes.index",$this->data);

    }


    public function save(Request $request ,$expense_or_income)
    {

        if ($expense_or_income != "expense" && $expense_or_income != "income")
        {
            return Redirect::to('accountant/assets/asset_expenses_and_incomes/save/expense')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["expense_or_income"] = "مصروف";
        if ($expense_or_income != "expense")
        {
            $this->data["expense_or_income"] = "إيراد";
        }

        $this->data["expense_or_income_url"] = "$expense_or_income";


        if(!$this->check_user_permission("assets/asset_expenses_and_incomes","add_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/asset_expenses_and_incomes/'.$expense_or_income)->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول الأن !!  </div>"
            ])->send();
        }


        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "asset_exp_value" => "required|numeric|min:0",
                    "payment_method" => "required",
                    "asset_exp_date" => "required|date",
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "asset_exp_value.required" => "المبلغ مطلوب إدخالة",
                    "payment_method.required" => "طريقة الدفع مطلوب إدخالة",
                    "asset_exp_date.required" => "التاريخ مطلوب إدخالة",
                ]

            );

            $request = clean($request->all());
            $asset_id = $request["asset_id"];
            $request["expense_or_income"] = $expense_or_income;

            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);


            // check if asset not sold
            if (is_object($check_asset) && $check_asset->asset_is_sold == 0)
            {

                asset_expenses_and_incomes_m::create($request);

                if ($expense_or_income == "expense")
                {
                    assets_m::findOrFail($asset_id)->update([
                        "asset_total_expenses" => ($check_asset->asset_total_expenses + $request["asset_exp_value"])
                    ]);
                }
                else{
                    assets_m::findOrFail($asset_id)->update([
                        "asset_total_incomes" => ($check_asset->asset_total_incomes + $request["asset_exp_value"])
                    ]);
                }

                $msg = "لقد تم الإدخال بنجاح...";

                // TODO make entry here or ask for this before do it

            }
            else{
                return Redirect::to('accountant/assets/asset_expenses_and_incomes/save/'.$expense_or_income)->with([
                    "msg" => "<div class='alert alert-danger'> الأصل غير صالح الان قد يكون تم بيعه !! </div>"
                ])->send();
            }

            return Redirect::to('accountant/assets/asset_expenses_and_incomes/'.$expense_or_income.'?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> $msg </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.asset_expenses_and_incomes.save",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/asset_expenses_and_incomes","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        $model_name = $request->get("table_name"); // App\User

        $obj = $model_name::find($item_id);

        $get_asset = assets_m::find($obj->asset_id);

        if (!is_object($get_asset))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>الأصل غير موجود</div>"]);
            return;
        }

        if ($obj->expense_or_income == "expense")
        {
            $get_asset->update([
                "asset_total_expenses" => ($get_asset->asset_total_expenses - $obj->asset_exp_value)
            ]);
        }
        else{
            $get_asset->update([
                "asset_total_incomes" => ($get_asset->asset_total_incomes - $obj->asset_exp_value)
            ]);
        }

        // TODO make reverse entry

        $this->general_remove_item($request,'App\models\accountant\assets\asset_expenses_and_incomes_m');

    }

}
