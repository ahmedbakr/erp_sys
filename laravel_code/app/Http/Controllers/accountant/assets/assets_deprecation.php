<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_deprecation_rules_m;
use App\models\accountant\assets\assets_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_deprecation extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();

        $this->data["deprecation_types"] = [
            "deprecation_rate" => "معدل الإهلاك",
            "fixed_value" => "القسط الثابت",
            "decreased_value" => "القسط المتناقص",
            "syd_function" => "مجموع ارقام السنوات",
            "depend_on_production" => "حسب الإنتاجية / وحدات الانتاج",
            "reevaluation" => "إعادة التقدير",
        ];

    }

    public function index()
    {

        if(!$this->check_user_permission("assets/assets_deprecation","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_deprecation_rules_m::get_data(" $cond " , false);
            }
        }

        #endregion

        return view("accountant.subviews.assets.assets_deprecation.index",$this->data);

    }


    public function calc_deprecation(Request $request)
    {

        $output = [];
        $output["success"] = "";
        $output["msg"] = "";

        if(!$this->check_user_permission("assets/assets_deprecation","count_deprecation")){
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        $get_all_assets = assets_m::get_assets("
            AND asset.asset_is_sold = 0
        ",false);

        $get_all_assets = collect($get_all_assets)->groupBy("asset_deprecation_type");
        $get_all_assets = $get_all_assets->all();

        if (is_array($get_all_assets) && count($get_all_assets))
        {
//            //dump($get_all_assets);
            foreach($get_all_assets as $key => $assets)
            {

                if (key_exists($key,$this->data["deprecation_types"]) && $key != "reevaluation")
                {
                    $this->$key($assets->all());
                }
            }
        }

        $output["success"] = "success";
        $output["msg"] = "<div class='alert alert-success'>تم حساب الإهلاك بنجاح</div>";

        echo json_encode($output);
        return;

    }


    private function deprecation_rate($assets_arr)
    {
//        //dump("معدل الاهلاك");
//        //dump($assets_arr);

        foreach($assets_arr as $asset_key => $asset)
        {

            // سعر الشراء + مجموع المصاريف اللي صرفت علي الاصل
            $total_asset_purchases_with_expenses = ($asset->asset_buy_amount + $asset->asset_total_expenses);


            $year_rate = round((100/$asset->asset_age),3);
            $month_rate = round(($year_rate/12),3);
//            //dump($total_asset_purchases_with_expenses);
//            //dump($year_rate);
//            //dump("month_rate");
//            //dump($month_rate);

            $count_deprecation_for_12_month = (($total_asset_purchases_with_expenses*$year_rate)/100);
            $count_deprecation_for_1_month = $count_deprecation_for_12_month/12;
            $count_deprecation_for_1_month = round($count_deprecation_for_1_month,3);

//            //dump($count_deprecation_for_12_month);
//            //dump($count_deprecation_for_1_month);

            $get_asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset->asset_id)->get();
            $get_asset_deprecation_rules = $get_asset_deprecation_rules->all();
            if (count($get_asset_deprecation_rules))
            {

                foreach($get_asset_deprecation_rules as $rule_key => $rule)
                {
                    $update_arr = [];

                    $update_arr["assets_deprecation_rate"] = round(($month_rate*$rule->asset_deprecation_months),3);
                    $update_arr["assets_deprecation_value"] = round(($count_deprecation_for_1_month*$rule->asset_deprecation_months),3);

                    if ($rule_key == 0)
                    {
                        // إضف عليه مجمع الاهلاك لو الأصل دخل باهلاك سابق
//                        $update_arr["assets_deprecation_value"] += $asset->asset_all_deprecation_values;

                        $update_arr["asset_deprecation_start_amount"] = $total_asset_purchases_with_expenses;
                        $update_arr["assets_deprecation_value"] = round(($update_arr["assets_deprecation_rate"]*$update_arr["asset_deprecation_start_amount"])/100,3);

                        $update_arr["assets_deprecation_total_value"] = $update_arr["assets_deprecation_value"];
                    }
                    else{
                        $get_prev_deprecation_rule = assets_deprecation_rules_m::findOrFail($get_asset_deprecation_rules[$rule_key-1]->asset_rule_id);
                        $update_arr["asset_deprecation_start_amount"] = $get_prev_deprecation_rule->asset_deprecation_end_amount;
                        $update_arr["assets_deprecation_value"] = round(($update_arr["assets_deprecation_rate"]*$update_arr["asset_deprecation_start_amount"])/100,3);

                        $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                    }

                    $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_value"]),3);

                    assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update($update_arr);

//                    //dump($update_arr);

                }

            }


        }

    }


    private function fixed_value($assets_arr)
    {
//        //dump("القسط الثابت");
//        //dump($assets_arr);

        foreach($assets_arr as $asset_key => $asset)
        {

            // سعر الشراء + مجموع المصاريف اللي صرفت علي الاصل
            $total_asset_purchases_with_expenses = ($asset->asset_buy_amount + $asset->asset_total_expenses);

            $count_deprecation_for_12_month = (

                ($total_asset_purchases_with_expenses - $asset->asset_expected_sell_amount)
                /
                $asset->asset_age

            );

            $count_deprecation_for_12_month = round($count_deprecation_for_12_month,3);

            $count_deprecation_for_1_month = $count_deprecation_for_12_month/12;
            $count_deprecation_for_1_month = round($count_deprecation_for_1_month,3);

            //dump($count_deprecation_for_1_month);

            $get_asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset->asset_id)->get();
            $get_asset_deprecation_rules = $get_asset_deprecation_rules->all();
            if (count($get_asset_deprecation_rules))
            {

                foreach($get_asset_deprecation_rules as $rule_key => $rule)
                {
                    $update_arr = [];
                    $update_arr["assets_deprecation_value"] = round(($count_deprecation_for_1_month*$rule->asset_deprecation_months),3);

                    if ($rule_key == 0)
                    {
                        // إضف عليه مجمع الاهلاك لو الأصل دخل باهلاك سابق
//                        $update_arr["assets_deprecation_value"] += $asset->asset_all_deprecation_values;

                        $update_arr["asset_deprecation_start_amount"] = $total_asset_purchases_with_expenses;
                        $update_arr["assets_deprecation_total_value"] = $update_arr["assets_deprecation_value"];
                    }
                    else{
                        $get_prev_deprecation_rule = assets_deprecation_rules_m::findOrFail($get_asset_deprecation_rules[$rule_key-1]->asset_rule_id);
                        $update_arr["asset_deprecation_start_amount"] = $get_prev_deprecation_rule->asset_deprecation_end_amount;
                        $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                    }

                    $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_value"]),3);

                    assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update($update_arr);

                    //dump($update_arr);

                }

            }


        }

    }


    private function decreased_value($assets_arr)
    {
//        //dump("القسط المتناقص");
//        //dump($assets_arr);

        foreach($assets_arr as $asset_key => $asset)
        {

            // سعر الشراء + مجموع المصاريف اللي صرفت علي الاصل
            $total_asset_purchases_with_expenses = ($asset->asset_buy_amount + $asset->asset_total_expenses);

            $year_rate = round((100/$asset->asset_age)*2,3,PHP_ROUND_HALF_UP);
            $month_rate = round(($year_rate/12),3,PHP_ROUND_HALF_UP);
            //dump($year_rate);
            //dump("month_rate");
            //dump($month_rate);

            $count_deprecation_for_12_month = (($total_asset_purchases_with_expenses*$year_rate)/100);
            $count_deprecation_for_1_month = $count_deprecation_for_12_month/12;
            $count_deprecation_for_1_month = round($count_deprecation_for_1_month,3);


            //dump($count_deprecation_for_1_month);

            $get_asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset->asset_id)->get();
            $get_asset_deprecation_rules = $get_asset_deprecation_rules->all();
            if (count($get_asset_deprecation_rules))
            {

                foreach($get_asset_deprecation_rules as $rule_key => $rule)
                {
                    $update_arr = [];

                    $update_arr["assets_deprecation_rate"] = round(($month_rate*$rule->asset_deprecation_months),3,PHP_ROUND_HALF_UP);


                    if ($rule_key == 0)
                    {
                        // إضف عليه مجمع الاهلاك لو الأصل دخل باهلاك سابق
//                        $update_arr["assets_deprecation_value"] += $asset->asset_all_deprecation_values;

                        $update_arr["asset_deprecation_start_amount"] = $total_asset_purchases_with_expenses;
                        $update_arr["assets_deprecation_value"] = round(($update_arr["assets_deprecation_rate"]*$update_arr["asset_deprecation_start_amount"])/100,3);

                        $update_arr["assets_deprecation_total_value"] = $update_arr["assets_deprecation_value"];
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_value"]),3);
                    }
                    else{
                        $get_prev_deprecation_rule = assets_deprecation_rules_m::findOrFail($get_asset_deprecation_rules[$rule_key-1]->asset_rule_id);
                        $update_arr["asset_deprecation_start_amount"] = $get_prev_deprecation_rule->asset_deprecation_end_amount;
                        $update_arr["assets_deprecation_value"] = round(($update_arr["assets_deprecation_rate"]*$update_arr["asset_deprecation_start_amount"])/100,3);

                        $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_value"]),3);

                        // نتأكد من اخر واحد هل هو مساوي للقيمة التخريدية ولا لا
                        if (count($get_asset_deprecation_rules) == ($rule_key+1))
                        {

                            if ($update_arr["asset_deprecation_end_amount"] != $asset->asset_expected_sell_amount)
                            {
                                $update_arr["asset_deprecation_end_amount"] = $asset->asset_expected_sell_amount;
                                $update_arr["assets_deprecation_value"] = round($update_arr["asset_deprecation_start_amount"] - $update_arr["asset_deprecation_end_amount"],3);
                                $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                                $update_arr["assets_deprecation_rate"] = round(($update_arr["assets_deprecation_value"]/$update_arr["asset_deprecation_start_amount"])*100,3);
                            }

                        }

                    }





                    assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update($update_arr);

                    //dump($update_arr);

                }

            }


        }

    }


    private function syd_function($assets_arr)
    {
//        //dump("دالة SYD");
//        //dump($assets_arr);

        foreach($assets_arr as $asset_key => $asset)
        {

            // سعر الشراء + مجموع المصاريف اللي صرفت علي الاصل
            $total_asset_purchases_with_expenses = ($asset->asset_buy_amount + $asset->asset_total_expenses);

            $get_asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset->asset_id)->get();
            $get_asset_deprecation_rules = $get_asset_deprecation_rules->all();
            if (count($get_asset_deprecation_rules))
            {

                foreach($get_asset_deprecation_rules as $rule_key => $rule)
                {
                    $update_arr = [];


                    $deprecation_value_on_year = $this->syd(
                        $total_asset_purchases_with_expenses,
                        (double)$asset->asset_expected_sell_amount,
                        $asset->asset_age,
                        ($rule_key+1)
                        );

                    $deprecation_value_per_year = round($deprecation_value_on_year,3,PHP_ROUND_HALF_UP);
                    $deprecation_value_per_month = round($deprecation_value_on_year/12,3,PHP_ROUND_HALF_UP);

                    $update_arr["assets_deprecation_value"] = round(($deprecation_value_per_month*$rule->asset_deprecation_months),3);
                    $update_arr["asset_deprecation_start_amount"] = $total_asset_purchases_with_expenses;

                    if ($rule_key == 0)
                    {
                        // إضف عليه مجمع الاهلاك لو الأصل دخل باهلاك سابق
//                        $update_arr["assets_deprecation_value"] += $asset->asset_all_deprecation_values;

                        $update_arr["assets_deprecation_total_value"] = $update_arr["assets_deprecation_value"];
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"] - $update_arr["assets_deprecation_total_value"]),3);
                    }
                    else{
                        $get_prev_deprecation_rule = assets_deprecation_rules_m::findOrFail($get_asset_deprecation_rules[$rule_key-1]->asset_rule_id);

                        $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_total_value"]),3);

                    }


                    assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update($update_arr);

                    //dump($update_arr);

                }

            }


        }

    }

    private function syd($cost,$salvage,$life,$per)
    {

        return (

            (($cost - $salvage)*($life - $per + 1)*2)
            /
            (($life)*($life + 1))

        );

    }


    private function depend_on_production($assets_arr)
    {
        //dump("حسب الإنتاجية");
        //dump($assets_arr);

        foreach($assets_arr as $asset_key => $asset)
        {

            // سعر الشراء + مجموع المصاريف اللي صرفت علي الاصل
            $total_asset_purchases_with_expenses = ($asset->asset_buy_amount + $asset->asset_total_expenses);

            $get_asset_deprecation_rules = assets_deprecation_rules_m::where("asset_id",$asset->asset_id)->get();
            $get_asset_deprecation_rules = $get_asset_deprecation_rules->all();
            if (count($get_asset_deprecation_rules))
            {

                $deprecation_rate = (
                    ($total_asset_purchases_with_expenses - $asset->asset_expected_sell_amount)
                    /
                    ($asset->asset_working_units)
                );

                $deprecation_rate = round($deprecation_rate,3,PHP_ROUND_HALF_UP);

                foreach($get_asset_deprecation_rules as $rule_key => $rule)
                {
                    $update_arr = [];


                    $deprecation_value_per_year = ($deprecation_rate * $rule->real_working_units);
                    $deprecation_value_per_year = round($deprecation_value_per_year,3,PHP_ROUND_HALF_UP);

                    $deprecation_value_per_month = round($deprecation_value_per_year/12,3,PHP_ROUND_HALF_UP);

                    $update_arr["assets_deprecation_rate"] = $deprecation_rate;

                    $update_arr["assets_deprecation_value"] = round(($deprecation_value_per_month*$rule->asset_deprecation_months),3);
                    $update_arr["asset_deprecation_start_amount"] = $total_asset_purchases_with_expenses;

                    if ($rule_key == 0)
                    {
                        // إضف عليه مجمع الاهلاك لو الأصل دخل باهلاك سابق
//                        $update_arr["assets_deprecation_value"] += $asset->asset_all_deprecation_values;

                        $update_arr["assets_deprecation_total_value"] = $update_arr["assets_deprecation_value"];
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"] - $update_arr["assets_deprecation_total_value"]),3);
                    }
                    else{
                        $get_prev_deprecation_rule = assets_deprecation_rules_m::findOrFail($get_asset_deprecation_rules[$rule_key-1]->asset_rule_id);

                        $update_arr["assets_deprecation_total_value"] = round(($update_arr["assets_deprecation_value"]+$get_prev_deprecation_rule->assets_deprecation_total_value),3);
                        $update_arr["asset_deprecation_end_amount"] = round(($update_arr["asset_deprecation_start_amount"]- $update_arr["assets_deprecation_total_value"]),3);

                    }

                    assets_deprecation_rules_m::findOrFail($rule->asset_rule_id)->update($update_arr);

                    //dump($update_arr);

                }

            }


        }

    }

}
