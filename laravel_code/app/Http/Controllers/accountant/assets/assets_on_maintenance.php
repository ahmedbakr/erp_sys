<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\asset_status_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\assets\assets_on_maintenance_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_on_maintenance extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        if(!$this->check_user_permission("assets/assets_on_maintenance","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_on_maintenance_m::get_data(" $cond " , false);
            }
        }

        #endregion


        return view("accountant.subviews.assets.assets_on_maintenance.index",$this->data);
    }


    public function save(Request $request , $asset_o_m_id = null)
    {

        $default_action = "add_action";
        if ($asset_o_m_id != null)
        {
            $default_action = "edit_action";
        }

        if(!$this->check_user_permission("assets/assets_on_maintenance","$default_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        $this->data["maintenance"] = "";
        $asset_id_required = "required|";
        $start_date_required = "required|";
        $days_required = "required|";

        if ($asset_o_m_id != null)
        {
            $get_maintenance = assets_on_maintenance_m::get_data(" 
                AND maintenance.asset_o_m_id = $asset_o_m_id
            ", true);
            if (!is_object($get_maintenance))
            {
                return Redirect::to('accountant/assets/assets_on_maintenance')->with([
                    "msg" => "<div class='alert alert-danger'> لا يمكنك التعديل لعدم وجود البيانات الخاصة به !!  </div>"
                ])->send();
            }
            $this->data["maintenance"] = $get_maintenance;
            $asset_id_required = "";

            // خرج من الصيانة
            if ($get_maintenance->is_maintenance_finish == 1)
            {
                $start_date_required = "";
                $days_required = "";
            }

        }


        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
                AND asset.asset_is_rubbish = 0 
                AND asset.asset_on_maintenance = 0 
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/assets_on_maintenance')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول لإدخالها الصيانة الأن !!  </div>"
            ])->send();
        }


        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => $asset_id_required."numeric|min:0",
                    "maintenance_start_date" => $start_date_required."date",
                    "maintenance_days" => $days_required."numeric|min:0"
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "maintenance_start_date.required" => "تاريخ الصيانة مطلوب إدخالة",
                    "maintenance_days.required" => "مدة الصيانة مطلوب إدخالة"
                ]

            );

            $request = clean($request->all());
            if (isset($request["asset_id"]) && $asset_o_m_id == null)
            {
                $asset_id = $request["asset_id"];
            }
            else{
                $asset_id = $get_maintenance->asset_id;
            }

            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);

            if ($asset_o_m_id == null)
            {
                // check if asset not sold , rubbish , maintenance

                if (is_object($check_asset) && $check_asset->asset_is_rubbish == 0
                    && $check_asset->asset_is_sold == 0 && $check_asset->asset_on_maintenance == 0)
                {

                    assets_on_maintenance_m::create($request);

                    assets_m::findOrFail($asset_id)->update([
                        "asset_on_maintenance" => 1
                    ]);

                    $msg = "لقد تم ادخال الأصل في الصيانة بنجاح...";

                }
                else{
                    return Redirect::to('accountant/assets/assets_on_maintenance/save')->with([
                        "msg" => "<div class='alert alert-danger'> الأصل غير صالح لادخالة الصيانة الان !! </div>"
                    ])->send();
                }
            }
            else{

                if (isset($request["asset_id"]))
                {
                    unset($request["asset_id"]);
                }

                if ($get_maintenance->is_maintenance_finish == 1)
                {
                    unset(
                        $request["maintenance_days"],
                        $request["maintenance_start_date"]
                    );
                }

                assets_on_maintenance_m::findOrFail($asset_o_m_id)->update($request);

                $msg = "لقد تم تعديل بيانات الأصل في الصيانة بنجاح...";

            }

            return Redirect::to('accountant/assets/assets_on_maintenance?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> $msg </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.assets_on_maintenance.save",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/assets_on_maintenance","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        $model_name = $request->get("table_name"); // App\User

        $obj = $model_name::find($item_id);

        // check if this item is last item to be removed on maintenance update asset
        $check = $model_name::where("asset_id",$obj->asset_id)->get();
        $check = $check->all();
        if (count($check) == 1)
        {
            assets_m::findOrFail($obj->asset_id)->update([
                "asset_on_maintenance" => 0
            ]);
        }


        $this->general_remove_item($request,'App\models\accountant\assets\assets_on_maintenance_m');

    }


    public function is_maintenance_finish(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/assets_on_maintenance","is_maintenance_finish"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }

        $output = array();

        $item_id = $request->get("item_id");
        $model_name = $request->get("table_name");
        $field_name = $request->get("field_name");
        $accept = $request->get("accept");
        $item_primary_col= $request->get("item_primary_col");
        $accepters_data= $request->get("acceptersdata");
        $accept_url= $request->get("accept_url");


        $output["msg"]="";
        if ($item_id > 0) {
            $obj = $model_name::find($item_id);
            $make_update=true;


            if($make_update)
            {

                if ($accept == 0)
                {
                    $output["msg"] = generate_multi_accepters($accept_url,$obj,$item_primary_col,$field_name,$model_name,json_decode($accepters_data));
                }
                else{
                    $obj->update([
                        "is_maintenance_finish" => 1
                    ]);

                    // check if this item is last item to be finished on maintenance update asset
                    $check = $model_name::where("asset_id",$obj->asset_id)
                                        ->where('is_maintenance_finish',0)->get();
                    $check = $check->all();
                    if (count($check) == 0)
                    {
                        assets_m::findOrFail($obj->asset_id)->update([
                            "asset_on_maintenance" => 0
                        ]);
                    }

                    $output["msg"] = "<div class='alert alert-warning'>";
                    $output["msg"] .= "تم إنتهاء الصيانة للأصل";
                    $output["msg"] .= " برجاء تغيير حالة الأصل";
                    $output["msg"] .= "</div>";
                }

            }

        }

        echo json_encode($output);

    }

}
