<?php

namespace App\Http\Controllers\accountant\assets;

use App\Http\Controllers\is_accountant_controller;
use App\models\accountant\assets\asset_locations_m;
use App\models\accountant\assets\assets_m;
use App\models\accountant\assets\assets_rent_m;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class assets_rent extends is_accountant_controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {


        if(!$this->check_user_permission("assets/assets_rent","show_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }

        #region filteration

        $this->data["all_locations"] = asset_locations_m::all();
        $this->data["assets"] = [];

        $this->data["asset_code"] = "";
        $this->data["from_asset_buy_date"] = "";
        $this->data["to_asset_buy_date"] = "";
        $this->data["asset_location_id"] = "";
        $this->data["asset_title"] = "";
        $this->data["asset_buy_amount"] = "";

        $get_filter_values = $_GET;
        $cond = "";
        if (is_array($get_filter_values) && count($get_filter_values))
        {

            $asset_code = $get_filter_values["asset_code"];
            $from_asset_buy_date = $get_filter_values["from_asset_buy_date"];
            $to_asset_buy_date = $get_filter_values["to_asset_buy_date"];
            $asset_location_id = $get_filter_values["asset_location_id"];
            $asset_title = $get_filter_values["asset_title"];
            $asset_buy_amount = $get_filter_values["asset_buy_amount"];

            $this->data["asset_code"] = $asset_code;
            $this->data["from_asset_buy_date"] = $from_asset_buy_date;
            $this->data["to_asset_buy_date"] = $to_asset_buy_date;
            $this->data["asset_location_id"] = $asset_location_id;
            $this->data["asset_title"] = $asset_title;
            $this->data["asset_buy_amount"] = $asset_buy_amount;

            ($asset_code != "")?($cond .= " AND asset.asset_code = '$asset_code' "):($cond .= "");
            ($from_asset_buy_date != "")?
                ($cond .= " AND asset.asset_buy_date >= '$from_asset_buy_date'
                            AND asset.asset_buy_date <= '$to_asset_buy_date' "):($cond .= "");

            ($asset_location_id != "0")?($cond .= " AND asset.asset_location_id = $asset_location_id "):($cond .= "");
            ($asset_title != "")?($cond .= " AND asset.asset_title like '%$asset_title%' "):($cond .= "");
            ($asset_buy_amount != "")?($cond .= " AND asset.asset_buy_amount = $asset_buy_amount "):($cond .= "");

            if (!empty($cond))
            {
                $this->data["assets"] = assets_rent_m::get_data(" $cond " , false);
            }
        }

        #endregion

        return view("accountant.subviews.assets.assets_rent.index",$this->data);

    }


    public function save(Request $request)
    {

        if(!$this->check_user_permission("assets/assets_rent","add_action")){
            return Redirect::to('accountant/dashboard')->with([
                "msg" => "<div class='alert alert-danger'> ليس لديك الصلاحية للدخول لهذة الصفحة !!  </div>"
            ])->send();
        }


        $all_assets = assets_m::get_assets(" 
                AND asset.asset_is_sold = 0 
                AND asset.asset_is_rubbish = 0 
            ");
        if (is_array($all_assets) && count($all_assets))
        {
            $this->data["all_assets"] = $all_assets;
        }
        else{
            return Redirect::to('accountant/assets/assets_rent')->with([
                "msg" => "<div class='alert alert-danger'> لا يوجد أصول الأن !!  </div>"
            ])->send();
        }


        if($request->method() == "POST")
        {
            $this->validate($request,
                [
                    "asset_id" => "required|numeric|min:0",
                    "assets_rent_amount" => "required|numeric|min:0",
                    "payment_method" => "required",
                    "assets_rent_period_type" => "required",
                    "assets_rent_period_number" => "required|numeric|min:1",
                    "assets_rent_start_date" => "required|date",
                ],
                [
                    "asset_id.required" => "الأصل مطلوب إدخالة",
                    "assets_rent_amount.required" => "المبلغ مطلوب إدخالة",
                    "payment_method.required" => "طريقة الدفع مطلوب إدخالة",
                    "assets_rent_period_type.required" => "نوع المده مطلوب إدخالة",
                    "assets_rent_period_number.required" => "المدة مطلوب إدخالة",
                    "assets_rent_start_date.required" => "التاريخ مطلوب إدخالة",
                ]

            );

            $request = clean($request->all());
            $asset_id = $request["asset_id"];

            $assets_rent_start_date = $request["assets_rent_start_date"];
            $assets_rent_period_number = $request["assets_rent_period_number"];
            $assets_rent_period_type = $request["assets_rent_period_type"];

            if ($assets_rent_period_type == "day")
            {
                $assets_rent_end_date = Carbon::
                    createFromTimestamp(strtotime($assets_rent_start_date))->addDays($assets_rent_period_number)->toDateString();
            }
            elseif ($assets_rent_period_type == "month")
            {
                $assets_rent_end_date = Carbon::
                    createFromTimestamp(strtotime($assets_rent_start_date))->addMonth($assets_rent_period_number)->toDateString();
            }
            elseif ($assets_rent_period_type == "Year")
            {
                $assets_rent_end_date = Carbon::
                    createFromTimestamp(strtotime($assets_rent_start_date))->addYear($assets_rent_period_number)->toDateString();
            }

            $request["assets_rent_end_date"] = $assets_rent_end_date;

            $check_asset = assets_m::get_assets(" AND asset.asset_id = $asset_id ",true);

            // check if asset not sold
            if (is_object($check_asset) && $check_asset->asset_is_sold == 0 && $check_asset->asset_is_rubbish == 0)
            {


                // check if this asset have previous rent on this range
                $check_asset_rent = assets_rent_m::where("asset_id",$asset_id)
                            ->where("assets_rent_end_date",">",$assets_rent_start_date)->get();
                $check_asset_rent = $check_asset_rent->all();

                if (is_array($check_asset_rent) && count($check_asset_rent))
                {

                    return Redirect::to('accountant/assets/assets_rent/save')->with([
                        "msg" => "<div class='alert alert-danger'> توجد إيجارات لهذا الأصل في هذة الفترة مسبقا !! </div>"
                    ])->send();

                }


                assets_rent_m::create($request);

                $msg = "لقد تم الإدخال بنجاح...";

                // TODO make entry here or ask for this before do it

            }
            else{
                return Redirect::to('accountant/assets/assets_rent/save/')->with([
                    "msg" => "<div class='alert alert-danger'> الأصل غير صالح الان قد يكون تم بيعه !! </div>"
                ])->send();
            }

            return Redirect::to('accountant/assets/assets_rent/?asset_code='.$check_asset->asset_code.'&from_asset_buy_date=&to_asset_buy_date=&asset_location_id=0&asset_title=&asset_buy_amount=')->with([
                "msg" => "<div class='alert alert-success'> $msg </div>"
            ])->send();

        }

        return view("accountant.subviews.assets.assets_rent.save",$this->data);
    }


    public function remove(Request $request)
    {

        if (!check_permission($this->user_permissions,"assets/assets_rent","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>غير مسموح للدخول لهذة الصفحه</div>"]);
            return;
        }


        // check if this status not own to any assets
        $item_id = (int)$request->get("item_id");

        $model_name = $request->get("table_name"); // App\User

        $obj = $model_name::find($item_id);

        $get_asset = assets_m::find($obj->asset_id);

        if (!is_object($get_asset))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>الأصل غير موجود</div>"]);
            return;
        }

        // TODO make reverse entry

        $this->general_remove_item($request,'App\models\accountant\assets\assets_rent_m');

    }

}
