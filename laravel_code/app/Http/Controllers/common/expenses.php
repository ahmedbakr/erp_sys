<?php

namespace App\Http\Controllers\common;

use App\Http\Controllers\dashbaord_controller;
use App\Http\Controllers\is_accountant_controller;
use App\Http\Controllers\is_admin_controller;
use App\Http\Controllers\is_branch_controller;
use App\models\admin\category_m;
use App\models\expenses\expenses_m;
use App\models\expenses\expenses_types_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class expenses extends dashbaord_controller
{

    public $page_permission="";
    public $syb_sys="";
    public $main_layout="";


    public function __construct(){
        parent::__construct();

        if(!in_array($this->current_user_data->user_type,["accountant","admin","factory_admin","branch","branch_admin"])){
            return Redirect::to("/")->send();
        }

        if($this->current_user_data->user_type=="accountant"){
            $this->page_permission="accountant/expenses";
            $this->syb_sys="accountant";
            $this->main_layout="accountant";

            $init_cons=new is_accountant_controller();


        }

        if($this->current_user_data->user_type=="admin"||$this->current_user_data->user_type=="factory_admin"){
            $this->page_permission="factory/expenses";
            $this->syb_sys="admin";
            $this->main_layout="admin";
            $init_cons=new is_admin_controller();

        }

        if($this->current_user_data->user_type=="branch"||$this->current_user_data->user_type=="branch_admin"){
            $this->page_permission="branch/expenses";
            $this->syb_sys="branch";
            $this->main_layout="branch";

            $init_cons=new is_branch_controller();
        }

        $this->data=array_merge($this->data,$init_cons->data);

        $this->data["page_permission"]=$this->page_permission;
        $this->data["syb_sys"]=$this->syb_sys;
        $this->data["main_layout"]=$this->main_layout;
    }

    public function show_all_expenses(Request $request){

        if(!check_permission($this->user_permissions,$this->page_permission,"show_action",$this->data["current_user"])){

            return Redirect::to($this->syb_sys.'/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }


        $this->show_category_tree(true,"expense");


        $expenses_type_id=$request->get("expenses_type_id","");
        $expenses_date_from=$request->get("expenses_date_from","");
        $expenses_date_to=$request->get("expenses_date_to","");
        $group_by=$request->get("group_by","");


        $this->data["group_by"]=$group_by;

        $this->data["all_expenses_types"]=category_m::where("cat_type","expense")->get()->all();

        $cond_arr=[];

        $this->data["select_expenses_type"]="0";
        if($expenses_type_id>0){

            //get all cat childs of $expenses_type_id
            $all_cats_ids=category_m::get_all_child_cats([$expenses_type_id]);

            if(isset_and_array($all_cats_ids)){
                $cond_arr[]=" AND ex.expense_type_id in (".implode(",",$all_cats_ids).")";
            }


            $this->data["expense_cat_data"]=category_m::find($expenses_type_id);
            $this->data["select_expenses_type"]=$expenses_type_id;
        }

        $this->data["expenses_date_from"]=0;
        if($expenses_date_from!=""&&$expenses_date_from!=0){
            $cond_arr[]=" AND ex.expense_date >='$expenses_date_from'";
            $this->data["expenses_date_from"]=$expenses_date_from;
        }

        $this->data["expenses_date_to"]=0;
        if($expenses_date_to!=""&&$expenses_date_to!=0){
            $cond_arr[]=" AND ex.expense_date <= '$expenses_date_to'";
            $this->data["expenses_date_to"]=$expenses_date_to;
        }

        if ($this->data["current_user"]->user_type != "admin")
        {
            $cond_arr[]=" AND ex.user_id = $this->branch_id";
        }

        $this->data["all_expenses_rows"]=expenses_m::get_expenses(implode(" ",$cond_arr));
        if(is_object($this->data["all_expenses_rows"])){
            $this->data["all_expenses_rows"]=[$this->data["all_expenses_rows"]];
        }

        return view("common.expenses.expenses.show",$this->data);
    }

    public function save_expenses(Request $request,$expenses_id=null){

        if(!in_array($this->current_user_data->user_type,["admin","branch_admin"])){
            return Redirect::to($this->syb_sys.'/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا يمكنك ان تدخل الا اذا كنت مدير فرع او موظف في الفرع</div>"
            )->send();
        }

        if($expenses_id==null){

            if(!check_permission($this->user_permissions,$this->page_permission,"add_action",$this->data["current_user"])){
                return Redirect::to($this->syb_sys.'/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }
        else{

            return Redirect::to('common/expenses/save_expenses')->with(
                "msg","<div class='alert alert-danger'>لا يمكنك تعديل مصروف</div>"
            )->send();

//            if(!check_permission($this->user_permissions,$this->page_permission,"edit_action",$this->data["current_user"])){
//                return Redirect::to($this->syb_sys.'/dashboard')->with(
//                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
//                )->send();
//            }
        }

        $this->show_category_tree(true,"expense");

        $this->get_branches_and_other_data();

        if ($this->data["current_user"]->user_type == "admin")
        {
            $this->data["all_branches"] = User::get_users(" AND u.user_type = 'branch' ");
        }
        else{
            $this->data["all_branches"] = User::get_users(" AND u.user_id = $this->branch_id ");
        }


        $expenses_data="";
        if($expenses_id!=null){
            $expenses_data=expenses_m::get_expenses(" AND ex.expense_id=$expenses_id");
        }
        $this->data["expenses_data"]=$expenses_data;


        if($request->method()=="POST"){

            $request_data = $this->cleaning_input($request->all(), $except = array());

            $validation_rules=[
                "user_id"=>"required",
                "expenses_maker"=>"required",
                "expense_name"=>"required",
                "expense_amount"=>"required",
                "expense_date"=>"required",
                "expense_type_id"=>"required"
            ];

            $validation_msgs = [
                "user_id.required" => " من فضلك إختار الفرع ",
                "expenses_maker.required" => " من فضلك إختار المستخدم ",
                "expense_name.required" => " من فضلك ادخل اسم المصروف ",
                "expense_amount.required" => " من فضلك ادخل قيمة المصروف ",
                "expense_date.required" => " من فضلك ادخل تاريخ المصروف ",
                "expense_type_id.required" => " من فضلك ادخل نوع المصروف ",
            ];

//            if(!$request->get("expense_type_id")){
//                unset($validation_rules["expense_type_id"]);
//            }

            $this->validate($request,$validation_rules,$validation_msgs);

            // check if his duty_balance is >= this expense
            $expenses_maker = clean($request->get('expenses_maker'));
            $expense_amount = clean($request->get('expense_amount'));

            $expenses_maker_user = User::get_users(" AND u.user_id = $expenses_maker AND u.user_type ='branch_admin' ");
            if(!count($expenses_maker_user))
            {
                return Redirect::to('common/expenses/save_expenses')->with(
                    "msg","<div class='alert alert-danger'>المستخدم غير موجود </div>"
                )->send();
            }
//
//            $expenses_maker_user = $expenses_maker_user[0];
//            if($expenses_maker_user->user_duty_balance < $expense_amount)
//            {
//                return Redirect::to('common/expenses/save_expenses')->with(
//                    "msg","<div class='alert alert-danger'>رصيد المستخدم للعهد غير كاف !! </div>"
//                )->send();
//            }

            $request["json_values_of_sliderslider_file"] = json_decode($request->get("json_values_of_sliderslider_file"));

            $request["expense_attachment"] = $this->general_save_slider(
                $request,
                $field_name="slider_file",
                $width=0,
                $height=0,
                $new_title_arr = "",
                $new_alt_arr = "",
                $json_values_of_slider=$request["json_values_of_sliderslider_file"],
                $old_title_arr = "",
                $old_alt_arr = "",
                $path="/att_slider"
            );

            $request_data["expense_attachment"] = json_encode($request["expense_attachment"]);


            if($expenses_id==null){
//                User::find($expenses_maker)->update([
//                    "user_duty_balance" => ($expenses_maker_user->user_duty_balance - $expense_amount)
//                ]);
                $this->track_my_action(" تم إضافة مصروف جديد ");

                $expenses_data=expenses_m::create($request_data);
                $expenses_id=$expenses_data->expense_id;

            }
            else{
//                expenses_m::find($expenses_id)->update($request_data);
            }

            return Redirect::to('common/expenses/show_all_expenses')->with([
                "msg"=>"<div class='alert alert-success'>تم الاضافة بنجاح في إنتظار الموافقة</div>"
            ])->send();
        }

        return view("common.expenses.expenses.save",$this->data);
    }

    public function remove_expenses(Request $request){

        if(!check_permission($this->user_permissions,$this->page_permission,"delete_action",$this->data["current_user"])){
            return Redirect::to($this->syb_sys.'/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $output = array();
        $item_id = (int)$request->get("item_id");
        $expense_obj = expenses_m::find($item_id);
        if($expense_obj->expense_status == 1)
        {
            $output["deleted"] = "no";
            echo json_encode($output);
            return;
        }

        if ($item_id > 0) {

            expenses_m::destroy($item_id);

            $removed_item = expenses_m::find($item_id);
            if (!isset($removed_item)) {

                #region save in site_tracking
                $this->track_my_action(
                    $action_desc = " مسح المصروف رقم $item_id"
                );
                #endregion

                $output["deleted"] = "yes";
            }

        }

        echo json_encode($output);

    }

    public function change_expense_status(Request $request){
        if (!check_permission($this->user_permissions,$this->page_permission,"change_expense_status",$this->data["current_user"]))
        {
            $output["msg"]="<div class='alert alert-danger'>غير مسموح</div>";
            echo json_encode($output);
            return;
        }
        $output=[];

        $expense_id = clean($request->get("item_id"));
        $expense_id=(int)$expense_id;

        $status_id = $request->get("accept");

        if(!(in_array($status_id,[0,1]))){
            $output["msg"]="error";
            echo json_encode($output);
            return;
        }

        $expense_obj=expenses_m::findOrFail($expense_id);

        //get branch of this expense
        $user_id=$expense_obj->expenses_maker;

        $user=User::
        where("user_id",$user_id)->
        get()->first();

        $output["msg"]="";
        if (!is_object($user))
        {
            $output["msg"].=" المستخدم غير موجود ";
            echo json_encode($output);
            return;
        }

//        if($user->user_duty_balance < $expense_obj->expense_amount)
//        {
//            $output["msg"].=" رصيد المستخدم غير كاف ";
//            echo json_encode($output);
//            return;
//        }

        if($status_id=="0"&&$expense_obj->expense_status=="1"&&is_object($user)) {

            $output["msg"].=" غير مسموح ";
            echo json_encode($output);
            return;

            //refuse expense after it was accepted

            $user_new_balance=$user->user_balance+$expense_obj->expense_amount;
            $user->update(["user_balance"=>"$user_new_balance"]);

            $output["msg"].="تم الغاء الموافقة علي هذا المصروف وتم اضافة قيمة المصروف الي رصيد المسؤل عن هذا المصروف";
        }
        else if($status_id=="1"&&$expense_obj->expense_status=="0"&&is_object($user)){
            $user_new_balance=$user->user_balance-$expense_obj->expense_amount;
            $user->update(["user_balance"=>"$user_new_balance"]);

            $output["msg"].="تمت  الموافقة علي هذا المصروف وتم خصم قيمة المصروف من رصيد المسؤل عن هذا المصروف";
        }

        $expense_obj->update(["expense_status"=>$status_id]);

//        $output["msg"].=generate_multi_accepters(
//            $accepturl=url("/common/expenses/change_expense_status"),
//            $item_obj=$expense_obj,
//            $item_primary_col="expense_id",
//            $accept_or_refuse_col="expense_status",
//            $model="",
//            $accepters_data=["0"=>"ارفض","1"=>"وافق"]
//        );

        echo json_encode($output);
    }

}
