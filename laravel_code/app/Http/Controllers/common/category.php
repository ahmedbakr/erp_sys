<?php

namespace App\Http\Controllers\common;

use App\Http\Controllers\dashbaord_controller;
use App\Http\Controllers\is_accountant_controller;
use App\Http\Controllers\is_admin_controller;
use App\models\admin\category_m;
use App\models\product\branches_products_m;
use App\models\product\products_m;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Redirect;

class category extends dashbaord_controller
{

    public $page_permission="";
    public $syb_sys="";
    public $main_layout="";

    public function __construct(){
        parent::__construct();


    }


    public function category_tree($just_load_data=false,$cat_type="product"){
        return parent::show_category_tree($just_load_data,$cat_type);
    }

    public function save_category(Request $request,$cat_type="product",$parent_id,$cat_id=null){

        if ($cat_id==null){
            if(!check_permission($this->user_permissions,$this->page_permission,"add_action",$this->current_user_data)){
                return Redirect::to($this->syb_sys.'/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }
        else{
            if(!check_permission($this->user_permissions,$this->page_permission,"edit_action",$this->current_user_data)){
                return Redirect::to($this->syb_sys.'/dashboard')->with(
                    "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
                )->send();
            }
        }


        $this->data["selected_cat_type"]=$cat_type;

        $this->data["parent_id"]=$parent_id;

        $cat_obj="";
        if ($cat_id!=null){

            $cat_obj=category_m::findOrFail($cat_id);
        }
        $this->data["cat_obj"]=$cat_obj;


        if ($request->method()=="POST"){

            $this->validate($request,[
                "cat_name"=>"required",
                "cat_name_en"=>"required",
            ]);


            $request["parent_id"]=$parent_id;
            $request["cat_type"]=$cat_type;


            if($cat_id==null){
                //add
                $cat_obj=category_m::create($request->all());
            }
            else{
                //edit
                $cat_obj->update($request->all());
            }



            //check if cat_code is empty if true then update cat_code to this obj cat_code
            if(empty($request["cat_code"])){
                $cat_obj->update([
                    "cat_code"=>$cat_obj->cat_id
                ]);
            }


            return Redirect::to('common/category/show_tree/false/'.$cat_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء العملية بنجاح</div>"
            )->send();

        }


        return view("common.category.save",$this->data);
    }

    public function move_category_to_another_parent(Request $request,$cat_type,$cat_id){

        if(!check_permission($this->user_permissions,$this->page_permission,"move_category_to_another_parent",$this->current_user_data)){
            return Redirect::to($this->syb_sys.'/dashboard')->with(
                "msg","<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>"
            )->send();
        }

        $this->show_category_tree(true,$cat_type);

        $this->data["selected_cat_type"]=$cat_type;

        //load all categories from tree

        $cat_data=category_m::get_cats(" AND cat.cat_id=$cat_id");

        if(isset_and_array($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            abort(404);
        }
        $this->data["cat_data"]=$cat_data;

        if ($request->method()=="POST"){
            $cat_obj=category_m::findOrFail($cat_id);

            $parent_id=Input::get("parent_id",0);

            //of course he can not make category itself the parent_id
            if($cat_id==$parent_id){
                return Redirect::to('common/category/show_tree')->with(
                    "msg","<div class='alert alert-danger'>
                            <img src='".url("/public_html/img/alert.jpg")."' >
                        </div>"
                )->send();
            }

            //nodes in same tree branch can not move to down
            //example
            //a>b>c>d
            //you can not make d parent of a

            //get all childs of current node and check if $parent_id is one of them
            //if true then this operation must cancelled
            $move_to_child=category_m::check_in_parent_node_contain_node([$cat_obj->cat_id],$parent_id);

            if ($move_to_child){
                return Redirect::to('common/category/show_tree')->with(
                    "msg","<div class='alert alert-danger'>
                            لا يمكن ان تتم هذه العملية لاسباب منطقية
                        </div>"
                )->send();
            }


            $cat_obj->update([
                "parent_id"=>$parent_id
            ]);


            return Redirect::to('common/category/show_tree/false/'.$cat_type)->with(
                "msg","<div class='alert alert-success'>تم اجراء عملية النقل بنجاح</div>"
            )->send();
        }



        return view("common.category.move_category_to_another_parent",$this->data);
    }

    public function remove_general_account_category(Request $request){


        if(!check_permission($this->user_permissions,$this->page_permission,"delete_action",$this->current_user_data)){
            $output["msg"]="<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>";
            echo json_encode($output);
            return;
        }
        $cat_id = (int)$request->get("item_id");


        //no one can remove category has accounts
        $accounts=category_m::get_all_accounts_in_branch([$cat_id]);

        if(isset_and_array($accounts)){
            $output["msg"]="<div class='alert alert-warning'>
لا يمكنك ان تمسح تصنيف يحتوي علي حسابات عامة.اذا اردت ان تمسحه يمكنك ان تمسح جميع
 الحسابات الفرعية بداخله اولا او انقل الحسابات الفرعية الي تصنيف اخر .حينئذ يمكنك ان تسمح 
            </div>";
            echo json_encode($output);
            return;
        }


        $this->general_remove_item($request,'App\models\admin\category_m');
    }


    public function remove_category(Request $request){


        if(!check_permission($this->user_permissions,$this->page_permission,"delete_action",$this->current_user_data)){
            $output["msg"]="<div class='alert alert-danger'>لا تمتلك صلاحية للدخول الي هنا</div>";
            echo json_encode($output);
            return;
        }
        $cat_id = (int)$request->get("item_id");


        //no one can remove category has childs
        $childs=category_m::get_all_child_cats([$cat_id]);

        if(count($childs) > 1){
            $output["msg"]="<div class='alert alert-warning'>لا يمكنك المسح لوجود أقسام فرعية بداخلة</div>";
            echo json_encode($output);
            return;
        }
        else{
            // check that this cat has products on product stock
            $get_products = products_m::where("cat_id",$cat_id)->get()->all();
            if(count($get_products))
            {
                $output["msg"]="<div class='alert alert-warning'>لا يمكنك المسح لوجود منتجات بداخلة</div>";
                echo json_encode($output);
                return;
            }
        }


        $this->general_remove_item($request,'App\models\admin\category_m');
    }





}
