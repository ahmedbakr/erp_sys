<?php

namespace App\Http\Controllers\cron_jobs;

use App\models\bills\clients_bills_m;
use App\models\commission\commission_rules_m;
use App\models\commission\day_commission_m;
use App\models\money_transfers_m;
use App\models\notification_m;
use App\models\orders\clients_orders_m;
use App\models\settings_m;
use App\models\site_tracking_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class count_commissions extends Controller
{

    public function index($redirect = false)
    {
        $this->calc_type_2_commission_rules();

        \Debugbar::disable();

        $current_hour = date("H");

        $get_current_date = date("Y-m-d");

        $current_data=Carbon::now();
        if ($current_hour >= 5)
        {
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day+1, $hour = 4,59);
        }
        else{
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }

        $start_day = $start_day->toDateTimeString();
        $end_day = $end_day->toDateTimeString();


        $today_date_time = date("Y-m-d H:i:s");


        $all_commission_rules = commission_rules_m::where("commission_rule_type","0")->get()->all();
//        $all_commission_rules = $all_commission_rules->all();
        if (is_array($all_commission_rules) && count($all_commission_rules))
        {

            #region get all users

            $all_users = User::all();
            $all_users = collect($all_users)->groupBy("user_type");

            #endregion

            #region get all admins

            if (!isset($all_users["admin"]))
            {
                return;
            }
            $all_admins = $all_users["admin"]->all();

            #endregion

            #region get all branches

            if (!isset($all_users["branch"]))
            {
                return;
            }
            $all_branches = $all_users["branch"]->all();
            #endregion


            foreach($all_branches as $key => $branch)
            {


                #region get last day commission if exist

                $last_commission = day_commission_m::get_data("
                    AND rule.commission_rule_type=0
                    AND commission.user_id = $branch->user_id
                    order by commission.day_commission_id desc 
                    limit 1
                ");



                #endregion
                if (is_array($last_commission) && count($last_commission))
                {
                    $last_commission = $last_commission[0];

                    #region get money transfers of client bills greater than last_commission date

                        if (
                                $last_commission->commission_updated_at >= $start_day &&
                                $last_commission->commission_updated_at <= $end_day
                            )
                        {
                            // on same range day
                            $get_all_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.to_user_id = $branch->user_id 
                                    OR
                                    money.from_user_id = $branch->user_id
                                )
                                AND (money.created_at) > '$last_commission->commission_updated_at'
                                AND (money.created_at) <= '$end_day' 
                                
                                order by money.created_at asc
                                
                            ");

                            // if received
                            if ($last_commission->is_received == 1)
                            {
                                $this->cal_new_commission_row($get_all_money, $branch, $today_date_time);
                            }
                            else{

                                $get_all_money = collect($get_all_money)->groupBy('money_created_date');
                                $get_all_money = $get_all_money->all();
                                if (is_array($get_all_money) && count($get_all_money))
                                {
                                    $total_money_on_day = $this->get_total_money($get_all_money,$branch->user_id);
                                    if ($total_money_on_day > 0)
                                    {

                                        $total_money_on_day_plus_old = ($total_money_on_day + $last_commission->total_day_bills_money);

                                        $get_matched_rule = commission_rules_m::
                                        where("minimum_day_earn","<=",$total_money_on_day_plus_old)->
                                        where("commission_rule_type","=","0")->
                                        where("branch_id",$branch->user_id)
                                            ->orderBy("total_commission_value","desc")->get()->first();

                                        if (is_object($get_matched_rule))
                                        {

                                            #region update commission to branch
                                            day_commission_m::findOrFail($last_commission->day_commission_id)->update([
                                                "commission_id" => $get_matched_rule->commission_id,
                                                "day_date" => date("Y-m-d"),
                                                "user_id" => $branch->user_id,
                                                "commission_value" => $get_matched_rule->branch_commission_value,
                                                "total_day_bills_money" => $total_money_on_day_plus_old,
                                            ]);
                                            #endregion

                                            $action_desc = " الفرع ".$branch->full_name." يستحق عموله بقيمة".$get_matched_rule->branch_commission_value." بتاريخ".$today_date_time;

                                            site_tracking_m::create([
                                                "user_id" => $branch->user_id,
                                                "action_desc" => $action_desc
                                            ]);

                                            notification_m::create([
                                                "not_title" => $action_desc,
                                                "not_type" => "info",
                                                "not_to_userid" => $branch->user_id
                                            ]);

                                        }

                                    }
                                    else{

                                        if (($total_money_on_day * (-1)) >= $last_commission->total_day_bills_money)
                                        {
                                            day_commission_m::findOrFail($last_commission->day_commission_id)->delete();
                                            continue;
                                        }
                                        else{
                                            $total_money_on_day_plus_old = ($last_commission->total_day_bills_money + $total_money_on_day);
                                        }


                                        $get_matched_rule = commission_rules_m::
                                        where("minimum_day_earn","<=",$total_money_on_day_plus_old)
                                            ->where("branch_id",$branch->user_id)
                                            ->where("commission_rule_type","0")
                                            ->orderBy("total_commission_value","desc")->get()->first();

                                        if (is_object($get_matched_rule))
                                        {

                                            #region update commission to branch
                                            day_commission_m::findOrFail($last_commission->day_commission_id)->update([
                                                "commission_id" => $get_matched_rule->commission_id,
                                                "day_date" => date("Y-m-d"),
                                                "user_id" => $branch->user_id,
                                                "commission_value" => $get_matched_rule->branch_commission_value,
                                                "total_day_bills_money" => $total_money_on_day_plus_old,
                                            ]);
                                            #endregion

                                            $action_desc = " الفرع ".$branch->full_name." يستحق عموله بقيمة".$get_matched_rule->branch_commission_value." بتاريخ".$today_date_time;

                                            site_tracking_m::create([
                                                "user_id" => $branch->user_id,
                                                "action_desc" => $action_desc
                                            ]);

                                            notification_m::create([
                                                "not_title" => $action_desc,
                                                "not_type" => "info",
                                                "not_to_userid" => $branch->user_id
                                            ]);

                                        }
                                        else{
                                            day_commission_m::findOrFail($last_commission->day_commission_id)->delete();
                                            continue;
                                        }

                                    }
                                }

                            }

                        }
                        else{

                            // on previous day
                            $get_all_money = money_transfers_m::get_data("
                                AND money.m_t_bill_type = 'client_bill'
                                AND money.m_t_status = 1
                                AND (
                                    money.to_user_id = $branch->user_id 
                                    OR
                                    money.from_user_id = $branch->user_id
                                )
                                AND (money.created_at) >= '$start_day'
                                AND (money.created_at) <= '$end_day' 
                                
                                order by money.created_at asc
                                
                            ");

                            $this->cal_new_commission_row($get_all_money, $branch, $today_date_time);

                        }



                    #endregion

                }
                else{

                    #region get money transfers of client bills

                    $get_all_money = money_transfers_m::get_data("
                            AND money.m_t_bill_type = 'client_bill'
                            AND money.m_t_status = 1
                            AND (
                                money.to_user_id = $branch->user_id 
                                OR
                                money.from_user_id = $branch->user_id
                            )
                            AND (money.created_at) >= '$start_day' 
                            AND (money.created_at) <= '$end_day' 
                            
                            order by money.created_at asc
                            
                        ");

                    #endregion

                    $this->cal_new_commission_row($get_all_money, $branch, $today_date_time);


                }


            }


        }


        if($redirect)
        {
            return Redirect::to('admin/dashboard')->with([
                "msg" => "<div class='alert alert-success'> تم تحديث العمولة بنجاح  </div>"
            ])->send();
        }

    }

    private function cal_new_commission_row($get_all_money, $branch, $today_date_time)
    {

        $get_all_money = collect($get_all_money)->groupBy('money_created_date');
        $get_all_money = $get_all_money->all();
        if (is_array($get_all_money) && count($get_all_money))
        {
            $total_money_on_day = $this->get_total_money($get_all_money,$branch->user_id);
            if ($total_money_on_day > 0)
            {

                $get_matched_rule = commission_rules_m::
                where("minimum_day_earn","<=",$total_money_on_day)
                    ->where("branch_id",$branch->user_id)
                    ->where("commission_rule_type","0")
                    ->orderBy("total_commission_value","desc")->get()->first();

                if (is_object($get_matched_rule))
                {

                    #region set commission to branch
                    $new_commission = day_commission_m::create([
                        "commission_id" => $get_matched_rule->commission_id,
                        "day_date" => date("Y-m-d"),
                        "user_id" => $branch->user_id,
                        "commission_value" => $get_matched_rule->branch_commission_value,
                        "total_day_bills_money" => $total_money_on_day,
                    ]);
                    #endregion

                    $action_desc = " الفرع ".$branch->full_name." يستحق عموله بقيمة".$get_matched_rule->branch_commission_value." بتاريخ".$today_date_time;

                    site_tracking_m::create([
                        "user_id" => $branch->user_id,
                        "action_desc" => $action_desc
                    ]);

                    notification_m::create([
                        "not_title" => $action_desc,
                        "not_type" => "info",
                        "not_to_userid" => $branch->user_id
                    ]);

                }
            }
        }

    }

    private function get_total_money($get_all_money,$branch_id)
    {
        $total_money_on_day = 0;
        foreach($get_all_money as $money_date => $money_items)
        {
            $money_items = collect($money_items)->groupBy('to_user_id');

            foreach($money_items as $money_key => $money_bills)
            {

                if ($money_key == $branch_id)
                {
                    // عمليات بيع
                    $temp_money = convert_inside_obj_to_arr($money_bills,"m_t_amount");
                    $total_money_on_day += array_sum($temp_money);
                }
                else{
                    // عمليات مرتجع
                    $temp_money = convert_inside_obj_to_arr($money_bills,"m_t_amount");
                    $total_money_on_day -= array_sum($temp_money);
                }

            }

        }

        return $total_money_on_day;
    }

    public function get_days_from_range($from_date , $to_date , $get_first_date = false)
    {

        $begin = new \DateTime( "$from_date" );
        if(!$get_first_date)
        {
            $begin = $begin->modify( '+1 day' );
        }

        $end = new \DateTime( "$to_date" );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $daterange_arr = [];
        foreach($daterange as $date)
        {
            $daterange_arr[$date->format("Y-m-d")] = 0;
        }

        return $daterange_arr;
    }

    public function calc_type_2_commission_rules(){

        //get current day
        $current_hour = date("H");

        $get_current_date = date("Y-m-d");

        $current_data=Carbon::now();
        if ($current_hour >= 5)
        {
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day+1, $hour = 4,59);
        }
        else{
            $start_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day-1, $hour = 5);
            $end_day=Carbon::create($year = $current_data->year, $month = $current_data->month, $day = $current_data->day, $hour = 4,59);
        }


        $current_day=$start_day->toDateString();
        $start_day = $start_day->toDateTimeString();
        $end_day = $end_day->toDateTimeString();


        //get all branches
        $all_branches=User::where("user_type","branch")->get()->all();

        foreach($all_branches as $branch_key=>$branch){

            //check if there is a commission today for this branch
            $day_commission_row=day_commission_m::
            select("day_commission.*")->
            join("commission_rules",'commission_rules.commission_id',"=","day_commission.commission_id")->
            where("commission_rules.commission_rule_type","1")->
            where("user_id",$branch->user_id)->
            where("day_date",$current_day)->
            orderBy("day_commission_id","desc")->get()->first();



            //get all bills in current day
            if(is_object($day_commission_row)){
                $all_bills=clients_bills_m::
                where("branch_id",$branch->user_id)->
                whereBetween("client_bill_date",[Carbon::createFromTimestamp(strtotime($day_commission_row->created_at))->addSeconds(1),$end_day])->
                get();
            }
            else{
                $all_bills=clients_bills_m::
                where("branch_id",$branch->user_id)->
                whereBetween("client_bill_date",[$start_day,$end_day])->
                get();
            }



            if(count($all_bills)==0){
                continue;
            }

            //get all products grouped by cat id
            $all_products=
            clients_orders_m::
            join("products as pro","pro.pro_id","=","clients_orders.pro_id")->
            whereIn("client_bill_id",$all_bills->pluck("client_bill_id"))->
            where("is_gift","0")->
            get()->groupBy("cat_id");


            //get all commission rules new type and check if there is rule match or not
            //group by cat id to check which rule of the group will be activate
            $all_rules=commission_rules_m::
            where("branch_id",$branch->user_id)->
            where("commission_rule_type","1")->
            where("commission_rule_cat_ids",">","0")->
            get()->groupBy("commission_rule_cat_ids")->all();


            //get all unreceived day_commissions rows and check if sum of these
            $this_day_commissions=day_commission_m::
            join("commission_rules",'commission_rules.commission_id',"=","day_commission.commission_id")->
            where("commission_rules.commission_rule_type","1")->
            where("user_id",$branch->user_id)->
            where("day_date",$current_day)->
            get()->groupBy("commission_rule_cat_ids")->all();

            $match_rules=[];

            foreach ($all_rules as $cat_id=>$group_rules){

                $this_rule=[];
                if(!isset($all_products[$cat_id])){
                    continue;
                }

                //minimum_day_earn will be considered as number of products
                $group_rules=$group_rules->sortBy("minimum_day_earn");

                foreach($group_rules as $single_rule_key=>$single_rule){

                    $number_of_products_match_rule=$all_products[$cat_id]->pluck("order_quantity")->sum();

                    //check if there is un received rows here and add to $number_of_products_match_rule
                    if(isset($this_day_commissions[$cat_id])){
                        $number_of_products_match_rule=$number_of_products_match_rule+$this_day_commissions[$cat_id]->pluck("total_day_bills_money")->sum();
                    }


                    if($single_rule->minimum_day_earn>$number_of_products_match_rule){
                        break;
                    }

                    $this_rule=[
                        'commission_id'=>$single_rule->commission_id,
                        'day_date'=>$current_day,
                        'user_id'=>$branch->user_id,
                        'commission_value'=>$single_rule->branch_commission_value,
                        'total_day_bills_money'=>$number_of_products_match_rule,
                        'is_received'=>"0",
                        "created_at"=>Carbon::now(),
                        "updated_at"=>Carbon::now()
                    ];

                }


                if(is_array($this_rule)&&count($this_rule)){
                    $match_rules[]=$this_rule;

                    if(isset($this_day_commissions[$cat_id])){
                        //remove $this_day_commissions[$cat_id]
                        day_commission_m::
                        where("user_id",$branch->user_id)->
                        whereIn("day_commission_id",$this_day_commissions[$cat_id]->pluck("day_commission_id")->all())->
                        delete();
                    }
                }
            }



            day_commission_m::insert($match_rules);
        }


    }



}
