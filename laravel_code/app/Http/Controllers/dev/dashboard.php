<?php

namespace App\Http\Controllers\dev;

use App\Http\Controllers\dev_controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class dashboard extends dev_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view("dev.subviews.dashboard",$this->data);
    }
    

}
