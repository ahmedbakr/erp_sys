<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

date_default_timezone_set("Asia/Riyadh");




Route::group(['middleware' => 'check_admin'], function () {


    #region package

        Route::get("admin/packages/show/{start_date?}/{end_date?}",'admin\packages@index');

        Route::get("admin/packages/save/{package_id?}",'admin\packages@save');
        Route::post("admin/packages/save/{package_id?}",'admin\packages@save');

        Route::post("admin/packages/remove_package",'admin\packages@remove_package');



    #endregion

    #region supplier bills

        Route::get('/admin/order/supplier_materials/show_bills/{supplier_id?}/{from_date?}/{to_date?}/{group_by?}','admin\orders@supplier_bills');
        Route::get('/admin/order/supplier_materials/bill/{bill_id}','admin\orders@supplier_materials');


        Route::get('/admin/order/supplier_materials/save_purchase_bill_orders','admin\orders@supplier_materials_save_purchase_bill_orders');
        Route::post('/admin/order/supplier_materials/save_purchase_bill_orders','admin\orders@supplier_materials_save_purchase_bill_orders');

        Route::get('/admin/order/supplier_materials/save_return_bill_orders/{bill_id?}','admin\orders@supplier_materials_save_return_bill_orders');
        Route::post('/admin/order/supplier_materials/save_return_bill_orders/{bill_id?}','admin\orders@supplier_materials_save_return_bill_orders');

        Route::post('/admin/order/supplier_materials/get_bill_orders','admin\orders@get_bill_orders');
        Route::post('/admin/order/supplier_materials/make_return_bill','admin\orders@make_return_bill');

    #endregion

    #region branch bills

    Route::get('/admin/order/branch_orders','admin\orders@show_branch_orders');
    Route::get('/admin/order/show_branch_bill_orders/{bill_id}','admin\orders@show_branch_bill_orders');
    Route::post('/admin/orders/change_bill_status','admin\orders@change_bill_status');
    Route::get('/admin/order/show_branch_bill_returns/{bill_id}','admin\orders@show_branch_bill_returns');
    Route::post('/admin/orders/change_bill_return_status','admin\orders@change_bill_return_status');
    Route::post('/admin/orders/remove_branch_bill','admin\orders@remove_branch_bill');



    #endregion

    #region money_transfer

        Route::get('/admin/money_transfer/{supplier_id?}/{bill_id?}','admin\money_transfer@index');
        Route::post('/admin/money_transfer/get_supplier_data','admin\money_transfer@get_supplier_data');
        Route::post('/admin/money_transfer/change_bill_orders_money_status','admin\money_transfer@change_bill_orders_money_status');

    #endregion

    #region products


    Route::get('/admin/product/products_on_stock','admin\product@products_on_stock');
    Route::get('/admin/product/products_on_branches_stock','admin\product@products_on_branches_stock');

    Route::get('/admin/product/save_products_on_stock/{cat_id}/{pro_id?}','admin\product@save_products_on_stock');
    Route::post('/admin/product/save_products_on_stock/{cat_id}/{pro_id?}','admin\product@save_products_on_stock');
    Route::post('/admin/product/import_products_to_stock/{cat_id}','admin\product@import_products_to_stock');

    Route::post('/admin/product/remove_product_from_stock','admin\product@remove_product_from_stock');

    Route::get('/admin/product/set_products_prices','admin\product@set_products_prices');
    Route::post('/admin/product/set_products_prices','admin\product@set_products_prices');

    Route::post('/admin/product/get_products_prices','admin\product@get_products_prices');

    Route::get('/admin/product/search_for_product','admin\product@search_for_product');
    Route::post('/admin/product/search_for_product','admin\product@search_for_product');

    Route::get('/admin/product/search_for_product_without_quantity','admin\product@search_for_product_without_quantity');
    Route::post('/admin/product/search_for_product_without_quantity','admin\product@search_for_product_without_quantity');



//    Route::get('/admin/product/save_branch_product_prices/{branch_id}/{pro_id}','admin\product@save_branch_product_prices');
//    Route::post('/admin/product/save_branch_product_prices/{branch_id}/{pro_id}','admin\product@save_branch_product_prices');


    Route::get('/admin/product/broken_products_on_stock','admin\product@broken_products_on_stock');
    Route::get('/admin/product/save_broken_products_on_stock','admin\product@save_broken_products_on_stock');
    Route::post('/admin/product/save_broken_products_on_stock','admin\product@save_broken_products_on_stock');

//    Route::get('/admin/product/save_produce_product','admin\product@save_produce_product');
//    Route::post('/admin/product/save_produce_product','admin\product@save_produce_product');

//    Route::get('/admin/product/save_reverse_produce_product','admin\product@save_reverse_produce_product');
//    Route::post('/admin/product/save_reverse_produce_product','admin\product@save_reverse_produce_product');

//    Route::get('/admin/product/review_produce_products','admin\product@review_produce_products');
//    Route::get('/admin/product/produce_product_details/{ppr_id}','admin\product@produce_product_details');
//    Route::post('/admin/product/change_produce_product_status','admin\product@change_produce_product_status');
//    Route::post('/admin/product/remove_product_produce_product_request','admin\product@remove_product_produce_product_request');

    #endregion

    #region prices
        Route::get('/admin/prices','admin\price_list@index');
        Route::get('/admin/prices/save_price/{price_id?}','admin\price_list@save_price');
        Route::post('/admin/prices/save_price/{price_id?}','admin\price_list@save_price');
        Route::post('/admin/prices/remove_price','admin\price_list@remove_price');
    #endregion


    #region reports2

    Route::get("/admin/reports/load_old_coupons/{offset?}",function($offset=0){

        //get all bills to get coupons and save them in client_bill_used_coupon table
        $all_bills=\App\models\bills\clients_bills_m::get_clients_bills(" limit 500 offset $offset ");

        dump(count($all_bills));

        $used_coupons=[];

        foreach($all_bills as $bill){

            $bill_coupons=$bill->bill_coupon;
            $bill_coupons=json_decode($bill_coupons);


            $bill_coupons=array_map(function($item){
                $item=explode("-",$item);
                return trim($item[0]);
            },$bill_coupons);


            if(!isset_and_array($bill_coupons)){
                continue;
            }


            $bill_coupons_objs=\App\models\coupons_m::whereIn("coupon_code",$bill_coupons)->get()->all();

            if(!isset_and_array($bill_coupons_objs)){
                continue;
            }

            $all_inserted_before=\App\models\client_bill_used\client_bill_used_coupon_m::all()->groupBy("coupon_id");

            foreach($bill_coupons_objs as $obj){

                if(isset($all_inserted_before[$obj->coupon_id])){
                    continue;
                }

                $used_coupons[]=[
                    'client_bill_id'=>$bill->client_bill_id,
                    'coupon_id'=>$obj->coupon_id,
                    'coupon_cat_id'=>$obj->cat_id,
                    'used_at'=>$bill->client_bill_date_only
                ];
            }

        }


        if(isset_and_array($used_coupons)){
            \App\models\client_bill_used\client_bill_used_coupon_m::insert($used_coupons);
        }


    });

    Route::get("/admin/reports/load_old_packages/{offset?}",function($offset=0){

        //get all bills to get coupons and save them in client_bill_used_package table
        $all_bills=\App\models\bills\clients_bills_m::get_clients_bills(" limit 500 offset $offset ");
        dump(count($all_bills));

        $used_packages=[];

        $all_used_packages=\App\models\client_bill_used\client_bill_used_package_m::all()->groupBy("client_bill_id")->all();

        foreach($all_bills as $key=>$bill){

            if(isset($all_used_packages[$bill->client_bill_id])){
                continue;
            }

            $bill_packages=$bill->bill_packages;
            $bill_packages=json_decode($bill_packages);

            if(!isset_and_array($bill_packages)){
                continue;
            }

            //get packages by name
            $bill_packages=array_map(function($item){
                return explode("-",$item);
            },$bill_packages);

            $bill_packages_names=[];

            foreach($bill_packages as $pac){

                if(!isset_and_array($pac)){
                    continue;
                }

                $bill_packages_names[]=trim($pac[0]);
            }


            $bill_packages_objs=\App\models\packages\packages_m::whereIn('package_name',$bill_packages_names)->get()->all();

            if(!isset_and_array($bill_packages_objs)){
                continue;
            }


            foreach($bill_packages_objs as $pac_obj){


                $used_packages[]=[
                    'client_bill_id'=>$bill->client_bill_id,
                    'package_id'=>$pac_obj->package_id,
                    'used_at'=>$bill->client_bill_date_only
                ];
            }

        }


        if(isset_and_array($used_packages)){
            \App\models\client_bill_used\client_bill_used_package_m::insert($used_packages);
        }

        dump("done");
    });


    Route::get("/admin/fix_related_id",function(){

        $users=\App\User::
        where("user_type","customer")->
        where("related_id","0")->
        limit(300)->
        get()->all();

        foreach($users as $user){

            //get first bill of this user and get branch id from it the update this user
            $bill=\App\models\bills\clients_bills_m::
            where("customer_id",$user->user_id)->
            orderBy("client_bill_id","asc")->
            get()->first();

            if(!is_object($bill)){
                continue;
            }

            $user->update([
                "related_id"=>$bill->branch_id
            ]);

        }


        dump("done");
    });




    Route::get("/admin/reports/report_branch_orders",'admin\reports2@report_branch_orders');
    Route::get("/admin/reports/report_branch_transfers",'admin\reports2@report_branch_transfers');
    Route::get("/admin/reports/report_permission_bills",'admin\reports2@report_permission_bills');
    Route::get("/admin/reports/report_clients_data",'admin\reports2@report_clients_data');
    Route::get("/admin/reports/report_clients_products",'admin\reports2@report_clients_products');
    Route::get("/admin/reports/most_products_bought",'admin\reports2@most_products_bought');
    Route::get("/admin/reports/report_products_bought",'admin\reports2@report_products_bought');



    #endregion

});


