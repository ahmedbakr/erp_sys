<?php

Route::group(['middleware' => 'check_login'], function () {



    #region expenses

        Route::get('common/expenses/show_all_expenses', 'common\expenses@show_all_expenses');

        Route::get('common/expenses/save_expenses/{expenses_id?}', 'common\expenses@save_expenses');
        Route::post('common/expenses/save_expenses/{expenses_id?}', 'common\expenses@save_expenses');

        Route::post('common/expenses/remove_expenses', 'common\expenses@remove_expenses');

        Route::post('common/expenses/change_expense_status', 'common\expenses@change_expense_status');
    #endregion

    #region category

        Route::get("common/category/show_tree/{just_load_data?}/{general?}",'common\category@category_tree');
        Route::post("common/category/show_tree/{just_load_data?}/{general?}",'common\category@category_tree');


        Route::get("common/category/save/{cat_type}/{parent_id}/{cat_id?}",'common\category@save_category');
        Route::post("common/category/save/{cat_type}/{parent_id}/{cat_id?}",'common\category@save_category');

        Route::get("common/category/move_category_to_another_parent/{cat_type}/{cat_id}",'common\category@move_category_to_another_parent');
        Route::post("common/category/move_category_to_another_parent/{cat_type}/{cat_id}",'common\category@move_category_to_another_parent');

        Route::post("common/category/remove_category",'common\category@remove_category');

    #endregion





});