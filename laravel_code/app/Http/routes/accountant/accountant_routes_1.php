<?php

Route::group(['middleware' => 'check_accountant'], function () {


    Route::get('accountant/dashboard', 'accountant\dashboard@index');


    Route::get("accountant/general_account_category/show_tree/{just_load_data?}/{general?}","is_accountant_controller@show_tree");
    Route::post("accountant/general_account_category/show_tree/{just_load_data?}/{general?}","is_accountant_controller@show_tree");

    #region account_category

        Route::get("accountant/general_account_category/save/{cat_type}/{parent_id}/{cat_id?}",'accountant\general_account_category@save_category');
        Route::post("accountant/general_account_category/save/{cat_type}/{parent_id}/{cat_id?}",'accountant\general_account_category@save_category');

        Route::get("accountant/general_account_category/move_category_to_another_parent/{cat_type}/{cat_id}",'accountant\general_account_category@move_category_to_another_parent');
        Route::post("accountant/general_account_category/move_category_to_another_parent/{cat_type}/{cat_id}",'accountant\general_account_category@move_category_to_another_parent');


        Route::post("accountant/general_account_category/remove_general_account_category",'accountant\general_account_category@remove_general_account_category');
    #endregion

    #region account

        Route::get("accountant/general_account/save/{acc_type}/{cat_id}/{acc_id?}",'accountant\general_accounts@save_general_account');
        Route::post("accountant/general_account/save/{acc_type}/{cat_id}/{acc_id?}",'accountant\general_accounts@save_general_account');


        Route::get("accountant/general_account/move_account_to_another_parent/{acc_type}/{acc_id}",'accountant\general_accounts@move_account_to_another_category');
        Route::post("accountant/general_account/move_account_to_another_parent/{acc_type}/{acc_id}",'accountant\general_accounts@move_account_to_another_category');

//        Route::post("accountant/general_account/remove_general_account",'accountant\general_accounts@remove_general_account');
    #endregion

    #region financial year

        Route::get("accountant/financial_year/show_all_financial_years",'accountant\financial_year@show_all_financial_years');

        Route::get("accountant/financial_year/save_financial_year/{fin_id?}",'accountant\financial_year@save_financial_year');
        Route::post("accountant/financial_year/save_financial_year/{fin_id?}",'accountant\financial_year@save_financial_year');

        Route::get("accountant/financial_year/close_financial_year/{fin_id?}",'accountant\financial_year@close_financial_year');
        Route::post("accountant/financial_year/close_financial_year/{fin_id?}",'accountant\financial_year@close_financial_year');

    #endregion

    #region entry

    Route::get("accountant/entry/show_entries",'accountant\entry@show_entries');
    Route::get("accountant/entry/show_entry_items/{entry_id}",'accountant\entry@show_entry_items');

    Route::post("accountant/entry/approve_entry",'accountant\entry@approve_entry');

    Route::get("accountant/entry/add_entry",'accountant\entry@add_entry_view');
    Route::post("accountant/entry/add_entry",'accountant\entry@add_entry_view');

    Route::post("accountant/entry/get_template_items",'accountant\entry@get_template_items');





    #endregion

    #region entry_templates

    Route::get("accountant/entry/show_all_templates",'accountant\entry_templates@show_all_templates');
    Route::get("accountant/entry/show_template_items",'accountant\entry_templates@show_template_items');

    Route::get("accountant/entry/show_template_items/{template_id}",'accountant\entry_templates@show_template_items');


    Route::get("accountant/entry/add_template",'accountant\entry_templates@add_template');
    Route::post("accountant/entry/add_template",'accountant\entry_templates@add_template');

    Route::post("accountant/entry/remove_template",'accountant\entry_templates@remove_template');

    Route::post("accountant/entry/get_template_items_for_template",'accountant\entry_templates@get_template_items_for_template');


    #endregion


    #region expenses
        Route::get('accountant/expenses/show_all_expenses_categories', 'accountant\expenses@show_all_expenses_categories');

        Route::get('accountant/expenses/save_expenses_type/{expenses_type_id?}', 'accountant\expenses@save_expenses_type');
        Route::post('accountant/expenses/save_expenses_type/{expenses_type_id?}', 'accountant\expenses@save_expenses_type');

        Route::get('accountant/expenses/show_all_expenses/{expenses_type_id?}/{expenses_date_from?}/{expenses_date_to?}/{group_by?}', 'accountant\expenses@show_all_expenses');

        Route::get('accountant/expenses/save_expenses/{expenses_id?}', 'accountant\expenses@save_expenses');
        Route::post('accountant/expenses/save_expenses/{expenses_id?}', 'accountant\expenses@save_expenses');

        Route::post('accountant/expenses/remove_expenses', 'accountant\expenses@remove_expenses');

        Route::post('accountant/expenses/change_expense_status', 'accountant\expenses@change_expense_status');
    #endregion



});

