<?php

Route::group(['middleware' => 'check_accountant'], function () {


    #region assets category

    Route::get('accountant/assets/category/show/{parent_id?}', 'accountant\assets\category@index');
    Route::get('accountant/assets/category/save/{asset_cat_id?}', 'accountant\assets\category@save');
    Route::post('accountant/assets/category/save/{asset_cat_id?}', 'accountant\assets\category@save');
    Route::post('accountant/assets/category/remove', 'accountant\assets\category@remove');

    #endregion


    #region assets locations

    Route::get('accountant/assets/locations/show', 'accountant\assets\locations@index');
    Route::get('accountant/assets/locations/save/{asset_location_id?}', 'accountant\assets\locations@save');
    Route::post('accountant/assets/locations/save/{asset_location_id?}', 'accountant\assets\locations@save');
    Route::post('accountant/assets/locations/remove', 'accountant\assets\locations@remove');

    #endregion


    #region assets status

    Route::get('accountant/assets/status/show', 'accountant\assets\status@index');
    Route::get('accountant/assets/status/save/{asset_location_id?}', 'accountant\assets\status@save');
    Route::post('accountant/assets/status/save/{asset_location_id?}', 'accountant\assets\status@save');
    Route::post('accountant/assets/status/remove', 'accountant\assets\status@remove');

    #endregion


    #region assets

    Route::get('accountant/assets', 'accountant\assets\assets@index');
    Route::get('accountant/assets/save/{asset_id?}', 'accountant\assets\assets@save');
    Route::post('accountant/assets/save/{asset_id?}', 'accountant\assets\assets@save');
    Route::post('accountant/assets/remove', 'accountant\assets\assets@remove');
    Route::post('accountant/assets/assets/get_years_in_range', 'accountant\assets\assets@get_years_in_range');

    #endregion


    #region documents

    Route::get('accountant/assets/documents/save/{doc_id?}', 'accountant\assets\documents@save');
    Route::get('accountant/assets/documents/{asset_id}', 'accountant\assets\documents@index');
    Route::post('accountant/assets/documents/save/{doc_id?}', 'accountant\assets\documents@save');
    Route::post('accountant/assets/documents/remove', 'accountant\assets\documents@remove');

    #endregion

    #region installments

    Route::get('accountant/assets/installments/save/{asset_istall_id?}', 'accountant\assets\installments@save');
    Route::get('accountant/assets/installments', 'accountant\assets\installments@index');
    Route::post('accountant/assets/installments/save/{asset_istall_id?}', 'accountant\assets\installments@save');
    Route::post('accountant/assets/installments/remove', 'accountant\assets\installments@remove');

    Route::get('accountant/assets/installments/save_generated_installments', 'accountant\assets\installments@save_generated_installments');
    Route::post('accountant/assets/installments/save_generated_installments', 'accountant\assets\installments@save_generated_installments');

    Route::get('accountant/assets/installments/pay_get_installment/{asset_install_id}', 'accountant\assets\installments@pay_get_installment');
    Route::post('accountant/assets/installments/pay_get_installment/{asset_install_id}', 'accountant\assets\installments@pay_get_installment');

    #endregion


    #region assets_sold

    Route::get('accountant/assets/assets_sold', 'accountant\assets\assets_sold@index');
    Route::get('accountant/assets/assets_sold/save', 'accountant\assets\assets_sold@save');
    Route::post('accountant/assets/assets_sold/save', 'accountant\assets\assets_sold@save');

    #endregion


    #region assets_sold

    Route::get('accountant/assets/assets_rubbish', 'accountant\assets\assets_rubbish@index');
    Route::get('accountant/assets/assets_rubbish/save', 'accountant\assets\assets_rubbish@save');
    Route::post('accountant/assets/assets_rubbish/save', 'accountant\assets\assets_rubbish@save');
    Route::post('accountant/assets/assets_rubbish/remove', 'accountant\assets\assets_rubbish@remove');

    #endregion

    #region assets_on_maintenance

    Route::get('accountant/assets/assets_on_maintenance', 'accountant\assets\assets_on_maintenance@index');
    Route::get('accountant/assets/assets_on_maintenance/save/{asset_o_m_id?}', 'accountant\assets\assets_on_maintenance@save');
    Route::post('accountant/assets/assets_on_maintenance/save/{asset_o_m_id?}', 'accountant\assets\assets_on_maintenance@save');
    Route::post('accountant/assets/assets_on_maintenance/remove', 'accountant\assets\assets_on_maintenance@remove');
    Route::post('accountant/assets/assets_on_maintenance/is_maintenance_finish', 'accountant\assets\assets_on_maintenance@is_maintenance_finish');

    #endregion


    #region assets_transports

    Route::get('accountant/assets/assets_transports', 'accountant\assets\assets_transports@index');
    Route::get('accountant/assets/assets_transports/save/{asset_transport_id?}', 'accountant\assets\assets_transports@save');
    Route::post('accountant/assets/assets_transports/save/{asset_transport_id?}', 'accountant\assets\assets_transports@save');

    #endregion


    #region asset_expenses_and_incomes

    Route::get('accountant/assets/asset_expenses_and_incomes/{expense_or_income}', 'accountant\assets\asset_expenses_and_incomes@index');
    Route::get('accountant/assets/asset_expenses_and_incomes/save/{expense_or_income}', 'accountant\assets\asset_expenses_and_incomes@save');
    Route::post('accountant/assets/asset_expenses_and_incomes/save/{expense_or_income}', 'accountant\assets\asset_expenses_and_incomes@save');
    Route::post('accountant/assets/asset_expenses_and_incomes/remove', 'accountant\assets\asset_expenses_and_incomes@remove');

    #endregion


    #region assets_rent

    Route::get('accountant/assets/assets_rent/', 'accountant\assets\assets_rent@index');
    Route::get('accountant/assets/assets_rent/save', 'accountant\assets\assets_rent@save');
    Route::post('accountant/assets/assets_rent/save', 'accountant\assets\assets_rent@save');
    Route::post('accountant/assets/assets_rent/remove', 'accountant\assets\assets_rent@remove');

    #endregion


    #region assets_deprecation

    Route::get('accountant/assets/assets_deprecation/', 'accountant\assets\assets_deprecation@index');
    Route::post('accountant/assets/assets_deprecation/calc_deprecation', 'accountant\assets\assets_deprecation@calc_deprecation');

    #endregion


    #region assets_that_reevaluated

    Route::get('accountant/assets/assets_that_reevaluated/', 'accountant\assets\assets_that_reevaluated@index');
    Route::get('accountant/assets/assets_that_reevaluated/save', 'accountant\assets\assets_that_reevaluated@save');
    Route::post('accountant/assets/assets_that_reevaluated/save', 'accountant\assets\assets_that_reevaluated@save');

    #endregion

});

?>