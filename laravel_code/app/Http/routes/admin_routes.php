<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

date_default_timezone_set("Asia/Riyadh");




Route::group(['middleware' => 'check_admin'], function () {

    Route::get('admin/dashboard', 'admin\dashboard@index');

    #region materials & status

    Route::get('/admin/materials','admin\settings@show_materials');
    Route::get('/admin/materials/save/{mat_id?}','admin\settings@save_material');
    Route::post('/admin/materials/save/{mat_id?}','admin\settings@save_material');
    Route::get('/admin/status/save/{status_id?}','admin\settings@save_status');
    Route::post('/admin/status/save/{status_id?}','admin\settings@save_status');

    #endregion


    #region users

    Route::get('/admin/suppliers/profile/{supplier_id}','admin\users@supplier_profile');

    #endregion


    #region site_tracking

    Route::get('/admin/tracking','admin\site_tracking@index');
    Route::post('/admin/tracking','admin\site_tracking@index');

    #endregion


    #region money_transfer

//    Route::get('/admin/money_transfer','admin\money_transfer@index');
//    Route::post('/admin/money_transfer/get_supplier_data','admin\money_transfer@get_supplier_data');
//    Route::post('/admin/money_transfer/change_bill_orders_money_status','admin\money_transfer@change_bill_orders_money_status');

    #endregion


    #region Admin Orders Routing

//    Route::get('/admin/order/supplier_materials/save_purchase_bill_orders','admin\orders@supplier_materials_save_purchase_bill_orders');
//    Route::post('/admin/order/supplier_materials/save_purchase_bill_orders','admin\orders@supplier_materials_save_purchase_bill_orders');
//
//    Route::get('/admin/order/supplier_materials/save_return_bill_orders','admin\orders@supplier_materials_save_return_bill_orders');
//    Route::post('/admin/order/supplier_materials/save_return_bill_orders','admin\orders@supplier_materials_save_return_bill_orders');
//
//    Route::post('/admin/order/supplier_materials/get_bill_orders','admin\orders@get_bill_orders');
//    Route::post('/admin/order/supplier_materials/make_return_bill','admin\orders@make_return_bill');
//
//    Route::get('/admin/order/branch_orders','admin\orders@show_branch_orders');
//    Route::get('/admin/order/show_branch_bill_orders/{bill_id}','admin\orders@show_branch_bill_orders');
//    Route::post('/admin/orders/change_bill_status','admin\orders@change_bill_status');
//    Route::get('/admin/order/show_branch_bill_returns/{bill_id}','admin\orders@show_branch_bill_returns');
//    Route::post('/admin/orders/change_bill_return_status','admin\orders@change_bill_return_status');
//
//    Route::get('/admin/order','admin\orders@index');
//    Route::get('/admin/order/supplier_materials/{bill_id?}/{status_id?}','admin\orders@supplier_materials');

    #endregion


    #region products

//    Route::get('/admin/product/product_materials','admin\product@product_materials');
//    Route::get('/admin/product/save_product_materials/{pro_mat_id?}','admin\product@save_product_materials');
//    Route::post('/admin/product/save_product_materials/{pro_mat_id?}','admin\product@save_product_materials');
//    Route::post('/admin/product/remove_product_materials','admin\product@remove_product_materials');
//
//
//    Route::get('/admin/product/broken_product_materials','admin\product@broken_product_materials');
//    Route::get('/admin/product/save_broken_product_materials','admin\product@save_broken_product_materials');
//    Route::post('/admin/product/save_broken_product_materials','admin\product@save_broken_product_materials');
//
//
//    Route::get('/admin/product/products_on_stock','admin\product@products_on_stock');
//    Route::get('/admin/product/save_products_on_stock/{pro_id?}','admin\product@save_products_on_stock');
//    Route::post('/admin/product/save_products_on_stock/{pro_id?}','admin\product@save_products_on_stock');
//    Route::get('/admin/product/save_branch_product_prices/{branch_id}/{pro_id}','admin\product@save_branch_product_prices');
//    Route::post('/admin/product/save_branch_product_prices/{branch_id}/{pro_id}','admin\product@save_branch_product_prices');
//
//
//    Route::get('/admin/product/broken_products_on_stock','admin\product@broken_products_on_stock');
//    Route::get('/admin/product/save_broken_products_on_stock','admin\product@save_broken_products_on_stock');
//    Route::post('/admin/product/save_broken_products_on_stock','admin\product@save_broken_products_on_stock');
//
//    Route::get('/admin/product/save_produce_product','admin\product@save_produce_product');
//    Route::post('/admin/product/save_produce_product','admin\product@save_produce_product');
//
//    Route::get('/admin/product/save_reverse_produce_product','admin\product@save_reverse_produce_product');
//    Route::post('/admin/product/save_reverse_produce_product','admin\product@save_reverse_produce_product');
//
//    Route::get('/admin/product/review_produce_products','admin\product@review_produce_products');
//    Route::get('/admin/product/produce_product_details/{ppr_id}','admin\product@produce_product_details');
//    Route::post('/admin/product/change_produce_product_status','admin\product@change_produce_product_status');
//    Route::post('/admin/product/remove_product_produce_product_request','admin\product@remove_product_produce_product_request');

    #endregion



    #region product_barcode

        Route::get('/admin/products_barcode','admin\barcode@index');

        Route::get('/admin/products_barcode/save/{barcode_id?}','admin\barcode@save_barcode');
        Route::post('/admin/products_barcode/save/{barcode_id?}','admin\barcode@save_barcode');
        Route::post('/admin/product_barcode/remove_barcode','admin\barcode@remove_barcode');

        Route::get('/admin/products_barcode/print/{barcode_id}','admin\barcode@print_barcode');
        Route::post('/admin/products_barcode/print/{barcode_id}','admin\barcode@print_barcode');
        Route::get('/admin/barcode/get_barcode_img/{code}','admin\barcode@barcode');

    #endregion


    #region Invoices


    // template
    Route::get('/admin/invoices_templates','admin\invoices_templates@index');

    Route::get('/admin/invoices_templates/save_template/{inv_temp_id?}','admin\invoices_templates@save_template');
    Route::post('/admin/invoices_templates/save_template/{inv_temp_id?}','admin\invoices_templates@save_template');
    Route::post('/admin/invoices_templates/change_template_default','admin\invoices_templates@change_template_default');

    Route::post('/admin/invoices_templates/remove_template/','admin\invoices_templates@remove_template');


    #template item
    Route::get('/admin/invoices_templates/show_invoice_items/{inv_temp_id}','admin\invoices_templates@show_invoice_items');

    Route::get('/admin/invoices_templates/save_template_item/{inv_temp_id}/{item_id?}','admin\invoices_templates@save_template_item');
    Route::post('/admin/invoices_templates/save_template_item/{inv_temp_id}/{item_id?}','admin\invoices_templates@save_template_item');

    Route::post('/admin/invoices_templates/change_template_item_status/','admin\invoices_templates@change_template_item_status');
    Route::post('/admin/invoices_templates/remove_template_item/','admin\invoices_templates@remove_template_item');


    #template prepare to print

    Route::get('/admin/invoices_templates/preview_template_items/{inv_temp_id}','admin\invoices_templates@preview_template_items');
    Route::post('/admin/invoices_templates/save_template_items_position','admin\invoices_templates@save_template_items_position');

    #endregion



    #region cheques

    Route::get('/admin/cheques','admin\cheques@index');

    Route::get('/admin/cheques/save_template/{cheque_template_id?}','admin\cheques@save_template');
    Route::post('/admin/cheques/save_template/{cheque_template_id?}','admin\cheques@save_template');
    Route::post('/admin/cheques/change_template_default','admin\cheques@change_template_default');

    Route::post('/admin/cheques/remove_template/','admin\cheques@remove_template');

    #template item
    Route::get('/admin/cheques/show_cheque_items/{cheque_template_id}','admin\cheques@show_cheque_items');

    Route::post('/admin/cheques/change_template_item_status/','admin\cheques@change_template_item_status');
    Route::post('/admin/cheques/remove_template_item/','admin\cheques@remove_template_item');

    #template prepare to print

    Route::get('/admin/cheques/preview_template_items/{cheque_template_id}','admin\cheques@preview_template_items');
    Route::post('/admin/cheques/save_template_items_position','admin\cheques@save_template_items_position');



    #endregion


    #region banks

    Route::get('/admin/banks','admin\banks@index');
    Route::get('/admin/banks/save_bank/{bank_id?}','admin\banks@save_bank');
    Route::post('/admin/banks/save_bank/{bank_id?}','admin\banks@save_bank');
    Route::post('/admin/banks/remove_bank','admin\banks@remove_bank');

    #endregion


    #region branch_target

    Route::get('/admin/branch_target','admin\branch_target@index');
    Route::get('/admin/branch_target/save_target/{branch_target_id?}','admin\branch_target@save_target');
    Route::post('/admin/branch_target/save_target/{branch_target_id?}','admin\branch_target@save_target');
    Route::post('/admin/branch_target/remove_branch_target','admin\branch_target@remove_branch_target');

    Route::get('admin/branch_target/branch_target/get_branch_target', 'admin\branch_target@branch_target');

    #endregion


    #region deposite_money

    Route::get('/admin/deposite_money','admin\deposite_money@index');
    Route::get('/admin/deposite_money/filter/{branch_id}/{from_date}/{to_date}','admin\deposite_money@filter_results');
    Route::post('/admin/deposite_money/remove_deposite_money','admin\deposite_money@remove_deposite_money');
    Route::post('/admin/deposite_money/cal_original_deposite_money','admin\deposite_money@cal_original_deposite_money');


    Route::get('/admin/deposite_money/save_deposite_money','admin\deposite_money@save_deposite_money');
    Route::post('/admin/deposite_money/save_deposite_money','admin\deposite_money@save_deposite_money');


    #endregion


    #region Start Commissions

    Route::get('/admin/commissions','admin\commissions@index');
    Route::get('/admin/commissions/filter/{branch_id}/{from_date}/{to_date}','admin\commissions@filter_results');

    Route::get('/admin/commissions/show_rules/{commission_id?}','admin\commissions@show_rules');

    Route::get('/admin/commissions/save_commission_rule/{commission_id?}','admin\commissions@save_commission_rule');
    Route::post('/admin/commissions/save_commission_rule/{commission_id?}','admin\commissions@save_commission_rule');

    Route::post('/admin/commissions/remove_day_commission','admin\commissions@remove_day_commission');
    Route::post('/admin/commissions/remove_commission_rule','admin\commissions@remove_commission_rule');

    Route::post('/admin/commissions/change_commission_received','admin\commissions@change_commission_received');

    #endregion Admin Commissions


    // Start notifications

    Route::get('/admin/notifications','admin\notifications@index');

    // End notifications


//    // Start Admin Category Routing
//
//    Route::get('/admin/category/{cat_type?}/{parent_id?}','admin\category@index')->where('parent_id', '[0-9]+');
//
//    Route::get('/admin/category/save_cat/{cat_type?}','admin\category@save_cat');
//    Route::get('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');
//    Route::post('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');
//    Route::post('/admin/category/check_validation_for_save_cat/{cat_id?}','admin\category@check_validation_for_save_cat');
//
//    // End Admin Category Routing


    // Start Admin Langs Routing

    Route::get('/admin/langs','admin\langs@index');
    Route::get('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');
    Route::post('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');

    // End Admin Langs Routing


    // Start Admin coupons Routing
    Route::get('/admin/coupons/cats','admin\coupons@show_all_coupon_cats');
    Route::get('/admin/coupons/cats/save_cat/{cat_id?}','admin\coupons@save_coupon_cat');
    Route::post('/admin/coupons/cats/save_cat/{cat_id?}','admin\coupons@save_coupon_cat');
    Route::post('/admin/coupons/remove_coupons_cat','admin\coupons@remove_coupons_cat');

    Route::get('/admin/coupons','admin\coupons@index');
    Route::get('/admin/coupons/save_coupon/{coupon_id?}','admin\coupons@save_coupon');
    Route::post('/admin/coupons/save_coupon/{coupon_id?}','admin\coupons@save_coupon');

    Route::post('/admin/coupons/import_coupons','admin\coupons@import_coupons');
    Route::post('/admin/coupons/remove_coupon','admin\coupons@remove_coupon');
    // End Admin coupons Routing


    #region Start Admin review_client_bills

    Route::get('/admin/review_client_bills','admin\client_bills@index');
    Route::get('/admin/review_client_bills/filter/{branch_id}/{from_date}/{to_date}','admin\client_bills@filter_results');

    Route::post('/admin/review_client_bills/bill_is_reviewed','admin\client_bills@bill_is_reviewed');
    Route::post('/admin/review_client_bills/set_atm_ratio','admin\client_bills@set_atm_ratio');

    #endregion



    // Start Admin users Routing

//    Route::get('/admin/users/save_user/{user_id?}','admin\users@save_user');
//    Route::post('/admin/users/save_user/{user_id?}','admin\users@save_user');
//    Route::get('/admin/users/{user_type?}','admin\users@index');


    Route::post('/admin/users/check_validation_for_save_user/{user_id?}','admin\users@check_validation_for_save_user');

    // End Admin users Routing


    // Start Admin support_messages Routing

    Route::get('/admin/support_messages','admin\support_messages@index');
    Route::get('/admin/trip_booking','admin\trip_booking@index');

    // End Admin support_messages Routing


    // Start Admin subscribe Routing

    Route::get('/admin/subscribe','admin\subscribe@index');
    Route::post('/admin/subscribe/send_custom_email','admin\subscribe@send_custom_email');
    Route::post('/admin/subscribe/send_all_subscribers_email','admin\subscribe@send_all_subscribers_email');
    Route::get('/admin/subscribe/stop','admin\subscribe@stop');
    Route::get('/admin/subscribe/pause','admin\subscribe@pause');
    Route::get('/admin/subscribe/resume','admin\subscribe@resume');

    Route::get('/admin/subscribe/save_email','admin\subscribe@save_email');
    Route::post('/admin/subscribe/save_email','admin\subscribe@save_email');

    Route::get('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::post('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::get('/admin/subscribe/export_subscribe','admin\subscribe@export_subscribe');

    Route::get('/admin/backup_restore/backup','admin\backup_restore@backup');

    Route::get('/subscribe_cron_jop','subscribe_cron_jop@index');
    Route::get('/subscribe_cron_jop/show_email','subscribe_cron_jop@show_email');


    // End Admin subscribe Routing


    // Start menus Routing
    Route::get('/admin/menus','admin\menus@index');
    Route::get('/admin/menus/save_menu/{lang_id?}/{menu_id?}','admin\menus@get_menu');
    Route::post('/admin/menus/save_sortable_menu','admin\menus@save_sortable_menu');
    // End menus Routing
    #region manage ads

    Route::get("/admin/ads","admin\ads@index");
    Route::get("/admin/ads/save_ad/{ad_id?}","admin\ads@save_ad");
    Route::post("/admin/ads/save_ad/{ad_id?}","admin\ads@save_ad");

    #endregion



    //assets
//    Route::get('admin/assets/show_all_assets', 'admin\assets@show_all_assets');
//
//    Route::get('admin/assets/save_asset/{asset_id?}', 'admin\assets@save_asset');
//    Route::post('admin/assets/save_asset/{asset_id?}', 'admin\assets@save_asset');
//
//    Route::post('admin/assets/calc_years_of_asset', 'admin\assets@calc_years_of_asset');
//
//    Route::post('admin/assets/remove_asset', 'admin\assets@remove_asset');
    //END assets





    //users
    Route::get('admin/users/get_all/{user_type}', 'admin\users@get_all');
    Route::get('admin/users/export_excel', 'admin\users@export_excel');

    Route::get('admin/users/save/{user_type}/{user_id?}', 'admin\users@save_user');
    Route::post('admin/users/save/{user_type}/{user_id?}', 'admin\users@save_user');
    Route::post('admin/users/import_customers', 'admin\users@import_customers');

    Route::get('admin/users/change_password', 'admin\users@change_password');
    Route::post('admin/users/change_password', 'admin\users@change_password');

    Route::get('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');
    Route::post('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');

    Route::get('admin/users/clients/client_profile/{user_id}', 'admin\users@client_profile');
    Route::get('admin/users/clients/money_transfers/{money_transfer_id}', 'admin\users@money_transfers');
    Route::post('admin/users/clients/money_transfers/{money_transfer_id}', 'admin\users@money_transfers');


    Route::post('admin/users/make_sponsor_zero', 'admin\users@make_sponsor_zero');
    Route::post('admin/users/remove_customer', 'admin\users@remove_customer');

    //END users


    //duty_documents
    Route::get('admin/duty_documents/{send_or_recieve}', 'admin\duty_documents@index');

    Route::get('admin/duty_documents/save/new', 'admin\duty_documents@save');
    Route::post('admin/duty_documents/save/new', 'admin\duty_documents@save');
    Route::post('admin/duty_documents/change_duty_document_received', 'admin\duty_documents@change_duty_document_received');

    //END duty_documents


    //users_transits
    Route::get('admin/users_transits', 'admin\users_transits@index');

    Route::get('admin/users_transits/save', 'admin\users_transits@save');
    Route::post('admin/users_transits/save', 'admin\users_transits@save');

    //END users_transits


    //payment_methods
    Route::get('admin/payment_methods', 'admin\payment_methods@index');

    Route::get('admin/payment_methods/save/{payment_method_id?}', 'admin\payment_methods@save');
    Route::post('admin/payment_methods/save/{payment_method_id?}', 'admin\payment_methods@save');

    //END payment_methods


    //reports
    Route::get('admin/reports/total_branches_clients_bills', 'admin\reports@total_branches_clients_bills');
    Route::get('admin/reports/total_cashiers_clients_bills', 'admin\reports@total_cashiers_clients_bills');

    Route::get('admin/reports/show_bill_orders/{bill_id}', 'admin\reports@show_bill_orders');
    Route::get('admin/reports/sell_bills', 'admin\reports@sell_bills');
    Route::get('admin/reports/sell_bills_session', 'admin\reports@sell_bills_session');
    Route::get('admin/reports/sell_bills_depend_on_customer', 'admin\reports@sell_bills_depend_on_customer');
    Route::get('admin/reports/sell_bills_depend_on_branch', 'admin\reports@sell_bills_depend_on_branch');
    Route::get('admin/reports/sell_bills_depend_on_cashier', 'admin\reports@sell_bills_depend_on_cashier');
    Route::get('admin/reports/sell_bills_depend_on_user_age', 'admin\reports@sell_bills_depend_on_user_age');
    Route::get('admin/reports/sell_bills_depend_on_user_gender', 'admin\reports@sell_bills_depend_on_user_gender');
    Route::get('admin/reports/sell_bills_depend_on_product', 'admin\reports@sell_bills_depend_on_product');
    Route::get('admin/reports/broken_products', 'admin\reports@broken_products');
    Route::get("/admin/reports/coupons_report",'admin\reports@coupons_report');
    Route::get("/admin/reports/packages_report",'admin\reports@packages_report');
    Route::get("/admin/reports/branches_report",'admin\reports@branches_report');
    Route::get("/admin/reports/cashiers_report",'admin\reports@cashiers_report');
    Route::get("/admin/reports/best_sell_bills_report",'admin\reports@best_sell_bills_report');

    #region products_on_branches_stock

    Route::get('admin/reports/products_on_branches_stock', 'admin\reports@products_on_branches_stock');
    Route::get('admin/reports/products_most_sell', 'admin\reports@products_most_sell');
    Route::get('admin/reports/products_on_factory_stock', 'admin\reports@products_on_factory_stock');
    Route::get('admin/reports/review_products_stock', 'admin\reports@review_products_stock');
    Route::get('admin/reports/gain_products_stock', 'admin\reports@gain_products_stock');

    #endregion

    #region commission
    Route::get('admin/reports/commissions_report', 'admin\reports@commissions_report');
    #endregion

    Route::get('admin/reports/daily_bills', 'admin\reports@daily_bills');


    Route::get('admin/reports/duty_report', 'admin\reports@duty_report');

    #region expenses

    Route::get('admin/reports/expenses_report', 'admin\reports@expenses_report');
    Route::get('admin/reports/review_expenses_report', 'admin\reports@review_expenses_report');

    #endregion



    //END reports



    #region Branch routes


    //points_of_sale
    Route::get('admin/points_of_sale', 'admin\points_of_sale@index');

    Route::get('admin/points_of_sale/save/{point_of_sale_id?}', 'admin\points_of_sale@save');
    Route::post('admin/points_of_sale/save/{point_of_sale_id?}', 'admin\points_of_sale@save');

    //END points_of_sale

    #region branches bills
    Route::get('admin/branch_bills/show_all', 'admin\branch_orders@show_all_branch_bills');
    Route::post('admin/branch_bills/change_bill_status', 'admin\branch_orders@change_bill_status');

    Route::get('admin/branch_bills/show_bill_orders/{bill_id}', 'admin\branch_orders@show_bill_orders');

    Route::get('admin/branch_bills/add_bill/{bill_id?}', 'admin\branch_orders@make_bill');
    Route::post('admin/branch_bills/add_bill/{bill_id?}', 'admin\branch_orders@make_bill');

    Route::get('admin/branch_bills/add_permission_to_stock/{permission_type}', 'admin\branch_orders@add_permission_to_stock');
    Route::post('admin/branch_bills/add_permission_to_stock/{permission_type}', 'admin\branch_orders@add_permission_to_stock');

    Route::get('admin/branch_bills/show_permission_to_stock_bills/{permission_type}', 'admin\branch_orders@show_permission_to_stock_bills');
    Route::get('admin/branch_bills/show_permission_to_stock_bill_orders/{permission_type}/{permission_id}', 'admin\branch_orders@show_permission_to_stock_bill_orders');

    Route::get('admin/branch_bills/add_return_bill/{bill_id?}', 'admin\branch_orders@make_return_bill');
    Route::post('admin/branch_bills/add_return_bill/{bill_id?}', 'admin\branch_orders@make_return_bill');

    Route::post('admin/branch_bills/get_bill_orders', 'admin\branch_orders@get_bill_orders');

    Route::get('admin/branch_bills/save_product_first_time', 'admin\branch_orders@save_product_first_time');
    Route::post('admin/branch_bills/save_product_first_time', 'admin\branch_orders@save_product_first_time');
    Route::get('admin/branch_bills/import_products_first_time_to_stock', 'admin\branch_orders@import_products_first_time_to_stock');
    Route::post('admin/branch_bills/import_products_first_time_to_stock', 'admin\branch_orders@import_products_first_time_to_stock');
    Route::post('admin/branch_bills/approve_import_products_first_time_to_stock', 'admin\branch_orders@approve_import_products_first_time_to_stock');
    Route::post('admin/branch_bills/get_products_for_save_product_first_time', 'admin\branch_orders@get_products_for_save_product_first_time');

    #endregion

    #endregion


    #region client bills
    Route::get('admin/client_bills/show_all', 'admin\client_bills@show_all_clients_bills');
    Route::get('admin/client_bills/show_bill_orders/{bill_id}', 'admin\client_bills@show_bill_orders');

    Route::get('admin/client_bills/add_bill', 'admin\client_bills@make_bill');
    Route::post('admin/client_bills/add_bill', 'admin\client_bills@make_bill');

    Route::get('admin/client_bills/add_return_bill/{bill_id?}', 'admin\client_bills@make_return_bill');
    Route::post('admin/client_bills/add_return_bill/{bill_id?}', 'admin\client_bills@make_return_bill');

    Route::post('admin/client_bills/get_product_by_barcode','admin\client_bills@get_product_by_barcode');
    Route::post('admin/client_bills/check_coupon','admin\client_bills@check_coupon');
    Route::post('admin/client_bills/get_client_data','admin\client_bills@get_client_data');
    Route::post('admin/client_bills/get_cat_child_cats_and_products','admin\client_bills@get_cat_child_cats_and_products');
    Route::post('admin/client_bills/search_for_product','admin\client_bills@search_for_product');

    Route::get('admin/client_bills/search_for_user','admin\client_bills@search_for_user');


    Route::post('admin/client_bills/get_bill_orders', 'admin\client_bills@get_bill_orders');
    Route::get('admin/client_bills/show_client_bills_statistics', 'admin\client_bills@show_client_bills_statistics');

    Route::get('admin/client_bills/replace_bill_order', 'admin\client_bills@replace_bill_order');
    Route::post('admin/client_bills/replace_bill_order', 'admin\client_bills@replace_bill_order');

    Route::post('admin/branch_orders/get_cat_child_cats_and_products','admin\branch_orders@get_cat_child_cats_and_products');
    Route::post('admin/branch_orders/search_for_product','admin\branch_orders@search_for_product');

//    Route::get('admin/client_bills/branch_target/{target_type?}/{year?}/{month?}', 'admin\client_bills@branch_target');


    #endregion


    #region company

    Route::get('admin/company', 'admin\company@save_company');
    Route::post('admin/company', 'admin\company@save_company');

    #endregion


    #region branch_transfers

    Route::get('admin/branch_transfers', 'admin\branch_transfer_products@index');
    Route::get('admin/branch_transfers/show_products/{b_t_id}', 'admin\branch_transfer_products@show_products');
    Route::get('admin/branch_transfers/save', 'admin\branch_transfer_products@save');
    Route::post('admin/branch_transfers/save', 'admin\branch_transfer_products@save');
    Route::post('admin/branch_transfers/remove_branch_transfers', 'admin\branch_transfer_products@remove_branch_transfers');
    Route::post('admin/branch_transfers/receive_branch_transfer', 'admin\branch_transfer_products@receive_branch_transfer');

    #endregion



    #region product_codes

    Route::get('admin/product_codes', 'admin\product_codes@index');
    Route::get('admin/product_codes/save_code/{pro_code_id?}', 'admin\product_codes@save_code');
    Route::post('admin/product_codes/save_code/{pro_code_id?}', 'admin\product_codes@save_code');
    Route::post('admin/product_codes/remove_code', 'admin\product_codes@remove_code');

    #endregion


    #region product_units

    Route::get('admin/product_units', 'admin\product_units@index');
    Route::get('admin/product_units/save/{pro_unit_id?}', 'admin\product_units@save');
    Route::post('admin/product_units/save/{pro_unit_id?}', 'admin\product_units@save');
    Route::post('admin/product_units/remove_product_unit', 'admin\product_units@remove_product_unit');

    #endregion

});


// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

//Route::auth();
//
//Route::get('/home', 'HomeController@index');
