<?php

date_default_timezone_set("Asia/Riyadh");

// Default Routing
Route::get('/','front\admin_panel@index');


// Web Routing
Route::group(['middleware' => ['web']], function () {

    // logout ebn el teeeeeeeeeeeeeeeeeeeeeeeet
    Route::get("/logout","logout@index");
    Route::get("/logout_page","logout@logout_page");

    Route::get('/admin_panel', 'front\admin_panel@index');
    Route::post('/admin_panel', 'front\admin_panel@try_login');

    Route::get('/admin_panel', 'front\admin_panel@index');


    #region send_custom_email
        Route::post('/send_custom_email', 'front\admin_panel@send_custom_email');
    #endregion

    // Temp Pages
    #region dashboard
        Route::get('/dashboard', 'front\pages@dashboard');
        Route::get('/sales_manager', 'front\pages@sales_manager');
        Route::get('/admin_page', 'front\pages@admin_page');
        Route::get('/cashier', 'front\pages@cashier');
    #endregion


});










