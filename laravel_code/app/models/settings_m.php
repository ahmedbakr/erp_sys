<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class settings_m extends Model
{
    protected $table = "settings";
    protected $primaryKey = "setting_id";


    protected $fillable = [
        "count_commission_after_days"
    ];


}
