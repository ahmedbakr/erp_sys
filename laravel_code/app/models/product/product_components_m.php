<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class product_components_m extends Model
{
    protected $table = "product_components";
    protected $primaryKey = "pro_comp_id";
    public $timestamps = false;

    protected $fillable = [
        'pro_id', 'mat_id' , 'pro_comp_amount'
    ];



    public static function get_data($additional_where=""){
        return DB::select("
            select 
                pro_comp.pro_comp_id , pro_comp.pro_comp_amount, pro_comp.mat_id,
                mat.mat_name , mat.mat_type,
                pro.*,
                att.path as 'pro_img_path'
            from product_components as pro_comp
            INNER JOIN materials as mat on mat.mat_id = pro_comp.mat_id
            INNER JOIN products as pro on pro.pro_id = pro_comp.pro_id
            left outer JOIN attachments as att on pro.pro_img=att.id
            where pro.deleted_at is NULL $additional_where
        ");
    }


    public static function get_data_with_product_materials($additional_where=""){
        return DB::select("
            select 
                pro_comp.pro_comp_id , pro_comp.pro_comp_amount, pro_comp.mat_id,
                mat.mat_name , mat.mat_type,
                pro.*,
                att.path as 'pro_img_path',
                pro_mat.mat_amount , pro_mat.mat_price , pro_mat.mat_pro_limit
                
            from product_components as pro_comp
            INNER JOIN materials as mat on mat.mat_id = pro_comp.mat_id
            INNER JOIN products as pro on pro.pro_id = pro_comp.pro_id
            INNER JOIN product_materials as pro_mat on pro_mat.pro_mat_id = pro_comp.mat_id
            left outer JOIN attachments as att on pro.pro_img=att.id
            where pro.deleted_at is NULL $additional_where
        ");
    }

}
