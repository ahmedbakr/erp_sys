<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_codes_m extends Model
{
    use SoftDeletes;
    protected $table = "product_codes";
    protected $primaryKey = "pro_code_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_code_label' , 'pro_code_label_en'
    ];



    public static function get_data($additional_where=""){
        return DB::select("
            select 
                p_s_c.*,
                pro_code.*
                
            from product_codes as pro_code
            LEFT OUTER JOIN product_select_codes as p_s_c on (p_s_c.pro_code_id = pro_code.pro_code_id and p_s_c.deleted_at IS NULL )
            
            where pro_code.deleted_at is NULL $additional_where
        ");
    }

}
