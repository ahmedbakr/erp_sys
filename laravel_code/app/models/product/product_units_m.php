<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_units_m extends Model
{
    use SoftDeletes;
    protected $table = "product_units";
    protected $primaryKey = "pro_unit_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_unit_name' , 'pro_unit_name_en'
    ];

}
