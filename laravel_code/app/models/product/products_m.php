<?php

namespace App\models\product;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class products_m extends Model
{
    use SoftDeletes;
    protected $table = "products";
    protected $primaryKey = "pro_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'cat_id','pro_unit_id','pro_img', 'pro_name','pro_name_en' , 'pro_desc' , 'pro_barcode',
        'pro_quantity' , 'pro_limit' , 'pro_expire_date'
    ];


    public static function get_pros($additional_where=""){
        return DB::select("
            select 
                pro.*,
                pro.pro_id as 'id',
                att.path as 'pro_img_path',
                unit.pro_unit_name,
                unit.pro_unit_name_en,
                cat.cat_name
                
            from products as pro
            left outer join category as cat on (pro.cat_id = cat.cat_id AND cat.deleted_at is null)
            left outer JOIN attachments as att on pro.pro_img=att.id
            left outer JOIN product_units as unit on pro.pro_unit_id=unit.pro_unit_id
            where pro.deleted_at is NULL $additional_where
        ");
    }

    static public function get_count()
    {
        return DB::table('products')->count();
    }

    // same as above but add join with product prices
//    public static function get_products($additional_where=""){
//        return DB::select("
//            select
//                pro.*,
//                (select pro_price_price from product_prices where pro_id = pro.pro_id and pro_price_original = 1) as pro_orgin_price,
//                (
//                    select max(pro_price_price)
//                    from product_prices
//                    WHERE
//                    product_prices.pro_id=pro.pro_id AND
//                    product_prices.branch_id=branch_pro.branch_id AND
//                    product_prices.pro_price_original=0 AND
//                    (curdate() BETWEEN product_prices.pro_price_from AND product_prices.pro_price_to)
//                )
//
//                ,att.path as 'pro_img_path'
//            from products as pro
//            left outer JOIN attachments as att on pro.pro_img=att.id
//            where pro.deleted_at is NULL $additional_where
//        ");
//    }



    public function price_list(){
        return $this->belongsToMany(
            'App\models\product\product_prices_values_m',
            "product_prices_values",
            "pro_id",
            "pro_id"
        );
    }



}
