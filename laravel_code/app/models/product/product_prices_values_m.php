<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;

class product_prices_values_m extends Model
{
    protected $table = "product_prices_values";
    protected $primaryKey = "ppv_id";
    public $timestamps = false;

    protected $fillable = [
        'pro_id', 'price_id', 'ppv_value'
    ];
}
