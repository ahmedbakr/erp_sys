<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_barcode_m extends Model
{
    use SoftDeletes;
    protected $table = "product_barcode";
    protected $primaryKey = "barcode_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'barcode_title','paper_width',
        'barcode_orientation', 'barcode_width' , 'barcode_height',
        'barcode_margin_top', 'barcode_margin_right',
        'barcode_margin_bottom', 'barcode_margin_left',
        'barcode_date_top_position', 'barcode_date_left_position',
        'barcode_code_top_position', 'barcode_code_left_position'
    ];

    static public function get_count()
    {
        return DB::table('product_barcode')->count();
    }


}
