<?php

namespace App\models\product\broken;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class broken_products_m extends Model
{
    use SoftDeletes;
    protected $table = "broken_products";
    protected $primaryKey = "b_p_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'pro_id' , 'b_p_quantity' , 'b_p_date'
    ];


    public static function get_data($additional_where=""){
        return DB::select("
            select 
                bro_pro.*,
                cat.cat_name,
                pro.pro_name,pro.pro_barcode , pro.pro_quantity , pro.pro_limit , pro.created_at as pro_created_date,
                att.path as 'pro_img_path'
                
            from broken_products as bro_pro
            INNER JOIN products as pro on bro_pro.pro_id = pro.pro_id
            INNER JOIN category as cat on cat.cat_id = pro.cat_id
            left outer JOIN attachments as att on pro.pro_img=att.id
            where pro.deleted_at is NULL $additional_where
        ");
    }
}
