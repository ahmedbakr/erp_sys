<?php

namespace App\models\product\broken;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class broken_materials_m extends Model
{
    use SoftDeletes;
    protected $table = "broken_materials";
    protected $primaryKey = "b_m_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'mat_id', 'b_m_quantity' , 'b_m_date'
    ];




    public static function get_data($additional_where = "")
    {
        $results = DB::select("
             select 
             bro_mat.b_m_id,bro_mat.mat_id,bro_mat.b_m_quantity,bro_mat.b_m_date,
             mat.*
             
             from broken_materials as bro_mat
             INNER JOIN materials as mat on (mat.mat_id = bro_mat.mat_id)
             #where
             where bro_mat.deleted_at is NULL $additional_where ");

        return $results;

    }
}
