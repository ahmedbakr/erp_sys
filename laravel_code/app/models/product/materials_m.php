<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class materials_m extends Model
{
    use SoftDeletes;
    protected $table = "materials";
    protected $primaryKey = "mat_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'mat_name' , 'mat_type'
    ];

    static public function get_count()
    {
        return DB::table('materials')->count();
    }

}
