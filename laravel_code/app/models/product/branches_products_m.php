<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class branches_products_m extends Model
{
    use SoftDeletes;
    protected $table = "branches_products";
    protected $primaryKey = "b_p_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'branch_id', 'pro_id' , 'b_p_quantity' ,'b_p_quantity_limit','b_p_date','b_p_first_time'
    ];

    public static function get_branches_pros($additional_where=""){
        //current_pro_price
        return DB::select("
            select 
            branch_pro.*,
            pro.*,
            att.path as 'pro_img_path',
            pro_price.pro_price_price as 'current_pro_price',
            unit.pro_unit_name,
            unit.pro_unit_name_en,
            branch_data.full_name,
            cat.cat_name
            
              
            from branches_products as branch_pro
            inner join products as pro on (pro.pro_id=branch_pro.pro_id AND pro.deleted_at is NULL )
            left outer join category as cat on (pro.cat_id = cat.cat_id AND cat.deleted_at is null)
            inner join users as branch_data on (branch_data.user_id = branch_pro.branch_id and branch_data.deleted_at is null)
            left outer join product_prices as pro_price on(pro_price.pro_id=pro.pro_id AND pro_price.branch_id=branch_pro.branch_id AND pro_price.pro_price_original=1)
            left outer JOIN attachments as att on pro.pro_img=att.id
            left outer JOIN product_units as unit on pro.pro_unit_id=unit.pro_unit_id
            
            where branch_pro.deleted_at is NULL $additional_where
        ");

    }



    //old_func may use it afterwards
    public static function old_get_branches_pros($additional_where=""){

        return DB::select("
            select 
            branch_pro.*,
            pro.*,
            
            if(
                (
                    select max(pro_price_price)
                    from product_prices 
                    WHERE 
                    product_prices.pro_id=branch_pro.pro_id AND
                    product_prices.branch_id=branch_pro.branch_id AND 
                    product_prices.pro_price_original=0 AND 
                    (curdate() BETWEEN product_prices.pro_price_from AND product_prices.pro_price_to)
                ) is null,
                (
                    select max(pro_price_price) 
                    from product_prices 
                    WHERE 
                    product_prices.pro_id=branch_pro.pro_id AND
                    product_prices.branch_id=branch_pro.branch_id AND 
                    product_prices.pro_price_original=1
                ),
                (
                    select max(pro_price_price)
                    from product_prices 
                    WHERE 
                    product_prices.pro_id=branch_pro.pro_id AND
                    product_prices.branch_id=branch_pro.branch_id AND 
                    product_prices.pro_price_original=0 AND 
                    (curdate() BETWEEN product_prices.pro_price_from AND product_prices.pro_price_to)
                )
            ) as 'current_pro_price',
            att.path as 'pro_img_path'
            
              
            from branches_products as branch_pro
            inner join products as pro on (pro.pro_id=branch_pro.pro_id AND pro.deleted_at is NULL )
            left outer JOIN attachments as att on pro.pro_img=att.id

            where branch_pro.deleted_at is NULL $additional_where
        ");

    }

}
