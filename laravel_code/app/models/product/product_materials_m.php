<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_materials_m extends Model
{
    use SoftDeletes;
    protected $table = "product_materials";
    protected $primaryKey = "pro_mat_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'mat_id' , 'mat_amount' , 'mat_price',
        'mat_pro_limit', 'mat_desc' , 'mat_barcode' 
    ];



    public static function get_data($additional_where = "")
    {
        $results = DB::select("
             select 
             pro_mat.pro_mat_id,pro_mat.mat_id,pro_mat.mat_amount,pro_mat.mat_price,
             pro_mat.mat_pro_limit, pro_mat.mat_desc,
             pro_mat.mat_barcode ,
             pro_mat.created_at as pro_mat_created,
             mat.*
             
             from product_materials as pro_mat
             INNER JOIN materials as mat on (mat.mat_id = pro_mat.mat_id)
             #where
             where pro_mat.deleted_at is NULL $additional_where ");

        return $results;

    }


    static public function get_count()
    {
        return DB::table('product_materials')->count();
    }
    
}
