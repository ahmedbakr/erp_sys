<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class produce_product_review_m extends Model
{
    use SoftDeletes;
    protected $table = "produce_product_review";
    protected $primaryKey = "ppr_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_id' , 'produce_quantity','is_produced'
    ];

    static public function get_count()
    {
        return DB::table('produce_product_review')->count();
    }


    public static function get_pros($additional_where=""){
        return DB::select("
            select 
            ppr.ppr_id,
            ppr.produce_quantity,
            ppr.created_at as 'produce_request_date',
            ppr.is_produced,
            
            pro.*,
            att.path as 'pro_img_path'
                
            from produce_product_review as ppr
            INNER JOIN products as pro ON (pro.pro_id = ppr.pro_id)
            
            left outer JOIN attachments as att on pro.pro_img=att.id
            
            where ppr.deleted_at is NULL $additional_where
            
        ");
    }

}
