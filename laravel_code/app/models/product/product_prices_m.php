<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class product_prices_m extends Model
{
    use SoftDeletes;
    protected $table = "product_prices";
    protected $primaryKey = "pro_price_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_id', 'branch_id' , 'pro_price_label' , 'pro_price_quantity',
        'pro_price_price', 'pro_price_original' , 'pro_price_from' , 'pro_price_to'
    ];
}
