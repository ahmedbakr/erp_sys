<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_select_codes_m extends Model
{
    use SoftDeletes;
    protected $table = "product_select_codes";
    protected $primaryKey = "p_s_c_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_code_id' , 'pro_id' , 'pro_code_val'
    ];

    public static function get_data($additional_where=""){
        return DB::select("
            select 
                p_s_c.*,
                pro.*,
                pro_code.*
                
            from product_select_codes as p_s_c
            INNER JOIN product_codes as pro_code on (pro_code.pro_code_id = p_s_c.pro_code_id and pro_code.deleted_at IS NULL )
            INNER JOIN products as pro on (pro.pro_id= p_s_c.pro_id and pro.deleted_at IS NULL )
            
            where p_s_c.deleted_at is NULL $additional_where
        ");
    }

}
