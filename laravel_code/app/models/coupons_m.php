<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class coupons_m extends Model
{
    use SoftDeletes;

    protected $table = "coupons";

    protected $primaryKey = "coupon_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        "cat_id",'coupon_code','coupon_used_times', 'coupon_current_used' ,
        'coupon_end_date',
        'is_rate', 'coupon_value','is_used'
    ];


}
