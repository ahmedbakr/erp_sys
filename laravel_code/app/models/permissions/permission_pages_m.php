<?php

namespace App\models\permissions;

use Illuminate\Database\Eloquent\Model;

class permission_pages_m extends Model
{
    protected $table = "permission_pages";
    protected $primaryKey = "per_page_id";
    public $timestamps = false;

    protected $fillable = [
        'page_name','page_label','sub_sys' , 'sub_group' , 'sub_group_ar' ,
        'all_additional_permissions', 'all_additional_permissions_ar'
    ];
}
