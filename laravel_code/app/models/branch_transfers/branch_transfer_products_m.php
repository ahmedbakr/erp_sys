<?php

namespace App\models\branch_transfers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class branch_transfer_products_m extends Model
{

    protected $table = "branch_transfer_products";
    protected $primaryKey = "b_t_p_id";
    public $timestamps = false;

    protected $fillable = [
        'b_t_id','pro_id', 'pro_quantity'
    ];


    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $results = DB::select("
            select 
            transfer_pros.*,
            transfer_pros.pro_quantity as 'transfer_pro_quantity',
            
            #product
            pro.*
            
             
            #joins
             from branch_transfer_products as transfer_pros
             INNER JOIN products as pro on (transfer_pros.pro_id = pro.pro_id AND pro.deleted_at is NULL )

             #where
             $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $results;

    }

}
