<?php

namespace App\models\branch_transfers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class branch_transfers_m extends Model
{
    use SoftDeletes;

    protected $table = "branch_transfers";
    protected $primaryKey = "b_t_id";
    public $timestamps = true;
    protected $dates = ["deleted_at"];

    protected $fillable = [
        'from_branch','to_branch', 'b_t_status'
    ];


    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $results = DB::select("
            select 
            transfer.*,
            
            #from_branch
            from_branch.user_code as 'from_branch_user_code',
            from_branch.full_name as 'from_branch_full_name',
            from_branch.user_type as 'from_branch_user_type',
            from_branch.related_id as 'from_branch_related_id',
            
            #to_branch
            to_branch.user_code as 'to_branch_user_code',
            to_branch.full_name as 'to_branch_full_name',
            to_branch.user_type as 'to_branch_user_type',
            to_branch.related_id as 'to_branch_related_id'
             
            #joins
             from branch_transfers as transfer
             INNER JOIN users as from_branch on (transfer.from_branch = from_branch.user_id AND from_branch.deleted_at is NULL )
             INNER JOIN users as to_branch on (transfer.to_branch = to_branch.user_id AND to_branch.deleted_at is NULL )

             #where
             where transfer.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $results;

    }

}
