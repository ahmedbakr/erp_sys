<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class user_messages_m extends Model
{
    use SoftDeletes;

    protected $table = "user_messages";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'from_user_id','to_user_id' ,'message'

    ];

    static function get_messages($additional_where = "")
    {
        $results = DB::select("
             select msg.message , msg.created_at as msg_date,
             user.*,attach.*
             
             from user_messages as msg
             INNER JOIN users as user on (user.user_id = msg.from_user_id)
             LEFT OUTER JOIN attachments attach on (user.logo_id = attach.id)
             #where
             $additional_where ");

        return $results;

    }
}
