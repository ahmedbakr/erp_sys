<?php

namespace App\models\commission;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class day_commission_m extends Model
{
    use SoftDeletes;
    protected $table = "day_commission";
    protected $primaryKey = "day_commission_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'commission_id', 'day_date', 'user_id',
        'commission_value','total_day_bills_money','is_received'
    ];

    public static function get_data($additional_where=""){
        return DB::select("
            select 
                rule.*,
                commission.*,
                commission.created_at as 'commission_created_at',
                commission.updated_at as 'commission_updated_at',
                user.*
                
            from day_commission as commission
            INNER JOIN commission_rules as rule on(rule.commission_id = commission.commission_id)
            INNER JOIN users as user on(user.user_id = commission.user_id)

            where commission.deleted_at is null $additional_where
        ");
    }

}
