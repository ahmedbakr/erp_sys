<?php

namespace App\models\commission;

use App\models\admin\category_m;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class commission_rules_m extends Model
{
    use SoftDeletes;
    protected $table = "commission_rules";
    protected $primaryKey = "commission_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        "branch_id","commission_rule_type","commission_rule_cat_ids",
        'minimum_day_earn', 'total_commission_value', 'branch_commission_value',
        'factory_commission_value'
    ];

    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $results = DB::select("
            select 
            rule.*,
            
            branch.*
            
             
            #joins
             from commission_rules as rule
             INNER JOIN users as branch on (rule.branch_id = branch.user_id AND branch.deleted_at is NULL )

             #where
             where rule.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");


        if(isset_and_array($results)&&count($results)==1) {

            if(isset($results[0]->commission_rule_cat_ids)){
                $results[0]->commission_rule_cats=category_m::where("cat_id",$results[0]->commission_rule_cat_ids)->get()->all();
            }

        }

        return $results;

    }

}
