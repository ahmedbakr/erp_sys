<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class site_tracking_m extends Model
{
    protected $table = "site_tracking";
    protected $primaryKey = "track_id";
    public $timestamps = true;

    protected $fillable = [
        'user_id' , 'action_desc'
    ];

    static function get_tracking($additional_where = "")
    {
        $results = DB::select("
             select u.*,
             track.action_desc as action_desc , track.created_at as track_date,
             attach.*
             
             #joins
             from site_tracking as track
             INNER JOIN users as u on (u.user_id = track.user_id)
             LEFT OUTER JOIN attachments as attach on (u.logo_id = attach.id)

             #where
             $additional_where ");

        return $results;

    }

    static function get_user_tracking($additional_where = "")
    {
        $results = DB::select("
             select track.*, hour(track.created_at) as hours,
              u.full_name, u.role
             
             from site_tracking as track
             INNER JOIN users as u on (u.user_id = track.user_id)
             #where
             $additional_where ");

        return $results;

    }

}
