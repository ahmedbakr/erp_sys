<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class points_of_sale_m extends Model
{
    use SoftDeletes;

    protected $table = "points_of_sale";
    protected $primaryKey = "point_of_sale_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'branch_id', 'point_of_sale_code', 'point_of_sale_desc', 'point_of_sale_desc_en',
        'point_of_sale_balance', 'set_cash_on_my_balance', 'is_exist_password_for_return',
        'return_password'
    ];

    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
             select point.*,
             user.*,
             point.point_of_sale_id as 'point_of_sale_id'
                          
             
             
             #joins
             from points_of_sale as point
             INNER JOIN users as user on (point.branch_id = user.user_id AND user.deleted_at is NULL )

             #where
             where point.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;

    }

}
