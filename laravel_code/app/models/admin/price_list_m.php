<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;

class price_list_m extends Model
{

    protected $table = "price_list";
    protected $primaryKey = "price_id";
    public $timestamps = false;

    protected $fillable = [
        'price_title','default_price'
    ];



}
