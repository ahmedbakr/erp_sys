<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class payment_methods_m extends Model
{
    use SoftDeletes;

    protected $table = "payment_methods";
    protected $primaryKey = "payment_method_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'payment_method_name', 'payment_method_name_en' , 'is_payment_method_atm' ,
        'atm_ratio'
    ];


}
