<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class client_bill_payment_details_m extends Model
{
    use SoftDeletes;

    protected $table = "client_bill_payment_details";
    protected $primaryKey = "pay_details_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'bill_id', 'payment_method_id', 'payment_amount', 'atm_ratio_amount'
    ];


    public static function get_bill_payments_methods($additional_where=""){

        return DB::select("
            select 
             pay_datails.*,
             pay.payment_method_name,
             pay.payment_method_name_en,
             pay.is_payment_method_atm
            
            from client_bill_payment_details as pay_datails
            inner join payment_methods as pay on (pay.payment_method_id=pay_datails.payment_method_id)
            where pay_datails.deleted_at is null $additional_where
        
        ");


    }


}
