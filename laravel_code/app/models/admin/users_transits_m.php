<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class users_transits_m extends Model
{
    use SoftDeletes;

    protected $table = "users_transits";
    protected $primaryKey = "user_transit_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'from_branch', 'to_branch', 'user_transit_date' , 'user_transit_reason'
    ];


    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
             select transit.*,
             
             #user_data
             user_data.user_code as 'user_data_code',
             user_data.full_name as 'user_data_full_name',
             user_data.full_name_en as 'user_data_full_name_en',
             
             #from_branch_data
             from_branch_data.user_code as 'from_branch_data_code',
             from_branch_data.full_name as 'from_branch_data_full_name',
             from_branch_data.full_name_en as 'from_branch_data_full_name_en',
             
             #from_branch_data
             to_branch_data.user_code as 'to_branch_data_code',
             to_branch_data.full_name as 'to_branch_data_full_name',
             to_branch_data.full_name_en as 'to_branch_data_full_name_en'
             
             
             #joins
             from users_transits as transit
             INNER JOIN users as user_data on (transit.user_id = user_data.user_id AND user_data.deleted_at is NULL )
             INNER JOIN users as from_branch_data on (transit.from_branch = from_branch_data.user_id AND from_branch_data.deleted_at is NULL )
             INNER JOIN users as to_branch_data on (transit.to_branch = to_branch_data.user_id AND to_branch_data.deleted_at is NULL )

             #where
             where transit.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;

    }

}
