<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class duty_documents_m extends Model
{
    use SoftDeletes;

    protected $table = "duty_documents";
    protected $primaryKey = "duty_document_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'from_user_id', 'to_user_id', 'duty_document_date', 'duty_document_money',
        'duty_document_money_text', 'duty_document_desc', 'duty_document_received'
    ];


    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
             select duty.*,
                          
             #from_user
             from_user.user_code as 'from_user_code',
             from_user.full_name as 'from_user_full_name',
             from_user.full_name_en as 'from_user_full_name_en',
             from_user.user_type as 'from_user_type',
             from_user.user_duty_balance as 'from_user_duty_balance',
             
             #to_user
             to_user.user_code as 'to_user_code',
             to_user.full_name as 'to_user_full_name',
             to_user.full_name_en as 'to_user_full_name_en',
             to_user.user_type as 'to_user_type',
             to_user.user_duty_balance as 'to_user_duty_balance'
             
             
             #joins
             from duty_documents as duty
             INNER JOIN users as from_user on (duty.from_user_id = from_user.user_id AND from_user.deleted_at is NULL )
             INNER JOIN users as to_user on (duty.to_user_id = to_user.user_id AND to_user.deleted_at is NULL )

             #where
             where duty.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;

    }

}
