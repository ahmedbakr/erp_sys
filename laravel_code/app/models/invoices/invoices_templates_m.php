<?php

namespace App\models\invoices;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class invoices_templates_m extends Model
{
    use SoftDeletes;
    protected $table = "invoices_templates";
    protected $primaryKey = "inv_temp_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'template_name','branch_id','template_width','template_height',"table_direction",
        "table_width","table_height",'set_default','table_position_left','table_position_top'
    ];

    public static function get_template_data($additional_where=""){
        return DB::select("
            select 
                inv_temp.*,
                inv_temp.created_at as 'invoice_created_date' ,
                user.*
            from invoices_templates as inv_temp
            LEFT OUTER JOIN users as user on(user.user_id = inv_temp.branch_id and user.deleted_at is null)
            where inv_temp.deleted_at is null $additional_where
        ");
    }

    public static function get_data($additional_where=""){
        return DB::select("
            select 
                inv_temp.*,
                itc.*,
                user.*,
                att.path as 'temp_img_path'
            from invoices_templates as inv_temp
            LEFT OUTER JOIN users as user on(user.user_id = inv_temp.branch_id and user.deleted_at is null)
            INNER JOIN invoices_template_content as itc on (itc.inv_temp_id = inv_temp.inv_temp_id)
            left outer JOIN attachments as att on itc.item_img=att.id
            where inv_temp.deleted_at is null $additional_where
        ");
    }

}
