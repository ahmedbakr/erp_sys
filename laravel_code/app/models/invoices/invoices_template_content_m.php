<?php

namespace App\models\invoices;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class invoices_template_content_m extends Model
{
    use SoftDeletes;
    protected $table = "invoices_template_content";
    protected $primaryKey = "item_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'inv_temp_id', 'item_name', 'item_label', 'item_type','item_code',
        'item_img', 'item_position_top', 'item_position_left', 'item_hide',
        'item_width','item_height'
    ];

    public static function get_data($additional_where="")
    {
        return DB::select("
            select 
                itc.*,
                att.path as 'temp_img_path'
            from invoices_template_content as itc
            left outer JOIN attachments as att on itc.item_img=att.id
            where itc.deleted_at is null $additional_where
        ");
    }

}
