<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class user_as_sponsor_m extends Model
{
    //
    protected $table="user_as_sponsor";
    protected $primaryKey="uas_id";
    public $timestamps=false;

    protected $fillable=[
        "user_id","sponsor_id","last_earn_gift_date"
    ];


}
