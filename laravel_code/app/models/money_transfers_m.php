<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class money_transfers_m extends Model
{
    use SoftDeletes;
    protected $table = "money_transfers";
    protected $primaryKey = "m_t_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'from_user_id', 'to_user_id' , 'm_t_bill_id' ,'m_t_bill_type',
        'm_t_amount' , 'm_t_amount_type' , 'm_t_status', 'm_t_desc'
    ];

    static function get_data($additional_where = "")
    {
        $results = DB::select("
             select money.*,
             date(money.created_at) as 'money_created_date' 
             
             from money_transfers as money
             inner join clients_bills as bill on (money.m_t_bill_id = bill.client_bill_id and bill.deleted_at is null)
             #where
             where money.deleted_at is NULL $additional_where ");

        return $results;

    }

}
