<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class branch_deposite_money_m extends Model
{

    use SoftDeletes;

    protected $table = "branch_deposite_money";
    protected $primaryKey = "b_d_money_id";
    public $timestamps = true;
    protected $dates = ["deleted_at"];

    protected $fillable = [
        'b_d_money_id', 'branch_id', 'attach_file_id', 'deposite_date',
        'origin_money', 'total_money_on_box' , 'deposite_money', 'lose_money','deposite_desc',
        'created_at','updated_at'
    ];


    public static function get_data($additional_where=""){
        return DB::select("
            select 
                deposite_money.*,
                deposite_money.created_at as 'deposite_created_at',
                branch.*,
                attach.path as 'attach_file_path'
                
            from branch_deposite_money as deposite_money
            INNER JOIN users as branch on (deposite_money.branch_id = branch.user_id and branch.deleted_at is null)
            LEFT OUTER JOIN attachments as attach on (attach.id = deposite_money.attach_file_id)
            
            where deposite_money.deleted_at is NULL $additional_where
        ");
    }


}
