<?php

namespace App\models\bills;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class branch_bills_m extends Model
{
    use SoftDeletes;
    
    protected $table = "branch_bills";
    protected $primaryKey = "branch_bill_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'branch_id', 'branch_bill_date','status_id'
    ];

    public static function get_branch_bills($additional_where=""){

        return DB::select("
            select 
            bill.*,
            status.status_label as 'current_status' 
            from branch_bills as bill
            inner join status as status on bill.status_id=status.status_id
            where bill.deleted_at is null $additional_where
        
        ");

    }

    public static function get_branch_user_bills($additional_where=""){

        return DB::select("
            select 
            bill.*,
            status.status_label as 'current_status' ,
            u.logo_id ,u.full_name , u.user_type , u.role, u.user_active , u.user_can_login , u.contacts
            ,attach.path
            
            from branch_bills as bill
            inner join status as status on bill.status_id=status.status_id
            inner join users as u on u.user_id=bill.branch_id
            LEFT OUTER join attachments as attach on u.logo_id=attach.id
            where bill.deleted_at is null $additional_where
        
        ");

    }



}
