<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class permission_bill_orders_m extends Model
{
    use SoftDeletes;
    protected $table = "permission_bill_orders";
    protected $primaryKey = "permission_bill_order_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'permission_id', 'pro_id' ,'pro_quantity'
    ];

    public static function get_data($additional_where){

        return DB::select("
            select 
            o.*,
            o.pro_quantity as 'order_pro_quantity',
            pro.*
            
            from permission_bill_orders as o 
            INNER join products as pro on (pro.pro_id=o.pro_id AND pro.deleted_at is NULL )
            
            where o.deleted_at is NULL $additional_where
        ");
    }

}
