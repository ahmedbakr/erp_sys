<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class permission_bills_m extends Model
{
    use SoftDeletes;
    protected $table = "permission_bills";
    protected $primaryKey = "permission_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'branch_id' ,'permission_type' ,'permission_date'
    ];

    public static function get_data($additional_where){

        return DB::select("
            select 
            bill.*,
            u.full_name as 'user_full_name',
            branch.full_name as 'branch_full_name'
            
            from permission_bills as bill 
            INNER join users as u on (u.user_id=bill.user_id AND u.deleted_at is NULL )
            LEFT OUTER JOIN users as branch on (branch.user_id=bill.branch_id AND branch.deleted_at is NULL )
            
            where bill.deleted_at is NULL $additional_where
        ");


    }

}
