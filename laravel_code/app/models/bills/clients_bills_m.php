<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class clients_bills_m extends Model
{
    use SoftDeletes;
    protected $table = "clients_bills";
    protected $primaryKey = "client_bill_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'cashier_id', 'branch_id' ,'customer_id',
        'client_bill_total_amount' ,
        'client_bill_total_return_amount' ,
        'client_bill_return_cash_amount','client_bill_return_atm_amount',
        'client_total_paid_amount_in_cash','client_total_paid_amount_in_atm',
        'client_bill_total_remain_amount',
        'bill_total_amount_before_tax','bill_tax_percent','bill_tax_value','bill_tax_return_value',
        'bank_atm_ratio_value',
        'bill_coupon','bill_coupon_amount',
        'bill_packages','bill_packages_amount',
        'is_gift_bill',
        'client_bill_date','is_reviewed'
    ];

    public static function get_clients_bills($additional_where=""){

        return DB::select("
            select
            bill.*,
            date(bill.client_bill_date) as 'client_bill_date_only',
            hour(bill.client_bill_date) as 'client_bill_hour_only',
            cashier.full_name as 'cashier_full_name',
            client.full_name as 'client_full_name',
            branch.full_name as 'branch_full_name',
            client.user_tel as 'client_user_tel',
            client.age_range as 'client_age_range',
            client.user_gender as 'client_user_gender',
            (select count(*) from clients_orders where clients_orders.client_bill_id = bill.client_bill_id) as 'count_orders'
            
            
            from clients_bills as bill
            LEFT OUTER join users as branch on branch.user_id=bill.branch_id
            LEFT OUTER join users as cashier on cashier.user_id=bill.cashier_id
            LEFT OUTER join users as client on client.user_id=bill.customer_id
            
            where bill.deleted_at is null $additional_where
        
        ");

    }

    public static function get_client_bills_count($additional_where){

        return DB::select("
            select 
            bill.branch_id,
            sum(bill.client_total_paid_amount_in_cash) as 'client_total_paid_amount_in_cash', 
            sum(bill.client_total_paid_amount_in_atm) as 'client_total_paid_amount_in_atm',
            sum(bill.client_bill_return_cash_amount) as 'client_bill_return_cash_amount',
            sum(bill.client_bill_return_atm_amount) as 'client_bill_return_atm_amount',
            sum(bill.client_bill_total_return_amount) as 'client_bill_total_return_amount',
            sum(bill.bill_coupon_amount) as 'bill_coupon_amount',
            sum(bill.bill_packages_amount) as 'bill_packages_amount',
            sum(bill.bill_tax_value) as 'total_client_bill_tax_value',
            sum(bill.bill_tax_return_value) as 'total_client_bill_tax_return_value',
            count(*) as 'bills_count'
            
            from clients_bills as bill 
            WHERE bill.deleted_at is NULL $additional_where
            GROUP BY branch_id
        ");

    }

}
