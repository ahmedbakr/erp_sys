<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class supplier_bills_m extends Model
{
    use SoftDeletes;
    protected $table = "supplier_bills";
    protected $primaryKey = "supplier_bill_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'supplier_id', 'supplier_bill_total_amount',
        'supplier_bill_total_paid' ,'supplier_bill_total_paid_in_cash','supplier_bill_total_paid_in_atm',
        'supplier_bill_total_return_money',
        'supplier_bill_extra_cost' , 'supplier_bill_date'
    ];

    public static function get_count()
    {
        return DB::table('supplier_bills')->count();
    }

    public static function get_bill_data($additional_where = "")
    {
        $results = DB::select("
             select 
             supplier_bill.*,
             supplier_bill.created_at as supplier_bill_created,
             u.*
             
             from supplier_bills as supplier_bill
             INNER JOIN users as u on (u.user_id = supplier_bill.supplier_id)
             #where
             where supplier_bill.deleted_at is NULL $additional_where ");

        return $results;

    }

    public static function get_bill_orders_with_money_transfers($additional_where = "")
    {
        $results = DB::select("
             select
                supplier_bill.supplier_bill_id , supplier_bill.supplier_bill_total_amount , 
                supplier_bill.supplier_bill_date, supplier_bill.supplier_bill_extra_cost ,  
                SUM(IF(sup_mat.sup_mat_return = 0, sup_mat.sup_mat_amount, 0 )) as total_purchases_amount,
                SUM(IF(sup_mat.sup_mat_return = 0, (sup_mat.sup_mat_amount*sup_mat.sup_mat_price), 0 )) as total_purchases_price,
                SUM(IF(sup_mat.sup_mat_return = 1, sup_mat.sup_mat_amount, 0 )) as total_returns_amount,
                SUM(IF(sup_mat.sup_mat_return = 1, (sup_mat.sup_mat_amount*sup_mat.sup_mat_price), 0 )) as total_returns_price,
                IFNULL((select SUM(m_t_amount) from money_transfers as money where money.to_user_id = supplier_bill.supplier_id AND money.m_t_bill_id = supplier_bill.supplier_bill_id AND money.m_t_status = 1 ),0) as total_paid_money,
                IFNULL((select SUM(m_t_amount) from money_transfers as money where money.to_user_id = supplier_bill.supplier_id AND money.m_t_bill_id = supplier_bill.supplier_bill_id AND money.m_t_status = 0 ),0) as total_remain_money,
                IFNULL((select SUM(m_t_amount) from money_transfers as money where money.from_user_id = supplier_bill.supplier_id AND money.m_t_bill_id = supplier_bill.supplier_bill_id AND money.m_t_status = 1 ),0) as return_received_money,
                IFNULL((select SUM(m_t_amount) from money_transfers as money where money.from_user_id = supplier_bill.supplier_id AND money.m_t_bill_id = supplier_bill.supplier_bill_id AND money.m_t_status = 0 ),0) as return_remained_money
                
                ,supplier_bill.deleted_at, supplier_bill.supplier_id , supplier_bill.created_at
                
                from supplier_bills as supplier_bill
                INNER JOIN suppliers_materials as sup_mat on (supplier_bill.supplier_bill_id = sup_mat.supplier_bill_id)
                #where
                GROUP BY supplier_bill.supplier_bill_id
                having supplier_bill.deleted_at is NULL  $additional_where");

        return $results;
    }

}
