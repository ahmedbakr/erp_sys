<?php

namespace App\models\cheques;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class cheque_template_items_m extends Model
{
    use SoftDeletes;
    protected $table = "cheque_template_items";
    protected $primaryKey = "item_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'cheque_template_id', 'item_name', 'item_width', 'item_height',
        'item_position_top', 'item_position_left', 'item_hide'
    ];

 
}
