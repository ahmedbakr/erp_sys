<?php

namespace App\models\cheques;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class cheque_templates_m extends Model
{
    use SoftDeletes;
    protected $table = "cheque_templates";
    protected $primaryKey = "cheque_template_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'cheque_template_id', 'branch_id', 'cheque_template_name',
        'cheque_width', 'cheque_height', 'set_default'
    ];

    public static function get_data($additional_where=""){
        return DB::select("
            select 
                cheque.*,
                cheque.created_at as 'cheque_date',
                user.*
                
            from cheque_templates as cheque
            LEFT OUTER JOIN users as user on(user.user_id = cheque.branch_id and user.deleted_at is null)

            where cheque.deleted_at is null $additional_where
        ");
    }

}
