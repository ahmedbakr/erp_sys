<?php

namespace App\models\client_bill_used;

use Illuminate\Database\Eloquent\Model;

class client_bill_used_coupon_m extends Model
{
    protected $table = "client_bill_used_coupon";
    protected $primaryKey = "cbuc_id";
    public $timestamps = false;

    protected $fillable = [
        'client_bill_id', 'coupon_id', 'coupon_cat_id', 'coupon_amount', 'used_at'
    ];
}
