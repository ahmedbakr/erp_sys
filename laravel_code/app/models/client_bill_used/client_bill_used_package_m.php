<?php

namespace App\models\client_bill_used;

use Illuminate\Database\Eloquent\Model;

class client_bill_used_package_m extends Model
{
    protected $table = "client_bill_used_package";
    protected $primaryKey = "cbup_id";
    public $timestamps = false;

    protected $fillable = [
        'client_bill_id', 'package_id', 'package_amount', 'used_at'
    ];
}
