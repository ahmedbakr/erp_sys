<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class old_assets_m extends Model
{
    use SoftDeletes;

    protected $table = "assets";
    protected $primaryKey = "asset_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'asset_name', 'asset_desc', 'asset_cost', 'asset_original_cost',
        'asset_broken_percentage', 'asset_buy_date','last_calculated_date', 'asset_sell_amount', 'asset_sell_date',
        'period_in_years', 'used_period'
    ];


    public static function get_assets($additional_where=""){
        return DB::select("
            select 
            asset.*,
            asset_owner.full_name 
            from assets as asset
            left outer join users as asset_owner on asset_owner.user_id=asset.user_id
            where asset.deleted_at is NULL $additional_where
        ");
    }

}
