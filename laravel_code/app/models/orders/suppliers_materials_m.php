<?php

namespace App\models\orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class suppliers_materials_m extends Model
{
    use SoftDeletes;
    protected $table = "suppliers_materials";
    protected $primaryKey = "sup_mat_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'supplier_bill_id', 'sup_mat_user_id' , 'mat_id' , 'sup_mat_amount',
        'sup_mat_price', 'sup_mat_desc' , 'sup_mat_return' , 'sup_mat_barcode'
    ];


    static public function get_count()
    {
        return DB::table('suppliers_materials')->count();
    }


    public static function get_orders_data($additional_where = "")
    {
        $results = DB::select("
             select 
             sup_mat.sup_mat_id,sup_mat.supplier_bill_id,sup_mat.sup_mat_user_id,
             sup_mat.mat_id, sup_mat.sup_mat_amount,
             sup_mat.sup_mat_price , sup_mat.sup_mat_desc,
             sup_mat.sup_mat_return,sup_mat.sup_mat_barcode ,sup_mat.created_at as sup_mat_created,
             user.*,mat.mat_name, mat.mat_type
             
             from suppliers_materials as sup_mat
             INNER JOIN users as user on (user.user_id = sup_mat.sup_mat_user_id)
             INNER JOIN materials as mat on (mat.mat_id = sup_mat.mat_id)
             #where
             where sup_mat.deleted_at is NULL $additional_where ");

        return $results;

    }
}
