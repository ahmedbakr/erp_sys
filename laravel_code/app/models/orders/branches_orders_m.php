<?php

namespace App\models\orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class branches_orders_m extends Model
{
    use SoftDeletes;
    protected $table = "branches_orders";
    protected $primaryKey = "b_o_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'branch_bill_id', 'pro_id' , 'branch_id' , 'b_o_quantity' , 'b_o_date' , 'status_id' , 'b_o_return','factory_accept_return'
    ];


    static public function get_bill_orders($additional_where=""){

        return
        DB::select("
            select 
            branches_order.*,
            branch_bill.*,
            pro.*,
            pro_att.path as 'pro_img_path',
            branch.full_name,
            branches_order.created_at as 'order_created_at'
              
            from branches_orders as branches_order
             
            INNER JOIN branch_bills as branch_bill on (branch_bill.branch_bill_id = branches_order.branch_bill_id AND branch_bill.deleted_at is NULL )
            INNER JOIN products as pro on (branches_order.pro_id=pro.pro_id AND pro.deleted_at is NULL)
            left outer join attachments as pro_att on pro_att.id=pro.pro_img
            INNER JOIN users as branch on (branch.user_id=branch_bill.branch_id AND branch.deleted_at is NULL) 
             
             
            #where
            where branch_bill.deleted_at is NULL $additional_where "
        );

    }


    static public function get_count()
    {
        return DB::table('branches_orders')->count();
    }

}
