<?php

namespace App\models\orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class clients_orders_m extends Model
{
    use SoftDeletes;
    protected $table = "clients_orders";
    protected $primaryKey = "order_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'pro_id', 'client_bill_id' ,'pro_sell_price','pro_sell_tax_value',
        'pro_sell_price_before_tax','pro_original_price',
        'order_quantity' , 'order_return','is_gift'
    ];


    static public function get_bill_orders($additional_where=""){

        return
            DB::select("
            select 
            clients_order.*,
            client_bill.*,
            pro.*,
            pro_att.path as 'pro_img_path',
            cashier.full_name as 'cashier_full_name',
            clients_order.created_at as 'order_created_at'
               
            from clients_orders as clients_order
             
            INNER JOIN clients_bills as client_bill on (client_bill.client_bill_id= clients_order.client_bill_id AND client_bill.deleted_at is NULL )
            INNER JOIN products as pro on (clients_order.pro_id=pro.pro_id AND pro.deleted_at is NULL)
            left outer join attachments as pro_att on pro_att.id=pro.pro_img
            INNER JOIN users as cashier on (cashier.user_id=client_bill.cashier_id AND cashier.deleted_at is NULL) 
             
             
            #where
            where clients_order.deleted_at is NULL $additional_where "
            );

    }

    static public function get_bill_orders_group_by_product($additional_where=""){

        return
            DB::select("
            select 
            clients_order.*,
            pro.*,
            sum(order_quantity) as 'product_sum'
               
            from clients_orders as clients_order
             
            INNER JOIN clients_bills as client_bill on (client_bill.client_bill_id= clients_order.client_bill_id AND client_bill.deleted_at is NULL )
            INNER JOIN products as pro on (clients_order.pro_id=pro.pro_id AND pro.deleted_at is NULL)
             
             
            #where
            where clients_order.deleted_at is NULL $additional_where 
            
            GROUP BY pro.pro_id,client_bill.customer_id,clients_order.order_return
            "



            );

    }

    static public function get_bill_orders_group_by_bill($additional_where=""){

        return
            DB::select("
            select 
            clients_order.*,
            pro.*,
            sum(order_quantity) as 'product_sum'
               
            from clients_orders as clients_order
             
            INNER JOIN clients_bills as client_bill on (client_bill.client_bill_id= clients_order.client_bill_id AND client_bill.deleted_at is NULL )
            INNER JOIN products as pro on (clients_order.pro_id=pro.pro_id AND pro.deleted_at is NULL)
             
             
            #where
            where clients_order.deleted_at is NULL $additional_where 
            
            GROUP BY clients_order.client_bill_id,clients_order.order_return
            "

            );

    }



    static public function get_count()
    {
        return DB::table('clients_orders')->count();
    }

}
