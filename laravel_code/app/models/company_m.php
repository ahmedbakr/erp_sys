<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class company_m extends Model
{
    use SoftDeletes;

    protected $table = "company";
    protected $primaryKey = "company_id";
    public $timestamps = true;
    protected $dates = ["deleted_at"];

    protected $fillable = [
        'company_logo_id','company_name', 'company_name_en', 'company_tel', 'company_fax',
        'company_email', 'company_website', 'company_owner_name','company_short_desc','company_short_desc_en',
        'company_owner_name_en', 'company_card_id', 'company_address', 'company_address_en',
        'after_logout_text_1','after_logout_text_2','sell_tax_percent'
    ];


    static function get_data($additional_where = "", $order_by = "" , $limit = "")
    {
        $results = DB::select("
            select 
            comp.*,
            attach.id,
            attach.path, attach.alt , attach.title
             
            #joins
             from company as comp
             LEFT OUTER JOIN attachments as attach on (comp.company_logo_id = attach.id)

             #where
             where comp.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $results;

    }

}
