<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class banks_m extends Model
{
    protected $table = "banks";
    protected $primaryKey = "bank_id";
    public $timestamps = false;

    protected $fillable = [
        'bank_name', 'bank_atm_ratio'
    ];
}
