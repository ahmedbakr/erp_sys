<?php

namespace App\models\expenses;

use App\models\attachments_m;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class expenses_m extends Model
{
    use SoftDeletes;
    protected $table = "expenses";
    protected $primaryKey = "expense_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'expenses_maker','expense_name' , 'expense_desc' , 'expense_amount'
        ,'expense_date', 'expense_type_id' , 'expense_attachment','expense_status'
    ];

    public static function get_expenses($additional_where=""){
        $res=DB::select("
            select 
            ex.*,
            ex_type.*,
            user.full_name as 'branch_full_name',
            user.user_duty_balance as 'branch_duty_balance',
            expense_maker.full_name as 'expense_maker_name',
            expense_maker.user_duty_balance as 'user_duty_balance'
            
            from expenses as ex
            inner JOIN category as ex_type on ex.expense_type_id=ex_type.cat_id
            left outer join users as user on (user.user_id=ex.user_id)
            inner join users as expense_maker on(ex.expenses_maker=expense_maker.user_id)
            where ex.deleted_at is NULL $additional_where
        ");

        if (count($res)==1){

            $res=$res[0];

            $slider_ids = json_decode($res->expense_attachment);
            $res->slider_items = array();
            if (is_array($slider_ids)&&  count($slider_ids) >0) {

                $slider_imgs = attachments_m::whereIn("id",$slider_ids)->get()->all();
                $res->slider_items=$slider_imgs;
            }

        }

        return $res;
    }

}
