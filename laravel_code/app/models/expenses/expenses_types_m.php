<?php

namespace App\models\expenses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class expenses_types_m extends Model
{
    use SoftDeletes;
    protected $table = "expenses_types";
    protected $primaryKey = "expense_type_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'expense_type_name', 'afford_on_product' , 'afford_percentage'
    ];
}
