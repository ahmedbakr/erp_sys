<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class status_m extends Model
{
    protected $table = "status";
    protected $primaryKey = "status_id";
    public $timestamps = false;

    protected $fillable = [
        'status_name', 'status_label'
    ];
}
