<?php

namespace App\models\packages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class package_products_m extends Model
{

    protected $table = "package_products";
    protected $primaryKey = "bpp_id";
    public $timestamps = false;

    protected $fillable = [
        'package_id','pro_id'
    ];


}
