<?php

namespace App\models\packages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class branch_packages_m extends Model
{

    protected $table = "branch_packages";
    protected $primaryKey = "bp_id";
    public $timestamps = false;

    protected $fillable = [
        'package_id','branch_id'
    ];


    public static function get_branch_packages($where=""){

        return DB::select("
            select 
            branch_pac.*, 
            pac.*,
            branch_pro.pro_name as 'gift_name'
            
            from branch_packages as branch_pac
            inner join packages as pac on (branch_pac.package_id=pac.package_id AND pac.deleted_at is null)
            left OUTER JOIN products as branch_pro on (branch_pro.pro_id=pac.product_gift_id) 
            $where
        ");

    }


}
