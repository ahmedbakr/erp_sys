<?php

namespace App\models\packages;

use App\models\admin\category_m;
use App\models\product\products_m;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class packages_m extends Model
{
    use SoftDeletes;

    protected $table = "packages";
    protected $primaryKey = "package_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'package_cats','package_name', 'package_start_date', 'package_end_date', 'min_products_to_get_offer',
        'min_products_to_get_gift', 'product_gift_id', 'package_type','discount_percentage_for_product'
    ];


    public static function get_packages($additional_where=""){
        $res=DB::select("
            select 
            pack.*,
            gift_pro.pro_name,
            gift_pro.pro_barcode
            
            from packages as pack
            left OUTER JOIN products as gift_pro on (gift_pro.pro_id=pack.product_gift_id)
            
            where pack.deleted_at is NULL $additional_where
        ");


        if(isset_and_array($res)&&count($res)==1){
            //get package products
            $res[0]->package_pros=package_products_m::where("package_id",$res[0]->package_id)->get()->all();
            if(isset_and_array($res[0]->package_pros)){
                $res[0]->package_pros=convert_inside_obj_to_arr($res[0]->package_pros,"pro_id");
                $res[0]->package_pros=products_m::get_pros(" AND pro.pro_id in (".implode(",",$res[0]->package_pros).")");
            }

            //get package branches
            $res[0]->package_branches=branch_packages_m::where("package_id",$res[0]->package_id)->get()->all();
            if(isset_and_array($res[0]->package_branches)){
                $res[0]->package_branches=convert_inside_obj_to_arr($res[0]->package_branches,"branch_id");
                $res[0]->package_branches=User::whereIn("user_id",$res[0]->package_branches)->get()->all();
            }

            $res[0]->package_cats=json_decode($res[0]->package_cats);
            if(is_array($res[0]->package_cats)){
                $res[0]->package_cats=category_m::whereIn("cat_id",$res[0]->package_cats)->get()->all();
            }

        }

        return $res;
    }


    public function package_branches()
    {
        return $this->belongsToMany(
            'App\User', # like user roles table
            'branch_packages', # like user_role table
            'package_id', # this col in user_role table
            'branch_id' # this col in user roles table
        );
    }

    public function package_products()
    {
        return $this->belongsToMany(
            'App\models\product\products_m', # like user roles table
            'package_products', # like user_role table
            'package_id', # this col in user_role table
            'pro_id' # this col in user roles table
        );
    }


}
