<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class asset_category_m extends Model
{
    use SoftDeletes;

    protected $table = "asset_category";
    protected $primaryKey = "asset_cat_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_cat_code', 'asset_cat_name', 'asset_cat_name_en', 'parent_id'
    ];


    public static function get_cats($additional_where="",$get_object = false){
        $results = DB::select("
            select 
            
            #child cat
            cat.*,
            
            #parent cat
            ifnull(parent_cat.asset_cat_id ,0) as 'asset_parent_cat_id',
            ifnull(parent_cat.asset_cat_code ,0) as 'asset_parent_cat_code',
            ifnull(parent_cat.asset_cat_name ,0) as 'asset_parent_cat_name',
            ifnull(parent_cat.asset_cat_name_en ,0) as 'asset_parent_cat_name_en',
            ifnull(parent_cat.parent_id ,0) as 'parent_parent_id'
            
            from asset_category as cat

            LEFT JOIN asset_category as parent_cat
                on(cat.parent_id = parent_cat.asset_cat_id and parent_cat.deleted_at IS NULL )
            
            where cat.deleted_at is NULL $additional_where
        ");

        if (count($results) == 1 && $get_object)
        {
            return $results[0];
        }

        return $results;
    }

}
