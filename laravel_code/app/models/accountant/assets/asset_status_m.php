<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class asset_status_m extends Model
{
    use SoftDeletes;

    protected $table = "asset_status";
    protected $primaryKey = "asset_status_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_status_code','asset_status_name', 'asset_status_name_en'
    ];

}
