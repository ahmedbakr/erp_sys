<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class assets_transports_m extends Model
{

    use SoftDeletes;

    protected $table = "assets_transports";
    protected $primaryKey = "asset_transport_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_id', 'asset_transport_location_from', 'asset_transport_location_to',
        'asset_transport_type', 'asset_transport_date' ,'asset_transport_desc'
    ];

    public static function get_data($additional_where="",$get_object = false){
        $results = DB::select("
            select 
            
            transport.*,
            asset.*,
            asset.created_at as 'asset_created_at',
            
            #location
            asset_location.*,
            
            
            #transport_location_from
            transport_location_from.asset_location_code as 'transport_location_from_code',
            transport_location_from.asset_location_name as 'transport_location_from_name',
            transport_location_from.asset_location_name_en as 'transport_location_from_name_en',
            
            #transport_location_to
            transport_location_to.asset_location_code as 'transport_location_to_code',
            transport_location_to.asset_location_name as 'transport_location_to_name',
            transport_location_to.asset_location_name_en as 'transport_location_to_en',
            
                        
            #status
            asset_status.*,
            
            #child cat
            asset_child_cat.asset_cat_id as 'asset_child_cat_id',
            asset_child_cat.asset_cat_code as 'asset_child_cat_code',
            asset_child_cat.asset_cat_name as 'asset_child_cat_name',
            asset_child_cat.asset_cat_name_en as 'asset_child_cat_name_en',
            asset_child_cat.parent_id as 'child_parent_id',
            
            #parent cat
            asset_parent_cat.asset_cat_id as 'asset_parent_cat_id',
            asset_parent_cat.asset_cat_code as 'asset_parent_cat_code',
            asset_parent_cat.asset_cat_name as 'asset_parent_cat_name',
            asset_parent_cat.asset_cat_name_en as 'asset_parent_cat_name_en',
            asset_parent_cat.parent_id as 'parent_parent_id',
            
            
            #asset_account
            asset_account.acc_code as 'asset_account_code',
            asset_account.acc_go_away as 'asset_account_go_away',
            asset_account.acc_name as 'asset_account_name',
            asset_account.acc_name_en as 'asset_account_name_en',
            asset_account.acc_money_type as 'asset_account_money_type',
            asset_account.acc_path as 'asset_account_path',
            asset_account.acc_path_en as 'asset_account_path_en',
            
            #asset_deprecation_account
            asset_deprecation_account.acc_code as 'asset_deprecation_account_code',
            asset_deprecation_account.acc_go_away as 'asset_deprecation_account_go_away',
            asset_deprecation_account.acc_name as 'asset_deprecation_account_name',
            asset_deprecation_account.acc_name_en as 'asset_deprecation_account_name_en',
            asset_deprecation_account.acc_money_type as 'asset_deprecation_account_money_type',
            asset_deprecation_account.acc_path as 'asset_deprecation_account_path',
            asset_deprecation_account.acc_path_en as 'asset_deprecation_account_path_en',
            
            #asset image
            asset_img.path as 'asset_img_path',
            asset_img.title as 'asset_img_title',
            asset_img.alt as 'asset_img_alt'
            
            from assets_transports as transport
            INNER JOIN assets as asset on (transport.asset_id = asset.asset_id AND asset.deleted_at IS NULL )
            
            INNER JOIN asset_locations as asset_location 
                on (asset.asset_location_id = asset_location.asset_location_id and asset_location.deleted_at is NULL )
            
            INNER JOIN asset_locations as transport_location_from 
                on (transport.asset_transport_location_from = transport_location_from.asset_location_id and transport_location_from.deleted_at is NULL )
           
            INNER JOIN asset_locations as transport_location_to 
                on (transport.asset_transport_location_to = transport_location_to.asset_location_id and transport_location_to.deleted_at is NULL )
           
            Left OUTER JOIN asset_status as asset_status
                on(asset.asset_status_id = asset_status.asset_status_id and asset_status.deleted_at is NULL )
                                
            INNER JOIN asset_category as asset_child_cat
                on(asset.asset_cat_id = asset_child_cat.asset_cat_id and asset_child_cat.deleted_at is NULL )
            
            INNER JOIN asset_category as asset_parent_cat
                on(asset_child_cat.parent_id = asset_parent_cat.asset_cat_id and asset_parent_cat.deleted_at IS NULL )
                
            #TODO make Inner Join to general_accounts
            LEFT JOIN general_accounts as asset_account
                on(asset.asset_acc = asset_account.acc_id and asset_account.deleted_at IS NULL )
                
            LEFT JOIN general_accounts as asset_deprecation_account
                on(asset.asset_deprecation_acc = asset_deprecation_account.acc_id and asset_deprecation_account.deleted_at IS NULL )
                
            LEFT JOIN attachments as asset_img
                on(asset.asset_img_id = asset_img.id)
            
            where transport.deleted_at is NULL $additional_where
            
        ");

        if (count($results) == 1 && $get_object)
        {
            return $results[0];
        }

        return $results;

    }

}
