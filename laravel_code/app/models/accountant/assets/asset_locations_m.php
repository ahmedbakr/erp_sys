<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class asset_locations_m extends Model
{
    use SoftDeletes;

    protected $table = "asset_locations";
    protected $primaryKey = "asset_location_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_location_code', 'asset_location_name', 'asset_location_name_en'
    ];

}
