<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class asset_documents_m extends Model
{

    use SoftDeletes;

    protected $table = "asset_documents";
    protected $primaryKey = "asset_doc_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_id', 'doc_id'
    ];

}
