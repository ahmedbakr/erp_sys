<?php

namespace App\models\accountant\assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class assets_m extends Model
{
    use SoftDeletes;

    protected $table = "assets";
    protected $primaryKey = "asset_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'asset_cat_id', 'asset_location_id', 'asset_status_id', 'asset_code',
        'asset_title',
        'asset_title_en', 'asset_desc', 'asset_desc_en', 'asset_buy_source', 'asset_buy_source_en',
        'asset_deprecation_type', 'asset_model_num', 'asset_piece_number',
        'asset_barcode', 'asset_responsable',
        'asset_working_units', 'asset_buy_date', 'asset_buy_amount', 'asset_expected_sell_amount', 'asset_age',
        'asset_acc', 'asset_deprecation_acc', 'asset_all_deprecation_values', 'asset_changes_value',
        'asset_assurnace_period', 'asset_img_id', 'asset_total_expenses', 'asset_total_incomes',
        'asset_guard_name', 'asset_address',
        'asset_license_number', 'asset_east_edge', 'asset_east_edge_en', 'asset_west_edge',
        'asset_west_edge_en', 'asset_north_edge', 'asset_north_edge_en', 'asset_south_edge',
        'asset_south_edge_en','asset_is_sold','asset_is_rubbish','asset_on_maintenance'
    ];


    public static function get_assets($additional_where="",$get_object = false){
        $results = DB::select("
            select 
            asset.*,
            asset.created_at as 'asset_created_at',
            
            #location
            asset_location.*,
            
            #status
            asset_status.*,
            
            #child cat
            asset_child_cat.asset_cat_id as 'asset_child_cat_id',
            asset_child_cat.asset_cat_code as 'asset_child_cat_code',
            asset_child_cat.asset_cat_name as 'asset_child_cat_name',
            asset_child_cat.asset_cat_name_en as 'asset_child_cat_name_en',
            asset_child_cat.parent_id as 'child_parent_id',
            
            #parent cat
            asset_parent_cat.asset_cat_id as 'asset_parent_cat_id',
            asset_parent_cat.asset_cat_code as 'asset_parent_cat_code',
            asset_parent_cat.asset_cat_name as 'asset_parent_cat_name',
            asset_parent_cat.asset_cat_name_en as 'asset_parent_cat_name_en',
            asset_parent_cat.parent_id as 'parent_parent_id',
            
            
            #asset_account
            asset_account.acc_code as 'asset_account_code',
            asset_account.acc_go_away as 'asset_account_go_away',
            asset_account.acc_name as 'asset_account_name',
            asset_account.acc_name_en as 'asset_account_name_en',
            asset_account.acc_money_type as 'asset_account_money_type',
            asset_account.acc_path as 'asset_account_path',
            asset_account.acc_path_en as 'asset_account_path_en',
            
            #asset_deprecation_account
            asset_deprecation_account.acc_code as 'asset_deprecation_account_code',
            asset_deprecation_account.acc_go_away as 'asset_deprecation_account_go_away',
            asset_deprecation_account.acc_name as 'asset_deprecation_account_name',
            asset_deprecation_account.acc_name_en as 'asset_deprecation_account_name_en',
            asset_deprecation_account.acc_money_type as 'asset_deprecation_account_money_type',
            asset_deprecation_account.acc_path as 'asset_deprecation_account_path',
            asset_deprecation_account.acc_path_en as 'asset_deprecation_account_path_en',
            
            #asset image
            asset_img.path as 'asset_img_path',
            asset_img.title as 'asset_img_title',
            asset_img.alt as 'asset_img_alt'
            
            from assets as asset
            INNER JOIN asset_locations as asset_location 
                on (asset.asset_location_id = asset_location.asset_location_id and asset_location.deleted_at is NULL )
            
            Left OUTER JOIN asset_status as asset_status
                on(asset.asset_status_id = asset_status.asset_status_id and asset_status.deleted_at is NULL )
                    
            INNER JOIN asset_category as asset_child_cat
                on(asset.asset_cat_id = asset_child_cat.asset_cat_id and asset_child_cat.deleted_at is NULL )
            
            INNER JOIN asset_category as asset_parent_cat
                on(asset_child_cat.parent_id = asset_parent_cat.asset_cat_id and asset_parent_cat.deleted_at IS NULL )
                
            #TODO make Inner Join to general_accounts
            LEFT JOIN general_accounts as asset_account
                on(asset.asset_acc = asset_account.acc_id and asset_account.deleted_at IS NULL )
                
            LEFT JOIN general_accounts as asset_deprecation_account
                on(asset.asset_deprecation_acc = asset_deprecation_account.acc_id and asset_deprecation_account.deleted_at IS NULL )
                
            LEFT JOIN attachments as asset_img
                on(asset.asset_img_id = asset_img.id)
            
            where asset.deleted_at is NULL $additional_where
        ");

        if (count($results) == 1 && $get_object)
        {
            return $results[0];
        }

        return $results;

    }

}
