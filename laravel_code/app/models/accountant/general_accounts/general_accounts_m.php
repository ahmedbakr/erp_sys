<?php

namespace App\models\accountant\general_accounts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class general_accounts_m extends Model
{
    use SoftDeletes;

    protected $table = "general_accounts";
    protected $primaryKey = "acc_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'acc_type','cat_id', 'acc_name', 'acc_name_en',
        'acc_code', 'acc_money_type',
        'acc_go_away', 'acc_path', 'acc_path_en'
    ];


    public static function get_general_accounts($additional_where=""){
        return DB::select("
            select 
            gen_acc.*,
            cat.*
            from general_accounts as gen_acc
            left outer join general_account_category as cat on (gen_acc.cat_id=cat.cat_id AND cat.deleted_at is null)
            where gen_acc.deleted_at is null $additional_where
            
        ");
    }

    public static function get_general_accounts_of_financial_year($additional_where=""){
        return DB::select("
            select 
            acc.*,
            fin_year_acc.fya_acc_value,
            fin_year.*
            from general_accounts as acc
            inner join financial_year_accounts as fin_year_acc on (fin_year_acc.fya_acc_id=acc.acc_id AND fin_year_acc.deleted_at is NULL )
            inner join financial_year as fin_year on (fin_year.fin_id=fin_year_acc.fya_fin_year_id AND fin_year.deleted_at is NULL )
            where acc.deleted_at is null $additional_where
        ");
    }






}
