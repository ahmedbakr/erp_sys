<?php

namespace App\models\accountant\general_accounts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class general_account_category_m extends Model
{
    use SoftDeletes;

    protected $table = "general_account_category";
    protected $primaryKey = "cat_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'cat_type','cat_code', 'cat_name', 'cat_name_en',
        'parent_id', 'cat_path', 'cat_path_en'
    ];


    public static function get_cats($additional_where=""){
        return DB::select("
            select 
            cat.*,
            parent_cat.cat_name as 'parent_cat_name', 
            parent_cat.cat_name_en as 'parent_cat_name_en'
             
            from general_account_category as cat
            left outer join general_account_category as parent_cat on (cat.parent_id=parent_cat.cat_id AND parent_cat.deleted_at is NULL )
            where cat.deleted_at is NULL $additional_where
            
        ");
    }

    public static function check_in_parent_node_contain_node($parent_node_id,$search_node_id){
        $res=self::whereIn("parent_id",$parent_node_id)->get()->pluck("cat_id")->all();

        if(count($res)){
            if(in_array($search_node_id,$res)){
                return true;
            }
            else{
                return self::check_in_parent_node_contain_node($res,$search_node_id);
            }
        }
        else{
            return false;
        }
    }


    public static function get_all_accounts_in_branch($parent_node_id){


        $cats=self::whereIn("parent_id",$parent_node_id)->get()->pluck("cat_id")->all();
        $accounts=general_accounts_m::whereIn("cat_id",$parent_node_id)->pluck("acc_id")->all();


        $all_accounts_ids=[];
        $all_accounts_ids=array_merge($all_accounts_ids,$accounts);

        if(count($cats)){
            $all_accounts_ids=array_merge($all_accounts_ids,self::get_all_accounts_in_branch($cats));
        }


        return $all_accounts_ids;

    }



}
