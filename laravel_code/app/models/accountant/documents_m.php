<?php

namespace App\models\accountant;

use App\models\attachments_m;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class documents_m extends Model
{
    use SoftDeletes;

    protected $table = "documents";
    protected $primaryKey = "doc_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'doc_type', 'doc_code', 'doc_start_date', 'doc_end_date', 'doc_source', 'doc_desc', 'doc_files'
    ];



    static function get_docs($additional_where = "")
    {
        $results = DB::select("
            
            select 
            doc.*
            
            from documents as doc
            
            #where
            where doc.deleted_at is null  $additional_where
        
        ");

        $new_docs=array();

        foreach($results as $key => $doc)
        {

            //get slider data
            $slider_ids = json_decode($doc->doc_files);
            $doc->slider_imgs = array();
            if (is_array($slider_ids)&&  count($slider_ids) >0) {

                $slider_imgs = attachments_m::whereIn("id",$slider_ids)->get()->all();
                $doc->slider_imgs=$slider_imgs;
            }

            $new_docs[]=$doc;

        }
        $results=$new_docs;

        return $results;
    }

}
