<?php

namespace App\models\accountant\financial_years;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class financial_year_m extends Model
{
    use SoftDeletes;

    protected $table = "financial_year";
    protected $primaryKey = "fin_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'fin_label', 'fin_start_range', 'fin_end_range','fin_closed'
    ];


}
