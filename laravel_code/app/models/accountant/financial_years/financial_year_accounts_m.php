<?php

namespace App\models\accountant\financial_years;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class financial_year_accounts_m extends Model
{
    use SoftDeletes;

    protected $table = "financial_year_accounts";
    protected $primaryKey = "fya_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'fya_fin_year_id', 'fya_acc_id', 'fya_acc_value'
    ];


}
