<?php

namespace App\models\accountant\entries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class entry_items_m extends Model
{
    use SoftDeletes;

    protected $table = "entry_items";
    protected $primaryKey = "item_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'entry_id', 'acc_id', 'credit_or_debit', 'item_amount','item_created_at'
    ];


    public static function get_accounts_debit_and_credit_values($acc_ids,$additional_where=""){

        if(!isset_and_array($acc_ids)){
            return [];
        }


        return DB::select("
            select 
            entry_item.*,
            sum(entry_item.item_amount) as 'item_sum_amount'
            
            from entry_items as entry_item
            inner join entries as entry on (entry.entry_id=entry_item.entry_id AND entry.deleted_at is NULL )
            inner join general_accounts as acc on (acc.acc_id=entry_item.acc_id AND acc.deleted_at is NULL )
            
            where 
                entry_item.deleted_at is NULL 
                AND entry_item.acc_id in (".implode(",",$acc_ids).")
                $additional_where
            
            group by entry_item.acc_id,entry_item.credit_or_debit
            
            
        ");

    }


}
