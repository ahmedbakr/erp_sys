<?php

namespace App\models\accountant\entries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class entry_templates_m extends Model
{
    use SoftDeletes;

    protected $table = "entry_templates";
    protected $primaryKey = "template_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        "template_name"
    ];




}
