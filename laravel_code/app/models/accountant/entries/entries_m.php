<?php

namespace App\models\accountant\entries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class entries_m extends Model
{
    use SoftDeletes;

    protected $table = "entries";
    protected $primaryKey = "entry_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
         'entry_day_help', 'entry_type', 'entry_desc', 'entry_approved', 'doc_id','entry_created_at'
    ];


}
