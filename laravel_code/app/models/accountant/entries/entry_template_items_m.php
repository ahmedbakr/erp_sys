<?php

namespace App\models\accountant\entries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class entry_template_items_m extends Model
{
    use SoftDeletes;

    protected $table = "entry_template_items";
    protected $primaryKey = "t_item_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'entry_template_id','t_item_acc_id', 't_item_acc_debit_or_credit', 't_item_value_type', 't_item_value'
    ];


    public static function get_template_items($additional_condition=""){

        return DB::select("
            select 
            items.*,
            acc.acc_name,
            acc.acc_name_en,
            acc.acc_id
            
            from entry_template_items as items
            inner join general_accounts as acc on (acc.acc_id=items.t_item_acc_id AND acc.deleted_at is null)
            where items.deleted_at is NULL $additional_condition
        ");


    }


}
