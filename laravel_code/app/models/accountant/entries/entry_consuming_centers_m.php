<?php

namespace App\models\accountant\entries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class entry_consuming_centers_m extends Model
{
    use SoftDeletes;

    protected $table = "entry_consuming_centers";
    protected $primaryKey = "ecc_id";
    protected $dates = ["deleted_at"];
    public $timestamps = true;

    protected $fillable = [
        'entry_item_id','consuming_acc_id','acc_value'
    ];


}
