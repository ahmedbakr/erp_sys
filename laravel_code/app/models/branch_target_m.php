<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class branch_target_m extends Model
{

    protected $table = "branch_target";
    protected $primaryKey = "branch_target_id";
    public $timestamps = false;

    protected $fillable = [
        'branch_id','month','year','target_amount'
    ];


    public static function get_data($additional_where=""){
        return DB::select("
            select 
                b_target.*,
                branch.*
                
            from branch_target as b_target
            INNER JOIN users as branch on (b_target.branch_id = branch.user_id and branch.deleted_at is null)
            $additional_where
        ");
    }


}
