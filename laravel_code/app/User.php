<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;

    protected $fillable = [
        'email', 'password',
        'user_type','related_id','point_of_sale_id',
        'user_code','full_name_en','start_join_date',
        'full_name','role',
        'logo_id','user_active','user_can_login','contacts',
        'user_balance','user_commission_days','user_atm_balance',
        'user_duty_balance','user_tel','user_address',
        'user_mobile','user_fax',
        'bank_id','selected_price_id',
        'age_range','user_gender','last_login_date','how_customer_know_us',
        'identification_number','nationality','identification_expire_date',
        'user_job_on_passport','user_job_on_company','commercial_record_number',
        'commercial_record_expire_date','have_all_permissions'

    ];

    protected $primaryKey = 'user_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $dates = ["deleted_at"];


    public static function get_all_users($additional_where){

        return DB::select("
            select 
            user.user_tel as 'id',
            user.*,
            att.path as 'logo_path',
            branch.full_name as 'related_branch_name',
            branch.full_name_en as 'related_branch_name_en'
            
            from users as user 
            left OUTER JOIN attachments as att on user.logo_id=att.id
            left outer join users as branch on user.related_id=branch.user_id
            
            where user.deleted_at is NULL $additional_where
        ");


    }

    static function get_users($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
            select 
            u.*,
            attach.id,
            attach.path, attach.alt , attach.title
             
            ,point.*
             
            #joins
             from users as u
             LEFT OUTER JOIN attachments as attach on (u.logo_id = attach.id)
             LEFT OUTER JOIN points_of_sale as point on (u.point_of_sale_id = point.point_of_sale_id)

             #where
             where u.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;

    }


    public function getContactsAttribute($value)
    {
        if (json_decode($value) != false) {
            return json_decode($value);
        }
        return [];
    }

    public static function get_all_branch_customers($additional_where=""){

        return DB::select("
            select 
            client.*,
            (select max(client_bill_date) from clients_bills where customer_id=client.user_id) as 'last_bill_date',
           
            (SELECT  sum(m_t_amount) from money_transfers where from_user_id=client.user_id AND m_t_status=1 AND money_transfers.m_t_amount_type='cash') as 'total_payed_cash',
            (SELECT  sum(m_t_amount) from money_transfers where from_user_id=client.user_id AND m_t_status=1 AND money_transfers.m_t_amount_type='atm') as 'total_payed_atm',
           
            (SELECT  sum(m_t_amount) from money_transfers where to_user_id=client.user_id AND m_t_status=1 AND money_transfers.m_t_amount_type='cash') as 'total_received_cash',
            (SELECT  sum(m_t_amount) from money_transfers where to_user_id=client.user_id AND m_t_status=1 AND money_transfers.m_t_amount_type='atm') as 'total_received_atm',
           
            (SELECT  sum(m_t_amount) from money_transfers where to_user_id=client.user_id AND m_t_status=0) as 'total_remain_for_him',
            (SELECT  sum(m_t_amount) from money_transfers where from_user_id=client.user_id AND m_t_status=0) as 'total_remain_for_branch',
            
            (select COUNT(uas_id) from user_as_sponsor as spon WHERE spon.sponsor_id=client.user_id AND spon.last_earn_gift_date>CURDATE()) as 'user_as_sponsor_count',
            
            

            price_list.price_title as 'client_price_title'
            
            
            
            
            from users as client
            left outer join price_list on client.selected_price_id=price_list.price_id
            where client.deleted_at is NULL $additional_where
        ");

    }

    public static function get_user_money_transfer_statistics($additional_where){
        return self::get_all_branch_customers($additional_where);
    }



}
