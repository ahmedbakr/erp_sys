-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 10:31 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `commission_rules`
--

CREATE TABLE IF NOT EXISTS `commission_rules` (
  `commission_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `minimum_day_earn` decimal(10,2) NOT NULL,
  `total_commission_value` decimal(10,2) NOT NULL,
  `branch_commission_value` decimal(10,2) NOT NULL,
  `factory_commission_value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commission_rules`
--

INSERT INTO `commission_rules` (`commission_id`, `branch_id`, `minimum_day_earn`, `total_commission_value`, `branch_commission_value`, `factory_commission_value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 18, '260.00', '20.00', '10.00', '10.00', NULL, '2017-06-06 08:19:49', NULL),
(2, 18, '500.00', '55.00', '30.00', '25.00', '2017-03-06 09:48:16', '2017-06-06 08:20:25', NULL),
(3, 18, '100.00', '13.00', '6.00', '7.00', NULL, '2017-06-06 08:20:30', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `commission_rules`
--
ALTER TABLE `commission_rules`
  ADD PRIMARY KEY (`commission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `commission_rules`
--
ALTER TABLE `commission_rules`
  MODIFY `commission_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
