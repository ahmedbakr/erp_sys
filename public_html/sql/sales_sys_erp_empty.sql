-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 05, 2017 at 09:00 AM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sales_sys_erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_cat_id` int(11) NOT NULL,
  `asset_location_id` int(11) NOT NULL,
  `asset_status_id` int(11) NOT NULL,
  `asset_code` varchar(300) NOT NULL,
  `asset_title` varchar(300) NOT NULL,
  `asset_title_en` varchar(300) NOT NULL,
  `asset_desc` text NOT NULL,
  `asset_desc_en` text NOT NULL,
  `asset_buy_source` varchar(300) NOT NULL COMMENT 'جهة الشراء',
  `asset_buy_source_en` text NOT NULL,
  `asset_deprecation_type` varchar(300) NOT NULL,
  `asset_model_num` varchar(300) NOT NULL,
  `asset_piece_number` varchar(300) NOT NULL,
  `asset_barcode` varchar(300) NOT NULL,
  `asset_responsable` varchar(300) NOT NULL,
  `asset_working_units` int(11) NOT NULL,
  `asset_buy_date` date NOT NULL,
  `asset_buy_amount` decimal(10,2) NOT NULL COMMENT 'القيمة الدفترية',
  `asset_expected_sell_amount` decimal(10,2) NOT NULL,
  `asset_age` int(11) NOT NULL,
  `asset_acc` int(11) NOT NULL,
  `asset_deprecation_acc` int(11) NOT NULL,
  `asset_all_deprecation_values` decimal(10,2) NOT NULL,
  `asset_changes_value` decimal(10,2) NOT NULL COMMENT 'قيمة التغييرات',
  `asset_assurnace_period` int(11) NOT NULL,
  `asset_img_id` int(11) NOT NULL,
  `asset_total_expenses` decimal(10,2) NOT NULL,
  `asset_total_incomes` decimal(10,2) NOT NULL,
  `asset_guard_name` varchar(300) NOT NULL,
  `asset_address` text NOT NULL,
  `asset_license_number` varchar(300) NOT NULL,
  `asset_east_edge` varchar(300) NOT NULL,
  `asset_east_edge_en` varchar(300) NOT NULL,
  `asset_west_edge` varchar(300) NOT NULL,
  `asset_west_edge_en` varchar(300) NOT NULL,
  `asset_north_edge` varchar(300) NOT NULL,
  `asset_north_edge_en` varchar(300) NOT NULL,
  `asset_south_edge` varchar(300) NOT NULL,
  `asset_south_edge_en` varchar(300) NOT NULL,
  `asset_is_sold` tinyint(1) NOT NULL COMMENT 'ده لو الاصل اتباع او دخل ليه اقساط بيع واتحصلت كلها',
  `asset_is_rubbish` tinyint(1) NOT NULL COMMENT 'هل هذا الاصل تم تكهينه ام لا',
  `asset_on_maintenance` tinyint(1) NOT NULL COMMENT 'هل هذا الاصل في الصيانة ام لا',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets_deprecation_rules`
--

CREATE TABLE IF NOT EXISTS `assets_deprecation_rules` (
  `asset_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `asset_rule_from_date` date NOT NULL,
  `asset_rule_to_date` date NOT NULL,
  `real_working_units` int(11) NOT NULL,
  `is_closed_to_edit` tinyint(1) NOT NULL,
  `asset_deprecation_months` int(11) NOT NULL COMMENT 'عدد شهور الاستخدام',
  `asset_deprecation_start_amount` decimal(10,3) NOT NULL COMMENT 'قيمة اول مده',
  `assets_deprecation_rate` decimal(10,3) NOT NULL COMMENT 'نسبة الاهلاك',
  `assets_deprecation_value` decimal(10,3) NOT NULL COMMENT 'قسط الاهلاك',
  `assets_deprecation_total_value` decimal(10,3) NOT NULL COMMENT 'مجمع الإهلاك',
  `asset_deprecation_end_amount` decimal(10,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_rule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `assets_deprecation_rules`
--

INSERT INTO `assets_deprecation_rules` (`asset_rule_id`, `asset_id`, `asset_rule_from_date`, `asset_rule_to_date`, `real_working_units`, `is_closed_to_edit`, `asset_deprecation_months`, `asset_deprecation_start_amount`, `assets_deprecation_rate`, `assets_deprecation_value`, `assets_deprecation_total_value`, `asset_deprecation_end_amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(46, 4, '2020-01-01', '2020-12-31', 0, 0, 12, '33233.903', '20.004', '6648.110', '34514.207', '26585.793', NULL, '2017-04-12 14:00:54', NULL),
(47, 4, '2021-01-01', '2021-12-31', 0, 0, 12, '26585.793', '20.004', '5318.222', '39832.429', '21267.571', NULL, '2017-04-12 12:17:02', NULL),
(48, 4, '2022-01-01', '2022-04-02', 0, 0, 4, '21267.571', '6.668', '1418.122', '41250.551', '19849.449', NULL, '2017-04-12 12:17:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assets_on_maintenance`
--

CREATE TABLE IF NOT EXISTS `assets_on_maintenance` (
  `asset_o_m_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `maintenance_start_date` date NOT NULL,
  `maintenance_days` int(11) NOT NULL,
  `broken_desc` text NOT NULL COMMENT 'وصف العطل',
  `what_is_fix` text NOT NULL COMMENT 'ما تم إصلاحه',
  `status_after_maintenance` int(11) NOT NULL COMMENT 'الحاله بعد الصيانة',
  `maintenance_note` text NOT NULL,
  `is_maintenance_finish` tinyint(1) NOT NULL COMMENT 'هل الصيانه انتهت للأصل',
  `maintenance_finish_date` date DEFAULT NULL COMMENT 'تاريخ إنتهاء الصيانة',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_o_m_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets_rent`
--

CREATE TABLE IF NOT EXISTS `assets_rent` (
  `asset_rent_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `assets_rent_start_date` date NOT NULL,
  `assets_rent_end_date` date NOT NULL,
  `assets_rent_period_type` varchar(200) NOT NULL COMMENT 'يوم او شهر او سنة',
  `assets_rent_period_number` int(11) NOT NULL COMMENT ' عدد الايام او الشهور او السنين',
  `assets_rent_amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(200) NOT NULL COMMENT 'cash or network',
  `assets_rent_tenant_name` varchar(300) NOT NULL,
  `assets_rent_desc` text NOT NULL COMMENT 'البيان',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_rent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `assets_rent`
--

INSERT INTO `assets_rent` (`asset_rent_id`, `asset_id`, `assets_rent_start_date`, `assets_rent_end_date`, `assets_rent_period_type`, `assets_rent_period_number`, `assets_rent_amount`, `payment_method`, `assets_rent_tenant_name`, `assets_rent_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '2017-04-11', '2017-08-11', 'month', 4, '5000.00', 'cash', 'ahmed', 'slkjdlkasjd', '2017-04-11 16:41:17', '2017-04-11 16:41:17', NULL),
(3, 3, '2017-08-11', '2017-09-11', 'month', 1, '1200.00', 'network', 'mohamed', 'asdasd', '2017-04-11 16:44:16', '2017-04-11 16:44:16', NULL),
(4, 7, '2017-04-11', '2017-05-26', 'day', 45, '800.00', 'network', 'mohamed', 'asdsad', '2017-04-11 16:45:10', '2017-04-11 16:45:10', NULL),
(5, 7, '2017-05-26', '2017-07-10', 'day', 45, '800.00', 'cash', 'aly', 'sadsad', '2017-04-11 16:46:08', '2017-04-11 16:49:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assets_rubbish`
--

CREATE TABLE IF NOT EXISTS `assets_rubbish` (
  `asset_rubbish_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `asset_rubbish_date` date NOT NULL,
  `asset_rubbish_amount` decimal(10,2) NOT NULL,
  `asset_rubbish_reasons` text NOT NULL,
  `asset_rubbish_desc` text NOT NULL COMMENT 'البيان',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_rubbish_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `assets_rubbish`
--

INSERT INTO `assets_rubbish` (`asset_rubbish_id`, `asset_id`, `asset_rubbish_date`, `asset_rubbish_amount`, `asset_rubbish_reasons`, `asset_rubbish_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, '2017-04-11', '5000.00', 'اي سبب', 'اي بيان', '2017-04-11 08:18:44', '2017-04-11 08:18:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assets_sold`
--

CREATE TABLE IF NOT EXISTS `assets_sold` (
  `asset_sold_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `sell_date` date NOT NULL,
  `sell_amount` decimal(10,2) NOT NULL,
  `buyer_name` varchar(300) NOT NULL,
  `desc_note` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_sold_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `assets_sold`
--

INSERT INTO `assets_sold` (`asset_sold_id`, `asset_id`, `sell_date`, `sell_amount`, `buyer_name`, `desc_note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '2017-04-09', '6000.00', 'ملكش دعوة', 'مصمم برده مش هقولك', '2017-04-10 13:27:28', '2017-04-10 13:27:28', NULL),
(2, 6, '2017-04-11', '20000.00', 'asdasdasd', 'sadasdasd', '2017-04-10 13:32:26', '2017-04-10 13:32:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assets_that_reevaluated`
--

CREATE TABLE IF NOT EXISTS `assets_that_reevaluated` (
  `reev_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `change_date` date NOT NULL COMMENT 'تاريخ التغيير',
  `value_before_change` decimal(10,2) NOT NULL COMMENT 'القيمة قبل التغيير',
  `change_value` decimal(10,2) NOT NULL COMMENT 'قيمة التغيير',
  `value_after_change` int(11) NOT NULL COMMENT 'القيمة بعد التغيير',
  `operation_desc` text NOT NULL COMMENT 'وصف العملية',
  `note_desc` text NOT NULL COMMENT 'بيان',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `assets_that_reevaluated`
--

INSERT INTO `assets_that_reevaluated` (`reev_id`, `asset_id`, `change_date`, `value_before_change`, `change_value`, `value_after_change`, `operation_desc`, `note_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '2017-04-12', '15000.00', '5000.00', 10000, 'sadsad', 'asdasd', '2017-04-12 15:11:00', '2017-04-12 15:11:00', NULL),
(3, 3, '2017-04-17', '10000.00', '5000.00', 5000, 'sdasd', 'asdasd', '2017-04-12 15:15:25', '2017-04-12 15:15:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assets_transports`
--

CREATE TABLE IF NOT EXISTS `assets_transports` (
  `asset_transport_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `asset_transport_location_from` int(11) NOT NULL,
  `asset_transport_location_to` int(11) NOT NULL,
  `asset_transport_type` varchar(200) NOT NULL COMMENT 'مؤقت او دائم',
  `asset_transport_date` date NOT NULL,
  `asset_transport_desc` text NOT NULL COMMENT 'بيان',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_transport_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `assets_transports`
--

INSERT INTO `assets_transports` (`asset_transport_id`, `asset_id`, `asset_transport_location_from`, `asset_transport_location_to`, `asset_transport_type`, `asset_transport_date`, `asset_transport_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, 2, 'always', '2017-04-12', 'بيان', '2017-04-11 12:40:38', '2017-04-11 12:50:38', NULL),
(2, 3, 2, 1, 'always', '2017-04-11', 'sadsad', '2017-04-11 12:49:48', '2017-04-11 13:03:26', NULL),
(3, 4, 2, 2, 'temp', '2017-04-11', 'sdsad', '2017-04-11 12:56:12', '2017-04-11 12:57:06', NULL),
(4, 3, 1, 3, 'always', '2017-04-13', 'استقر هنا هو ', '2017-04-11 13:05:30', '2017-04-11 13:05:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_category`
--

CREATE TABLE IF NOT EXISTS `asset_category` (
  `asset_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_cat_code` varchar(300) NOT NULL,
  `asset_cat_name` varchar(300) NOT NULL,
  `asset_cat_name_en` varchar(300) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `asset_category`
--

INSERT INTO `asset_category` (`asset_cat_id`, `asset_cat_code`, `asset_cat_name`, `asset_cat_name_en`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '123456', 'الات', 'tools', 0, '2017-03-30 13:50:48', '2017-03-30 13:50:48', NULL),
(2, '1000', 'الات زراعية', '', 1, '2017-03-30 14:03:09', '2017-03-30 14:31:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_documents`
--

CREATE TABLE IF NOT EXISTS `asset_documents` (
  `asset_doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_doc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `asset_documents`
--

INSERT INTO `asset_documents` (`asset_doc_id`, `asset_id`, `doc_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 3, NULL, NULL, NULL),
(2, 4, 4, NULL, NULL, NULL),
(5, 4, 5, NULL, '2017-04-05 08:03:46', NULL),
(6, 4, 6, '2017-04-05 09:54:55', '2017-04-05 09:54:55', NULL),
(7, 4, 7, '2017-04-05 09:58:36', '2017-04-05 09:58:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_expenses_and_incomes`
--

CREATE TABLE IF NOT EXISTS `asset_expenses_and_incomes` (
  `asset_exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_or_income` varchar(200) NOT NULL COMMENT 'مصروفات او ايرادات',
  `asset_exp_date` date NOT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_exp_value` decimal(10,2) NOT NULL,
  `asset_exp_desc` text NOT NULL COMMENT 'البيان',
  `receipt_number` varchar(300) NOT NULL COMMENT 'رقم الإيصال',
  `payment_method` varchar(200) NOT NULL COMMENT 'طريقة الدفع',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_exp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `asset_expenses_and_incomes`
--

INSERT INTO `asset_expenses_and_incomes` (`asset_exp_id`, `expense_or_income`, `asset_exp_date`, `asset_id`, `asset_exp_value`, `asset_exp_desc`, `receipt_number`, `payment_method`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'expense', '2017-04-02', 4, '500.00', 'شحن', '', 'cash', NULL, NULL, NULL),
(2, 'expense', '2017-04-03', 4, '600.00', 'نقل', '', 'cash', NULL, NULL, NULL),
(3, 'expense', '2017-04-04', 10, '1200.00', 'asas', '', 'cash', NULL, NULL, NULL),
(4, 'income', '2017-04-03', 10, '800.00', 'wqwqwq', '', 'network', NULL, NULL, NULL),
(5, 'income', '2017-04-04', 11, '500.00', '', '', 'network', NULL, NULL, NULL),
(6, 'expense', '2017-04-11', 12, '500.00', 'sadasd', '123456', 'cash', NULL, NULL, NULL),
(7, 'expense', '2017-04-11', 12, '600.00', 'aaa', '1111', 'network', NULL, '2017-04-11 15:10:19', NULL),
(8, 'expense', '2017-04-11', 12, '900.00', 'aaaaa', '2222', 'network', '2017-04-11 15:16:58', '2017-04-11 15:16:58', NULL),
(9, 'income', '2017-04-11', 12, '1500.00', 'asdasdas', '12331', 'cash', '2017-04-11 15:18:35', '2017-04-11 15:18:35', NULL),
(10, 'income', '2017-04-11', 12, '1500.00', 'sadasd', '4654546', 'cash', '2017-04-11 15:19:51', '2017-04-11 15:20:04', '2017-04-11 15:20:04'),
(11, 'expense', '2017-04-12', 13, '500.00', 'sadsad', '123456', 'cash', NULL, NULL, NULL),
(12, 'expense', '2017-04-12', 13, '500.00', 'aaa', '23456', 'cash', NULL, NULL, NULL),
(13, 'expense', '2017-04-12', 14, '1000.00', 'asdasd', 'asdasd', 'cash', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_installment`
--

CREATE TABLE IF NOT EXISTS `asset_installment` (
  `asset_install_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `asset_install_type` varchar(200) NOT NULL COMMENT 'sell or purchase',
  `asset_install_desc` text NOT NULL,
  `asset_install_date` date NOT NULL COMMENT 'تاريخ الاستحقاق',
  `asset_install_value` decimal(10,2) NOT NULL,
  `asset_install_additions` decimal(10,2) NOT NULL COMMENT 'الاضافات',
  `asset_install_discount` decimal(10,2) NOT NULL COMMENT 'الخصميات',
  `asset_install_allowed_days` int(11) NOT NULL COMMENT 'مده السماح بالايام',
  `asset_install_delay_fine` decimal(10,2) NOT NULL COMMENT 'غرامة التأخير بال %',
  `asset_install_total_value` decimal(10,2) NOT NULL COMMENT 'الإجمالي',
  `asset_install_payed_value` decimal(10,2) NOT NULL COMMENT 'قيمة المسدد',
  `asset_install_payed_date` date DEFAULT NULL COMMENT 'تاريخ التسديد',
  `asset_install_remained` decimal(10,2) NOT NULL COMMENT 'المتبقي',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_install_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `asset_locations`
--

CREATE TABLE IF NOT EXISTS `asset_locations` (
  `asset_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_location_code` varchar(300) NOT NULL,
  `asset_location_name` varchar(300) NOT NULL,
  `asset_location_name_en` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `asset_locations`
--

INSERT INTO `asset_locations` (`asset_location_id`, `asset_location_code`, `asset_location_name`, `asset_location_name_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '000', 'الادارة', 'manager', '2017-03-30 14:50:43', '2017-03-30 14:50:43', NULL),
(2, '001', 'فرع 1', 'branch 1', '2017-03-30 14:51:51', '2017-03-30 14:56:59', NULL),
(3, '002', 'فرع 2', 'branch 2', '2017-03-30 14:51:51', '2017-03-30 14:56:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asset_status`
--

CREATE TABLE IF NOT EXISTS `asset_status` (
  `asset_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_status_code` varchar(300) NOT NULL,
  `asset_status_name` varchar(200) NOT NULL,
  `asset_status_name_en` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `asset_status`
--

INSERT INTO `asset_status` (`asset_status_id`, `asset_status_code`, `asset_status_name`, `asset_status_name_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '001', 'في العمل', 'in work', '2017-03-30 15:09:31', '2017-03-30 15:10:31', NULL),
(2, '002', 'في الصيانة', 'in maintenance', '2017-03-30 15:09:57', '2017-03-30 15:09:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `alt` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `title`, `alt`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', '', 'uploads/att_slider/0a8c13706aff1002e715455ef26ac917.png', '2017-01-23 14:53:38', '2017-01-23 14:53:38', NULL),
(2, '', '', 'uploads/att_slider/c3ff91191e3567cffe77067b9f2796d5.jpg', '2017-01-23 14:53:38', '2017-01-23 14:53:38', NULL),
(3, '', '', 'uploads/att_slider/cdc04867eb257dc72329d50783495dc4.jpg', '2017-01-23 14:55:49', '2017-01-23 14:55:49', NULL),
(4, '', '', 'uploads/att_slider/d01df9b264fae94d095859ac89740ef7.jpg', '2017-01-23 14:56:10', '2017-01-23 14:56:10', NULL),
(5, '', '', 'uploads/att_slider/dcbcec0a1d372d933362f644f3f1c748.jpg', '2017-01-23 14:57:09', '2017-01-23 15:00:50', NULL),
(6, '', '', 'uploads/att_slider/6d019a008bac8a9c910fd0415b6665c8.jpg', '2017-01-23 15:00:50', '2017-01-23 15:00:50', NULL),
(7, '', '', 'uploads/att_slider/7eced4363094b1b71f650a8d8ea2311f.jpg', '2017-01-23 15:00:50', '2017-01-23 15:00:50', NULL),
(8, '', '', 'uploads/admins/66251021969cd1838b69a1a01c0666ac.jpg', '2017-01-24 11:11:27', '2017-01-24 11:15:08', NULL),
(9, '', '', 'T', '2017-02-02 09:29:20', '2017-05-22 17:56:08', NULL),
(10, '', '', 'T', '2017-02-02 09:29:47', '2017-02-02 09:29:47', NULL),
(11, '', '', 'uploads/products//1bb3c547f95103ac3a93da17a2b75063.jpg', '2017-02-02 10:36:44', '2017-05-28 02:19:39', NULL),
(12, '', '', 'uploads/products//8f0c275e910f7cc08bd0fb5c721c737f.jpg', '2017-02-02 10:37:58', '2017-05-28 02:20:34', NULL),
(13, '', '', 'uploads/products//35de61091f68721560e08c3c6abfef78.jpg', '2017-02-05 13:36:00', '2017-05-28 02:21:13', NULL),
(14, '', '', 'T', '2017-03-06 17:13:02', '2017-03-06 17:14:33', NULL),
(15, '', '', 'uploads/assets//36e08d58e878ca5b2e01467221ea6f3c.jpg', '2017-04-02 13:51:10', '2017-04-02 13:51:10', NULL),
(16, '', '', 'uploads/assets//f8e86cb5bc3bf531dac9b0bfafa214d0.jpg', '2017-04-02 14:10:50', '2017-04-03 08:57:29', NULL),
(17, '', '', 'uploads/assets//e322b72013f90a6d97d2c33b5e0ba459.jpg', '2017-04-03 11:32:59', '2017-04-03 11:32:59', NULL),
(18, '', '', 'T', '2017-04-04 10:30:23', '2017-04-04 10:30:23', NULL),
(19, '', '', 'uploads/assets//a84aa18f4a6da8d83b3a38a9c4ec8a4f.jpg', '2017-04-04 10:43:04', '2017-04-04 10:43:04', NULL),
(20, '', '', 'T', '2017-04-04 10:54:27', '2017-04-04 10:54:27', NULL),
(21, '', '', 'uploads/assets//1b1fbf5c76a1a8e920ce4f5d1842cce3.jpg', '2017-04-04 10:57:52', '2017-04-04 10:57:52', NULL),
(22, '', '', 'uploads/assets//b31882355a2992654df8c55865ea7804.jpg', '2017-04-04 11:05:48', '2017-04-04 11:05:48', NULL),
(23, '', '', 'uploads/assets//21d0d919c719582ea4f60d31c92ad7b1.jpg', '2017-04-04 11:06:41', '2017-04-04 11:06:41', NULL),
(24, '', '', 'uploads/assets//9fd1bec07a3e3c96a51fec0e16b49d9a.jpg', '2017-04-04 11:07:34', '2017-04-04 11:07:34', NULL),
(25, '', '', 'uploads/invoices_templates/76c65e721a4983b200e1e7089bfdf985.png', '2017-04-04 11:12:49', '2017-05-28 02:09:24', NULL),
(26, '', '', 'uploads/assets//9cfd955de413e90dbea4eab7f93a769c.jpg', '2017-04-04 11:14:41', '2017-04-04 11:14:41', NULL),
(27, '', '', 'uploads/documents/files/84c2f3917f1e86d5ab5e813a26b0c4b3.pdf', '2017-04-05 07:39:09', '2017-04-05 07:39:09', NULL),
(28, '', '', 'uploads/documents/files/051d1f3568297fe124e5417e4ba2de50.jpg', '2017-04-05 07:39:09', '2017-04-05 07:39:09', NULL),
(29, '', '', 'uploads/documents/files/22917bd9d646f14f72f3f8f6d5cdc93a.jpg', '2017-04-05 07:39:30', '2017-04-05 07:39:30', NULL),
(30, '', '', 'uploads/documents/files/da6c5d1fc6b8718e9130020337eaac6c.jpg', '2017-04-05 07:39:31', '2017-04-05 07:39:31', NULL),
(31, '', '', 'uploads/documents/files/36d1c6aa9ac173fe9c351271a3ba05fc.jpg', '2017-04-05 07:49:17', '2017-04-05 07:49:17', NULL),
(32, '', '', 'uploads/documents/files/1bb47529f2e452350281f7d06bf43785.pdf', '2017-04-05 07:49:18', '2017-04-05 07:49:18', NULL),
(33, '', '', 'uploads/documents/files/95bc2770aeb1f633863f5f45fded896c.jpg', '2017-04-05 07:53:15', '2017-04-05 07:53:15', NULL),
(34, '', '', 'uploads/documents/files/02799f4f2e44032b55f4a83df4c0d886.jpg', '2017-04-05 07:53:15', '2017-04-05 07:53:15', NULL),
(35, '', '', 'uploads/documents/files/fb03844ba0fc4ccbb6e0fbdc1bc60dea.jpg', '2017-04-05 07:55:22', '2017-04-05 07:55:22', NULL),
(36, '', '', 'uploads/documents/files/a9249cf964acd57ea08a550824724c69.jpg', '2017-04-05 07:55:22', '2017-04-05 07:59:39', NULL),
(37, '', '', 'uploads/documents/files/b756aa6d49aac7d449f7cb2e7f21860a.pdf', '2017-04-05 07:55:22', '2017-04-05 07:59:39', NULL),
(38, '', '', 'uploads/documents/files/9a9d722adf88c21cc66eb1fdffaea1f5.pdf', '2017-04-05 07:58:51', '2017-04-05 07:58:51', NULL),
(39, '', '', 'uploads/documents/files/10112d8cefd12cbbaacab37ca62837ff.jpg', '2017-04-05 09:56:24', '2017-04-05 09:56:24', NULL),
(40, '', '', 'uploads/assets//2c99ce9c0791964d9e57709239921917.jpg', '2017-04-11 14:19:35', '2017-04-11 14:19:35', NULL),
(41, '', '', 'uploads/assets//0a40bc0849fb2c206df299444b4a57e2.jpg', '2017-04-12 09:39:54', '2017-04-12 09:39:54', NULL),
(42, '', '', 'uploads/assets//0317d9ae5355da3fd1dce1dfdb6c9c26.jpg', '2017-04-12 09:52:31', '2017-04-12 09:52:31', NULL),
(43, '', '', 'uploads/products//c5dc1387e9a47410d5c0f60fb9620fd8.jpg', '2017-04-26 15:56:08', '2017-05-22 17:57:33', NULL),
(44, '', '', 'T', '2017-04-27 09:31:10', '2017-04-27 09:31:10', NULL),
(45, '', '', 'T', '2017-05-01 16:39:20', '2017-05-03 12:57:16', NULL),
(46, '', '', 'uploads/deposite_money//3375e4ceb725b1bd9318cb2610a13626.jpg', '2017-05-10 14:23:10', '2017-05-10 14:23:10', NULL),
(47, '', '', 'uploads/att_slider/4d1aa559f79c8bf2136340b7f13a5e2e.jpg', '2017-05-10 14:41:28', '2017-05-10 14:41:28', NULL),
(48, '', '', 'uploads/att_slider/4d1aa559f79c8bf2136340b7f13a5e2e.jpg', '2017-05-10 14:41:28', '2017-05-10 14:41:28', NULL),
(49, '', '', 'uploads/admins/92fb0e0f7f3e11b9f48df95de05db94d.png', '2017-05-17 12:22:53', '2017-05-28 02:52:16', NULL),
(50, '', '', 'uploads/company//38546528d9eebd3c504bb7f09d38053d.jpg', '2017-05-18 06:09:27', '2017-05-18 06:09:27', NULL),
(51, '', '', 'uploads/company//c6e5ffbde30078ded06dae5b07aa25f3.jpg', '2017-05-18 06:11:36', '2017-05-18 06:11:36', NULL),
(52, '', '', 'uploads/company//ca8526da988d5827f6780a00c464cf46.jpg', '2017-05-18 06:12:01', '2017-05-18 06:12:01', NULL),
(53, '', '', 'uploads/company//f3f8f0c900381feea29306692cab386c.png', '2017-05-21 21:29:10', '2017-05-21 21:29:10', NULL),
(54, '', '', 'uploads/company//6ba295c63449aa052cbe6801724a7e94.jpg', '2017-05-21 21:39:29', '2017-05-21 21:39:29', NULL),
(55, '', '', 'uploads/company//2bd24fbb4d34d75d043a728048e3a382.JPG', '2017-05-21 21:40:01', '2017-05-21 21:40:01', NULL),
(56, '', '', 'uploads/admins/9fce9d272b3986d6afb000cfc15168bb.png', '2017-05-21 21:53:29', '2017-05-21 21:53:54', NULL),
(57, '', '', 'T', '2017-05-21 21:56:44', '2017-05-22 17:58:16', NULL),
(58, '', '', 'uploads/admins/29166b3de990902603cfc2572efeb810.png', '2017-05-21 22:00:03', '2017-05-25 17:04:58', NULL),
(59, '', '', 'T', '2017-05-21 22:40:02', '2017-05-25 17:05:45', NULL),
(60, '', '', 'T', '2017-05-21 23:39:04', '2017-05-21 23:39:04', NULL),
(61, '', '', 'uploads/company//18538c1d312043987cc982afa699b2a9.png', '2017-05-21 23:39:47', '2017-05-21 23:39:47', NULL),
(62, '', '', 'T', '2017-05-22 17:18:43', '2017-05-22 17:18:43', NULL),
(63, '', '', 'uploads/company//c18f25b52ef1498d215c8f3faed87f6e.png', '2017-05-22 17:19:26', '2017-05-22 17:19:26', NULL),
(64, '', '', 'T', '2017-05-22 17:58:49', '2017-05-22 17:58:49', NULL),
(65, '', '', 'T', '2017-05-22 17:59:53', '2017-05-22 17:59:53', NULL),
(66, '', '', 'T', '2017-05-22 18:00:18', '2017-05-22 18:00:18', NULL),
(67, '', '', 'T', '2017-05-22 18:00:41', '2017-05-22 18:00:41', NULL),
(68, '', '', 'T', '2017-05-22 18:01:05', '2017-05-22 18:01:05', NULL),
(69, '', '', 'T', '2017-05-22 18:01:31', '2017-05-22 18:01:31', NULL),
(70, '', '', 'T', '2017-05-22 18:27:38', '2017-05-25 16:29:50', NULL),
(71, '', '', 'T', '2017-05-25 03:33:59', '2017-05-25 03:36:42', NULL),
(72, '', '', 'T', '2017-05-25 03:35:46', '2017-05-25 03:35:46', NULL),
(73, '', '', 'T', '2017-05-25 03:37:17', '2017-05-25 03:37:17', NULL),
(74, '', '', 'T', '2017-05-25 03:37:48', '2017-05-25 03:37:48', NULL),
(75, '', '', 'T', '2017-05-25 16:35:12', '2017-05-25 16:35:12', NULL),
(76, '', '', 'T', '2017-05-25 16:39:40', '2017-05-25 16:39:40', NULL),
(77, '', '', 'T', '2017-05-25 16:39:42', '2017-05-25 16:39:42', NULL),
(78, '', '', 'T', '2017-05-25 18:58:32', '2017-05-25 18:58:54', NULL),
(79, '', '', 'T', '2017-05-25 19:00:28', '2017-05-25 19:00:40', NULL),
(80, '', '', 'uploads/invoices_templates/a16b4b0683dbd4798ff98a06fe8d1e67.png', '2017-05-28 02:13:35', '2017-05-28 02:13:35', NULL),
(81, '', '', 'uploads/company//c8a7ba78684c84ecd642bd1511131aeb.png', '2017-05-28 02:51:41', '2017-05-28 02:51:41', NULL),
(82, '', '', 'T', '2017-05-28 02:53:12', '2017-05-28 02:53:12', NULL),
(83, '', '', 'T', '2017-05-28 02:54:42', '2017-06-05 23:26:15', NULL),
(84, '', '', 'T', '2017-05-28 02:59:59', '2017-06-04 02:57:54', NULL),
(85, '', '', 'T', '2017-06-01 02:52:17', '2017-06-01 02:52:17', NULL),
(86, '', '', 'T', '2017-06-01 07:44:44', '2017-06-01 07:44:44', NULL),
(87, '', '', 'T', '2017-06-01 08:13:24', '2017-06-01 08:13:24', NULL),
(88, '', '', 'T', '2017-06-02 03:05:16', '2017-06-02 03:05:16', NULL),
(89, '', '', 'T', '2017-06-02 03:05:32', '2017-06-02 03:05:32', NULL),
(90, '', '', 'T', '2017-06-04 02:48:22', '2017-06-04 02:58:14', NULL),
(91, '', '', 'T', '2017-06-04 20:55:50', '2017-06-04 20:55:50', NULL),
(92, '', '', 'T', '2017-06-04 21:42:50', '2017-06-04 21:42:50', NULL),
(93, '', '', 'T', '2017-06-04 21:43:07', '2017-06-04 21:43:07', NULL),
(94, '', '', 'T', '2017-06-05 03:09:29', '2017-06-05 03:09:29', NULL),
(95, '', '', 'T', '2017-06-05 03:11:32', '2017-06-05 03:12:32', NULL),
(96, '', '', 'T', '2017-06-05 03:13:25', '2017-06-05 03:13:25', NULL),
(97, '', '', 'T', '2017-06-05 06:45:32', '2017-06-05 06:45:32', NULL),
(98, '', '', 'T', '2017-06-05 06:46:26', '2017-06-05 06:46:26', NULL),
(99, '', '', 'T', '2017-06-05 20:15:03', '2017-06-05 20:15:03', NULL),
(100, '', '', 'T', '2017-06-05 23:44:16', '2017-06-05 23:44:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(200) NOT NULL,
  `bank_atm_ratio` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`bank_id`, `bank_name`, `bank_atm_ratio`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'بنك البلاد', '0.00', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branches_orders`
--

CREATE TABLE IF NOT EXISTS `branches_orders` (
  `b_o_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_bill_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `b_o_quantity` int(11) NOT NULL,
  `b_o_date` date NOT NULL,
  `b_o_return` tinyint(1) NOT NULL COMMENT 'المرتجع لو 1',
  `factory_accept_return` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_o_id`),
  KEY `branch_bill_id` (`branch_bill_id`),
  KEY `pro_id` (`pro_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branches_products`
--

CREATE TABLE IF NOT EXISTS `branches_products` (
  `b_p_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `pro_id` int(11) NOT NULL,
  `b_p_quantity` int(11) NOT NULL,
  `b_p_quantity_limit` int(11) NOT NULL,
  `b_p_date` datetime NOT NULL COMMENT 'last addition to quantity of this product',
  `b_p_first_time` tinyint(1) NOT NULL COMMENT 'بضاعه اول مده',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'last operation from clients',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_p_id`),
  KEY `branch_id` (`branch_id`),
  KEY `pro_id` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_bills`
--

CREATE TABLE IF NOT EXISTS `branch_bills` (
  `branch_bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `branch_bill_date` date NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_bill_id`),
  KEY `branch_id` (`branch_id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_deposite_money`
--

CREATE TABLE IF NOT EXISTS `branch_deposite_money` (
  `b_d_money_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `attach_file_id` int(11) NOT NULL,
  `deposite_date` date NOT NULL,
  `origin_money` decimal(10,2) NOT NULL COMMENT 'المفروض يتم ايداعه',
  `total_money_on_box` decimal(10,2) NOT NULL,
  `deposite_money` decimal(10,2) NOT NULL COMMENT 'المبلغ الذي تم ايداعه',
  `lose_money` decimal(10,2) NOT NULL COMMENT 'الفرق الناقص',
  `deposite_desc` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_d_money_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_packages`
--

CREATE TABLE IF NOT EXISTS `branch_packages` (
  `bp_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`bp_id`),
  KEY `package_id` (`package_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_target`
--

CREATE TABLE IF NOT EXISTS `branch_target` (
  `branch_target_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `target_amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`branch_target_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_transfers`
--

CREATE TABLE IF NOT EXISTS `branch_transfers` (
  `b_t_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_branch` int(11) NOT NULL,
  `to_branch` int(11) NOT NULL,
  `b_t_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_transfer_products`
--

CREATE TABLE IF NOT EXISTS `branch_transfer_products` (
  `b_t_p_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_t_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_quantity` int(11) NOT NULL,
  PRIMARY KEY (`b_t_p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `broken_materials`
--

CREATE TABLE IF NOT EXISTS `broken_materials` (
  `b_m_id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_id` int(11) NOT NULL,
  `b_m_quantity` int(11) NOT NULL,
  `b_m_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_m_id`),
  KEY `mat_id` (`mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `broken_products`
--

CREATE TABLE IF NOT EXISTS `broken_products` (
  `b_p_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `pro_id` int(11) NOT NULL,
  `b_p_quantity` int(11) NOT NULL,
  `b_p_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`b_p_id`),
  KEY `user_id` (`user_id`),
  KEY `pro_id` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_type` varchar(200) NOT NULL COMMENT 'product or expenses',
  `cat_code` varchar(200) NOT NULL,
  `cat_name` varchar(400) NOT NULL,
  `cat_name_en` varchar(400) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `cat_path` text NOT NULL,
  `cat_path_en` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cheque_templates`
--

CREATE TABLE IF NOT EXISTS `cheque_templates` (
  `cheque_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `cheque_template_name` varchar(200) NOT NULL,
  `cheque_width` decimal(10,2) NOT NULL,
  `cheque_height` decimal(10,2) NOT NULL,
  `set_default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cheque_template_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cheque_templates`
--

INSERT INTO `cheque_templates` (`cheque_template_id`, `branch_id`, `cheque_template_name`, `cheque_width`, `cheque_height`, `set_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'شيك بنك الراجحي', '19.05', '5.00', 1, '2017-03-02 07:40:42', '2017-03-02 08:36:46', NULL),
(2, 9, 'test', '25.00', '5.00', 0, '2017-03-02 07:48:14', '2017-03-02 07:48:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cheque_template_items`
--

CREATE TABLE IF NOT EXISTS `cheque_template_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `cheque_template_id` int(11) NOT NULL,
  `item_name` varchar(200) NOT NULL,
  `item_width` varchar(200) NOT NULL DEFAULT '100px',
  `item_height` varchar(200) NOT NULL DEFAULT 'auto',
  `item_position_top` varchar(200) NOT NULL DEFAULT '0px',
  `item_position_left` varchar(200) NOT NULL DEFAULT '0px',
  `item_hide` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `cheque_template_id` (`cheque_template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cheque_template_items`
--

INSERT INTO `cheque_template_items` (`item_id`, `cheque_template_id`, `item_name`, `item_width`, `item_height`, `item_position_top`, `item_position_left`, `item_hide`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'cheque_date', '163px', '20px', '-167px', '512px', 0, NULL, '2017-03-02 08:45:21', NULL),
(2, 1, 'receiver_name', '361px', '21px', '-139px', '83px', 0, NULL, '2017-03-02 08:45:21', NULL),
(3, 1, 'money_in_numbers', '173px', '22px', '-165px', '499px', 0, NULL, '2017-03-02 08:45:21', NULL),
(4, 1, 'money_in_text', '634px', '17px', '-153px', '44px', 0, NULL, '2017-03-02 08:45:21', NULL),
(5, 2, 'cheque_date', '100px', 'auto', '0px', '0px', 0, NULL, NULL, NULL),
(6, 2, 'receiver_name', '100px', 'auto', '0px', '0px', 0, NULL, NULL, NULL),
(7, 2, 'money_in_numbers', '100px', 'auto', '0px', '0px', 0, NULL, NULL, NULL),
(8, 2, 'money_in_text', '100px', 'auto', '0px', '0px', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients_bills`
--

CREATE TABLE IF NOT EXISTS `clients_bills` (
  `client_bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `cashier_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `customer_id` int(11) NOT NULL,
  `client_bill_total_amount` decimal(10,2) NOT NULL,
  `client_bill_total_return_amount` decimal(12,2) NOT NULL,
  `client_total_paid_amount_in_cash` decimal(10,2) NOT NULL,
  `client_total_paid_amount_in_atm` decimal(10,2) NOT NULL,
  `client_bill_total_remain_amount` decimal(10,2) NOT NULL,
  `bank_atm_ratio_value` decimal(10,2) NOT NULL COMMENT 'مصاريف بنكية',
  `client_bill_date` datetime NOT NULL,
  `bill_coupon` varchar(200) NOT NULL,
  `bill_coupon_amount` decimal(10,2) NOT NULL,
  `is_gift_bill` tinyint(1) NOT NULL COMMENT 'all bill items is gift and bill will not show with other bills',
  `is_reviewed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`client_bill_id`),
  KEY `cashier_id` (`cashier_id`),
  KEY `branch_id` (`branch_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients_orders`
--

CREATE TABLE IF NOT EXISTS `clients_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `client_bill_id` int(11) NOT NULL,
  `pro_sell_price` decimal(10,3) NOT NULL,
  `pro_original_price` decimal(10,3) NOT NULL,
  `order_quantity` int(11) NOT NULL,
  `order_return` tinyint(1) NOT NULL COMMENT 'المرتجع لو 1',
  `is_gift` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `pro_id` (`pro_id`),
  KEY `client_bill_id` (`client_bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_bill_payment_details`
--

CREATE TABLE IF NOT EXISTS `client_bill_payment_details` (
  `pay_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `payment_amount` decimal(10,2) NOT NULL,
  `atm_ratio_amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pay_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commission_rules`
--

CREATE TABLE IF NOT EXISTS `commission_rules` (
  `commission_id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum_day_earn` decimal(10,2) NOT NULL,
  `total_commission_value` decimal(10,2) NOT NULL,
  `branch_commission_value` decimal(10,2) NOT NULL,
  `factory_commission_value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`commission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_logo_id` int(11) NOT NULL,
  `company_name` varchar(300) NOT NULL,
  `company_name_en` varchar(300) NOT NULL,
  `company_short_desc` text NOT NULL,
  `company_short_desc_en` text NOT NULL,
  `company_tel` varchar(300) NOT NULL,
  `company_fax` varchar(300) NOT NULL,
  `company_email` varchar(300) NOT NULL,
  `company_website` varchar(300) NOT NULL,
  `company_owner_name` varchar(300) NOT NULL,
  `company_owner_name_en` varchar(300) NOT NULL,
  `company_card_id` varchar(300) NOT NULL COMMENT 'رقم بطاقة الاحوال',
  `company_address` text NOT NULL,
  `company_address_en` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_logo_id`, `company_name`, `company_name_en`, `company_short_desc`, `company_short_desc_en`, `company_tel`, `company_fax`, `company_email`, `company_website`, `company_owner_name`, `company_owner_name_en`, `company_card_id`, `company_address`, `company_address_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 100, 'بوابة العطور', 'بوابة العطور', 'Our goal is to produce advertising with a distinctive image and brand position\n﻿\nWe’ve met all of our deadlines, expanded our services and have a creative staff to meet all of our customer needs. Since 2001 Forgood Communications has worked with clients across the globe offering our creative boutique services with a twist. Our main focus has always been advertising from print to online and brand development. These include companies who are just beginning to market themselves and introduce new products or services, as well as more established companies looking to strengthen their image, awareness and overall brand position. We are always in the process of updating our services and adding new ones to ensure that your marketing needs are covered', 'Our goal is to produce advertising with a distinctive image and brand position\n﻿\nWe’ve met all of our deadlines, expanded our services and have a creative staff to meet all of our customer needs. Since 2001 Forgood Communications has worked with clients across the globe offering our creative boutique services with a twist. Our main focus has always been advertising from print to online and brand development. These include companies who are just beginning to market themselves and introduce new products or services, as well as more established companies looking to strengthen their image, awareness and overall brand position. We are always in the process of updating our services and adding new ones to ensure that your marketing needs are covered', '265555 11 966 +', '265555 11 966 +', 'info@perfumesgate.com', 'perfumesgate.com', 'aa', 'bb', '123456789000', 'الرياض - طريق العليا العام - المروج', '', NULL, '2017-06-05 23:44:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `coupon_code` text COLLATE utf8_unicode_ci NOT NULL,
  `coupon_end_date` date NOT NULL,
  `is_rate` tinyint(1) NOT NULL,
  `coupon_value` decimal(10,2) NOT NULL,
  `is_used` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `daily_help_note`
--

CREATE TABLE IF NOT EXISTS `daily_help_note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_name` varchar(200) NOT NULL,
  `note_name_en` varchar(200) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `day_commission`
--

CREATE TABLE IF NOT EXISTS `day_commission` (
  `day_commission_id` int(11) NOT NULL AUTO_INCREMENT,
  `commission_id` int(11) NOT NULL,
  `day_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `commission_value` decimal(10,2) NOT NULL,
  `total_day_bills_money` decimal(10,2) NOT NULL,
  `is_received` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`day_commission_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_type` varchar(200) NOT NULL,
  `doc_code` varchar(200) NOT NULL,
  `doc_start_date` date NOT NULL,
  `doc_end_date` date NOT NULL,
  `doc_source` varchar(200) NOT NULL,
  `doc_desc` text NOT NULL,
  `doc_files` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `duty_documents`
--

CREATE TABLE IF NOT EXISTS `duty_documents` (
  `duty_document_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `duty_document_date` datetime NOT NULL,
  `duty_document_money` decimal(10,2) NOT NULL,
  `duty_document_money_text` text NOT NULL,
  `duty_document_desc` text NOT NULL COMMENT 'بيان',
  `duty_document_received` tinyint(1) NOT NULL COMMENT 'هل تم الموافقه عليه',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`duty_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(300) NOT NULL,
  `email_subject` varchar(300) NOT NULL,
  `email_body` text NOT NULL,
  `run_send` tinyint(1) NOT NULL,
  `offset` int(11) NOT NULL,
  `limit` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `email_settings`
--

INSERT INTO `email_settings` (`id`, `sender_email`, `email_subject`, `email_body`, `run_send`, `offset`, `limit`, `created_at`, `updated_at`) VALUES
(1, 'info@mazziktna.com', 'New Test Email', '<p>New Test Email&nbsp;New Test Email&nbsp;New Test Email&nbsp;New Test Email</p>\r\n', 0, 0, 50, '2016-08-17 22:00:00', '2016-10-11 13:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE IF NOT EXISTS `entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_day_help` varchar(200) NOT NULL,
  `entry_type` varchar(200) NOT NULL COMMENT 'مميز القيد',
  `entry_desc` text NOT NULL,
  `entry_approved` tinyint(1) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `entry_created_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`entry_id`, `entry_day_help`, `entry_type`, `entry_desc`, `entry_approved`, `doc_id`, `entry_created_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '0', '', '', 1, 0, '2017-04-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(28, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:43:26', '2017-04-05 16:43:26', NULL),
(29, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:43:26', '2017-04-05 16:43:26', NULL),
(30, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:46:36', '2017-04-05 16:46:36', NULL),
(31, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:46:36', '2017-04-05 16:46:36', NULL),
(32, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:46:55', '2017-04-05 16:46:55', NULL),
(33, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:46:55', '2017-04-05 16:46:55', NULL),
(34, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:47:39', '2017-04-05 16:47:39', NULL),
(35, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:47:39', '2017-04-05 16:47:39', NULL),
(36, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:49:31', '2017-04-05 16:49:31', NULL),
(37, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:49:31', '2017-04-05 16:49:31', NULL),
(38, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:51:14', '2017-04-05 16:51:14', NULL),
(39, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:51:14', '2017-04-05 16:51:14', NULL),
(40, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:52:41', '2017-04-05 16:52:41', NULL),
(41, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:52:41', '2017-04-05 16:52:41', NULL),
(42, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:53:35', '2017-04-05 16:53:35', NULL),
(43, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:53:35', '2017-04-05 16:53:35', NULL),
(44, '0', '', '2017قيد نهاية المدة للسنة المالية ', 1, 0, '2017-12-31 00:00:00', '2017-04-05 16:55:01', '2017-04-05 16:55:01', NULL),
(45, '0', '', '2018قيد بداية المدة للسنة المالية ', 1, 0, '2018-01-01 00:00:00', '2017-04-05 16:55:01', '2017-04-05 16:55:01', NULL),
(46, '1', '2', '3', 1, 0, '2017-04-12 00:00:00', '2017-04-12 06:38:46', '2017-04-12 06:38:46', NULL),
(47, '1', '2', '3', 1, 0, '2017-04-12 00:00:00', '2017-04-12 06:41:28', '2017-04-12 06:41:28', NULL),
(48, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:10:30', '2017-04-12 07:10:30', NULL),
(49, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:12:58', '2017-04-12 07:12:58', NULL),
(50, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:17:48', '2017-04-12 07:17:48', NULL),
(51, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:19:38', '2017-04-12 07:19:38', NULL),
(52, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:22:16', '2017-04-12 07:22:16', NULL),
(53, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:22:50', '2017-04-12 07:22:50', NULL),
(54, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:31:52', '2017-04-12 07:31:52', NULL),
(55, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:32:12', '2017-04-12 07:32:12', NULL),
(56, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:32:36', '2017-04-12 07:32:36', NULL),
(57, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:33:12', '2017-04-12 07:33:12', NULL),
(58, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:34:56', '2017-04-12 07:34:56', NULL),
(59, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(60, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(61, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:37:01', '2017-04-12 07:37:01', NULL),
(62, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(63, '', '', '', 1, 0, '2017-04-12 00:00:00', '2017-04-12 07:43:35', '2017-04-12 08:43:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entry_consuming_centers`
--

CREATE TABLE IF NOT EXISTS `entry_consuming_centers` (
  `ecc_id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_item_id` int(11) NOT NULL,
  `consuming_acc_id` int(11) NOT NULL,
  `acc_value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ecc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entry_items`
--

CREATE TABLE IF NOT EXISTS `entry_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `credit_or_debit` tinyint(1) NOT NULL COMMENT '0 fro debit 1 for credit',
  `item_amount` double(10,2) NOT NULL,
  `item_created_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `acc_id` (`acc_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95 ;

--
-- Dumping data for table `entry_items`
--

INSERT INTO `entry_items` (`item_id`, `entry_id`, `acc_id`, `credit_or_debit`, `item_amount`, `item_created_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(81, 57, 1, 1, 100.00, '2017-04-12 00:00:00', '2017-04-12 07:33:12', '2017-04-12 07:33:12', NULL),
(82, 57, 5, 0, 100.00, '2017-04-12 00:00:00', '2017-04-12 07:33:12', '2017-04-12 07:33:12', NULL),
(83, 58, 1, 1, 100.00, '2017-04-12 00:00:00', '2017-04-12 07:34:56', '2017-04-12 07:34:56', NULL),
(84, 58, 5, 0, 100.00, '2017-04-12 00:00:00', '2017-04-12 07:34:57', '2017-04-12 07:34:57', NULL),
(85, 59, 1, 1, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(86, 59, 5, 0, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(87, 60, 1, 1, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(88, 60, 5, 0, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(89, 61, 1, 1, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:37:01', '2017-04-12 07:37:01', NULL),
(90, 61, 5, 0, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:37:01', '2017-04-12 07:37:01', NULL),
(91, 62, 1, 1, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(92, 62, 5, 0, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(93, 63, 1, 1, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:43:35', '2017-04-12 07:43:35', NULL),
(94, 63, 5, 0, 20.00, '2017-04-12 00:00:00', '2017-04-12 07:43:35', '2017-04-12 07:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entry_templates`
--

CREATE TABLE IF NOT EXISTS `entry_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `entry_templates`
--

INSERT INTO `entry_templates` (`template_id`, `template_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'نموذج 1', '2017-04-11 23:45:18', '2017-04-11 23:45:18', NULL),
(2, 'نموذج 1', '2017-04-11 23:45:18', '2017-04-11 23:45:18', NULL),
(3, 'نموذج2', '2017-04-11 23:47:16', '2017-04-11 23:47:16', NULL),
(4, 'نموذج2', '2017-04-11 23:49:50', '2017-04-11 23:49:50', NULL),
(5, 'نموذج2', '2017-04-11 23:50:28', '2017-04-11 23:50:28', NULL),
(6, 'نموذج2', '2017-04-11 23:52:18', '2017-04-11 23:52:18', NULL),
(7, 'نموذج2', '2017-04-11 23:53:51', '2017-04-11 23:53:51', NULL),
(8, 'نموذج2', '2017-04-11 23:56:49', '2017-04-11 23:56:49', NULL),
(9, 'نموذح جدسد 3', '2017-04-11 23:58:32', '2017-04-11 23:58:32', NULL),
(10, 'نموذح جدسد 3', '2017-04-11 23:59:12', '2017-04-11 23:59:12', NULL),
(11, 'new1', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(12, 'new1', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(13, 'new1', '2017-04-12 07:37:00', '2017-04-12 07:37:00', NULL),
(14, 'new1', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(15, 'new1', '2017-04-12 07:43:35', '2017-04-12 07:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entry_template_items`
--

CREATE TABLE IF NOT EXISTS `entry_template_items` (
  `t_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_template_id` int(11) NOT NULL,
  `t_item_acc_id` int(11) NOT NULL,
  `t_item_acc_debit_or_credit` tinyint(1) NOT NULL COMMENT '0 for debit 1 for credit',
  `t_item_value_type` tinyint(1) NOT NULL COMMENT '0 for amount or 1 for percent',
  `t_item_value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`t_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `entry_template_items`
--

INSERT INTO `entry_template_items` (`t_item_id`, `entry_template_id`, `t_item_acc_id`, `t_item_acc_debit_or_credit`, `t_item_value_type`, `t_item_value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 10, 1, 1, 0, '100.00', '2017-04-11 23:59:12', '2017-04-11 23:59:12', NULL),
(2, 10, 5, 0, 0, '100.00', '2017-04-11 23:59:12', '2017-04-11 23:59:12', NULL),
(3, 11, 1, 1, 0, '20.00', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(4, 11, 5, 0, 0, '20.00', '2017-04-12 07:36:04', '2017-04-12 07:36:04', NULL),
(5, 12, 1, 1, 0, '20.00', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(6, 12, 5, 0, 0, '20.00', '2017-04-12 07:36:44', '2017-04-12 07:36:44', NULL),
(7, 13, 1, 1, 0, '20.00', '2017-04-12 07:37:01', '2017-04-12 07:37:01', NULL),
(8, 13, 5, 0, 0, '20.00', '2017-04-12 07:37:01', '2017-04-12 07:37:01', NULL),
(9, 14, 1, 1, 0, '20.00', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(10, 14, 5, 0, 0, '20.00', '2017-04-12 07:43:07', '2017-04-12 07:43:07', NULL),
(11, 15, 1, 1, 0, '20.00', '2017-04-12 07:43:35', '2017-04-12 07:43:35', NULL),
(12, 15, 5, 0, 0, '20.00', '2017-04-12 07:43:35', '2017-04-12 07:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT 'branch_id or factory',
  `expenses_maker` int(11) NOT NULL COMMENT 'user_id of branch or branch admin who made this expense row and it will reduce his user_balance',
  `expense_name` varchar(200) NOT NULL,
  `expense_desc` text NOT NULL,
  `expense_amount` decimal(10,2) NOT NULL,
  `expense_date` date NOT NULL,
  `expense_type_id` int(11) NOT NULL,
  `expense_attachment` text NOT NULL,
  `expense_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`expense_id`),
  KEY `user_id` (`user_id`),
  KEY `expense_type_id` (`expense_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `expenses_types`
--

CREATE TABLE IF NOT EXISTS `expenses_types` (
  `expense_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_type_name` varchar(200) NOT NULL,
  `afford_on_product` tinyint(1) NOT NULL,
  `afford_percentage` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`expense_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `financial_year`
--

CREATE TABLE IF NOT EXISTS `financial_year` (
  `fin_id` int(11) NOT NULL AUTO_INCREMENT,
  `fin_label` varchar(200) NOT NULL,
  `fin_start_range` date NOT NULL,
  `fin_end_range` date NOT NULL,
  `fin_closed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `financial_year`
--

INSERT INTO `financial_year` (`fin_id`, `fin_label`, `fin_start_range`, `fin_end_range`, `fin_closed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2017', '2017-01-01', '2017-12-31', 0, '2017-04-04 14:08:39', '2017-04-05 16:55:02', NULL),
(2, '2018', '2018-01-01', '2018-12-31', 0, '2017-04-05 15:53:18', '2017-04-05 15:53:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `financial_year_accounts`
--

CREATE TABLE IF NOT EXISTS `financial_year_accounts` (
  `fya_id` int(11) NOT NULL AUTO_INCREMENT,
  `fya_fin_year_id` int(11) NOT NULL,
  `fya_acc_id` int(11) NOT NULL,
  `fya_acc_value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fya_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `financial_year_accounts`
--

INSERT INTO `financial_year_accounts` (`fya_id`, `fya_fin_year_id`, `fya_acc_id`, `fya_acc_value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 1, 1, '884.00', NULL, '2017-04-12 07:43:35', NULL),
(8, 1, 2, '0.00', NULL, NULL, NULL),
(9, 1, 3, '0.00', NULL, NULL, NULL),
(10, 1, 4, '0.00', NULL, NULL, NULL),
(11, 1, 5, '82.00', NULL, '2017-04-12 07:43:36', NULL),
(12, 1, 6, '4.00', NULL, '2017-04-12 07:43:35', NULL),
(19, 2, 1, '0.00', NULL, NULL, NULL),
(20, 2, 2, '0.00', NULL, NULL, NULL),
(21, 2, 3, '0.00', NULL, NULL, NULL),
(22, 2, 4, '0.00', NULL, NULL, NULL),
(23, 2, 5, '0.00', NULL, NULL, NULL),
(24, 2, 6, '0.00', NULL, NULL, NULL),
(25, 2, 1, '0.00', NULL, NULL, NULL),
(26, 2, 2, '0.00', NULL, NULL, NULL),
(27, 2, 3, '0.00', NULL, NULL, NULL),
(28, 2, 4, '0.00', NULL, NULL, NULL),
(29, 2, 5, '0.00', NULL, NULL, NULL),
(30, 2, 6, '0.00', NULL, NULL, NULL),
(31, 2, 1, '0.00', NULL, NULL, NULL),
(32, 2, 2, '0.00', NULL, NULL, NULL),
(33, 2, 3, '0.00', NULL, NULL, NULL),
(34, 2, 4, '0.00', NULL, NULL, NULL),
(35, 2, 5, '0.00', NULL, NULL, NULL),
(36, 2, 6, '0.00', NULL, NULL, NULL),
(37, 2, 1, '0.00', NULL, NULL, NULL),
(38, 2, 2, '0.00', NULL, NULL, NULL),
(39, 2, 3, '0.00', NULL, NULL, NULL),
(40, 2, 4, '0.00', NULL, NULL, NULL),
(41, 2, 5, '0.00', NULL, NULL, NULL),
(42, 2, 6, '0.00', NULL, NULL, NULL),
(43, 2, 1, '0.00', NULL, NULL, NULL),
(44, 2, 2, '0.00', NULL, NULL, NULL),
(45, 2, 3, '0.00', NULL, NULL, NULL),
(46, 2, 4, '0.00', NULL, NULL, NULL),
(47, 2, 5, '0.00', NULL, NULL, NULL),
(48, 2, 6, '0.00', NULL, NULL, NULL),
(49, 2, 1, '1000.00', NULL, NULL, NULL),
(50, 2, 2, '0.00', NULL, NULL, NULL),
(51, 2, 3, '0.00', NULL, NULL, NULL),
(52, 2, 4, '0.00', NULL, NULL, NULL),
(53, 2, 5, '0.00', NULL, NULL, NULL),
(54, 2, 6, '0.00', NULL, NULL, NULL),
(55, 2, 1, '1000.00', NULL, NULL, NULL),
(56, 2, 2, '0.00', NULL, NULL, NULL),
(57, 2, 3, '0.00', NULL, NULL, NULL),
(58, 2, 4, '0.00', NULL, NULL, NULL),
(59, 2, 5, '0.00', NULL, NULL, NULL),
(60, 2, 6, '0.00', NULL, NULL, NULL),
(61, 1, 7, '-6.00', '2017-04-12 07:36:44', '2017-04-12 07:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_accounts`
--

CREATE TABLE IF NOT EXISTS `general_accounts` (
  `acc_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_type` varchar(200) NOT NULL COMMENT 'general or consuming',
  `cat_id` int(11) NOT NULL,
  `acc_name` varchar(200) NOT NULL,
  `acc_name_en` varchar(200) NOT NULL,
  `acc_code` varchar(200) NOT NULL,
  `acc_money_type` varchar(200) NOT NULL COMMENT 'debit|credit',
  `acc_go_away` tinyint(1) NOT NULL COMMENT 'الحساب مرحل',
  `acc_path` text NOT NULL,
  `acc_path_en` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `general_accounts`
--

INSERT INTO `general_accounts` (`acc_id`, `acc_type`, `cat_id`, `acc_name`, `acc_name_en`, `acc_code`, `acc_money_type`, `acc_go_away`, `acc_path`, `acc_path_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'general', 1, 'الخزينة', 'الخزينة', '1', 'debit', 1, 'الخزينة', 'الخزينة', '0000-00-00 00:00:00', '2017-04-02 10:04:29', NULL),
(2, 'general', 2, 'المشتريات', 'المشتريات', '2', 'debit', 1, 'المشتريات', 'المشتريات', '0000-00-00 00:00:00', NULL, NULL),
(3, 'general', 3, 'حساب تحت فرع تحت الاصول', 'حساب تحت فرع تحت الاصول', '3', 'debit', 1, 'المشتريات', 'المشتريات', '0000-00-00 00:00:00', NULL, NULL),
(4, 'general', 9, 'حساب جديد', 'حساب جديد', '91', 'debit', 1, 'المشتريات', 'المشتريات', '0000-00-00 00:00:00', NULL, NULL),
(5, 'general', 1, 'حساب جديد بقي', 'حساب جديد بقيen', '123', 'debit', 1, 'حساب جديد بقي', 'حساب جديد بقيen', '2017-04-02 12:31:09', '2017-04-02 12:31:09', NULL),
(6, 'consuming', 10, 'حساب مركز نكلفة', 'حساب مركز نكلفة', '12', 'credit', 0, 'حساب مركز نكلفة', 'حساب مركز نكلفة', '2017-04-03 13:13:57', '2017-04-08 13:56:35', NULL),
(7, 'consuming', 10, 'مركز تكلفة جديد', 'مركز تكلفة جديد', '22', 'debit', 1, 'مركز تكلفة جديد', 'مركز تكلفة جديد', '2017-04-08 13:57:15', '2017-04-08 13:57:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_account_category`
--

CREATE TABLE IF NOT EXISTS `general_account_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_type` varchar(200) NOT NULL COMMENT 'general or consuming',
  `cat_code` varchar(200) NOT NULL,
  `cat_name` varchar(400) NOT NULL,
  `cat_name_en` varchar(400) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `cat_path` text NOT NULL,
  `cat_path_en` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `general_account_category`
--

INSERT INTO `general_account_category` (`cat_id`, `cat_type`, `cat_code`, `cat_name`, `cat_name_en`, `parent_id`, `cat_path`, `cat_path_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'general', '1', 'الاصول', 'الاصول', 0, 'الاصول', 'الاصول', '0000-00-00 00:00:00', '2017-04-01 18:16:01', NULL),
(2, 'general', '2', 'مصروفات', 'مصروفات', 0, 'مصروفات', 'مصروفات', NULL, NULL, NULL),
(3, 'general', '3', 'فرع تحت الاصول', 'فرع تحت الاصول', 1, 'فرع تحت الاصول', 'فرع تحت الاصول', '0000-00-00 00:00:00', '2017-04-01 17:07:45', NULL),
(6, 'general', '5', 'تصنيف رئيسي جديد', 'تصنيف رئيسي جديد', 3, 'تصنيف رئيسي جديد', 'تصنيف رئيسي جديد', '2017-04-01 14:29:37', '2017-04-01 17:30:51', NULL),
(9, 'general', '12', 'تصنيف فرعي جيدد', 'تصنيف فرعي جيدد', 6, 'تصنيف فرعي جيد', 'تصنيف فرعي جيدد', '2017-04-01 14:56:57', '2017-04-01 14:57:18', NULL),
(10, 'consuming', '1', 'مركز تكلفة 1', 'مركز تكلفة 1', 0, 'مركز تكلفة 1', 'مركز تكلفة 1', '2017-04-03 12:42:54', '2017-04-03 12:42:54', NULL),
(11, 'consuming', '2', 'تصنيف فرعي لمركز تكلفة', 'تصنيف فرعي لمركز تكلفة', 10, 'تصنيف فرعي لمركز تكلفة', 'تصنيف فرعي لمركز تكلفة', '2017-04-03 12:54:47', '2017-04-03 13:09:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `generate_site_content_methods`
--

CREATE TABLE IF NOT EXISTS `generate_site_content_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_name` varchar(200) NOT NULL,
  `method_title` varchar(200) NOT NULL,
  `method_requirments` text NOT NULL,
  `method_img_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `generate_site_content_methods`
--

INSERT INTO `generate_site_content_methods` (`id`, `method_name`, `method_title`, `method_requirments`, `method_img_id`) VALUES
(5, 'support', 'Support Page', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","contact_us_bg_img_header","contact_us_bg_img_icon","contact_us_bg_img_text","breadcrumb_header","header","contact_us_body","form_header","form_fullname","form_email","form_btn_text","form_sucess_msg"],"customize":{}},"imgs_fields":{"fields":["contact_us_bg_img"],"customize":{"required":{"contact_us_bg_img":""},"need_alt_title":{"contact_us_bg_img":""},"width":{"contact_us_bg_img":""},"height":{"contact_us_bg_img":""}}},"arr_fields":{"fields":{"main_info_data":["label","value"]},"customize":{"main_info_data":{"field_type":{"label":"text","value":"textarea"}}}}}', 100),
(7, 'email_page', 'Email Content', '{"input_fields":{"fields":["copyright"],"customize":{"label_name":{"copyright":"copyright"},"required":{"copyright":""},"type":{"copyright":"textarea"},"class":{"copyright":""}}},"imgs_fields":{"fields":["logo_img"],"customize":{"required":{"logo_img":""},"need_alt_title":{"logo_img":""},"width":{"logo_img":""},"height":{"logo_img":""}}},"arr_fields":{"fields":{"social_imgs":["social_imgs"],"social_links":["social_links"]},"customize":{"social_imgs":{"label_name":{"social_imgs":"social_imgs"},"field_type":{"social_imgs":""},"field_class":{"social_imgs":"form-control"},"add_tiny_mce":{"social_imgs":""}},"social_links":{"label_name":{"social_links":"social_links"},"field_type":{"social_links":""},"field_class":{"social_links":"form-control"},"add_tiny_mce":{"social_links":""}}}}}', 110),
(14, 'keywords', 'keywords', '{"input_fields":{"fields":["homepage","more"],"customize":{}}}', 205),
(16, 'all_services', 'all_services', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","header","info","body"],"customize":{"type":{"meta_title":"text","meta_desc":"textarea","meta_keywords":"textarea","header":"text","info":"textarea","body":"textarea"},"class":{"meta_title":"","meta_desc":"","meta_keywords":"","header":"","info":"","body":"ckeditor"}}},"imgs_fields":{"fields":["cover"],"customize":{"required":{"cover":""},"need_alt_title":{"cover":""},"width":{"cover":"1409"},"height":{"cover":"400"}}}}', 207),
(18, 'gallery', 'gallery', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","breadcrumb_header","bg_img_header","bg_img_icon","bg_img_text"],"customize":{}},"imgs_fields":{"fields":["bg_img"],"customize":{"required":{"bg_img":""},"need_alt_title":{"bg_img":""},"width":{"bg_img":""},"height":{"bg_img":""}}},"slider_fields":{"fields":["gallery"],"customize":{"gallery":{"label_name":"","accept":"","need_alt_title":"yes","additional_inputs_arr":"","width":"","height":""}}}}', 212),
(19, 'edit_header', 'edit_header', '{"input_fields":{"fields":["search_placeholder"],"customize":{"label_name":{"search_placeholder":"search_placeholder"},"required":{"search_placeholder":""},"type":{"search_placeholder":""},"class":{"search_placeholder":""}}},"imgs_fields":{"fields":["logo","icon","header_bg"],"customize":{}},"select_fields":{"fields":["header_menu"],"tables":{"header_menu":{"model":"App\\\\models\\\\sortable_menus_m","text_col":"menu_title","val_col":"menu_id"}},"customize":{"label_name":{"header_menu":"Select Header Menu"},"class":{"header_menu":"form-control"},"multiple":{"header_menu":""}}},"arr_fields":{"fields":{"social_links_arr":["social_link_icon","social_link_href"],"header_contact_us_arr":["contact_icon","contact_title","contact_data"]},"customize":{"social_links_arr":{"label_name":{"social_link_icon":"icon you can get class from this link http:\\/\\/fontawesome.io\\/icons\\/","social_link_href":"Link"}},"header_contact_us_arr":{"label_name":{"contact_icon":"icon you can get from this link http:\\/\\/fontawesome.io\\/icons\\/","contact_title":"Title","contact_data":"Data"}}}}}', 0),
(20, 'edit_footer', 'edit_footer', '{"input_fields":{"fields":["copy_right","footer_contact_us_header","footer_contact_us_text","footer_contact_us_email_placeholder","footer_contact_us_message_placeholder","footer_contact_us_btn_text"],"customize":{}},"imgs_fields":{"fields":["footer_bg"],"customize":{"required":{"footer_bg":""},"need_alt_title":{"footer_bg":""},"width":{"footer_bg":""},"height":{"footer_bg":""}}},"select_fields":{"fields":["footer_menu"],"tables":{"footer_menu":{"model":"App\\\\models\\\\sortable_menus_m","text_col":"menu_title","val_col":"menu_id"}},"customize":{"label_name":{"footer_menu":"Select Footer Menu"},"class":{"footer_menu":"form-control"},"multiple":{"footer_menu":""}}},"arr_fields":{"fields":{"footer_contact_us_arr":["contact_icon","contact_data"]},"customize":{"footer_contact_us_arr":{"label_name":{"contact_icon":"icon you can get from this link http:\\/\\/fontawesome.io\\/icons\\/","contact_data":"Data"}}}}}', 0),
(21, 'edit_index_page', 'edit_index_page', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","art_sec_1_header","art_sec_1_text","index_about_section_header","index_about_section_text","index_about_section_btn_1_text","index_about_section_btn_1_href","index_about_section_btn_2_text","index_about_section_btn_2_href","art_sec_2_header","art_sec_2_text","art_sec_3_header","art_sec_3_text","art_sec_3_embed_youtube_link","art_sec_4_header","art_sec_4_text"],"customize":{}},"imgs_fields":{"fields":["sec_3_bg_img"],"customize":{"required":{"sec_3_bg_img":""},"need_alt_title":{"sec_3_bg_img":""},"width":{"sec_3_bg_img":""},"height":{"sec_3_bg_img":""}}},"arr_fields":{"fields":{"about_section_data_arr":["about_icon","about_header","about_header","about_paragraph"]},"customize":{"about_section_data_arr":{}}},"slider_fields":{"fields":["index_top_slider"],"customize":{"index_top_slider":{"label_name":"","accept":"","need_alt_title":"yes","additional_inputs_arr":"index_top_slider_header,index_top_slider_text","width":"","height":""}}}}', 0),
(22, 'sidebar', 'sidebar', '{"input_fields":{"fields":["sidebar_img_header","sidebar_img_link","follow_us_header","news_litter_header","subscribe_label","subscribe_placeholder","subscribe_btn_icon","subscribe_text"],"customize":{}},"imgs_fields":{"fields":["sidebar_img"],"customize":{"required":{"sidebar_img":""},"need_alt_title":{"sidebar_img":""},"width":{"sidebar_img":""},"height":{"sidebar_img":""}}},"slider_fields":{"fields":["sidebar_social_links"],"customize":{"sidebar_social_links":{"label_name":"","accept":"","need_alt_title":"yes","additional_inputs_arr":"social_link_title,social_link_href","width":"","height":""}}}}', 275),
(23, 'video_gallery', 'video_gallery', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","breadcrumb_header","bg_img_header","bg_img_icon","bg_img_text"],"customize":{}},"imgs_fields":{"fields":["bg_img"],"customize":{"required":{"bg_img":""},"need_alt_title":{"bg_img":""},"width":{"bg_img":""},"height":{"bg_img":""}}},"arr_fields":{"fields":{"video_gallery":["video_title","video_link"]},"customize":{"video_gallery":{}}}}', 291),
(24, 'trip_booking', 'trip_booking', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","breadcrumb_header","bg_img_header","bg_img_icon","bg_img_text","header","body","form_header","form_full_name","form_email","form_nationality","form_phone","form_country_code","form_mobile","form_airport","form_flight_number","form_arrival_date","form_departure_date","form_stay_duration","form_people_number","form_deliver_from_airport","form_btn_text"],"customize":{}},"imgs_fields":{"fields":["bg_img"],"customize":{"required":{"bg_img":""},"need_alt_title":{"bg_img":""},"width":{"bg_img":""},"height":{"bg_img":""}}}}', 297),
(25, 'search_page', 'search_page', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","breadcrumb_header","bg_img_header","bg_img_icon","bg_img_text"],"customize":{}},"imgs_fields":{"fields":["bg_img"],"customize":{"required":{"bg_img":""},"need_alt_title":{"bg_img":""},"width":{"bg_img":""},"height":{"bg_img":""}}}}', 299);

-- --------------------------------------------------------

--
-- Table structure for table `invoices_templates`
--

CREATE TABLE IF NOT EXISTS `invoices_templates` (
  `inv_temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `template_name` varchar(200) NOT NULL,
  `template_width` decimal(10,2) NOT NULL,
  `template_height` decimal(10,2) NOT NULL,
  `table_direction` varchar(100) NOT NULL,
  `table_position_left` varchar(200) NOT NULL,
  `table_position_top` varchar(200) NOT NULL,
  `table_width` varchar(200) NOT NULL,
  `table_height` varchar(200) NOT NULL,
  `set_default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`inv_temp_id`),
  KEY `inv_temp_id` (`inv_temp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `invoices_templates`
--

INSERT INTO `invoices_templates` (`inv_temp_id`, `branch_id`, `template_name`, `template_width`, `template_height`, `table_direction`, `table_position_left`, `table_position_top`, `table_width`, `table_height`, `set_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'قالب فاتورة 1', '12.00', '25.00', 'ltr', '12.5px', '133.484px', '356px', '681px', 1, '2017-02-26 13:29:28', '2017-05-28 02:12:42', '2017-05-28 02:12:42'),
(2, 55, 'الأساسي', '20.00', '30.00', 'ltr', '0px', '611px', '268px', '109px', 1, '2017-05-28 02:13:11', '2017-06-05 20:12:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices_template_content`
--

CREATE TABLE IF NOT EXISTS `invoices_template_content` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_temp_id` int(11) NOT NULL,
  `item_name` varchar(200) NOT NULL,
  `item_label` text NOT NULL,
  `item_type` varchar(200) NOT NULL COMMENT 'static,dynamic,image',
  `item_img` int(11) NOT NULL,
  `item_code` text NOT NULL,
  `item_position_top` varchar(200) NOT NULL DEFAULT '0px',
  `item_position_left` varchar(200) NOT NULL DEFAULT '0px',
  `item_width` varchar(200) NOT NULL DEFAULT '300px',
  `item_height` varchar(200) NOT NULL DEFAULT 'auto',
  `item_hide` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `invoices_template_content`
--

INSERT INTO `invoices_template_content` (`item_id`, `inv_temp_id`, `item_name`, `item_label`, `item_type`, `item_img`, `item_code`, `item_position_top`, `item_position_left`, `item_width`, `item_height`, `item_hide`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'logo', '', 'image', 25, '', '30px', '146.492px', '125px', '107px', 0, NULL, '2017-05-27 21:41:26', NULL),
(2, 1, 'cashier_name', '<p><span style="font-family:Comic Sans MS,cursive;"><strong>اسم المستخدم</strong></span></p>\r\n', 'static', 0, '', '669.5px', '32.5px', '122px', '21px', 0, NULL, '2017-05-27 21:41:26', NULL),
(3, 1, 'customer_tel', 'Customer Tel', 'static', 0, '', '157px', '-15px', '200px', '24px', 1, NULL, '2017-05-27 21:39:42', NULL),
(4, 1, 'created', 'Created', 'static', 0, '', '84px', '143px', '245px', '42px', 0, NULL, '2017-05-27 21:41:35', NULL),
(5, 1, 'item_name', 'Item Name', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(6, 1, 'item_desc', 'Item Desc', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(7, 1, 'item_quantity', 'Item Quantity', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, '2017-02-27 12:07:32', NULL),
(8, 1, 'item_price', 'Item Price', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, '2017-02-27 12:07:34', NULL),
(9, 1, 'items_pre_total', 'Total', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(10, 1, 'items_coupon', 'Coupon', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, '2017-02-27 12:07:36', NULL),
(11, 1, 'items_discount', 'Discount', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(12, 1, 'items_post_total', 'Total', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(13, 1, 'invoice_note', '<p><span style="font-family:Comic Sans MS,cursive;"><span style="font-size:14px;"><strong>المرتجع بحد اقصي 15 يوم من تاريخ الشراء</strong></span></span></p>\r\n', 'static', 0, '', '294.8px', '66.2px', '599px', '53px', 0, NULL, '2017-05-27 21:41:26', NULL),
(14, 1, 'invoice_number', 'Invoice #', 'static', 0, '', '112.5px', '0.796875px', '104px', '8px', 0, NULL, '2017-05-27 21:41:26', NULL),
(15, 1, 'total_cash_paid', 'Total Cash Paid', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(16, 1, 'total_atm_paid', 'Total ATM Paid', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(17, 1, 'total_paid', 'Total Paid', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(18, 1, 'total_remain', 'Total Remain', 'static', 0, '', '0px', '0px', '200px', 'auto', 0, NULL, NULL, NULL),
(19, 2, 'logo', '', 'image', 80, '', '526.992px', '109.961px', '99px', '125px', 0, NULL, '2017-06-05 20:09:56', NULL),
(20, 2, 'invoice_number', 'Invoice #', 'static', 0, '', '614.99px', '0.989583px', '97px', '19px', 0, NULL, '2017-06-05 20:12:01', NULL),
(21, 2, 'cashier_name', 'Cashier', 'static', 0, '', '593.99px', '125.99px', '140px', '24px', 0, NULL, '2017-06-05 20:09:56', NULL),
(22, 2, 'customer_tel', 'Customer Tel', 'static', 0, '', '81.9896px', '175.99px', '0px', '70px', 1, NULL, '2017-05-31 02:41:56', NULL),
(23, 2, 'created', 'Created', 'static', 0, '', '610px', '1px', '255px', '27px', 0, NULL, '2017-06-04 17:15:03', NULL),
(24, 2, 'item_name', 'Item Name', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(25, 2, 'item_desc', 'Item Desc', 'static', 0, '', '0px', '0px', '0', 'auto', 1, NULL, '2017-05-31 23:49:17', NULL),
(26, 2, 'item_quantity', 'Item Quantity', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(27, 2, 'item_price', 'Item Price', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(28, 2, 'items_pre_total', 'Total', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(29, 2, 'items_coupon', 'Coupon', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(30, 2, 'items_discount', 'Discount', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(31, 2, 'items_post_total', 'Total', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(32, 2, 'total_cash_paid', '<p>Cash</p>\r\n', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, '2017-06-05 20:05:15', NULL),
(33, 2, 'total_atm_paid', '<p>SPAN</p>\r\n', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, '2017-06-05 20:04:45', NULL),
(34, 2, 'total_paid', 'Total', 'static', 0, '', '0px', '0px', '0', 'auto', 0, NULL, NULL, NULL),
(35, 2, 'total_remain', 'Total', 'static', 0, '', '0px', '0px', '0', 'auto', 1, NULL, '2017-06-05 20:08:34', NULL),
(36, 2, 'invoice_note', '<p>يسعدنا خدمتكم علي الرقم الموحد 920000857</p>\r\n', 'static', 0, '', '796px', '0px', '273px', '50px', 0, NULL, '2017-06-05 20:09:56', NULL),
(37, 2, 'branch_name', '', 'static', 0, '', '609.992px', '78.9922px', '125px', '24px', 0, NULL, '2017-06-05 20:12:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_name` varchar(300) NOT NULL,
  `mat_type` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mat_id`),
  KEY `mat_name` (`mat_name`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`mat_id`, `mat_name`, `mat_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'زيت', 'مللي لتر', '2017-02-02 10:29:23', '2017-02-02 10:29:42', NULL),
(2, 'عود', 'مللي لتر', '2017-02-02 10:30:21', '2017-02-02 10:30:21', NULL),
(3, 'زجاجات', 'قطع', '2017-02-02 10:30:38', '2017-02-02 10:31:01', NULL),
(4, 'كارتون', 'قطعه', '2017-02-05 13:28:58', '2017-02-05 13:28:58', NULL),
(5, 'test', 'kilo', '2017-02-07 10:23:06', '2017-02-07 10:23:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `money_transfers`
--

CREATE TABLE IF NOT EXISTS `money_transfers` (
  `m_t_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(10) unsigned NOT NULL,
  `to_user_id` int(10) unsigned NOT NULL,
  `m_t_bill_id` int(11) NOT NULL,
  `m_t_bill_type` varchar(200) NOT NULL,
  `m_t_amount` decimal(10,2) NOT NULL,
  `m_t_amount_type` varchar(200) NOT NULL COMMENT 'cash or atm ',
  `m_t_status` tinyint(1) NOT NULL,
  `m_t_desc` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`m_t_id`),
  KEY `from_user_id` (`from_user_id`),
  KEY `to_user_id` (`to_user_id`),
  KEY `m_t_bill_id` (`m_t_bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `not_id` int(11) NOT NULL AUTO_INCREMENT,
  `not_title` text NOT NULL,
  `not_type` varchar(200) NOT NULL,
  `not_to_userid` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`not_id`),
  KEY `not_to_userid` (`not_to_userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=428 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`not_id`, `not_title`, `not_type`, `not_to_userid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, ' Add New Material with N.1 ', 'info', 2, '2017-02-02 10:29:24', '2017-02-02 10:29:24', NULL),
(2, ' Add New Material with N.1 ', 'info', 10, '2017-02-02 10:29:24', '2017-02-02 10:29:24', NULL),
(3, ' Edit Material N.1', 'info', 2, '2017-02-02 10:29:42', '2017-02-02 10:29:42', NULL),
(4, ' Edit Material N.1', 'info', 10, '2017-02-02 10:29:42', '2017-02-02 10:29:42', NULL),
(5, ' Add New Material with N.2 ', 'info', 2, '2017-02-02 10:30:21', '2017-02-02 10:30:21', NULL),
(6, ' Add New Material with N.2 ', 'info', 10, '2017-02-02 10:30:21', '2017-02-02 10:30:21', NULL),
(7, ' Add New Material with N.3 ', 'info', 2, '2017-02-02 10:30:38', '2017-02-02 10:30:38', NULL),
(8, ' Add New Material with N.3 ', 'info', 10, '2017-02-02 10:30:38', '2017-02-02 10:30:38', NULL),
(9, ' Edit Material N.3', 'info', 2, '2017-02-02 10:31:01', '2017-02-02 10:31:01', NULL),
(10, ' Edit Material N.3', 'info', 10, '2017-02-02 10:31:01', '2017-02-02 10:31:01', NULL),
(11, 'Add New Product Material on Stock with N.1', 'info', 2, '2017-02-02 10:32:35', '2017-02-02 10:32:35', NULL),
(12, 'Add New Product Material on Stock with N.1', 'info', 10, '2017-02-02 10:32:35', '2017-02-02 10:32:35', NULL),
(13, 'Edit Product Material on Stock with N.1', 'info', 2, '2017-02-02 10:32:49', '2017-02-02 10:32:49', NULL),
(14, 'Edit Product Material on Stock with N.1', 'info', 10, '2017-02-02 10:32:50', '2017-02-02 10:32:50', NULL),
(15, 'Add New Product Material on Stock with N.2', 'info', 2, '2017-02-02 10:33:26', '2017-02-02 10:33:26', NULL),
(16, 'Add New Product Material on Stock with N.2', 'info', 10, '2017-02-02 10:33:26', '2017-02-02 10:33:26', NULL),
(17, 'Add New Product Material on Stock with N.3', 'info', 2, '2017-02-02 10:33:59', '2017-02-02 10:33:59', NULL),
(18, 'Add New Product Material on Stock with N.3', 'info', 10, '2017-02-02 10:33:59', '2017-02-02 10:33:59', NULL),
(19, 'Add new product (برفان هوجو) With N.1', 'info', 2, '2017-02-02 10:36:44', '2017-02-02 10:36:44', NULL),
(20, 'Add new product (برفان هوجو) With N.1', 'info', 10, '2017-02-02 10:36:44', '2017-02-02 10:36:44', NULL),
(21, 'Add new product (برفان هورث) With N.2', 'info', 2, '2017-02-02 10:37:58', '2017-02-02 10:37:58', NULL),
(22, 'Add new product (برفان هورث) With N.2', 'info', 10, '2017-02-02 10:37:58', '2017-02-02 10:37:58', NULL),
(23, ' Add Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 10:40:24', '2017-02-02 10:40:24', NULL),
(24, ' Add Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 10:40:24', '2017-02-02 10:40:24', NULL),
(25, ' Add Origin Price of Product ''برفان هوجو'' to branch ''branch_2@ksasys.com'' ', 'info', 2, '2017-02-02 10:40:39', '2017-02-02 10:40:39', NULL),
(26, ' Add Origin Price of Product ''برفان هوجو'' to branch ''branch_2@ksasys.com'' ', 'info', 10, '2017-02-02 10:40:39', '2017-02-02 10:40:39', NULL),
(27, ' Add Origin Price of Product ''برفان هورث'' to branch ''Branch'' ', 'info', 2, '2017-02-02 10:40:50', '2017-02-02 10:40:50', NULL),
(28, ' Add Origin Price of Product ''برفان هورث'' to branch ''Branch'' ', 'info', 10, '2017-02-02 10:40:50', '2017-02-02 10:40:50', NULL),
(29, ' Add Origin Price of Product ''برفان هورث'' to branch ''branch_2@ksasys.com'' ', 'info', 2, '2017-02-02 10:40:59', '2017-02-02 10:40:59', NULL),
(30, ' Add Origin Price of Product ''برفان هورث'' to branch ''branch_2@ksasys.com'' ', 'info', 10, '2017-02-02 10:40:59', '2017-02-02 10:40:59', NULL),
(31, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 10:41:39', '2017-02-02 10:41:39', NULL),
(32, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 10:41:39', '2017-02-02 10:41:39', NULL),
(33, ' Add New Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 10:41:39', '2017-02-02 10:41:39', NULL),
(34, ' Add New Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 10:41:39', '2017-02-02 10:41:39', NULL),
(35, 'Add New Product Broken Material(زيت) with amount 1000', 'info', 2, '2017-02-02 10:42:22', '2017-02-02 10:42:22', NULL),
(36, 'Add New Product Broken Material(زيت) with amount 1000', 'info', 10, '2017-02-02 10:42:22', '2017-02-02 10:42:22', NULL),
(37, 'Edit Quantity of Product Material (زيت) from 10000.00 to 9000', 'info', 2, '2017-02-02 10:42:26', '2017-02-02 10:42:26', NULL),
(38, 'Edit Quantity of Product Material (زيت) from 10000.00 to 9000', 'info', 10, '2017-02-02 10:42:26', '2017-02-02 10:42:26', NULL),
(39, 'Update Product (برفان هوجو)''s Quantity from 100 to 90', 'info', 2, '2017-02-02 10:43:08', '2017-02-02 10:43:08', NULL),
(40, 'Update Product (برفان هوجو)''s Quantity from 100 to 90', 'info', 10, '2017-02-02 10:43:08', '2017-02-02 10:43:08', NULL),
(41, ' Have Produced 100 of Product برفان هوجو', 'info', 2, '2017-02-02 10:44:52', '2017-02-02 10:44:52', NULL),
(42, ' Have Produced 100 of Product برفان هوجو', 'info', 10, '2017-02-02 10:44:52', '2017-02-02 10:44:52', NULL),
(43, ' Add New Bill N.1', 'info', 2, '2017-02-02 10:47:38', '2017-02-02 10:47:38', NULL),
(44, ' Add New Bill N.1', 'info', 10, '2017-02-02 10:47:38', '2017-02-02 10:47:38', NULL),
(45, ' Edit Material زيت On Product Materials Stock''s Quantity From 9000.00 To 8900', 'info', 2, '2017-02-02 10:50:25', '2017-02-02 10:50:25', NULL),
(46, ' Edit Material زيت On Product Materials Stock''s Quantity From 9000.00 To 8900', 'info', 10, '2017-02-02 10:50:25', '2017-02-02 10:50:25', NULL),
(47, ' Add return orders to bill N.1', 'info', 2, '2017-02-02 10:50:27', '2017-02-02 10:50:27', NULL),
(48, ' Add return orders to bill N.1', 'info', 10, '2017-02-02 10:50:27', '2017-02-02 10:50:27', NULL),
(49, ' Have received money 1000.00 ', 'info', 2, '2017-02-02 11:04:30', '2017-02-02 11:04:30', NULL),
(50, ' Have received money 1000.00 ', 'info', 10, '2017-02-02 11:04:31', '2017-02-02 11:04:31', NULL),
(51, ' Have received money 50.00 ', 'info', 2, '2017-02-02 11:04:42', '2017-02-02 11:04:42', NULL),
(52, ' Have received money 50.00 ', 'info', 10, '2017-02-02 11:04:42', '2017-02-02 11:04:42', NULL),
(53, 'Your Bill N.1 Status has changed to be waiting', 'info', 3, '2017-02-02 11:08:27', '2017-02-02 11:08:27', NULL),
(54, 'Your Bill N.1 Status has changed to be sending', 'info', 3, '2017-02-02 11:08:54', '2017-02-02 11:08:54', NULL),
(55, ' Edit Branch  Branch products (برفان هوجو)  want to return 10 to factory ', 'info', 2, '2017-02-02 11:11:53', '2017-02-02 11:11:53', NULL),
(56, ' Edit Branch  Branch products (برفان هوجو)  want to return 10 to factory ', 'info', 10, '2017-02-02 11:11:53', '2017-02-02 11:11:53', NULL),
(57, ' Update Product Quantity N.1 on branch N.3 ''s stock  from 100 to 90 ', 'info', 3, '2017-02-02 11:12:31', '2017-02-02 11:12:31', NULL),
(58, ' Return quantity 10 from bill N.1 from branch 3 ', 'info', 2, '2017-02-02 11:12:34', '2017-02-02 11:12:34', NULL),
(59, ' Return quantity 10 from bill N.1 from branch 3 ', 'info', 10, '2017-02-02 11:12:34', '2017-02-02 11:12:34', NULL),
(60, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 11:27:37', '2017-02-02 11:27:37', NULL),
(61, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 11:27:37', '2017-02-02 11:27:37', NULL),
(62, ' Edit Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 11:27:37', '2017-02-02 11:27:37', NULL),
(63, ' Edit Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 11:27:37', '2017-02-02 11:27:37', NULL),
(64, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 11:28:02', '2017-02-02 11:28:02', NULL),
(65, ' Edit Origin Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 11:28:02', '2017-02-02 11:28:02', NULL),
(66, ' Edit Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 2, '2017-02-02 11:28:02', '2017-02-02 11:28:02', NULL),
(67, ' Edit Promotion Price of Product ''برفان هوجو'' to branch ''Branch'' ', 'info', 10, '2017-02-02 11:28:02', '2017-02-02 11:28:02', NULL),
(68, ' Have Produced 50 of Product برفان هوجو', 'info', 2, '2017-02-02 12:05:27', '2017-02-02 12:05:27', NULL),
(69, ' Have Produced 50 of Product برفان هوجو', 'info', 10, '2017-02-02 12:05:27', '2017-02-02 12:05:27', NULL),
(70, ' Edit Material زيت On Product Materials Stock''s Quantity From 8400.00 To 8300', 'info', 2, '2017-02-02 12:11:25', '2017-02-02 12:11:25', NULL),
(71, ' Edit Material زيت On Product Materials Stock''s Quantity From 8400.00 To 8300', 'info', 10, '2017-02-02 12:11:26', '2017-02-02 12:11:26', NULL),
(72, ' Add return orders to bill N.1', 'info', 2, '2017-02-02 12:11:28', '2017-02-02 12:11:28', NULL),
(73, ' Add return orders to bill N.1', 'info', 10, '2017-02-02 12:11:28', '2017-02-02 12:11:28', NULL),
(74, ' Have received money 50.00 ', 'info', 2, '2017-02-02 12:14:01', '2017-02-02 12:14:01', NULL),
(75, ' Have received money 50.00 ', 'info', 10, '2017-02-02 12:14:01', '2017-02-02 12:14:01', NULL),
(76, ' Add New Material with N.4 ', 'info', 2, '2017-02-05 13:28:58', '2017-02-05 13:28:58', NULL),
(77, ' Add New Material with N.4 ', 'info', 10, '2017-02-05 13:28:58', '2017-02-05 13:28:58', NULL),
(78, 'Add New Product Material on Stock with N.4', 'info', 2, '2017-02-05 13:31:33', '2017-02-05 13:31:33', NULL),
(79, 'Add New Product Material on Stock with N.4', 'info', 10, '2017-02-05 13:31:33', '2017-02-05 13:31:33', NULL),
(80, 'Add new product (عطر2) With N.3', 'info', 2, '2017-02-05 13:36:00', '2017-02-05 13:36:00', NULL),
(81, 'Add new product (عطر2) With N.3', 'info', 10, '2017-02-05 13:36:00', '2017-02-05 13:36:00', NULL),
(82, ' Have Produced 100 of Product عطر2', 'info', 2, '2017-02-05 13:57:34', '2017-02-05 13:57:34', NULL),
(83, ' Have Produced 100 of Product عطر2', 'info', 10, '2017-02-05 13:57:34', '2017-02-05 13:57:34', NULL),
(84, 'Add New Product Broken Material(كارتون) with amount 10', 'info', 2, '2017-02-05 13:59:56', '2017-02-05 13:59:56', NULL),
(85, 'Add New Product Broken Material(كارتون) with amount 10', 'info', 10, '2017-02-05 13:59:56', '2017-02-05 13:59:56', NULL),
(86, 'Edit Quantity of Product Material (كارتون) from 900.00 to 890', 'info', 2, '2017-02-05 13:59:59', '2017-02-05 13:59:59', NULL),
(87, 'Edit Quantity of Product Material (كارتون) from 900.00 to 890', 'info', 10, '2017-02-05 13:59:59', '2017-02-05 13:59:59', NULL),
(88, 'Update Product (عطر2)''s Quantity from 250 to 200', 'info', 2, '2017-02-05 14:00:43', '2017-02-05 14:00:43', NULL),
(89, 'Update Product (عطر2)''s Quantity from 250 to 200', 'info', 10, '2017-02-05 14:00:43', '2017-02-05 14:00:43', NULL),
(90, ' Add New Material with N.5 ', 'info', 2, '2017-02-07 10:23:06', '2017-02-07 10:23:06', NULL),
(91, ' Add New Material with N.5 ', 'info', 10, '2017-02-07 10:23:06', '2017-02-07 10:23:06', NULL),
(92, ' Add New Bill N.2', 'info', 2, '2017-02-07 11:22:16', '2017-02-07 11:22:16', NULL),
(93, ' Add New Bill N.2', 'info', 10, '2017-02-07 11:22:17', '2017-02-07 11:22:17', NULL),
(94, ' Edit Material زيت On Product Materials Stock''s Quantity From 7400.00 To 7380', 'info', 2, '2017-02-07 11:28:10', '2017-02-07 11:28:10', NULL),
(95, ' Edit Material زيت On Product Materials Stock''s Quantity From 7400.00 To 7380', 'info', 10, '2017-02-07 11:28:10', '2017-02-07 11:28:10', NULL),
(96, ' Edit Material عود On Product Materials Stock''s Quantity From 3475.00 To 3465', 'info', 2, '2017-02-07 11:28:19', '2017-02-07 11:28:19', NULL),
(97, ' Edit Material عود On Product Materials Stock''s Quantity From 3475.00 To 3465', 'info', 10, '2017-02-07 11:28:19', '2017-02-07 11:28:19', NULL),
(98, ' Add return orders to bill N.2', 'info', 2, '2017-02-07 11:28:26', '2017-02-07 11:28:26', NULL),
(99, ' Add return orders to bill N.2', 'info', 10, '2017-02-07 11:28:26', '2017-02-07 11:28:26', NULL),
(100, ' Have Produced 100 of Product برفان هوجو', 'info', 2, '2017-02-20 15:35:41', '2017-02-20 15:35:41', NULL),
(101, ' Have Produced 100 of Product برفان هوجو', 'info', 10, '2017-02-20 15:35:41', '2017-02-20 15:35:41', NULL),
(102, ' إضافة فاتورة برقم.3', 'info', 2, '2017-02-20 15:39:36', '2017-02-20 15:39:36', NULL),
(103, ' إضافة فاتورة برقم.3', 'info', 10, '2017-02-20 15:39:36', '2017-02-20 15:39:36', NULL),
(104, ' تعديل كمية الماده الخام زيت من 6480.00 الي 6460', 'info', 2, '2017-02-20 15:41:01', '2017-02-20 15:41:01', NULL),
(105, ' تعديل كمية الماده الخام زيت من 6480.00 الي 6460', 'info', 10, '2017-02-20 15:41:01', '2017-02-20 15:41:01', NULL),
(106, ' تعديل كمية الماده الخام عود من 3415.00 الي 3405', 'info', 2, '2017-02-20 15:41:43', '2017-02-20 15:41:43', NULL),
(107, ' تعديل كمية الماده الخام عود من 3415.00 الي 3405', 'info', 10, '2017-02-20 15:41:43', '2017-02-20 15:41:43', NULL),
(108, ' إضافة فاتورة مرتجع برقم.3', 'info', 2, '2017-02-20 15:41:52', '2017-02-20 15:41:52', NULL),
(109, ' إضافة فاتورة مرتجع برقم.3', 'info', 10, '2017-02-20 15:41:52', '2017-02-20 15:41:52', NULL),
(110, ' لقد تم استلام المبلغ 4000.00 ', 'info', 2, '2017-02-20 15:46:03', '2017-02-20 15:46:03', NULL),
(111, ' لقد تم استلام المبلغ 4000.00 ', 'info', 10, '2017-02-20 15:46:03', '2017-02-20 15:46:03', NULL),
(112, ' لقد تم استلام المبلغ 400.00 ', 'info', 2, '2017-02-20 15:46:16', '2017-02-20 15:46:16', NULL),
(113, ' لقد تم استلام المبلغ 400.00 ', 'info', 10, '2017-02-20 15:46:16', '2017-02-20 15:46:16', NULL),
(114, 'حاله الفاتورة رقم.2 تغيرت لتكون فيد الانتظار', 'info', 3, '2017-02-20 15:57:23', '2017-02-20 15:57:23', NULL),
(115, 'حاله الفاتورة رقم.2 تغيرت لتكون ارسال', 'info', 3, '2017-02-20 15:57:52', '2017-02-20 15:57:52', NULL),
(116, ' إضافة فاتورة برقم.4', 'info', 2, '2017-02-22 15:20:15', '2017-02-22 15:20:15', NULL),
(117, ' إضافة فاتورة برقم.4', 'info', 10, '2017-02-22 15:20:15', '2017-02-22 15:20:15', NULL),
(118, ' إضافة فاتورة برقم.5', 'info', 2, '2017-02-22 15:23:42', '2017-02-22 15:23:42', NULL),
(119, ' إضافة فاتورة برقم.5', 'info', 10, '2017-02-22 15:23:42', '2017-02-22 15:23:42', NULL),
(120, ' إضافة فاتورة برقم.6', 'info', 2, '2017-02-22 15:31:47', '2017-02-22 15:31:47', NULL),
(121, ' إضافة فاتورة برقم.6', 'info', 10, '2017-02-22 15:31:47', '2017-02-22 15:31:47', NULL),
(122, ' إضافة فاتورة برقم.7', 'info', 2, '2017-02-22 15:35:46', '2017-02-22 15:35:46', NULL),
(123, ' إضافة فاتورة برقم.7', 'info', 10, '2017-02-22 15:35:46', '2017-02-22 15:35:46', NULL),
(124, ' إضافة فاتورة برقم.8', 'info', 2, '2017-02-22 15:36:55', '2017-02-22 15:36:55', NULL),
(125, ' إضافة فاتورة برقم.8', 'info', 10, '2017-02-22 15:36:55', '2017-02-22 15:36:55', NULL),
(126, ' إضافة فاتورة برقم.9', 'info', 2, '2017-02-22 15:39:31', '2017-02-22 15:39:31', NULL),
(127, ' إضافة فاتورة برقم.9', 'info', 10, '2017-02-22 15:39:31', '2017-02-22 15:39:31', NULL),
(128, ' إضافة فاتورة برقم.10', 'info', 2, '2017-02-23 16:53:06', '2017-02-23 16:53:06', NULL),
(129, ' إضافة فاتورة برقم.10', 'info', 10, '2017-02-23 16:53:06', '2017-02-23 16:53:06', NULL),
(130, ' إضافة فاتورة برقم.11', 'info', 2, '2017-02-23 17:01:09', '2017-02-23 17:01:09', NULL),
(131, ' إضافة فاتورة برقم.11', 'info', 10, '2017-02-23 17:01:09', '2017-02-23 17:01:09', NULL),
(132, ' إضافة فاتورة برقم.12', 'info', 2, '2017-02-23 17:02:41', '2017-02-23 17:02:41', NULL),
(133, ' إضافة فاتورة برقم.12', 'info', 10, '2017-02-23 17:02:41', '2017-02-23 17:02:41', NULL),
(134, ' تعديل كمية الماده الخام زيت من 7310.00 الي 7300', 'info', 2, '2017-02-23 17:03:49', '2017-02-23 17:03:49', NULL),
(135, ' تعديل كمية الماده الخام زيت من 7310.00 الي 7300', 'info', 10, '2017-02-23 17:03:49', '2017-02-23 17:03:49', NULL),
(136, ' إضافة فاتورة مرتجع برقم.12', 'info', 2, '2017-02-23 17:03:59', '2017-02-23 17:03:59', NULL),
(137, ' إضافة فاتورة مرتجع برقم.12', 'info', 10, '2017-02-23 17:03:59', '2017-02-23 17:03:59', NULL),
(138, ' تعديل كمية الماده الخام زيت من 7300.00 الي 7295', 'info', 2, '2017-02-23 17:05:17', '2017-02-23 17:05:17', NULL),
(139, ' تعديل كمية الماده الخام زيت من 7300.00 الي 7295', 'info', 10, '2017-02-23 17:05:18', '2017-02-23 17:05:18', NULL),
(140, ' إضافة فاتورة مرتجع برقم.12', 'info', 2, '2017-02-23 17:05:28', '2017-02-23 17:05:28', NULL),
(141, ' إضافة فاتورة مرتجع برقم.12', 'info', 10, '2017-02-23 17:05:28', '2017-02-23 17:05:28', NULL),
(142, ' إضافة فاتورة برقم.13', 'info', 2, '2017-02-24 10:10:00', '2017-02-24 10:10:00', NULL),
(143, ' إضافة فاتورة برقم.13', 'info', 10, '2017-02-24 10:10:00', '2017-02-24 10:10:00', NULL),
(144, ' إضافة فاتورة برقم.14', 'info', 2, '2017-02-24 10:15:26', '2017-02-24 10:15:26', NULL),
(145, ' إضافة فاتورة برقم.14', 'info', 10, '2017-02-24 10:15:26', '2017-02-24 10:15:26', NULL),
(146, 'حاله الفاتورة رقم.3 تغيرت لتكون فيد الانتظار', 'info', 3, '2017-02-24 10:56:28', '2017-02-24 10:56:28', NULL),
(147, 'حاله الفاتورة رقم.3 تغيرت لتكون ارسال', 'info', 3, '2017-02-24 10:59:48', '2017-02-24 10:59:48', NULL),
(148, 'حاله الفاتورة رقم.4 تغيرت لتكون ارسال', 'info', 3, '2017-02-24 11:02:39', '2017-02-24 11:02:39', NULL),
(149, 'حاله الفاتورة رقم.5 تغيرت لتكون ارسال', 'info', 3, '2017-02-24 11:07:13', '2017-02-24 11:07:13', NULL),
(150, 'حاله الفاتورة رقم.6 تغيرت لتكون ارسال', 'info', 3, '2017-02-24 11:09:36', '2017-02-24 11:09:36', NULL),
(151, ' تعديل كمية الماده الخام زيت من 7495.00 الي 7485', 'info', 2, '2017-03-08 08:27:11', '2017-03-08 08:27:11', NULL),
(152, ' تعديل كمية الماده الخام زيت من 7495.00 الي 7485', 'info', 10, '2017-03-08 08:27:11', '2017-03-08 08:27:11', NULL),
(153, ' تعديل كمية الماده الخام عود من 3505.00 الي 3485', 'info', 2, '2017-03-08 08:27:16', '2017-03-08 08:27:16', NULL),
(154, ' تعديل كمية الماده الخام عود من 3505.00 الي 3485', 'info', 10, '2017-03-08 08:27:16', '2017-03-08 08:27:16', NULL),
(155, ' إضافة فاتورة مرتجع برقم.1', 'info', 2, '2017-03-08 08:27:18', '2017-03-08 08:27:18', NULL),
(156, ' إضافة فاتورة مرتجع برقم.1', 'info', 10, '2017-03-08 08:27:18', '2017-03-08 08:27:18', NULL),
(157, 'إضافة منتج جديد (عطر عربي) برقم.4', 'info', 2, '2017-04-26 15:56:08', '2017-04-26 15:56:08', NULL),
(158, 'إضافة منتج جديد (عطر عربي) برقم.4', 'info', 10, '2017-04-26 15:56:08', '2017-04-26 15:56:08', NULL),
(159, 'إضافة منتج جديد (عطر عربي) برقم.5', 'info', 2, '2017-04-27 09:31:10', '2017-04-27 09:31:10', NULL),
(160, 'إضافة منتج جديد (عطر عربي) برقم.5', 'info', 10, '2017-04-27 09:31:10', '2017-04-27 09:31:10', NULL),
(161, 'تعديل المنتج رقم.4', 'info', 2, '2017-04-27 09:33:11', '2017-04-27 09:33:11', NULL),
(162, 'تعديل المنتج رقم.4', 'info', 10, '2017-04-27 09:33:11', '2017-04-27 09:33:11', NULL),
(163, 'تعديل المنتج رقم.4', 'info', 2, '2017-04-27 09:34:31', '2017-04-27 09:34:31', NULL),
(164, 'تعديل المنتج رقم.4', 'info', 10, '2017-04-27 09:34:31', '2017-04-27 09:34:31', NULL),
(165, 'تعديل المنتج رقم.4', 'info', 2, '2017-04-27 09:34:43', '2017-04-27 09:34:43', NULL),
(166, 'تعديل المنتج رقم.4', 'info', 10, '2017-04-27 09:34:43', '2017-04-27 09:34:43', NULL),
(167, 'تعديل المنتج رقم.3', 'info', 2, '2017-04-27 15:13:50', '2017-04-27 15:13:50', NULL),
(168, 'تعديل المنتج رقم.3', 'info', 10, '2017-04-27 15:13:50', '2017-04-27 15:13:50', NULL),
(169, 'تعديل المنتج رقم.3', 'info', 2, '2017-04-27 15:14:00', '2017-04-27 15:14:00', NULL),
(170, 'تعديل المنتج رقم.3', 'info', 10, '2017-04-27 15:14:00', '2017-04-27 15:14:00', NULL),
(171, 'تعديل المنتج رقم.2', 'info', 2, '2017-04-27 15:14:22', '2017-04-27 15:14:22', NULL),
(172, 'تعديل المنتج رقم.2', 'info', 10, '2017-04-27 15:14:22', '2017-04-27 15:14:22', NULL),
(173, 'تعديل المنتج رقم.1', 'info', 2, '2017-04-27 15:14:42', '2017-04-27 15:14:42', NULL),
(174, 'تعديل المنتج رقم.1', 'info', 10, '2017-04-27 15:14:42', '2017-04-27 15:14:42', NULL),
(175, ' تعديل الفرع  Branch للمنتجات (برفان هوجو)  لترجيع كمية 5 ', 'info', 2, '2017-05-11 10:17:12', '2017-05-11 10:17:12', NULL),
(176, ' تعديل الفرع  Branch للمنتجات (برفان هوجو)  لترجيع كمية 5 ', 'info', 10, '2017-05-11 10:17:12', '2017-05-11 10:17:12', NULL),
(177, 'حاله الفاتورة رقم.7 تغيرت لتكون قيد الانتظار', 'info', 18, '2017-05-11 12:00:18', '2017-05-11 12:00:18', NULL),
(178, 'المنتج ''برفان هوجو'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-11 12:01:37', '2017-05-11 12:01:37', NULL),
(179, 'المنتج ''برفان هوجو'' وصلت كميته لحد الطلب ', 'info', 10, '2017-05-11 12:01:37', '2017-05-11 12:01:37', NULL),
(180, 'المنتج ''برفان هورث'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-11 12:01:41', '2017-05-11 12:01:41', NULL),
(181, 'المنتج ''برفان هورث'' وصلت كميته لحد الطلب ', 'info', 10, '2017-05-11 12:01:41', '2017-05-11 12:01:41', NULL),
(182, 'حاله الفاتورة رقم.7 تغيرت لتكون ارسال', 'info', 18, '2017-05-11 12:01:43', '2017-05-11 12:01:43', NULL),
(183, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-24 18:45:52', '2017-05-24 18:45:52', NULL),
(184, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 10, '2017-05-24 18:45:52', '2017-05-24 18:45:52', NULL),
(185, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 30, '2017-05-24 18:45:52', '2017-05-24 18:45:52', NULL),
(186, 'إضافة منتج جديد (عطر رقم 1) برقم.6', 'info', 2, '2017-05-25 03:33:59', '2017-05-25 03:33:59', NULL),
(187, 'إضافة منتج جديد (عطر رقم 1) برقم.6', 'info', 10, '2017-05-25 03:33:59', '2017-05-25 03:33:59', NULL),
(188, 'إضافة منتج جديد (عطر رقم 1) برقم.6', 'info', 30, '2017-05-25 03:33:59', '2017-05-25 03:33:59', NULL),
(189, 'تعديل المنتج رقم.6', 'info', 2, '2017-05-25 03:34:44', '2017-05-25 03:34:44', NULL),
(190, 'تعديل المنتج رقم.6', 'info', 10, '2017-05-25 03:34:44', '2017-05-25 03:34:44', NULL),
(191, 'تعديل المنتج رقم.6', 'info', 30, '2017-05-25 03:34:44', '2017-05-25 03:34:44', NULL),
(192, 'إضافة منتج جديد (عطر رقم 2) برقم.7', 'info', 2, '2017-05-25 03:35:46', '2017-05-25 03:35:46', NULL),
(193, 'إضافة منتج جديد (عطر رقم 2) برقم.7', 'info', 10, '2017-05-25 03:35:46', '2017-05-25 03:35:46', NULL),
(194, 'إضافة منتج جديد (عطر رقم 2) برقم.7', 'info', 30, '2017-05-25 03:35:46', '2017-05-25 03:35:46', NULL),
(195, 'تعديل المنتج رقم.6', 'info', 2, '2017-05-25 03:36:42', '2017-05-25 03:36:42', NULL),
(196, 'تعديل المنتج رقم.6', 'info', 10, '2017-05-25 03:36:42', '2017-05-25 03:36:42', NULL),
(197, 'تعديل المنتج رقم.6', 'info', 30, '2017-05-25 03:36:42', '2017-05-25 03:36:42', NULL),
(198, 'إضافة منتج جديد (عطر 3) برقم.8', 'info', 2, '2017-05-25 03:37:17', '2017-05-25 03:37:17', NULL),
(199, 'إضافة منتج جديد (عطر 3) برقم.8', 'info', 10, '2017-05-25 03:37:17', '2017-05-25 03:37:17', NULL),
(200, 'إضافة منتج جديد (عطر 3) برقم.8', 'info', 30, '2017-05-25 03:37:17', '2017-05-25 03:37:17', NULL),
(201, 'إضافة منتج جديد (عطر 3) برقم.9', 'info', 2, '2017-05-25 03:37:48', '2017-05-25 03:37:48', NULL),
(202, 'إضافة منتج جديد (عطر 3) برقم.9', 'info', 10, '2017-05-25 03:37:48', '2017-05-25 03:37:48', NULL),
(203, 'إضافة منتج جديد (عطر 3) برقم.9', 'info', 30, '2017-05-25 03:37:48', '2017-05-25 03:37:48', NULL),
(204, 'إضافة منتج جديد (عطر 4) برقم.10', 'info', 2, '2017-05-25 16:35:12', '2017-05-25 16:35:12', NULL),
(205, 'إضافة منتج جديد (عطر 4) برقم.10', 'info', 10, '2017-05-25 16:35:12', '2017-05-25 16:35:12', NULL),
(206, 'إضافة منتج جديد (عطر 4) برقم.10', 'info', 30, '2017-05-25 16:35:12', '2017-05-25 16:35:12', NULL),
(207, 'إضافة منتج جديد (perfum) برقم.11', 'info', 2, '2017-05-25 16:39:40', '2017-05-25 16:39:40', NULL),
(208, 'إضافة منتج جديد (perfum) برقم.11', 'info', 10, '2017-05-25 16:39:40', '2017-05-25 16:39:40', NULL),
(209, 'إضافة منتج جديد (perfum) برقم.11', 'info', 30, '2017-05-25 16:39:40', '2017-05-25 16:39:40', NULL),
(210, 'إضافة منتج جديد (دايفكس) برقم.12', 'info', 2, '2017-05-25 16:39:42', '2017-05-25 16:39:42', NULL),
(211, 'إضافة منتج جديد (دايفكس) برقم.12', 'info', 10, '2017-05-25 16:39:42', '2017-05-25 16:39:42', NULL),
(212, 'إضافة منتج جديد (دايفكس) برقم.12', 'info', 30, '2017-05-25 16:39:42', '2017-05-25 16:39:42', NULL),
(213, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-25 18:09:08', '2017-05-25 18:09:08', NULL),
(214, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 10, '2017-05-25 18:09:08', '2017-05-25 18:09:08', NULL),
(215, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 30, '2017-05-25 18:09:08', '2017-05-25 18:09:08', NULL),
(216, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-25 19:38:33', '2017-05-25 19:38:33', NULL),
(217, 'تم إضافة تارجت جديد للفرع ', 'info', 10, '2017-05-25 19:38:33', '2017-05-25 19:38:33', NULL),
(218, 'تم إضافة تارجت جديد للفرع ', 'info', 30, '2017-05-25 19:38:33', '2017-05-25 19:38:33', NULL),
(219, 'تم إضافة تارجت جديد للفرع ', 'info', 3, '2017-05-25 19:38:33', '2017-05-25 19:38:33', NULL),
(220, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-25 19:38:42', '2017-05-25 19:38:42', NULL),
(221, 'تم تعديل تارجت للفرع ', 'info', 10, '2017-05-25 19:38:42', '2017-05-25 19:38:42', NULL),
(222, 'تم تعديل تارجت للفرع ', 'info', 30, '2017-05-25 19:38:42', '2017-05-25 19:38:42', NULL),
(223, 'تم تعديل تارجت للفرع ', 'info', 3, '2017-05-25 19:38:42', '2017-05-25 19:38:42', NULL),
(224, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-25 19:39:07', '2017-05-25 19:39:07', NULL),
(225, 'تم إضافة تارجت جديد للفرع ', 'info', 10, '2017-05-25 19:39:07', '2017-05-25 19:39:07', NULL),
(226, 'تم إضافة تارجت جديد للفرع ', 'info', 30, '2017-05-25 19:39:07', '2017-05-25 19:39:07', NULL),
(227, 'تم إضافة تارجت جديد للفرع ', 'info', 3, '2017-05-25 19:39:07', '2017-05-25 19:39:07', NULL),
(228, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-25 19:39:30', '2017-05-25 19:39:30', NULL),
(229, 'تم إضافة تارجت جديد للفرع ', 'info', 10, '2017-05-25 19:39:30', '2017-05-25 19:39:30', NULL),
(230, 'تم إضافة تارجت جديد للفرع ', 'info', 30, '2017-05-25 19:39:30', '2017-05-25 19:39:30', NULL),
(231, 'تم إضافة تارجت جديد للفرع ', 'info', 3, '2017-05-25 19:39:30', '2017-05-25 19:39:30', NULL),
(232, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 2, '2017-05-26 00:43:49', '2017-05-26 00:43:49', NULL),
(233, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 10, '2017-05-26 00:43:49', '2017-05-26 00:43:49', NULL),
(234, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 30, '2017-05-26 00:43:49', '2017-05-26 00:43:49', NULL),
(235, ' لقد تم تسجيل العملاء بنجاح ', 'info', 2, '2017-05-26 00:45:23', '2017-05-26 00:45:23', NULL),
(236, ' لقد تم تسجيل العملاء بنجاح ', 'info', 10, '2017-05-26 00:45:23', '2017-05-26 00:45:23', NULL),
(237, ' لقد تم تسجيل العملاء بنجاح ', 'info', 30, '2017-05-26 00:45:23', '2017-05-26 00:45:23', NULL),
(238, 'حاله الفاتورة رقم.8 تغيرت لتكون قيد الانتظار', 'info', 3, '2017-05-26 01:13:23', '2017-05-26 01:13:23', NULL),
(239, 'المنتج ''دايفكس'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-26 01:13:35', '2017-05-26 01:13:35', NULL),
(240, 'المنتج ''دايفكس'' وصلت كميته لحد الطلب ', 'info', 10, '2017-05-26 01:13:35', '2017-05-26 01:13:35', NULL),
(241, 'المنتج ''دايفكس'' وصلت كميته لحد الطلب ', 'info', 30, '2017-05-26 01:13:35', '2017-05-26 01:13:35', NULL),
(242, 'حاله الفاتورة رقم.8 تغيرت لتكون ارسال', 'info', 3, '2017-05-26 01:13:35', '2017-05-26 01:13:35', NULL),
(243, 'المنتج ''عطر عربي'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-26 01:15:06', '2017-05-26 01:15:06', NULL),
(244, 'المنتج ''عطر عربي'' وصلت كميته لحد الطلب ', 'info', 10, '2017-05-26 01:15:06', '2017-05-26 01:15:06', NULL),
(245, 'المنتج ''عطر عربي'' وصلت كميته لحد الطلب ', 'info', 30, '2017-05-26 01:15:06', '2017-05-26 01:15:06', NULL),
(246, 'حاله الفاتورة رقم.9 تغيرت لتكون ارسال', 'info', 31, '2017-05-26 01:15:06', '2017-05-26 01:15:06', NULL),
(247, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 2, '2017-05-26 06:43:05', '2017-05-26 06:43:05', NULL),
(248, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 10, '2017-05-26 06:43:05', '2017-05-26 06:43:05', NULL),
(249, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 30, '2017-05-26 06:43:05', '2017-05-26 06:43:05', NULL),
(250, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 2, '2017-05-26 06:43:13', '2017-05-26 06:43:13', NULL),
(251, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 10, '2017-05-26 06:43:13', '2017-05-26 06:43:13', NULL),
(252, ' تم تعديل العنصر رقم 2 لقالب الفاتورة رقم 1', 'info', 30, '2017-05-26 06:43:13', '2017-05-26 06:43:13', NULL),
(253, ' تم تعديل وحدة قياس ', 'info', 2, '2017-05-26 20:38:42', '2017-05-26 20:38:42', NULL),
(254, ' تم تعديل وحدة قياس ', 'info', 10, '2017-05-26 20:38:42', '2017-05-26 20:38:42', NULL),
(255, ' تم تعديل وحدة قياس ', 'info', 30, '2017-05-26 20:38:42', '2017-05-26 20:38:42', NULL),
(256, ' لقد تم تسجيل المنتجات اول مده بنجاح ', 'info', 2, '2017-05-26 21:38:57', '2017-05-26 21:38:57', NULL),
(257, ' لقد تم تسجيل المنتجات اول مده بنجاح ', 'info', 10, '2017-05-26 21:38:57', '2017-05-26 21:38:57', NULL),
(258, ' لقد تم تسجيل المنتجات اول مده بنجاح ', 'info', 30, '2017-05-26 21:38:57', '2017-05-26 21:38:57', NULL),
(259, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-27 21:34:54', '2017-05-27 21:34:54', NULL),
(260, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 10, '2017-05-27 21:34:54', '2017-05-27 21:34:54', NULL),
(261, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 30, '2017-05-27 21:34:54', '2017-05-27 21:34:54', NULL),
(262, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-27 21:39:32', '2017-05-27 21:39:32', NULL),
(263, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 10, '2017-05-27 21:39:32', '2017-05-27 21:39:32', NULL),
(264, 'تم تعديل قالب الفاتورة رقم 1يمكنك تعديل العناصر للقالب من هنا ', 'info', 30, '2017-05-27 21:39:32', '2017-05-27 21:39:32', NULL),
(265, ' تم تعديل العنصر رقم 1 لقالب الفاتورة رقم 1', 'info', 2, '2017-05-28 02:09:24', '2017-05-28 02:09:24', NULL),
(266, ' تم تعديل العنصر رقم 1 لقالب الفاتورة رقم 1', 'info', 10, '2017-05-28 02:09:24', '2017-05-28 02:09:24', NULL),
(267, ' تم تعديل العنصر رقم 1 لقالب الفاتورة رقم 1', 'info', 30, '2017-05-28 02:09:24', '2017-05-28 02:09:24', NULL),
(268, 'لقد تم مسح قالب فاتورة رقم #1', 'info', 2, '2017-05-28 02:12:42', '2017-05-28 02:12:42', NULL),
(269, 'لقد تم مسح قالب فاتورة رقم #1', 'info', 10, '2017-05-28 02:12:42', '2017-05-28 02:12:42', NULL),
(270, 'لقد تم مسح قالب فاتورة رقم #1', 'info', 30, '2017-05-28 02:12:42', '2017-05-28 02:12:42', NULL),
(271, 'تم إضافة قالب فاتورة جديد برقم 2 يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-28 02:13:11', '2017-05-28 02:13:11', NULL),
(272, 'تم إضافة قالب فاتورة جديد برقم 2 يمكنك تعديل العناصر للقالب من هنا ', 'info', 10, '2017-05-28 02:13:11', '2017-05-28 02:13:11', NULL),
(273, 'تم إضافة قالب فاتورة جديد برقم 2 يمكنك تعديل العناصر للقالب من هنا ', 'info', 30, '2017-05-28 02:13:11', '2017-05-28 02:13:11', NULL),
(274, ' تم تعديل العنصر رقم 19 لقالب الفاتورة رقم 2', 'info', 2, '2017-05-28 02:13:35', '2017-05-28 02:13:35', NULL),
(275, ' تم تعديل العنصر رقم 19 لقالب الفاتورة رقم 2', 'info', 10, '2017-05-28 02:13:35', '2017-05-28 02:13:35', NULL),
(276, ' تم تعديل العنصر رقم 19 لقالب الفاتورة رقم 2', 'info', 30, '2017-05-28 02:13:35', '2017-05-28 02:13:35', NULL),
(277, ' اضافة وحدة قياس جديدة', 'info', 2, '2017-05-28 02:16:54', '2017-05-28 02:16:54', NULL),
(278, ' اضافة وحدة قياس جديدة', 'info', 10, '2017-05-28 02:16:54', '2017-05-28 02:16:54', NULL),
(279, ' اضافة وحدة قياس جديدة', 'info', 30, '2017-05-28 02:16:54', '2017-05-28 02:16:54', NULL),
(280, 'تعديل المنتج رقم.1', 'info', 2, '2017-05-28 02:19:39', '2017-05-28 02:19:39', NULL),
(281, 'تعديل المنتج رقم.1', 'info', 10, '2017-05-28 02:19:39', '2017-05-28 02:19:39', NULL),
(282, 'تعديل المنتج رقم.1', 'info', 30, '2017-05-28 02:19:39', '2017-05-28 02:19:39', NULL),
(283, 'تعديل المنتج رقم.2', 'info', 2, '2017-05-28 02:20:34', '2017-05-28 02:20:34', NULL),
(284, 'تعديل المنتج رقم.2', 'info', 10, '2017-05-28 02:20:34', '2017-05-28 02:20:34', NULL),
(285, 'تعديل المنتج رقم.2', 'info', 30, '2017-05-28 02:20:34', '2017-05-28 02:20:34', NULL),
(286, 'تعديل المنتج رقم.3', 'info', 2, '2017-05-28 02:21:13', '2017-05-28 02:21:13', NULL),
(287, 'تعديل المنتج رقم.3', 'info', 10, '2017-05-28 02:21:13', '2017-05-28 02:21:13', NULL),
(288, 'تعديل المنتج رقم.3', 'info', 30, '2017-05-28 02:21:13', '2017-05-28 02:21:13', NULL),
(289, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-28 03:19:16', '2017-05-28 03:19:16', NULL),
(290, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-28 03:19:16', '2017-05-28 03:19:16', NULL),
(291, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 2, '2017-05-28 03:41:22', '2017-05-28 03:41:22', NULL),
(292, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 54, '2017-05-28 03:41:22', '2017-05-28 03:41:22', NULL),
(293, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 2, '2017-05-28 04:00:13', '2017-05-28 04:00:13', NULL),
(294, ' لقد تم تسجيل الكوبونات بنجاح ', 'info', 54, '2017-05-28 04:00:13', '2017-05-28 04:00:13', NULL),
(295, ' تم حفظ قاعده عمولة جديدة برقم 3', 'info', 2, '2017-05-28 04:06:02', '2017-05-28 04:06:02', NULL),
(296, ' تم حفظ قاعده عمولة جديدة برقم 3', 'info', 54, '2017-05-28 04:06:02', '2017-05-28 04:06:02', NULL),
(297, ' تم حفظ قاعده عمولة جديدة برقم 4', 'info', 2, '2017-05-28 04:07:19', '2017-05-28 04:07:19', NULL),
(298, ' تم حفظ قاعده عمولة جديدة برقم 4', 'info', 54, '2017-05-28 04:07:19', '2017-05-28 04:07:19', NULL),
(299, ' تم حفظ قاعده عمولة جديدة برقم 5', 'info', 2, '2017-05-28 04:07:39', '2017-05-28 04:07:39', NULL),
(300, ' تم حفظ قاعده عمولة جديدة برقم 5', 'info', 54, '2017-05-28 04:07:39', '2017-05-28 04:07:39', NULL),
(301, ' تم حفظ قاعده عمولة جديدة برقم 6', 'info', 2, '2017-05-28 04:08:05', '2017-05-28 04:08:05', NULL),
(302, ' تم حفظ قاعده عمولة جديدة برقم 6', 'info', 54, '2017-05-28 04:08:05', '2017-05-28 04:08:05', NULL),
(303, ' تم حفظ قاعده عمولة جديدة برقم 7', 'info', 2, '2017-05-28 04:08:25', '2017-05-28 04:08:25', NULL),
(304, ' تم حفظ قاعده عمولة جديدة برقم 7', 'info', 54, '2017-05-28 04:08:25', '2017-05-28 04:08:25', NULL),
(305, ' تم حفظ قاعده عمولة جديدة برقم 8', 'info', 2, '2017-05-28 04:08:46', '2017-05-28 04:08:46', NULL),
(306, ' تم حفظ قاعده عمولة جديدة برقم 8', 'info', 54, '2017-05-28 04:08:46', '2017-05-28 04:08:46', NULL),
(307, ' تم حفظ قاعده عمولة جديدة برقم 9', 'info', 2, '2017-05-28 04:09:12', '2017-05-28 04:09:12', NULL),
(308, ' تم حفظ قاعده عمولة جديدة برقم 9', 'info', 54, '2017-05-28 04:09:12', '2017-05-28 04:09:12', NULL),
(309, ' تم حفظ قاعده عمولة جديدة برقم 10', 'info', 2, '2017-05-28 04:09:35', '2017-05-28 04:09:35', NULL),
(310, ' تم حفظ قاعده عمولة جديدة برقم 10', 'info', 54, '2017-05-28 04:09:35', '2017-05-28 04:09:35', NULL),
(311, ' تم حفظ قاعده عمولة جديدة برقم 11', 'info', 2, '2017-05-28 04:09:56', '2017-05-28 04:09:56', NULL),
(312, ' تم حفظ قاعده عمولة جديدة برقم 11', 'info', 54, '2017-05-28 04:09:56', '2017-05-28 04:09:56', NULL),
(313, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:14:32', '2017-05-28 04:14:32', NULL),
(314, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:14:32', '2017-05-28 04:14:32', NULL),
(315, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:14:32', '2017-05-28 04:14:32', NULL),
(316, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:14:53', '2017-05-28 04:14:53', NULL),
(317, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:14:53', '2017-05-28 04:14:53', NULL),
(318, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:14:53', '2017-05-28 04:14:53', NULL),
(319, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:15:26', '2017-05-28 04:15:26', NULL),
(320, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:15:26', '2017-05-28 04:15:26', NULL),
(321, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:15:26', '2017-05-28 04:15:26', NULL),
(322, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:16:06', '2017-05-28 04:16:06', NULL),
(323, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:16:06', '2017-05-28 04:16:06', NULL),
(324, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:16:06', '2017-05-28 04:16:06', NULL),
(325, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:16:23', '2017-05-28 04:16:23', NULL),
(326, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:16:23', '2017-05-28 04:16:23', NULL),
(327, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:16:23', '2017-05-28 04:16:23', NULL),
(328, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:17:14', '2017-05-28 04:17:14', NULL),
(329, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:17:14', '2017-05-28 04:17:14', NULL),
(330, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:17:14', '2017-05-28 04:17:14', NULL),
(331, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:17:40', '2017-05-28 04:17:40', NULL),
(332, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:17:40', '2017-05-28 04:17:40', NULL),
(333, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:17:40', '2017-05-28 04:17:40', NULL),
(334, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:17:53', '2017-05-28 04:17:53', NULL),
(335, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:17:53', '2017-05-28 04:17:53', NULL),
(336, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:17:53', '2017-05-28 04:17:53', NULL),
(337, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:18:04', '2017-05-28 04:18:04', NULL),
(338, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:18:04', '2017-05-28 04:18:04', NULL),
(339, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:18:04', '2017-05-28 04:18:04', NULL),
(340, 'تم تعديل تارجت للفرع ', 'info', 2, '2017-05-28 04:18:15', '2017-05-28 04:18:15', NULL),
(341, 'تم تعديل تارجت للفرع ', 'info', 54, '2017-05-28 04:18:15', '2017-05-28 04:18:15', NULL),
(342, 'تم تعديل تارجت للفرع ', 'info', 55, '2017-05-28 04:18:15', '2017-05-28 04:18:15', NULL),
(343, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:18:45', '2017-05-28 04:18:45', NULL),
(344, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:18:45', '2017-05-28 04:18:45', NULL),
(345, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:18:45', '2017-05-28 04:18:45', NULL),
(346, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:19:09', '2017-05-28 04:19:09', NULL),
(347, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:19:09', '2017-05-28 04:19:09', NULL),
(348, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:19:09', '2017-05-28 04:19:09', NULL),
(349, 'تم إضافة تارجت جديد للفرع ', 'info', 2, '2017-05-28 04:19:24', '2017-05-28 04:19:24', NULL),
(350, 'تم إضافة تارجت جديد للفرع ', 'info', 54, '2017-05-28 04:19:24', '2017-05-28 04:19:24', NULL),
(351, 'تم إضافة تارجت جديد للفرع ', 'info', 55, '2017-05-28 04:19:24', '2017-05-28 04:19:24', NULL),
(352, ' لقد تم تسجيل العملاء بنجاح ', 'info', 2, '2017-05-28 04:29:56', '2017-05-28 04:29:56', NULL),
(353, ' لقد تم تسجيل العملاء بنجاح ', 'info', 54, '2017-05-28 04:29:56', '2017-05-28 04:29:56', NULL),
(354, 'حاله الفاتورة رقم.10 تغيرت لتكون ارسال', 'info', 55, '2017-05-28 04:50:23', '2017-05-28 04:50:23', NULL),
(355, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:28:47', '2017-05-29 03:28:47', NULL),
(356, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:28:47', '2017-05-29 03:28:47', NULL),
(357, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:32:15', '2017-05-29 03:32:15', NULL),
(358, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:32:15', '2017-05-29 03:32:15', NULL),
(359, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:34:48', '2017-05-29 03:34:48', NULL),
(360, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:34:48', '2017-05-29 03:34:48', NULL),
(361, 'تعديل المنتج رقم.725', 'info', 2, '2017-05-29 03:43:29', '2017-05-29 03:43:29', NULL),
(362, 'تعديل المنتج رقم.725', 'info', 54, '2017-05-29 03:43:29', '2017-05-29 03:43:29', NULL),
(363, 'تعديل المنتج رقم.726', 'info', 2, '2017-05-29 03:43:38', '2017-05-29 03:43:38', NULL),
(364, 'تعديل المنتج رقم.726', 'info', 54, '2017-05-29 03:43:38', '2017-05-29 03:43:38', NULL),
(365, 'تعديل المنتج رقم.727', 'info', 2, '2017-05-29 03:43:46', '2017-05-29 03:43:46', NULL),
(366, 'تعديل المنتج رقم.727', 'info', 54, '2017-05-29 03:43:46', '2017-05-29 03:43:46', NULL),
(367, 'تعديل المنتج رقم.728', 'info', 2, '2017-05-29 03:43:56', '2017-05-29 03:43:56', NULL),
(368, 'تعديل المنتج رقم.728', 'info', 54, '2017-05-29 03:43:56', '2017-05-29 03:43:56', NULL),
(369, 'تعديل المنتج رقم.729', 'info', 2, '2017-05-29 03:44:07', '2017-05-29 03:44:07', NULL),
(370, 'تعديل المنتج رقم.729', 'info', 54, '2017-05-29 03:44:07', '2017-05-29 03:44:07', NULL),
(371, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:45:53', '2017-05-29 03:45:53', NULL),
(372, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:45:53', '2017-05-29 03:45:53', NULL),
(373, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:50:22', '2017-05-29 03:50:22', NULL),
(374, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:50:22', '2017-05-29 03:50:22', NULL),
(375, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 03:55:36', '2017-05-29 03:55:36', NULL),
(376, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 03:55:36', '2017-05-29 03:55:36', NULL),
(377, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 04:07:43', '2017-05-29 04:07:43', NULL),
(378, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 04:07:43', '2017-05-29 04:07:43', NULL),
(379, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 04:10:14', '2017-05-29 04:10:14', NULL),
(380, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 04:10:14', '2017-05-29 04:10:14', NULL),
(381, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 2, '2017-05-29 04:13:50', '2017-05-29 04:13:50', NULL),
(382, ' لقد تم تسجيل المنتجات بنجاح ', 'info', 54, '2017-05-29 04:13:50', '2017-05-29 04:13:50', NULL),
(383, 'حاله الفاتورة رقم.12 تغيرت لتكون ارسال', 'info', 55, '2017-05-30 02:11:11', '2017-05-30 02:11:11', NULL),
(384, 'المنتج ''معمول ملكي فاخر 45 جرام'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-30 02:11:40', '2017-05-30 02:11:40', NULL),
(385, 'المنتج ''معمول ملكي فاخر 45 جرام'' وصلت كميته لحد الطلب ', 'info', 54, '2017-05-30 02:11:40', '2017-05-30 02:11:40', NULL),
(386, 'المنتج ''معمول ملكي ممتاز 100 جرام'' وصلت كميته لحد الطلب ', 'info', 2, '2017-05-30 02:11:41', '2017-05-30 02:11:41', NULL),
(387, 'المنتج ''معمول ملكي ممتاز 100 جرام'' وصلت كميته لحد الطلب ', 'info', 54, '2017-05-30 02:11:41', '2017-05-30 02:11:41', NULL),
(388, 'حاله الفاتورة رقم.13 تغيرت لتكون ارسال', 'info', 55, '2017-05-30 02:11:41', '2017-05-30 02:11:41', NULL),
(389, ' تم تعديل العنصر رقم 36 لقالب الفاتورة رقم 2', 'info', 2, '2017-05-31 02:42:42', '2017-05-31 02:42:42', NULL),
(390, ' تم تعديل العنصر رقم 36 لقالب الفاتورة رقم 2', 'info', 54, '2017-05-31 02:42:42', '2017-05-31 02:42:42', NULL),
(391, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-05-31 23:34:33', '2017-05-31 23:34:33', NULL),
(392, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-05-31 23:34:33', '2017-05-31 23:34:33', NULL),
(393, 'حاله الفاتورة رقم.14 تغيرت لتكون ارسال', 'info', 6379, '2017-06-01 07:54:17', '2017-06-01 07:54:17', NULL),
(394, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-01 19:29:28', '2017-06-01 19:29:28', NULL);
INSERT INTO `notification` (`not_id`, `not_title`, `not_type`, `not_to_userid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(395, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-01 19:29:28', '2017-06-01 19:29:28', NULL),
(396, 'حاله الفاتورة رقم.16 تغيرت لتكون قيد الانتظار', 'info', 6379, '2017-06-01 20:58:14', '2017-06-01 20:58:14', NULL),
(397, 'حاله الفاتورة رقم.16 تغيرت لتكون ارسال', 'info', 6379, '2017-06-01 20:58:33', '2017-06-01 20:58:33', NULL),
(398, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:26:28', '2017-06-03 20:26:28', NULL),
(399, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:26:28', '2017-06-03 20:26:28', NULL),
(400, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:29:09', '2017-06-03 20:29:09', NULL),
(401, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:29:09', '2017-06-03 20:29:09', NULL),
(402, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:31:54', '2017-06-03 20:31:54', NULL),
(403, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:31:54', '2017-06-03 20:31:54', NULL),
(404, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:34:16', '2017-06-03 20:34:16', NULL),
(405, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:34:16', '2017-06-03 20:34:16', NULL),
(406, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:35:24', '2017-06-03 20:35:24', NULL),
(407, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:35:24', '2017-06-03 20:35:24', NULL),
(408, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 2, '2017-06-03 20:35:54', '2017-06-03 20:35:54', NULL),
(409, 'تم تعديل قالب الفاتورة رقم 2يمكنك تعديل العناصر للقالب من هنا ', 'info', 54, '2017-06-03 20:35:54', '2017-06-03 20:35:54', NULL),
(410, 'حاله الفاتورة رقم.17 تغيرت لتكون ارسال', 'info', 6379, '2017-06-04 02:55:28', '2017-06-04 02:55:28', NULL),
(411, 'حاله الفاتورة رقم.15 تغيرت لتكون ارسال', 'info', 55, '2017-06-04 02:55:50', '2017-06-04 02:55:50', NULL),
(412, ' الفرع فرع المروج يستحق عموله بقيمة35.00 بتاريخ2017-05-31', 'info', 55, '2017-06-04 17:56:02', '2017-06-04 17:56:02', NULL),
(413, ' الفرع فرع الروضة يستحق عموله بقيمة35.00 بتاريخ2017-06-01', 'info', 6379, '2017-06-04 17:56:02', '2017-06-04 17:56:02', NULL),
(414, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 2, '2017-06-05 03:26:26', '2017-06-05 03:26:26', NULL),
(415, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 54, '2017-06-05 03:26:26', '2017-06-05 03:26:26', NULL),
(416, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 2, '2017-06-05 03:28:24', '2017-06-05 03:28:24', NULL),
(417, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 54, '2017-06-05 03:28:24', '2017-06-05 03:28:24', NULL),
(418, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 2, '2017-06-05 20:04:45', '2017-06-05 20:04:45', NULL),
(419, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 54, '2017-06-05 20:04:45', '2017-06-05 20:04:45', NULL),
(420, ' تم تعديل العنصر رقم 33 لقالب الفاتورة رقم 2', 'info', 6386, '2017-06-05 20:04:45', '2017-06-05 20:04:45', NULL),
(421, ' تم تعديل العنصر رقم 32 لقالب الفاتورة رقم 2', 'info', 2, '2017-06-05 20:05:15', '2017-06-05 20:05:15', NULL),
(422, ' تم تعديل العنصر رقم 32 لقالب الفاتورة رقم 2', 'info', 54, '2017-06-05 20:05:15', '2017-06-05 20:05:15', NULL),
(423, ' تم تعديل العنصر رقم 32 لقالب الفاتورة رقم 2', 'info', 6386, '2017-06-05 20:05:15', '2017-06-05 20:05:15', NULL),
(424, ' تم تعديل العنصر رقم 36 لقالب الفاتورة رقم 2', 'info', 2, '2017-06-05 20:07:22', '2017-06-05 20:07:22', NULL),
(425, ' تم تعديل العنصر رقم 36 لقالب الفاتورة رقم 2', 'info', 54, '2017-06-05 20:07:22', '2017-06-05 20:07:22', NULL),
(426, ' تم تعديل العنصر رقم 36 لقالب الفاتورة رقم 2', 'info', 6386, '2017-06-05 20:07:22', '2017-06-05 20:07:22', NULL),
(427, ' الفرع فرع المروج يستحق عموله بقيمة50.00 بتاريخ2017-06-05 16:00:02', 'info', 55, '2017-06-05 23:00:03', '2017-06-05 23:00:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `old_assets`
--

CREATE TABLE IF NOT EXISTS `old_assets` (
  `asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `asset_name` varchar(200) NOT NULL,
  `asset_desc` text NOT NULL,
  `asset_cost` decimal(10,2) NOT NULL,
  `asset_original_cost` decimal(10,2) NOT NULL,
  `asset_broken_percentage` decimal(10,2) NOT NULL,
  `asset_buy_date` date NOT NULL,
  `last_calculated_date` date NOT NULL,
  `asset_sell_amount` decimal(10,2) DEFAULT NULL,
  `asset_sell_date` date DEFAULT NULL,
  `period_in_years` int(11) NOT NULL,
  `used_period` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `old_assets`
--

INSERT INTO `old_assets` (`asset_id`, `user_id`, `asset_name`, `asset_desc`, `asset_cost`, `asset_original_cost`, `asset_broken_percentage`, `asset_buy_date`, `last_calculated_date`, `asset_sell_amount`, `asset_sell_date`, `period_in_years`, `used_period`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'مبني', '', '10000.00', '90000.00', '10.00', '2017-02-02', '2017-02-02', '0.00', '0000-00-00', 5, 0.00, '2017-02-02 12:16:09', '2017-02-02 12:16:09', NULL),
(2, 3, 'مكنه', 'وصف', '1000.00', '1000.00', '25.00', '2017-02-05', '2017-02-05', '0.00', '0000-00-00', 5, 0.00, '2017-02-05 14:14:27', '2017-02-05 14:14:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_cats` text NOT NULL,
  `package_name` varchar(200) NOT NULL,
  `package_start_date` date NOT NULL,
  `package_end_date` date NOT NULL,
  `min_products_to_get_offer` int(11) NOT NULL,
  `min_products_to_get_gift` int(11) NOT NULL,
  `product_gift_id` int(11) NOT NULL,
  `package_type` tinyint(1) NOT NULL,
  `discount_percentage_for_product` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`package_id`),
  KEY `product_gift_id` (`product_gift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `package_products`
--

CREATE TABLE IF NOT EXISTS `package_products` (
  `bpp_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  PRIMARY KEY (`bpp_id`),
  KEY `package_id` (`package_id`),
  KEY `pro_id` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE IF NOT EXISTS `payment_methods` (
  `payment_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_name` varchar(300) NOT NULL,
  `payment_method_name_en` varchar(300) NOT NULL,
  `is_payment_method_atm` tinyint(1) NOT NULL,
  `atm_ratio` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`payment_method_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`payment_method_id`, `payment_method_name`, `payment_method_name_en`, `is_payment_method_atm`, `atm_ratio`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'نقدي', 'نقدي', 0, '0.00', '2017-04-27 08:12:22', '2017-04-27 08:12:22', NULL),
(2, 'شبكة', 'شبكة', 1, '0.08', '2017-04-27 08:13:59', '2017-05-25 17:21:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `per_page_id` int(11) NOT NULL,
  `show_action` tinyint(1) NOT NULL,
  `add_action` tinyint(1) NOT NULL,
  `edit_action` tinyint(1) NOT NULL,
  `delete_action` tinyint(1) NOT NULL,
  `additional_permissions` text NOT NULL,
  PRIMARY KEY (`per_id`),
  KEY `user_id` (`user_id`),
  KEY `per_page_id` (`per_page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=865 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`per_id`, `user_id`, `per_page_id`, `show_action`, `add_action`, `edit_action`, `delete_action`, `additional_permissions`) VALUES
(1, 2, 1, 1, 1, 1, 1, '["manage_permissions"]'),
(2, 2, 2, 1, 1, 1, 1, 'null'),
(4, 2, 4, 1, 1, 1, 1, 'null'),
(5, 2, 5, 1, 1, 1, 1, 'null'),
(6, 3, 6, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill"]'),
(7, 2, 7, 1, 1, 1, 1, '["add_purchase_bill","add_return_bill"]'),
(8, 3, 8, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill","show_client_bills_statistics","check_branch_target","allow_cashier_atm","allow_cashier_to_remain_money","return_bill_allow_use_atm","return_bill_allow_remain_money"]'),
(9, 3, 9, 1, 1, 1, 1, '["manage_permissions","show_client_profile","finish_money_transfer","finish_money_transfer_with_atm","finish_money_transfer_partially"]'),
(10, 6, 6, 0, 1, 0, 0, '["add_return_bill"]'),
(11, 6, 8, 1, 1, 1, 0, '["show_bill_orders"]'),
(12, 6, 9, 0, 0, 0, 0, '["manage_permissions"]'),
(16, 7, 6, 1, 1, 0, 0, '["change_bill_status","show_bill_orders"]'),
(17, 7, 8, 1, 1, 1, 1, '["change_bill_status","show_bill_orders"]'),
(18, 7, 9, 0, 0, 0, 0, 'null'),
(19, 3, 10, 1, 1, 1, 1, 'null'),
(20, 3, 11, 1, 1, 1, 1, 'null'),
(21, 1, 1, 0, 0, 0, 0, 'null'),
(22, 1, 2, 0, 0, 0, 0, 'null'),
(23, 1, 3, 0, 0, 0, 0, 'null'),
(24, 1, 4, 0, 0, 0, 0, 'null'),
(25, 1, 5, 0, 0, 0, 0, 'null'),
(26, 1, 6, 0, 0, 0, 0, 'null'),
(27, 1, 7, 0, 0, 0, 0, 'null'),
(28, 1, 8, 0, 0, 0, 0, 'null'),
(29, 1, 9, 0, 0, 0, 0, 'null'),
(30, 1, 10, 0, 0, 0, 0, 'null'),
(31, 1, 11, 0, 0, 0, 0, 'null'),
(32, 1, 12, 0, 0, 0, 0, 'null'),
(33, 1, 13, 0, 0, 0, 0, 'null'),
(34, 1, 14, 0, 0, 0, 0, 'null'),
(35, 2, 3, 1, 1, 1, 1, 'null'),
(36, 2, 6, 1, 1, 1, 1, 'null'),
(37, 2, 8, 1, 1, 1, 1, 'null'),
(38, 2, 9, 1, 1, 1, 1, 'null'),
(39, 2, 10, 1, 1, 1, 1, 'null'),
(40, 2, 11, 1, 1, 1, 1, 'null'),
(41, 2, 12, 1, 1, 1, 1, 'null'),
(42, 2, 13, 1, 1, 1, 1, '["change_expense_status"]'),
(43, 2, 14, 1, 1, 1, 1, 'null'),
(44, 1, 15, 0, 0, 0, 0, 'null'),
(45, 2, 15, 1, 1, 1, 1, '["manage_bill_orders","manage_bill_return_orders"]'),
(46, 1, 16, 0, 0, 0, 0, 'null'),
(47, 2, 16, 1, 1, 1, 1, 'null'),
(48, 2, 17, 1, 1, 1, 1, 'null'),
(49, 1, 17, 0, 0, 0, 0, 'null'),
(50, 2, 18, 1, 1, 1, 1, 'null'),
(51, 1, 18, 0, 0, 0, 0, 'null'),
(52, 2, 19, 1, 1, 1, 1, 'null'),
(53, 1, 19, 0, 0, 0, 0, 'null'),
(54, 2, 20, 1, 1, 1, 1, 'null'),
(55, 1, 20, 0, 0, 0, 0, 'null'),
(56, 2, 21, 1, 1, 1, 1, 'null'),
(57, 1, 21, 0, 0, 0, 0, 'null'),
(58, 2, 22, 1, 1, 1, 1, 'null'),
(59, 1, 22, 0, 0, 0, 0, 'null'),
(60, 2, 23, 1, 1, 1, 1, 'null'),
(61, 1, 23, 0, 0, 0, 0, 'null'),
(62, 2, 24, 1, 1, 1, 1, 'null'),
(63, 1, 24, 0, 0, 0, 0, 'null'),
(64, 6, 10, 0, 0, 0, 0, 'null'),
(65, 6, 11, 0, 1, 1, 0, 'null'),
(66, 2, 25, 1, 1, 1, 1, 'null'),
(67, 2, 26, 1, 1, 1, 1, 'null'),
(68, 2, 27, 1, 1, 1, 1, 'null'),
(69, 3, 1, 1, 1, 1, 1, 'null'),
(70, 3, 2, 1, 1, 1, 1, 'null'),
(71, 3, 3, 1, 1, 1, 1, 'null'),
(72, 3, 4, 1, 1, 1, 1, 'null'),
(73, 3, 5, 1, 1, 1, 1, 'null'),
(74, 3, 7, 1, 1, 1, 1, 'null'),
(75, 3, 12, 1, 1, 1, 1, 'null'),
(76, 3, 13, 1, 1, 1, 1, 'null'),
(77, 3, 14, 1, 1, 1, 1, 'null'),
(78, 3, 15, 1, 1, 1, 1, 'null'),
(79, 3, 16, 1, 1, 1, 1, 'null'),
(80, 3, 17, 1, 1, 1, 1, 'null'),
(81, 3, 18, 1, 1, 1, 1, 'null'),
(82, 3, 19, 1, 1, 1, 1, 'null'),
(83, 3, 20, 1, 1, 1, 1, 'null'),
(84, 3, 21, 1, 1, 1, 1, 'null'),
(85, 3, 22, 1, 1, 1, 1, 'null'),
(86, 3, 23, 1, 1, 1, 1, 'null'),
(87, 3, 24, 1, 1, 1, 1, 'null'),
(88, 3, 25, 1, 1, 1, 1, 'null'),
(89, 3, 26, 1, 1, 1, 1, 'null'),
(90, 3, 27, 1, 1, 1, 1, 'null'),
(91, 3, 28, 1, 1, 1, 1, 'null'),
(92, 3, 29, 1, 1, 1, 1, 'null'),
(93, 3, 30, 1, 1, 1, 1, 'null'),
(94, 3, 31, 1, 1, 1, 1, 'null'),
(95, 3, 32, 1, 1, 1, 1, 'null'),
(96, 3, 33, 1, 1, 1, 1, 'null'),
(97, 3, 34, 1, 1, 1, 1, 'null'),
(98, 3, 35, 1, 1, 1, 1, 'null'),
(99, 3, 36, 1, 1, 1, 1, 'null'),
(100, 3, 37, 1, 1, 1, 1, 'null'),
(101, 2, 28, 1, 1, 1, 1, 'null'),
(102, 2, 29, 1, 1, 1, 1, 'null'),
(103, 2, 30, 1, 1, 1, 1, 'null'),
(104, 2, 31, 1, 1, 1, 1, '["print_barcode"]'),
(105, 2, 32, 1, 1, 1, 1, 'null'),
(106, 2, 33, 1, 1, 1, 1, '["save_items_position"]'),
(107, 2, 34, 1, 1, 1, 1, 'null'),
(108, 2, 35, 1, 1, 1, 1, '["show_branch_target"]'),
(109, 2, 36, 1, 1, 1, 1, 'null'),
(110, 2, 37, 1, 1, 1, 1, '["save_items_position"]'),
(111, 2, 38, 1, 1, 1, 1, '["receive_commission"]'),
(112, 2, 39, 1, 1, 1, 1, 'null'),
(113, 2, 40, 1, 1, 1, 1, 'null'),
(114, 2, 41, 1, 1, 1, 1, 'null'),
(115, 2, 42, 1, 1, 1, 1, 'null'),
(116, 2, 43, 1, 1, 1, 1, 'null'),
(117, 2, 44, 1, 1, 1, 1, 'null'),
(118, 2, 45, 1, 1, 1, 1, 'null'),
(119, 4, 1, 0, 0, 0, 0, ''),
(120, 4, 2, 0, 0, 0, 0, ''),
(121, 4, 3, 0, 0, 0, 0, ''),
(122, 4, 4, 0, 0, 0, 0, ''),
(123, 4, 5, 0, 0, 0, 0, ''),
(124, 4, 6, 0, 0, 0, 0, ''),
(125, 4, 7, 0, 0, 0, 0, ''),
(126, 4, 8, 0, 0, 0, 0, ''),
(127, 4, 9, 0, 0, 0, 0, ''),
(128, 4, 10, 0, 0, 0, 0, ''),
(129, 4, 11, 0, 0, 0, 0, ''),
(130, 4, 12, 0, 0, 0, 0, ''),
(131, 4, 13, 0, 0, 0, 0, ''),
(132, 4, 14, 0, 0, 0, 0, ''),
(133, 4, 15, 0, 0, 0, 0, ''),
(134, 4, 16, 0, 0, 0, 0, ''),
(135, 4, 17, 0, 0, 0, 0, ''),
(136, 4, 18, 0, 0, 0, 0, ''),
(137, 4, 19, 0, 0, 0, 0, ''),
(138, 4, 20, 0, 0, 0, 0, ''),
(139, 4, 21, 0, 0, 0, 0, ''),
(140, 4, 22, 0, 0, 0, 0, ''),
(141, 4, 23, 0, 0, 0, 0, ''),
(142, 4, 24, 0, 0, 0, 0, ''),
(143, 4, 25, 0, 0, 0, 0, ''),
(144, 4, 26, 0, 0, 0, 0, ''),
(145, 4, 27, 0, 0, 0, 0, ''),
(146, 4, 28, 0, 0, 0, 0, ''),
(147, 4, 29, 0, 0, 0, 0, ''),
(148, 4, 30, 0, 0, 0, 0, ''),
(149, 4, 31, 0, 0, 0, 0, ''),
(150, 4, 32, 0, 0, 0, 0, ''),
(151, 4, 33, 0, 0, 0, 0, ''),
(152, 4, 34, 0, 0, 0, 0, ''),
(153, 4, 35, 0, 0, 0, 0, ''),
(154, 4, 36, 0, 0, 0, 0, ''),
(155, 4, 37, 0, 0, 0, 0, ''),
(156, 4, 38, 0, 0, 0, 0, ''),
(157, 4, 39, 0, 0, 0, 0, ''),
(158, 4, 40, 0, 0, 0, 0, ''),
(159, 4, 41, 0, 0, 0, 0, ''),
(160, 4, 42, 0, 0, 0, 0, ''),
(161, 4, 43, 0, 0, 0, 0, ''),
(162, 4, 44, 0, 0, 0, 0, ''),
(163, 4, 45, 0, 0, 0, 0, ''),
(164, 3, 38, 1, 1, 1, 1, 'null'),
(165, 3, 39, 1, 1, 1, 1, 'null'),
(166, 3, 40, 1, 1, 1, 1, 'null'),
(167, 3, 41, 1, 1, 1, 1, 'null'),
(168, 3, 42, 1, 1, 1, 1, 'null'),
(169, 3, 43, 1, 1, 1, 1, 'null'),
(170, 3, 44, 1, 1, 1, 1, 'null'),
(171, 3, 45, 1, 1, 1, 1, 'null'),
(172, 16, 46, 1, 1, 1, 1, 'null'),
(173, 16, 47, 1, 1, 1, 1, 'null'),
(174, 16, 48, 1, 1, 1, 1, 'null'),
(175, 16, 49, 1, 1, 1, 1, 'null'),
(176, 16, 50, 1, 1, 1, 1, 'null'),
(177, 16, 51, 1, 1, 1, 1, '["pay_get_installment"]'),
(178, 16, 52, 1, 1, 1, 1, 'null'),
(179, 16, 53, 1, 1, 1, 1, 'null'),
(180, 16, 54, 1, 1, 1, 1, '["is_maintenance_finish"]'),
(181, 16, 55, 1, 1, 1, 1, 'null'),
(182, 16, 56, 1, 1, 1, 1, 'null'),
(183, 16, 57, 1, 1, 1, 1, 'null'),
(184, 16, 58, 1, 1, 1, 1, '["count_deprecation"]'),
(185, 16, 59, 1, 1, 1, 1, '["move_account_to_another_category"]'),
(186, 16, 60, 1, 1, 1, 1, '["close_financial_year"]'),
(187, 16, 61, 1, 1, 1, 1, '["entry_approve"]'),
(188, 16, 62, 1, 1, 1, 1, 'null'),
(189, 16, 63, 1, 1, 1, 1, 'null'),
(190, 3, 64, 1, 1, 1, 1, 'null'),
(191, 2, 64, 1, 1, 1, 1, '["move_category_to_another_parent"]'),
(192, 2, 65, 1, 1, 1, 1, 'null'),
(193, 16, 66, 1, 1, 1, 1, '["change_expense_status"]'),
(194, 16, 67, 1, 1, 1, 1, '["move_category_to_another_parent"]'),
(195, 16, 68, 1, 1, 1, 1, '["move_category_to_another_parent"]'),
(196, 2, 69, 1, 1, 1, 1, '["change_duty_document_received"]'),
(197, 2, 70, 1, 1, 1, 1, 'null'),
(198, 2, 71, 1, 1, 1, 1, 'null'),
(199, 2, 75, 1, 1, 1, 1, '["sell_bills","show_bill_orders","sell_bills_depend_on_customer","sell_bills_depend_on_user_age","sell_bills_depend_on_cashier","sell_bills_depend_on_branch","sell_bills_depend_on_user_gender","sell_bills_depend_on_product"]'),
(200, 3, 72, 1, 1, 1, 1, '["change_duty_document_received"]'),
(201, 3, 73, 1, 1, 1, 1, 'null'),
(202, 3, 74, 1, 1, 1, 1, 'null'),
(203, 3, 76, 1, 1, 1, 1, '["sell_bills","show_bill_orders","sell_bills_depend_on_customer","sell_bills_depend_on_user_age","sell_bills_depend_on_cashier","sell_bills_depend_on_branch","sell_bills_depend_on_user_gender","sell_bills_depend_on_product"]'),
(204, 20, 1, 0, 0, 0, 0, 'null'),
(205, 20, 2, 0, 0, 0, 0, 'null'),
(206, 20, 3, 0, 0, 0, 0, 'null'),
(207, 20, 4, 0, 0, 0, 0, 'null'),
(208, 20, 5, 0, 0, 0, 0, 'null'),
(209, 20, 7, 0, 0, 0, 0, 'null'),
(210, 20, 12, 0, 0, 0, 0, 'null'),
(211, 20, 13, 0, 0, 0, 0, 'null'),
(212, 20, 14, 0, 0, 0, 0, 'null'),
(213, 20, 15, 0, 0, 0, 0, 'null'),
(214, 20, 16, 0, 0, 0, 0, 'null'),
(215, 20, 17, 0, 0, 0, 0, 'null'),
(216, 20, 18, 0, 0, 0, 0, 'null'),
(217, 20, 19, 0, 0, 0, 0, 'null'),
(218, 20, 20, 0, 0, 0, 0, 'null'),
(219, 20, 21, 0, 0, 0, 0, 'null'),
(220, 20, 22, 0, 0, 0, 0, 'null'),
(221, 20, 23, 0, 0, 0, 0, 'null'),
(222, 20, 24, 0, 0, 0, 0, 'null'),
(223, 20, 25, 0, 0, 0, 0, 'null'),
(224, 20, 26, 0, 0, 0, 0, 'null'),
(225, 20, 28, 0, 0, 0, 0, 'null'),
(226, 20, 29, 0, 0, 0, 0, 'null'),
(227, 20, 30, 0, 0, 0, 0, 'null'),
(228, 20, 31, 0, 0, 0, 0, 'null'),
(229, 20, 32, 1, 1, 1, 1, 'null'),
(230, 20, 33, 0, 0, 0, 0, 'null'),
(231, 20, 34, 0, 0, 0, 0, 'null'),
(232, 20, 35, 0, 0, 0, 0, 'null'),
(233, 20, 36, 0, 0, 0, 0, 'null'),
(234, 20, 37, 0, 0, 0, 0, 'null'),
(235, 20, 38, 0, 0, 0, 0, 'null'),
(236, 20, 39, 1, 1, 1, 1, 'null'),
(237, 20, 40, 1, 1, 1, 1, 'null'),
(238, 20, 41, 1, 1, 1, 1, 'null'),
(239, 20, 64, 1, 1, 1, 1, 'null'),
(240, 20, 65, 1, 1, 1, 1, 'null'),
(241, 20, 69, 1, 1, 1, 1, 'null'),
(242, 20, 70, 1, 0, 0, 0, 'null'),
(243, 20, 71, 1, 0, 0, 0, 'null'),
(244, 20, 75, 0, 0, 0, 0, 'null'),
(245, 20, 6, 0, 0, 0, 0, 'null'),
(246, 20, 8, 0, 1, 0, 0, '["change_bill_status","show_bill_orders","add_return_bill","show_client_bills_statistics","check_branch_target","allow_cashier_atm","allow_cashier_to_remain_money","return_bill_allow_use_atm","return_bill_allow_remain_money"]'),
(247, 20, 9, 0, 0, 0, 0, 'null'),
(248, 20, 10, 1, 1, 1, 1, 'null'),
(249, 20, 11, 0, 0, 0, 0, 'null'),
(250, 20, 27, 1, 1, 1, 0, 'null'),
(251, 20, 42, 0, 0, 0, 1, 'null'),
(252, 20, 43, 1, 1, 1, 1, 'null'),
(253, 20, 44, 0, 0, 0, 0, 'null'),
(254, 20, 45, 0, 0, 0, 0, 'null'),
(255, 20, 72, 1, 1, 1, 1, '["change_duty_document_received"]'),
(256, 20, 73, 1, 0, 0, 0, 'null'),
(257, 20, 74, 0, 0, 0, 0, 'null'),
(258, 20, 76, 0, 0, 0, 0, 'null'),
(259, 10, 1, 0, 0, 0, 0, 'null'),
(260, 10, 2, 0, 0, 0, 0, 'null'),
(261, 10, 3, 0, 0, 0, 0, 'null'),
(262, 10, 4, 0, 0, 0, 0, 'null'),
(263, 10, 5, 0, 0, 0, 0, 'null'),
(264, 10, 7, 0, 0, 0, 0, 'null'),
(265, 10, 12, 0, 0, 0, 0, 'null'),
(266, 10, 13, 0, 0, 0, 0, 'null'),
(267, 10, 14, 0, 0, 0, 0, 'null'),
(268, 10, 15, 0, 0, 0, 0, 'null'),
(269, 10, 16, 0, 0, 0, 0, 'null'),
(270, 10, 17, 0, 0, 0, 0, 'null'),
(271, 10, 18, 0, 0, 0, 0, 'null'),
(272, 10, 19, 0, 0, 0, 0, 'null'),
(273, 10, 20, 0, 0, 0, 0, 'null'),
(274, 10, 21, 0, 0, 0, 0, 'null'),
(275, 10, 22, 0, 0, 0, 0, 'null'),
(276, 10, 23, 0, 0, 0, 0, 'null'),
(277, 10, 24, 0, 0, 0, 0, 'null'),
(278, 10, 25, 0, 0, 0, 0, 'null'),
(279, 10, 26, 0, 0, 0, 0, 'null'),
(280, 10, 28, 0, 0, 0, 0, 'null'),
(281, 10, 29, 0, 0, 0, 0, 'null'),
(282, 10, 30, 0, 0, 0, 0, 'null'),
(283, 10, 31, 0, 0, 0, 0, 'null'),
(284, 10, 32, 0, 0, 0, 0, 'null'),
(285, 10, 33, 0, 0, 0, 0, 'null'),
(286, 10, 34, 0, 0, 0, 0, 'null'),
(287, 10, 35, 0, 0, 0, 0, 'null'),
(288, 10, 36, 0, 0, 0, 0, 'null'),
(289, 10, 37, 1, 1, 1, 1, '["save_items_position"]'),
(290, 10, 38, 0, 0, 0, 0, 'null'),
(291, 10, 39, 0, 0, 0, 0, 'null'),
(292, 10, 40, 0, 0, 0, 0, 'null'),
(293, 10, 41, 0, 0, 0, 0, 'null'),
(294, 10, 64, 0, 0, 0, 0, 'null'),
(295, 10, 65, 0, 0, 0, 0, 'null'),
(296, 10, 69, 0, 0, 0, 0, 'null'),
(297, 10, 70, 0, 0, 0, 0, 'null'),
(298, 10, 71, 0, 0, 0, 0, 'null'),
(299, 10, 75, 0, 0, 0, 0, 'null'),
(300, 2, 77, 1, 1, 1, 1, '[]'),
(301, 1, 25, 0, 0, 0, 0, ''),
(302, 1, 26, 0, 0, 0, 0, ''),
(303, 1, 28, 0, 0, 0, 0, ''),
(304, 1, 29, 0, 0, 0, 0, ''),
(305, 1, 30, 0, 0, 0, 0, ''),
(306, 1, 31, 0, 0, 0, 0, ''),
(307, 1, 32, 0, 0, 0, 0, ''),
(308, 1, 33, 0, 0, 0, 0, ''),
(309, 1, 34, 0, 0, 0, 0, ''),
(310, 1, 35, 0, 0, 0, 0, ''),
(311, 1, 36, 0, 0, 0, 0, ''),
(312, 1, 37, 0, 0, 0, 0, ''),
(313, 1, 38, 0, 0, 0, 0, ''),
(314, 1, 39, 0, 0, 0, 0, ''),
(315, 1, 40, 0, 0, 0, 0, ''),
(316, 1, 41, 0, 0, 0, 0, ''),
(317, 1, 64, 0, 0, 0, 0, ''),
(318, 1, 65, 0, 0, 0, 0, ''),
(319, 1, 69, 0, 0, 0, 0, ''),
(320, 1, 70, 0, 0, 0, 0, ''),
(321, 1, 71, 0, 0, 0, 0, ''),
(322, 1, 75, 0, 0, 0, 0, ''),
(323, 1, 77, 0, 0, 0, 0, ''),
(324, 6, 1, 0, 0, 0, 0, 'null'),
(325, 6, 2, 0, 0, 0, 0, 'null'),
(326, 6, 3, 0, 0, 0, 0, 'null'),
(327, 6, 4, 0, 0, 0, 0, 'null'),
(328, 6, 5, 0, 0, 0, 0, 'null'),
(329, 6, 7, 0, 0, 0, 0, 'null'),
(330, 6, 12, 0, 0, 0, 0, 'null'),
(331, 6, 13, 0, 0, 0, 0, 'null'),
(332, 6, 14, 0, 0, 0, 0, 'null'),
(333, 6, 15, 0, 0, 0, 0, 'null'),
(334, 6, 16, 0, 0, 0, 0, 'null'),
(335, 6, 17, 0, 0, 0, 0, 'null'),
(336, 6, 18, 0, 0, 0, 0, 'null'),
(337, 6, 19, 0, 0, 0, 0, 'null'),
(338, 6, 20, 0, 0, 0, 0, 'null'),
(339, 6, 21, 0, 0, 0, 0, 'null'),
(340, 6, 22, 0, 0, 0, 0, 'null'),
(341, 6, 23, 0, 0, 0, 0, 'null'),
(342, 6, 24, 0, 0, 0, 0, 'null'),
(343, 6, 25, 0, 0, 0, 0, 'null'),
(344, 6, 26, 0, 0, 0, 0, 'null'),
(345, 6, 28, 0, 0, 0, 0, 'null'),
(346, 6, 29, 0, 0, 0, 0, 'null'),
(347, 6, 30, 0, 0, 0, 0, 'null'),
(348, 6, 31, 0, 0, 0, 0, 'null'),
(349, 6, 32, 0, 0, 0, 0, 'null'),
(350, 6, 33, 0, 0, 0, 0, 'null'),
(351, 6, 34, 0, 0, 0, 0, 'null'),
(352, 6, 35, 0, 0, 0, 0, 'null'),
(353, 6, 36, 0, 0, 0, 0, 'null'),
(354, 6, 37, 0, 0, 0, 0, 'null'),
(355, 6, 38, 0, 0, 0, 0, 'null'),
(356, 6, 39, 0, 0, 0, 0, 'null'),
(357, 6, 40, 0, 0, 0, 0, 'null'),
(358, 6, 41, 0, 0, 0, 0, 'null'),
(359, 6, 64, 0, 0, 0, 0, 'null'),
(360, 6, 65, 0, 0, 0, 0, 'null'),
(361, 6, 69, 0, 0, 0, 0, 'null'),
(362, 6, 70, 0, 0, 0, 0, 'null'),
(363, 6, 71, 0, 0, 0, 0, 'null'),
(364, 6, 75, 0, 0, 0, 0, 'null'),
(365, 6, 77, 0, 0, 0, 0, 'null'),
(366, 6, 27, 0, 0, 0, 0, 'null'),
(367, 6, 42, 0, 0, 0, 0, 'null'),
(368, 6, 43, 0, 0, 0, 0, 'null'),
(369, 6, 44, 0, 0, 0, 0, 'null'),
(370, 6, 45, 0, 0, 0, 0, 'null'),
(371, 6, 72, 0, 0, 0, 0, 'null'),
(372, 6, 73, 0, 0, 0, 0, 'null'),
(373, 6, 74, 0, 0, 0, 0, 'null'),
(374, 6, 76, 0, 0, 0, 0, 'null'),
(375, 2, 72, 0, 0, 0, 0, ''),
(376, 2, 73, 0, 0, 0, 0, ''),
(377, 2, 74, 0, 0, 0, 0, ''),
(378, 2, 76, 0, 0, 0, 0, ''),
(379, 2, 78, 1, 1, 1, 1, ''),
(380, 3, 79, 1, 1, 1, 1, '["receive_transfers"]'),
(381, 30, 1, 1, 0, 0, 0, ''),
(382, 30, 2, 1, 0, 0, 0, ''),
(383, 30, 3, 0, 0, 0, 0, ''),
(384, 30, 4, 0, 0, 0, 0, ''),
(385, 30, 5, 0, 0, 0, 0, ''),
(386, 30, 6, 0, 0, 0, 0, ''),
(387, 30, 7, 0, 0, 0, 0, ''),
(388, 30, 8, 0, 0, 0, 0, ''),
(389, 30, 9, 0, 0, 0, 0, ''),
(390, 30, 10, 0, 0, 0, 0, ''),
(391, 30, 11, 0, 0, 0, 0, ''),
(392, 30, 12, 0, 0, 0, 0, ''),
(393, 30, 13, 0, 0, 0, 0, ''),
(394, 30, 14, 0, 0, 0, 0, ''),
(395, 30, 15, 0, 0, 0, 0, ''),
(396, 30, 16, 0, 0, 0, 0, ''),
(397, 30, 17, 0, 0, 0, 0, ''),
(398, 30, 18, 0, 0, 0, 0, ''),
(399, 30, 19, 0, 0, 0, 0, ''),
(400, 30, 20, 0, 0, 0, 0, ''),
(401, 30, 21, 0, 0, 0, 0, ''),
(402, 30, 22, 0, 0, 0, 0, ''),
(403, 30, 23, 0, 0, 0, 0, ''),
(404, 30, 24, 0, 0, 0, 0, ''),
(405, 30, 25, 0, 0, 0, 0, ''),
(406, 30, 26, 0, 0, 0, 0, ''),
(407, 30, 27, 0, 0, 0, 0, ''),
(408, 30, 28, 0, 0, 0, 0, ''),
(409, 30, 29, 0, 0, 0, 0, ''),
(410, 30, 30, 0, 0, 0, 0, ''),
(411, 30, 31, 0, 0, 0, 0, ''),
(412, 30, 32, 0, 0, 0, 0, ''),
(413, 30, 33, 0, 0, 0, 0, ''),
(414, 30, 34, 0, 0, 0, 0, ''),
(415, 30, 35, 0, 0, 0, 0, ''),
(416, 30, 36, 0, 0, 0, 0, ''),
(417, 30, 37, 0, 0, 0, 0, ''),
(418, 30, 38, 0, 0, 0, 0, ''),
(419, 30, 39, 0, 0, 0, 0, ''),
(420, 30, 40, 0, 0, 0, 0, ''),
(421, 30, 41, 0, 0, 0, 0, ''),
(422, 30, 42, 0, 0, 0, 0, ''),
(423, 30, 43, 0, 0, 0, 0, ''),
(424, 30, 44, 0, 0, 0, 0, ''),
(425, 30, 45, 0, 0, 0, 0, ''),
(426, 30, 64, 0, 0, 0, 0, ''),
(427, 30, 65, 0, 0, 0, 0, ''),
(428, 30, 69, 0, 0, 0, 0, ''),
(429, 30, 70, 0, 0, 0, 0, ''),
(430, 30, 71, 0, 0, 0, 0, ''),
(431, 30, 72, 0, 0, 0, 0, ''),
(432, 30, 73, 0, 0, 0, 0, ''),
(433, 30, 74, 0, 0, 0, 0, ''),
(434, 30, 75, 0, 0, 0, 0, ''),
(435, 30, 76, 0, 0, 0, 0, ''),
(436, 30, 77, 0, 0, 0, 0, ''),
(437, 30, 78, 0, 0, 0, 0, ''),
(438, 30, 79, 0, 0, 0, 0, ''),
(439, 32, 1, 0, 0, 0, 0, 'null'),
(440, 32, 2, 0, 0, 0, 0, 'null'),
(441, 32, 3, 0, 0, 0, 0, 'null'),
(442, 32, 4, 1, 1, 0, 0, 'null'),
(443, 32, 5, 1, 1, 0, 0, 'null'),
(444, 32, 6, 1, 1, 0, 0, 'null'),
(445, 32, 7, 0, 0, 0, 0, 'null'),
(446, 32, 8, 0, 0, 0, 0, 'null'),
(447, 32, 9, 0, 0, 0, 0, 'null'),
(448, 32, 10, 0, 0, 0, 0, 'null'),
(449, 32, 11, 0, 0, 0, 0, 'null'),
(450, 32, 12, 0, 0, 0, 0, 'null'),
(451, 32, 13, 0, 0, 0, 0, 'null'),
(452, 32, 14, 0, 0, 0, 0, 'null'),
(453, 32, 15, 0, 0, 0, 0, 'null'),
(454, 32, 16, 0, 0, 0, 0, 'null'),
(455, 32, 17, 0, 0, 0, 0, 'null'),
(456, 32, 18, 0, 0, 0, 0, 'null'),
(457, 32, 19, 0, 0, 0, 0, 'null'),
(458, 32, 20, 0, 0, 0, 0, 'null'),
(459, 32, 21, 0, 0, 0, 0, 'null'),
(460, 32, 22, 0, 0, 0, 0, 'null'),
(461, 32, 23, 0, 0, 0, 0, 'null'),
(462, 32, 24, 0, 0, 0, 0, 'null'),
(463, 32, 25, 0, 0, 0, 0, 'null'),
(464, 32, 26, 0, 0, 0, 0, 'null'),
(465, 32, 27, 0, 0, 0, 0, 'null'),
(466, 32, 28, 0, 0, 0, 0, 'null'),
(467, 32, 29, 0, 0, 0, 0, 'null'),
(468, 32, 30, 0, 0, 0, 0, 'null'),
(469, 32, 31, 0, 0, 0, 0, 'null'),
(470, 32, 32, 0, 0, 0, 0, 'null'),
(471, 32, 33, 0, 0, 0, 0, 'null'),
(472, 32, 34, 0, 0, 0, 0, 'null'),
(473, 32, 35, 0, 0, 0, 0, 'null'),
(474, 32, 36, 0, 0, 0, 0, 'null'),
(475, 32, 37, 0, 0, 0, 0, 'null'),
(476, 32, 38, 0, 0, 0, 0, 'null'),
(477, 32, 39, 0, 0, 0, 0, 'null'),
(478, 32, 40, 0, 0, 0, 0, 'null'),
(479, 32, 41, 0, 0, 0, 0, 'null'),
(480, 32, 42, 0, 0, 0, 0, 'null'),
(481, 32, 43, 0, 0, 0, 0, 'null'),
(482, 32, 44, 0, 0, 0, 0, 'null'),
(483, 32, 45, 0, 0, 0, 0, 'null'),
(484, 32, 64, 0, 0, 0, 0, 'null'),
(485, 32, 65, 1, 1, 0, 0, 'null'),
(486, 32, 69, 0, 0, 0, 0, 'null'),
(487, 32, 70, 0, 0, 0, 0, 'null'),
(488, 32, 71, 0, 0, 0, 0, 'null'),
(489, 32, 72, 0, 0, 0, 0, 'null'),
(490, 32, 73, 0, 0, 0, 0, 'null'),
(491, 32, 74, 0, 0, 0, 0, 'null'),
(492, 32, 75, 0, 0, 0, 0, 'null'),
(493, 32, 76, 0, 0, 0, 0, 'null'),
(494, 32, 77, 0, 0, 0, 0, 'null'),
(495, 32, 78, 0, 0, 0, 0, 'null'),
(496, 32, 79, 0, 0, 0, 0, 'null'),
(497, 6, 78, 0, 0, 0, 0, 'null'),
(498, 6, 79, 0, 0, 0, 0, 'null'),
(499, 7, 1, 0, 0, 0, 0, 'null'),
(500, 7, 2, 0, 0, 0, 0, 'null'),
(501, 7, 3, 0, 0, 0, 0, 'null'),
(502, 7, 4, 0, 0, 0, 0, 'null'),
(503, 7, 5, 0, 0, 0, 0, 'null'),
(504, 7, 7, 0, 0, 0, 0, 'null'),
(505, 7, 10, 0, 0, 0, 0, 'null'),
(506, 7, 11, 0, 0, 0, 0, 'null'),
(507, 7, 12, 0, 0, 0, 0, 'null'),
(508, 7, 13, 0, 0, 0, 0, 'null'),
(509, 7, 14, 0, 0, 0, 0, 'null'),
(510, 7, 15, 0, 0, 0, 0, 'null'),
(511, 7, 16, 0, 0, 0, 0, 'null'),
(512, 7, 17, 0, 0, 0, 0, 'null'),
(513, 7, 18, 0, 0, 0, 0, 'null'),
(514, 7, 19, 0, 0, 0, 0, 'null'),
(515, 7, 20, 0, 0, 0, 0, 'null'),
(516, 7, 21, 0, 0, 0, 0, 'null'),
(517, 7, 22, 0, 0, 0, 0, 'null'),
(518, 7, 23, 0, 0, 0, 0, 'null'),
(519, 7, 24, 0, 0, 0, 0, 'null'),
(520, 7, 25, 0, 0, 0, 0, 'null'),
(521, 7, 26, 0, 0, 0, 0, 'null'),
(522, 7, 27, 0, 0, 0, 0, 'null'),
(523, 7, 28, 0, 0, 0, 0, 'null'),
(524, 7, 29, 0, 0, 0, 0, 'null'),
(525, 7, 30, 0, 0, 0, 0, 'null'),
(526, 7, 31, 0, 0, 0, 0, 'null'),
(527, 7, 32, 0, 0, 0, 0, 'null'),
(528, 7, 33, 0, 0, 0, 0, 'null'),
(529, 7, 34, 0, 0, 0, 0, 'null'),
(530, 7, 35, 0, 0, 0, 0, 'null'),
(531, 7, 36, 0, 0, 0, 0, 'null'),
(532, 7, 37, 0, 0, 0, 0, 'null'),
(533, 7, 38, 0, 0, 0, 0, 'null'),
(534, 7, 39, 0, 0, 0, 0, 'null'),
(535, 7, 40, 0, 0, 0, 0, 'null'),
(536, 7, 41, 0, 0, 0, 0, 'null'),
(537, 7, 42, 0, 0, 0, 0, 'null'),
(538, 7, 43, 0, 0, 0, 0, 'null'),
(539, 7, 44, 0, 0, 0, 0, 'null'),
(540, 7, 45, 0, 0, 0, 0, 'null'),
(541, 7, 64, 0, 0, 0, 0, 'null'),
(542, 7, 65, 0, 0, 0, 0, 'null'),
(543, 7, 69, 0, 0, 0, 0, 'null'),
(544, 7, 70, 0, 0, 0, 0, 'null'),
(545, 7, 71, 0, 0, 0, 0, 'null'),
(546, 7, 72, 0, 0, 0, 0, 'null'),
(547, 7, 73, 0, 0, 0, 0, 'null'),
(548, 7, 74, 0, 0, 0, 0, 'null'),
(549, 7, 75, 0, 0, 0, 0, 'null'),
(550, 7, 76, 0, 0, 0, 0, 'null'),
(551, 7, 77, 0, 0, 0, 0, 'null'),
(552, 7, 78, 0, 0, 0, 0, 'null'),
(553, 7, 79, 0, 0, 0, 0, 'null'),
(554, 33, 1, 0, 0, 0, 0, 'null'),
(555, 33, 2, 0, 0, 0, 0, 'null'),
(556, 33, 3, 0, 0, 0, 0, 'null'),
(557, 33, 4, 1, 1, 0, 0, 'null'),
(558, 33, 5, 1, 1, 0, 0, 'null'),
(559, 33, 6, 1, 1, 0, 0, 'null'),
(560, 33, 7, 0, 0, 0, 0, 'null'),
(561, 33, 8, 1, 1, 0, 0, 'null'),
(562, 33, 9, 0, 0, 0, 0, 'null'),
(563, 33, 10, 1, 1, 0, 0, 'null'),
(564, 33, 11, 0, 0, 0, 0, 'null'),
(565, 33, 12, 0, 0, 0, 0, 'null'),
(566, 33, 13, 0, 0, 0, 0, 'null'),
(567, 33, 14, 0, 0, 0, 0, 'null'),
(568, 33, 15, 1, 1, 0, 0, 'null'),
(569, 33, 16, 0, 0, 0, 0, 'null'),
(570, 33, 17, 0, 0, 0, 0, 'null'),
(571, 33, 18, 0, 0, 0, 0, 'null'),
(572, 33, 19, 0, 0, 0, 0, 'null'),
(573, 33, 20, 0, 0, 0, 0, 'null'),
(574, 33, 21, 0, 0, 0, 0, 'null'),
(575, 33, 22, 0, 0, 0, 0, 'null'),
(576, 33, 23, 0, 0, 0, 0, 'null'),
(577, 33, 24, 0, 0, 0, 0, 'null'),
(578, 33, 25, 0, 0, 0, 0, 'null'),
(579, 33, 26, 0, 0, 0, 0, 'null'),
(580, 33, 27, 0, 0, 0, 0, 'null'),
(581, 33, 28, 0, 0, 0, 0, 'null'),
(582, 33, 29, 0, 0, 0, 0, 'null'),
(583, 33, 30, 0, 0, 0, 0, 'null'),
(584, 33, 31, 1, 1, 0, 0, 'null'),
(585, 33, 32, 0, 0, 0, 0, 'null'),
(586, 33, 33, 0, 0, 0, 0, 'null'),
(587, 33, 34, 0, 0, 0, 0, 'null'),
(588, 33, 35, 0, 0, 0, 0, 'null'),
(589, 33, 36, 1, 1, 0, 0, 'null'),
(590, 33, 37, 0, 0, 0, 0, 'null'),
(591, 33, 38, 0, 0, 0, 0, 'null'),
(592, 33, 39, 0, 0, 0, 0, 'null'),
(593, 33, 40, 0, 0, 0, 0, 'null'),
(594, 33, 41, 0, 0, 0, 0, 'null'),
(595, 33, 42, 0, 0, 0, 0, 'null'),
(596, 33, 43, 0, 0, 0, 0, 'null'),
(597, 33, 44, 1, 1, 0, 0, 'null'),
(598, 33, 45, 0, 0, 0, 0, 'null'),
(599, 33, 64, 0, 0, 0, 0, 'null'),
(600, 33, 65, 0, 0, 0, 0, 'null'),
(601, 33, 69, 0, 0, 0, 0, 'null'),
(602, 33, 70, 0, 0, 0, 0, 'null'),
(603, 33, 71, 0, 0, 0, 0, 'null'),
(604, 33, 72, 0, 0, 0, 0, 'null'),
(605, 33, 73, 1, 1, 1, 1, 'null'),
(606, 33, 74, 0, 0, 0, 0, 'null'),
(607, 33, 75, 1, 1, 0, 0, 'null'),
(608, 33, 76, 1, 1, 0, 0, 'null'),
(609, 33, 77, 0, 0, 0, 0, 'null'),
(610, 33, 78, 0, 0, 0, 0, 'null'),
(611, 33, 79, 1, 1, 0, 0, 'null'),
(612, 33, 80, 0, 0, 0, 0, 'null'),
(613, 40, 1, 0, 0, 0, 0, 'null'),
(614, 40, 2, 0, 0, 0, 0, 'null'),
(615, 40, 3, 0, 0, 0, 0, 'null'),
(616, 40, 4, 1, 1, 0, 0, 'null'),
(617, 40, 5, 0, 0, 0, 0, 'null'),
(618, 40, 6, 1, 1, 0, 0, 'null'),
(619, 40, 7, 0, 0, 0, 0, 'null'),
(620, 40, 8, 1, 1, 1, 0, 'null'),
(621, 40, 9, 0, 0, 0, 0, 'null'),
(622, 40, 10, 0, 0, 0, 0, 'null'),
(623, 40, 11, 0, 0, 0, 0, 'null'),
(624, 40, 12, 0, 0, 0, 0, 'null'),
(625, 40, 13, 0, 0, 0, 0, 'null'),
(626, 40, 14, 0, 0, 0, 0, 'null'),
(627, 40, 15, 1, 1, 1, 0, 'null'),
(628, 40, 16, 0, 0, 0, 0, 'null'),
(629, 40, 17, 0, 0, 0, 0, 'null'),
(630, 40, 18, 0, 0, 0, 0, 'null'),
(631, 40, 19, 0, 0, 0, 0, 'null'),
(632, 40, 20, 0, 0, 0, 0, 'null'),
(633, 40, 21, 0, 0, 0, 0, 'null'),
(634, 40, 22, 0, 0, 0, 0, 'null'),
(635, 40, 23, 0, 0, 0, 0, 'null'),
(636, 40, 24, 0, 0, 0, 0, 'null'),
(637, 40, 25, 1, 0, 0, 0, 'null'),
(638, 40, 26, 0, 0, 0, 0, 'null'),
(639, 40, 27, 0, 0, 0, 0, 'null'),
(640, 40, 28, 0, 0, 0, 0, 'null'),
(641, 40, 29, 0, 0, 0, 0, 'null'),
(642, 40, 30, 1, 0, 0, 0, 'null'),
(643, 40, 31, 0, 0, 0, 0, 'null'),
(644, 40, 32, 0, 0, 0, 0, 'null'),
(645, 40, 33, 0, 0, 0, 0, 'null'),
(646, 40, 34, 0, 0, 0, 0, 'null'),
(647, 40, 35, 1, 1, 0, 0, 'null'),
(648, 40, 36, 0, 0, 0, 0, 'null'),
(649, 40, 37, 0, 0, 0, 0, 'null'),
(650, 40, 38, 0, 0, 0, 0, 'null'),
(651, 40, 39, 1, 0, 0, 0, 'null'),
(652, 40, 40, 0, 0, 0, 0, 'null'),
(653, 40, 41, 1, 1, 1, 0, 'null'),
(654, 40, 42, 0, 0, 0, 0, 'null'),
(655, 40, 43, 1, 0, 0, 0, 'null'),
(656, 40, 44, 1, 1, 1, 0, 'null'),
(657, 40, 45, 1, 0, 0, 0, 'null'),
(658, 40, 64, 0, 0, 0, 0, 'null'),
(659, 40, 65, 1, 0, 0, 0, 'null'),
(660, 40, 69, 0, 0, 0, 0, 'null'),
(661, 40, 70, 0, 0, 0, 0, 'null'),
(662, 40, 71, 0, 0, 0, 0, 'null'),
(663, 40, 72, 0, 0, 0, 0, 'null'),
(664, 40, 73, 1, 1, 1, 0, 'null'),
(665, 40, 74, 0, 0, 0, 0, 'null'),
(666, 40, 75, 0, 0, 0, 0, 'null'),
(667, 40, 76, 0, 0, 0, 0, 'null'),
(668, 40, 77, 0, 0, 0, 0, 'null'),
(669, 40, 78, 0, 0, 0, 0, 'null'),
(670, 40, 79, 0, 1, 0, 0, 'null'),
(671, 40, 80, 0, 0, 0, 0, 'null'),
(672, 2, 79, 0, 0, 0, 0, ''),
(673, 2, 80, 0, 0, 0, 0, ''),
(674, 40, 81, 1, 1, 1, 0, 'null'),
(675, 33, 81, 1, 1, 1, 1, 'null'),
(676, 56, 1, 0, 0, 0, 0, 'null'),
(677, 56, 2, 0, 0, 0, 0, 'null'),
(678, 56, 3, 0, 0, 0, 0, 'null'),
(679, 56, 4, 1, 1, 1, 1, 'null'),
(680, 56, 5, 1, 1, 1, 1, 'null'),
(681, 56, 6, 1, 1, 1, 1, 'null'),
(682, 56, 7, 0, 0, 0, 0, 'null'),
(683, 56, 8, 1, 1, 1, 1, '["change_bill_status","add_return_bill","allow_cashier_atm"]'),
(684, 56, 9, 1, 1, 0, 0, '["show_client_profile"]'),
(685, 56, 10, 1, 1, 1, 1, 'null'),
(686, 56, 11, 1, 0, 0, 0, 'null'),
(687, 56, 12, 0, 0, 0, 0, 'null'),
(688, 56, 13, 0, 0, 0, 0, 'null'),
(689, 56, 14, 0, 0, 0, 0, 'null'),
(690, 56, 15, 1, 1, 1, 1, 'null'),
(691, 56, 16, 0, 0, 0, 0, 'null'),
(692, 56, 17, 0, 0, 0, 0, 'null'),
(693, 56, 18, 1, 0, 0, 0, 'null'),
(694, 56, 19, 0, 0, 0, 0, 'null'),
(695, 56, 20, 0, 0, 0, 0, 'null'),
(696, 56, 21, 0, 0, 0, 0, 'null'),
(697, 56, 22, 0, 0, 0, 0, 'null'),
(698, 56, 23, 0, 0, 0, 0, 'null'),
(699, 56, 24, 0, 0, 0, 0, 'null'),
(700, 56, 25, 1, 0, 0, 0, 'null'),
(701, 56, 26, 0, 0, 0, 0, 'null'),
(702, 56, 27, 1, 0, 0, 0, 'null'),
(703, 56, 28, 0, 0, 0, 0, 'null'),
(704, 56, 29, 0, 0, 0, 0, 'null'),
(705, 56, 30, 0, 0, 0, 0, 'null'),
(706, 56, 31, 1, 0, 0, 0, 'null'),
(707, 56, 32, 0, 0, 0, 0, 'null'),
(708, 56, 33, 0, 0, 0, 0, 'null'),
(709, 56, 34, 0, 0, 0, 0, 'null'),
(710, 56, 35, 1, 1, 1, 1, '["show_branch_target"]'),
(711, 56, 36, 1, 1, 1, 1, 'null'),
(712, 56, 37, 0, 0, 0, 0, 'null'),
(713, 56, 38, 1, 0, 0, 0, '["receive_commission"]'),
(714, 56, 39, 1, 0, 0, 0, 'null'),
(715, 56, 40, 0, 0, 0, 0, 'null'),
(716, 56, 41, 1, 1, 1, 1, 'null'),
(717, 56, 42, 0, 0, 0, 0, 'null'),
(718, 56, 43, 1, 0, 0, 0, 'null'),
(719, 56, 44, 1, 1, 1, 1, 'null'),
(720, 56, 45, 1, 0, 0, 0, 'null'),
(721, 56, 64, 0, 0, 0, 0, 'null'),
(722, 56, 65, 0, 0, 0, 0, 'null'),
(723, 56, 69, 1, 0, 0, 0, 'null'),
(724, 56, 70, 1, 0, 0, 0, 'null'),
(725, 56, 71, 0, 0, 0, 0, 'null'),
(726, 56, 72, 0, 0, 0, 0, 'null'),
(727, 56, 73, 1, 1, 1, 1, 'null'),
(728, 56, 74, 1, 0, 0, 0, 'null'),
(729, 56, 75, 0, 0, 0, 0, '["sell_bills_depend_on_branch"]'),
(730, 56, 76, 0, 0, 0, 0, 'null'),
(731, 56, 77, 0, 0, 0, 0, 'null'),
(732, 56, 78, 0, 0, 0, 0, 'null'),
(733, 56, 79, 1, 1, 1, 1, '["receive_transfers"]'),
(734, 56, 80, 0, 0, 0, 0, 'null'),
(735, 56, 81, 0, 0, 0, 0, 'null'),
(736, 56, 82, 1, 1, 0, 0, 'null'),
(737, 6380, 1, 0, 0, 0, 0, 'null'),
(738, 6380, 2, 0, 0, 0, 0, 'null'),
(739, 6380, 3, 0, 0, 0, 0, 'null'),
(740, 6380, 4, 0, 0, 0, 0, 'null'),
(741, 6380, 5, 0, 0, 0, 0, 'null'),
(742, 6380, 6, 0, 0, 0, 0, 'null'),
(743, 6380, 7, 0, 0, 0, 0, 'null'),
(744, 6380, 8, 0, 0, 0, 0, 'null'),
(745, 6380, 9, 0, 0, 0, 0, 'null'),
(746, 6380, 10, 0, 0, 0, 0, 'null'),
(747, 6380, 11, 0, 0, 0, 0, 'null'),
(748, 6380, 12, 0, 0, 0, 0, 'null'),
(749, 6380, 13, 0, 0, 0, 0, 'null'),
(750, 6380, 14, 0, 0, 0, 0, 'null'),
(751, 6380, 15, 0, 0, 0, 0, 'null'),
(752, 6380, 16, 0, 0, 0, 0, 'null'),
(753, 6380, 17, 0, 0, 0, 0, 'null'),
(754, 6380, 18, 0, 0, 0, 0, 'null'),
(755, 6380, 19, 0, 0, 0, 0, 'null'),
(756, 6380, 20, 0, 0, 0, 0, 'null'),
(757, 6380, 21, 0, 0, 0, 0, 'null'),
(758, 6380, 22, 0, 0, 0, 0, 'null'),
(759, 6380, 23, 0, 0, 0, 0, 'null'),
(760, 6380, 24, 0, 0, 0, 0, 'null'),
(761, 6380, 25, 1, 0, 0, 0, 'null'),
(762, 6380, 26, 0, 0, 0, 0, 'null'),
(763, 6380, 28, 0, 0, 0, 0, 'null'),
(764, 6380, 29, 0, 0, 0, 0, 'null'),
(765, 6380, 30, 0, 0, 0, 0, 'null'),
(766, 6380, 31, 0, 0, 0, 0, 'null'),
(767, 6380, 32, 0, 0, 0, 0, 'null'),
(768, 6380, 33, 0, 0, 0, 0, 'null'),
(769, 6380, 34, 0, 0, 0, 0, 'null'),
(770, 6380, 35, 0, 0, 0, 0, 'null'),
(771, 6380, 36, 0, 0, 0, 0, 'null'),
(772, 6380, 37, 0, 0, 0, 0, 'null'),
(773, 6380, 38, 0, 0, 0, 0, 'null'),
(774, 6380, 39, 0, 0, 0, 0, 'null'),
(775, 6380, 40, 0, 0, 0, 0, 'null'),
(776, 6380, 41, 1, 1, 1, 1, 'null'),
(777, 6380, 44, 0, 0, 0, 0, 'null'),
(778, 6380, 64, 0, 0, 0, 0, 'null'),
(779, 6380, 65, 0, 0, 0, 0, 'null'),
(780, 6380, 69, 0, 0, 0, 0, 'null'),
(781, 6380, 70, 0, 0, 0, 0, 'null'),
(782, 6380, 71, 0, 0, 0, 0, 'null'),
(783, 6380, 72, 0, 0, 0, 0, 'null'),
(784, 6380, 73, 1, 1, 1, 1, 'null'),
(785, 6380, 74, 0, 0, 0, 0, 'null'),
(786, 6380, 75, 0, 0, 0, 0, 'null'),
(787, 6380, 76, 0, 0, 0, 0, 'null'),
(788, 6380, 77, 0, 0, 0, 0, 'null'),
(789, 6380, 78, 0, 0, 0, 0, 'null'),
(790, 6380, 79, 0, 0, 0, 0, 'null'),
(791, 6380, 80, 0, 0, 0, 0, 'null'),
(792, 6380, 81, 0, 0, 0, 0, 'null'),
(793, 6380, 82, 0, 0, 0, 0, 'null'),
(794, 6381, 1, 0, 1, 1, 0, '["manage_permissions"]'),
(795, 6381, 2, 0, 0, 0, 0, 'null'),
(796, 6381, 3, 0, 0, 0, 0, 'null'),
(797, 6381, 4, 1, 1, 0, 0, '["show_bill_orders","manage_bill_orders"]'),
(798, 6381, 5, 1, 1, 1, 0, 'null'),
(799, 6381, 6, 0, 0, 0, 0, 'null'),
(800, 6381, 7, 0, 0, 0, 0, 'null'),
(801, 6381, 8, 1, 1, 1, 0, '["change_bill_status","show_bill_orders","add_return_bill","check_branch_target","allow_cashier_atm","return_bill_allow_use_atm"]'),
(802, 6381, 9, 1, 1, 1, 0, '["show_client_profile","finish_money_transfer_with_atm"]'),
(803, 6381, 10, 1, 1, 0, 0, 'null'),
(804, 6381, 11, 0, 0, 0, 0, 'null'),
(805, 6381, 12, 0, 0, 0, 0, 'null'),
(806, 6381, 13, 0, 0, 0, 0, 'null'),
(807, 6381, 14, 0, 0, 0, 0, 'null'),
(808, 6381, 15, 0, 0, 0, 0, 'null'),
(809, 6381, 16, 0, 0, 0, 0, 'null'),
(810, 6381, 17, 0, 0, 0, 0, 'null'),
(811, 6381, 18, 0, 0, 0, 0, 'null'),
(812, 6381, 19, 0, 0, 0, 0, 'null'),
(813, 6381, 20, 0, 0, 0, 0, 'null'),
(814, 6381, 21, 0, 0, 0, 0, 'null'),
(815, 6381, 22, 0, 0, 0, 0, 'null'),
(816, 6381, 23, 0, 0, 0, 0, 'null'),
(817, 6381, 24, 0, 0, 0, 0, 'null'),
(818, 6381, 25, 1, 0, 0, 0, 'null'),
(819, 6381, 26, 0, 0, 0, 0, 'null'),
(820, 6381, 28, 0, 0, 0, 0, 'null'),
(821, 6381, 29, 0, 0, 0, 0, 'null'),
(822, 6381, 30, 0, 0, 0, 0, 'null'),
(823, 6381, 31, 0, 0, 0, 0, 'null'),
(824, 6381, 32, 0, 0, 0, 0, 'null'),
(825, 6381, 33, 0, 0, 0, 0, 'null'),
(826, 6381, 34, 0, 0, 0, 0, 'null'),
(827, 6381, 35, 1, 0, 0, 0, '["show_branch_target"]'),
(828, 6381, 36, 0, 1, 0, 0, 'null'),
(829, 6381, 37, 0, 0, 0, 0, 'null'),
(830, 6381, 38, 1, 1, 0, 0, '["receive_commission"]'),
(831, 6381, 39, 1, 0, 0, 0, 'null'),
(832, 6381, 40, 0, 0, 0, 0, 'null'),
(833, 6381, 41, 1, 0, 0, 0, 'null'),
(834, 6381, 44, 1, 1, 0, 0, 'null'),
(835, 6381, 64, 0, 0, 0, 0, 'null'),
(836, 6381, 65, 0, 0, 0, 0, 'null'),
(837, 6381, 69, 0, 0, 0, 0, 'null'),
(838, 6381, 70, 0, 0, 0, 0, 'null'),
(839, 6381, 71, 0, 0, 0, 0, 'null'),
(840, 6381, 72, 0, 0, 0, 0, 'null'),
(841, 6381, 73, 1, 1, 1, 0, 'null'),
(842, 6381, 74, 1, 0, 0, 0, 'null'),
(843, 6381, 75, 0, 0, 0, 0, 'null'),
(844, 6381, 76, 0, 0, 0, 0, 'null'),
(845, 6381, 77, 0, 0, 0, 0, 'null'),
(846, 6381, 78, 0, 0, 0, 0, 'null'),
(847, 6381, 79, 1, 1, 0, 0, '["receive_transfers"]'),
(848, 6381, 80, 0, 0, 0, 0, 'null'),
(849, 6381, 81, 0, 0, 0, 0, 'null'),
(850, 6381, 82, 0, 0, 0, 0, 'null'),
(851, 6384, 1, 1, 1, 1, 1, '[]'),
(852, 6384, 8, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill","check_branch_target","allow_cashier_atm","return_bill_allow_use_atm"]'),
(853, 6384, 35, 1, 0, 0, 0, '["show_branch_target"]'),
(854, 6384, 4, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill"]'),
(855, 6384, 38, 1, 0, 0, 0, '["receive_commission"]'),
(856, 6384, 39, 1, 0, 0, 0, '[]'),
(857, 6384, 36, 1, 1, 1, 0, '[]'),
(858, 6387, 1, 1, 1, 1, 1, '[]'),
(859, 6387, 8, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill","check_branch_target","allow_cashier_atm","return_bill_allow_use_atm"]'),
(860, 6387, 35, 1, 0, 0, 0, '["show_branch_target"]'),
(861, 6387, 4, 1, 1, 1, 1, '["change_bill_status","show_bill_orders","add_return_bill"]'),
(862, 6387, 38, 1, 0, 0, 0, '["receive_commission"]'),
(863, 6387, 39, 1, 0, 0, 0, '[]'),
(864, 6387, 36, 1, 1, 1, 0, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `permission_bills`
--

CREATE TABLE IF NOT EXISTS `permission_bills` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'اللي طالب',
  `branch_id` int(11) NOT NULL COMMENT 'if 0 >> on main branch',
  `permission_type` varchar(300) NOT NULL COMMENT 'add or get',
  `permission_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `permission_bills`
--

INSERT INTO `permission_bills` (`permission_id`, `user_id`, `branch_id`, `permission_type`, `permission_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 54, 0, 'add', '2017-05-31', '2017-05-31 23:22:41', '2017-05-31 23:22:41', NULL),
(2, 54, 0, 'get', '2017-05-31', '2017-05-31 23:23:03', '2017-05-31 23:23:03', NULL),
(3, 2, 6379, 'add', '2017-06-01', '2017-06-01 07:55:49', '2017-06-01 07:55:49', NULL),
(4, 2, 0, 'add', '2017-06-04', '2017-06-05 06:49:01', '2017-06-05 06:49:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_bill_orders`
--

CREATE TABLE IF NOT EXISTS `permission_bill_orders` (
  `permission_bill_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_quantity` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`permission_bill_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `permission_bill_orders`
--

INSERT INTO `permission_bill_orders` (`permission_bill_order_id`, `permission_id`, `pro_id`, `pro_quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 13, '100.00', NULL, NULL, NULL),
(2, 1, 14, '100.00', NULL, NULL, NULL),
(3, 2, 13, '100.00', NULL, NULL, NULL),
(4, 2, 14, '100.00', NULL, NULL, NULL),
(5, 3, 13, '1.00', NULL, NULL, NULL),
(6, 3, 17, '1.00', NULL, NULL, NULL),
(7, 3, 22, '1.00', NULL, NULL, NULL),
(8, 4, 725, '50.00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_pages`
--

CREATE TABLE IF NOT EXISTS `permission_pages` (
  `per_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(200) NOT NULL,
  `page_label` varchar(200) NOT NULL,
  `sub_sys` varchar(200) NOT NULL,
  `sub_group` varchar(300) NOT NULL,
  `sub_group_ar` varchar(300) NOT NULL,
  `all_additional_permissions` text NOT NULL,
  `all_additional_permissions_ar` text NOT NULL,
  PRIMARY KEY (`per_page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `permission_pages`
--

INSERT INTO `permission_pages` (`per_page_id`, `page_name`, `page_label`, `sub_sys`, `sub_group`, `sub_group_ar`, `all_additional_permissions`, `all_additional_permissions_ar`) VALUES
(1, 'factory/users', 'شاشه مستخدمين النظام', 'admin', 'system_settings', 'إعدادات النظام', '["manage_permissions"]', '["\\u0625\\u0639\\u0637\\u0627\\u0621 \\u0635\\u0644\\u0627\\u062d\\u064a\\u0627\\u062a"]'),
(2, 'site_tracking', 'شاشه متابعه حركات المستخدم', 'admin', 'system_settings', 'إعدادات النظام', '[]', '[]'),
(3, 'supplier_materials', 'شاشة المواد الخام من الموردين', 'admin', 'suppliers', 'الموردين', '[]', '[]'),
(4, 'branch_orders', 'شاشه طلبات الفروع', 'admin', 'branches', 'الفروع', '["change_bill_status","show_bill_orders","add_return_bill","manage_bill_orders","manage_bill_return_orders"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0647 \\u0627\\u0644\\u0641\\u0627\\u062a\\u0648\\u0631\\u0629","\\u0645\\u0634\\u0627\\u062f\\u0647 \\u0637\\u0644\\u0628\\u0627\\u062a \\u0627\\u0644\\u0641\\u0627\\u062a\\u0648\\u0631\\u0629","\\u0625\\u0636\\u0627\\u0641\\u0629 \\u0641\\u0627\\u062a\\u0648\\u0631\\u0629 \\u0645\\u0631\\u062a\\u062c\\u0639","\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0629 \\u0641\\u0648\\u0627\\u062a\\u064a\\u0631 \\u0627\\u0644\\u0637\\u0644\\u0628\\u0627\\u062a","\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0629 \\u0641\\u0648\\u0627\\u062a\\u064a\\u0631 \\u0627\\u0644\\u0645\\u0631\\u062a\\u062c\\u0639"]'),
(5, 'branch_clients_orders', 'شاشه طلبات العملاء', 'admin', 'branches', 'الفروع', '[]', '[]'),
(6, 'branch/branch_orders', 'hideشاشه طلبات الفروع', 'admin', 'hide', 'hide', '[]', ''),
(7, 'orders/suppliers', 'شاشه فواتير الموردين', 'admin', 'suppliers', 'الموردين', '["add_purchase_bill","add_return_bill"]', '["\\u0625\\u0636\\u0627\\u0641\\u0629 \\u0641\\u0627\\u062a\\u0648\\u0631\\u0629 \\u0634\\u0631\\u0627\\u0621 \\u0645\\u0646 \\u0645\\u0648\\u0631\\u062f","\\u0625\\u0636\\u0627\\u0641\\u0629 \\u0641\\u0627\\u062a\\u0648\\u0631\\u0629 \\u0645\\u0631\\u062a\\u062c\\u0639 \\u0645\\u0646 \\u0645\\u0648\\u0631\\u062f"]'),
(8, 'admin/client_orders', 'شاشه فواتير العملاء', 'admin', 'branches', 'الفروع', '["change_bill_status","show_bill_orders","add_return_bill","show_client_bills_statistics","check_branch_target","allow_cashier_atm","allow_cashier_to_remain_money","return_bill_allow_use_atm","return_bill_allow_remain_money"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0629 \\u0627\\u0644\\u0641\\u0627\\u062a\\u0648\\u0631\\u0629","\\u0645\\u0634\\u0627\\u0647\\u062f\\u0647 \\u0637\\u0644\\u0628\\u0627\\u062a \\u0627\\u0644\\u0641\\u0627\\u062a\\u0648\\u0631\\u0629","\\u0625\\u0636\\u0627\\u0641\\u0629 \\u0641\\u0627\\u062a\\u0648\\u0631\\u0629 \\u0645\\u0631\\u062a\\u062c\\u0639","\\u0645\\u0634\\u0627\\u0647\\u062f\\u0629 \\u0627\\u062d\\u0635\\u0627\\u0626\\u064a\\u0627\\u062a \\u0639\\u0644\\u064a \\u0641\\u0648\\u0627\\u062a\\u064a\\u0631 \\u0627\\u0644\\u0639\\u0645\\u0644\\u0627\\u0621","\\u0645\\u062a\\u0627\\u0628\\u0639\\u0647 \\u0627\\u0644\\u062a\\u0627\\u0631\\u062c\\u062a","\\u0627\\u0644\\u0633\\u0645\\u0627\\u062d \\u0644\\u0644\\u0628\\u064a\\u0639 \\u0628\\u0627\\u0644\\u0634\\u0628\\u0643\\u0647","\\u0627\\u0644\\u0633\\u0645\\u0627\\u062d \\u0644\\u0644\\u0628\\u064a\\u0639 \\u0628\\u0627\\u0644\\u0623\\u062c\\u0644","\\u0627\\u0644\\u0633\\u0645\\u0627\\u062d \\u0644\\u0639\\u0645\\u0644 \\u0645\\u0631\\u062a\\u062c\\u0639 \\u0648\\u0627\\u0644\\u062f\\u0641\\u0639 \\u0628\\u0627\\u0644\\u0634\\u0628\\u0643\\u0647","\\u0627\\u0644\\u0633\\u0645\\u0627\\u062d \\u0644\\u0639\\u0645\\u0644 \\u0645\\u0631\\u062a\\u062c\\u0639 \\u0628\\u0627\\u0644\\u0623\\u062c\\u0644"]'),
(9, 'branch/users', 'شاشه مستخدمين الفرع', 'admin', 'branches', 'الفروع', '["manage_permissions","show_client_profile","finish_money_transfer","finish_money_transfer_with_atm","finish_money_transfer_partially"]', '["\\u0625\\u0639\\u0637\\u0627\\u0621 \\u0635\\u0644\\u0627\\u062d\\u064a\\u0627\\u062a","\\u0645\\u0634\\u0627\\u0647\\u062f\\u0629 \\u0628\\u0631\\u0648\\u0641\\u0627\\u064a\\u0644 \\u0627\\u0644\\u0639\\u0645\\u064a\\u0644","\\u0627\\u0644\\u0633\\u0645\\u0627\\u062d \\u0644\\u0634\\u0627\\u0634\\u0647 \\u0625\\u0646\\u0647\\u0627\\u0621 \\u0627\\u0644\\u0645\\u0639\\u0627\\u0645\\u0644\\u0627\\u062a \\u0627\\u0644\\u0645\\u0627\\u062f\\u064a\\u0629","\\u0625\\u0646\\u0647\\u0627\\u0621 \\u0627\\u0644\\u0645\\u0639\\u0627\\u0645\\u0644\\u0627\\u062a \\u0627\\u0644\\u0645\\u0627\\u062f\\u064a\\u0629 \\u0628\\u0627\\u0644\\u0634\\u0628\\u0643\\u0629","\\u0625\\u0646\\u0647\\u0627\\u0621 \\u0627\\u0644\\u0645\\u0639\\u0627\\u0645\\u0644\\u0627\\u062a \\u0627\\u0644\\u0645\\u0627\\u062f\\u064a\\u0629 \\u062c\\u0632\\u0626\\u064a\\u0627"]'),
(10, 'branch/expenses', 'شاشه مصاريف الفرع', 'admin', 'branches', 'الفروع', '[]', '[]'),
(11, 'branch/assets', 'شاشه اصول الفرع', 'admin', 'branches', 'الفروع', '[]', '[]'),
(12, 'factory/assets', 'شاشه الاصول', 'admin', 'branches', 'الفروع', '[]', '[]'),
(13, 'factory/expenses', 'شاشه المصاريف', 'admin', 'branches', 'الفروع', '["change_expense_status"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0647 \\u0627\\u0644\\u0645\\u0635\\u0631\\u0648\\u0641"]'),
(14, 'factory/materials', 'شاشه المواد الخام', 'admin', 'hide', 'hide', '[]', '[]'),
(15, 'orders/branch_orders', 'hideشاشه طلبات الفروع', 'admin', 'hide', 'hide', '[]', ''),
(16, 'product/product_materials', 'شاشه مكونات المنتجات من المواد الخام', 'admin', 'hide', 'hide', '[]', '[]'),
(17, 'product/broken_product_materials', 'شاشه الهالك من المواد الخام للمنتجات', 'admin', 'hide', 'hide', '[]', '[]'),
(18, 'product/products_on_stock', 'شاشه المنتجات في المخزن', 'admin', 'store', 'المخزن', '[]', '[]'),
(19, 'product/broken_products_on_stock', 'شاشه الهالك من المنتجات', 'admin', 'store', 'المخزن', '[]', '[]'),
(20, 'product/produce', 'شاشه إنتاج منتج', 'admin', 'hide', 'hide', '[]', '[]'),
(21, 'admin/money_transfer', 'شاشه التحويلات المادية ', 'admin', 'money', 'الحسابات المالية', '[]', '[]'),
(22, 'materials/broken', 'شاشه المواد الخام الهالكه', 'admin', 'production', 'الانتاج', '[]', '[]'),
(23, 'suppliers/profile', 'شاشه بروفايل الموردين', 'admin', 'suppliers', 'الموردين', '[]', '[]'),
(24, 'product/branch_product_prices', 'شاشه اسعار المنتجات للفروع', 'admin', 'store', 'المخزن', '[]', '[]'),
(25, 'factory/packages', 'شاشه العروض', 'admin', 'selling', 'عمليات البيع', '[]', '[]'),
(26, 'factory/atm_permission', 'الدفع او الاستلام بالشبكه', 'admin', 'money', 'الحسابات المالية', '[]', '[]'),
(28, 'product/review_produce_products', 'شاشه مراجعه طلبات إنتاج المنتجات', 'admin', 'hide', 'hide', '[]', '[]'),
(29, 'product/reverse_produce', 'شاشه إلغاء منتجات أنتجت بالخطأ', 'admin', 'hide', 'hide', '[]', '[]'),
(30, 'admin/coupons', 'شاشه الكوبونات', 'admin', 'selling', 'عمليات البيع', '[]', '[]'),
(31, 'admin/product_barcode', 'شاشه باركود المنتجات', 'admin', 'templates', 'القوالب', '["print_barcode"]', '["\\u0637\\u0628\\u0627\\u0639\\u0647 \\u0628\\u0627\\u0631\\u0643\\u0648\\u062f"]'),
(32, 'admin/invoices_templates', 'شاشه قوالب الفواتير', 'admin', 'templates', 'القوالب', '[]', '[]'),
(33, 'admin/invoice_template_items', 'شاشه عناصر قوالب الفواتير', 'admin', 'templates', 'القوالب', '["save_items_position"]', '["\\u062d\\u0641\\u0638 \\u0627\\u0645\\u0627\\u0643\\u0646 \\u0627\\u0644\\u0639\\u0646\\u0627\\u0635\\u0631 \\u0644\\u0644\\u0642\\u0627\\u0644\\u0628"]'),
(34, 'admin/banks', 'شاشه البنوك', 'admin', 'money', 'الحسابات المالية', '[]', '[]'),
(35, 'admin/branch_target', 'شاشه تارجت الفروع', 'admin', 'branches', 'الفروع', '["show_branch_target"]', '["\\u0645\\u0634\\u0627\\u0647\\u062f\\u0647 \\u0648\\u0645\\u062a\\u0627\\u0628\\u0639\\u0647 \\u062a\\u0627\\u0631\\u062c\\u062a \\u0627\\u0644\\u0641\\u0631\\u0648\\u0639"]'),
(36, 'admin/deposite_money', 'شاشه إيداعات الفروع', 'admin', 'branches', 'الفروع', '[]', '[]'),
(37, 'admin/cheque_templates', 'شاشه قوالب الشيكات', 'admin', 'templates', 'القوالب', '["save_items_position"]', '["\\u062d\\u0641\\u0638 \\u0627\\u0645\\u0627\\u0643\\u0646 \\u0639\\u0646\\u0627\\u0635\\u0631 \\u0627\\u0644\\u0634\\u064a\\u0643\\u0627\\u062a"]'),
(38, 'admin/commission', 'شاشه العمولات', 'admin', 'branches', 'الفروع', '["receive_commission"]', '["\\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0627\\u0644\\u0639\\u0645\\u0648\\u0644\\u0647"]'),
(39, 'admin/commission_rules', 'شاشه قواعد العمولات', 'admin', 'branches', 'الفروع', '[]', '[]'),
(40, 'admin/print_cheques', 'شاشه طباعة شيك', 'admin', 'templates', 'القوالب', '[]', '[]'),
(41, 'admin/print_client_bill', 'شاشه طباعة فاتورة عميل', 'admin', 'selling', 'عمليات البيع', '[]', '[]'),
(46, 'assets/category', 'شاشه تصنيفات الأصول', 'accountant', '', '', '[]', '[]'),
(47, 'assets/locations', '', 'accountant', '', '', '[]', ''),
(48, 'assets/status', '', 'accountant', '', '', '[]', ''),
(49, 'assets', '', 'accountant', '', '', '[]', ''),
(50, 'assets/documents', '', 'accountant', '', '', '[]', ''),
(51, 'assets/installment', '', 'accountant', '', '', '["pay_get_installment"]', ''),
(52, 'assets/assets_sold', '', 'accountant', '', '', '[]', ''),
(53, 'assets/assets_rubbish', '', 'accountant', '', '', '[]', ''),
(54, 'assets/assets_on_maintenance', '', 'accountant', '', '', '["is_maintenance_finish"]', ''),
(55, 'assets/assets_transports', '', 'accountant', '', '', '[]', ''),
(56, 'assets/asset_expenses_and_incomes', '', 'accountant', '', '', '[]', ''),
(57, 'assets/assets_rent', '', 'accountant', '', '', '[]', ''),
(58, 'assets/assets_deprecation', '', 'accountant', '', '', '["count_deprecation"]', ''),
(59, 'accountant/general_account', '', 'accountant', '', '', '["move_account_to_another_category"]', ''),
(60, 'accountant/financial_year', '', 'accountant', '', '', '["close_financial_year"]', ''),
(61, 'accountant/entry', '', 'accountant', '', '', '["entry_approve"]', ''),
(62, 'assets/assets_that_reevaluated', '', 'accountant', '', '', '[]', ''),
(63, 'accountant/entry_templates', '', 'accountant', '', '', '[]', ''),
(64, 'factory/category', 'ادارة التصنيفات', 'admin', 'production', 'الانتاج', '["move_category_to_another_parent"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u0645\\u0643\\u0627\\u0646 \\u0627\\u0644\\u062a\\u0646\\u0635\\u064a\\u0641 \\u0641\\u0627\\u0644\\u0634\\u0643\\u0644 \\u0627\\u0644\\u0634\\u062c\\u0631\\u064a"]'),
(65, 'admin/price_list', 'قائمة الاسعار', 'admin', 'selling', 'عمليات البيع', '[]', '[]'),
(66, 'accountant/expenses', 'المصاريف', 'accountant', '', '', '["change_expense_status"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u062d\\u0627\\u0644\\u0647 \\u0627\\u0644\\u0645\\u0635\\u0631\\u0648\\u0641"]'),
(67, 'accountant/category', 'التصنيفات', 'accountant', '', '', '["move_category_to_another_parent"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u0645\\u0643\\u0627\\u0646 \\u0627\\u0644\\u062a\\u0646\\u0635\\u064a\\u0641 \\u0641\\u0627\\u0644\\u0634\\u0643\\u0644 \\u0627\\u0644\\u0634\\u062c\\u0631\\u064a"]'),
(68, 'accountant/general_account_category', 'general_account_category', 'accountant', '', '', '["move_category_to_another_parent"]', '["\\u062a\\u063a\\u064a\\u064a\\u0631 \\u0645\\u0643\\u0627\\u0646 \\u0627\\u0644\\u062a\\u0646\\u0635\\u064a\\u0641 \\u0641\\u0627\\u0644\\u0634\\u0643\\u0644 \\u0627\\u0644\\u0634\\u062c\\u0631\\u064a"]'),
(69, 'factory/duty_documents', 'شاشه سندات العهد', 'admin', 'money', 'الحسابات المالية', '["change_duty_document_received"]', '["\\u0625\\u0633\\u062a\\u0644\\u0627\\u0645 \\u0627\\u0644\\u0639\\u0647\\u062f\\u0647"]'),
(70, 'factory/users_transits', 'شاشه التنقلات للمستخدمين', 'admin', 'branches', 'الفروع', '[]', '[]'),
(71, 'factory/payment_methods', 'شاشه طرق السداد', 'admin', 'money', 'الحسابات المالية', '[]', '[]'),
(73, 'branch/points_of_sale', 'شاشه نقاط البيع', 'admin', 'branches', 'الفروع', '[]', '[]'),
(74, 'branch/products_first_time', 'شاشه بضاعه أول مده', 'admin', 'branches', 'الفروع', '[]', '[]'),
(75, 'factory/reports', 'شاشه التقارير', 'admin', 'selling', 'عمليات البيع', '["sell_bills","show_bill_orders","sell_bills_depend_on_customer","sell_bills_depend_on_user_age","sell_bills_depend_on_cashier","sell_bills_depend_on_branch","sell_bills_depend_on_user_gender","sell_bills_depend_on_product"]', '["\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0625\\u062c\\u0645\\u0627\\u0644\\u064a","\\u0645\\u0634\\u0627\\u0647\\u062f\\u0629 \\u0637\\u0644\\u0628\\u0627\\u062a \\u0627\\u0644\\u0641\\u0627\\u062a\\u0648\\u0631\\u0629","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u0644\\u0639\\u0645\\u0644\\u0627\\u0621","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u0644\\u0639\\u0645\\u0631","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u0644\\u0643\\u0627\\u0634\\u064a\\u064a\\u0631","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u0644\\u0641\\u0631\\u0639","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u062c\\u0646\\u0633","\\u0627\\u0644\\u062a\\u0642\\u0631\\u064a\\u0631 \\u0628\\u0627\\u0644\\u0646\\u0633\\u0628\\u0647 \\u0644\\u0644\\u0645\\u0646\\u062a\\u062c"]'),
(77, 'admin/review_client_bills', 'شاشه مراجعه فواتير العملاء', 'admin', 'branches', 'عمليات البيع', '[]', '[]'),
(78, 'company', 'شاشة بيانات الشركة', 'admin', 'system_settings', 'إعدادات النظام', '[]', '[]'),
(79, 'branch_transfer_products', 'شاشة تحويلات المخازن', 'admin', 'branches', 'الفروع', '["receive_transfers"]', '["استلام التحويل"]'),
(80, 'admin/product_codes', 'شاشه أنواع الأكواد', 'admin', 'production', 'الانتاج', '[]', '[]'),
(81, 'get_permission_from_stock', 'اذن صرف', 'admin', 'store', 'المخزن', '[]', '[]'),
(82, 'add_permission_to_stock', 'اذن اضافة', 'admin', 'store', 'المخزن', '[]', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `points_of_sale`
--

CREATE TABLE IF NOT EXISTS `points_of_sale` (
  `point_of_sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `point_of_sale_code` varchar(300) NOT NULL,
  `point_of_sale_desc` text NOT NULL,
  `point_of_sale_desc_en` text NOT NULL,
  `point_of_sale_balance` decimal(10,2) NOT NULL,
  `set_cash_on_my_balance` tinyint(1) NOT NULL COMMENT 'هل الكاش يدخل في درج نقطة البيع ولا الفرع',
  `is_exist_password_for_return` tinyint(1) NOT NULL COMMENT 'هل يوجد كلمة سر للمردودات',
  `return_password` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`point_of_sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `price_list`
--

CREATE TABLE IF NOT EXISTS `price_list` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_title` varchar(200) NOT NULL,
  `default_price` tinyint(1) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `price_list`
--

INSERT INTO `price_list` (`price_id`, `price_title`, `default_price`) VALUES
(2, 'سعر الجمهور', 1),
(3, 'سعر التكلفة', 0);

-- --------------------------------------------------------

--
-- Table structure for table `produce_product_review`
--

CREATE TABLE IF NOT EXISTS `produce_product_review` (
  `ppr_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `produce_quantity` int(11) NOT NULL,
  `is_produced` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ppr_id`),
  KEY `pro_id` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `pro_unit_id` int(11) NOT NULL,
  `pro_img` int(11) NOT NULL,
  `pro_name` varchar(300) NOT NULL,
  `pro_name_en` varchar(200) NOT NULL,
  `pro_desc` text NOT NULL,
  `pro_barcode` text NOT NULL,
  `pro_quantity` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `pro_expire_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  KEY `pro_name` (`pro_name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_barcode`
--

CREATE TABLE IF NOT EXISTS `product_barcode` (
  `barcode_id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode_orientation` varchar(200) NOT NULL,
  `barcode_width` decimal(10,2) NOT NULL,
  `barcode_height` decimal(10,2) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `barcode_margin_top` decimal(10,2) NOT NULL,
  `barcode_margin_right` decimal(10,2) NOT NULL,
  `barcode_margin_bottom` decimal(10,2) NOT NULL,
  `barcode_margin_left` decimal(10,2) NOT NULL,
  `barcode_date_top_position` decimal(10,2) NOT NULL,
  `barcode_date_left_position` decimal(10,2) NOT NULL,
  `barcode_code_top_position` decimal(10,2) NOT NULL,
  `barcode_code_left_position` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`barcode_id`),
  KEY `pro_id` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_codes`
--

CREATE TABLE IF NOT EXISTS `product_codes` (
  `pro_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_code_label` varchar(200) NOT NULL,
  `pro_code_label_en` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_components`
--

CREATE TABLE IF NOT EXISTS `product_components` (
  `pro_comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `mat_id` int(11) NOT NULL,
  `pro_comp_amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`pro_comp_id`),
  KEY `pro_id` (`pro_id`),
  KEY `mat_id` (`mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_materials`
--

CREATE TABLE IF NOT EXISTS `product_materials` (
  `pro_mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_id` int(11) NOT NULL,
  `mat_amount` decimal(10,2) NOT NULL,
  `mat_price` decimal(10,2) NOT NULL,
  `mat_pro_limit` int(11) NOT NULL,
  `mat_desc` text NOT NULL,
  `mat_barcode` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_mat_id`),
  KEY `mat_id` (`mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE IF NOT EXISTS `product_prices` (
  `pro_price_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `pro_price_label` varchar(100) NOT NULL,
  `pro_price_quantity` int(11) NOT NULL,
  `pro_price_price` decimal(10,2) NOT NULL,
  `pro_price_original` tinyint(1) NOT NULL,
  `pro_price_from` date DEFAULT NULL COMMENT 'if not null there is promotion',
  `pro_price_to` date DEFAULT NULL COMMENT 'if not null there is promotion',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_price_id`),
  KEY `pro_id` (`pro_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='old table' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_prices_values`
--

CREATE TABLE IF NOT EXISTS `product_prices_values` (
  `ppv_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `ppv_value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`ppv_id`),
  KEY `pro_id` (`pro_id`),
  KEY `price_id` (`price_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_select_codes`
--

CREATE TABLE IF NOT EXISTS `product_select_codes` (
  `p_s_c_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_code_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_code_val` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`p_s_c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_units`
--

CREATE TABLE IF NOT EXISTS `product_units` (
  `pro_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_unit_name` varchar(300) NOT NULL,
  `pro_unit_name_en` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('2e39b1896fceab59688f4c6900ffa887a6518e8b', NULL, '160.153.161.238', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiSVgySmg3SXlkbWpUTGpXT0tjZkYwSDJOcjNnUlhEVk1CYzdickM4NCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDc6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2NvdW50X2NvbW1pc3Npb25zIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2NzMwMDI7czoxOiJjIjtpOjE0OTY2NzMwMDI7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1496673002),
('42c8361be0887ff13d5b106fd8b03b6a00375980', NULL, '160.153.161.238', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiSHpwa25NUlFoank1ZzU2TTF2dkdpVm1HR1JxdUhGbXZkaFoxSUJ1SSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDc6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2NvdW50X2NvbW1pc3Npb25zIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2NzEyMDI7czoxOiJjIjtpOjE0OTY2NzEyMDI7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1496671203),
('432654eeea6f08bfc065d99a3d6fb2646c9fb0f7', 2, '51.39.227.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiS0pxQWVhVEpQQ0RUM1hEaVR0Q0dIRmVuRmFJR2JFcXB6eUZkRnVaNCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NzQ6Imh0dHA6Ly93d3cucGVyZnVtZXNnYXRlLmNvbS9zYWxlcy9fZGVidWdiYXIvYXNzZXRzL2phdmFzY3JpcHQ/dj0xNDgzODk1Mzg4Ijt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToyO3M6MjI6IlBIUERFQlVHQkFSX1NUQUNLX0RBVEEiO2E6MDp7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDk2NjczODc0O3M6MToiYyI7aToxNDk2NjYwNjAyO3M6MToibCI7czoxOiIwIjt9fQ==', 1496673875),
('7c7731fc9c5f3c3057f95fdda04cc2d8e39efb55', 54, '197.133.219.38', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoicEo3aktEdTg2OHVteDBSMEpyd0dzY2xla1lZc1diWnVVcEJlVjZrVCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTU6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2FkbWluL3BvaW50c19vZl9zYWxlL3NhdmUiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjU0O3M6MjI6IlBIUERFQlVHQkFSX1NUQUNLX0RBVEEiO2E6MDp7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDk2NjczNDE4O3M6MToiYyI7aToxNDk2NjYwNTMyO3M6MToibCI7czoxOiIwIjt9fQ==', 1496673418),
('81620b6e41d0fa05da6a9f2675717bdcc08f2f5a', NULL, '160.153.161.238', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiZnRhdzVKQVZscUVtOVRJUzlTcHZZeGZsaVVWT0FXelYyRWhRYlUwRyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDc6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2NvdW50X2NvbW1pc3Npb25zIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2Njc2MDE7czoxOiJjIjtpOjE0OTY2Njc2MDE7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1496667603),
('91a52aed5bf7e500d97b8e824c4dac0af5ef8a95', 54, '51.39.227.105', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) GSA/27.0.155813979 Mobile/14F89 Safari/602.1', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiTFd1VU9MdnpjTzVWdGZuelhBeVV5RVAzRk9yY1pzVjlYU3l4T0VLcCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly93d3cucGVyZnVtZXNnYXRlLmNvbS9zYWxlcyI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NTQ7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2NzQ4MDM7czoxOiJjIjtpOjE0OTY2NjUxOTc7czoxOiJsIjtzOjE6IjAiO319', 1496674803),
('a205dd21c636e839c52b26234c584592ffee1357', 54, '197.133.219.38', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiZDg5MnluMkQ0RkN5N1lxc3RwbVkxNnc3ZWpRN1Y3SHdEYnRTQnZPOSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NzI6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2FkbWluL3JlcG9ydHMvdG90YWxfYnJhbmNoZXNfY2xpZW50c19iaWxscyI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NTQ7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2NzQwNjE7czoxOiJjIjtpOjE0OTY2NzQwNTI7czoxOiJsIjtzOjE6IjAiO319', 1496674062),
('b226a0c6edd69311399791c90c15f031e732d9f1', NULL, '45.55.254.212', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiVW5POGFtSnJDZ01Zd2ZJQjNOb1BJZ0QwMTlpcGhIODNaOEVlbEY5NCI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly93d3cucGVyZnVtZXNnYXRlLmNvbS9zYWxlcyI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDk2NjczMTYwO3M6MToiYyI7aToxNDk2NjczMTYwO3M6MToibCI7czoxOiIwIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1496673160),
('c173e979de60829cba7aeee3232c7d4b3f5ced94', 54, '46.152.54.175', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiNWRoUmEzMWc1cW5JVk50a0M3TVk5cmw4c3R5SW1xalNUMFoya0hOeiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Njg6Imh0dHA6Ly93d3cucGVyZnVtZXNnYXRlLmNvbS9zYWxlcy9hZG1pbi91c2Vycy9hc3NpZ25fcGVybWlzc2lvbi82MzgxIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTo1NDtzOjIyOiJQSFBERUJVR0JBUl9TVEFDS19EQVRBIjthOjA6e31zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ5NjY3MDc2NztzOjE6ImMiO2k6MTQ5NjY3MDYzNjtzOjE6ImwiO3M6MToiMCI7fX0=', 1496670767),
('d4d7db1981d50d574bf50a61c379e86c047b0582', NULL, '160.153.161.238', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiNlE1VTNNR0gzdnNUcERXZmpIN1NDNjh5VGFzWWtWenh0N2VUTERqMSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDc6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2NvdW50X2NvbW1pc3Npb25zIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2Njk0MDI7czoxOiJjIjtpOjE0OTY2Njk0MDI7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1496669402),
('dca7ab20e4bf8faa17c2c7ae7de1e2353aa6ac77', NULL, '160.153.161.238', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiWEhnbjlQY2gyQmZhWHcxZDlWNUl6WnpuWVdQVTZOV3lqMnp1T2p2bSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDc6Imh0dHA6Ly9wZXJmdW1lc2dhdGUuY29tL3NhbGVzL2NvdW50X2NvbW1pc3Npb25zIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY2NzQ4MDI7czoxOiJjIjtpOjE0OTY2NzQ4MDI7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1496674802);

-- --------------------------------------------------------

--
-- Table structure for table `site_content`
--

CREATE TABLE IF NOT EXISTS `site_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_title` varchar(200) NOT NULL,
  `content_json` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `site_content`
--

INSERT INTO `site_content` (`id`, `content_title`, `content_json`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 'support', '{"meta_title":"Contact US","meta_desc":"Contact US","meta_keywords":"Contact US","contact_us_bg_img_header":"Contact US","contact_us_bg_img_icon":"fa fa-mobile","contact_us_bg_img_text":"Contact Us Now","breadcrumb_header":"Contact US","header":"Contact US","contact_us_body":"Contact Us\\r\\n","form_header":"Send Massage\\r\\n","form_fullname":"Full Name","form_email":"Email","form_btn_text":"Send","form_sucess_msg":"Done","img_ids":{"contact_us_bg_img":282},"label":["No. Phone","No. Mobile","No. Fax","Email","Map"],"value":["0123456789","0123456789","0123456789","info@visitkosovo.co","<iframe class=\\"b-lazy\\"  data-src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d1504707.4445591401!2d19.780887996622877!3d42.557804210974616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13537af354bf7df1%3A0xbfffeedfabc31791!2sKosovo!5e0!3m2!1sen!2seg!4v1479041095978\\" width=\\"100%\\" height=\\"250\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen=\\"\\"><\\/iframe>",""]}', NULL, NULL, NULL),
(12, 'email_page', '{"copyright":"all right reseved . <a href=''www.seoera.net''>Seoera<\\/a>","img_ids":{"logo_img":111},"social_imgs":["fb.png","gplus.png"],"social_links":["#","#"]}', NULL, NULL, NULL),
(13, 'photo_gallery', '{"meta_title":"Mazziktna-Photo-Gallery","meta_desc":"Mazziktna-Photo-Gallery Short Description","meta_keywords":"Mazziktna-Photo-Gallery Keywords","breadcrump_current":"Photo-Gallery","header":"Photo Gallery","info":"Welcome To Mazziktna Photo Gallery","body":"<h3>Video&nbsp;Gallry<\\/h3>\\r\\n<p>Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna Welcome To Mazziktna<\\/p>","img_ids":{"background_img":113},"slider1":{"img_ids":[114,115,116,117,118,119],"other_fields":{"title":["Photo-Gallery","Photo-Gallery","Photo-Gallery","Photo-Gallery","Photo-Gallery","Photo-Gallery",""]}}}', NULL, NULL, NULL),
(31, 'keywords', '{"homepage":"Home","more":"Read More","img_ids":[]}', NULL, NULL, NULL),
(32, 'all_clients', '{"meta_title":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","meta_desc":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","meta_keywords":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","header":"\\u0645\\u0646 \\u0646\\u062d\\u0646","info":"\\u0631\\u064a\\u062c\\u0627 \\u0647\\u064a \\u0627\\u062d\\u062f\\u064a \\u0627\\u0644\\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0648\\u0637\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0631\\u0627\\u0626\\u062f\\u0629 \\u0641\\u064a \\u0645\\u062c\\u0627\\u0644 \\u0627\\u0644\\u0635\\u064a\\u0627\\u0646\\u0629 \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0648\\u0643\\u0627\\u0641\\u0629 \\u0627\\u0644\\u0639\\u0645\\u0631\\u0627\\u062a \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0644\\u0645\\u0635\\u0627\\u0646\\u0639 \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a","body":"<h3 style=\\"text-align: center;\\">\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629&nbsp;<strong>\\u0639\\u0645\\u0644\\u0627\\u0626\\u0646\\u0627<\\/strong><\\/h3>\\r\\n\\r\\n<p>\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629<\\/p>\\r\\n","img_ids":{"cover":209}}', NULL, NULL, NULL),
(33, 'all_services', '{"meta_title":"Kosovo","meta_desc":"Kosovo","meta_keywords":"Kosovo","header":"Our Services","info":"\\u0631\\u064a\\u062c\\u0627 \\u0647\\u064a \\u0627\\u062d\\u062f\\u064a \\u0627\\u0644\\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0648\\u0637\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0631\\u0627\\u0626\\u062f\\u0629 \\u0641\\u064a \\u0645\\u062c\\u0627\\u0644 \\u0627\\u0644\\u0635\\u064a\\u0627\\u0646\\u0629 \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0648\\u0643\\u0627\\u0641\\u0629 \\u0627\\u0644\\u0639\\u0645\\u0631\\u0627\\u062a \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0644\\u0645\\u0635\\u0627\\u0646\\u0639 \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a","body":"<h3 style=\\"text-align:center\\">&nbsp;<strong>\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627<\\/strong><\\/h3>\\r\\n\\r\\n<p>\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629<\\/p>\\r\\n","img_ids":{"cover":210}}', NULL, NULL, NULL),
(34, 'all_projects', '{"meta_title":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","meta_desc":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","meta_keywords":"\\u0631\\u064a\\u062c\\u0627 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629","header":"\\u0645\\u0646 \\u0646\\u062d\\u0646","info":"\\u0631\\u064a\\u062c\\u0627 \\u0647\\u064a \\u0627\\u062d\\u062f\\u064a \\u0627\\u0644\\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0648\\u0637\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0631\\u0627\\u0626\\u062f\\u0629 \\u0641\\u064a \\u0645\\u062c\\u0627\\u0644 \\u0627\\u0644\\u0635\\u064a\\u0627\\u0646\\u0629 \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0648\\u0643\\u0627\\u0641\\u0629 \\u0627\\u0644\\u0639\\u0645\\u0631\\u0627\\u062a \\u0627\\u0644\\u0645\\u064a\\u0643\\u0627\\u0646\\u064a\\u0643\\u064a\\u0629 \\u0644\\u0645\\u0635\\u0627\\u0646\\u0639 \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a","body":"<h3 style=\\"text-align: center;\\">\\u0631\\u064a\\u062c\\u0627&nbsp;\\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0627\\u0644\\u0645\\u062d\\u062f\\u0648\\u062f\\u0629&nbsp;<strong>\\u0623\\u0639\\u0645\\u0627\\u0644\\u0646\\u0627<\\/strong><\\/h3>\\r\\n\\r\\n<p>\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629<\\/p>\\r\\n","img_ids":{"cover":211}}', NULL, NULL, NULL),
(35, 'gallery', '{"meta_title":"Photo Gallery","meta_desc":"Photo Gallery","meta_keywords":"Photo Gallery","breadcrumb_header":"Photo Gallery","bg_img_header":"Photo Gallery","bg_img_icon":"fa fa-camera","bg_img_text":"Kosovo Sightseeing Tour","img_ids":{"bg_img":285},"slider1":{"img_ids":[314,315,316,317,318,319,320,321,322,323,324,325]}}', NULL, NULL, NULL),
(36, 'edit_header', '{"search_placeholder":"Search","header_menu":"9","img_ids":{"logo":208,"icon":209,"header_bg":273},"social_link_icon":["fa fa-facebook","fa fa-google-plus","fa fa-twitter","fa fa-youtube","fa fa-instagram"],"social_link_href":["#","#","#","#","#",""],"contact_icon":["fa fa-envelope","fa fa-phone"],"contact_title":["\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","\\u0644\\u0644\\u0627\\u062a\\u0635\\u0627\\u0644 \\u0648\\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644",""],"contact_data":["info@visitkosovo.co","00966 56 254",""]}', '0000-00-00 00:00:00', NULL, NULL),
(37, 'edit_index_page', '{"meta_title":"Kosovo","meta_desc":"Kosovo md","meta_keywords":"Kosovo mk","art_sec_1_header":"Welcome to the site\\r\\n","art_sec_1_text":"The first website specialist in tourism in Kosovo in Saudi Arabia and MENA, the best tourist information, places and sightseeing tours you will find here\\r\\n","index_about_section_header":"Welcome To Visit Kosovo\\r\\n","index_about_section_text":"Irma International travel and tourism services, is one of the international companies working in the travel field and tourism services in the State of Kosovo and all the Balkan countries, where the company headquarters is located in the capital Pristina through qualified cadres in the field of tourism services to all our clients from countries around the world who wish to spend their holidays throughout the state of Kosovo and recognize these hospitable which is characterized by the Division of hospitality and scenic nature and historical places or through Verona in the GCC and other Arab states of the country.\\r\\n","index_about_section_btn_1_text":"Read More","index_about_section_btn_1_href":"http:\\/\\/www.visitkosovo.co\\/About-Us","index_about_section_btn_2_text":"Our Services","index_about_section_btn_2_href":"http:\\/\\/www.visitkosovo.co\\/Our-Services","art_sec_2_header":"Visit Kosovo\\r\\n","art_sec_2_text":"The first website specialist in tourism in Kosovo in Saudi Arabia and MENA, the best tourist information, places and sightseeing tours you will find here","art_sec_3_header":"Visit Kosovo\\r\\n","art_sec_3_text":"The first website specialist in tourism in Kosovo in Saudi Arabia and MENA, the best tourist information, places and sightseeing tours you will find here","art_sec_3_embed_youtube_link":"https:\\/\\/www.youtube.com\\/watch?v=McFKtbNs1po","art_sec_4_header":"Visit Kosovo\\r\\n","art_sec_4_text":"The first website specialist in tourism in Kosovo in Saudi Arabia and MENA, the best tourist information, places and sightseeing tours you will find here\\r\\n\\r\\n\\r\\n\\r\\n","img_ids":{"sec_3_bg_img":269},"about_icon":["fa fa-commenting-o","fa fa-rocket","fa fa-compass","fa fa-male"],"about_header":["\\u0631\\u0633\\u0627\\u0644\\u062a\\u0646\\u0627","\\u0631\\u0633\\u0627\\u0644\\u062a\\u0646\\u0627","\\u0631\\u0624\\u064a\\u062a\\u0646\\u0627","\\u0645\\u0646 \\u0646\\u062d\\u0646",""],"about_paragraph":["\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629","\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629","\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629","\\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0634\\u0631\\u0627\\u0643\\u062a\\u0646\\u0627 \\u0627\\u0644\\u0633\\u0627\\u0628\\u0642\\u0629 \\u0645\\u0639 \\u0634\\u0631\\u0643\\u0629 \\u0631\\u064a\\u062c\\u0627 \\u0627\\u064a\\u062c\\u064a\\u0628\\u062a , \\u0648\\u0630\\u0627\\u0644\\u0643 \\u0628\\u0641\\u0636\\u0644 \\u0645\\u0646 \\u0627\\u0644\\u0644\\u0647 \\u0648\\u0627\\u0644\\u0643\\u0648\\u0627\\u0627\\u062f\\u0631 \\u0627\\u0644\\u0647\\u0646\\u062f\\u0633\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0641\\u0646\\u064a\\u0629 \\u0627\\u0644\\u0645\\u0624\\u0647\\u0644\\u0629 \\u062a\\u0623\\u0647\\u064a\\u0644 \\u0639\\u0627\\u0644\\u064a \\u0645\\u0645\\u0627 \\u0633\\u0627\\u0639\\u062f \\u0641\\u064a \\u0627\\u0646\\u062c\\u0627\\u0632 \\u0627\\u0649 \\u0627\\u0639\\u0645\\u0627\\u0644 \\u062a\\u0645 \\u0627\\u0633\\u0646\\u0627\\u062f\\u0647\\u0627 \\u0644\\u0646\\u0627 \\u0648\\u0639\\u0645\\u0644\\u0646\\u0627 \\u0628\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629 \\u0645\\u0639 \\u0643\\u0627\\u0641\\u0629 \\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u0627\\u0633\\u0645\\u0646\\u062a \\u0627\\u0644\\u0645\\u062d\\u0644\\u064a\\u0629",""],"slider1":{"img_ids":[270,271,272],"other_fields":{"index_top_slider_header":["are you searching a tours","are you searching a tours","are you searching a tours",""],"index_top_slider_text":["Contact us Now and we will provide all information you ask about","Contact us Now and we will provide all information you ask about","Contact us Now and we will provide all information you ask about",""]}}}', '0000-00-00 00:00:00', NULL, NULL),
(38, 'edit_footer', '{"copy_right":"Designed&Developed By \\r\\n<a href=\\"http:\\/\\/www.seoera.net\\/\\">Seoera<\\/a>","footer_contact_us_header":"Contact Us:","footer_contact_us_text":"we are so fast in reply","footer_contact_us_email_placeholder":"Your Email","footer_contact_us_message_placeholder":"Your Message","footer_contact_us_btn_text":"Send","footer_menu":"9","img_ids":{"footer_bg":274},"contact_icon":["fa fa-phone","fa fa-fax","fa fa-envelope","fa fa-map-marker"],"contact_data":["0123 456 789","002135 487 9887","info@visitkosovo.co"," 4 St. abd Elnasser  - Cairo - Egypt",""]}', NULL, NULL, NULL),
(39, 'sidebar', '{"sidebar_img_header":"Visit Kosovo\\r\\n","sidebar_img_link":"#","follow_us_header":"Follow us\\r\\n","news_litter_header":"News Litter\\r\\n","subscribe_label":"Subscribe To our News Letter","subscribe_placeholder":"Add Your Email","subscribe_btn_icon":"fa fa-envelope","subscribe_text":"Subscribe now to get all our offers and information about Kosovo\\r\\n","img_ids":{"sidebar_img":276},"slider1":{"img_ids":[277,278,279,280,281],"other_fields":{"social_link_title":["Facebook","Google Plus","Inistagram","Twitter","Youtube",""],"social_link_href":["#","#","#","#","#",""]}}}', NULL, NULL, NULL),
(40, 'video_gallery', '{"meta_title":"Video Gallery ","meta_desc":"Video Gallery ","meta_keywords":"Video Gallery ","breadcrumb_header":"Video Gallery ","bg_img_header":"Video Gallery ","bg_img_icon":"fa fa-video-camera","bg_img_text":"Kosovo Videos","img_ids":{"bg_img":292},"video_title":["Visit Kosovo","Know about Kosovo","Be in Kosovo","Kosova is a beautiful place","Places to visit in Kosovo"],"video_link":["https:\\/\\/www.youtube.com\\/watch?v=McFKtbNs1po&t=76s","https:\\/\\/www.youtube.com\\/watch?v=TrZwJFBo_Qs","https:\\/\\/www.youtube.com\\/watch?v=nXMfPBelbvU","https:\\/\\/www.youtube.com\\/watch?v=xMdcmHMe0uc","https:\\/\\/www.youtube.com\\/watch?v=zEXLAxAH7VU",""]}', NULL, NULL, NULL),
(41, 'trip_booking', '{"meta_title":"Book Your Trip\\r\\n","meta_desc":"Book Your Trip","meta_keywords":"Book Your Trip","breadcrumb_header":"Book Your Trip","bg_img_header":"Book Your Trip","bg_img_icon":"fa fa-bookmark","bg_img_text":"Book Your Trip with us","header":"Book Your Trip\\r\\n","body":"Book your trip now and get our best offer and Kosovo best rates","form_header":"Book Now\\r\\n","form_full_name":"Full Name","form_email":"Email","form_nationality":"Nationality","form_phone":"Phone","form_country_code":"Country Code","form_mobile":"Mobile","form_airport":"Airport","form_flight_number":"Flight Number","form_arrival_date":"Arrival Date\\r\\n","form_departure_date":"Departure Date","form_stay_duration":"Stay Duration","form_people_number":"People Number","form_deliver_from_airport":"Deliver From Airport","form_btn_text":"Book","img_ids":{"bg_img":298}}', NULL, NULL, NULL),
(42, 'search_page', '{"meta_title":"Search ","meta_desc":"Search Search ","meta_keywords":"Search ","breadcrumb_header":"Search ","bg_img_header":"Search ","bg_img_icon":"fa fa-search","bg_img_text":"Search in KOSOVO","img_ids":{"bg_img":300}}', NULL, NULL, NULL),
(43, 'edit_header', '{"search_placeholder":"\\u0628\\u062d\\u062b","header_menu":"9","img_ids":{"logo":309,"icon":310,"header_bg":311},"social_link_icon":["fa fa-facebook","fa fa-google-plus","fa fa-twitter","fa fa-youtube","fa fa-instagram"],"social_link_href":["#","#","#","#","#",""],"contact_icon":["fa fa-envelope","fa fa-phone"],"contact_title":["\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","\\u0644\\u0644\\u0627\\u062a\\u0635\\u0627\\u0644 \\u0648\\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644",""],"contact_data":["info@visitkosovo.co","00966 56 254",""]}', NULL, NULL, NULL),
(44, 'support', '{"meta_title":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","meta_desc":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","meta_keywords":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","contact_us_bg_img_header":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","contact_us_bg_img_icon":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","contact_us_bg_img_text":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","breadcrumb_header":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","header":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","contact_us_body":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","form_header":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","form_fullname":"\\u0627\\u0644\\u0627\\u0633\\u0645","form_email":"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","form_btn_text":"\\u0627\\u0631\\u0633\\u0627\\u0644","form_sucess_msg":"\\u062a\\u0645 \\u0627\\u0631\\u0633\\u0627\\u0644 \\u0627\\u0644\\u0631\\u0633\\u0627\\u0644\\u0629 \\u0628\\u0646\\u062c\\u0627\\u062d \\u0648\\u0633\\u0648\\u0641 \\u064a\\u062a\\u0645 \\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644 \\u0645\\u0639\\u0643 \\u062e\\u0644\\u0627\\u0644 24 \\u0633\\u0627\\u0639\\u0647 \\u0627\\u0648 \\u0627\\u0642\\u0644","img_ids":{"contact_us_bg_img":302},"label":[],"value":[""]}', NULL, NULL, NULL),
(45, 'gallery', '{"meta_title":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","meta_desc":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","meta_keywords":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","breadcrumb_header":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","bg_img_header":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","bg_img_icon":"\\u0627\\u0644\\u0628\\u0648\\u0645 \\u0627\\u0644\\u0635\\u0648\\u0631","bg_img_text":"\\u0635\\u0648\\u0631 \\u0627\\u0644\\u0645\\u0639\\u0627\\u0644\\u0645 \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","img_ids":{"bg_img":303},"slider1":{"img_ids":[326,327,328,329,330,331,332,333,334,335,336,337]}}', NULL, NULL, NULL),
(46, 'search_page', '{"meta_title":"\\u0628\\u062d\\u062b","meta_desc":"\\u0628\\u062d\\u062b","meta_keywords":"\\u0628\\u062d\\u062b","breadcrumb_header":"\\u0628\\u062d\\u062b","bg_img_header":"\\u0628\\u062d\\u062b","bg_img_icon":"\\u0628\\u062d\\u062b","bg_img_text":"\\u0628\\u062d\\u062b","img_ids":{"bg_img":307}}', NULL, NULL, NULL),
(47, 'keywords', '{"homepage":"\\u0627\\u0644\\u0635\\u0641\\u062d\\u0629 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629","more":"\\u0627\\u0644\\u0645\\u0632\\u064a\\u062f","img_ids":[]}', NULL, NULL, NULL),
(48, 'email_page', '{"copyright":"","img_ids":{"logo_img":0},"social_imgs":[],"social_links":[]}', NULL, NULL, NULL),
(49, 'all_services', '{"meta_title":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627","meta_desc":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627","meta_keywords":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627","header":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627","info":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627 1","body":"<p>\\u062a\\u0639\\u0631\\u0641 \\u0639\\u0644\\u0649 \\u062e\\u062f\\u0645\\u0627\\u062a<\\/p>\\r\\n","img_ids":{"cover":304}}', NULL, NULL, NULL),
(50, 'edit_footer', '{"copy_right":"\\u062a\\u0635\\u0645\\u064a\\u0645 \\u0648\\u0628\\u0631\\u0645\\u062c\\u0629\\r\\n<a href=\\"http:\\/\\/www.seoera.net\\/\\">Seoera<\\/a>","footer_contact_us_header":"\\u062a\\u0648\\u0627\\u0635\\u0644 \\u0645\\u0639\\u0646\\u0627","footer_contact_us_text":"\\u064a\\u0633\\u0639\\u062f\\u0646\\u0627 \\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644 \\u0645\\u0639\\u0643\\u0645","footer_contact_us_email_placeholder":"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","footer_contact_us_message_placeholder":"\\u0631\\u0633\\u0627\\u0644\\u062a\\u0643 \\u0627\\u0648 \\u0627\\u0633\\u062a\\u0641\\u0633\\u0627\\u0631\\u0643","footer_contact_us_btn_text":"\\u0627\\u0631\\u0633\\u0627\\u0644","footer_menu":"9","img_ids":{"footer_bg":312},"contact_icon":["fa fa-phone","fa fa-fax","fa fa-envelope","fa fa-map-marker"],"contact_data":["0123 456 789","002135 487 9887","info@visitkosovo.co","\\u0627\\u0644\\u0645\\u0645\\u0644\\u0643\\u0629 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0633\\u0639\\u0648\\u062f\\u064a\\u0629",""]}', NULL, NULL, NULL),
(51, 'edit_index_page', '{"meta_title":"\\u0627\\u0644\\u0635\\u0641\\u062d\\u0629 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629","meta_desc":"\\u0627\\u0644\\u0635\\u0641\\u062d\\u0629 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629","meta_keywords":"\\u0627\\u0644\\u0635\\u0641\\u062d\\u0629 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629","art_sec_1_header":"\\u0645\\u0631\\u062d\\u0628\\u0627 \\u0628\\u0643\\u0645 \\u0641\\u0649 \\u0645\\u0648\\u0642\\u0639 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_1_text":"\\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a \\u0627\\u0644\\u0627\\u0648\\u0644 \\u0627\\u0644\\u0645\\u062e\\u062a\\u0635 \\u0641\\u0649 \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648\\u060c \\u0627\\u0641\\u0636\\u0644 \\u0627\\u0644\\u0645\\u0639\\u0644\\u0648\\u0645\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u0648\\u0627\\u0644\\u0627\\u0645\\u0627\\u0643\\u0646 \\u0648\\u0627\\u0644\\u0645\\u0632\\u0627\\u0631\\u0627\\u062a","index_about_section_header":"\\u0645\\u0631\\u062d\\u0628\\u0627 \\u0628\\u0643\\u0645 \\u0641\\u0649 \\u0645\\u0648\\u0642\\u0639 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","index_about_section_text":"\\u0634\\u0631\\u0643\\u0629 \\u0627\\u064a\\u0631\\u0645\\u0627 \\u0627\\u0644\\u062f\\u0648\\u0644\\u064a\\u0629 \\u0644\\u0644\\u0633\\u0641\\u0631 \\u0648 \\u0627\\u0644\\u062e\\u062f\\u0645\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u060c \\u0647\\u064a \\u0627\\u062d\\u062f\\u0649 \\u0627\\u0644\\u0634\\u0631\\u0643\\u0627\\u062a \\u0627\\u0644\\u062f\\u0648\\u0644\\u064a\\u0629 \\u0627\\u0644\\u0639\\u0627\\u0645\\u0644\\u0629 \\u0641\\u064a \\u062d\\u0642\\u0644 \\u0627\\u0644\\u0633\\u0641\\u0631 \\u0648\\u0627\\u0644\\u062e\\u062f\\u0645\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u0641\\u064a \\u062f\\u0648\\u0644\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648 \\u0648\\u062c\\u0645\\u064a\\u0639 \\u062f\\u0648\\u0644 \\u0627\\u0644\\u0628\\u0644\\u0642\\u0627\\u0646 \\u060c \\u062d\\u064a\\u062b \\u064a\\u0642\\u0639 \\u0627\\u0644\\u0645\\u0642\\u0631 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a \\u0644\\u0644\\u0634\\u0631\\u0643\\u0629 \\u0641\\u064a \\u0627\\u0644\\u0639\\u0627\\u0635\\u0645\\u0629 \\u0628\\u0631\\u0634\\u062a\\u064a\\u0646\\u0627 \\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0643\\u0648\\u0627\\u062f\\u0631 \\u0645\\u0624\\u0647\\u0644\\u0629 \\u0641\\u064a \\u0645\\u062c\\u0627\\u0644 \\u0627\\u0644\\u062e\\u062f\\u0645\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u0644\\u0643\\u0627\\u0641\\u0629 \\u0639\\u0645\\u0644\\u0627\\u0626\\u0646\\u0627 \\u0645\\u0646 \\u062f\\u0648\\u0644 \\u0627\\u0644\\u0639\\u0627\\u0644\\u0645 \\u0627\\u0644\\u0630\\u064a\\u0646 \\u064a\\u0631\\u063a\\u0628\\u0648\\u0646 \\u0628\\u0642\\u0636\\u0627\\u0621 \\u0625\\u062c\\u0627\\u0632\\u0627\\u062a\\u0647\\u0645 \\u0641\\u064a \\u0631\\u0628\\u0648\\u0639 \\u062f\\u0648\\u0644\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648 \\u0648 \\u0627\\u0644\\u062a\\u0639\\u0631\\u0641 \\u0639\\u0644\\u0649 \\u0647\\u0630\\u0647 \\u0627\\u0644\\u0628\\u0644\\u062f \\u0627\\u0644\\u0645\\u0636\\u064a\\u0627\\u0641 \\u0627\\u0644\\u0630\\u064a \\u064a\\u0645\\u062a\\u0627\\u0632 \\u0634\\u0639\\u0628\\u0629 \\u0628\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0636\\u064a\\u0627\\u0641\\u0629 \\u0648\\u0627\\u0644\\u0637\\u0628\\u064a\\u0639\\u0629 \\u0627\\u0644\\u062e\\u0644\\u0627\\u0628\\u0629 \\u0648\\u0627\\u0644\\u0627\\u0645\\u0627\\u0643\\u0646 \\u0627\\u0644\\u062a\\u0627\\u0631\\u064a\\u062e\\u064a\\u0629 \\u0627\\u0648 \\u0645\\u0646 \\u062e\\u0644\\u0627\\u0644 \\u0641\\u0631\\u0648\\u0646\\u0627 \\u0641\\u064a \\u062f\\u0648\\u0644 \\u0645\\u062c\\u0644\\u0633 \\u0627\\u0644\\u062a\\u0639\\u0627\\u0648\\u0646 \\u0627\\u0644\\u062e\\u0644\\u064a\\u062c\\u064a \\u0648\\u0627\\u0644\\u062f\\u0648\\u0644 \\u0627\\u0644\\u0639\\u0631\\u0628\\u064a\\u0629 \\u0627\\u0644\\u0623\\u062e\\u0631\\u0649 .","index_about_section_btn_1_text":"\\u0627\\u0642\\u0631\\u0623 \\u0627\\u0644\\u0645\\u0632\\u064a\\u062f","index_about_section_btn_1_href":"http:\\/\\/www.visitkosovo.co\\/ar\\/Aboutus","index_about_section_btn_2_text":"\\u062e\\u062f\\u0645\\u0627\\u062a\\u0646\\u0627","index_about_section_btn_2_href":"http:\\/\\/www.visitkosovo.co\\/ar\\/Services-ar","art_sec_2_header":"\\u0632\\u064a\\u0627\\u0631\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_2_text":"\\u0627\\u0647\\u0644\\u0627 \\u0628\\u0643\\u0645 \\u0641\\u0649 \\u0645\\u0648\\u0642\\u0639 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648\\u060c \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0627\\u0648\\u0644 \\u0639\\u0631\\u0628\\u064a\\u0627 \\u0644\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_3_header":"\\u0632\\u064a\\u0627\\u0631\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_3_text":"\\u0627\\u0647\\u0644\\u0627 \\u0628\\u0643\\u0645 \\u0641\\u0649 \\u0645\\u0648\\u0642\\u0639 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648\\u060c \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0627\\u0648\\u0644 \\u0639\\u0631\\u0628\\u064a\\u0627 \\u0644\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_3_embed_youtube_link":"https:\\/\\/www.youtube.com\\/watch?v=McFKtbNs1po","art_sec_4_header":"\\u0632\\u064a\\u0627\\u0631\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","art_sec_4_text":"\\u0627\\u0647\\u0644\\u0627 \\u0628\\u0643\\u0645 \\u0641\\u0649 \\u0645\\u0648\\u0642\\u0639 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648\\u060c \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0627\\u0648\\u0644 \\u0639\\u0631\\u0628\\u064a\\u0627 \\u0644\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","img_ids":{"sec_3_bg_img":308},"about_icon":[],"about_header":[""],"about_paragraph":[""],"slider1":{"img_ids":[313,344,345],"other_fields":{"index_top_slider_header":["\\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0631\\u062d\\u0644\\u0627\\u062a \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0627\\u0641\\u0636\\u0644 \\u0627\\u0644\\u0645\\u0632\\u0627\\u0631\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629"],"index_top_slider_text":["\\u0632\\u064a\\u0627\\u0631\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0631\\u062d\\u0644\\u0627\\u062a \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0627\\u0641\\u0636\\u0644 \\u0627\\u0644\\u0645\\u0632\\u0627\\u0631\\u0627\\u062a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629"]}}}', NULL, NULL, NULL),
(52, 'sidebar', '{"sidebar_img_header":"\\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","sidebar_img_link":"#","follow_us_header":"\\u062a\\u0627\\u0628\\u0639\\u0646\\u0627","news_litter_header":"\\u0627\\u0644\\u0642\\u0627\\u0626\\u0645\\u0629 \\u0627\\u0644\\u0628\\u0631\\u064a\\u062f\\u064a\\u0629","subscribe_label":"\\u0627\\u0634\\u062a\\u0631\\u0643 \\u0627\\u0644\\u0627\\u0646","subscribe_placeholder":"\\u0642\\u0645 \\u0628\\u0648\\u0636\\u0639 \\u0628\\u0631\\u064a\\u062f\\u0643 \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","subscribe_btn_icon":"fa fa-envelope","subscribe_text":"\\u0644\\u064a\\u0635\\u0644\\u0643 \\u0643\\u0644 \\u062c\\u062f\\u064a\\u062f \\u0639\\u0646 \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u0629 \\u0641\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648 \\u060c \\u0642\\u0645 \\u0628\\u0648\\u0636\\u0639 \\u0628\\u0631\\u064a\\u062f\\u0643 \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a \\u0644\\u0644\\u0627\\u0634\\u062a\\u0631\\u0627\\u0643 \\u0641\\u0649 \\u0627\\u0644\\u0642\\u0627\\u0626\\u0645\\u0629 \\u0627\\u0644\\u0628\\u0631\\u064a\\u062f\\u064a\\u0629","img_ids":{"sidebar_img":338},"slider1":{"img_ids":[339,340,341,342,343],"other_fields":{"social_link_title":["\\u0641\\u064a\\u0633 \\u0628\\u0648\\u0643","\\u062c\\u0648\\u062c\\u0644 \\u0628\\u0627\\u0633","\\u064a\\u0648\\u062a\\u064a\\u0648\\u0628","\\u0627\\u0646\\u0633\\u062a\\u0642\\u0631\\u0627\\u0645","\\u062a\\u0648\\u064a\\u062a\\u0631"],"social_link_href":["#","#","#","#","#"]}}}', NULL, NULL, NULL),
(53, 'video_gallery', '{"meta_title":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","meta_desc":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","meta_keywords":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","breadcrumb_header":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","bg_img_header":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","bg_img_icon":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","bg_img_text":"\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648\\u0647\\u0627\\u062a","img_ids":{"bg_img":305},"video_title":["\\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u062a\\u0639\\u0631\\u0641 \\u0639\\u0644\\u0649 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0632\\u064a\\u0627\\u0631\\u0629 \\u0643\\u0648\\u0633\\u0648\\u0641\\u0648","\\u0643\\u0648\\u0633\\u0648\\u0641\\u0648\\u060c \\u0627\\u0644\\u0645\\u0643\\u0627\\u0646 \\u0627\\u0644\\u0631\\u0627\\u0626\\u0639","\\u0627\\u0641\\u0636\\u0644 \\u0627\\u0644\\u0627\\u0645\\u0627\\u0643\\u0646 \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\\u0629 \\u0628\\u0643\\u0633\\u0648\\u0641\\u0648"],"video_link":["https:\\/\\/www.youtube.com\\/watch?v=McFKtbNs1po&t=76s","https:\\/\\/www.youtube.com\\/watch?v=TrZwJFBo_Qs","https:\\/\\/www.youtube.com\\/watch?v=nXMfPBelbvU","https:\\/\\/www.youtube.com\\/watch?v=xMdcmHMe0uc","https:\\/\\/www.youtube.com\\/watch?v=zEXLAxAH7VU"]}', NULL, NULL, NULL),
(54, 'trip_booking', '{"meta_title":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","meta_desc":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","meta_keywords":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","breadcrumb_header":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","bg_img_header":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","bg_img_icon":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","bg_img_text":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","header":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","body":"\\u0642\\u0645 \\u0628\\u0645\\u0644\\u0649\\u0621 \\u0627\\u0644\\u0646\\u0645\\u0648\\u0630\\u062c \\u0627\\u0644\\u062a\\u0627\\u0644\\u064a \\u0648\\u0633\\u0646\\u0642\\u0648\\u0645 \\u0628\\u0627\\u0644\\u0631\\u062f \\u0639\\u0644\\u064a\\u0643 \\u0628\\u0627\\u0644\\u0631\\u062d\\u0644\\u0629 \\u0648\\u0627\\u0644\\u062a\\u0641\\u0627\\u0635\\u064a\\u0644 \\u0648\\u0643\\u064a\\u0641\\u064a\\u0629 \\u0627\\u0644\\u062d\\u062c\\u0632","form_header":"\\u062d\\u062c\\u0632 \\u0627\\u0644\\u0631\\u062d\\u0644\\u0627\\u062a","form_full_name":"\\u0627\\u0644\\u0623\\u0633\\u0645","form_email":"\\u0627\\u0644\\u0628\\u0631\\u064a\\u062f \\u0627\\u0644\\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a","form_nationality":"\\u0627\\u0644\\u062c\\u0646\\u0633\\u064a\\u0629","form_phone":"\\u0631\\u0642\\u0645 \\u0627\\u0644\\u0647\\u0627\\u062a\\u0641","form_country_code":"\\u0643\\u0648\\u062f","form_mobile":"\\u0627\\u0644\\u062c\\u0648\\u0627\\u0644","form_airport":"\\u0627\\u0644\\u0645\\u0637\\u0627\\u0631","form_flight_number":"\\u0631\\u0642\\u0645 \\u0631\\u062d\\u0644\\u0629 \\u0627\\u0644\\u0637\\u064a\\u0631\\u0627\\u0646","form_arrival_date":"\\u062a\\u0627\\u0631\\u064a\\u062e \\u0627\\u0644\\u0648\\u0635\\u0648\\u0644","form_departure_date":"\\u062a\\u0627\\u0631\\u064a\\u062e \\u0627\\u0644\\u0645\\u063a\\u0627\\u062f\\u0631\\u0629","form_stay_duration":"\\u0639\\u062f\\u062f \\u0627\\u0644\\u0627\\u064a\\u0627\\u0645","form_people_number":"\\u0639\\u062f\\u062f \\u0627\\u0644\\u0627\\u0634\\u062e\\u0627\\u0635","form_deliver_from_airport":"\\u0627\\u0644\\u0627\\u0646\\u062a\\u0642\\u0627\\u0644\\u0627\\u062a","form_btn_text":"\\u0627\\u0631\\u0633\\u0627\\u0644","img_ids":{"bg_img":306}}', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_tracking`
--

CREATE TABLE IF NOT EXISTS `site_tracking` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `action_desc` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`track_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(200) NOT NULL,
  `status_label` varchar(100) NOT NULL,
  `user_type` varchar(200) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`, `status_label`, `user_type`) VALUES
(1, 'pending', 'لم تستلم', 'general'),
(2, 'waiting', 'قيد الانتظار', 'admin'),
(3, 'sending', 'ارسال', 'admin'),
(4, 'received', 'استلام', 'branch'),
(5, 'cancelled', 'الغاء', 'branch'),
(6, 'refused', 'رفض', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers_materials`
--

CREATE TABLE IF NOT EXISTS `suppliers_materials` (
  `sup_mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_bill_id` int(11) NOT NULL,
  `sup_mat_user_id` int(10) unsigned NOT NULL,
  `mat_id` int(11) NOT NULL,
  `sup_mat_amount` decimal(10,2) NOT NULL,
  `sup_mat_price` decimal(10,2) NOT NULL,
  `sup_mat_desc` text NOT NULL,
  `sup_mat_return` tinyint(1) NOT NULL COMMENT 'المرتجع لو 1',
  `sup_mat_barcode` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sup_mat_id`),
  KEY `supplier_bill_id` (`supplier_bill_id`),
  KEY `sup_mat_user_id` (`sup_mat_user_id`),
  KEY `mat_id` (`mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_bills`
--

CREATE TABLE IF NOT EXISTS `supplier_bills` (
  `supplier_bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) unsigned NOT NULL,
  `supplier_bill_total_amount` decimal(10,2) NOT NULL,
  `supplier_bill_extra_cost` decimal(10,2) NOT NULL,
  `supplier_bill_total_paid` decimal(10,2) NOT NULL,
  `supplier_bill_total_paid_in_cash` decimal(10,2) NOT NULL,
  `supplier_bill_total_paid_in_atm` decimal(10,2) NOT NULL,
  `supplier_bill_total_return_money` decimal(10,2) NOT NULL,
  `supplier_bill_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supplier_bill_id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `logo_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_code` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `full_name_en` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `start_join_date` date NOT NULL,
  `role` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `point_of_sale_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_active` tinyint(1) NOT NULL,
  `user_can_login` tinyint(1) NOT NULL,
  `contacts` text COLLATE utf8_unicode_ci NOT NULL,
  `user_balance` decimal(12,2) NOT NULL,
  `user_commission_days` int(11) NOT NULL,
  `user_atm_balance` decimal(12,2) NOT NULL,
  `user_duty_balance` decimal(10,2) NOT NULL,
  `user_tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_mobile` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_fax` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `bank_id` int(11) NOT NULL,
  `selected_price_id` int(11) NOT NULL,
  `age_range` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_gender` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_login_date` datetime NOT NULL,
  `how_customer_know_us` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `identification_number` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `identification_expire_date` date NOT NULL,
  `user_job_on_passport` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `user_job_on_company` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `commercial_record_number` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `commercial_record_expire_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_tel` (`user_tel`),
  KEY `bank_id` (`bank_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `logo_id`, `email`, `user_code`, `full_name`, `full_name_en`, `start_join_date`, `role`, `user_type`, `related_id`, `point_of_sale_id`, `password`, `remember_token`, `user_active`, `user_can_login`, `contacts`, `user_balance`, `user_commission_days`, `user_atm_balance`, `user_duty_balance`, `user_tel`, `user_mobile`, `user_fax`, `user_address`, `bank_id`, `selected_price_id`, `age_range`, `user_gender`, `last_login_date`, `how_customer_know_us`, `identification_number`, `nationality`, `identification_expire_date`, `user_job_on_passport`, `user_job_on_company`, `commercial_record_number`, `commercial_record_expire_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'admin@admin.com', '', 'Admin', '', '0000-00-00', 'Administrator', 'dev', 0, 0, '$2y$10$sFCb1K2R2yJjT6MAW8vdg.79L6ID65TElao5L91uH4M2nF/uwH3si', 'T4QQQuxuKM5BiN8ZMcBBukiASEqLS5sbtKNc4Rj0AKuuhrJWt7DXiJOX9wAB', 1, 1, '', '0.00', 0, '0.00', '0.00', '', '', '', '', 0, 0, '', '', '2017-06-05 16:07:45', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '2016-07-28 08:51:57', '2017-06-05 23:07:45', NULL),
(2, 49, 'tarek_asfour464@hotmail.com', '', 'الاداره والمصنع', 'الاداره والمصنع', '0000-00-00', 'Administrator', 'admin', 0, 0, '$2y$10$zHQnXCPVgMc2pUVWbbgeI.02rL4mnNcBfBArBUTj9j9Nrtt61HZEW', 'HCpwjA7A0mYuFsvxb0e5RZlB72Vjop6XbwpeL5yEHBhCOvfOrPHcHxMpbmIX', 1, 1, '', '-900.00', 0, '-2050.00', '1200.00', '', '', '', '', 0, 0, '', '', '2017-06-05 14:44:17', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '2016-07-28 08:51:57', '2017-06-05 21:44:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_transits`
--

CREATE TABLE IF NOT EXISTS `users_transits` (
  `user_transit_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `from_branch` int(11) NOT NULL,
  `to_branch` int(11) NOT NULL,
  `user_transit_date` date NOT NULL,
  `user_transit_reason` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_transit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_as_sponsor`
--

CREATE TABLE IF NOT EXISTS `user_as_sponsor` (
  `uas_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `last_earn_gift_date` date NOT NULL,
  PRIMARY KEY (`uas_id`),
  KEY `user_id` (`user_id`),
  KEY `sponsor_id` (`sponsor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

CREATE TABLE IF NOT EXISTS `user_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(10) unsigned NOT NULL,
  `to_user_id` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
