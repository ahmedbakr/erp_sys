-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 10:31 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `day_commission`
--

CREATE TABLE IF NOT EXISTS `day_commission` (
  `day_commission_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  `day_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `commission_value` decimal(10,2) NOT NULL,
  `total_day_bills_money` decimal(10,2) NOT NULL,
  `is_received` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `day_commission`
--

INSERT INTO `day_commission` (`day_commission_id`, `commission_id`, `day_date`, `user_id`, `commission_value`, `total_day_bills_money`, `is_received`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 2, '2017-02-19', 9, '30.00', '900.00', 0, '2017-03-06 13:43:47', '2017-03-06 13:43:47', '2017-06-03 22:00:00'),
(11, 1, '2017-03-06', 3, '10.00', '260.00', 0, '2017-03-07 12:42:11', '2017-06-01 11:06:55', '2017-06-03 22:00:00'),
(13, 1, '2017-05-01', 3, '10.00', '327.00', 0, '2017-06-04 08:55:09', '2017-06-04 08:55:09', '2017-06-03 22:00:00'),
(21, 1, '2017-06-04', 18, '10.00', '280.00', 1, '2017-06-05 09:52:40', '2017-06-05 10:26:20', NULL),
(22, 3, '2017-06-05', 18, '6.00', '140.00', 0, '2017-06-05 09:54:16', '2017-06-05 10:33:05', '2017-06-05 12:33:05'),
(23, 3, '2017-06-05', 18, '6.00', '149.00', 0, '2017-06-05 12:50:40', '2017-06-05 12:50:40', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `day_commission`
--
ALTER TABLE `day_commission`
  ADD PRIMARY KEY (`day_commission_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `day_commission`
--
ALTER TABLE `day_commission`
  MODIFY `day_commission_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
