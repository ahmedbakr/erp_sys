$(function(){

    var base_url2 = $(".url_class").val();
    var base_url = base_url2 + "/public_html/";
    var _token = $(".csrf_input_class").val();
    var ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";
    var ajax_loader_img_func=function(img_width){
        return "<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' style='width:"+img_width+";height:"+img_width+";'>";
    };


    // invoices templates

    $( ".invoice_item" ).draggable({
        cursor: "move",
        containment: ".invoice-box-container",
        scroll: false
    });

    $( ".invoice_item" ).resizable({
        helper: "ui-resizable-helper"
    });


    // start custom for logo image
    $( ".logo_invoice_item" ).draggable({
        cursor: "move",
        containment: ".invoice-box-container",
        scroll: false
    });

    $( ".logo_resize_item" ).resizable({
        helper: "ui-resizable-helper"
    });

    // end custom for logo image


    if($( ".resize_invoice_box" ).length > 0)
    {
        $( ".resize_invoice_box" ).resizable({
            helper: "ui-resizable-helper"
        });
    }



    $('.save_template_html').click(function ()
    {

        var this_element = $(this);
        var inv_temp_id = this_element.attr("data-inv_temp_id");
        if(inv_temp_id > 0)
        {
            this_element.attr("disabled","disabled");
            this_element.append(" ".ajax_loader_img);

            var send_data = {};
            send_data._token = _token;
            send_data.inv_temp_id = inv_temp_id;

            // for image
            if($('.logo_invoice_item').length > 0)
            {
                var img_obj = {};
                var item_id = $('.logo_invoice_item').attr("data-item_id");
                img_obj.top = $('.logo_invoice_item').css("top");
                img_obj.left = $('.logo_invoice_item').css("left");
                img_obj.width = $('.logo_resize_item').css("width");
                img_obj.height = $('.logo_resize_item').css("height");
                send_data[item_id] = img_obj;
            }

            var other_items = $('.invoice_item');

            $.each(other_items,function (ind,val)
            {

                var img_obj = {};
                var item_id = $(this).attr("data-item_id");
                img_obj.top = $(this).css("top");
                img_obj.left = $(this).css("left");
                img_obj.width = $(this).css("width");
                img_obj.height = $(this).css("height");
                if($(this).hasClass('format_table'))
                {
                    send_data.table = img_obj;
                }
                else{
                    send_data[item_id] = img_obj;
                }

            });

            console.log(send_data);
            
            $.ajax({
                url: base_url2 + '/admin/invoices_templates/save_template_items_position',
                type: 'POST',
                data: send_data,
                success: function (data) {
                    $(".ajax_loader_class").hide();
                    this_element.removeAttr("disabled");
                    var json_data = JSON.parse(data);

                    if (typeof (json_data) != "undefiend") {
                        if (typeof (json_data.success) != "undefined" && json_data.success == "success") {
                            window.location.reload();
                        }

                        if (typeof (json_data.error) != "undefined" && json_data.error != "") {
                            this_element.parents.find('.show_template_errors').html(json_data.error);
                        }
                    }
                }
            });

        }

        return false;
    });


    $('.fire_print_event').click(function () {
        window.print();
    });

});