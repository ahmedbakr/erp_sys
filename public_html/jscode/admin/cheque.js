$(function(){

    var base_url2 = $(".url_class").val();
    var base_url = base_url2 + "/public_html/";
    var _token = $(".csrf_input_class").val();
    var ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";
    var ajax_loader_img_func=function(img_width){
        return "<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' style='width:"+img_width+";height:"+img_width+";'>";
    };


    // cheque templates

    $( ".cheque_item" ).draggable({
        cursor: "move",
        containment: ".cheque-box-container",
        scroll: false
    });

    $( ".cheque_item" ).resizable({
        helper: "ui-resizable-helper"
    });


    $('.save_cheque_template_html').click(function ()
    {

        var this_element = $(this);
        var cheque_template_id = this_element.attr("data-cheque_template_id");
        if(cheque_template_id > 0)
        {
            this_element.attr("disabled","disabled");
            this_element.append(" ".ajax_loader_img);

            var send_data = {};
            send_data._token = _token;
            send_data.cheque_template_id = cheque_template_id;

            var other_items = $('.cheque_item');

            $.each(other_items,function (ind,val)
            {

                var img_obj = {};
                var item_id = $(this).attr("data-item_id");
                img_obj.top = $(this).css("top");
                img_obj.left = $(this).css("left");
                img_obj.width = $(this).css("width");
                img_obj.height = $(this).css("height");
                if($(this).hasClass('format_table'))
                {
                    send_data.table = img_obj;
                }
                else{
                    send_data[item_id] = img_obj;
                }

            });

            console.log(send_data);

            $.ajax({
                url: base_url2 + '/admin/cheques/save_template_items_position',
                type: 'POST',
                data: send_data,
                success: function (data) {
                    $(".ajax_loader_class").hide();
                    this_element.removeAttr("disabled");
                    var json_data = JSON.parse(data);

                    if (typeof (json_data) != "undefiend") {
                        if (typeof (json_data.success) != "undefined" && json_data.success == "success") {
                            window.location.reload();
                        }

                        if (typeof (json_data.error) != "undefined" && json_data.error != "") {
                            this_element.parents.find('.show_cheque_template_errors').html(json_data.error);
                        }
                    }
                }
            });

        }

        return false;
    });


});