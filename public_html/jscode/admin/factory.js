$(function () {

    // var base_url2 = $(".url_class").val();
    // var base_url = base_url2 + "/public_html/";
    // var _token = $(".csrf_input_class").val();
    // var ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";
    // var ajax_loader_img_func=function(img_width){
    //     return "<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' style='width:"+img_width+";height:"+img_width+";'>";
    // };


    // make collapse
    // $('body').on('click','.collapse_action',function(){
    //
    //     console.log("dada");
    //
    //     $(this).parent().find('.collapse_body').toggleClass('in');
    //
    //     return false
    // });

    //clone order item

    // $(".clone_order_item").click(function () {
    //
    //     var new_item = $(this).parents().find('.bill_order_item').first().clone();
    //     $(".bill_orders_containers").append(new_item);
    //
    //     return false;
    // });


    // calculate total bill amount
    $('body').on('click','.count_bill_orders',function(){
        var total_price = 0;

        $.each($(".add_bill_order_price"),function (ind,val) {
            var current_val = parseFloat($(this).val());
            if(current_val > 0)
            {
                total_price += (current_val * parseFloat($(".add_bill_order_quantity").eq(ind).val()));
            }
        });

        $(this).parents().find(".total_bill_amount").val(total_price);
        // $(this).parents().find(".total_bill_amount").attr("value",item_price + old_total);

        return false;
    });

    // show expenses data fields depend on checked
    $('.extra_costs').change(function(){

        var is_checked = $(this).is(":checked");
        if(is_checked)
        {
            $('.additional_extra_costs_fields').removeClass('hide_row');
        }
        else{
            $('.additional_extra_costs_fields').addClass('hide_row');
        }

        return false;
    });


    if(false){


        // search for bill and get orders
        $(".type_bill_number").keyup(function(event){

            if (event.which==13) {
                $(".get_bill_orders_submit").click();
            }

        });

        $('.get_bill_orders_submit').click(function ()
        {

            var this_element = $(this);
            var input_element = $(this).parents().find(".type_bill_number");
            var bill_id = input_element.val();

            if(bill_id == "")
            {
                input_element.css({"border": "2px solid red"});
            }
            else{
                input_element.css({"border": "2px solid green"});
                input_element.attr("disabled","disabled");
                this_element.append(ajax_loader_img);
                this_element.attr("disabled","disabled");

                $.ajax({
                    url: base_url2 + "/admin/branch_bills/get_bill_orders",
                    data: {"_token": _token, "bill_id": bill_id},
                    type: 'POST',
                    success: function (data) {
                        var return_data = JSON.parse(data);
                        $(".ajax_loader_class").remove();
                        this_element.removeAttr("disabled");
                        input_element.removeAttr("disabled");
                        this_element.parents().find('.bill_orders_containers').removeClass("hide_row");

                        if (typeof (return_data.success) != "undefined" && return_data.success == "error") {
                            this_element.parents().find('.form_submit_btns').addClass("hide_row");
                            this_element.parents().find('.bill_short_info').addClass("hide_row");
                            this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                            this_element.parents().find('.collapse_body').html(return_data.msg);
                        } else {
                            this_element.parents().find('.form_submit_btns').removeClass("hide_row");
                            this_element.parents().find('.bill_short_info').removeClass("hide_row");
                            this_element.parents().find('.bill_short_info').find('.content_body').html(return_data.bill_data);
                            this_element.parents().find('.bill_orders_containers').find('.collapse_body').html(return_data.table_data);

                            if (return_data.return_orders_table != "")
                            {
                                this_element.parents().find('.bill_returns_containers').removeClass("hide_row");
                                this_element.parents().find('.bill_returns_containers').find('.collapse_body').html(return_data.return_orders_table);
                            }
                            else{
                                this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                            }

                        }
                    }
                });

            }

            return false;
        });

        if($(".type_bill_number").val()>0){
            $('.get_bill_orders_submit').click();
        }

    }


    // delete order item but not first
    // $("body").on("click",".delete_order_item",function () {
    //
    //     var length = $(".delete_order_item").length;
    //     if (length > 1)
    //     {
    //         $res=confirm("you want delete item,sure?");
    //
    //         if($res){
    //             $(this).parents(".bill_order_item").remove();
    //         }
    //     }
    //     else{
    //         alert(" Not Allowed To Removed !! ");
    //     }
    //
    //
    //     return false;
    // });


    // custom clone order item for product_prices

    $(".custom_clone_order_item").click(function () {

        var new_item = $(this).parents().find('.bill_order_item').first().clone().appendTo(".bill_orders_containers").find('.new_pro_price_id').val("");

        return false;
    });



    //money transfer

    // get money transfers to specific supplier

    $(".get_transfers_bill_number").keyup(function(event){

        if (event.which==13) {
            $(".get_transfers").click();
        }

    });

    $('.get_transfers').click(function ()
    {

        var this_element = $(this);

        var supplier_element = $('.select_supplier');
        var bill_element = $('.get_transfers_bill_number');

        var supplier_element_val = $('.select_supplier').val();
        var bill_element_val = $('.get_transfers_bill_number').val();

        if(!(bill_element_val>0))
        {
            bill_element.css({"border": "2px solid red"});
        }
        else{
            bill_element.css({"border": "2px solid green"});
            supplier_element.css({"border": "2px solid green"});

            bill_element.attr("disabled","disabled");
            supplier_element.attr("disabled","disabled");

            this_element.append(ajax_loader_img);
            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/money_transfer/get_supplier_data",
                data: {"_token": _token, "user_id": supplier_element_val, "bill_id":bill_element_val},
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    $(".ajax_loader_class").remove();
                    this_element.removeAttr("disabled");
                    bill_element.removeAttr("disabled");
                    supplier_element.removeAttr("disabled");
                    $('.show_bill_transfers_body').html(return_data.msg);
                }
            });

        }

        return false;
    });

    if($("#user_id_id").val()>0&&$(".get_transfers_bill_number").val()>0){
        $(".get_transfers").click();
    }



    $("body").on("keyup",".money_transfer_cash,.money_transfer_atm",function () {

        var this_element=$(this);
        var parent_div=this_element.parents("td");
        var max_value=$(".max_value",parent_div).val();
        var cash_input=$(".money_transfer_cash",parent_div);
        var atm_input=$(".money_transfer_atm",parent_div);
        var make_transfer_money=$(".make_transfer_money",parent_div);
        var money_transfer_msgs=$(".money_transfer_msgs",parent_div);

        if(cash_input.val()==""){
            cash_input.val(0);
        }
        if(atm_input.val()==""){
            atm_input.val(0);
        }


        if(parseFloat(max_value)>=(parseFloat(cash_input.val())+parseFloat(atm_input.val()))){
            cash_input.css("border","1px solid #ccc");
            atm_input.css("border","1px solid #ccc");

            money_transfer_msgs.html("المتبقي"+(parseFloat(max_value)-(parseFloat(cash_input.val())+parseFloat(atm_input.val()))).toFixed(2));

            make_transfer_money.removeAttr("disabled");

        }
        else{
            cash_input.css("border","1px solid red");
            atm_input.css("border","1px solid red");

            money_transfer_msgs.html("لا يمكنك ان تكتب قيمة اكبر من "+max_value);

            make_transfer_money.attr("disabled","disabled");
        }


    });

    $("body").on("click",".make_transfer_money",function(){

        var this_element=$(this);
        var parent_div=this_element.parents("td");
        var max_value=$(".max_value",parent_div).val();
        var cash_input=$(".money_transfer_cash",parent_div);
        var atm_input=$(".money_transfer_atm",parent_div);
        var money_transfer_msgs=$(".money_transfer_msgs",parent_div);


        var object = {};
        object._token = _token;
        object.money_transfer_id=this_element.data("money_transfer_id");
        object.cash=cash_input.val();
        object.atm=atm_input.val();


        this_element.append(ajax_loader_img_func("15px"));

        this_element.attr("disabled","disabled");

        $.ajax({
            url: base_url2+"/admin/money_transfer/change_bill_orders_money_status",
            data: object,
            type: 'POST',
            success: function (data) {
                console.log(data);
                var json_data=JSON.parse(data);
                if(typeof (json_data.msg)!="undefined"){
                    money_transfer_msgs.html(json_data.msg);

                    this_element.removeAttr("disabled");
                    this_element.children(".ajax_loader_class").remove();


                }

            }

        });

    });


    //END money transfer



    var get_remain_of_bill=function(){
        var total_bill_amount=$(".total_bill_amount").val();
        var supplier_bill_total_paid_in_cash=$(".supplier_bill_total_paid_in_cash").val();
        var supplier_bill_total_paid_in_atm=$(".supplier_bill_total_paid_in_atm").val();


        if(parseFloat(total_bill_amount)<(parseFloat(supplier_bill_total_paid_in_cash)+parseFloat(supplier_bill_total_paid_in_atm))){

            $(".supplier_bill_total_paid_in_cash").css("border","1px solid red");
            $(".supplier_bill_total_paid_in_atm").css("border","1px solid red");

            $(".amount_remain").html("لا يمكنك ان تدفع مبلغ اكبر من قيمة الفاتورة");
            $(".purchase_bill_form_submit").attr("disabled","disabled");
        }
        else{
            $(".supplier_bill_total_paid_in_cash").css("border","1px solid #CCC");
            $(".supplier_bill_total_paid_in_atm").css("border","1px solid #CCC");

            $(".amount_remain").html(parseFloat(total_bill_amount)-(parseFloat(supplier_bill_total_paid_in_cash)+parseFloat(supplier_bill_total_paid_in_atm)));
            $(".purchase_bill_form_submit").removeAttr("disabled");
        }

    };

    $(".supplier_bill_total_paid_in_cash,.supplier_bill_total_paid_in_atm").keyup(function () {
        get_remain_of_bill();
    });


    $("body").on("keyup",".add_bill_order_price,.add_bill_order_quantity",function(){
        $(".count_bill_orders").click();
    });



    $('.show_deposite_money_filter').click(function () {
        
        var this_element = $(this);
        var branch_id = this_element.parents().find('.select_branch').val();
        var from_date_id = this_element.parents().find('#from_date_id').val();
        var to_date_id = this_element.parents().find('#to_date_id').val();

        if(
            typeof(branch_id) != "undefined" &&
            typeof(from_date_id) != "undefined" &&
            typeof(to_date_id) != "undefined" &&
            branch_id > 0
        )
        {
            location.href = base_url2 + "/admin/deposite_money/filter/"+branch_id+"/"+from_date_id+"/"+to_date_id;
        }
        else{
            alert("بيانات غير صالحة");
        }


        return false;

    });

    $('.cal_original_deposite_money').click(function () {

        var this_element = $(this);
        var to_date_id = this_element.parents().find('#to_date_id').val();
        var branch_id = this_element.parents().find('.select_branch').val();

        console.log(branch_id);

        if(typeof(to_date_id) != "undefined" && typeof(branch_id) != "undefined")
        {

            var object = {};
            object._token = _token;
            object.to_date_id=to_date_id;
            object.branch_id =branch_id;


            this_element.append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            console.log(object);

            $.ajax({
                url: base_url2+"/admin/deposite_money/cal_original_deposite_money",
                data: object,
                type: 'POST',
                success: function (data) {
                    // console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.msg)!="undefined")
                    {
                        this_element.parents().find('.show_original_deposite_money').html(json_data.msg);

                        this_element.removeAttr("disabled");
                        this_element.children(".ajax_loader_class").remove();

                    }

                }

            });

        }


        return false;
    });


    /* Start preview print cheque */

    $('.preview_cheque_before_print').click(function () {

        var this_element = $(this);
        var link = "";

        if(this_element.parents().find('#cheque_date').length > 0 )
        {
            var cheque_date = this_element.parents().find('#cheque_date').val();
            link += "?cheque_date="+cheque_date+"&";
        }


        if(this_element.parents().find('#receiver_name').length > 0
            && this_element.parents().find('#receiver_name').val() != "")
        {
            var receiver_name = this_element.parents().find('#receiver_name').val();
            link += "receiver_name="+encodeURI(receiver_name)+"&";
        }


        if(this_element.parents().find('#money_in_numbers').length > 0
            && this_element.parents().find('#money_in_numbers').val() > 0)
        {
            var money_in_numbers = this_element.parents().find('#money_in_numbers').val();
            link += "money_in_numbers="+money_in_numbers+"&";
        }


        if(this_element.parents().find('#money_in_text').length > 0
            && this_element.parents().find('#money_in_text').val() != "")
        {
            var money_in_text = this_element.parents().find('#money_in_text').val();
            link += "money_in_text="+encodeURI(money_in_text);
        }


        if(
            link != ""
        )
        {
            console.log( base_url2 + "/prepare_print_cheques"+link);
            location.href = base_url2 + "/prepare_print_cheques"+link;
        }
        else{
            alert("بيانات غير صالحة");
        }


        return false;

    });

    /* End preview print cheque */


    /* Start preview print invoice */

    $('.preview_invoice_before_print').click(function () {

        var this_element = $(this);
        var link = "";

        if(this_element.parents().find('#bill_id').length > 0 )
        {
            var bill_id = this_element.parents().find('#bill_id').val();
            link += "?bill_id="+bill_id+"&";
        }


        if(this_element.parents().find('#bill_type_id').length > 0
            && this_element.parents().find('#bill_type_id').val() != "")
        {
            var bill_type = this_element.parents().find('#bill_type_id').val();
            link += "bill_type="+encodeURI(bill_type);
        }


        if(
            link != ""
        )
        {
            console.log( base_url2 + "/print_client_bill"+link);
            location.href = base_url2 + "/print_client_bill"+link;
        }
        else{
            alert("بيانات غير صالحة");
        }

        return false;

    });

    /* End preview print invoice */


    /* Start show_non_reviewed_bills_filter */

    $('.show_non_reviewed_bills_filter').click(function () {

        var this_element = $(this);
        var branch_id = this_element.parents().find('.select_branch').val();
        var from_date_id = this_element.parents().find('#from_date_id').val();
        var to_date_id = this_element.parents().find('#to_date_id').val();

        if(
            typeof(branch_id) != "undefined" &&
            typeof(from_date_id) != "undefined" &&
            typeof(to_date_id) != "undefined" &&
            branch_id > 0
        )
        {
            location.href = base_url2 + "/admin/review_client_bills/filter/"+branch_id+"/"+from_date_id+"/"+to_date_id;
        }
        else{
            alert("بيانات غير صالحة");
        }


        return false;

    });

    /* End show_non_reviewed_bills_filter */

});