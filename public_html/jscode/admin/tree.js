$(function(){

    var xhr={};
    var selected_items={};
    var select_type="single";
    var show_tree_btn;
    var modal_class;

    $("body").on("click",".load_cat_childs",function(){

        var this_element = $(this);
        var load_childs=this_element.attr("data-loadchilds");

        if(load_childs=="false"){

            var object = {};
            object._token = _token;
            object.parent_id = this_element.attr("data-catid");
            object.return_to_ajax = true;
            object.cat_type = this_element.attr("data-cattype");

            if (xhr[object.parent_id]){
                xhr[object.parent_id].abort();
            }

            $(".accordion-inner",this_element).append(ajax_loader_img_func("15px"));



            xhr[object.parent_id]=$.ajax({
                url: base_url2 + "/common/category/show_tree",
                data: object,
                type: 'POST',
                success: function (data) {
                    var json_data=JSON.parse(data);

                    console.log(json_data);
                    $(".ajax_loader_class",$(".accordion-inner",this_element)).remove();

                    if(typeof (json_data.tree_html)!="undefined"){
                        $(".accordion-inner",this_element).append(json_data.tree_html);
                        this_element.attr("data-loadchilds","true");

                        if($("div#tree_modal.modal.fade.in").length>0){
                            show_tree_btn.click();
                        }
                    }

                }

            });
        }


    });

    $("body").on("click",".show_tree_modal",function(){

        modal_class=$(this).data("modalclass");
        if(typeof (modal_class)=="undefined"){
            modal_class="#tree_modal";
        }

        $(modal_class).modal("show");


        show_tree_btn=$(this);


        var select_what=$(this).attr("data-select_what");
        select_type=$(this).attr("data-select_type");

        console.log("select_what", select_what);

        if(select_what=="category"){
            $(".item_links").hide();

            $(".category_links_dropdown li").hide();
            $(".category_links_dropdown .select_li").show();
        }
        else if(select_what=="item"){
            //item
            $(".category_links").hide();

            $(".item_links_dropdown li").hide();
            $(".item_links_dropdown .select_li").show();
        }


        return false;
    });

    $("body").on("click",".select_li",function () {

        var item_id=$(this).attr("data-item_id");
        var item_name=$(this).attr("data-item_name");

        if(select_type=="single"){
            selected_items={};
            selected_items[item_id]=item_name;
        }
        else{
            selected_items[item_id]=item_name;
        }


        //show selected items in .selected_items
        draw_selected_items(".selected_items");

        return false;
    });

    var draw_selected_items=function(draw_where,input_field_name,draw_is_selector){
        var input_type=show_tree_btn.attr("data-input_type");

        if(select_type!="single"){
            input_field_name=input_field_name+"[]";
        }

        var draw_html="";
        $.each(selected_items,function (i,v) {
            draw_html+='<label class="label label-primary selected_individual_item" data-item_id="'+i+'">';
            draw_html+=v;
            draw_html+='<a href="#" class="remove_selected_individual_item">&times;</a>';

            if(typeof (input_field_name)!="undefined"){
                draw_html+='<input type="hidden" name="'+input_field_name+'" value="'+i+'">';

                if(typeof (input_type)!="undefined"){
                    draw_html+='<input type="'+input_type+'" name="value_of_'+input_field_name+'" value="">';
                }
            }

            draw_html+='</label>';
        });


        if(draw_is_selector===true,draw_is_selector==="true"){
            draw_is_selector.html(draw_html);
        }
        else{
            $(draw_where).html(draw_html);
        }
    };


    $("body").on("click",".remove_selected_individual_item",function(){

        if(confirm("هل أنت متأكد ؟")){

            var item_id=$(this).parents(".selected_individual_item").attr("data-item_id");

            delete selected_items[item_id];

            draw_selected_items(".selected_items");

            $(this).parents("label").remove();

        }



        return false;
    });

    $(".approve_selected_items_btn").click(function(){

        var div_to_show_selection=show_tree_btn.attr("data-div_to_show_selection");
        var input_field_name=show_tree_btn.attr("data-input_field_name");
        var use_this=show_tree_btn.attr("data-use_this");

        if(use_this===true,use_this==="true"){
            var parent_div_to_show_selection=show_tree_btn.attr("data-parent_div_to_show_selection");
            div_to_show_selection=$(div_to_show_selection,show_tree_btn.parents(parent_div_to_show_selection));
        }
        else{
            div_to_show_selection=$(div_to_show_selection);
        }

        draw_selected_items(div_to_show_selection,input_field_name);

        selected_items={};

        $(modal_class).modal("hide");
        draw_selected_items(".selected_items");

        return false;
    });


    $(".search_for_category").keyup(function () {

        var search_text=$(this).val();

        if(search_text.length==0){
            $(".load_cat_childs").show();
        }
        else{
            $.each($(".load_cat_childs"),function(){
                var cat_name=$(this).attr("data-catname");
                if(cat_name.includes(search_text)){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
        }

    });


    $("body").on("keyup",".search_for_item",function () {

        var search_text=$(this).val();
        var parent_div=$(this).parents(".accordion-inner");

        var search_by_name=$(".search_by_name",parent_div).is(":checked");
        var search_by_barcode=$(".search_by_barcode",parent_div).is(":checked");

        if(search_text.length==0){
            $(".item_itself",parent_div).show();
        }
        else{
            $.each($(".item_itself",parent_div),function(){
                var cat_name=$(this).attr("data-itemname");
                var barcode=$(this).attr("data-itembarcode");
                if((search_by_name==true&&cat_name.includes(search_text))||(search_by_barcode==true&&barcode.includes(search_text))){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
        }

    });


});