$(function(){

    var parent_div;

    $("body").on("click",".remove_parent_tr",function(){

        var confirm_res=confirm("are you sure?");

        if(confirm_res){

            $(this).parents("tr").remove();
        }

        return false;
    });

    if($(".add_permission_to_stock_parent_div").length){

        parent_div=$(".add_permission_to_stock_parent_div");

        parent_div.on("click",".select_product",function(){


            var appends_tr="<tr>";

            appends_tr+="<td>";
                appends_tr+="<input type='hidden' name='pro_id[]' value='"+$(this).data("pro_id")+"'>";
                appends_tr+=$(this).data("pro_name");
            appends_tr+="</td>";

            appends_tr+="<td>";
                appends_tr+="<input type='number' class='form-control' name='pro_quantity[]' value=''>";
            appends_tr+="</td>";

            appends_tr+="<td>";
                appends_tr+="<button type='button' class='btn btn-danger remove_parent_tr'>مسح</button>";
            appends_tr+="</td>";

            appends_tr+="</tr>";

            $(".orders_table").append(appends_tr);

        });

        parent_div.on("change","[name='pro_quantity[]']",function(){
            var products_count=0;

            $.each($("[name='pro_quantity[]']"),function(){
                products_count=products_count+parseInt($(this).val());
            });

            console.log("products_count",products_count);

            $(".all_pros_count").html("مجموع المنتجات "+products_count);
        });

        parent_div.on("keyup","[name='pro_quantity[]']",function(){
            $(this).change();
        });
    }

    if($(".add_branch_bill_another_class").length){
        parent_div=$(".add_branch_bill_another_class");

        parent_div.on("click",".select_product",function(){


            var appends_tr="<tr>";

            appends_tr+="<td>";
            appends_tr+="<input type='hidden' name='pro_id[]' value='"+$(this).data("pro_id")+"'>";
            appends_tr+=$(this).data("pro_name");
            appends_tr+="</td>";

            appends_tr+="<td>";
            appends_tr+="<input type='number' class='form-control check_quantity' name='b_o_quantity[]' value=''>";
            appends_tr+="</td>";

            appends_tr+="<td>";
            appends_tr+="<button type='button' class='btn btn-danger remove_parent_tr'>مسح</button>";
            appends_tr+="</td>";

            appends_tr+="</tr>";

            $(".orders_table").append(appends_tr);

        });

        parent_div.on("change","[name='b_o_quantity[]']",function(){

            var products_count=0;
            $.each($("[name='b_o_quantity[]']"),function(){
                products_count=products_count+parseInt($(this).val());
            });

            $(".all_pros_count").html("مجموع المنتجات "+products_count);
        });

        parent_div.on("keyup","[name='b_o_quantity[]']",function(){
            $(this).change();
        });

    }

    if($(".pro_advanced_search").length&&parent_div!=""){


        parent_div.on("click",".select_cat",function(){

            console.log("coorentbtm");

            var this_element = $(this);
            var cat_id=this_element.data("cat_id");

            $(".search_res").hide();
            $(".load_products_div").show();


            //check if child cats loaded before or not
            if($(".loaded_cats[data-parent_id='"+cat_id+"']").length==0){

                //load cats and products with parent_id=cat_id

                var object = {};
                object._token = _token;
                object.cat_id = cat_id;
                object.branch_id = $("#branch_id_id").val();

                this_element.append(ajax_loader_img_func("15px"));
                this_element.attr("disabled","disabled");

                $.ajax({
                    url: base_url2 + "/admin/branch_orders/get_cat_child_cats_and_products",
                    data: object,
                    type: 'POST',
                    success: function (data) {

                        var json_data=JSON.parse(data);

                        if(typeof (json_data)!="undefined"){
                            this_element.removeAttr("disabled");
                            this_element.children(".ajax_loader_class").remove();

                            $(".loaded_cats").hide();
                            $(".loaded_products").hide();

                            $.each(json_data.cats_ids,function(i,v){
                                $(".loaded_cats[data-parent_id='"+v+"']").remove();
                                $(".loaded_products[data-cat_id='"+v+"']").remove();
                            });

                            $(".load_cats_div").append(json_data.cats_view);
                            $(".load_products_div").append(json_data.products_view);

                            $(".loaded_cats[data-parent_id='"+cat_id+"']").show();
                            $(".loaded_products[data-cat_id='"+cat_id+"']").show();
                        }
                    }

                });




            }
            else{
                $(".loaded_cats").hide();
                $(".loaded_products").hide();

                $(".loaded_cats[data-parent_id='"+cat_id+"']").show();
                $(".loaded_products[data-cat_id='"+cat_id+"']").show();
            }


            return false;
        });

        parent_div.on("click",".show_parent_0_cats",function () {
            $(".loaded_cats").hide();
            $(".loaded_cats[data-parent_id='0']").show();

            return false;
        });

        parent_div.on("click",".show_prev_cats",function () {
            var load_cat_id=$(this).data("load_cat_id");
            $(".loaded_cats").hide();
            $('[data-cat_id="'+load_cat_id+'"]').parents(".loaded_cats").show();
            return false;
        });

        parent_div.on("keyup",".fast_search",function (e) {

            var loaded_products=$(this).parents(".loaded_products");
            var search_text=$(this).val();

            if(search_text==""){
                $(".select_product",loaded_products).show();
            }

            $.each($(".select_product",loaded_products),function(){
                var this_ele=$(this);
                var pro_name=this_ele.attr("data-pro_name");
                var pro_barcode=this_ele.attr("data-pro_barcode");

                var search_res=pro_name.includes(search_text)||pro_barcode.includes(search_text);


                if(search_res===true){
                    this_ele.show();
                }
                else if(search_res!==true){
                    this_ele.hide();
                }
            });

        });

        $(".search_for_product_by_name_or_barcode",parent_div).keydown(function(e){

            $(".load_products_div").hide();
            $(".search_res").show();

            if(e.which==13){

                var object = {};
                object._token = _token;
                object.search_keyword = $(this).val();
                object.branch_id = $("#branch_id_id").val();

                var this_element = $(this);
                var this_element_label = $(this).parents(".form-group").children("label");
                this_element_label.append(ajax_loader_img_func("15px"));

                this_element.attr("disabled","disabled");

                $.ajax({
                    url: base_url2 +"/admin/branch_orders/search_for_product",
                    data: object,
                    type: 'POST',
                    success: function (data) {
                        var json_data=JSON.parse(data);
                        if(typeof (json_data)!="undefined"){
                            this_element.removeAttr("disabled");
                            this_element_label.children(".ajax_loader_class").remove();

                            $(".search_res").html(json_data.products_view);
                        }

                    }

                });


            }

        });


    }





});