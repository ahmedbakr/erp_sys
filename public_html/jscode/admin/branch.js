$(function(){


    //branch bill
    // make collapse
    $('body').on('click','.collapse_action',function(){

        $(this).parent().find('.collapse_body').toggleClass('in');

        return false
    });

    $(".clone_order_item").click(function () {

        var new_item = $(this).parents().find('.bill_order_item').first().clone();

        if(typeof (new_item.attr("data-itemid"))!="undefined"){
            var new_item_id=parseInt($(this).parents().find('.bill_order_item').last().attr("data-itemid"))+1;

            new_item.attr("data-itemid",new_item_id );
            $(".show_tree_modal",new_item).attr("data-div_to_show_selection","[data-itemid='"+new_item_id+"'] .select_product");
        }
        console.log("new_item ",new_item);

        $(".select2",new_item).remove();
        $(".bill_orders_containers").append(new_item);

        if($('.select_2_class').length){
            $('.select_2_class').select2();
            seacrh_for_product(new_item.find('.branch_get_products'));
            seacrh_for_product_without_search(new_item.find(".branch_get_products_without_search"));
        }

        return false;
    });

    $("body").on("click",".delete_order_item",function () {

        var length = $(".delete_order_item").length;
        if (length > 1)
        {
            $res=confirm("you want delete item,sure?");

            if($res){
                $(this).parents(".bill_order_item").remove();
            }
        }
        else{
            alert(" Not Allowed To Removed !! ");
        }


        return false;
    });
    //END branch bill


    $("input[type='submit']").click(function(){
        //$(this).attr("disabled","disabled");
    });


    //expenses

    $(".show_expenses_filter_btn").click(function(){

        var select_expenses_type_id=$("#select_expenses_type_id").val();
        var filter_date_from=$("#filter_date_from").val();
        var filter_date_to=$("#filter_date_to").val();
        var group_by_id=$("#group_by_id").val();

        if(select_expenses_type_id==""){
            select_expenses_type_id=0;
        }

        if(filter_date_from==""){
            filter_date_from=0;
        }

        if(filter_date_to==""){
            filter_date_to=0;
        }

        if(group_by_id==""){
            group_by_id=0;
        }

        location.href=base_url2+"/branch/expenses/show_all_expenses/"
            +select_expenses_type_id+"/"
            +filter_date_from+"/"
            +filter_date_to+"/"
            +group_by_id;
    });

    //END expenses

    $(".show_clients_bills_filter_btn").click(function () {


        var select_user=$("#select_user_id_id").val();
        var from_date=$("#filter_date_from").val();
        var to_date=$("#filter_date_to").val();
        var group_by=$("#group_by_id").val();

        if(to_date==""){
            to_date="0";
        }

        if(select_user==""){
            select_user="0";
        }

        if(from_date==""){
            from_date="0";
        }

        if(to_date==""){
            to_date="0";
        }

        if(group_by==""){
            group_by="0";
        }


        location.href=base_url2+"/branch/client_bills/show_all/" +
            select_user+"/" +
            from_date+"/" +
            to_date+"/" +
            group_by;

    });


    $("#hide_finish_transfers").change(function(){

        if($(this).is(":checked")){
            $("tr.transfer_done").hide();
        }else{
            $("tr.transfer_done").show();
        }

    });


    $(".money_transfer_cash_money,.money_transfer_atm_money").keyup(function(){
        $(this).change();
    });


    $(".money_transfer_cash_money,.money_transfer_atm_money").change(function(){

        var finish_money_transfer_partially=$(".finish_money_transfer_partially").val();


        var money_transfer_amount=parseFloat($(".money_transfer_amount").html());

        var money_transfer_cash_money=parseFloat($(".money_transfer_cash_money").val());
        var money_transfer_atm_money=parseFloat($(".money_transfer_atm_money").val());

        if(!(money_transfer_cash_money>0)){
            money_transfer_cash_money=0;
        }

        if(!(money_transfer_atm_money>0)){
            money_transfer_atm_money=0;
        }

        var diff=money_transfer_amount-(money_transfer_cash_money+money_transfer_atm_money);


        $(".finish_money_transfer_btn").removeAttr("disabled");

        $(".transfer_money_msgs").html("");
        if(diff>0){
            $(".transfer_money_msgs").html("المتبقي "+diff);


            if(finish_money_transfer_partially==0){
                $(".transfer_money_msgs").append("لا يمكنك ان تبقي اي اموال");
                $(".finish_money_transfer_btn").attr("disabled","disabled");
            }
        }
        else if(diff<0){
            $(".transfer_money_msgs").html("لا يمكن ان تدفع اكثر من قيمة المعالمة");
            $(".finish_money_transfer_btn").attr("disabled","disabled");
        }




    });


    $('.show_branch_deposite_money_filter').click(function () {

        var this_element = $(this);
        var from_date_id = this_element.parents().find('#from_date_id').val();
        var to_date_id = this_element.parents().find('#to_date_id').val();

        if(
            typeof(from_date_id) != "undefined" &&
            typeof(to_date_id) != "undefined"
        )
        {
            location.href = base_url2 + "/branch/deposite_money/filter/"+from_date_id+"/"+to_date_id;
        }
        else{
            alert("بيانات غير صالحة");
        }


        return false;

    });

    $('.cal_original_deposite_money').click(function () {

        var this_element = $(this);
        var to_date_id = this_element.parents().find('#to_date_id').val();

        if(typeof(to_date_id) != "undefined")
        {

            var object = {};
            object._token = _token;
            object.to_date_id=to_date_id;


            this_element.append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2+"/branch/deposite_money/cal_original_deposite_money",
                data: object,
                type: 'POST',
                success: function (data) {
                    // console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.msg)!="undefined")
                    {
                        this_element.parents().find('.show_original_deposite_money').html(json_data.msg);

                        this_element.removeAttr("disabled");
                        this_element.children(".ajax_loader_class").remove();

                    }

                }

            });

        }


        return false;
    });


    /* Start Commissions */

    $('.show_commission_filter').click(function () {

        var this_element = $(this);
        var from_date_id = this_element.parents().find('#from_date_id').val();
        var to_date_id = this_element.parents().find('#to_date_id').val();
        var branch_id = this_element.parents().find('.select_branch').val();

        if(
            typeof(from_date_id) != "undefined" &&
            typeof(to_date_id) != "undefined" &&
            typeof(branch_id) != "undefined"
        )
        {
            location.href = base_url2 + "/admin/commissions/filter/"+branch_id+"/"+from_date_id+"/"+to_date_id;
        }
        else{
            alert("بيانات غير صالحة");
        }


        return false;

    });

    /* End Commissions */


    $("body").on("click",".make_sponsor_zero_btn",function(){


        var object = {};
        object._token = _token;
        object.user_id = $(this).attr("data-custid");

        var this_element = $(this);
        this_element.append(ajax_loader_img_func("15px"));

        this_element.attr("disabled","disabled");

        $.ajax({
            url: base_url2 + "/admin/users/make_sponsor_zero",
            data: object,
            type: 'POST',
            success: function (data) {
                console.log(data);
                var json_data=JSON.parse(data);
                if(typeof (json_data)!="undefined"){
                    this_element.html(json_data.msg);
                    this_element.removeAttr("disabled");
                    this_element.children(".ajax_loader_class").remove();
                }

            }

        });


        return false;
    });


    /**
     * Start Search for Products
     */

    function format_pro (pro) {
        if(typeof (pro.pro_name)=="undefined"){
            return "ابحث بواسطة الكود او الاسم";
        }

        var markup =
            "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" +
            +"#"+pro.pro_barcode+" -- "+pro.pro_name +
            "</div>";
        "</div>";
        "</div>";


        return markup;
    }

    function formatProSelection (pro) {
        if(typeof (pro.pro_name)=="undefined"){
            return "ابحث بواسطة الكود او الاسم";
        }
        return "#"+pro.pro_barcode+" -- "+pro.pro_name;
    }

    // search for bill and get orders

    function seacrh_for_product(element)
    {
        if($(".branch_get_products").length > 0)
        {
            element.select2({
                ajax: {
                    url: base_url2+"/admin/product/search_for_product",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: format_pro, // omitted for brevity, see the source of this page
                templateSelection: formatProSelection // omitted for brevity, see the source of this page
            });
        }

    }
    seacrh_for_product($(".branch_get_products"));

    /**
     * END Search for Products
     */




    function seacrh_for_product_without_search(element) {

        if(element.length > 0)
        {
            element.select2({
                ajax: {
                    url: base_url2+"/admin/product/search_for_product_without_quantity",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: format_pro, // omitted for brevity, see the source of this page
                templateSelection: formatProSelection // omitted for brevity, see the source of this page
            });
        }
        
    }
    seacrh_for_product_without_search($(".branch_get_products_without_search"));

});