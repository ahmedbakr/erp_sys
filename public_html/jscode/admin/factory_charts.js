$(function () {

    var base_url2 = $(".url_class").val();
    var base_url = base_url2 + "/public_html/";
    var _token = $(".csrf_input_class").val();
    var ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";


    if (typeof(google) != "undefined")
    {

        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawPurchasesMaterialsChart);
        function drawPurchasesMaterialsChart() {

            var chart_data = [];
            var input_element = $('.material_purchases_orders_names');
            if(input_element.length > 0)
            {
                input_element = input_element.val();
                input_element = JSON.parse(input_element);

                chart_data.push(['Material Name', 'Quanity']);
                $.each(input_element,function(ind,val){
                    var tmp = [];
                    tmp = [ind.toString(), val];
                    chart_data.push(tmp);
                });

                if(chart_data.length > 0)
                {
                    var data = google.visualization.arrayToDataTable(chart_data);

                    var options = {
                        title: 'Quantity of Materials from Purchases Orders',
                        pieHole: 0.4,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('material_purchases_quantity'));
                    chart.draw(data, options);
                }

            }

        }


        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawReturnsMaterialsChart);
        function drawReturnsMaterialsChart() {

            var chart_data = [];
            var input_element = $('.material_returns_orders_names');
            if(input_element.length > 0) {
                input_element = input_element.val();
                input_element = JSON.parse(input_element);

                chart_data.push(['Material Name', 'Quanity']);
                $.each(input_element, function (ind, val) {
                    var tmp = [];
                    tmp = [ind.toString(), val];
                    chart_data.push(tmp);
                });

                if(chart_data.length > 0)
                {
                    var data = google.visualization.arrayToDataTable(chart_data);

                    var options = {
                        title: 'Quantity of Materials from Returns Orders',
                        pieHole: 0.4,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('material_returns_quantity'));
                    chart.draw(data, options);

                }

            }


        }

    }

});