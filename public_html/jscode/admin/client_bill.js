$(function(){

    var client_bill_parnet_div=$(".add_client_bill_form");
    var tax_percent = 4.75;

    if ($('.sell_tax_percent').length)
    {
        tax_percent = $('.sell_tax_percent').val();
    }

    if(false){


        function format_user (user) {
            if(typeof (user.full_name)=="undefined"){
                return "ابحث بواسطة الكود او الاسم";
            }

            var markup =
            "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'>" +
                        user.full_name+" - "+user.user_tel +
                    "</div>";
                "</div>";
            "</div>";


            return markup;
        }

        function formatUserSelection (user) {
            if(typeof (user.full_name)=="undefined"){
                return "ابحث بواسطة الكود او الاسم";
            }
            return user.full_name +" - "+ user.user_tel;
        }

        // search for bill and get orders

        $(".client_tel_number").select2({
            ajax: {
                url: base_url2+"/admin/client_bills/search_for_user",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.items,
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: format_user, // omitted for brevity, see the source of this page
            templateSelection: formatUserSelection // omitted for brevity, see the source of this page
        });
    }


    $(".add_new_bill_btn,.add_return_bill_submit_btn").click(function(){
        $(this).hide();
    });


    $(".type_bill_number",$(".return_branch_bill_form")).keyup(function(event){
        if (event.which==13) {
            $(".get_bill_orders_submit",$(".return_branch_bill_form")).click();
        }
    });

    $('.get_bill_orders_submit',$(".return_branch_bill_form")).click(function (){

        var this_element = $(this);
        var input_element = $(this).parents().find(".type_bill_number");
        var bill_id = input_element.val();

        if(bill_id == "")
        {
            input_element.css({"border": "2px solid red"});
        }
        else{
            input_element.css({"border": "2px solid green"});
            input_element.attr("disabled","disabled");
            this_element.append(ajax_loader_img);
            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/branch_bills/get_bill_orders",
                data: {"_token": _token, "bill_id": bill_id},
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    $(".ajax_loader_class").remove();
                    this_element.removeAttr("disabled");
                    input_element.removeAttr("disabled");
                    this_element.parents().find('.bill_orders_containers').removeClass("hide_row");

                    if (typeof (return_data.success) != "undefined" && return_data.success == "error") {
                        this_element.parents().find('.form_submit_btns').addClass("hide_row");
                        this_element.parents().find('.bill_short_info').addClass("hide_row");
                        this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                        this_element.parents().find('.collapse_body').html(return_data.msg);
                    } else {
                        this_element.parents().find('.form_submit_btns').removeClass("hide_row");
                        this_element.parents().find('.bill_short_info').removeClass("hide_row");
                        this_element.parents().find('.bill_short_info').find('.content_body').html(return_data.bill_data);
                        this_element.parents().find('.bill_orders_containers').find('.collapse_body').html(return_data.table_data);

                        if (typeof (return_data.return_orders_table) != "undefined")
                        {
                            this_element.parents().find('.bill_returns_containers').removeClass("hide_row");
                            this_element.parents().find('.bill_returns_containers').find('.collapse_body').html(return_data.return_orders_table);
                        }
                        else{
                            this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                        }

                    }
                }
            });

        }

        return false;
    });


    //clients bills
    $(".add_new_bill_btn").attr("disabled","disabled");


    $(".add_branch_bill_form").on("change",".check_quantity",function(){

        var products_quantities=JSON.parse($(".products_quantities_in_factory").val());

        //get pro_id of chosen product
        var current_pro_id=$(".select_product",$(this).parents(".bill_order_item")).val();

        $(this).attr("max",products_quantities[current_pro_id]);

        if($(this).val()>products_quantities[current_pro_id]){
            $(this).val(products_quantities[current_pro_id]);
        }

    });

    $(".add_branch_bill_form").on("keyup",".check_quantity",function () {
        if($(this).val()==""){
            $(this).val("0");
        }

        $(this).change();
    });

    $('.add_client_bill_form form').on('keyup keypress keydown', function(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if(charCode==13){

            e.preventDefault();
            return false;
        }

    });

    $(".pro_sell_price_class",$(".add_client_bill_form")).attr("disabled","disabled");

    $(".calc_total_price",$(".add_client_bill_form")).click(function () {

        var totla_amount=0;
        $.each($(".bill_order_item"),function (i,v) {
            totla_amount=totla_amount+($(".check_quantity",$(this)).val()*$(".pro_sell_price_class",$(this)).val());

        });

        $(".client_bill_total_amount_class",".add_client_bill_form").val(totla_amount);
        $(".client_bill_total_amount_class",".add_client_bill_form").data("totla_amount",totla_amount);
        $(".client_bill_total_amount_class_label").html($(".client_bill_total_amount_class").data('total_bill_amount_without_tax'));


        return false;
    });

    $(".add_client_bill_form").on("change",".check_quantity",function(){

        var this_element=$(this);
        var parent_element=$(this).parents("tr");

        //check if current val of this input is <=max
        if(parseFloat(this_element.val())>parseFloat(this_element.attr("max"))){
            this_element.val(parseFloat(this_element.attr("max")));
        }

        // var total=parseFloat(this_element.val())*parseFloat($(".pro_sell_price_class",parent_element).val());
        // $(".total_tr_price",parent_element).val(total);
    });

    $(".add_client_bill_form").on("keyup",".check_quantity",function () {
        $(this).change();
    });

    var this_order_tr=$(".orders_table tr").eq(1);

    if(this_order_tr.length>0){
        $(".orders_table tr").removeClass("success");
        this_order_tr.addClass("success");
    }

    $(".orders_table").on("mousedown","tr",function(){
        this_order_tr=$(this);
        $(".orders_table tr").removeClass("success");
        this_order_tr.addClass("success");
    });

    $(document).keydown(function(e) {

        var charCode = (e.which) ? e.which : e.keyCode;


        var current_quantity_input=$(".check_quantity",this_order_tr);
        var current_quantity=parseInt(current_quantity_input.val());

        if((charCode==107||charCode==187)&&e.altKey){
            //+
            //check if current_quantity ! > product_quantiy
            if(current_quantity_input.val()==""){
                current_quantity_input.val("0");
                current_quantity=parseInt(current_quantity_input.val());
            }
            current_quantity_input.val(current_quantity+1);
        }
        else if((charCode==109||charCode==189)&&e.altKey){
            //-
            //check if current_quantity!<1
            if(current_quantity_input.val()==""){
                current_quantity_input.val("0");
                current_quantity=parseInt(current_quantity_input.val());
            }
            if(current_quantity>1){
                current_quantity_input.val(current_quantity-1);
            }
        }
        else if(((charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105))&&e.altKey){
            current_quantity_input.val(current_quantity_input.val()+e.key);
        }
        else if((charCode == 8)&&e.altKey){
            current_quantity_input.val(current_quantity_input.val().slice(0,-1));
        }
        else if((charCode == 13)&&e.altKey){
            $(".calc_total_bill").click();
        }
        else if((charCode == 17)&&e.altKey) {
            this_order_tr=$(".orders_table tr.success").next();
            if(this_order_tr.length==0){
                this_order_tr=$(".orders_table tr").eq(1);
            }

            $(".orders_table tr.success").removeClass("success");
            this_order_tr.addClass("success");
        }

        current_quantity_input.change();
    });

    client_bill_parnet_div.on("click",".btn_increase_order",function(){
        var parent_td=$(this).parents("td");
        var max=$(".check_quantity",parent_td).attr("max");

        if(max>=parseInt($(".check_quantity",parent_td).val())+1){
            $(".check_quantity",parent_td).val(parseInt($(".check_quantity",parent_td).val())+1);
        }

        $(".check_quantity",parent_td).change();

        return false;
    });

    client_bill_parnet_div.on("click",".btn_decrease_order",function(){
        var parent_td=$(this).parents("td");
        var min=$(".check_quantity",parent_td).attr("min");


        if(min<=parseInt($(".check_quantity",parent_td).val())-1) {
            $(".check_quantity", parent_td).val(parseInt($(".check_quantity", parent_td).val()) - 1);
        }

        $(".check_quantity",parent_td).change();

        return false;
    });

    $(".search_for_product_barcode",client_bill_parnet_div).keyup(function(e){
        var charCode = (e.which) ? e.which : e.keyCode;
        var this_element=$(this);

        if(charCode=="13"&&e.altKey==false){

            var object = {};
            object._token = _token;
            object.pro_barcode = this_element.val();
            object.selected_price_id = $(".selected_price_id").val();
            object.branch_id = $("#branch_id_id").val();


            $(".barcode_msgs").append(ajax_loader_img_func("15px"));
            $(".barcode_msgs").attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/client_bills/get_product_by_barcode",
                data: object,
                type: 'POST',
                success: function (data) {
                    this_element.val("");

                    var json_data=JSON.parse(data);
                    if(typeof (json_data)!="undefined"){
                        $(".barcode_msgs").html(json_data.msg);
                        $(".barcode_msgs").children(".ajax_loader_class").remove();
                        this_element.removeAttr("disabled");

                        //before adding table tr check for pro_id is exist in table or not
                        var new_pro_id=json_data.pro_data.pro_id;
                        var tr_with_same_id=$("tr[data-proid='"+new_pro_id+"']");

                        if(tr_with_same_id.length==0||(tr_with_same_id.length>0&&typeof (tr_with_same_id.attr("data-packageid"))!="undefined")){
                            $(".orders_table").append(json_data.table_tr);
                            $(".orders_table tr").removeClass("success");
                            this_order_tr=$(".orders_table tr").last();
                            this_order_tr.addClass("success");
                        }
                        else{
                            //increase quantity of tr_with_same_id
                            $(".check_quantity",tr_with_same_id).val(parseInt($(".check_quantity",tr_with_same_id).val())+1);
                            $(".check_quantity",tr_with_same_id).change();
                        }

                        check_enter_package();

                    }

                }

            });

        }
        if(charCode=="13"&&e.altKey){
            $(".calc_total_bill").click();
        }

    });

    client_bill_parnet_div.on("click",".remove_tr_order",function () {
        if(confirm("هل انت متاكد ؟")){
            $(this).parents("tr").remove();
        }

        return false;
    });

    var check_enter_package=function(){

        var orders={};
        var orders_items_count=0;

        //get all product with buyed quantity
        $.each($(".orders_table tr"),function (i,v) {
            var this_tr=$(this);
            var this_tr_pro_id=this_tr.data("proid");

            if(typeof (this_tr_pro_id)!="undefined"){
                if(typeof ($(".check_quantity",this_tr).val())!="undefined"){
                    orders[this_tr_pro_id]=$(".check_quantity",this_tr).val();
                    orders_items_count=parseInt(orders_items_count)+parseInt(orders[this_tr_pro_id]);
                }


            }
        });


        //get all packages
        var package={
            package_pro_ids:{},
            package_count:{}
        };


        $.each($(".package_data"),function (i,v) {


            var this_package=$(this);
            var this_package_min_products_to_get_offer=this_package.data("min_products_to_get_offer");
            var this_package_min_products_to_get_gift=this_package.data("min_products_to_get_gift");
            var this_package_gift_id=this_package.data("gift_id");
            var this_package_gift_name=this_package.data("gift_name");
            var this_package_type=this_package.data("packagetype");

            var package_id=this_package.data("packageid");
            var discount_percentage=parseFloat(this_package.data("discount_percentage"));


            package.package_count[package_id]=0;
            package.package_pro_ids[package_id]=[];


            var this_packages_products=this_package.data("proids");
            this_packages_products = this_packages_products.map(function(x) {
                return parseInt(x);
            });


            $.each(orders,function(i,v){
                //get count of products in common with package and orders
                if($.inArray(parseInt(i),this_packages_products)>=0){
                    package.package_count[package_id]=parseInt(package.package_count[package_id])+parseInt(v);
                    package.package_pro_ids[package_id].push(i);
                }
            });

            //check if this package is activate of not

            //it could be there is no discount offer
            if(this_package_min_products_to_get_offer>0){

                if(package.package_count[package_id]>=this_package_min_products_to_get_offer){
                    //so this package is activated
                    var item_discount=discount_percentage/package.package_count[package_id];
                    item_discount=Math.round(item_discount * 100) / 100;

                    $.each(package.package_pro_ids[package_id],function(i,v){
                        var parent_tr=$("tr[data-proid='"+v+"']");
                        $(".enter_package_label",parent_tr).removeClass("label-default");
                        $(".enter_package_label",parent_tr).addClass("label-success");
                        $(".enter_package_label",parent_tr).html(
                            this_package.data("packagename")+
                            "<input type='hidden' name='selected_pac_id[]' value='"+package_id+"' />"
                        );

                        //change sell price for product
                        //current_pro_price*discount_percentage/100

                        var pro_sell_price_class=0;
                        if(this_package_type=="0"){
                            pro_sell_price_class=
                                (parseFloat($(".pro_sell_price_class",parent_tr).data("current_pro_price")))-
                                (
                                    (parseFloat($(".pro_sell_price_class",parent_tr).data("current_pro_price")))*
                                    (discount_percentage/100)
                                )
                            ;
                        }else{

                            if(item_discount>(parseFloat($(".pro_sell_price_class",parent_tr).data("current_pro_price")))){
                                pro_sell_price_class=0;
                            }
                            else{
                                pro_sell_price_class=
                                    (parseFloat($(".pro_sell_price_class",parent_tr).data("current_pro_price")))-item_discount
                                ;
                            }
                        }

                        // pro_sell_price_class=round_fund(pro_sell_price_class);
                        $(".pro_sell_price_class",parent_tr).val(pro_sell_price_class);
                        $(".check_quantity",parent_tr).change();
                        parent_tr.attr("data-current_package",package_id);
                    });

                }
                else{
                    //so this package is deactivated
                    $.each(package.package_pro_ids[package_id],function(i,v){
                        var parent_tr=$("tr[data-proid='"+v+"'][data-current_package='"+package_id+"']");


                        $(".enter_package_label",parent_tr).addClass("label-default");
                        $(".enter_package_label",parent_tr).removeClass("label-success");
                        $(".enter_package_label",parent_tr).html("لا");


                        $(".pro_sell_price_class",parent_tr).val($(".pro_sell_price_class",parent_tr).data("current_pro_price"));
                        $(".check_quantity",parent_tr).change();
                        parent_tr.removeAttr("data-current_package",package_id);
                    });
                }
            }


            //get number of gifts that supplier will take
            //it could be there is no gift in this package
            if(this_package_gift_id>0&&this_package_min_products_to_get_gift>0){

                var number_of_this_package_gifts=package.package_count[package_id]/this_package_min_products_to_get_gift;
                number_of_this_package_gifts=parseInt(number_of_this_package_gifts);

                if(number_of_this_package_gifts>0){

                    $.each(package.package_pro_ids[package_id],function(i,v){
                        var parent_tr=$("tr[data-proid='"+v+"']");
                        $(".enter_package_label",parent_tr).removeClass("label-default");
                        $(".enter_package_label",parent_tr).addClass("label-success");
                        $(".enter_package_label",parent_tr).html(this_package.data("packagename"));
                    });

                    //add to .orders_table row to show the number of gifts
                    $(".orders_table tr[data-packageid='"+package_id+"']").remove();

                    $(".orders_table").append(
                        "<tr data-proid='"+this_package_gift_id+"' data-packageid='"+package_id+"'>" +
                            "<td>" +
                                this_package_gift_name +
                            "</td>" +
                            "<td>" +
                                number_of_this_package_gifts +
                            "</td>" +
                            "<td>" +
                                "هدية العرض"+
                            "</td>" +
                            "<td>" +
                                "هدية العرض"+
                            "</td>" +
                            "<td>" +
                                this_package.data("packagename")+
                            "</td>" +
                            "<td></td>"+
                        "</tr>"
                    );
                }
                else{

                    $.each(package.package_pro_ids[package_id],function(i,v){
                        var parent_tr=$("tr[data-proid='"+v+"']");
                        $(".enter_package_label",parent_tr).addClass("label-default");
                        $(".enter_package_label",parent_tr).removeClass("label-success");
                        $(".enter_package_label",parent_tr).html("لا");
                    });

                    $(".orders_table tr[data-packageid='"+package_id+"']").remove();
                }
            }




        });



    };

    $(".calc_total_bill").click(function () {

        check_enter_package();

        var bill_total=0;

        $.each($(".check_quantity"),function(){
            var this_element=$(this);
            var parent_element=$(this).parents("tr");

            //check if current val of this input is <=max
            if(parseFloat(this_element.val())>parseFloat(this_element.attr("max"))){
                this_element.val(parseFloat(this_element.attr("max")));
            }

            var item_price=Math.round(parseFloat($(".pro_sell_price_class",parent_element).val()) * 100) / 100;
            var total=(parseInt(this_element.val())*item_price).toFixed(2);

            $(".total_tr_price",parent_element).val(total);
            bill_total=parseFloat(bill_total)+parseFloat(total);
        });

        var bill_tax_value = (bill_total*tax_percent)/100;
        var bill_total_with_tax = bill_total+bill_tax_value;

        bill_total_with_tax=round_fund(bill_total_with_tax);

        bill_total=round_fund(bill_total);

        console.log("bill_total",bill_total);
        console.log("bill_total_with_tax",bill_total_with_tax);

        $('.total_bill_tax_value').val(round_fund(bill_tax_value));

        $(".client_bill_total_amount_class").val(bill_total_with_tax);
        $(".client_bill_total_amount_class").prop("data-total_amount",bill_total_with_tax);
        $(".client_bill_total_amount_class").prop("data-total_bill_amount_without_tax",bill_total);
        $(".client_bill_total_amount_class_label").html(bill_total);

        //calc coupon effect after calc total amount
        calc_coupon_effect();
        calc_remain_money();


    });


    $(".check_coupons").click(function(){

        var coupon_text_arr=[];

        $.each($(".coupon_text"),function(){
            if(coupon_text_arr.indexOf($(this).val())>=0){
               $(this).parents("tr").remove();
            }
            else{
                coupon_text_arr.push($(this).val());
            }
        });


        var object = {};
        object._token = _token;
        object.coupon_text = coupon_text_arr;

        $(".coupon_msgs").append(ajax_loader_img_func("10px"));

        $(".coupon_text").attr("readonly","readonly");

        $.ajax({
            url: base_url2 + "/admin/client_bills/check_coupon",
            data: object,
            type: 'POST',
            success: function (data) {
                var json_data=JSON.parse(data);
                if(typeof (json_data.msg)!="undefined"){
                    $(".coupon_msgs").html(json_data.msg);
                    $(".coupon_msgs").children(".ajax_loader_class").remove();

                }
                if(typeof (json_data.coupons)!="undefined"){

                    var coupons_data=json_data.coupons;

                    $.each($(".coupon_text"),function(){
                        var this_element=$(this);
                        var parent_element=this_element.parents("tr");

                        var data_obj;

                        if(typeof (coupons_data[this_element.val()])!="undefined"&&typeof (coupons_data[this_element.val()][0])!="undefined"){
                            data_obj=coupons_data[this_element.val()][0];
                        }
                        else{
                            return true;
                        }

                        $(".coupon_type_label",parent_element).val(((data_obj.is_rate)?"كوبون خصم":"كوبون بقيمة خصم"));
                        $(".coupon_type",parent_element).val(((data_obj.is_rate=="1")?"percentage":"value"));
                        $(".coupon_value",parent_element).val(data_obj.coupon_value);
                    });



                    calc_coupon_effect();
                    calc_remain_money();
                }

            }

        });


    });

    $(".add_coupon_row").click(function(){

        var coupon_item=$(".coupon_item").first().clone();

        $.each($("input",coupon_item),function(){
            $(this).val("");
        });


        $(".coupon_text",coupon_item).removeAttr("readonly");

        $(".coupons_table tbody").append(coupon_item);
    });

    client_bill_parnet_div.on("click",".remove_coupon_row",function(){

        if(confirm("هل انت متأكد")){
            if($(".coupon_item").length==1){
                $(".coupon_msgs").html("<div class='alert alert-warning'>لا يمكنك ان تسمح هذا الصف لانه الصف الوحيد المتبقي</div>");
            }
            else{
                $(this).parents(".coupon_item").remove();
            }
        }



       return false;
    });

    $(".coupon_text").keyup(function (e) {

        var charCode = (e.which) ? e.which : e.keyCode;
        var this_element=$(this);

        if(charCode=="13"&&e.altKey==false){

            var object = {};
            object._token = _token;
            object.coupon_text = this_element.val();

            $(".coupon_msgs").append(ajax_loader_img_func("10px"));
            this_element.attr("readonly","readonly");

            $.ajax({
                url: base_url2 + "/admin/client_bills/check_coupon",
                data: object,
                type: 'POST',
                success: function (data) {
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.msg)!="undefined"){
                        $(".coupon_msgs").html(json_data.msg);
                        $(".coupon_msgs").children(".ajax_loader_class").remove();
                        this_element.removeAttr("readonly");
                    }
                    if(typeof (json_data.coupon_type)!="undefined"){
                        this_element.attr("readonly","readonly");

                        $(".coupon_type_label").val(json_data.coupon_type_label);
                        $(".coupon_value").val(json_data.coupon_value);
                        $(".coupon_type").val(json_data.coupon_type);


                        calc_coupon_effect();
                        calc_remain_money();
                    }

                }

            });

        }

    });

    var calc_coupon_effect=function(){
        var bill_total_amount=parseFloat($(".client_bill_total_amount_class").prop("data-total_bill_amount_without_tax"));
        var bill_total_discount=0;

        $.each($(".coupon_text"),function(){
            var parent_element=$(this).parents("tr");

            var coupon_value=parseFloat($(".coupon_value",parent_element).val());
            var coupon_type=$(".coupon_type",parent_element).val();

            if(coupon_type=="percentage"){
                bill_total_discount=bill_total_discount+(bill_total_amount*(parseFloat(coupon_value)/100));
                bill_total_discount=round_fund(bill_total_discount);
            }
            else if(coupon_type=="value"){
                bill_total_discount=bill_total_discount+parseFloat(coupon_value);
            }

        });


        // bill_total_discount=round_fund(bill_total_discount);

        if(bill_total_amount-bill_total_discount>0){
            $(".total_coupon_discount").html(bill_total_discount);

            var new_bill_total_amount = (bill_total_amount-bill_total_discount);
            $(".client_bill_total_amount_class").val(new_bill_total_amount);

            // re calc tax after discount
            var bill_tax_value = (new_bill_total_amount*tax_percent)/100;
            var bill_total_with_tax = new_bill_total_amount+bill_tax_value;

            bill_total_with_tax=round_fund(bill_total_with_tax);

            new_bill_total_amount=round_fund(new_bill_total_amount);

            $('.total_bill_tax_value').val(round_fund(bill_tax_value));

            $(".client_bill_total_amount_class").val(bill_total_with_tax);
            $(".client_bill_total_amount_class").prop("data-total_amount",bill_total_with_tax);
            $(".client_bill_total_amount_class").prop("data-total_bill_amount_without_tax",new_bill_total_amount);

            $(".client_bill_total_amount_class_label").html($(".client_bill_total_amount_class").prop('data-total_bill_amount_without_tax'));

        }
        else{
            $(".total_coupon_discount").html(bill_total_amount);

            $(".client_bill_total_amount_class").val("0");
            $(".client_bill_total_amount_class_label").html($(".client_bill_total_amount_class").prop('data-total_bill_amount_without_tax'));

        }

    };





    var calc_remain_money=function(){

        var total_amount=parseFloat($(".client_bill_total_amount_class").val());
        var cash_money=parseFloat($(".cash_money").val());
        var atm_money=parseFloat($(".atm_money").val());
        var allow_cashier_to_remain_money=$("#allow_cashier_to_remain_money").val();


        if(!(total_amount>0)){
            cash_money=0;
        }

        if(!(cash_money>0)){
            cash_money=0;
        }

        if(!(atm_money>0)){
            atm_money=0;
        }


        var diff=total_amount-(cash_money+atm_money);


        if(total_amount>=(cash_money+atm_money)){
            $(".money_msgs").html("الباقي : "+diff.toFixed(2));
        }
        else if(total_amount<(cash_money+atm_money)){
            $(".money_msgs").html("لا يمكنك ان تدفع مبلغ اكثر من المطلوب");
        }

        $(".add_new_bill_btn").attr("disabled","disabled");
        if(diff==0){
            $(".add_new_bill_btn").removeAttr("disabled");
        }

        if(diff>0&&allow_cashier_to_remain_money=="0"){
            $(".add_new_bill_btn").attr("disabled","disabled");
            $(".money_msgs").append("لا يمكنك ان تبقي اي مال علي العميل");
        }
        else{
            $(".add_new_bill_btn").removeAttr("disabled");
        }

    };

    $(".cash_money,.atm_money").keyup(function(){
        calc_remain_money();
    });

    $(".payment_method_money").focus(function(){
        if($(this).val()=="0"){
            $(this).val("");
        }
    });

    $(".payment_method_money").focusout(function(){
        if($(this).val()==""){
            $(this).val("0");
        }
    });


    $(".payment_method_money").keyup(function(){

        var all_atm_money=parseFloat(0);
        var all_cash_money=parseFloat(0);


        $.each($("[data-is_payment_method_atm='1']"),function(){
            all_atm_money=all_atm_money+parseFloat($(this).val());
        });

        $.each($("[data-is_payment_method_atm='0']"),function(){
            all_cash_money=all_cash_money+parseFloat($(this).val());
        });


        $(".atm_money").val(all_atm_money.toFixed(2));
        $(".cash_money").val(all_cash_money.toFixed(2));


        calc_remain_money();
    });

    $(".total_client_paid_just_for_calc").keyup(function(){

        var client_bill_total_amount=parseFloat($(".client_bill_total_amount_class").val());

        if(!(parseFloat(client_bill_total_amount)>0)){
            $(".money_msgs").html("يجب ان تحسب مجموع الفاتورة اولا قبل ان تدخل بيانات في هذا المدخل");
        }

        var this_val=parseFloat($(this).val());

        if(this_val>=client_bill_total_amount){
            $(".payment_method_money").first().val(client_bill_total_amount.toFixed(2));

            $(".total_remain_just_for_calc").val((this_val-client_bill_total_amount).toFixed(2));
        }
        else if(this_val<client_bill_total_amount){
            $(".payment_method_money").first().val(this_val.toFixed(2));
        }


        var e = $.Event('keyup');
        e.which = 13; // Character 'A'
        $('.payment_method_money').trigger(e);

    });




    $(".add_new_client_data").click(function(){
        var e = $.Event('keyup');
        e.which = 13; // Character 'A'
        $('.client_tel_number').trigger(e);

        $(this).attr("disabled","disabled");
    });

    $(".client_tel_number").change(function(){
        // var e = $.Event('keyup');
        // e.which = 13; // Character 'A'
        // $('.client_tel_number').trigger(e);
    });

    $(".client_tel_number").keyup(function(e){

        if(e.which==13){

            var this_element = $(this);

            var object = {};
            object._token = _token;
            object.user_tel =this_element.val();
            object.user_name =$(".client_name").val();
            object.user_address =$(".client_address").val();
            object.user_email =$(".client_email").val();
            object.original_client_tel_number =$(".original_client_tel_number").val();
            object.branch_id = $("#branch_id_id").val();

            $(".client_msgs").append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2+ "/admin/client_bills/get_client_data",
                data: object,
                type: 'POST',
                success: function (data) {
                    var json_data=JSON.parse(data);
                    if(typeof (json_data)!="undefined"){
                        $(".client_msgs").html(json_data.msg);
                        this_element.removeAttr("disabled");
                        $(".client_msgs").children(".ajax_loader_class").remove();
                        $(".add_new_client_data").removeAttr("disabled");

                        $(".search_for_product_barcode").removeAttr("disabled");
                        $(".selected_price_id").val(json_data.selected_price_id);
                    }

                }

            });


        }

    });


    client_bill_parnet_div.on("click",".select_product",function(){

        $(".search_for_product_barcode").val($(this).data("pro_barcode"));

        var e = $.Event('keyup');
        e.which = 13; // Character 'A'
        e.altKey=false;

        $('.search_for_product_barcode').trigger(e);

    });

    client_bill_parnet_div.on("click",".select_cat",function(){

        var this_element = $(this);
        var cat_id=this_element.data("cat_id");

        $(".search_res").hide();
        $(".load_products_div").show();


        //check if child cats loaded before or not
        if($(".loaded_cats[data-parent_id='"+cat_id+"']").length==0){

            //load cats and products with parent_id=cat_id

            var object = {};
            object._token = _token;
            object.cat_id = cat_id;
            object.branch_id = $("#branch_id_id").val();

            this_element.append(ajax_loader_img_func("15px"));
            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/client_bills/get_cat_child_cats_and_products",
                data: object,
                type: 'POST',
                success: function (data) {

                    var json_data=JSON.parse(data);

                    if(typeof (json_data)!="undefined"){
                        this_element.removeAttr("disabled");
                        this_element.children(".ajax_loader_class").remove();

                        $(".loaded_cats").hide();
                        $(".loaded_products").hide();

                        $.each(json_data.cats_ids,function(i,v){
                            $(".loaded_cats[data-parent_id='"+v+"']").remove();
                            $(".loaded_products[data-cat_id='"+v+"']").remove();
                        });

                        $(".load_cats_div").append(json_data.cats_view);
                        $(".load_products_div").append(json_data.products_view);

                        $(".loaded_cats[data-parent_id='"+cat_id+"']").show();
                        $(".loaded_products[data-cat_id='"+cat_id+"']").show();
                    }
                }

            });




        }
        else{
            $(".loaded_cats").hide();
            $(".loaded_products").hide();

            $(".loaded_cats[data-parent_id='"+cat_id+"']").show();
            $(".loaded_products[data-cat_id='"+cat_id+"']").show();
        }


        return false;
    });

    client_bill_parnet_div.on("click",".show_parent_0_cats",function () {
        $(".loaded_cats").hide();
        $(".loaded_cats[data-parent_id='0']").show();

        return false;
    });

    client_bill_parnet_div.on("click",".show_prev_cats",function () {
        var load_cat_id=$(this).data("load_cat_id");
        $(".loaded_cats").hide();
        $('[data-cat_id="'+load_cat_id+'"]').parents(".loaded_cats").show();
        return false;
    });

    client_bill_parnet_div.on("keyup",".fast_search",function (e) {

        console.log("client bill");
        var loaded_products=$(this).parents(".loaded_products");
        var search_text=$(this).val();

        if(search_text==""){
            $(".select_product",loaded_products).show();
        }

        $.each($(".select_product",loaded_products),function(){
            var this_ele=$(this);
            var pro_name=this_ele.attr("data-pro_name");
            var pro_barcode=this_ele.attr("data-pro_barcode");

            var search_res=pro_name.includes(search_text)||pro_barcode.includes(search_text);


            if(search_res===true){
                this_ele.show();
            }
            else if(search_res!==true){
                this_ele.hide();
            }
        });

    });

    $(".search_for_product_by_name_or_barcode",client_bill_parnet_div).keydown(function(e){

        $(".load_products_div").hide();
        $(".search_res").show();

        if(e.which==13){

            var object = {};
            object._token = _token;
            object.search_keyword = $(this).val();
            object.branch_id = $("#branch_id_id").val();

            var this_element = $(this);
            var this_element_label = $(this).parents(".form-group").children("label");
            this_element_label.append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 +"/admin/client_bills/search_for_product",
                data: object,
                type: 'POST',
                success: function (data) {
                    var json_data=JSON.parse(data);
                    if(typeof (json_data)!="undefined"){
                        this_element.removeAttr("disabled");
                        this_element_label.children(".ajax_loader_class").remove();

                        $(".search_res").html(json_data.products_view);
                    }

                }

            });


        }

    });

    //END clients bills



    //return bill

    $(".type_bill_number",$(".return_client_bill_form")).keyup(function(event){
        if (event.which==13) {
            $('.get_bill_orders_submit',$(".return_client_bill_form")).click();
        }
    });

    $('.get_bill_orders_submit',$(".return_client_bill_form")).click(function (){



        var this_element = $(this);
        var input_element = $(this).parents().find(".type_bill_number");
        var bill_id = input_element.val();

        if(bill_id == "")
        {
            input_element.css({"border": "2px solid red"});
        }
        else{
            input_element.css({"border": "2px solid green"});
            input_element.attr("disabled","disabled");
            this_element.append(ajax_loader_img);
            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/client_bills/get_bill_orders",
                data: {"_token": _token, "bill_id": bill_id},
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    $(".ajax_loader_class").remove();
                    this_element.removeAttr("disabled");
                    input_element.removeAttr("disabled");
                    this_element.parents().find('.bill_orders_containers').removeClass("hide_row");

                    if (typeof (return_data.success) != "undefined" && return_data.success == "error") {
                        this_element.parents().find('.form_submit_btns').addClass("hide_row");
                        this_element.parents().find('.bill_short_info').addClass("hide_row");
                        this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                        this_element.parents().find('.collapse_body').html(return_data.msg);
                    } else {
                        this_element.parents().find('.form_submit_btns').removeClass("hide_row");
                        this_element.parents().find('.bill_short_info').removeClass("hide_row");
                        this_element.parents().find('.bill_short_info').find('.content_body').html(return_data.bill_data);
                        this_element.parents().find('.bill_orders_containers').find('.collapse_body').html(return_data.table_data);

                        if (typeof (return_data.return_orders_table) != "undefined")
                        {
                            this_element.parents().find('.bill_returns_containers').removeClass("hide_row");
                            this_element.parents().find('.bill_returns_containers').find('.collapse_body').html(return_data.return_orders_table);
                        }
                        else{
                            this_element.parents().find('.bill_returns_containers').addClass("hide_row");
                        }

                    }
                }
            });

        }

        return false;
    });

    if($(".bill_id_value").val()>0){
        $(".get_bill_orders_submit").click();
    }

    $(".return_client_bill_form").on("change",".returned_quantity",function () {
        var this_element=$(this);
        var parent_tr=this_element.parents("tr");
        var max_val=this_element.attr("max");
        var this_row_product_price=round_3_numbers(parseFloat($(".product_price",parent_tr).html()));
        var this_val=parseInt(this_element.val());

        if(this_val>max_val){
            this_element.val(max_val);
            this_val=parseInt(this_element.val());
        }

        //calc user returned money
        $(".row_returned_money",parent_tr).html((this_val*this_row_product_price));


        //sum all row_returned_money
        var row_returned_money=0;
        $.each($(".row_returned_money"),function(){
            if(parseFloat($(this).html())>0){
                row_returned_money=row_returned_money+parseFloat($(this).html());
            }

        });

        row_returned_money=round_fund(row_returned_money);

        $(".total_returned_money").html(row_returned_money);
        $(".return_money_to_client").val(row_returned_money);

    });

    $(".return_client_bill_form").on("keyup",".returned_quantity",function (e) {
        $(this).change();
    });


    $(".return_client_bill_form").on("keyup",".return_cash_money_to_client,.return_atm_money_to_client",function () {


        var return_money_to_client=($(".return_money_to_client").val());
        var return_cash_money_to_client=($(".return_cash_money_to_client").val());
        var return_atm_money_to_client=($(".return_atm_money_to_client").val());

        if(return_money_to_client==""){
            return_money_to_client=0;
        }

        if(return_cash_money_to_client==""){
            return_cash_money_to_client=0;
        }

        if(return_atm_money_to_client==""||typeof (return_atm_money_to_client)=="undefined"){
            return_atm_money_to_client=0;
        }

        console.log("return_money_to_client",return_money_to_client);

        return_money_to_client=parseFloat(return_money_to_client);
        return_cash_money_to_client=parseFloat(return_cash_money_to_client);
        return_atm_money_to_client=parseFloat(return_atm_money_to_client);



        var diff=return_money_to_client-(return_cash_money_to_client+return_atm_money_to_client);

        diff=round_3_numbers(diff);


        if(diff>0){
            $(".return_money_to_client_class").html( "المتبقي "+diff );

            if($(".return_bill_allow_remain_money").val()=="0"){
                $(".return_money_to_client_class").append("غير مسموح لك ان تبقي اي اموال");
                $(".add_return_bill_submit_btn").attr("disabled","disabled");
            }
            else if($(".return_bill_allow_remain_money").val()=="1"){
                $(".add_return_bill_submit_btn").removeAttr("disabled");
            }

        }
        else if(diff==0){
            $(".add_return_bill_submit_btn").removeAttr("disabled");


            $(".return_money_to_client_class").html("");
        }
        else if(diff<0){
            $(".return_money_to_client_class").html("لا يمكنك ان تدفع اكثر من اللذي يستحقه العميل");
        }




    });


    //END return bill

    var round_fund=function(number){
        // return Math.round(number * 100) / 100
        return Math.round(number);
    };

    var round_3_numbers=function(number){
        return Math.round(number * 1000) / 1000
        // return Math.round(number);
    };



});