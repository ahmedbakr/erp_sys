$(function () {

    if(!location.href.includes("localhost")){
        setTimeout(function () {
            location.href = base_url2 + '/logout'
        },(1800000)*2); // about 30 min
    }


    //expenses

    $(".show_expenses_filter_btn").click(function(){

        var select_expenses_type_id=$("#select_expenses_type_id").val();
        var filter_date_from=$("#filter_date_from").val();
        var filter_date_to=$("#filter_date_to").val();
        var group_by_id=$("#group_by_id").val();

        if(select_expenses_type_id==""){
            select_expenses_type_id=0;
        }

        if(filter_date_from==""){
            filter_date_from=0;
        }

        if(filter_date_to==""){
            filter_date_to=0;
        }

        if(group_by_id==""){
            group_by_id=0;
        }

        location.href=base_url2+"/admin/expenses/show_all_expenses/"
            +select_expenses_type_id+"/"
            +filter_date_from+"/"
            +filter_date_to+"/"
            +group_by_id;
    });

    //END expenses



    //packages

    $(".show_packages_filter_btn").click(function(){

        var from_date=$("#from_date_id").val();
        var to_date=$("#to_date_id").val();

        if(from_date==""){
            from_date="0";
        }

        if(to_date==""){
            to_date="0";
        }


        location.href=base_url2+"/admin/packages/show/" +
            from_date+"/" +
            to_date;

    });

    //END packages

    //supplier bills
    $(".show_supplier_bills_filter_btn").click(function(){

        var select_user=$("#select_user_id_id").val();
        var from_date=$("#filter_date_from").val();
        var to_date=$("#filter_date_to").val();
        var group_by=$("#group_by_id").val();

        if(to_date==""){
            to_date="0";
        }


        if(select_user==""){
            select_user="0";
        }

        if(from_date==""){
            from_date="0";
        }

        if(to_date==""){
            to_date="0";
        }

        if(group_by==""){
            group_by="0";
        }


        location.href=base_url2+"/admin/order/supplier_materials/show_bills/" +
            select_user+"/" +
            from_date+"/" +
            to_date+"/" +
            group_by;

    });
    //END supplier bills


    //branch target
    $(".filter_branch_target").click(function () {

        var branch_id=$("#branch_id_id").val();
        var target_type=$("#target_type_id").val();
        var target_month=$(".target_month").val();
        var target_year=$(".target_year").val();

        if(target_type=="month"){
            if(!(target_year>0)){
                $(".target_year").css("border","1px solid red");
                return false;
            }
            $(".target_year").css("border","1px solid #CCC");

        }
        else if(target_type=="day"){
            if(!(target_year>0)){
                $(".target_year").css("border","1px solid red");
                return false;
            }
            $(".target_year").css("border","1px solid #CCC");

            if(!(target_month>0)){
                $(".target_month").css("border","1px solid red");
                return false;
            }
            $(".target_month").css("border","1px solid #CCC");
        }
    });

    //END branch target



    if($(".add_product_prices").length){


        $(".load_products").click(function(){
            var this_element = $(this);

            var cat_ids=[];
            $.each($("input[name='cat_ids[]']"),function(){
                cat_ids.push($(this).val());
            });


            var object = {};
            object._token = _token;
            object.cat_ids = JSON.stringify(cat_ids);


            this_element.append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 +"/admin/product/get_products_prices",
                data: object,
                type: 'POST',
                success: function (data) {
                    console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.products_table)!="undefined"){
                        this_element.removeAttr("disabled");
                        this_element.children(".ajax_loader_class").remove();

                        $(".load_products_div").html(json_data.products_table);
                        $(".form_submit_btn").removeAttr("disabled");
                        this_element.remove();
                    }
                }

            });


            return false;
        });




    }

    $('body').on("change",'.select_branch_to_get_products',function () {

        var branch_id = $(this).val();
        if(branch_id > 0)
        {
            console.log(branch_id);
            var object = {};
            object.branch_id = branch_id;
            object._token = _token;

            $.ajax({
                url: base_url2+"/admin/branch_bills/get_products_for_save_product_first_time",
                data: object,
                type: 'POST',
                success: function (data) {
                    // console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.success)!="error" && json_data.options != "")
                    {
                        $('.show_products_list').removeAttr("disabled");
                        $('.show_products_list').html(json_data.options);
                    }
                    else{
                        alert(json_data.msg);
                    }

                }

            });

        }
        else{
            $('.show_products_list').html("");
            $('.show_products_list').attr("disabled","disabled");
        }

        return false;
    });

    $('.cal_original_deposite_money').click(function () {

        var this_element = $(this);
        var to_date_id = this_element.parents().find('#to_date_id').val();
        var branch_id = this_element.parents().find('.select_branch').val();

        console.log(branch_id);

        if(typeof(to_date_id) != "undefined" && typeof(branch_id) != "undefined")
        {

            var object = {};
            object._token = _token;
            object.to_date_id=to_date_id;
            object.branch_id =branch_id;


            this_element.append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            console.log(object);

            $.ajax({
                url: base_url2+"/admin/deposite_money/cal_original_deposite_money",
                data: object,
                type: 'POST',
                success: function (data) {
                    // console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data.msg)!="undefined")
                    {
                        this_element.parents().find('.show_original_deposite_money').html(json_data.msg);

                        this_element.removeAttr("disabled");
                        this_element.children(".ajax_loader_class").remove();

                    }

                }

            });

        }


        return false;
    });


    $('body').on('keyup','.filter_left_menu',function(){

        var input = $(this).val();
        var li_items = $(this).parent().find('li a');


        if(input.length > 0)
        {
            $.each(li_items,function (ind,val) {
                // console.log($(this).text());
                // console.log($(this).text().indexOf(input));
                if($(this).text().indexOf(input) == -1)
                {
                    $(this).parent().hide();
                }
                else{
                    $(this).parents().show();
                }

            });
        }
        else{
            $.each(li_items,function (ind,val) {
                $(this).parents().show();
            });
        }

        // console.log(input);
        // console.log(li_items);

    });

    if($('.get_flash_message').length > 0)
    {

        var get_flash_message = $('.get_flash_message').val();

        var str = $('.get_flash_message').val();

        // get alert type
        var type = 'success';
        var pure_msg = '<b style="font-size: 20px;">'+$(str).text()+'</b>';

        if(str.indexOf('alert-danger') > -1)
        {
            type = 'warning';
        }
        else if(str.indexOf('alert-warning') > -1)
        {
            type = 'warning';
        }
        else if(str.indexOf('alert-info') > -1)
        {
            type = 'info';
        }


        if(get_flash_message != "")
        {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "5000",
                "timeOut": "5000",
                "extendedTimeOut": "5000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr[type](pure_msg, '', toastr.options);
        }

    }

    $("body").on("click",".delete_table_row",function () {

        var row_id = $(this).attr('data-row_id');
        var length = $('.table_item').length;
        if (length > 1)
        {
            $res=confirm("هل تريد المسح ؟");

            if($res){
                $('#'+row_id).remove();
            }
        }
        else{
            alert(" غير مسموح !! ");
        }


        return false;
    });

    $("body").on("keyup",".general_input",function(){

        var inputs=$(this).data("inputs");
        $("input[name='"+inputs+"']").val($(this).val());
    });

    $(".change_all_commissions").click(function(){

        var confirmed=confirm("Are you sure?");
        if(confirmed){
            $('.new_general_accept_item[data-accept="1"]').click();
        }
    });

    $('.rotate_barcode').click(function () {

        var rotation = $('.reformat_barcode_block').attr('data-rotation');
        if(rotation == "0")
        {
            $('.reformat_barcode_block').addClass('rotate_barcode_block');
            $('.reformat_barcode_block').attr('data-rotation',"1");
        }
        else{
            $('.reformat_barcode_block').removeClass('rotate_barcode_block');
            $('.reformat_barcode_block').attr('data-rotation',"0");
        }
        return false;
    });

});