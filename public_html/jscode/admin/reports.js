$(function () {


    google.charts.load('current', {'packages':['corechart']});


    if($('#sell_bills').length > 0)
    {

        google.charts.setOnLoadCallback(draw_sell_bills_charts);

        function draw_sell_bills_charts() {

            var items = $('.sell_bills').val();
            items = JSON.parse(items);

            var data_rows = [
                ['الإحصائية', 'العدد'],
                ['إجمالي المبيعات',parseFloat(items.total_amount_of_bills)],
                ['إجمالي المدفوع كاش',parseFloat(items.total_amount_of_bills_in_cash)],
                ['إجمالي المدفوع شبكه',parseFloat(items.total_amount_of_bills_in_atm)],
                ['إجمالي المتبقي',parseFloat(items.total_remain_amount_of_bills)],
                ['إجمالي المرتجع',parseFloat(items.total_amount_of_bills_return)],
                ['إجمالي الخصومات',parseFloat(items.total_discount_of_bills)]
            ];


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المبيعات حسب المدة الزمنية'
            };

            var chart = new google.visualization.PieChart(document.getElementById('sell_bills'));

            chart.draw(data, options);
        }

    }



    if($('#sell_products_chart').length > 0)
    {

        google.charts.setOnLoadCallback(draw_sell_products_chart);

        function draw_sell_products_chart() {

            var sell_items = $('.sell_products_chart').val();
            sell_items = JSON.parse(sell_items);
            console.log(sell_items);
            var data_rows = [
                ['المنتج', 'العدد']
            ];

            $.each(sell_items,function (ind,val) {
                var temp = [
                    ind,parseInt(val)
                ];
                data_rows.push(temp);
            });


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المنتجات التي تم شرائها'
            };

            var chart = new google.visualization.PieChart(document.getElementById('sell_products_chart'));

            chart.draw(data, options);
        }

    }



    if($('#return_products_chart').length > 0)
    {

        google.charts.setOnLoadCallback(draw_return_products_chart);

        function draw_return_products_chart() {

            var sell_items = $('.return_products_chart').val();
            sell_items = JSON.parse(sell_items);
            console.log(sell_items);
            var data_rows = [
                ['المنتج', 'العدد']
            ];

            $.each(sell_items,function (ind,val) {
                var temp = [
                    ind,parseInt(val)
                ];
                data_rows.push(temp);
            });


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المنتجات التي تم إرجاعها'
            };

            var chart = new google.visualization.PieChart(document.getElementById('return_products_chart'));

            chart.draw(data, options);
        }

    }



    if($('#age_range_chart').length > 0)
    {
        google.charts.setOnLoadCallback(draw_age_range_chart);

        function draw_age_range_chart() {

            var items = $('.age_range_chart').val();
            items = JSON.parse(items);

            var data_rows = [
                ['الفئة', 'الإجمالي']
            ];

            $.each(items,function (ind,val) {
                var temp = [
                    ind,parseFloat(val)
                ];
                data_rows.push(temp);
            });


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المبيعات بالنسبه للفئات العمربة'
            };

            var chart = new google.visualization.PieChart(document.getElementById('age_range_chart'));

            chart.draw(data, options);
        }

    }



    if($('#user_gender_chart').length > 0)
    {

        google.charts.setOnLoadCallback(draw_user_gender_chart);

        function draw_user_gender_chart() {

            var items = $('.user_gender_chart').val();
            items = JSON.parse(items);

            var data_rows = [
                ['الفئة', 'الإجمالي']
            ];

            $.each(items,function (ind,val) {

                if(ind == "male")
                    ind = "ذكر";
                else
                    ind = "أنثي";

                var temp = [
                    ind,parseFloat(val)
                ];
                data_rows.push(temp);
            });


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المبيعات بالنسبه للجنس'
            };

            var chart = new google.visualization.PieChart(document.getElementById('user_gender_chart'));

            chart.draw(data, options);
        }

    }



    if($('#products_chart').length > 0)
    {

        google.charts.setOnLoadCallback(draw_products_chart);

        function draw_products_chart() {

            var items = $('.products_chart').val();
            items = JSON.parse(items);

            var data_rows = [
                ['الفئة', 'الإجمالي']
            ];

            $.each(items,function (ind,val) {
                var temp = [
                    ind,parseFloat(val)
                ];
                data_rows.push(temp);
            });


            var data = google.visualization.arrayToDataTable(data_rows);

            var options = {
                title: 'المبيعات بالنسبه للمنتج'
            };

            var chart = new google.visualization.PieChart(document.getElementById('products_chart'));

            chart.draw(data, options);
        }

    }





});