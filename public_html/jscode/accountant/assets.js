$(function () {

    //Start make collapse
    $('body').on('click','.collapse_action',function(){

        $(this).parent().find('.collapse_body').toggleClass('in');

        return false
    });
    //END make collapse

    //Start clone items
    $(".clone_item_to_be_repeated").click(function () {

        var new_item = $(this).parents().find('.item_to_be_repeated').first().clone();
        $(this).parent().parent().find(".container_items").append(new_item);

        return false;
    });

    $("body").on("click",".delete_item_to_be_repeated",function () {

        var length = $(".delete_item_to_be_repeated").length;
        if (length > 1)
        {
            $res=confirm("هل أنت متأكد من مسح هذا العنصر ؟");

            if($res){
                $(this).parents(".item_to_be_repeated").remove();
            }
        }
        else{
            alert(" غير مسموح !! ");
        }


        return false;
    });
    //END clone items


    // Start change asset_deprecation_type

    var asset_age;
    var asset_buy_date;
    var selected_type;

    $('.select_asset_deprecation_type').change(function () {

        selected_type = $(this).val();

        check_valid_fields_to_get_years_in_range(selected_type);

    });


    $('.regenerate_deprecation_rules').click(function () {

        selected_type = $('.select_asset_deprecation_type').val();
        check_valid_fields_to_get_years_in_range(selected_type);
    });


    var check_valid_fields_to_get_years_in_range = function (selected_type) {
        if(selected_type == "depend_on_production")
        {
            asset_age = $('.asset_age').val();
            asset_buy_date = $('.asset_buy_date').val();
            if(typeof(asset_age) == "undefined" || asset_age == "" || asset_age == 0)
            {
                alert(' أدخل عمر الأصل لعرض وتحديد قواعد الإهلاك ');
                $('.select_asset_deprecation_type option').eq(0).attr("selected","selected");
                return;
            }

            if(typeof(asset_buy_date) == "undefined" || asset_buy_date == "")
            {
                alert(' أدخل تاريخ شراء الأصل لعرض وتحديد قواعد الإهلاك ');
                $('.select_asset_deprecation_type option').eq(0).attr("selected","selected");
                return;
            }

            else{
                $('.asset_deprecation_rules').removeClass("hide_div");
                get_years_in_range(asset_age, asset_buy_date);
            }
        }
        else{
            $('.asset_deprecation_rules').addClass("hide_div");
        }
    };


    var get_years_in_range = function (asset_age, asset_buy_date) {


        // remove old items except first
        $.each( $('.asset_deprecation_rules_item'),function (ind,val) {

            if(ind > 0)
            {
                $(this).remove();
            }
        });

        var object = {};
        object._token = _token;
        object.asset_age = asset_age;
        object.asset_buy_date = asset_buy_date;

        $.ajax({
            url: base_url2 + "/accountant/assets/assets/get_years_in_range",
            data: object,
            type: 'POST',
            success: function (data) {
                var json_data=JSON.parse(data);
                // console.log(json_data);

                if(typeof (json_data.data)!="undefined" && json_data.data != ""){

                    // console.log(json_data.data);

                    $.each(json_data.data,function (ind,val) {


                        if($('.asset_rule_from_date').eq((ind-1)).length == 0)
                        {
                            var new_item = $('.asset_deprecation_rules_item').eq(0).clone();
                            $('.asset_deprecation_rules_item').parent('.container_items').append(new_item);
                        }

                        $('.asset_rule_from_date').eq((ind-1)).val(val.from);
                        $('.asset_rule_to_date').eq((ind-1)).val(val.to);

                    });

                }

            }

        });

    };

    // End change asset_deprecation_type



    // Start get asset document files

        $('.show_document_files').click(function () {

            var get_files = $(this).attr('data-items');

            get_files = JSON.parse(get_files);

            console.log(get_files);

            $('.asset_doc_attachment_files').removeClass('hide_div');

            $('.asset_doc_attachment_files').find('.collapse_body').html("");


            var html = "";

            if(get_files.length > 0)
            {
                $.each(get_files,function (ind,val) {

                    html += '<a href="'+base_url2+'/'+val.path+'" target="_blank">'
                    html += 'مشاهدة <<';
                    html += '</a>';
                    html += '<br>';

                });

            }
            else{
                html += '<div class="alert alert-danger">';
                html += 'لا تحتوي علي ملفات';
                html += '</div>';
            }

            $('.asset_doc_attachment_files').find('.collapse_body').html(html);

            return false;
        });

    // End get asset document files



    // Start generate installments

        $('.generate_installments_btn').click(function () {

            var custom_installment_value = $(this).parents().find('.custom_installment_value').val();
            var custom_installment_numbers = $(this).parents().find('.custom_installment_numbers').val();
            var custom_installment_first_date = $(this).parents().find('.custom_installment_first_date').val();
            var custom_next_installment_days = $(this).parents().find('.custom_next_installment_days').val();

            if(
                (typeof(custom_installment_value) != "undefined" && custom_installment_value > 0) &&
                (typeof(custom_installment_numbers) != "undefined" && custom_installment_numbers > 0) &&
                (typeof(custom_installment_first_date) != "undefined" && custom_installment_first_date != "") &&
                (typeof(custom_next_installment_days) != "undefined" && custom_next_installment_days >0)
            )
            {

                // remove old items except first
                $.each( $('.generated_installments_item'),function (ind,val) {

                    if(ind > 0)
                    {
                        $(this).remove();
                    }
                });

                // console.log(custom_installment_value);
                // console.log(custom_installment_numbers);
                // console.log(custom_installment_first_date);
                // console.log(custom_next_installment_days);

                $('.generated_installments_item').removeClass('hide_div');
                custom_installment_first_date = new Date(custom_installment_first_date);
                var target_date = custom_installment_first_date;

                for(var ind = 0;ind<custom_installment_numbers;ind++)
                {
                    // console.log(ind);

                    if(ind > 0)
                    {
                        var new_item = $('.generated_installments_item').eq(0).clone();
                        $('.show_generated_installments').append(new_item);
                        target_date = target_date.setDate(target_date.getDate()+(parseInt(custom_next_installment_days)));
                        target_date = new Date(target_date);
                    }


                    var output_date = target_date.getFullYear() + '-' + (target_date.getMonth()+1) + '-' + target_date.getDate();

                    $('.generated_installments_item').eq(ind).find('.generated_installment_value').val(custom_installment_value);
                    $('.generated_installments_item').eq(ind).find('.generated_installment_date').val(output_date);

                }

                if ($('.generated_installments_item').length > 0)
                {
                    $('.submit_installment').removeAttr("disabled");
                }

            }
            else{

                alert("برجاء ملئ كافة الحقول لتكملة إنشاء الأقساط");

            }

            return false;
        });

    // End generate installments


    // Start Calc Deprecation

        $('.calc_assets_deprecations').click(function () {

            var this_element = $(this);
            this_element.attr("disabled");
            $('.show_deprecation_msg').html("<div class='alert alert-warning'> برجاء الإنتظار قد تستغرق وقت ... </div>");

            $.ajax({
                url: base_url2 + "/accountant/assets/assets_deprecation/calc_deprecation",
                data: {_token:_token},
                type: 'POST',
                success: function (data) {
                    var json_data=JSON.parse(data);
                    // console.log(json_data);
                    this_element.removeAttr("disabled");

                    if(typeof (json_data.success)!="undefined" && json_data.success != "" && json_data.success != ""){

                        $('.show_deprecation_msg').html(json_data.msg);
                    }

                }

            });

            return false;
        });

    // End Calc Deprecation

});