$(function () {


    $("#select_entry_template_id").change(function(){

        var this_element=$(this);

        var object = {};
        object._token = _token;
        object.template_id = this_element.val();

        this_element.parents(".form-group").append(ajax_loader_img_func("15px"));


        var ajax_url="/accountant/entry/get_template_items";
        if($(".save_entry_template").length>0){
            ajax_url="/accountant/entry/get_template_items_for_template";
        }

        $.ajax({
            url: base_url2 + ajax_url,
            data: object,
            type: 'POST',
            success: function (data) {
                var json_data=JSON.parse(data);

                console.log(json_data);
                this_element.parents(".form-group").children("img").remove();

                if(typeof (json_data.template_items_html)!="undefined"){
                    $(".entries_table tbody").html(json_data.template_items_html);
                    $(".credit_cell").change();
                    $(".debit_cell").change();
                }

            }

        });


    });

    $(".add_entry_item").click(function () {
        var copy_row=$(".entry_row").first().clone();
        $(".credit_cell,.debit_cell",copy_row).val("");

        $(".accounts_div,.consuming_div",copy_row).html("");

        $(".entries_table tbody").append(copy_row);
        $(".credit_cell,.debit_cell",copy_row).change();

        $(".select_consuming_accounts .show_tree_modal",copy_row).attr("data-input_field_name","selected_consuming_accounts_"+($(".entry_row").length-1));

        return false;
    });

    $("body").on("change",".credit_cell",function(){
        var this_val=parseFloat($(this).val());


        //if there is value here make debit cell read only
        if(this_val>0){
            $(".debit_cell",$(this).parents(".entry_row")).attr("readonly","readonly");
        }
        else{
            $(".debit_cell",$(this).parents(".entry_row")).removeAttr("readonly");
        }

        $("input[name='value_of_selected_consuming_accounts[][]']").change();
        check_entry_balance();
    });

    $("body").on("keyup",".credit_cell",function(){
        $(this).change();
    });

    $("body").on("change",".debit_cell",function(){
        var this_val=parseFloat($(this).val());

        //if there is value here make debit cell read only
        if(this_val>0){
            $(".credit_cell",$(this).parents(".entry_row")).attr("readonly","readonly");
        }
        else{
            $(".credit_cell",$(this).parents(".entry_row")).removeAttr("readonly");
        }

        $("input[name='value_of_selected_consuming_accounts[][]']").change();
        check_entry_balance();
    });

    $("body").on("keyup",".debit_cell",function(){
        $(this).change();
    });

    var check_entry_balance=function(){

        var credit_cells_val=0;
        var debit_cells_val=0;

        $.each($(".credit_cell"),function(){

            var this_val=parseFloat($(this).val());
            if(!(this_val>0)){
                this_val=0;
            }

            credit_cells_val=credit_cells_val+this_val;
        });

        $.each($(".debit_cell"),function(){

            var this_val=parseFloat($(this).val());
            if(!(this_val>0)){
                this_val=0;
            }

            debit_cells_val=debit_cells_val+this_val;
        });

        if(debit_cells_val!=credit_cells_val){
            $(".entry_msgs").html("<div class='alert alert-danger'>طرفي القيد غير متساويين.لا يمكنك اكمال العملية </div>");
            $("input[type='submit']").attr("disabled","disabled");

        }
        else{
            $(".entry_msgs").html("");
            $("input[type='submit']").removeAttr("disabled");
        }

    };

    $("body").on("click",".remove_entry_item",function(){

        if(confirm("هل انت متأمج؟")){

            if($(".entry_row").length>1){
                $(this).parents(".entry_row").remove();

            }
            else{
                $(this).html("لا يمكنك مسح البند . لانه لا يوجد غيره");
            }

        }

        return false;
    });

    $("body").on("keyup","input[name='value_of_selected_consuming_accounts[][]']",function(){
       $(this).change();
    });

    $("body").on("change","input[name='value_of_selected_consuming_accounts[][]']",function(){

        var this_element=$(this);
        var entry_row=this_element.parents(".entry_row");

        var get_debit_or_credit_val=
            (parseFloat($(".credit_cell",entry_row).val())>0)?parseFloat($(".credit_cell",entry_row).val()):parseFloat($(".debit_cell",entry_row).val());

        if(!(get_debit_or_credit_val>0)){
            get_debit_or_credit_val=0;
        }

        //get all values of value_of_selected_consuming_accounts in this entry_row

        var total_val=0;
        $.each($("input[name='value_of_selected_consuming_accounts[][]']",entry_row),function(){
            var item_val=parseFloat($(this).val());
            if(!(item_val>0)){
                item_val=0;
            }

            total_val=item_val+total_val;
        });


        if(total_val>get_debit_or_credit_val){
            $("input[type='submit']").attr("disabled","disabled");
            $(".consuming_msgs",entry_row).html("لا يمكنك ان يكون مجموع الارقام الموجود في المدخلات اعلاه.اكبر من خانة الدائن او المدين");
        }
        else{
            $("input[type='submit']").removeAttr("disabled");
            $(".consuming_msgs",entry_row).html("");
        }

    });


    $(".save_template_only_class").change(function () {

        if($(this).is(":checked")){
            $(".main_entry_data_div").hide();
        }
        else{
            $(".main_entry_data_div").show();
        }
    });

    $(".save_template_only_class").change();


    $(".approve_entry_btn").click(function(){
        var this_element=$(this);

        var object = {};
        object._token = _token;
        object.entry_id = this_element.data("entry_id");

        this_element.append(ajax_loader_img_func("15px"));
        this_element.attr("disabled","disabled");

        $.ajax({
            url: base_url2 + "/accountant/entry/approve_entry",
            data: object,
            type: 'POST',
            success: function (data) {
                this_element.removeAttr("disabled");

                var json_data=JSON.parse(data);

                console.log(json_data);
                this_element.children("img").remove();

                if(typeof (json_data.msg)!="undefined"){
                    this_element.html(json_data.msg);
                }
            }
        });


        return false;
    });


});