jQuery(function ($) {


    if($('#cat_table').length > 0)
    {
        $('#cat_table').DataTable();
    }

    /* Start Get Time */

    var d = new Date(); // for now
    var hours = (d.getHours());
    var minutes = d.getMinutes();
    var time_status = "AM";
    if(d.getHours() > 12)
    {
        time_status = "PM";
        hours = hours % 12;
    }

    $(".show_time").html(hours+":"+minutes+" "+time_status);

    /* End Get Time */


    /* Start Send Custom Email */

    $('.send_custom_email').click(function () {

        var this_element = $(this);
        this_element.attr("disabled","disabled");
        this_element.append(ajax_loader_img);

        var receiver_email_element = this_element.parents().find(".receiver_email");
        var receiver_email = this_element.parents().find(".receiver_email").val();

        var email_body_element = this_element.parents().find(".email_body");
        var email_body = this_element.parents().find(".email_body").val();
        if (typeof(receiver_email) == "undefined" || receiver_email == "")
        {
            receiver_email_element.css({"border": "2px solid red"});
            this_element.removeAttr("disabled");
            $(".ajax_loader_class").remove();
        }
        else if(typeof(email_body) == "undefined" || email_body == "")
        {
            receiver_email_element.css({"border": "2px solid green"});
            email_body_element.css({"border": "2px solid red"});
            this_element.removeAttr("disabled");
            $(".ajax_loader_class").remove();
        }
        else {

            receiver_email_element.css({"border": "2px solid green"});
            email_body_element.css({"border": "2px solid green"});


            var object = {};
            object._token = _token;
            object.receiver_email = receiver_email;
            object.email_body = email_body;

            $.ajax({
                url: base_url2 + "/send_custom_email",
                data: object,
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    this_element.removeAttr("disabled");
                    $(".ajax_loader_class").remove();
                    this_element.parents().find('.show_custom_email_msg').html(return_data.msg);
                }

            });
        }

        return false;
    });

    /* End Send Custom Email */


    /* Start generate tree */



    /* End generate tree */

});

